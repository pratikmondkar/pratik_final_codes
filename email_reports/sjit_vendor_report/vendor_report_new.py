import pandas as pd
import numpy as np
import datetime
from datetime import date, timedelta
from pandas import DataFrame
import sqlalchemy as sq
import smtplib
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import gspread
import pandas as pd
import httplib2
from oauth2client.file import Storage

pd.set_option('max_colwidth',5000)

storage = Storage('/home/admin/email_reports/data/creds/gsheet_creds.data')
credentials = storage.get()
http = credentials.authorize(httplib2.Http())
credentials.refresh(http)

gc = gspread.authorize(credentials)

book = gc.open("SJIT vendor details")
sheet = book.sheet1 #choose the first sheet
vendor_details = pd.DataFrame(sheet.get_all_records())

engine = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

sql_po_sku="""
WITH s AS
(
SELECT fpo.po_sku_id,
       fpo.sku_id,
       SUM(fpo.quantity)
FROM fact_purchase_order fpo
WHERE (fpo.po_type LIKE 'JIT_MARKETPLACE' AND fpo.prioritization LIKE 'CENTRAL_REPLENISHMENT')
OR    fpo.po_type = 'SMART_JIT'
GROUP BY 1,
         2
)
SELECT 
     brand,
     SKU,
     PO_Code,
     Total_Inward,
     Inward_QC_Reject,
     Returns_QC_Rejection,
     (On_Hand_Inventory + BBD_Inventory) as On_Hand,
     Shipped,
     Unsold_RTV,
     (Total_Inward - Inward_QC_Reject - Returns_QC_Rejection - On_Hand_Inventory - Shipped - Unsold_RTV - BBD_Inventory) AS Returns_In_Transit
FROM (SELECT 
           sku_code AS SKU,
           po_barcode AS PO_Code,
	   brand,
           SUM(CASE WHEN action_status = 'RO_SOR_ITEM' THEN 1 ELSE 0 END) AS Unsold_RTV,
           COUNT(*) AS Total_Inward,
           SUM(CASE WHEN rejected_at = 'NOT_RECEIVED' THEN 1 ELSE 0 END) AS Inward_QC_Reject,
           SUM(CASE WHEN rejected_at <> 'NOT_RECEIVED' AND action_status <> 'RO_SOR_ITEM' THEN 1 ELSE 0 END) AS Returns_QC_Rejection,
           SUM(CASE WHEN item_status = 'STORED' AND quality = 'Q1' AND action_status IS NULL THEN 1 ELSE 0 END) AS On_Hand_Inventory,
           SUM(CASE WHEN item_status IN ('SHIPPED','ISSUED') AND quality = 'Q1' AND action_status IS NULL THEN 1 ELSE 0 END) AS Shipped,
           SUM(CASE when item_status in ('PROCESSING') and quality = 'Q1' and action_status is null then 1 else 0 end) as BBD_Inventory
    FROM (SELECT DISTINCT fii.core_item_id,
                 dp.brand,
                 dp.sku_code,
                 fii.item_status,
                 fii.inward_date,
                 fii.quality,
                 fii.sku_id,
                 fii.po_barcode,
                 fii.rejected_at,
                 fii.action_status
          FROM fact_inventory_item fii
            JOIN dim_product dp ON fii.sku_id = dp.sku_id
            JOIN s ON fii.po_sku_id = s.po_sku_id
          WHERE fii.item_status NOT IN ('NOT_RECEIVED','DELETED') 
--  and upper(dp.brand) = %(brand)s)
    GROUP BY 1,
             2,
	     3) AS u
"""

sql_po="""
WITH s AS
(
 SELECT fpo.po_sku_id,
        fpo.sku_id,
        SUM(fpo.quantity)
 FROM fact_purchase_order fpo
 WHERE (fpo.po_type LIKE 'JIT_MARKETPLACE' AND fpo.prioritization LIKE 'CENTRAL_REPLENISHMENT')
 OR    fpo.po_type = 'SMART_JIT'
 GROUP BY 1,
          2
)
SELECT
      brand,
      PO_Code,
      Total_Inward,
      Inward_QC_Reject,
      Returns_QC_Rejection,
      (On_Hand_Inventory + BBD_Inventory) as On_Hand,
      Shipped,
      Unsold_RTV,
      (Total_Inward - Inward_QC_Reject - Returns_QC_Rejection - On_Hand_Inventory - Shipped - Unsold_RTV - BBD_Inventory) AS Returns_In_Transit
FROM (SELECT 
            po_barcode AS PO_Code,
	    brand,
            SUM(CASE WHEN action_status = 'RO_SOR_ITEM' THEN 1 ELSE 0 END) AS Unsold_RTV,
            COUNT(*) AS Total_Inward,
            SUM(CASE WHEN rejected_at = 'NOT_RECEIVED' THEN 1 ELSE 0 END) AS Inward_QC_Reject,
            SUM(CASE WHEN rejected_at <> 'NOT_RECEIVED' AND action_status <> 'RO_SOR_ITEM' THEN 1 ELSE 0 END) AS Returns_QC_Rejection,
            SUM(CASE WHEN item_status = 'STORED' AND quality = 'Q1' AND action_status IS NULL THEN 1 ELSE 0 END) AS On_Hand_Inventory,
            SUM(CASE WHEN item_status IN ('SHIPPED','ISSUED') AND quality = 'Q1' AND action_status IS NULL THEN 1 ELSE 0 END) AS Shipped,
            SUM(CASE when item_status in ('PROCESSING') and quality = 'Q1' and action_status is null then 1 else 0 end) as BBD_Inventory
     FROM (SELECT DISTINCT fii.core_item_id,
                  dp.brand,
                  dp.sku_code,
                  fii.item_status,
                  fii.inward_date,
                  fii.quality,
                  fii.sku_id,
                  fii.po_barcode,
                  fii.rejected_at,
                  fii.action_status
           FROM fact_inventory_item fii
             JOIN dim_product dp ON fii.sku_id = dp.sku_id
             JOIN s ON fii.po_sku_id = s.po_sku_id
           WHERE fii.item_status NOT IN ('NOT_RECEIVED','DELETED')
 --          AND   upper(dp.brand) = %(brand)s
           )
     GROUP BY 1,2
     ) AS u
"""

sql_reject_reasons="""
------------------------------------------------------------
------------ Rejection reasons -------------------

WITH s AS
(
SELECT fpo.po_sku_id,
       fpo.sku_id,
       SUM(fpo.quantity)
FROM fact_purchase_order fpo
WHERE (fpo.po_type LIKE 'JIT_MARKETPLACE' AND fpo.prioritization LIKE 'CENTRAL_REPLENISHMENT')
OR    fpo.po_type = 'SMART_JIT'
GROUP BY 1,
         2
)
SELECT 
     SKU,
     PO_Code,
     brand,
     rejected_at,
     reject_reason_description,
     Rejects_count
FROM (SELECT 
           sku_code AS SKU,
           po_barcode AS PO_Code,
           brand,
           rejected_at,
           reject_reason_description,
           SUM(1) AS Rejects_count
    FROM (SELECT DISTINCT fii.core_item_id,
                 dp.brand,
                 dp.sku_code,
                 fii.item_status,
                 fii.inward_date,
                 fii.quality,
                 fii.sku_id,
                 fii.po_barcode,
                 fii.rejected_at,
                 fii.reject_reason_description,
                 fii.action_status
          FROM fact_inventory_item fii
            JOIN dim_product dp ON fii.sku_id = dp.sku_id
            JOIN s ON fii.po_sku_id = s.po_sku_id
          WHERE fii.item_status NOT IN ('NOT_RECEIVED','DELETED') 
 --         and upper(dp.brand) = %(brand)s
          and rejected_at = 'NOT_RECEIVED'
          and fii.inward_date >= to_char(sysdate - Interval '2 months','YYYYMMDD')
          )
    GROUP BY 1,
             2,
             3,
             4,
             5) AS u

"""

po_sku={}
po={}
reject_reasons={}

#for i in vendor_details['Brand']:    
po_sku_all = pd.read_sql_query(sql_po_sku,engine)
po_all = pd.read_sql_query(sql_po,engine)
reject_reasons_all = pd.read_sql_query(sql_reject_reasons,engine)
print "%s data pull completed" % i

outpath = r'/home/admin/email_reports/data/sjit_vendor_report/'
filename_po_sku={}
filename_reject_reasons={}
gls=pd.read_csv('/home/admin/email_reports/data/sjit_vendor_report/glossary.csv',error_bad_lines=False)
for i in vendor_details['Brand']:
    filename_po_sku[i]=outpath+ i +"_po_sku.csv"
    po_sku[i].to_csv(filename_po_sku[i],index=False)
    filename_reject_reasons[i]=outpath+ i +"_reject_reasons_Inward_QC.csv"
    reject_reasons[i].to_csv(filename_reject_reasons[i],index=False)
    sender   = 'mpsmartjit1@myntra.com'
    TOADDR   = vendor_details[['To']][vendor_details['Brand']==i].to_string(index=False,header=False)
    CCADDR   = vendor_details[['CC']][vendor_details['Brand']==i].to_string(index=False,header=False)
    msg = MIMEMultipart()
    msg['Subject'] = 'Smart JIT '+ i +' vendor report'
    msg['From'] =sender
    msg['to'] = TOADDR
    msg['cc'] = CCADDR
    t="Hi\n Please find the latest status for Smart JIT Inventory. \n Please use the key below to read the table:"
    
    h=po[i].to_html(index=False)
    g=gls.to_html(index=False)
    part1 = MIMEText(t,'plain')
    part2 = MIMEText(g,'html')
    part3 = MIMEText(h,'html')
    msg.attach(part1)
    msg.attach(part2)  
    msg.attach(part3)
    
    f = file(filename_po_sku[i])
    attachment1 = MIMEText(f.read())
    attachment1.add_header('Content-Disposition', 'attachment', filename=filename_po_sku[i])
    msg.attach(attachment1)
    
    r = file(filename_reject_reasons[i])
    attachment2 = MIMEText(r.read())
    attachment2.add_header('Content-Disposition', 'attachment', filename=filename_reject_reasons[i])
    msg.attach(attachment2)
    
    toaddrs=TOADDR.split(",")+CCADDR.split(",")

    try:
       smtpObj = smtplib.SMTP('smtp.gmail.com:587')
       smtpObj.ehlo()
       smtpObj.starttls()
       smtpObj.login("mpsmartjit1@myntra.com", "dssckksafegiiiyi")
       smtpObj.sendmail(sender, toaddrs, msg.as_string())
       smtpObj.close()
       print "Successfully sent email"
    except :
       print "Error: unable to send email"
