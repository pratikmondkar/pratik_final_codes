#Code for creating wordcloud:
import pandas as pd
import datetime
import sqlalchemy as sq
from PIL import Image
import numpy as np
from os import path
from pandas import DataFrame
from datetime import date, timedelta
import nltk
from nltk import FreqDist
from wordcloud import WordCloud, STOPWORDS
import smtplib
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage

engine = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

#variable to go back in time in no. of days 
td=0
#Benchmark time period in days
bk=5
    
yst = date.today() - timedelta(td)
fn_tday = "yday_"+(yst.strftime("%Y%m%d")+".png")
fn_bench = "bench_"+(yst.strftime("%Y%m%d")+".png")
fn_index = "final_"+(yst.strftime("%Y%m%d")+".png")
fn_freq  = "freq_"+(yst.strftime("%Y%m%d")+".csv")

sql_str_yday="""
SELECT case when suggest_usage='true' then suggest_text else search_text end as search_text
FROM clickstream.events_view
WHERE event_type='SearchFired' and search_text IS NOT NULL
AND   load_date = TO_CHAR(convert_timezone('Asia/Calcutta',sysdate)-interval '%(ds)s day','YYYYMMDD')::bigint  
            """
sql_str_bench="""
select case when suggest_usage='true' then suggest_text else search_text end as search_text
from clickstream.events_view  
where event_type in ('SearchFired') and search_text IS NOT NULL and
load_date between to_char(sysdate-interval '%(ds1)s day','YYYYMMDD')::bigint and to_char(sysdate-interval '%(ds2)s day','YYYYMMDD')::bigint 
"""

yday=pd.read_sql_query(sql_str_yday,engine,params={"ds":td})
bench=pd.read_sql_query(sql_str_bench,engine,params={"ds1":td+bk,"ds2":td+1})

yday_clean=yday.apply(lambda s:s.str.replace("'", ""))
bench_clean=bench.apply(lambda s:s.str.replace("'", ""))

upsw=STOPWORDS.copy()
upsw.update(['page','shopping','men','women','girl','boy','null'])

yday_wrds2 = nltk.tokenize.word_tokenize(yday_clean.to_string().lower())
yday_wrds2= [w for w in yday_wrds2 if not w in upsw]
yday_freq2=FreqDist(yday_wrds2).items()

bench_wrds2 = nltk.tokenize.word_tokenize(bench_clean.to_string().lower())
bench_wrds2= [w for w in bench_wrds2 if not w in upsw]
bench_freq2=FreqDist(bench_wrds2).items()

yday_freq_wrds2= pd.DataFrame(FreqDist(yday_wrds2).most_common(10000),columns=['Words', 'yday_freq']) 
bench_freq_wrds2 = pd.DataFrame(FreqDist(bench_wrds2).most_common(10000),columns=['Words', 'bench_freq']) 

outpath = r'/home/python_notebooks/Akash/'

freq2=pd.merge(bench_freq_wrds2,yday_freq_wrds2 ,on='Words',how='outer')
freq2['bench']=100*freq2['bench_freq']/freq2['bench_freq'].sum()
freq2['yday']=100*freq2['yday_freq']/freq2['yday_freq'].sum()
freq2['index']=freq2['yday']/freq2['bench']

freq_index2=freq2[freq2['yday_freq']>500]
freq_index_filt2=freq_index2[['Words','index']]
freq_index_filt2.sort_values(['index'], ascending=[0], inplace=[1])
fn2=[tuple(x) for x in freq_index_filt2.values]

freq_index2.sort_values(['index'], ascending=[0], inplace=[1])
freq_index2.to_csv(outpath + "/" +fn_freq,index=False,encoding ='utf-8')

img_mask = np.array(Image.open('/home/python_notebooks/Akash/myntra.png'))

wordcloud = WordCloud(background_color='Black',
                          width=2400,
                          height=2000,
                          mask=img_mask,
                          font_path='/home/python_notebooks/Akash/lobster-two.bold-italic.ttf'
                         ).generate_from_frequencies(yday_freq2)

wordcloud.to_file(outpath + "/" + fn_tday)

wordcloud = WordCloud(background_color='White',
                          width=2400,
                          height=2000,
                          mask=img_mask,
                          font_path='/home/python_notebooks/Akash/lobster-two.bold-italic.ttf'
                         ).generate_from_frequencies(fn2)

wordcloud.to_file(outpath + "/" + fn_index)


sender = 'pratik.mondkar@myntra.com'
#receivers = ['growth_core@myntra.com','vinayak.naik@myntra.com','gaurav.prateek@myntra.com','shrinivas.ron@myntra.com','pratik.mondkar@myntra.com','anjali.bohara@myntra.com','akash.chaturvedi@myntra.com']
#receivers = ['akash.chaturvedi@myntra.com','shrinivas.ron@myntra.com','pratik.mondkar@myntra.com','pankaj.soni@myntra.com']
receivers=['anjali.bohara@myntra.com']
msg = MIMEMultipart('related')
msg['Subject'] = 'Search Keywords report'
msg['From'] =sender
msg['to'] =", ".join(receivers)

msgAlternative = MIMEMultipart('alternative')
msg.attach(msgAlternative)

img=open(outpath + "/" + fn_tday, 'rb').read()
img2=open(outpath + "/" + fn_index, 'rb').read()

msgText = MIMEText('Hi\nPlease find the search report for absolute search terms. \nThe frequencies are also attached as a csv.<br><img src="cid:image1"><br>', 'html')
msgAlternative.attach(msgText)

part2 = MIMEImage(img)
part2.add_header('Content-ID', '<image1>')
msg.attach(part2)

image = MIMEImage(img2, name=path.basename(fn_index))
msg.attach(image)

r = file(outpath + "/" +fn_freq)
attachment2 = MIMEText(r.read())
attachment2.add_header('Content-Disposition', 'attachment', filename=outpath + "/" +fn_freq)
msg.attach(attachment2)

try:
    smtpObj = smtplib.SMTP('smtp.gmail.com:587')
    smtpObj.ehlo()
    smtpObj.starttls()
    smtpObj.login("pratik.mondkar@myntra.com", "kaxuftanzoyzmjfe")
    smtpObj.sendmail(sender, receivers, msg.as_string())
    smtpObj.close()
    print "Successfully sent email"
except :
    print "Error: unable to send email"
