import pandas as pd
from pandas import DataFrame
import sqlalchemy as sq
import datetime
from datetime import date, timedelta

engine = sq.create_engine("postgresql+psycopg2://analysis_user:AdhoCus@123!@dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

devices = pd.read_sql_query("""
select device_id as androidid,customer_id as userid,max(load_date)
from clickstream.events_view
where event_type in ('push-notification-received','beacon-ping','ScreenLoad') and
load_date between to_char(sysdate - interval '15 days','YYYYMMDD')::bigint and to_char(sysdate - interval '1 days','YYYYMMDD')::bigint
group by 1,2
"""
, engine)

print "Data pull completed"
print "Sorting.."

devices['length']=devices['userid'].str.len()
devices.sort(['androidid','length','max'], ascending=[1,0,0], inplace=[1])

print "Data sorted"
print "Deduping.."

final = devices.drop_duplicates(subset='androidid')
final.drop(['max','length'], axis=1, inplace=True)

print "Data deduped"
print "Writing file.."

outpath = '/home/admin/email_reports/data/device_list_nai'
yst = date.today() - timedelta(1)
filename = "device_list_active_base_"+(yst.strftime("%Y%m%d")+".csv")
final.to_csv(outpath + "/" + filename,index=False,encoding ='utf-8')

print "Completed"
