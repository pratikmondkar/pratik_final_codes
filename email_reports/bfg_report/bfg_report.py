import pandas as pd
import numpy as np
import datetime
from datetime import date, timedelta
from pandas import DataFrame
import sqlalchemy as sq
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
engine = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

sql_str1="""
select 
TO_CHAR(ROUND(SUM(item_revenue + nvl(cashback_redeemed,0)),0),'99,99,99,99,999') as revenue,
TO_CHAR(count(distinct order_group_id),'99,99,99,99,999') as orders,
round(sum(trade_discount)*100/nullif(sum(item_mrp*item_quantity),0),2) as td,
round(sum(coupon_discount)*100/nullif(sum(item_mrp*item_quantity),0),2) as cd,
round(SUM(item_revenue + nvl(cashback_redeemed,0))/nullif(count(distinct order_group_id),0),2) as asp,
round((sum(item_revenue + cashback_redeemed - tax_amount)-sum(item_purchase_price_inc_tax - vendor_funding + nvl(royalty_commission,0)))*100/nullif(sum(item_revenue + cashback_redeemed - tax_amount),0),2) as gm_perc
from fact_order_live 
where (is_shipped=1 or is_realised=1) and cast(order_created_date ||' '|| lpad(order_created_time,4,0) as timestamp) between
to_char(convert_timezone('Asia/Calcutta',sysdate - interval '1 hour'),'YYYY-MM-DD HH24:00:00') and to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYY-MM-DD HH24:00:00')
and upper(coupon_code) in 
('LUCKY10',	'BIG25',	'BIG10',	'BIG22',	'BIG40',	'BIG50',	'BIG501',	'BIG25',	'BIG30',	'BIG4U22S1',	'BIG4U22S2',	'BIG4U22S3',	'BIG4U22S4',	'BIG4U22S5',	'BIGHH25',	' LUCKY15',	'BIG15',	'BIG20',	'BIG40',	'BIG20',	'BIG35',	' LUCKY20',	'BIG20',	'BIG18',	'BIG30',	'BIG40')
"""

sql_str2="""
select coupon_code,TO_CHAR(a.revenue,'99,99,99,99,999') as revenue,orders,td,cd,asp,gm_perc
,round(a.revenue*100/nullif(b.revenue,0),2) as revenue_contribution
from
(select upper(coupon_code) as coupon_code,
ROUND(SUM(item_revenue + nvl(cashback_redeemed,0)),0) as revenue,
TO_CHAR(count(distinct order_group_id),'99,99,99,99,999') as orders,
round(sum(trade_discount)*100/nullif(sum(item_mrp*item_quantity),0),2) as td,
round(sum(coupon_discount)*100/nullif(sum(item_mrp*item_quantity),0),2) as cd,
round(SUM(item_revenue + nvl(cashback_redeemed,0))/nullif(count(distinct order_group_id),0),2) as asp,
round((sum(item_revenue + cashback_redeemed - tax_amount)-sum(item_purchase_price_inc_tax - vendor_funding + nvl(royalty_commission,0)))*100/nullif(sum(item_revenue + cashback_redeemed - tax_amount),0),2) as gm_perc
from fact_order_live 
where (is_shipped=1 or is_realised=1) and cast(order_created_date ||' '|| lpad(order_created_time,4,0) as timestamp) between
to_char(convert_timezone('Asia/Calcutta',sysdate - interval '1 hour'),'YYYY-MM-DD HH24:00:00') and to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYY-MM-DD HH24:00:00')
and upper(coupon_code) in 
('LUCKY10',	'BIG25',	'BIG10',	'BIG22',	'BIG40',	'BIG50',	'BIG501',	'BIG25',	'BIG30',	'BIG4U22S1',	'BIG4U22S2',	'BIG4U22S3',	'BIG4U22S4',	'BIG4U22S5',	'BIGHH25',	' LUCKY15',	'BIG15',	'BIG20',	'BIG40',	'BIG20',	'BIG35',	' LUCKY20',	'BIG20',	'BIG18',	'BIG30',	'BIG40')
group by 1
order by revenue desc) a, 
(select ROUND(SUM(item_revenue + nvl(cashback_redeemed,0)),0) as revenue
from fact_order_live 
where (is_shipped=1 or is_realised=1) and cast(order_created_date ||' '|| lpad(order_created_time,4,0) as timestamp) between
to_char(convert_timezone('Asia/Calcutta',sysdate - interval '1 hour'),'YYYY-MM-DD HH24:00:00') and to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYY-MM-DD HH24:00:00')) b 
order by revenue_contribution desc
"""

report1=pd.read_sql_query(sql_str1,engine)
report2=pd.read_sql_query(sql_str2,engine)

report1.round(decimals=0)
report2.round(decimals=0)

a1=report1.to_html(index=False)
a2=report2.to_html(index=False)
t1 ="Hi,\nPlease find below the overall performance and the coupon wise performance in the last hour.\nOverall peformance:"
t2 ="Coupon wise performance :"

sender = 'pratik.mondkar@myntra.com'
receivers = ['naresh.krishnaswamy@myntra.com','anjali.bohara@myntra.com','vinayak.naik@myntra.com','anuj.sharma@myntra.com','bhavesh.singhal@myntra.com','apoorv.kalra@myntra.com','aashish.kumar@myntra.com','mohit.panjwani@myntra.com','pricing@myntra.com','neha.malhan@myntra.com']
#receivers = ['pratik.mondkar@myntra.com']

msg = MIMEMultipart()
msg['Subject'] = 'Big 10 coupon performance report'
msg['From'] =sender
msg['to'] =", ".join(receivers)


part1 = MIMEText(t1,'plain')
part2 = MIMEText(a1,'html')
part3 = MIMEText(t2,'plain')
part4 = MIMEText(a2,'html')
msg.attach(part1)
msg.attach(part2)
msg.attach(part3)
msg.attach(part4)

try:
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login("pratik.mondkar@myntra.com", "kaxuftanzoyzmjfe")
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"
