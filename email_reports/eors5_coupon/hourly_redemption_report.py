import pandas as pd
import numpy as np
import datetime
from datetime import date, timedelta
from pandas import DataFrame
import sqlalchemy as sq
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
engine = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

sql_str2="""
SELECT 
       order_created_time / 100 AS hour,
       TO_CHAR(ROUND(SUM(item_revenue + nvl(cashback_redeemed,0)),0),'99,99,99,99,999') AS revenue,
       TO_CHAR(COUNT(DISTINCT order_group_id),'99,99,99,99,999') AS orders,
       round(sum(coupon_discount)*100 /sum(item_mrp*item_quantity),2) as cd,
       round(sum(trade_discount)*100 /sum(item_mrp*item_quantity),2) as td,
       round(sum(item_revenue + nvl(cashback_redeemed,0) - nvl(tax_amount,0) - nvl(item_purchase_price_inc_tax,0) + nvl(vendor_funding,0) - nvl(royalty_commission,0))*100/sum(item_revenue + cashback_redeemed - tax_amount),2) as gm
FROM fact_order_live fci
WHERE (is_shipped = 1 OR is_realised = 1)
AND  coupon_code ilike ('%%HHSUN1RFG3%%') or  coupon_code ilike ('%%HHSUN2HDS4%%') or  coupon_code ilike ('%%HHSUN3BHN5%%')
or  coupon_code ilike ('%%HHSUN4GNB6%%') or  coupon_code ilike ('%%HAPPY22%%')
AND   order_created_date =20170430
GROUP BY 1
ORDER BY 1
"""

sql_str1="""
select TO_CHAR(ROUND(SUM(item_revenue + nvl(cashback_redeemed,0)),0),'99,99,99,99,999') AS revenue,
       TO_CHAR(COUNT(DISTINCT order_group_id),'99,99,99,99,999') AS orders,
       round(sum(coupon_discount)*100 /sum(item_mrp*item_quantity),2) as cd,
       round(sum(trade_discount)*100 /sum(item_mrp*item_quantity),2) as td,
       round(sum(item_revenue + nvl(cashback_redeemed,0) - nvl(tax_amount,0) - nvl(item_purchase_price_inc_tax,0) + nvl(vendor_funding,0) - nvl(royalty_commission,0))*100/sum(item_revenue + cashback_redeemed - tax_amount),2) as gm
FROM fact_order_live fci
WHERE (is_shipped = 1 OR is_realised = 1)
AND  coupon_code ilike ('%%HHSUN5RDG2%%') or  coupon_code ilike ('%%HHSUN6FRT3%%') or  coupon_code ilike ('%%HHSUN7HNK4%%')
or  coupon_code ilike ('%%HHSUN8WDS5%%') or  coupon_code ilike ('%%BLOCKBUSTER25%%')
AND   order_created_date =20170430
"""

sql_str3="""
SELECT 
       order_created_time / 100 AS hour,
       TO_CHAR(ROUND(SUM(item_revenue + nvl(cashback_redeemed,0)),0),'99,99,99,99,999') AS revenue,
       TO_CHAR(COUNT(DISTINCT order_group_id),'99,99,99,99,999') AS orders,
       round(sum(coupon_discount)*100 /sum(item_mrp*item_quantity),2) as cd,
       round(sum(trade_discount)*100 /sum(item_mrp*item_quantity),2) as td,
       round(sum(item_revenue + nvl(cashback_redeemed,0) - nvl(tax_amount,0) - nvl(item_purchase_price_inc_tax,0) + nvl(vendor_funding,0) - nvl(royalty_commission,0))*100/sum(item_revenue + cashback_redeemed - tax_amount),2) as gm
FROM fact_order_live fci
WHERE (is_shipped = 1 OR is_realised = 1)
AND   coupon_code LIKE 'HH0%%'
AND   order_created_date = 20170209
GROUP BY 1
ORDER BY 1
"""


report1=pd.read_sql_query(sql_str1,engine)
report2=pd.read_sql_query(sql_str2,engine)
#report3=pd.read_sql_query(sql_str3,engine)

report1.round(decimals=0)
report2.round(decimals=0)
#report3.round(decimals=0)

a1=report1.to_html(index=False)
a2=report2.to_html(index=False)
#a3=report3.to_html(index=False)
t1 ="Hi,\nPlease find below the overall performance and the hourly performance of the happy hour coupons.\nOverall peformance:"
t2 ="Hourly performance of Coupons :"
#t3 ="Hourly performance of COupon 2 (14%) :"

sender = 'pratik.mondkar@myntra.com'
receivers = ['naresh.krishnaswamy@myntra.com','anjali.bohara@myntra.com','vinayak.naik@myntra.com','anuj.sharma@myntra.com','bhavesh.singhal@myntra.com','apoorv.kalra@myntra.com','aashish.kumar@myntra.com','mohit.panjwani@myntra.com','pricing@myntra.com','neha.malhan@myntra.com']
#receivers = ['pratik.mondkar@myntra.com']

msg = MIMEMultipart()
msg['Subject'] = 'Happy Hour coupon hourly report'
msg['From'] =sender
msg['to'] =", ".join(receivers)


part1 = MIMEText(t1,'plain')
part2 = MIMEText(a1,'html')
part3 = MIMEText(t2,'plain')
part4 = MIMEText(a2,'html')
#part5 = MIMEText(t3,'plain')
#part6 = MIMEText(a3,'html')
msg.attach(part1)
msg.attach(part2)
msg.attach(part3)
msg.attach(part4)
#msg.attach(part5)
#msg.attach(part6)

try:
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login("pratik.mondkar@myntra.com", "kaxuftanzoyzmjfe")
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"
