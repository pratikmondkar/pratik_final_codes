import pandas as pd
import sqlalchemy as sq
import smtplib
from datetime import date, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

engine = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

sql_grn_packed_shipped_metrics="""
 with a as (Select
        umib.facility,
        umib.Item_Code,
        uso.sale_order_item_status,
        uso.order_date,
        uso.display_order_code,
        uso.Item_Code as item_code_uso,
        umg.grn_date,
        umib.last_modified_on,
        cast(to_date(fo.order_packed_date,
        'YYYYMMDD') || ' ' || (fo.order_packed_time/100) || ':' || MOD(CAST(fo.order_packed_time as INT),
        100) as timestamp) as pk_date,
        cast(to_date(fo.order_shipped_date,
        'YYYYMMDD') || ' ' || (fo.order_shipped_time/100) || ':' || MOD(CAST(fo.order_shipped_time as INT),
        100) as timestamp) as shipped_time ,
        umib.item_status            
    FROM
        unicommerce_myntra_item_barcodes      umib        
    FULL JOIN
        unicommerce_sale_orders      uso         
            ON umib.facility = uso.facility                                             
            AND umib.item_code = uso.Item_Code                                                         
    LEFT JOIN
        unicommerce_myntra_grn     umg           
            ON umg.facility = umib.facility                                             
            AND umg.grn_code = umib.grn_number                                            
            AND umg.item_skucode = umib.item_type_skucode                                                       
    left JOIN
        fact_orderitem fo                                             
            on uso.display_order_code=fo.order_id    
    LEFT JOIN
        fact_core_item fci     
            on uso.display_order_code = fci.order_id         
    left JOIN
        fact_order ford                       
            on ford.order_id = fo.order_id              
    left JOIN
        unicommerce_myntra_gatepass umgp                       
            on uso.Item_Code = umgp.itemcode             
    LEFT JOIN
        dim_courier dic                       
            on fo.idcourier = dic.id             
    LEFT JOIN
        dim_location dl                       
            on fo.idlocation = dl.id             
    WHERE
        TRUNC(umg.grn_date)>=DATEADD(hour,-480,GETDATE()) 
        and umib.facility in ('Bhiwandi Fulfillment Center','Bijwasan') 
        and uso.facility in ('Bhiwandi Fulfillment Center','Bijwasan') 
        and TRUNC(uso.order_date)>=DATEADD(hour,-480,GETDATE()))     


        Select
        facility,
        COUNT(DISTINCT case 
            when TRUNC(grn_date)=TRUNC(sysdate-1) then Item_Code 
        end) as grn_qty,
        COUNT(DISTINCT case 
            when TRUNC(pk_date)=TRUNC(sysdate-1) then Item_Code 
        end) as packed_qty,
        COUNT(DISTINCT case 
            when TRUNC(shipped_time)=TRUNC(sysdate-1) then Item_Code 
        end) as shipped_qty,
        count(distinct case 
            when  TRUNC(last_modified_on)=TRUNC(sysdate-1) 
            and item_status='LIQUIDATED' then Item_Code 
        end) as RTVed,
        AVG(CASE 
            when TRUNC(grn_date)=TRUNC(sysdate-1) then CONVERT(DECIMAL(8,
            2),
            DATEDIFF(hour,
            order_date,
            grn_date)) 
        end) as order2grn_time,
        AVG(CASE 
            when TRUNC(pk_date)=TRUNC(sysdate-1) then CONVERT(DECIMAL(8,
            2),
            DATEDIFF(hour,
            grn_date,
            pk_date)) 
        end) as grn2pack_time,
        AVG(CASE 
            WHEN TRUNC(shipped_time)=TRUNC(sysdate-1) then CONVERT(DECIMAL(8,
            2),
            DATEDIFF(hour,
            pk_date,
            shipped_time)) 
        end) as pack2ship_time,
        (Convert(decimal(8,
        2),
        count(distinct case 
            when TRUNC(grn_date)=TRUNC(sysdate-1) 
            and CONVERT(DECIMAL(8,
            2),
            DATEDIFF(hour,
            order_date,
            grn_date))<=24 then item_code 
        end))/convert(decimal(8,
        2),
        count(distinct case 
            when TRUNC(grn_date)=TRUNC(sysdate-1) then Item_Code 
        end)))*100 as perc_grn_in_24,
        (convert(decimal(8,
        2),
        count(distinct case 
            when TRUNC(pk_date)=TRUNC(sysdate-1) 
            and CONVERT(DECIMAL(8,
            2),
            DATEDIFF(hour,
            grn_date,
            pk_date))<=4 then item_code 
        end))/convert(decimal(8,
        2),
        count(distinct case 
            when TRUNC(pk_date)=TRUNC(sysdate-1) then Item_Code 
        end)))*100 as perc_pk_4hrs_grn,
        (convert(decimal(8,
        2),
        count(distinct case 
            when TRUNC(shipped_time)=TRUNC(sysdate-1) 
            and CONVERT(DECIMAL(8,
            2),
            DATEDIFF(hour,
            pk_date,
            shipped_time))<=4 then item_code 
        end))/convert(decimal(8,
        2),
        count(distinct case 
            when TRUNC(shipped_time)=TRUNC(sysdate-1) then Item_Code 
        end)))*100 as perc_shp_4hrs_pk
    from
        a
    group by 1
"""
sql_ordered_cancelled="""
Select (Case when warehouse_id=47 then 'Bhiwandi Fulfillment Center' else 'Bijwasan' end) as facility,
 sum(quantity) as ordered_qty, sum(case when item_cancelreason is not null then quantity end) as cancelled 
 from fact_core_item where order_created_date=TO_CHAR(CURRENT_DATE-1, 'YYYYMMDD') and warehouse_id in (20,47)
group by 1 
"""
sql_qc_rejections= """
Select facility, count(distinct Item_Code) as qc_rejections from 
(SELECT
        TRUNC(umg.grn_date),
        umib.item_code,
        umg.vendor_name,
        umib.brand,
        umib.item_type_skucode,
        umib.rejection_reason,
        umib.category,
        umib.facility, 
        gp.type,
        gp.status,
        umib.item_status,
        gp.gatePassCreatedBy,
        datediff(day,
        umg.grn_date,
        CURRENT_DATE) AS TAT,
        (datediff(day,
        umg.grn_date,
        CURRENT_DATE)*24) AS TAT_Hour_day,
        CASE         
            WHEN (datediff (day,
            umg.grn_date,
            CURRENT_DATE)) >= 0
            AND (datediff (day,
            umg.grn_date,
            CURRENT_DATE)) < 3 THEN '0 to 2 Days'         
            WHEN (datediff (day,
            umg.grn_date,
            CURRENT_DATE)) >= 3
            AND (datediff (day,
            umg.grn_date,
            CURRENT_DATE)) < 6 THEN '3 to 5 Days'         
            WHEN (datediff (day,
            umg.grn_date,
            CURRENT_DATE)) >= 6 THEN '6 and above'       
        END AS DAYS_BUCKET,
        CASE         
            WHEN uv.billing_address_state = 'Delhi'
            AND umib.facility = 'Bijwasan' THEN 'INTERSTATE'         
            WHEN uv.billing_address_state = 'Karnataka'
            AND umib.facility = 'Bangalore Fulfillment Center' THEN 'INTERSTATE'         
            WHEN uv.billing_address_state = 'Maharashtra'
            AND umib.facility = 'Bijwasan' THEN 'INTERSTATE'         
            WHEN uv.billing_address_state = 'West Bengal'
            AND umib.facility = 'Kolkata Fulfilment Center' THEN 'INTERSTATE'         
            WHEN uv.billing_address_state = 'Telangana'
            AND umib.facility = 'Hyderabad Fulfillment Center' THEN 'INTERSTATE'         
            ELSE 'OUTSTATION'       
        END AS INTERSTATE_OR_OUTSTATION
    FROM
        unicommerce_myntra_item_barcodes umib  
    JOIN
        unicommerce_myntra_grn umg    
            ON umg.facility = umib.facility   
            AND umg.grn_code = umib.grn_number   
            AND umg.item_skucode = umib.item_type_skucode  
    LEFT JOIN
        unicommerce_myntra_gatepass gp
            ON gp.itemcode = umib.item_code  
    LEFT JOIN
        unicommerce_vendor uv
            ON uv.vendor_code = umg.vendor_code
    WHERE
            umib.facility in ('Bhiwandi Fulfillment Center','Bijwasan')
        AND TRUNC(umg.grn_date)=TRUNC(sysdate-1) and grn_date is not null 
        AND   umib.rejection_reason IS NOT NULL
        AND   umib.rejection_reason IN (
            'Damaged','Stained','Broken Stitch','Fabric/Material Defect','Shade variation','Logo Missing','Uneven Length','Expired product','Wrong Pair','Used Product','Tag','Warranty','Part','Box Damage','Product Mismatch','Sticker Mismatch','Damaged (Cut/Torn/Hole)','Different sizes in one product','Missing part','Missing warranty card','Myntra sticker mismatch','Stain/dirty','Stitching','System (Image/Size /Price) mismatch','USED_PRODUCT - Used Product','Artical Mismatch','STAIN_DIRTY - Stain/dirty','MISSING_PART - Missing part','SHADE_VARIATION - Shade variation','Material Damage','MISSING_BRAND_TAG - Missing brand tag','DIFF_SIZES - Different sizes in one product','PTB_MISMATCH - Mismatch between product, tag,','STITCHING - Stitching','Mismatch between product, tag, box','SYSTEM_ISP_MISMATCH - System (Image/Size /Pri','DAMAGED_CTH - Damaged (Cut/Torn/Hole)','STICKER_MISMATCH - Myntra sticker mismatch','FM_DEFECT - Fabric/Material defect','PACKAGING_DAMAGE - Packaging damage','MISSING_WARRANTY_CARD - Missing warranty card'
        ) )
        group by 1
"""

sql_pendency_kpi="""
with a as (Select
        umib.facility,
        umib.Item_Code,
        uso.sale_order_item_status,
        uso.order_date,
        uso.display_order_code,
        uso.Item_Code as item_code_uso,
        umg.grn_date,
        fci.item_cancelreason,
        cast(to_date(fo.order_packed_date,
        'YYYYMMDD') || ' ' || (fo.order_packed_time/100) || ':' || MOD(CAST(fo.order_packed_time as INT),
        100) as timestamp) as pk_date,
        cast(to_date(fo.order_shipped_date,
        'YYYYMMDD') || ' ' || (fo.order_shipped_time/100) || ':' || MOD(CAST(fo.order_shipped_time as INT),
        100) as timestamp) as shipped_time ,
        umib.item_status,
        umg.status,   
        umgp.type as gp_type,
        umgp.createdat as gp_create_date,
        umgp.myntrastatusogp as gp_status,
        umgp.gatepasscode as gpcode,
        umib.last_modified_on,
        dic.courier_desc,
        dl.region_name             
    FROM
        unicommerce_myntra_item_barcodes      umib        
    FULL JOIN
        unicommerce_sale_orders      uso         
            ON umib.facility = uso.facility                                             
            AND umib.item_code = uso.Item_Code                                                         
    LEFT JOIN
        unicommerce_myntra_grn     umg           
            ON umg.facility = umib.facility                                             
            AND umg.grn_code = umib.grn_number                                            
            AND umg.item_skucode = umib.item_type_skucode                                                       
    left JOIN
        fact_orderitem fo                                             
            on uso.display_order_code=fo.order_id    
    LEFT JOIN
        fact_core_item fci     
            on uso.display_order_code = fci.order_id         
    left JOIN
        fact_order ford                       
            on ford.order_id = fo.order_id              
    left JOIN
        unicommerce_myntra_gatepass umgp                       
            on uso.Item_Code = umgp.itemcode             
    LEFT JOIN
        dim_courier dic                       
            on fo.idcourier = dic.id             
    LEFT JOIN
        dim_location dl                       
            on fo.idlocation = dl.id             
    WHERE
        TRUNC(umg.grn_date)>=DATEADD(hour,-720,GETDATE()) 
        and umib.facility in ('Bhiwandi Fulfillment Center','Bijwasan') 
        and uso.facility in ('Bhiwandi Fulfillment Center','Bijwasan') 
        and TRUNC(uso.order_date)>=DATEADD(hour,-720,GETDATE()))

    Select 
        facility,
        count(distinct case when grn_date<=DATEADD(hour,-24,GETDATE()) and grn_date>=DATEADD(hour,-180,GETDATE()) and sale_order_item_status<>'CANCELLED' and TRUNC(grn_date)<>'1970-01-01' and status='CREATED' then Item_Code end) as GRN_Pending,
        count(distinct case when grn_date<=DATEADD(hour,-24,GETDATE()) and TRUNC(grn_date)<>'1970-01-01' and sale_order_item_status<>'CANCELLED' and status='QC_PENDING' then Item_Code end) as Allocation_Pending,
        count(distinct case when grn_date<=DATEADD(hour,-24,GETDATE()) and TRUNC(grn_date)<>'1970-01-01' and sale_order_item_status<>'CANCELLED' and (pk_date is null or TRUNC(pk_date)='1970-01-01') and item_status='ALLOCATED'  then Item_Code end) as Packing_Pending,
        count(distinct case when grn_date<=DATEADD(hour,-24,GETDATE()) and (shipped_time is null or TRUNC(shipped_time)='1970-01-01') and item_status='ALLOCATED' and sale_order_item_status<>'CANCELLED'  then Item_Code end) as Shipping_Pending,
        count(distinct case when item_status in ('BAD_INVENTORY', 'RETURN_AWAITED') and last_modified_on<=DATEADD(hour,-72,GETDATE()) then item_code end) as rtv_pending_items_before_gp,
        count(distinct case when item_status = 'LIQUIDATED' and gp_status='At warehouse' and gp_create_date<=DATEADD(hour,-72,GETDATE()) then gpcode end) as rtv_gp_in_wh,
        count(distinct case when item_status = 'LIQUIDATED' and gp_status='In transit' and gp_create_date<=DATEADD(hour,-148,GETDATE()) then gpcode end) as rtv_gp_in_transit
    from a
    group by 1
"""

sql_compliance_brijwasan="""
with a as (Select
        umib.facility,
        umib.Item_Code,
        uso.sale_order_item_status,
        uso.order_date,
        uso.display_order_code,
        uso.Item_Code as item_code_uso,
        umg.grn_date,
        umg.last_modified_on,
        fci.item_cancelreason,
        cast(to_date(fo.order_packed_date,
        'YYYYMMDD') || ' ' || (fo.order_packed_time/100) || ':' || MOD(CAST(fo.order_packed_time as INT),
        100) as timestamp) as pk_date,
        cast(to_date(fo.order_shipped_date,
        'YYYYMMDD') || ' ' || (fo.order_shipped_time/100) || ':' || MOD(CAST(fo.order_shipped_time as INT),
        100) as timestamp) as shipped_time ,
        umib.item_status,
        dic.courier_desc,
        dl.region_name             
    FROM
        unicommerce_myntra_item_barcodes      umib        
    FULL JOIN
        unicommerce_sale_orders      uso         
            ON umib.facility = uso.facility                                             
            AND umib.item_code = uso.Item_Code                                                         
    LEFT JOIN
        unicommerce_myntra_grn     umg           
            ON umg.facility = umib.facility                                             
            AND umg.grn_code = umib.grn_number                                            
            AND umg.item_skucode = umib.item_type_skucode                                                       
    left JOIN
        fact_orderitem fo                                             
            on uso.display_order_code=fo.order_id    
    LEFT JOIN
        fact_core_item fci     
            on uso.display_order_code = fci.order_id         
    left JOIN
        fact_order ford                       
            on ford.order_id = fo.order_id              
    left JOIN
        unicommerce_myntra_gatepass umgp                       
            on uso.Item_Code = umgp.itemcode             
    LEFT JOIN
        dim_courier dic                       
            on fo.idcourier = dic.id             
    LEFT JOIN
        dim_location dl                       
            on fo.idlocation = dl.id             
    WHERE
        TRUNC(umg.grn_date)>=DATEADD(hour,-240,GETDATE()) 
        and umib.facility in ('Bijwasan') 
        and uso.facility in ('Bijwasan') 
        and TRUNC(uso.order_date)>=DATEADD(hour,-240,GETDATE()))   

Select  facility, (CONVERT(DECIMAL(8,2), count(distinct case when compliance='compliant' then item_code end))/CONVERT(DECIMAL(8, 2), count(distinct item_code)))*100 as cutoff_compliance
            FROM
                (Select
                    facility,
                    item_code,
                    TRUNC(order_date) as daten,
                    TRUNC(shipped_time) as dates,
                    (case              
                        when courier_desc='Myntra Logistics' 
                        and (region_name='North' 
                        or region_name='East/NorthEast') 
                        and        ((shipped_time>=dateadd(m,
                        60,
                        TRUNC(shipped_time)) 
                        and shipped_time<=dateadd(m,
                        1500,
                        TRUNC(shipped_time))) 
                        and grn_date<=dateadd(m,
                        1080,
                        TRUNC(shipped_time)) 
                        and grn_date>=dateadd(m,
                        -360,
                        TRUNC(shipped_time)))  then 'compliant'            
                        
                        when courier_desc='Myntra Logistics' 
                        and region_name='South' 
                        and         ((shipped_time>=dateadd(m,
                        -900,
                        TRUNC(shipped_time)) 
                        and shipped_time<=dateadd(m,
                        540,
                        TRUNC(shipped_time))) 
                        and grn_date<=dateadd(m,
                        120,
                        TRUNC(shipped_time)) 
                        and grn_date>=dateadd(m,
                        -1320,
                        TRUNC(shipped_time)))  then 'compliant'             
                        when courier_desc='Myntra Logistics' 
                        and region_name='West' 
                        and       (((shipped_time>=dateadd(m,
                        60,
                        TRUNC(shipped_time)) 
                        and shipped_time<=dateadd(m,
                        540,
                        TRUNC(shipped_time))) 
                        and grn_date<=dateadd(m,
                        180,
                        TRUNC(shipped_time)) 
                        and grn_date>=dateadd(m,
                        -360,
                        TRUNC(shipped_time))) 
                        OR        ((shipped_time>=dateadd(m,
                        540,
                        TRUNC(shipped_time)) 
                        and shipped_time<=dateadd(m,
                        1500,
                        TRUNC(shipped_time))) 
                        and grn_date<=dateadd(m,
                        1080,
                        TRUNC(shipped_time)) 
                        and grn_date>=dateadd(m,
                        180,
                        TRUNC(shipped_time)))) then 'compliant'            
                        when courier_desc='Ekart Logistics' 
                        and         (((shipped_time>=dateadd(m,
                        -360,
                        TRUNC(shipped_time)) 
                        and shipped_time<=dateadd(m,
                        300,
                        TRUNC(shipped_time))) 
                        and grn_date<=dateadd(m,
                        -120,
                        TRUNC(shipped_time)) 
                        and grn_date>=dateadd(m,
                        -600,
                        TRUNC(shipped_time))) 
                        OR        ((shipped_time>=dateadd(m,
                        300,
                        TRUNC(shipped_time)) 
                        and shipped_time<=dateadd(m,
                        780,
                        TRUNC(shipped_time))) 
                        and grn_date<=dateadd(m,
                        360,
                        TRUNC(shipped_time)) 
                        and grn_date>=dateadd(m,
                        -120,
                        TRUNC(shipped_time))) 
                        OR         ((shipped_time>=dateadd(m,
                        780,
                        TRUNC(shipped_time)) 
                        and shipped_time<=dateadd(m,
                        1260,
                        TRUNC(shipped_time))) 
                        and grn_date<=dateadd(m,
                        840,
                        TRUNC(shipped_time)) 
                        and grn_date>=dateadd(m,
                        360,
                        TRUNC(shipped_time)))) then 'compliant'              
                        when courier_desc='Blue Dart' 
                        and        (((shipped_time>=dateadd(m,
                        -240,
                        TRUNC(shipped_time)) 
                        and shipped_time<=dateadd(m,
                        780,
                        TRUNC(shipped_time))) 
                        and grn_date<=dateadd(m,
                        360,
                        TRUNC(shipped_time)) 
                        and grn_date>=dateadd(m,
                        -660,
                        TRUNC(shipped_time))) 
                        OR        ((shipped_time>=dateadd(m,
                        780,
                        TRUNC(shipped_time)) 
                        and shipped_time<=dateadd(m,
                        1200,
                        TRUNC(shipped_time))) 
                        and grn_date<=dateadd(m,
                        780,
                        TRUNC(shipped_time)) 
                        and grn_date>=dateadd(m,
                        1080,
                        TRUNC(shipped_time))) ) then 'compliant'              
                        when courier_desc='Delhivery' 
                        and            (((shipped_time>=dateadd(m,
                        660,
                        TRUNC(shipped_time)) 
                        and shipped_time<=dateadd(m,
                        1080,
                        TRUNC(shipped_time))) 
                        and grn_date<=dateadd(m,
                        660,
                        TRUNC(shipped_time)) 
                        and grn_date>=dateadd(m,
                        240,
                        TRUNC(shipped_time))) 
                        OR        ((shipped_time>=dateadd(m,
                        -360,
                        TRUNC(shipped_time)) 
                        and shipped_time<=dateadd(m,
                        660,
                        TRUNC(shipped_time))) 
                        and grn_date<=dateadd(m,
                        240,
                        TRUNC(shipped_time)) 
                        and grn_date>=dateadd(m,
                        -780,
                        TRUNC(shipped_time)))) then 'compliant'         
                        else 'non-compliant' 
                    end) as compliance      
                from
                    a   
                WHERE
                    sale_order_item_status not in (
                        'CANCELLED','UNFULFILLABLE'
                    ) and TRUNC(shipped_time)=TRUNC(sysdate-1) and shipped_time is not null 
                group by
                    1,
                    2,
                    3,
                    4,
                    5) 
            group by 1
"""

sql_compliance_bhiwandi="""
with a as (Select
        umib.facility,
        umib.Item_Code,
        uso.sale_order_item_status,
        uso.order_date,
        uso.display_order_code,
        uso.Item_Code as item_code_uso,
        umg.grn_date,
        umg.last_modified_on,
        fci.item_cancelreason,
        cast(to_date(fo.order_packed_date,
        'YYYYMMDD') || ' ' || (fo.order_packed_time/100) || ':' || MOD(CAST(fo.order_packed_time as INT),
        100) as timestamp) as pk_date,
        cast(to_date(fo.order_shipped_date,
        'YYYYMMDD') || ' ' || (fo.order_shipped_time/100) || ':' || MOD(CAST(fo.order_shipped_time as INT),
        100) as timestamp) as shipped_time ,
        umib.item_status,
        dic.courier_desc,
        dl.region_name             
    FROM
        unicommerce_myntra_item_barcodes      umib        
    FULL JOIN
        unicommerce_sale_orders      uso         
            ON umib.facility = uso.facility                                             
            AND umib.item_code = uso.Item_Code                                                         
    LEFT JOIN
        unicommerce_myntra_grn     umg           
            ON umg.facility = umib.facility                                             
            AND umg.grn_code = umib.grn_number                                            
            AND umg.item_skucode = umib.item_type_skucode                                                       
    left JOIN
        fact_orderitem fo                                             
            on uso.display_order_code=fo.order_id    
    LEFT JOIN
        fact_core_item fci     
            on uso.display_order_code = fci.order_id         
    left JOIN
        fact_order ford                       
            on ford.order_id = fo.order_id              
    left JOIN
        unicommerce_myntra_gatepass umgp                       
            on uso.Item_Code = umgp.itemcode             
    LEFT JOIN
        dim_courier dic                       
            on fo.idcourier = dic.id             
    LEFT JOIN
        dim_location dl                       
            on fo.idlocation = dl.id             
    WHERE
        TRUNC(umg.grn_date)>=DATEADD(hour,-240,GETDATE()) 
        and umib.facility in ('Bhiwandi Fulfillment Center') 
        and uso.facility in ('Bhiwandi Fulfillment Center') 
        and TRUNC(uso.order_date)>=DATEADD(hour,-240,GETDATE())
        and umg.grn_date is not null and uso.order_date is not null)   

Select  facility, (CONVERT(DECIMAL(8,2), count(distinct case when compliance='compliant' then item_code end))/CONVERT(DECIMAL(8, 2), count(distinct item_code)))*100 as cutoff_compliance
            FROM
                (Select
                    facility,
                    item_code,
                    TRUNC(order_date) as daten,
                    TRUNC(shipped_time) as dates,
                    (case 

                    when courier_desc='Myntra Logistics' and (region_name='North' or region_name='East/Northeast') and grn_date>=dateadd(m,-360,TRUNC(shipped_time)) and grn_date<=dateadd(m,1080,TRUNC(shipped_time)) and shipped_time>=dateadd(m,180,TRUNC(shipped_time)) and shipped_time<=dateadd(m,1500,TRUNC(shipped_time)) then 'compliant'

                    when courier_desc='Myntra Logistics' and (region_name='South' or region_name='West') and grn_date>=dateadd(m,-1200,TRUNC(shipped_time)) and grn_date<=dateadd(m,240,TRUNC(shipped_time)) and shipped_time>=dateadd(m,-720,TRUNC(shipped_time)) and shipped_time<=dateadd(m,660,TRUNC(shipped_time)) then 'compliant'

                    when courier_desc='Ekart Logistics' and 
                    ((grn_date>dateadd(m,-540,TRUNC(shipped_time)) and grn_date<=dateadd(m,360,TRUNC(shipped_time)) and shipped_time >= dateadd(m,-60,TRUNC(shipped_time)) and shipped_time <= dateadd(m,780,trunc(shipped_time))) or 
                    (grn_date>dateadd(m,360,TRUNC(shipped_time)) and grn_date<=dateadd(m,660,TRUNC(shipped_time)) and shipped_time >= dateadd(m,780,trunc(shipped_time)) and shipped_time <= dateadd(m,1080,trunc(shipped_time))) or 
                    (grn_date>dateadd(m,660,TRUNC(shipped_time)) and grn_date<=dateadd(m,900,TRUNC(shipped_time)) and shipped_time >= dateadd(m,1080,trunc(shipped_time)) and shipped_time <= dateadd(m,1320,TRUNC(shipped_time)))
                    ) then 'compliant'

                    when courier_desc='Delhivery' and 
                    ((grn_date>=dateadd(m,-780,trunc(shipped_time)) and grn_date<dateadd(m,240,trunc(shipped_time)) AND shipped_time>=dateadd(m,-300,TRUNC(shipped_time)) and shipped_time<=dateadd(m,660,trunc(shipped_time))) OR
                    (grn_date>=dateadd(m,240,trunc(shipped_time)) and grn_date<dateadd(m,660,trunc(shipped_time)) and shipped_time>=dateadd(m,660,trunc(shipped_time)) and shipped_time<=dateadd(m,1080,trunc(shipped_time))))  then 'compliant'

                    when courier_desc='Blue Dart' and
                    ((grn_date>=dateadd(m,-660,trunc(shipped_time)) and grn_date<dateadd(m,360,trunc(shipped_time)) AND shipped_time>=dateadd(m,-180,TRUNC(shipped_time)) and shipped_time<=dateadd(m,780,trunc(shipped_time))) OR
                    (grn_date>=dateadd(m,360,trunc(shipped_time)) and grn_date<dateadd(m,780,trunc(shipped_time))  AND shipped_time>=dateadd(m,-780,TRUNC(shipped_time)) and shipped_time<=dateadd(m,1200,trunc(shipped_time))))

                    then 'compliant' else 'non-compliant' end) as compliance

                from
                    a   
                WHERE
                    sale_order_item_status not in (
                        'CANCELLED','UNFULFILLABLE'
                    ) and TRUNC(shipped_time)=TRUNC(sysdate-1) and shipped_time is not null and grn_date is not null
                group by
                    1,
                    2,
                    3,
                    4,
                    5) 
            group by 1
"""

grn_packed_shipped_metrics = pd.read_sql_query(sql_grn_packed_shipped_metrics,engine)
ordered_cancelled = pd.read_sql_query(sql_ordered_cancelled,engine)
qc_rejections = pd.read_sql_query(sql_qc_rejections,engine)
pendency_kpi = pd.read_sql_query(sql_pendency_kpi,engine)
compliance_brijwasan = pd.read_sql_query(sql_compliance_brijwasan,engine)
compliance_bhiwandi = pd.read_sql_query(sql_compliance_bhiwandi,engine)

compliance=compliance_brijwasan.append(compliance_bhiwandi)
final=grn_packed_shipped_metrics.merge(ordered_cancelled,on='facility',how='left').merge(qc_rejections,on='facility',how='left').merge(pendency_kpi,on='facility',how='left').merge(compliance,on='facility',how='left')

sender = 'sudarson.tm@myntra.com'
receivers = ['aman.dhoot@myntra.com','pratik.mondkar@myntra.com']
abc = date.today().strftime('%d-%b-%Y')

msg = MIMEMultipart()
msg['Subject'] = 'Marketplace warehouse report '+ str(abc)
msg['From'] =sender
msg['to'] =", ".join(receivers)

t="Hi\n Please see below the report"
g=final.to_html(index=False)

part1 = MIMEText(t,'plain')
part2 = MIMEText(g,'html')
msg.attach(part1)
msg.attach(part2) 

try:
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login("sudarson.tm@myntra.com", "tdnhlfqsqyxgtjwh")
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"
