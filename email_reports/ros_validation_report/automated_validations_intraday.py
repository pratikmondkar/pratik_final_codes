
# coding: utf-8

# In[1]:

import pandas as pd
from pandas import DataFrame
import sqlalchemy as sq
import pickle
import numpy as np 
import scipy as scipy
from scipy import stats
from sklearn import linear_model, metrics, preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor
import xgboost as xgb
from xgboost import XGBRegressor
import multiprocessing
from datetime import date, timedelta
import gc
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

engine_tr = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")
engine_cs = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")


# In[58]:

ind_list=['wgt_input_td','input_td_diff','lc_share_pltf','live_styles','presale_flag','postsale_flag',
          'sessions','brokeness','freshness','output_cd','loyalty_points','vis_cannib_mean','comp_index','sales_lag',
          'index_week_year','price_cannib_mean','brokenness_rm','freshness_rm','atc_rm']


# In[56]:

sql_str1="""
select dp.brand,dp.article_type,dp.gender,
count(distinct case when is_live_on_portal=1 then o.style_id end) as live_styles,
count(distinct case when is_live_on_portal=1 and live_skus::float8/nullif(total_skus,0) <0.6 then o.style_id end) as broken_styles,
count(distinct case when is_live_on_portal=1 and datediff(d,to_date(style_catalogued_date,'YYYYMMDD'),sysdate) <=60 then o.style_id end) as fresh_styles
from 
(select style_id,max(is_live_on_portal) as is_live_on_portal,count(distinct atp.sku_id) as total_skus, count(distinct case when is_live_on_portal=1 then atp.sku_id end) as live_skus
from o_atp_inventory atp
join dim_product dp1 on atp.sku_id=dp1.sku_id
group by 1)o
join dim_style dp on o.style_id=dp.style_id
join dev.bag_list b on dp.brand=b.brand and dp.article_type=b.article_type and dp.gender=b.gender
group by 1,2,3
"""
sql_str2="""
select a.brand,a.article_type,a.gender,count(*) as lc 
from clickstream.widget_entity_date_plc we
join dim_style a on we.entity_id=a.style_id
group by 1,2,3
"""

sql_str3="""
select ([brand]+[article_type]+[gender]) as bag, brand, article_type,gender,business_unit 
from pricing_snapshot 
group by brand, article_type, gender, business_unit
"""

sql_str4="""
select u.*,w.wgt_inp_td_rm,w.inp_td_rm,day_of_the_week,iso_week_of_the_year,week_number,month from
(select to_char(sysdate,'YYYYMMDD') as date,brand,article_type,gender,
sum(ros_15days) as last_15_ros,
sum(ros_15days*nvl(discount_rule_percent,0))/nullif(sum(ros_15days),0) as wgt_input_td,
avg( nvl(discount_rule_percent,0)::float) as input_td 
from
(select style_id,brand,article_type,gender,max(nvl(discount_rule_percent,0)) as discount_rule_percent from pricing_snapshot group by 1,2,3,4) a
left join
(select style_id,avg(nvl(sold_quantity,0)::float) as ros_15days
from fact_category_over_view_metrics
where date>=to_char(sysdate - interval '15 days','YYYYMMDD')::bigint
group by 1) b on a.style_id=b.style_id
group by 1,2,3,4) u 
left join
(select brand,article_type,gender,wgt_inp_td_rm,inp_td_rm
from 
(select a.date,brand,article_type,gender,wgt_input_td,input_td,
avg(nvl(wgt_input_td,0)::float) over (partition by brand,article_type,gender order by a.date  rows between 15 preceding and 1 preceding) wgt_inp_td_rm,
avg(nvl(input_td,0)::float) over (partition by brand,article_type,gender order by a.date  rows between 15 preceding and 1 preceding) inp_td_rm
from
(select date,brand,article_type,gender,
sum(ros_15days) as last_15_ros,
sum(ros_15days*nvl(discount_rule_percentage,0))/nullif(sum(ros_15days),0) as wgt_input_td,
avg(case when is_live_style=1 then nvl(discount_rule_percentage,0) end) as input_td 
from
(select date,brand,article_type,product_gender as gender,style_id,sold_quantity,is_live_style,
discount_rule_percentage,
avg(nvl(sold_quantity,0)::float) over (partition by style_id order by date  rows between 15 preceding and 1 preceding) as ros_15days
from fact_category_over_view_metrics
where date>=to_char(sysdate - interval '30 days','YYYYMMDD')::bigint)
group by 1,2,3,4) a
where a.date>=to_char(sysdate - interval '15 days','YYYYMMDD')::bigint)
where date=to_char(sysdate - interval '1 day','YYYYMMDD')::bigint) w on u.brand=w.brand and u.article_Type=w.article_Type and u.gender=w.gender
left join dim_date dd on u.date=dd.full_date
join dev.bag_list v on u.brand=v.brand and u.article_type=v.article_type and u.gender=v.gender
"""

sql_str5="""
select sum(sessions) as sessions from customer_insights.daily_traffic 
where app_platform in ('Android','iOS') and load_date= to_char(sysdate- interval '1 day','YYYYMMDD')::bigint
"""
sql_str6="""
select ([brand]+[article_type]+[gender]) as bag,sum(item_revenue_inc_cashback)/sum(quantity) as isp
FROM fact_core_item
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date >=to_char(sysdate- interval '10 days','YYYYMMDD')::bigint
group by 1
"""
sql_str7="""
select brand,article_type,gender,
avg(atc_count) as atc_rm,
avg(fresh_styles::float/nullif(live_styles,0)) as freshness_rm,
avg(broken_styles::float/nullif(live_styles,0)) as brokenness_rm,
sum(case when date=to_char(sysdate - interval '1 day','YYYYMMDD')::bigint then qty_sold end) as sales_lag
from
(select a.date,a.brand,a.article_type,product_gender as gender,
sum(sold_quantity) as qty_sold,
sum(live_styles) as live_styles,
sum(case when is_live_style=1 and days_since_cataloguing<=60 then live_styles else 0 end) as fresh_styles,
sum(broken_styles) as broken_styles,
sum(atc_count) as atc_count,
sum(coupon_discount)/nullif(sum(total_mrp),0) as output_cd
from customer_insights.fact_category_over_view_metrics a
join dev.bag_list b on a.brand=b.brand and a.article_type=b.article_type and a.product_gender=b.gender
where date >= to_char(sysdate - interval '7 days','YYYYMMDD')::bigint
group by 1,2,3,4)
group by 1,2,3
"""
sql_str8="""
select dp.brand,dp.article_type,dp.gender,
sum(coupon_discount)/nullif(sum(item_quantity*item_mrp),0) as output_cd,
sum(loyalty_used::float/2)/nullif(sum(item_quantity*item_mrp),0) as loyalty_points
from fact_order_live fol
join dim_product dp on fol.sku_id=dp.sku_id
where (is_shipped=1 or is_realised=1) and order_created_date=to_char(sysdate,'YYYYMMDD')::bigint
group by 1,2,3
"""


# In[57]:

catalog=pd.read_sql_query(sql_str1,engine_tr)
vis=pd.read_sql_query(sql_str2.replace('date_plc',date.today().strftime('%Y_%m_%d')),engine_cs)
inp_disc=pd.read_sql_query(sql_str4,engine_tr)
bu_map=pd.read_sql_query(sql_str3,engine_tr)
bu_map.drop_duplicates('bag',inplace=True)
sess=pd.read_sql_query(sql_str5,engine_tr)
isp=pd.read_sql_query(sql_str6,engine_tr)
rm=pd.read_sql_query(sql_str7,engine_tr)
aux_disc=pd.read_sql_query(sql_str8,engine_tr)


# In[7]:

inp_disc.head()


# In[9]:

bag_list=pd.read_csv('/data/pratik/tensor/bag_list_2000.csv',error_bad_lines=False)['bag'].tolist()


# In[59]:

vis['lc_share_pltf']=vis['lc']*100/vis['lc'].sum()
raw_data=catalog.merge(vis,how='left',on=['brand','article_type','gender']).merge(inp_disc,how='left',on=['brand','article_type','gender']).merge(rm,how='left',on=['brand','article_type','gender']).merge(aux_disc,how='left',on=['brand','article_type','gender'])
raw_data.head()


# In[60]:

raw_data['bag']=raw_data['brand']+raw_data['article_type']+raw_data['gender']
data=raw_data[raw_data['bag'].isin(bag_list)]


# In[61]:

data.head()


# In[62]:

data['lc_share_pltf'].sum()


# In[14]:

month_index=pd.read_csv('/data/pratik/tensor/datasets/month_index.csv',error_bad_lines=False)
week_year_index=pd.read_csv('/data/pratik/tensor/datasets/week_year_index.csv',error_bad_lines=False)
week_month_index=pd.read_csv('/data/pratik/tensor/datasets/week_month_index.csv',error_bad_lines=False)
day_index=pd.read_csv('/data/pratik/tensor/datasets/day_index.csv',error_bad_lines=False)


# In[15]:

similar_brands=pd.read_csv('/data/pratik/tensor/datasets/similar_brands_collated.csv',error_bad_lines=False)
similar_brands.rename(columns={'rhs':'brand'},inplace=True)


# In[16]:

similar_final=similar_brands[similar_brands['lift']>1]
cannib=data[['date','brand','article_type','gender','wgt_input_td','lc_share_pltf']].merge(similar_final,how='left',on=['brand','article_type','gender'])
ratio=cannib.groupby(['date','lhs','article_type','gender']).agg({'wgt_input_td':{'price_mean':'mean','price_max':'max'},'lc_share_pltf':{'vis_mean':'mean','vis_max':'max'}}).reset_index(col_level=1)
ratio.rename(columns={'lhs':'brand'},inplace=True)
ratio.columns = ratio.columns.droplevel(0)


# In[63]:

ad_final=data.merge(month_index,how='left',on=['month','article_type','gender']).merge(week_year_index,how='left',on=['iso_week_of_the_year','article_type','gender']).merge(week_month_index,how='left',on=['week_number','article_type','gender']).merge(day_index,how='left',on=['day_of_the_week','article_type','gender']).merge(ratio,how='left',on=['brand','article_type','gender'])
ad_final['price_cannib_max']=ad_final['wgt_input_td']/ad_final['price_max']
ad_final['price_cannib_mean']=ad_final['wgt_input_td']/ad_final['price_mean']
ad_final['vis_cannib_max']=ad_final['lc_share_pltf']/ad_final['vis_max']
ad_final['vis_cannib_mean']=ad_final['lc_share_pltf']/ad_final['vis_mean']
ad_final.drop(['price_max','price_mean','vis_mean','vis_max'],axis=1,inplace=True)


# In[64]:

compete=pd.read_csv('/data/pratik/tensor/trends/google_trends.csv',error_bad_lines=False)
compete['comp_index']=compete['Myntra']/compete['Amazon']
compete['date']=compete['date'].str.replace('-','').astype(int)
compete.drop_duplicates('date',inplace=True)
ad_final['comp_index']=compete.comp_index.mean()


# In[65]:

ad_final.columns


# In[66]:

ad_final['brokeness']=ad_final['broken_styles']/ad_final['live_styles']
ad_final['freshness']=ad_final['fresh_styles']/ad_final['live_styles']
ad_final['presale_flag']=0
ad_final['postsale_flag']=0
ad_final['sessions']=sess.ix[0,0]
ad_final['input_td_diff']=ad_final['wgt_input_td']/ad_final['wgt_inp_td_rm']
lst=ad_final[['brand','article_type','gender']].to_records(index=False).tolist()


# In[16]:

ad_final.describe().T


# In[71]:

ad_final[['input_td_diff','price_cannib_max','price_cannib_mean']]=ad_final[['input_td_diff','price_cannib_max','price_cannib_mean']].fillna(1)
ad_final.replace([np.inf, -np.inf], np.nan,inplace=True)
ad_final[['input_td_diff','price_cannib_max','price_cannib_mean']]=ad_final[['input_td_diff','price_cannib_mean','price_cannib_mean']].fillna(10)
ad_final.fillna(0,inplace=True)


# In[74]:

ad_final.loc[ad_final['input_td_diff']>10,'input_td_diff']=10
ad_final.loc[ad_final['price_cannib_max']>10,'price_cannib_max']=10
ad_final.loc[ad_final['price_cannib_mean']>10,'price_cannib_mean']=10


# In[75]:

ad_final.describe().T


# In[76]:

def gen_model_error(lst):
    b=lst[0]
    a=lst[1]
    g=lst[2]
    path_o=r'/data/pratik/tensor/models/'
    filename=b+'-'+a+'-'+g
    bag=b+a+g 
    with open(path_o+filename+'.pkl', 'rb') as f:
        model_o = pickle.load(f)
    dat=ad_final[ad_final['bag']==bag]
    dat1=dat[ind_list]
    dat['ros']=model_o.predict(dat1)
    dat['bag']=bag
    print "Intraday Prediction for "+bag+" done." 
    return dat


# In[ ]:

p = multiprocessing.Pool(10)
ros_pred=p.map(gen_model_error, lst)
p.close()
predictions = pd.concat(ros_pred)


# In[78]:

predictions.head()


# In[83]:

#predictions['ros']=predictions['ros'].round().astype(int)
model_error=predictions[['bag','ros']].merge(bu_map,how='left',on='bag').merge(isp,how='left',on='bag')
model_error['isp'].fillna(950,inplace=True)
model_error['revenue']=model_error['ros'].round().astype(int)*model_error['isp']
model_error['revenue']=model_error['revenue'].round().astype(int)
model_error['isp']=model_error['isp'].round().astype(int)
model_error['ros']=model_error['ros'].round().astype(int)


# In[85]:

bu_summary=model_error.groupby('business_unit')[['ros','revenue']].sum().reset_index()
bu_summary['isp']=(bu_summary['revenue']/bu_summary['ros']).round().astype(int)
bu_summary['revenue'] = bu_summary.apply(lambda x: "{:,}".format(x['revenue']), axis=1)
bu_summary['ros'] = bu_summary.apply(lambda x: "{:,}".format(x['ros']), axis=1)
bu_summary


# In[87]:

brand_summary=model_error.groupby('brand')[['ros','revenue']].sum().reset_index()
brand_summary['isp']=(brand_summary['revenue']/brand_summary['ros']).fillna(0).round().astype(int)
brand_summary['revenue'] = brand_summary.apply(lambda x: "{:,}".format(x['revenue']), axis=1)
top_25Brands=brand_summary.sort_values(by='ros',ascending=False).head(n=25)
top_25Brands


# In[88]:

a1=bu_summary.to_html(index=False)
a2=top_25Brands.to_html(index=False)

t1 ="Hi,\nPlease find below the projected performance for the whole day .\n \nBU level summary:\n"
t2 ="Top 25 brands summary:\n"


# In[89]:

outpath = '/home/admin/email_reports/data/ros_validation_report/'
abc = date.today().strftime('%d-%b-%Y')
filename2=outpath+ "bag_projections_"+str(abc)+".csv"
model_error.to_csv(filename2,index=False)

sender = 'pratik.mondkar@myntra.com'
receivers = ['shrinivas.ron@myntra.com','pratik.mondkar@myntra.com','sudarson.tm@myntra.com']
#receivers = ['pratik.mondkar@myntra.com','sankalp.kumar@myntra.com','kaivalya.kumar@myntra.com','shrinivas.ron@myntra.com']

msg = MIMEMultipart()
msg['Subject'] = 'BAG level model intraday projection report'
msg['From'] =sender
msg['to'] =", ".join(receivers)

part1 = MIMEText(t1,'plain')
part2 = MIMEText(a1,'html')
part3 = MIMEText(t2,'plain')
part4 = MIMEText(a2,'html')

msg.attach(part1)
msg.attach(part2)
msg.attach(part3)
msg.attach(part4)

f2 = file(filename2)
attachment2 = MIMEText(f2.read())
attachment2.add_header('Content-Disposition', 'attachment', filename=filename2)
msg.attach(attachment2)


try:
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login("pratik.mondkar@myntra.com", "kaxuftanzoyzmjfe")
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"

