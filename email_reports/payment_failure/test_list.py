import pandas as pd
import numpy as np
import datetime
from datetime import date, timedelta
from pandas import DataFrame
import sqlalchemy as sq
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
engine = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

sql_str1="""
select distinct phone from 
(select distinct phone, rnk, CASE
         WHEN rnk <= tenth THEN 'Control'
         ELSE 'Test'
       END AS bucket
FROM 

(SELECT distinct phone as phone, rnk as rnk, 
             percentile_disc(0.50) within GROUP (ORDER BY rnk) OVER () AS tenth
      FROM 
(select distinct dp.phone as phone, ROW_NUMBER() OVER (ORDER BY RANDOM()) AS rnk
from 
dim_customer_idea dci 
JOIN dim_customer_phone dp 
on dci.id = dp.uid 
where dci.customer_login in (SELECT
        a.customer_login
    from
        ( (   SELECT
            foci.store_order_id,
            foci.customer_login,
            sum(foci.item_revenue) as order_revenue,
            count(foci.item_id) as items,
            foci.order_created_date,
            foci.order_created_time,
            foci.payment_method,
            fp.payment_option,
            max((to_date(foci.order_created_date,
            'YYYYMMDD') || ' ' || 
        left(dt.time_value,
        2) || ':' || right(left(dt.time_value||':00',
        5),
        2) ||':00')::timestamp) as order_created_datetime                 from
            fact_order_live foci                  
        INNER JOIN
            payments_log_rs fp                          
                on (                             foci.store_order_id=fp.order_id                         )                  
        INNER JOIN
            dim_time dt 
                on foci.order_created_time = dt.time    
        where
            foci.order_created_date = to_char(CURRENT_DATE,'YYYYMMDD')::bigint  and foci.browser_info='Android'                  
            and fp.is_complete=0 
            and fp.payment_gateway_name not ilike '%%test%%'                 
        group by
            1,
            2,
            5,
            6,
            7,
            8 )a     
    left JOIN
        (             SELECT
            foci.store_order_id,
            foci.customer_login,
            sum(foci.item_revenue),
            foci.order_created_date,
            foci.order_created_time,
            foci.payment_method,
            fp.payment_option             
        from
            fact_order_live foci              
        left JOIN
            payments_log_rs fp                      
                on (                         foci.store_order_id=fp.order_id                     )              
        where
            foci.order_created_date=to_char(CURRENT_DATE,'YYYYMMDD')::bigint  and foci.browser_info='Android'               
            and (                     foci.is_shipped=1                      
            or foci.is_realised=1                 )              
        group by
            1,
            2,
            4,
            5,
            6,
            7        ) b              
            on a.customer_login = b.customer_login              
            and a.order_created_date =b.order_created_date         )  
    where
        b.customer_login is null 
						and	 to_char(a.order_created_datetime,'HH24')::bigint     >=to_char(dateadd(m,-60,convert_timezone('Asia/Calcutta',sysdate)),'HH24')::bigint 
  )) as x) as y) as god
  where bucket = 'Test'
"""



outpath = r'/home/admin/email_reports/data/payment_failure/'
abc = date.today().strftime('%d-%b-%Y')
filename1=outpath+ "test_group_"+str(abc)+".csv"

test=pd.read_sql_query(sql_str1,engine)
test.to_csv(filename1,index=False)

sender = 'pratik.mondkar@myntra.com'
receivers = ['dhritiman.mitra@myntra.com','suresh.a@myntra.com']
#receivers=['pratik.mondkar@myntra.com']

msg = MIMEMultipart()
msg['Subject'] = 'Payment Failure test group '+str(abc)
msg['From'] =sender
msg['to'] =", ".join(receivers)

part = MIMEText("Hi, Please find attached the test group")
msg.attach(part)

f1 = file(filename1)
attachment1 = MIMEText(f1.read())
attachment1.add_header('Content-Disposition', 'attachment', filename=filename1)
msg.attach(attachment1)


try:
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login("pratik.mondkar@myntra.com", "kaxuftanzoyzmjfe")
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"
