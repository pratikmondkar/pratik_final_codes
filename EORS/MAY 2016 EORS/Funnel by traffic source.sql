SELECT s.load_date,
			 s.utm_medium,
       s.utm_campaign,
       s.utm_source,
       COUNT(DISTINCT s.session_id) AS sessions,
       COUNT(DISTINCT s.device_id) AS users,
       SUM(list) AS list_views,
       count(distinct case when list>1 then s.device_id end) as list_devices,
       SUM(pdp) AS pdp_views,
       count(distinct case when pdp>1 then s.device_id end) as pdp_devices,
       SUM(atc) AS add_to_carts,
       count(distinct case when atc>1 then s.device_id end) as atc_devices
FROM (SELECT session_id,
             device_id,
             utm_medium,
             utm_campaign,
             utm_source,
             load_date
      FROM (SELECT session_id,
                   device_id,
                   utm_campaign,
                   utm_medium,
                   utm_source,
                   load_date,
                   ROW_NUMBER () OVER (PARTITION BY session_id ORDER BY atlas_ts) AS rnk
            FROM clickstream.events_view
            WHERE load_date between 20160401 and 20160430
            AND   event_type IN ('appLaunch')) a
      WHERE rnk = 1) s
  LEFT JOIN (SELECT session_id,
                    COUNT(CASE WHEN landing_screen LIKE 'Shopping Page-List Page%' THEN 1 END) AS list,
                    COUNT(CASE WHEN landing_screen LIKE 'Shopping Page-PDP%' THEN 1 END) AS pdp,
                    COUNT(CASE WHEN event_type IN ('addToCart') THEN 1 END) AS atc,
                    MAX(device_id) AS device_id
             FROM clickstream.events_view
             WHERE load_date between 20160401 and 20160430
             AND   event_type IN ('ScreenLoad','addToCart')
             GROUP BY 1) f ON s.session_id = f.session_id
GROUP BY 1,2,3,4
