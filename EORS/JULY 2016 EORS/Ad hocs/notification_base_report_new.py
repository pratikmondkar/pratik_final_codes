import pandas as pd
import numpy as np
import datetime
from datetime import date, timedelta
from pandas import DataFrame
import sqlalchemy as sq
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
engine = sq.create_engine("postgresql+psycopg2://analysis_user:AdhoCus@123!@dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

sql_str="""
SELECT a.referrer_url,
        a.utm_campaign,
       COUNT(DISTINCT CASE WHEN received > 0 THEN uidx END) AS received,
       COUNT(DISTINCT CASE WHEN dismissed > 0 THEN uidx END) AS dismissed,
       COUNT(DISTINCT CASE WHEN opened > 0 THEN uidx END) AS opened,
       COUNT(DISTINCT CASE WHEN dt > notif_time THEN order_group_id END) AS orders
FROM (SELECT uidx,
             referrer_url,
             utm_campaign,
             COUNT(CASE WHEN event_type = 'push-notification-received' THEN 1 END) AS received,
             COUNT(CASE WHEN event_type = 'push-notification-dismissed' THEN 1 END) AS dismissed,
             COUNT(CASE WHEN event_type = 'push-notification' THEN 1 END) AS opened
      FROM clickstream.events_view
      WHERE event_type IN ('push-notification','push-notification-dismissed','push-notification-received')
      AND   load_date IN (20160702,20160703)
      AND   atlas_ts > 1467424800000
      AND   os = 'Android'
      GROUP BY 1,
               2,
               3) a
  LEFT JOIN (SELECT customer_login,
                    order_group_id,
                    dt
             FROM (SELECT DISTINCT customer_login,
                          order_group_id::BIGINT,
                          CAST(order_created_date || ' ' || LPAD(order_created_time,4,0) AS TIMESTAMP) AS dt
                   FROM bidb.fact_order_live fol
                   WHERE (is_shipped = 1 OR is_realised = 1)
                   UNION
                   SELECT DISTINCT customer_login,
                          order_group_id::BIGINT,
                          CAST(order_created_date || ' ' || LPAD(order_created_time,6,0) AS TIMESTAMP) AS dt
                   FROM bidb.fact_core_item fci
                     JOIN bidb.dim_customer_idea dci ON fci.idcustomer = dci.id
                   WHERE store_id = 1
                   AND   (is_shipped = 1 OR is_realised = 1)
                   AND   order_created_date = 20160702)) b ON a.uidx = b.customer_login
  LEFT JOIN (SELECT referrer_url,
                    MIN((TIMESTAMP 'epoch' + atlas_ts / 1000*INTERVAL '1 Second ')) AS notif_time
             FROM clickstream.events_view
             WHERE event_type IN ('push-notification-received')
             AND   load_date >= 20160702
             AND   atlas_ts > 1467424800000
             AND   os = 'Android'
             GROUP BY 1) c ON a.referrer_url = c.referrer_url
GROUP BY 1,2
"""

notif=pd.read_sql_query(sql_str,engine)

searchfor=['0930AM_3JULY2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_300Store',
'1230PM_3JULY2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_B1G5',
'0730PM_2JULY2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_400Store',
'0930PM_2JULY2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_AllBrandsDisc',
'02PM_3JULY2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_Min3000',
'04PM_3JULY2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_Extra50',
'06PM_3JULY2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_HeroBrands',
'08PM_3JULY2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_EverythingDisc',
'10PM_3JULY2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_ThanksFolks']

final=notif[notif['referrer_url'].str.contains('|'.join(searchfor),na=False)]
final.drop('referrer_url',axis=1,inplace=True)
fn=final[final['received']>100]
fn.sort(['received'], ascending=[0], inplace=[1])

sender = 'pratik.mondkar@myntra.com'
receivers = ['harini.r@myntra.com','gaurav.prateek@myntra.com','ankul.batra@myntra.com','saurabh.singh3@myntra.com','shrinivas.ron@myntra.com','anjali.bohara@myntra.com']

msg = MIMEMultipart('alternative')
msg['Subject'] = 'Notification performance report'
msg['From'] =sender
msg['to'] =", ".join(receivers)

b="Hi please find the attached the report. \n"
t=fn.to_html(index=False)
part1 = MIMEText(b,'plain')
part2 = MIMEText(t,'html')

msg.attach(part1)
msg.attach(part2)


try:
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login("pratik.mondkar@myntra.com", "kaxuftanzoyzmjfe")
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"
