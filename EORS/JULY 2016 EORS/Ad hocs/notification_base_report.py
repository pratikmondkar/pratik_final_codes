import pandas as pd
import numpy as np
import datetime
from datetime import date, timedelta
from pandas import DataFrame
import sqlalchemy as sq
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
engine = sq.create_engine("postgresql+psycopg2://analysis_user:AdhoCus@123!@dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

sql_str="""
select a.*,nvl(b.orders,0) as orders from
(select uidx,
count( case when event_type='push-notification-received' then 1 end) as received,
count( case when event_type='push-notification-dismissed' then 1 end) as dismissed,
count( case when event_type='push-notification' then 1 end) as opened
FROM clickstream.events_view
WHERE event_type IN ('push-notification', 'push-notification-dismissed', 'push-notification-received') and load_date=20160702
and atlas_ts>1467441000000 and os='Android'
group by 1) a
left join
(select customer_login,count(distinct order_group_id) as orders
from bidb.fact_order_live
where (is_shipped=1 or is_realised=1) and order_created_time>1200
group by 1) b
on a.uidx=b.customer_login
"""

notif=pd.read_sql_query(sql_str,engine)
base=pd.read_csv('/home/pratik/data_files/reporting_base.csv',error_bad_lines=False)

final=base.merge(notif,how='left',on='uidx')
final.fillna(0,inplace=True)

agg_final = final.groupby(['base'],as_index=False).agg({'received' : np.sum,
                                                        'dismissed' : np.sum,
                                                        'opened' : np.sum,
                                                        'orders' : np.sum})


sender = 'pratik.mondkar@myntra.com'
receivers = ['harini.r@myntra.com','gaurav.prateek@myntra.com','ankul.batra@myntra.com','saurabh.singh3@myntra.com','shrinivas.ron@myntra.com','anjali.bohara@myntra.com']

msg = MIMEMultipart('alternative')
msg['Subject'] = 'Notification performance report'
msg['From'] =sender
msg['to'] =", ".join(receivers)

b="Hi please find the attached the report. \n"
t=agg_final.to_html(index=False)
part1 = MIMEText(b,'plain')
part2 = MIMEText(t,'html')

msg.attach(part1)
msg.attach(part2)


try:
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login("pratik.mondkar@myntra.com", "kaxuftanzoyzmjfe")
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"
