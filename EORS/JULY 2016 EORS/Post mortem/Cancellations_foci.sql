SELECT 			 booked_item_count,
       SUM(revenue) AS booked_revenue,
       sum(case when (is_shipped=0 and is_realised=0) then revenue else 0 end) as cancelled_revenue
FROM (SELECT order_created_date,
             (CASE WHEN (order_status IN ('C','SH','DL','Q','WP','OH','PK')) OR (order_status IN ('F') AND item_cancellation_reason_id IS NOT NULL) THEN 1 ELSE 0 END) AS is_booked,
             CASE
               WHEN nvl (booked_item_count,0) > 10 THEN '10+'
               ELSE nvl (booked_item_count,0)::VARCHAR
             END AS booked_item_count,
             fci.order_group_id,
             quantity,
             is_shipped,
             is_realised,
             nvl(item_revenue_inc_cashback,0) + nvl(gift_charges,0) + nvl(shipping_charges,0) AS revenue
      FROM fact_core_item fci
        LEFT JOIN (SELECT order_group_id,
                          SUM(quantity) AS booked_item_count
                   FROM fact_core_item
                   WHERE store_id = 1
                   AND   (order_status IN ('C','SH','DL','Q','WP','OH','PK')) OR (order_status IN ('F') AND item_cancellation_reason_id IS NOT NULL)
                   AND   ((order_created_date = 20160701 AND order_created_time > 2345) OR order_created_date IN (20160702,20160703))
                   GROUP BY 1) o ON fci.order_group_id = o.order_group_id
      WHERE store_id = 1
      AND   ((order_created_date = 20160701 AND order_created_time > 234500) OR order_created_date IN (20160702,20160703)))
WHERE is_booked = 1
GROUP BY 1
LIMIT 1000
