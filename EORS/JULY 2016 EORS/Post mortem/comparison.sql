select order_created_date,
--case when order_created_date BETWEEN 20170624 and 20170626 then 'EORS6' when order_created_date BETWEEN 20170103 and 20170105 then 'EORS5' end as eors,
case when device_channel = 'mobile-app' then os_info else device_channel end as channel,
count(distinct idcustomer) as customers,
count(distinct order_group_id) as orders  
FROM fact_core_item
WHERE store_id = 1
AND   is_booked=1
AND   (order_created_date BETWEEN 20170624 and 20170626  or order_created_date BETWEEN 20170103 and 20170105)
group by 1,2;


select case when load_date BETWEEN 20170624 and 20170626 then 'EORS6' 
when load_date BETWEEN 20170103 and 20170105 then 'EORS5' end as eors,load_date,
os,count(distinct device_id) as devices,
count(distinct uidx) as logged_in_users,
sum(sessions) 
from clickstream.daily_aggregates
where (load_date BETWEEN 20170624 and 20170626  or load_date BETWEEN 20170103 and 20170105)
group by 1,2,3;

select * from stv_inflight;
