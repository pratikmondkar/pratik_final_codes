SELECT 
			 order_created_date,
			 HOUR,
			 article_type,
			 device_channel,
--			 booked_item_count,
--			 booked_order_count,
       SUM(revenue) AS booked_revenue,
       SUM(quantity) AS booked_quantity,
       sum(case when (is_shipped=0 and is_realised=0) then revenue else 0 end) as cancelled_revenue,
       sum(case when (is_shipped=0 and is_realised=0) then quantity else 0 end) as cancelled_quantity
FROM (SELECT order_created_date,order_created_time/10000 AS HOUR,
             (CASE WHEN (order_status IN ('C','SH','DL','Q','WP','OH','PK')) OR (order_status IN ('F') AND item_cancellation_reason_id IS NOT NULL) THEN 1 ELSE 0 END) AS is_booked,
             article_type,
             case when nvl(booked_item_count,0) > 10 then '10+' else nvl(booked_item_count,0)::varchar end as booked_item_count,
             case when nvl(booked_order_count,0) > 6 then '6+' else nvl(booked_order_count,0)::varchar end as booked_order_count,
             device_channel,
             quantity,
             is_shipped,
             is_realised,
             nvl(item_revenue_inc_cashback,0) + nvl(gift_charges,0) + nvl(shipping_charges,0) AS revenue
      FROM fact_core_item fci
      left join (select order_group_id,sum(booked_item_count) as booked_item_count from fact_order where store_id=1 and is_booked=1 
      						and ((order_created_date = 20160701 AND order_created_time > 2345) OR order_created_date IN (20160702,20160703)) group by 1) o
      on fci.order_group_id=o.order_group_id
      left join (select idcustomer_idea,count(distinct order_group_id) as booked_order_count from fact_order where store_id=1 and is_booked=1 
      						and ((order_created_date = 20160701 AND order_created_time > 2345) OR order_created_date IN (20160702,20160703)) group by 1) c
      on fci.idcustomer=c.idcustomer_idea
      WHERE store_id = 1
      AND   ((order_created_date = 20160701 AND order_created_time > 234500) OR order_created_date IN (20160702,20160703)))
where is_booked=1
GROUP BY 1,2,3,4
