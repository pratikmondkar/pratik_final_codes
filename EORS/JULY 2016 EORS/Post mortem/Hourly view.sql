select 
to_char(load_datetime,'YYYYMMDD-HH24') as date_hour,os,
count(distinct session_id) as sessions,
count(distinct device_id) as users
from clickstream.traffic_realtime
where load_date between 20170102 and 20170105 or load_date between 20170623 and 20170626
group by 1,2
order by 1,2
limit 1000;
