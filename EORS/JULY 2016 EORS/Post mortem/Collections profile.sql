SELECT c.uidx,
       datediff(d,TO_DATE(first_login_date,'YYYYMMDD'),'2016-07-01 00:00:00') AS tenure_days,
       gender,
       avg(CASE WHEN collection_flag = 1 then disc end) as atc_disc,
       avg(CASE WHEN purchase_flag = 1 then disc end) as pur_disc,
       avg(CASE WHEN collection_flag = 1 and purchase_flag = 1 then disc end) as atc_pur_disc,      
       count(DISTINCT CASE WHEN collection_flag = 1 THEN c.style_id END) as c_styles_added,
       count(DISTINCT CASE WHEN purchase_flag = 1 THEN c.style_id END) as styles_purchased,       
       COUNT(DISTINCT CASE WHEN collection_flag = 1 AND purchase_flag = 1 THEN c.style_id END) AS c_styles_purchased,
       COUNT(DISTINCT CASE WHEN collection_flag = 0 AND purchase_flag = 1 THEN c.style_id END) AS nc_styles_purchased
FROM (SELECT nvl(a.uidx,b.customer_login) AS uidx,
             nvl(a.style_id,b.style_id) AS style_id,
             nvl(a.disc,b.disc) as disc,
             nvl(collection_flag,0) AS collection_flag,
             nvl(purchase_flag,0) AS purchase_flag
      FROM (SELECT DISTINCT uidx,
                   style_id::BIGINT,
                   nvl(discount_percentage,0) as disc,
                   1 AS collection_flag
            FROM bidb.collection_log
            WHERE action = 1) a
        FULL OUTER JOIN (SELECT DISTINCT customer_login,
                                style_id::BIGINT,
                                nvl(effective_discount_percent,0) as disc,
                                1 AS purchase_flag
                         FROM bidb.fact_core_item fci
                           JOIN bidb.dim_customer_idea dci ON fci.idcustomer = dci.id
                         WHERE ((order_created_date = 20160701 AND order_created_time > 234500) OR order_created_date IN (20160702,20160703))
                         AND   store_id = 1
                         AND   (is_shipped = 1 OR is_realised = 1)) b
                     ON a.uidx = b.customer_login
                    AND a.style_id = b.style_id) c
JOIN (SELECT DISTINCT uidx FROM bidb.collection_log WHERE action = 1) e
ON c.uidx=e.uidx
LEFT JOIN bidb.dim_customer_idea dci ON e.uidx=dci.customer_login
GROUP BY 1,2,3
LIMIT 100

