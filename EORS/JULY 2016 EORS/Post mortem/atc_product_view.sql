SELECT brand,
       brand_type,
       current_commercial_type,
       article_type,
       gender,
       season_code,
       subcategory,
       master_category,
       business_unit,
       nvl(discount_percentage,0) AS disc_perc, 
       sum(DISTINCT uidx) AS users,
       COUNT(cl.style_id) AS styles
FROM (SELECT style_id::BIGINT,
             COUNT(DISTINCT uidx) AS users_atc
      FROM bidb.collection_log
      WHERE action = 1
      and added_date = 20160701
      GROUP BY 1) a
  LEFT JOIN (SELECT entity_id AS style_id,
                    COUNT(1) AS list_page_count
             FROM clickstream.widget_entity_view
             WHERE event_type = 'Product list loaded'
             AND   app_version IN (SELECT app_version
                                   FROM clickstream.dim_app_version
                                   WHERE app_version >= '3.3.0')
             AND   load_Date = 20160701
             GROUP BY 1) b ON a.style_id = b.style_id) cl
  JOIN dim_style ds ON cl.style_id = ds.style_id
group by 1,2,3,4,5,6,7,8,9,10
limit 100
