SELECT DISTINCT order_group_id,
       article_type
FROM bidb.fact_core_item
WHERE (is_shipped = 1 OR is_realised = 1)
AND   store_id = 1
AND   order_created_date BETWEEN 20150701 AND 20160627

