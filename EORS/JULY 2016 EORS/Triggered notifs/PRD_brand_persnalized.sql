SELECT customer_id,
       name,
       brand
FROM (SELECT customer_id,
             name,
             brand,
             ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY brand_rank DESC) AS Brand_final_rank
      FROM (SELECT customer_id,
                   name,
                   brand,
                   ((viewed*1) +(liked*5) +(added_to_collection*10)) AS brand_rank
            FROM (SELECT l.uidx AS customer_id,
                         s.brand AS brand,
                         COUNT(CASE WHEN event_type = 'ScreenLoad' AND data_set_type = 'product' THEN 1 END) AS viewed,
                         COUNT(CASE WHEN l.event_type = 'pdpLike' THEN 1 END) AS liked,
                         COUNT(CASE WHEN l.event_type = 'AddToCollection' THEN 1 END) AS added_to_collection
                  FROM clickstream.events_view AS l
                    JOIN bidb.dim_style AS s
                      ON l.data_set_value = s.style_id
                     AND (l.load_date BETWEEN TO_CHAR (SYSDATE-INTERVAL '15 days','YYYYMMDD')::BIGINT
                     AND TO_CHAR (SYSDATE-INTERVAL '1 days','YYYYMMDD')::BIGINT)
                  WHERE event_type IN ('ScreenLoad','pdpLike','AddToCollection')
                  AND   s.brand IN ('Roadster','Monteil & Munero','American Crew','HIGHLANDER','LOCOMOTIVE','WROGN','Moda Rapido','Harvard','ether','Numero Uno','Puma','Reebok','FILA','ASICS','Adidas','Adidas Originals','Adidas NEO','Nike','Skybags','Supra','Nike','Puma','Vans','Mast & Harbour','Kook N Keech','20Dresses','United Colors of Benetton','Celio','Indian Terrain','Pepe Jeans','Wrangler','Jack & Jones','Mufti','Being Human','Lee','Flying Machine','Killer','Antony Morato','mothercare','FOREVER 21','MANGO','ONLY','Qupid','Vero Moda','all about you','Miss Chase','PrettySecrets','DressBerry','Marks & Spencer','Belle Fille','SASSAFRAS','Tokyo Talkies','    DressBerry','Shoetopia','Lavie','Eavan','RARE','Cation','FOREVER 21','Cottinfab','Moda Rapido','Tulsattva','Alom','Aum','Nayo','AKS','Vishudh','Aujjessa','Jaipur Kurti','Libas','La Firangi','HRX by Hrithik Roshan','Adidas','WROGN','INVICTUS','French Connection','John Players','U.S. Polo Assn.','United Colors of Benetton','HRX by Hrithik Roshan','Anouk','IMARA','BIBA OUTLET','Biba','Fabindia','W','Biba','Anouk')
                  GROUP BY 1,
                           2) a
              LEFT JOIN (SELECT first_name|| ' ' ||last_name AS name,
                                customer_login
                         FROM bidb.dim_customer_idea dci) b ON a.customer_id = b.customer_login))
WHERE Brand_final_rank = 1

