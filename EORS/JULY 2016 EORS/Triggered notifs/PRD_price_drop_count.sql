SELECT a.* AS base
FROM (select distinct uidx,data_set_value from
			(SELECT DISTINCT uidx,
             data_set_value
      FROM clickstream.events_view
      WHERE event_type IN ('pdpLike','AddToCollection','addToList')
      AND   load_date BETWEEN TO_CHAR(SYSDATE-INTERVAL '15 days','YYYYMMDD')::BIGINT AND TO_CHAR(SYSDATE-INTERVAL '1 days','YYYYMMDD')::BIGINT
      UNION
      SELECT DISTINCT uidx,
             dp.style_id AS data_set_value
      FROM clickstream.events_view AS ev
        JOIN bidb.dim_product dp ON ev.data_set_value = dp.sku_id
      WHERE event_type IN ('addToCart')
      AND   load_date BETWEEN TO_CHAR(SYSDATE-INTERVAL '15 days','YYYYMMDD')::BIGINT AND TO_CHAR(SYSDATE-INTERVAL '1 days','YYYYMMDD')::BIGINT)) a
  LEFT JOIN (SELECT DISTINCT customer_login
             FROM bidb.fact_core_item fci
               JOIN bidb.dim_customer_idea dci ON fci.idcustomer = dci.id
             WHERE order_created_date >= TO_CHAR(SYSDATE-INTERVAL '15 days','YYYYMMDD')::BIGINT
             AND   (is_shipped = 1 OR is_realised = 1)
             AND   store_id = 1) b
         ON a.uidx = b.customer_login
WHERE b.customer_login IS NULL
