SELECT customer_id,
       style_id,
       priceb
FROM (SELECT customer_id,
             style_id,
             priceb,
             ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY price_diff DESC,curr_disc DESC) AS styl_rnk
      FROM (SELECT cusid AS customer_id,
                   a.style_id,
                   event_type,
                   event_pr,
                   priceb,
                   curr_disc,
                   (pricea - priceb) / NULLIF(pricea,0)*100 AS price_diff
            FROM (SELECT cusid,
                         style_id,
                         event_type,
                         event_pr,
                         pricea,
                         ROW_NUMBER() OVER (PARTITION BY cusid,style_id ORDER BY event_pr,load_date DESC) AS ev_rnk
                  FROM (SELECT l.uidx AS cusid,
                               load_date,
                               ph.style_id AS style_id,
                               l.event_type,
                               CASE
                                 WHEN l.event_type = 'addToCart' THEN 1
                                 WHEN l.event_type = 'addToList' THEN 2
                                 WHEN l.event_type = 'AddToCollection' THEN 3
                                 ELSE 4
                               END AS event_pr,
                               ph.article_mrp -(nvl (ph.discount_rule_percent,0)::DECIMAL(10,2) / 100)*ph.article_mrp AS pricea
                        FROM (SELECT DISTINCT uidx,
                                     data_set_value,
                                     load_date,
                                     event_type
                              FROM clickstream.events_view
                              WHERE event_type IN ('pdpLike','AddToCollection','addToList')
                              AND   load_date BETWEEN TO_CHAR(SYSDATE-INTERVAL '15 days','YYYYMMDD')::BIGINT AND TO_CHAR(SYSDATE-INTERVAL '1 days','YYYYMMDD')::BIGINT
                              UNION
                              SELECT DISTINCT uidx,
                                     dp.style_id AS data_set_value,
                                     load_date,
                                     event_type
                              FROM clickstream.events_view AS ev
                                JOIN bidb.dim_product dp ON ev.data_set_value = dp.sku_id
                              WHERE event_type IN ('addToCart')
                              AND   load_date BETWEEN TO_CHAR(SYSDATE-INTERVAL '15 days','YYYYMMDD')::BIGINT AND TO_CHAR(SYSDATE-INTERVAL '1 days','YYYYMMDD')::BIGINT) AS l
                          JOIN bidb.pricing_snapshot_history AS ph
                            ON l.data_set_value = ph.style_id
                           AND l.load_date = ph.date)) a
         JOIN (SELECT style_id,
                          article_mrp -(nvl (discount_rule_percent,0)::DECIMAL(10,2) / 100)*article_mrp AS priceb,
                          nvl(discount_rule_percent,0) AS curr_disc
                   FROM bidb.pricing_snapshot where total_inv_count>0) b ON a.style_id = b.style_id
      WHERE ev_rnk = 1)) c
  LEFT JOIN (SELECT DISTINCT customer_login
             FROM bidb.fact_core_item fci
               JOIN bidb.dim_customer_idea dci ON fci.idcustomer = dci.id
             WHERE order_created_date >= TO_CHAR(SYSDATE-INTERVAL '15 days','YYYYMMDD')::BIGINT
             AND   (is_shipped = 1 OR is_realised = 1)
             AND   store_id = 1) d ON c.customer_id = d.customer_login
WHERE styl_rnk = 1
AND   d.customer_login IS NULL

