SELECT 
			 article_type,
       gender,
       style_id
FROM (SELECT 
						 article_type,
             gender,
             style_id,
             ROW_NUMBER() OVER (PARTITION BY article_type,gender ORDER BY ros DESC) AS rnk
      FROM (SELECT s.*,
                   inv - 3*ros AS inv_left
            FROM (SELECT 
            						 dp.article_type,
                         dp.gender,
                         dp.style_id,
                         SUM(item_quantity)::DECIMAL(14,2) / 3 AS ros
                  FROM bidb.fact_order_live fol
                    JOIN bidb.dim_product dp ON fol.sku_id = dp.sku_id
                  WHERE order_created_time <= 300
                  GROUP BY 1,
                           2,
                           3) s
              LEFT JOIN (SELECT style_id,
                                SUM(inventory_count) AS inv
                         FROM bidb.fact_wh_inventory wh
                           JOIN bidb.dim_product dp ON wh.sku_id = dp.sku_id
                         WHERE store_id = 1
                         AND   warehouse_id NOT IN (24,56)
                         GROUP BY 1) i ON s.style_id = i.style_id)
      WHERE inv_left > 0)
WHERE rnk = 1

