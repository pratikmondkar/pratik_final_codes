SELECT DISTINCT customer_login,
       style_id
FROM bidb.fact_order_live fol
  JOIN dim_product dp ON fol.sku_id = dp.sku_id
WHERE (is_shipped = 1 OR is_realised = 1)
UNION
SELECT DISTINCT customer_login,
       style_id
FROM bidb.fact_core_item fci
  JOIN dim_customer_idea dci ON fci.idcustomer = dci.id
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date = 20160702

