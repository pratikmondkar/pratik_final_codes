SELECT cusid,
      article_type,
      gender
FROM (SELECT a.cusid AS cusid,
            article_type,
            gender,
            ROW_NUMBER() OVER (PARTITION BY a.cusid ORDER BY count1 DESC) AS rank1
     FROM (SELECT l.uidx AS cusid,
                  s.article_type AS article_type,
                  s.gender AS gender,
                  COUNT(DISTINCT s.style_id) AS count1
           FROM bidb.collection_log AS l
             JOIN bidb.dim_style AS s
               ON l.style_id = s.style_id
              AND (l.added_date = TO_CHAR (SYSDATE-INTERVAL '1 days','YYYYMMDD')::BIGINT)
           GROUP BY 1,
                    2,
                    3) a
       LEFT JOIN (SELECT DISTINCT dci.customer_login AS cusid
                  FROM bidb.dim_customer_idea dci
                    JOIN bidb.fact_core_item fci ON dci.id = fci.idcustomer
                  WHERE store_id = 1
                  AND   (is_shipped = 1 OR is_realised = 1)
                  AND   fci.order_created_date = TO_CHAR(convert_timezone('Asia/Calcutta',SYSDATE),'YYYYMMDD')::BIGINT) c ON a.cusid = c.cusid
     WHERE c.cusid IS NULL)
WHERE rank1 = 1
