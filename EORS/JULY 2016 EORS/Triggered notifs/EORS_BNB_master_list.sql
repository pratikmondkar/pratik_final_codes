SELECT cusid,
      article_type,
      gender
FROM (SELECT a.cusid AS cusid,
						brand,
            gender,
            ROW_NUMBER() OVER (PARTITION BY a.cusid ORDER BY count1 DESC) AS rank1
     FROM (SELECT l.uidx AS cusid,
     							s.brand,
                  s.article_type AS article_type,
                  s.gender AS gender,
                  COUNT(DISTINCT s.style_id) AS count1
           FROM bidb.collection_log AS l
             JOIN bidb.dim_style AS s
               ON l.style_id = s.style_id
              AND (l.added_date = TO_CHAR (convert_timezone('Asia/Calcutta',SYSDATE)-INTERVAL '1 days','YYYYMMDD')::BIGINT)
           GROUP BY 1,
                    2,
                    3,
                    4) a
       LEFT JOIN (SELECT DISTINCT customer_login
             FROM bidb.fact_order_live
             WHERE (is_shipped = 1 OR is_realised = 1)) c ON a.cusid = c.customer_login
     WHERE c.customer_login IS NULL)
WHERE rank1 = 1
