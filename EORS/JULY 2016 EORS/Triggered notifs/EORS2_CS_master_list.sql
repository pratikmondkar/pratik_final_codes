SELECT cusid,
       article_type,
       gender
FROM (SELECT a.cusid AS cusid,
             article_type,
             gender,
             ROW_NUMBER() OVER (PARTITION BY a.cusid ORDER BY count1 DESC) AS rank1
      FROM (SELECT l.customer_login AS cusid,
                   s.article_type AS article_type,
                   s.gender AS gender,
                   COUNT(DISTINCT l.style_id) AS count1
            FROM (SELECT DISTINCT customer_login,
                         style_id
                  FROM bidb.fact_order_live fol
                    JOIN dim_product dp ON fol.sku_id = dp.sku_id
                  WHERE (is_shipped = 1 OR is_realised = 1)
                  UNION
                  SELECT DISTINCT customer_login,
                         style_id
                  FROM bidb.fact_core_item fci
                    JOIN dim_customer_idea dci ON fci.idcustomer = dci.id
                  WHERE store_id = 1
                  AND   (is_shipped = 1 OR is_realised = 1)
                  AND   order_created_date = 20160702) AS l
              JOIN bidb.dim_style AS s ON l.style_id = s.style_id
            GROUP BY 1,
                     2,
                     3) a)
WHERE rank1 = 1

