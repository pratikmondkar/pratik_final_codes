SELECT count(distinct a.uidx) as users ,count(distinct a.style_id) as styles,sum(nvl(inv,0)) as inv
FROM (SELECT uidx, style_id FROM bidb.collection_log where added_date = TO_CHAR (convert_timezone('Asia/Calcutta',SYSDATE)-INTERVAL '1 days','YYYYMMDD')::BIGINT) a
  LEFT JOIN (SELECT DISTINCT customer_login
             FROM bidb.fact_order_live
             WHERE (is_shipped = 1 OR is_realised = 1)) b ON a.uidx = b.customer_login
  LEFT JOIN (SELECT style_id,
                    SUM(inventory_count) AS inv
             FROM bidb.fact_wh_inventory wh
               JOIN bidb.dim_product dp ON wh.sku_id = dp.sku_id
             WHERE store_id = 1
             AND   warehouse_id NOT IN (24,56)
             GROUP BY 1) i ON a.style_id = i.style_id
and b.customer_login is null
AND   nvl (i.inv,0) > 0
