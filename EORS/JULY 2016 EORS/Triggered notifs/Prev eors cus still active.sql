select count(distinct a.customer_login) as eors_customners,count(distinct case when b.uidx is not null then a.customer_login end) as still_active 
from
(select distinct customer_login from bidb.fact_order fo join bidb.dim_customer_idea dci on fo.idcustomer_idea=dci.id
where store_id=1 and (is_shipped=1 or is_realised=1) and 
order_created_date in (20150103,20150718,20150719,20160102,20160103,20160104)) a
left join
(select distinct uidx from 
clickstream.events_view 
where load_date between to_char(sysdate - interval '7 days','YYYYMMDD')::bigint and to_char(sysdate - interval '1 days','YYYYMMDD')::bigint 
and event_type in ('ScreenLoad','push-notification-received','beacon-ping')) b
on a.customer_login=b.uidx
