SELECT customer_id,
       style_id,
       priceb
FROM (SELECT customer_id,
             style_id,
             priceb,
             ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY price_diff DESC,curr_disc DESC) AS styl_rnk
      FROM (SELECT cusid AS customer_id,
                   a.style_id,
                   priceb,
                   curr_disc,
                   (pricea - priceb) / NULLIF(pricea,0)*100 AS price_diff
            FROM (SELECT l.uidx AS cusid,
                         ph.style_id AS style_id,
                         MIN(ph.article_mrp -(nvl (ph.discount_rule_percent,0)::DECIMAL(10,2) / 100)*ph.article_mrp) AS pricea
                  FROM clickstream.events_view AS l
                    JOIN bidb.pricing_snapshot_history AS ph
                      ON l.data_set_value = ph.style_id
                     AND l.load_date = ph.date
                  WHERE event_type IN ('ScreenLoad')
                  AND   load_date BETWEEN TO_CHAR(SYSDATE-INTERVAL '15 days','YYYYMMDD')::BIGINT AND TO_CHAR(SYSDATE-INTERVAL '1 days','YYYYMMDD')::BIGINT
                  GROUP BY 1,2) a
              JOIN (SELECT style_id,
                           article_mrp -(nvl (discount_rule_percent,0)::DECIMAL(10,2) / 100)*article_mrp AS priceb,
                           nvl(discount_rule_percent,0) AS curr_disc
                    FROM bidb.pricing_snapshot
                    WHERE total_inv_count > 0) b ON a.style_id = b.style_id)) c
  LEFT JOIN (SELECT DISTINCT customer_login
             FROM bidb.fact_core_item fci
               JOIN bidb.dim_customer_idea dci ON fci.idcustomer = dci.id
             WHERE order_created_date >= TO_CHAR(SYSDATE-INTERVAL '15 days','YYYYMMDD')::BIGINT
             AND   (is_shipped = 1 OR is_realised = 1)
             AND   store_id = 1) d ON c.customer_id = d.customer_login
    LEFT JOIN (SELECT distinct uidx FROM bidb.collection_log where added_date = TO_CHAR (convert_timezone('Asia/Calcutta',SYSDATE)-INTERVAL '1 days','YYYYMMDD')::BIGINT) e
     ON c.customer_id = e.uidx
WHERE styl_rnk = 1
AND   d.customer_login IS NULL
and 	e.uidx is NULL

