SELECT b.*
FROM (SELECT device_id AS customer_id
      FROM (SELECT device_id,
                   uidx,
                   ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY len (uidx) DESC) AS rnk
            FROM (SELECT DISTINCT device_id,
                         uidx
                  FROM clickstream.events_view
                  WHERE event_type IN ('push-notification-received','beacon-ping','ScreenLoad')
                  AND   load_date BETWEEN TO_CHAR(SYSDATE-INTERVAL '7 days','YYYYMMDD')::BIGINT AND TO_CHAR(SYSDATE-INTERVAL '1 day','YYYYMMDD')::BIGINT))
      WHERE rnk = 1
      AND   uidx IS NULL) a
  JOIN (SELECT customer_id,
               style_id,
               priceb
        FROM (SELECT customer_id,
                     style_id,
                     priceb,
                     ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY price_diff DESC,curr_disc DESC) AS styl_rnk
              FROM (SELECT cusid AS customer_id,
                           a.style_id,
                           priceb,
                           curr_disc,
                           (pricea - priceb) / NULLIF(pricea,0)*100 AS price_diff
                    FROM (SELECT l.device_id AS cusid,
                                 ph.style_id AS style_id,
                                 MIN(ph.article_mrp -(nvl (ph.discount_rule_percent,0)::DECIMAL(10,2) / 100)*ph.article_mrp) AS pricea
                          FROM clickstream.events_view l
                            JOIN bidb.pricing_snapshot_history AS ph
                              ON l.data_set_value = ph.style_id
                             AND l.load_date = ph.date
                          WHERE event_type IN ('ScreenLoad')
                          AND   load_date BETWEEN TO_CHAR(SYSDATE-INTERVAL '15 days','YYYYMMDD')::BIGINT AND TO_CHAR(SYSDATE-INTERVAL '1 days','YYYYMMDD')::BIGINT
                          GROUP BY 1,2) a
                      JOIN (SELECT style_id,
                                   article_mrp -(nvl (discount_rule_percent,0)::DECIMAL(10,2) / 100)*article_mrp AS priceb,
                                   nvl(discount_rule_percent,0) AS curr_disc
                            FROM bidb.pricing_snapshot
                            WHERE total_inv_count > 10) b ON a.style_id = b.style_id)) c
        WHERE styl_rnk = 1) b ON a.customer_id = b.customer_id

