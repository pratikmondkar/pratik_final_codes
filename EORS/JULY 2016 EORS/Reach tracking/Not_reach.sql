SELECT DISTINCT a.device_id 
FROM (SELECT DISTINCT device_id
      FROM clickstream.events_view
      WHERE event_type IN ('push-notification-received','beacon-ping','ScreenLoad')
      AND   os = 'Android'
      AND   load_date BETWEEN TO_CHAR(SYSDATE-INTERVAL '2 days','YYYYMMDD')::BIGINT AND TO_CHAR(SYSDATE-INTERVAL '1 days','YYYYMMDD')::BIGINT) a
  LEFT JOIN (SELECT distinct device_id
             FROM clickstream.events_view
             WHERE event_type IN ('push-notification-received','push-notification-dismissed','push-notification')
             AND   os = 'Android'
             and referrer_url in ('http://www.myntra.com/pre-launch-eors?utm_source=Offers_Coupons&utm_medium=Notif&utm_term=1&utm_campaign=9PM_27JUNE2016_Y_SEG_30_21_ANDROID_OffersCoupons_EORSLaunch&utm_nid=60ab7740-3c7c-11e6-b1a4-635',
										 							'http://www.myntra.com/pre-launch-eors?utm_source=Offers_Coupons&utm_medium=Notif&utm_term=1&utm_campaign=06PM_26JUNE2016_Y_SEG_30_21_ANDROID_OffersCoupons_EORSLaunch&utm_nid=bd609b50-3ba0-11e6-b45a-ed',
										 							'http://www.myntra.com/pre-launch-eors?utm_source=Offers_Coupons&utm_medium=Notif&utm_term=1&utm_campaign=6PM_28JUNE2016_Y_ALL_30_21_ANDROID_OffersCoupons_eors_Wishlist&utm_nid=52d508c0-3d2b-11e6-821d-',
										 							'http://www.myntra.com/pre-launch-eors?utm_source=Offers_Coupons&utm_medium=Notif&utm_term=1&utm_campaign=9PM_29JUNE2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_brands&utm_nid=4028e8c0-3dfa-11e6-821d-a3',
										 							'http://www.myntra.com/pre-launch-eors?utm_source=Offers_Coupons&utm_medium=Notif&utm_term=1&utm_campaign=2PM_29JUNE2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_Slots&utm_nid=cb2daab0-3d04-11e6-821d-a3f')
             AND   load_date >= 20160626
             GROUP BY 1) b ON a.device_id = b.device_id
      where b.device_id is null
