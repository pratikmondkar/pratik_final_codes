-- 20160626: bd609b50-3ba0-11e6-b45a-ede2a627ca87
-- 20160627: 60ab7740-3c7c-11e6-b1a4-635e89a4f772
-- 20160628: 52d508c0-3d2b-11e6-821d-a3f8bbac9480

SELECT TO_CHAR(sysdate-interval '1 day','YYYYMMDD') AS DATE,
       COUNT(DISTINCT a.device_id) AS d7_base,
       COUNT(DISTINCT CASE WHEN nvl (b.received,0) > 0 THEN a.device_id END) AS unique_reach,
       COUNT(DISTINCT CASE WHEN nvl (b.dismissed,0) > 0 THEN a.device_id END) AS unique_dismissed,
       COUNT(DISTINCT CASE WHEN nvl (b.opened,0) > 0 THEN a.device_id END) AS unique_opened,
       SUM(received) AS reached,
       SUM(dismissed) AS dismissed,
       SUM(opened) AS opened
FROM (SELECT DISTINCT device_id
      FROM clickstream.events_view
      WHERE event_type IN ('push-notification-received','beacon-ping','ScreenLoad')
      AND   os = 'Android'
      AND   load_date BETWEEN TO_CHAR(SYSDATE-INTERVAL '7 days','YYYYMMDD')::BIGINT AND TO_CHAR(SYSDATE-INTERVAL '1 days','YYYYMMDD')::BIGINT) a
  LEFT JOIN (SELECT device_id,
                    COUNT(CASE WHEN event_type = 'push-notification-received' THEN 1 END) AS received,
                    COUNT(CASE WHEN event_type = 'push-notification-dismissed' THEN 1 END) AS dismissed,
                    COUNT(CASE WHEN event_type = 'push-notification' THEN 1 END) AS opened
             FROM clickstream.events_view
             WHERE event_type IN ('push-notification-received','push-notification-dismissed','push-notification')
             AND   os = 'Android'
             and referrer_url in ('http://www.myntra.com/pre-launch-eors?utm_source=Offers_Coupons&utm_medium=Notif&utm_term=1&utm_campaign=9PM_27JUNE2016_Y_SEG_30_21_ANDROID_OffersCoupons_EORSLaunch&utm_nid=60ab7740-3c7c-11e6-b1a4-635',
										 							'http://www.myntra.com/pre-launch-eors?utm_source=Offers_Coupons&utm_medium=Notif&utm_term=1&utm_campaign=06PM_26JUNE2016_Y_SEG_30_21_ANDROID_OffersCoupons_EORSLaunch&utm_nid=bd609b50-3ba0-11e6-b45a-ed',
										 							'http://www.myntra.com/pre-launch-eors?utm_source=Offers_Coupons&utm_medium=Notif&utm_term=1&utm_campaign=6PM_28JUNE2016_Y_ALL_30_21_ANDROID_OffersCoupons_eors_Wishlist&utm_nid=52d508c0-3d2b-11e6-821d-',
										 							'http://www.myntra.com/pre-launch-eors?utm_source=Offers_Coupons&utm_medium=Notif&utm_term=1&utm_campaign=9PM_29JUNE2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_brands&utm_nid=4028e8c0-3dfa-11e6-821d-a3',
										 							'http://www.myntra.com/pre-launch-eors?utm_source=Offers_Coupons&utm_medium=Notif&utm_term=1&utm_campaign=2PM_29JUNE2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_Slots&utm_nid=cb2daab0-3d04-11e6-821d-a3f')
             AND   load_date >= 20160626
             GROUP BY 1) b ON a.device_id = b.device_id;

SELECT TO_CHAR(sysdate-interval '2 day','YYYYMMDD') AS DATE,
       COUNT(DISTINCT CASE WHEN nvl (b.received,0) > 0 THEN b.device_id END) AS unique_reach,
       COUNT(DISTINCT CASE WHEN nvl (b.dismissed,0) > 0 THEN b.device_id END) AS unique_dismissed,
       COUNT(DISTINCT CASE WHEN nvl (b.opened,0) > 0 THEN b.device_id END) AS unique_opened
FROM  (SELECT device_id,
                    COUNT(CASE WHEN event_type = 'push-notification-received' THEN 1 END) AS received,
                    COUNT(CASE WHEN event_type = 'push-notification-dismissed' THEN 1 END) AS dismissed,
                    COUNT(CASE WHEN event_type = 'push-notification' THEN 1 END) AS opened
             FROM clickstream.events_view
             WHERE event_type IN ('push-notification-received','push-notification-dismissed','push-notification')
             AND   os = 'Android'
             AND   referrer_url IN ('http://www.myntra.com/pre-launch-eors?utm_source=Offers_Coupons&utm_medium=Notif&utm_term=1&utm_campaign=9PM_29JUNE2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_brands&utm_nid=4028e8c0-3dfa-11e6-821d-a3',
										 								'http://www.myntra.com/pre-launch-eors?utm_source=Offers_Coupons&utm_medium=Notif&utm_term=1&utm_campaign=2PM_29JUNE2016_Y_ALL_30_21_ANDROID_OffersCoupons_EORS_Slots&utm_nid=cb2daab0-3d04-11e6-821d-a3f')
             AND   load_date >= to_char(sysdate - INTERVAL '2 days','YYYYMMDD')::bigint
             GROUP BY 1) b
