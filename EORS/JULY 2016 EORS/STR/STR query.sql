SELECT brand,
       brand_type,
       current_commercial_type,
       article_type,
       gender,
       season_code,
       subcategory,
       master_category,
       business_unit,
       SUM(inv) AS inv,
       SUM(nvl (disc,0)) AS disc,
       SUM(nvl (mrp,0)) AS mrp,
       SUM(nvl (qty,0)) AS qty
FROM (SELECT style_id, inv FROM dev.atp_snapshot_20160101) i
  LEFT JOIN (SELECT style_id,
                    SUM(disc) AS disc,
                    SUM(mrp) AS mrp,
                    SUM(qty) AS qty
             FROM (SELECT dp.style_id,
                          SUM(product_discount + coupon_discount) AS disc,
                          SUM(item_mrp_value*quantity) AS mrp,
                          SUM(quantity) AS qty
                   FROM fact_core_item fci
                     JOIN dim_product dp ON fci.sku_id = dp.sku_id
                   WHERE (CAST(order_created_date || ' ' || LPAD(order_created_time,6,0) AS TIMESTAMP) > '2017-01-01 19:00:00' AND order_created_date <= TO_CHAR(convert_timezone('Asia/Calcutta',SYSDATE) -INTERVAL '1 day','YYYYMMDD')::BIGINT)
                   AND   (is_shipped = 1 OR is_realised = 1)
                   AND   store_id = 1
                   GROUP BY 1
                   UNION
                   SELECT dp.style_id,
                          SUM(trade_discount + coupon_discount) AS disc,
                          SUM(item_mrp*item_quantity) AS mrp,
                          SUM(item_quantity) AS qty
                   FROM fact_order_live fol
                     JOIN dim_product dp ON fol.sku_id = dp.sku_id
                   WHERE (is_shipped = 1 OR is_realised = 1)
                   AND   order_created_date = TO_CHAR(convert_timezone('Asia/Calcutta',SYSDATE),'YYYYMMDD')::BIGINT
                   AND   CAST(order_created_date || ' ' || LPAD(order_created_time,4,0) AS TIMESTAMP) > '2017-01-01 19:00:00'
                   GROUP BY 1)
             GROUP BY 1) s ON i.style_id = s.style_id
  LEFT JOIN dim_style dp ON i.style_id = dp.style_id
GROUP BY 1,
         2,
         3,
         4,
         5,
         6,
         7,
         8,
         9 LIMIT 1000

