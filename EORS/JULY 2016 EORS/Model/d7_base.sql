SELECT DISTINCT device_id
FROM clickstream.events_view
WHERE event_type IN ('push-notification-received','beacon-ping','ScreenLoad')
AND   load_date BETWEEN TO_CHAR(sysdate-interval '7 days','YYYYMMDD')::bigint AND TO_CHAR(sysdate-interval '1 days','YYYYMMDD')::bigint

