SELECT a.*,
       nvl(b.last_order_value,0) AS last_order_value
FROM (SELECT customer_login,
             to_date(MAX(order_created_date),'YYYYMMDD') AS last_order_date,
             to_date(MIN(order_created_date),'YYYYMMDD') AS first_order_date,
             COUNT(DISTINCT order_group_id) AS orders,
             SUM(shipped_order_revenue_inc_cashback) AS revenue,
             COUNT(DISTINCT CASE WHEN weekend_indicator = 1 THEN order_group_id END) AS weekend_orders,
             COUNT(DISTINCT CASE WHEN weekend_indicator = 0 THEN order_group_id END) AS weekday_orders
      FROM bidb.fact_order fo
        JOIN bidb.dim_date dd ON fo.order_created_date = dd.full_date
        JOIN bidb.dim_customer_idea dci ON fo.idcustomer_idea = dci.id
      WHERE store_id = 1
      AND   (is_shipped = 1 OR is_realised = 1)
      AND   order_created_date < 20160625
      GROUP BY 1) a
  LEFT JOIN (SELECT customer_login,
                    order_value AS last_order_value from (SELECT customer_login,
                                                           order_value,
                                                           ROW_NUMBER() OVER (PARTITION BY customer_login ORDER BY order_created_date DESC) AS rnk
                                                    FROM (SELECT customer_login,
                                                                 order_group_id,
                                                                 order_created_date,
                                                                 SUM(shipped_order_revenue_inc_cashback) AS order_value
                                                          FROM bidb.fact_order fo
                                                            JOIN bidb.dim_customer_idea dci ON fo.idcustomer_idea = dci.id
                                                          WHERE store_id = 1
                                                          AND   (is_shipped = 1 OR is_realised = 1)
                                                          AND   order_created_date < 20160624
                                                          GROUP BY 1,2,3))
             WHERE rnk = 1) b ON a.customer_login = b.customer_login

