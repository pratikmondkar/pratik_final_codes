SELECT j.uidx as customer_login,
			 a.notifs_received_pre,
			 a.notifs_opened_pre,
			 a.banner_clicks_filt,
			 a.inapp_notifs,
			 a.offer_clicks,
			 a.sessions,
			 a.add_to_carts,
			 a.searches,
       b.tme_since_ll,
       a.avg_disc_view
FROM 
  		 (SELECT distinct uidx
        FROM clickstream.events_view
        WHERE load_date BETWEEN 20160610 and 20160616
        AND   event_type in ('ScreenLoad','appLaunch','push-notification-received','beacon-ping')) j 
left join        
(SELECT uidx,
             COUNT(CASE WHEN event_type = 'push-notification-received' THEN 1 END) AS notifs_received_pre,
             COUNT(CASE WHEN event_type = 'push-notification' THEN 1 END) AS notifs_opened_pre,
             COUNT(CASE WHEN event_type = 'NotificationItemsClick' THEN 1 END) AS inapp_notifs,
             COUNT(CASE WHEN event_type = 'clickForOffer' THEN 1 END) AS offer_clicks,
             COUNT(CASE WHEN event_type = 'banner-click' THEN 1 END) AS banner_clicks_filt,
             COUNT(DISTINCT CASE WHEN event_type = 'ScreenLoad' THEN session_id end) AS sessions,
             (sum(case when  event_type='ScreenLoad' and data_set_type='product' then mrp end) - sum(case when  event_type='ScreenLoad' and data_set_type='product' then discounted_price end))/sum(case when  event_type='ScreenLoad' and data_set_type='product' then mrp end) as avg_disc_view,
             count(CASE WHEN event_type = 'addToCart' THEN 1 END) AS add_to_carts,
             count(CASE WHEN event_type in ('SearchFired','searchFired') THEN 1 END) AS searches
      FROM clickstream.events_view
      WHERE load_date BETWEEN 20160602 and 20160616 and event_type in ('ScreenLoad','push-notification-received','push-notification','NotificationItemsClick','clickForOffer',
      'banner-click','addToCart','SearchFired','searchFired')
      GROUP BY 1) a
  ON a.uidx = j.uidx
  LEFT JOIN (SELECT uidx,
                    datediff(d,TO_DATE(MAX(load_date),'YYYYMMDD'),'2016-06-16 00:00:00') AS tme_since_ll
             FROM clickstream.events_view sl
             WHERE load_date BETWEEN 20160301 AND 20160616 and event_type in ('ScreenLoad') 
             GROUP BY 1) b ON j.uidx = b.uidx ;
             

select event_type,count(1) from clickstream.events_2016_06_20 group by 1
