drop table if exists dev.pm_temp1;

create table dev.pm_temp1 distkey(uidx) sortkey(oos_flag) as
select a.*,case when inv=0 then 1 else 0 end as oos_flag,case when avail_skus::float/total_skus <0.6 then 1 else 0 end as broken_flag from
(SELECT uidx,
                   style_id
            FROM (SELECT uidx,
                         cl.style_id,
                         action,
                         ROW_NUMBER() OVER (PARTITION BY uidx,cl.style_id ORDER BY added_datetime DESC) AS rnk
                  FROM bidb.collection_log_eors cl
                    JOIN dim_style dp ON cl.style_id = dp.style_id
                  WHERE uidx NOT LIKE '%myntra360.com%'
                  AND   uidx NOT LIKE '%eossbenchmarking%@gmail.com'
                  AND   uidx NOT LIKE '%scmloadtest%'
                  AND   uidx NOT LIKE '%sfloadtest%'
                  and added_date>=20170622 and added_datetime>='2017-06-22 19:00:00')
            WHERE rnk = 1
            AND   action = 1) a
            left join 
            (select style_id,sum(net_inventory_count) as inv ,
            count(distinct atp.sku_id ) as total_skus,count(distinct case when net_inventory_count>0 then atp.sku_id end) as avail_skus
            from o_atp_inventory atp 
            join dim_product dp on atp.sku_id=dp.sku_id 
            group by 1) c
            on a.style_id=c.style_id;
            
select * from dev.pm_temp1 limit 100;

select count(1) as total_users,count(case when oos_flag=1 then 1 end) as oos_users,count(case when broken_flag=1 then 1 end) as broken_users
from dev.pm_temp1;

select count(distinct uidx) as total_users,count(distinct case when oos_flag=1 then uidx end) as oos_users,count(distinct case when broken_flag=1 then uidx end) as broken_users
from dev.pm_temp1;

select count(distinct style_id) as total_users,count(distinct case when oos_flag=1 then style_id end) as oos_users,count(distinct case when broken_flag=1 then style_id end) as broken_users
from dev.pm_temp1;
