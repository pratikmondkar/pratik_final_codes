create table dev.pm_mfg_send_list distkey(uidx) as
select count(distinct a.uidx) as total_user,count(distinct case when c.uidx is not null then c.uidx end) as group_reco,
count(distinct case when d.uidx is not null and c.uidx is null then d.uidx end) as add_user_reco from
(SELECT DISTINCT a.group_id,group_member as uidx
FROM  (select group_member,group_id from 
							(select distinct group_member,group_id,row_number() over (partition by group_member order by join_timestamp desc) as rnk from clickstream.mfg_group_user where is_active = 1)
			 where rnk=1) a
  JOIN (SELECT group_id,
               group_is_active,
               campaign_id
        FROM clickstream.mfg_group) b ON a.group_id = b.group_id
WHERE b.group_is_active = 'true'
AND   b.campaign_id = 'jun-eors-2k17') a
left join (select distinct customer_login as uidx from bidb.fact_order_live where (is_shipped=1 or is_realised=1) and order_Created_date>=20170623) b on a.uidx=b.uidx
left join customer_insights.mfg_group_reco c on a.uidx=c.uidx
left join customer_insights.mfg_usr_reco d on a.uidx=d.uidx
where b.uidx is null;
--limit 100


drop table if exists dev.pm_mfg_temp_swp;
create table dev.pm_mfg_temp_swp distkey(uidx) as 
(select distinct a.uidx from
(SELECT DISTINCT a.group_id,group_member as uidx
FROM  (select group_member,group_id from 
							(select distinct group_member,group_id,row_number() over (partition by group_member order by join_timestamp desc) as rnk from clickstream.mfg_group_user where is_active = 1)
			 where rnk=1) a
  JOIN (SELECT group_id,
               group_is_active,
               campaign_id
        FROM clickstream.mfg_group) b ON a.group_id = b.group_id
WHERE b.group_is_active = 'true'
AND   b.campaign_id = 'jun-eors-2k17') a
left join (select distinct customer_login as uidx from bidb.fact_order_live where (is_shipped=1 or is_realised=1) and order_Created_date>=20170623) b on a.uidx=b.uidx
left join customer_insights.mfg_group_reco c on a.uidx=c.uidx
left join customer_insights.mfg_usr_reco d on a.uidx=d.uidx
where b.uidx is null and d.uidx is not null and c.uidx is null);


delete from customer_insights.mfg_group_reco where uidx in (select uidx from dev.pm_mfg_temp_swp);

insert into customer_insights.mfg_group_reco (select * from customer_insights.mfg_usr_reco where uidx in (select uidx from dev.pm_mfg_temp_swp));

select count(*),count(distinct uidx) from customer_insights.mfg_group_reco;


drop table if exists dev.pm_mfg_send_list;
create table dev.pm_mfg_send_list distkey(uidx) as 
(select distinct a.uidx from
(SELECT DISTINCT a.group_id,group_member as uidx
FROM  (select group_member,group_id from 
							(select distinct group_member,group_id,row_number() over (partition by group_member order by join_timestamp desc) as rnk from clickstream.mfg_group_user where is_active = 1)
			 where rnk=1) a
  JOIN (SELECT group_id,
               group_is_active,
               campaign_id
        FROM clickstream.mfg_group) b ON a.group_id = b.group_id
WHERE b.group_is_active = 'true'
AND   b.campaign_id = 'jun-eors-2k17') a
left join (select distinct customer_login as uidx from bidb.fact_order_live where (is_shipped=1 or is_realised=1) and order_Created_date>=20170623) b on a.uidx=b.uidx
left join customer_insights.mfg_group_reco c on a.uidx=c.uidx
where b.uidx is null and c.uidx is not null);
