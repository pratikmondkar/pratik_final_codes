select 
case when order_created_date BETWEEN 20170623 and 20170629 then 'EORS6' when order_created_date BETWEEN 20170103 and 20170108 then 'EORS5' end as eors,
order_created_date,
business_unit,
sum(quantity) as qty   
FROM fact_core_item fci
left join dim_product dp on fci.sku_id=dp.sku_id
WHERE store_id = 1
AND   (is_shipped=1 or is_realised=1)
AND   (order_created_date BETWEEN 20170623 and 20170629  or order_created_date BETWEEN 20170103 and 20170108) and fci.supply_type='ON_HAND' and po_type!='SMART_JIT'
group by 1,2,3;

select
business_unit,
sum(inv) as starting_inv  
from dev.atp_snapshot_20170623 a
join 
(select distinct style_id,business_unit
FROM fact_inventory_count fci
left join dim_product dp on fci.sku_id=dp.sku_id
WHERE warehouse_id not in (24,56,117) and quality = 'Q1' and  date=20170622  and inv_count is not null ) b on a.style_id=b.style_id
group by 1;


select a.* from 
dev.atp_snapshot_20170623 a
join 
(select distinct style_id
FROM fact_inventory_count fci
left join dim_product dp on fci.sku_id=dp.sku_id
WHERE warehouse_id not in (24,56,117) and quality = 'Q1' and  date=20170622  and inv_count is not null
group by 1) b on a.style_id=b.style_id
;

select 
case when date BETWEEN 20170623 and 20170629 then 'EORS6' when date BETWEEN 20170103 and 20170108 then 'EORS5' end as eors,
date,
business_unit,
count(distinct case when is_broken_style=1 then fci.style_id end) as broken_styles,
count(distinct case when days_since_cataloguing<=30 then fci.style_id end) as broken_styles,
count(distinct case when is_live_style=1 then fci.style_id end) as live_styles
FROM customer_insights.fact_category_over_view_metrics fci
WHERE (date BETWEEN 20170623 and 20170629  or date BETWEEN 20170103 and 20170108) and fci.supply_type='ON_HAND' and commercial_type!='SMART_JIT'
group by 1,2,3 ;

select 
case when date =20170622 then 'EORS6' when date =20170101 then 'EORS5' end as eors,
business_unit,
sum(live_styles) as live_styles
FROM customer_insights.fact_category_over_view_metrics fci
WHERE date in(20170622,20170101) and fci.supply_type='ON_HAND' and commercial_type!='SMART_JIT'
group by 1,2;


select sum(live_styles)
--style_id,live_styles,is_live_style,business_unit 
from customer_insights.fact_category_over_view_metrics 
where date=20170623 and supply_type='ON_HAND' and commercial_type!='SMART_JIT' and business_unit='Accessories'
limit 1000;


select 
case when date BETWEEN 20170623 and 20170629 then 'EORS6' when date BETWEEN 20170103 and 20170108 then 'EORS5' end as eors,
date,
business_unit,
sum(live_styles-broken_styles) as non_broken_styles
FROM customer_insights.fact_category_over_view_metrics fci
WHERE (date BETWEEN 20170623 and 20170629  or date BETWEEN 20170103 and 20170108) and fci.supply_type='ON_HAND' and commercial_type!='SMART_JIT'
group by 1,2,3;

select 
b.eors,
b.date,
business_unit,
count(distinct case when is_live_style=1 and is_broken_style=1 then b.style_id end) as non_broken_styles
from
(select
case when date =20170622 then 'EORS6' when date =20170101 then 'EORS5' end as eors,
style_id
FROM customer_insights.fact_category_over_view_metrics fci
WHERE date in(20170622,20170101) and fci.supply_type='ON_HAND' and commercial_type!='SMART_JIT' and is_live_style=1
group by 1,2) a
left join 
(select distinct
case when date BETWEEN 20170623 and 20170629 then 'EORS6' when date BETWEEN 20170103 and 20170108 then 'EORS5' end as eors,
date,
business_unit,
style_id,is_live_style,is_broken_style
FROM customer_insights.fact_category_over_view_metrics fci
WHERE (date BETWEEN 20170623 and 20170629  or date BETWEEN 20170103 and 20170108) and fci.supply_type='ON_HAND' and commercial_type!='SMART_JIT') b
on  a.style_id=b.style_id and a.eors=b.eors
group by 1,2,3;



select distinct
case when date BETWEEN 20170623 and 20170629 then 'EORS6' when date BETWEEN 20170103 and 20170108 then 'EORS5' end as eors,
date,
business_unit,
style_id,is_live_style,is_broken_style
FROM customer_insights.fact_category_over_view_metrics fci
WHERE (date BETWEEN 20170623 and 20170629  or date BETWEEN 20170103 and 20170108) and fci.supply_type='ON_HAND' and commercial_type!='SMART_JIT';
