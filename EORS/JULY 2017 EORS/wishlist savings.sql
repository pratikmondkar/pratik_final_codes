drop table if exists dev.pm_temp2;
create table dev.pm_temp2 distkey(uidx) as
select a.uidx,sum(article_mrp*nvl(discount_rule_percent::float8/100,0)) as savings from 
(SELECT uidx,
                   style_id
            FROM (SELECT uidx,
                         cl.style_id,
                         action,
                         ROW_NUMBER() OVER (PARTITION BY uidx,cl.style_id ORDER BY added_datetime DESC) AS rnk
                  FROM bidb.collection_log_eors cl
                    JOIN dim_style dp ON cl.style_id = dp.style_id
                  WHERE uidx NOT LIKE '%myntra360.com%'
                  AND   uidx NOT LIKE '%eossbenchmarking%@gmail.com'
                  AND   uidx NOT LIKE '%scmloadtest%'
                  AND   uidx NOT LIKE '%sfloadtest%'
                  and added_date>=20170622 and added_datetime>='2017-06-22 19:00:00')
            WHERE rnk = 1
            AND   action = 1) a
            left join pricing_snapshot ps on a.style_id=ps.style_id
            group by 1;
            
select * from dev.pm_temp2;
            
select * from stv_inflight;


select * from pricing_snapshot where style_id=710843;


