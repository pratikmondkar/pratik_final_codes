drop table if exists dev.pm_temp4;
create table dev.pm_temp4 distkey(group_id) as 
select group_id,uidx, savings,case when b.customer_login is not null then 1 else 0 end as purchase_flag from
(SELECT DISTINCT a.group_id, a.uidx
FROM  mfg_source_members a
  JOIN (SELECT group_id,
               group_is_active,
               campaign_id
        FROM mfg_source_groups) b ON a.group_id = b.group_id
WHERE b.group_is_active = 'true') a
left join 
(select customer_login,sum(nvl(item_revenue,0)+nvl(cashback_redeemed,0)) as savings 
from fact_order_live fol
where (is_shipped=1 or is_realised=1) 
and order_created_date>=20170623
group by 1) b on a.uidx=b.customer_login;

select sum(savings) from dev.pm_temp3 where group_id='4fa5013e-1335-4399-8db9-de2ec8a57df4' limit 100;

alter table dev.pm_temp3 add column group_savings float8;

update  dev.pm_temp3 
set group_savings=nvl(src.group_savings,0)
from
(select group_id,sum(savings) as group_savings
from dev.pm_temp3 
group by 1) src
where src.group_id=dev.pm_temp3.group_id;


