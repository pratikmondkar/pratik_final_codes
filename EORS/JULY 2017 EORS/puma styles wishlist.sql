select a.*,c.seller_id,nvl(b.qty,0) as qty_sold,c.inv from
(select style_id,count(1) as wishlisted from 
(SELECT uidx,
                   style_id
            FROM (SELECT uidx,
                         cl.style_id,
                         action,
                         ROW_NUMBER() OVER (PARTITION BY uidx,cl.style_id ORDER BY added_datetime DESC) AS rnk
                  FROM bidb.collection_log_eors cl
                    JOIN dim_style dp ON cl.style_id = dp.style_id
                  WHERE uidx NOT LIKE '%myntra360.com%'
                  AND   uidx NOT LIKE '%eossbenchmarking%@gmail.com'
                  AND   uidx NOT LIKE '%scmloadtest%'
                  AND   uidx NOT LIKE '%sfloadtest%'
                  and   dp.brand='Puma'
                  and added_date>=20170622 and added_datetime>='2017-06-22 19:00:00')
            WHERE rnk = 1
            AND   action = 1)
            group by 1) a
            left join 
            (select style_id,sum(item_quantity) as qty 
            from fact_order_live fol 
            join dim_product dp on fol.sku_id=dp.sku_id 
            where (is_shipped=1 or is_realised=1) and dp.brand='Puma' and order_created_date>=20170623
            group by 1) b on a.style_id=b.style_id
            left join 
            (select style_id,max(atp.seller_id) as seller_id ,sum(net_inventory_count) as inv from o_atp_inventory atp join dim_product dp on atp.sku_id=dp.sku_id where dp.brand='Puma' group by 1) c
            on a.style_id=c.style_id; 
