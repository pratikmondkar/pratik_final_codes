SELECT CASE
         WHEN a.discount < 10 THEN '<10'
         WHEN a.discount BETWEEN 10 AND 19 THEN '10-20'
         WHEN a.discount BETWEEN 20 AND 29 THEN '20-30'
         WHEN a.discount BETWEEN 30 AND 39 THEN '30-40'
         WHEN a.discount BETWEEN 40 AND 49 THEN '40-50'
         WHEN a.discount BETWEEN 50 AND 59 THEN '50-60'
         WHEN a.discount BETWEEN 60 AND 69 THEN '60-70'
         WHEN a.discount >= 70 THEN '70+'
         ELSE 'unk'
       END AS discount_bucket,
       SUM(inv.inv) AS inv
FROM (SELECT style_id,
             MAX(nvl (discount_rule_percent,0)) AS discount
      FROM bidb.pricing_snapshot_history
      WHERE DATE BETWEEN 20170102 AND 20170105
      GROUP BY 1) a
  LEFT JOIN (SELECT fps.style_id,
                    SUM(inv) AS inv
             FROM dev.atp_snapshot_20160101 fps
             GROUP BY 1) inv ON a.style_id = inv.style_id
GROUP BY 1;

SELECT SUM(inv)
FROM dev.;

