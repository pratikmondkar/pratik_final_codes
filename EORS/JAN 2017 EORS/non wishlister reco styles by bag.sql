SELECT brand,
       article_type,
       gender,
       style_id,
       ros,
       rnk
FROM (SELECT ds.brand,
             ds.article_type,
             ds.gender,
             a.style_id,
             a.ros,
             b.inventory,
             ROW_NUMBER() OVER (PARTITION BY brand,article_type,gender ORDER BY ros DESC) AS rnk
      FROM (SELECT q.style_id,
                   nvl((q.qty::FLOAT8 / l.live_days),0) AS ros
            FROM (SELECT style_id,
                         SUM(quantity) qty
                  FROM fact_core_item
                  WHERE store_id = 1
                  AND   (is_shipped = 1 OR is_realised = 1)
                  AND   order_created_date BETWEEN 20170102 AND 20170119
                  GROUP BY 1) q
              JOIN (SELECT style_id,
                           COUNT(DISTINCT DATE) AS live_days
                    FROM fact_product_snapshot
                    WHERE DATE BETWEEN 20170102 AND 20170119
                    AND   is_live_on_portal = 1
                    GROUP BY 1) l ON q.style_id = l.style_id) a
        JOIN (SELECT atp.style_id,
                     SUM(inventory_count - blocked_order_count) AS inventory
              FROM bidb.fact_atp_inventory atp
              WHERE style_status = 'P'
              GROUP BY 1) b ON a.style_id = b.style_id
        JOIN dim_style ds ON a.style_id = ds.style_id)
WHERE rnk <= 50
AND   inventory >= 5
ORDER BY 1,
         2,
         3,
         6 LIMIT 1000
