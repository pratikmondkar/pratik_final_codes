--Hourly traffic 
SELECT TO_CHAR(load_datetime,'YYYYMMDD-HH24'),
       COUNT(distinct session_id) AS sessions,
       COUNT(DISTINCT nvl (uidx,device_id)) AS users
FROM clickstream.traffic_realtime
WHERE load_date BETWEEN 20170101 AND 20170105
GROUP BY 1;

--hourly revenue
SELECT TO_CHAR(CAST(order_created_date || ' ' || LPAD(order_created_time,6,0) AS TIMESTAMP),'YYYYMMDD-HH24'),
       COUNT(distinct order_group_id) AS orders,
       COUNT(DISTINCT idcustomer) AS customers,
       SUM(item_revenue_inc_cashback + nvl (shipping_charges) + nvl (gift_charges)) AS revenue
FROM bidb.fact_core_item
WHERE order_created_date BETWEEN 20170102 AND 20170105
AND   store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
GROUP BY 1;

--Discount bucket performance
SELECT lp.load_date,
				CASE
         WHEN a.discount < 10 THEN '<10'
         WHEN a.discount BETWEEN 10 AND 19 THEN '10-20'
         WHEN a.discount BETWEEN 20 AND 29 THEN '20-30'
         WHEN a.discount BETWEEN 30 AND 39 THEN '30-40'
         WHEN a.discount BETWEEN 40 AND 49 THEN '40-50'
         WHEN a.discount BETWEEN 50 AND 59 THEN '50-60'
         WHEN a.discount BETWEEN 60 AND 69 THEN '60-70'
         WHEN a.discount >= 70 THEN '70+'
         ELSE 'unk'
       END AS discount_bucket,
       SUM(lp.list_pages) AS lp,
       SUM(pdp.pdp_views) AS pdp,
       SUM(atc.add_to_carts) AS atc,
       SUM(qty.revenue) AS rev,
       SUM(qty.qty) AS qty,
       SUM(qty.cogs) AS cogs,
       COUNT(DISTINCT a.style_id) AS styles
FROM (SELECT style_id,
             MAX(nvl (discount_rule_percent,0)) AS discount
      FROM bidb.pricing_snapshot_history
      WHERE DATE BETWEEN 20170102 AND 20170105
      GROUP BY 1) a
  LEFT JOIN (SELECT load_date,
  									entity_id,
                    COUNT(1) AS list_pages
             FROM clickstream.widget_entity_view
             WHERE event_type = 'Product list loaded'
             AND   load_date BETWEEN 20170102 AND 20170105
             AND   atlas_ts > 1483367400000
             GROUP BY 1,2) lp ON a.style_id = lp.entity_id
  LEFT JOIN (SELECT load_date,
  									data_set_value,
                    COUNT(1) AS pdp_views
             FROM clickstream.events_view
             WHERE event_type = 'ScreenLoad'
             AND   screen_name LIKE 'Shopping Page-PDP%'
             AND   load_date BETWEEN 20170102 AND 20170105
             AND   atlas_ts > 148336740000
             GROUP BY 1,2) pdp ON a.style_id = pdp.data_set_value and lp.load_date=pdp.load_date
  LEFT JOIN (SELECT load_date,
  								  data_set_value,
                    COUNT(1) AS add_to_carts
             FROM clickstream.events_view
             WHERE event_type = 'addToCart'
             AND   load_date BETWEEN 20170102 AND 20170105
             AND   atlas_ts > 148336740000
             GROUP BY 1,2) atc ON a.style_id = atc.data_set_value and lp.load_date=atc.load_date
  LEFT JOIN (SELECT order_created_date,
  									style_id,
                    SUM(item_revenue_inc_cashback + nvl (shipping_charges,0) + nvl (gift_charges,0) - nvl (tax,0)) AS revenue,
                    SUM(quantity) AS qty,
                    sum(product_discount) as td,
                    sum(item_mrp_value*quantity) as mrp,
                    SUM(nvl (cogs,0) + nvl (royalty_commission,0) + nvl (stn_input_vat_reversal,0) + nvl (entry_tax,0)) AS cogs
             FROM fact_core_item
             WHERE order_created_date BETWEEN 20170102 AND 20170105
             AND   store_id = 1
             AND   (is_shipped = 1 OR is_realised = 1)
             GROUP BY 1,2) qty ON a.style_id = qty.style_id and lp.load_date=qty.order_created_date
GROUP BY 1,2;


--Purchase sequence performance
SELECT case when purchase_sequence > 10 then '10+' else purchase_sequence::varchar(255) end as purchase_sequence,
       CASE
         WHEN order_created_date BETWEEN 20160701 AND 20160704 THEN 'EORS 4'
         ELSE 'EORS 5'
       END AS eors_flag,
       COUNT(DISTINCT idcustomer) AS customers,
       SUM(revenue) AS revenue,
       SUM(qty) AS qty
FROM (SELECT *,
             ROW_NUMBER() OVER (PARTITION BY idcustomer ORDER BY order_dt) AS purchase_sequence
      FROM (SELECT idcustomer_idea AS idcustomer,
                   order_group_id::varchar(255),
                   order_created_date,
                   CAST(order_created_date || ' ' || LPAD(order_created_time,4,0) AS TIMESTAMP) AS order_dt,
                   SUM(shipped_order_revenue_inc_cashback) AS revenue,
                   SUM(shipped_item_count) AS qty
            FROM fact_order
            WHERE order_created_date <= 20161130
            AND   store_id = 1
            AND   (is_shipped = 1 OR is_realised = 1)
            GROUP BY 1,
                     2,3,4
            UNION ALL
            SELECT idcustomer,
                   order_group_id,
                   order_created_date,
                   CAST(order_created_date || ' ' || LPAD(order_created_time,6,0) AS TIMESTAMP) AS order_dt,
                   SUM(item_revenue_inc_cashback) AS revenue,
                   SUM(quantity) AS qty
            FROM fact_core_item
            WHERE order_created_date > 20161130
            AND   store_id = 1
            AND   (is_shipped = 1 OR is_realised = 1)
            GROUP BY 1,
                     2,3,4))
                     where (order_created_date between 20160701 and 20160704 or order_created_date between 20170102 and 20170105)
                     group by 1,2;

