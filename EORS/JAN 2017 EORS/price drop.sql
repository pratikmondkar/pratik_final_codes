SELECT DISTINCT b.uidx,
       listagg(b.style_id,','),
       listagg(b.new_price,',')
FROM (SELECT uidx,
             style_id,
             ROUND(priceb,0) AS new_price
      FROM (SELECT uidx,
                   style_id,
                   priceb,
                   ROW_NUMBER() OVER (PARTITION BY uidx ORDER BY nvl(disc_diff,0) DESC,curr_disc DESC) AS styl_rnk
            FROM (SELECT uidx,
                         a.style_id,
                         priceb,
                         curr_disc,
                         (curr_disc - added_disc) / NULLIF(added_disc,0) AS disc_diff
                  FROM (SELECT uidx,
                               style_id,
                               pricea,
                               added_disc
                        FROM (SELECT uidx,
                                     cl.style_id,
                                     action,
                                     article_mrp -(nvl (discount_percentage,0)::float8 / 100)*article_mrp AS pricea,
                                     nvl (discount_percentage,0) as added_disc,
                                     ROW_NUMBER() OVER (PARTITION BY uidx,cl.style_id ORDER BY added_datetime DESC) AS rnk
                              FROM bidb.collection_log_eors cl
                              left join dim_style ds on cl.style_id=ds.style_id
                              WHERE uidx NOT LIKE '%myntra360.com%'
                              AND   uidx NOT LIKE '%eossbenchmarking%@gmail.com'
                              AND   uidx NOT LIKE '%scmloadtest%'
                              AND   uidx NOT LIKE '%sfloadtest%')
                        WHERE rnk = 1
                        AND   action = 1) a
                    JOIN (SELECT DISTINCT style_id
                               FROM fact_atp_inventory inv
                               WHERE style_status = 'P'
                               AND   inventory_count - blocked_order_count > 3) y ON y.style_id = a.style_id
                    JOIN (SELECT DISTINCT style_id,
                                 article_mrp -(nvl (discount_rule_percent,0)::DECIMAL(10,2) / 100)*article_mrp AS priceb,
                                 nvl(discount_rule_percent,0) AS curr_disc
                          FROM bidb.pricing_snapshot) z ON a.style_id = z.style_id
                  GROUP BY 1,
                           2,
                           3,
                           4,
                           5)) c
      WHERE styl_rnk <= 10) b
GROUP BY 1
limit 1000
