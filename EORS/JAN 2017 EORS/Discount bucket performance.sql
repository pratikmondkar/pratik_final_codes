SELECT CASE
         WHEN a.discount < 10 THEN '<10'
         WHEN a.discount BETWEEN 10 AND 19 THEN '10-20'
         WHEN a.discount BETWEEN 20 AND 29 THEN '20-30'
         WHEN a.discount BETWEEN 30 AND 39 THEN '30-40'
         WHEN a.discount BETWEEN 40 AND 49 THEN '40-50'
         WHEN a.discount BETWEEN 50 AND 59 THEN '50-60'
         WHEN a.discount BETWEEN 60 AND 69 THEN '60-70'
         WHEN a.discount >= 70 THEN '70+'
         ELSE 'unk'
       END AS discount_bucket,
       SUM(lp.list_pages) AS lp,
       SUM(pdp.pdp_views) AS pdp,
       SUM(atc.add_to_carts) AS atc,
       SUM(qty.revenue) AS rev,
       SUM(qty.qty) AS qty,
       SUM(qty.cogs) AS cogs,
       COUNT(DISTINCT a.style_id) AS styles
FROM (SELECT style_id,
             MAX(nvl (discount_rule_percent,0)) AS discount
      FROM bidb.pricing_snapshot_history
      WHERE DATE BETWEEN 20170102 AND 20170105
      GROUP BY 1) a
  LEFT JOIN (SELECT entity_id,
                    COUNT(1) AS list_pages
             FROM clickstream.widget_entity_view
             WHERE event_type = 'Product list loaded'
             AND   load_date BETWEEN 20170102 AND 20170105
             AND   atlas_ts > 1483367400000
             GROUP BY 1) lp ON a.style_id = lp.entity_id
  LEFT JOIN (SELECT data_set_value,
                    COUNT(1) AS pdp_views
             FROM clickstream.events_view
             WHERE event_type = 'ScreenLoad'
             AND   screen_name LIKE 'Shopping Page-PDP%'
             AND   load_date BETWEEN 20170102 AND 20170105
             AND   atlas_ts > 148336740000
             GROUP BY 1) pdp ON a.style_id = pdp.data_set_value
  LEFT JOIN (SELECT data_set_value,
                    COUNT(1) AS add_to_carts
             FROM clickstream.events_view
             WHERE event_type = 'addToCart'
             AND   load_date BETWEEN 20170102 AND 20170105
             AND   atlas_ts > 148336740000
             GROUP BY 1) atc ON a.style_id = atc.data_set_value
  LEFT JOIN (SELECT style_id,
                    SUM(item_revenue_inc_cashback + nvl (shipping_charges,0) + nvl (gift_charges,0) - nvl (tax,0)) AS revenue,
                    SUM(quantity) AS qty,
                    sum(product_discount) as td,
                    sum(item_mrp_value*quantity) as mrp,
                    SUM(nvl (cogs,0) + nvl (royalty_commission,0) + nvl (stn_input_vat_reversal,0) + nvl (entry_tax,0)) AS cogs
             FROM fact_core_item
             WHERE order_created_date BETWEEN 20170102 AND 20170105
             AND   store_id = 1
             AND   (is_shipped = 1 OR is_realised = 1)
             GROUP BY 1) qty ON a.style_id = qty.style_id 
GROUP BY 1
