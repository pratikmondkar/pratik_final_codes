SELECT 20170623 AS DATE,
       16 AS WEEK,
       'JUNE' AS MONTH,
       2017 AS year,
       i.style_id,
       SUM(inv) AS inv,
       SUM(nvl (disc,0)) AS disc,
       SUM(nvl (mrp,0)) AS mrp,
       SUM(nvl (qty,0)) AS qty
FROM (SELECT style_id,
             inv
      FROM dev.atp_snapshot_20170623) i
  LEFT JOIN (SELECT style_id,
                    SUM(disc)  as disc,
                    SUM(mrp) AS mrp,
                    SUM(qty) AS qty
             FROM (SELECT dp.style_id,
                          SUM(product_discount + coupon_discount) AS disc,
                          SUM(item_mrp_value*quantity) AS mrp,
                          SUM(quantity) AS qty
                   FROM fact_core_item fci
                     JOIN dim_product dp ON fci.sku_id = dp.sku_id
                   WHERE cast(order_created_date ||' '|| lpad(order_created_time,6,0) as timestamp) > '2017-06-23 19:40:00' 
                   AND order_created_date<=TO_CHAR(convert_timezone ('Asia/Calcutta',SYSDATE) -INTERVAL '1 day','YYYYMMDD')::bigint
				   AND order_created_date<=20170626
                   AND   (is_shipped = 1 OR is_realised = 1)
                   and 		store_id=1
                   GROUP BY 1
                   UNION
                   SELECT dp.style_id,
                          SUM(trade_discount + coupon_discount) AS disc,
                          SUM(item_mrp*item_quantity) AS mrp,
                          SUM(item_quantity) AS qty
                   FROM fact_order_live fol
                     JOIN dim_product dp ON fol.sku_id = dp.sku_id
                   WHERE (is_shipped = 1 OR is_realised = 1)
                   and order_created_date=TO_CHAR(convert_timezone ('Asia/Calcutta',SYSDATE),'YYYYMMDD')::bigint
				   and cast(order_created_date ||' '|| lpad(order_created_time,4,0) as timestamp) > '2017-06-23 19:00:00'
				   AND order_created_date<=20170626
                   GROUP BY 1)
                   group by 1) s ON i.style_id = s.style_id
  LEFT JOIN dim_style dp ON i.style_id = dp.style_id
GROUP BY 1,
         2,
         3,
         4,
         5
         limit 100;


create table dev.atp_snapshot_20171221 distkey(style_id) as 
(SELECT style_id,
       SUM(CASE WHEN blocked_order_count > inventory_count THEN 0 ELSE inventory_count - blocked_order_count END) AS inv
FROM fact_atp_inventory
group by 1);


select article_type ,count(*) from dim_product group by 1
