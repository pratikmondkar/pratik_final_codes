drop table if exists dev.dev.d30_user_top_bags;

create table dev.d30_user_top_bags as (
SELECT a.*
FROM (SELECT uidx,
             brand,
             article_type,
             gender
      FROM (SELECT user_id as uidx,
                   brand,
                   article_type,
                   gender,
                   SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                   ROW_NUMBER() OVER (PARTITION BY uidx ORDER BY score DESC) AS rnk
            FROM bidb.notif_person_base_lifetime
            WHERE identifier='uidx'
            GROUP BY 1,
                     2,
                     3,
                     4)
      WHERE rnk <= 3) a
  JOIN (SELECT DISTINCT uidx
        FROM (SELECT uidx
              FROM clickstream.daily_notif_aggregates
              WHERE load_date >= TO_CHAR(SYSDATE-INTERVAL '30 days','YYYYMMDD')::BIGINT
              UNION ALL
              SELECT uidx
              FROM clickstream.daily_aggregates
              WHERE load_date >= TO_CHAR(SYSDATE-INTERVAL '30 days','YYYYMMDD')::BIGINT) l) b ON a.uidx = b.uidx
  LEFT JOIN (SELECT DISTINCT uidx
             FROM (SELECT uidx,
                          cl.style_id,
                          action,
                          ROW_NUMBER() OVER (PARTITION BY uidx,cl.style_id ORDER BY added_datetime DESC) AS rnk
                   FROM bidb.collection_log_eors cl
                     LEFT JOIN bidb.dim_style ds ON cl.style_id = ds.style_id
                   WHERE uidx NOT LIKE '%myntra360.com%'
                   AND   uidx NOT LIKE '%eossbenchmarking%@gmail.com'
                   AND   uidx NOT LIKE '%scmloadtest%'
                   AND   uidx NOT LIKE '%sfloadtest%')
             WHERE rnk = 1
             AND   action = 1) c ON a.uidx = c.uidx
WHERE c.uidx IS NULL)
