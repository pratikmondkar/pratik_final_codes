SELECT a.*
FROM (SELECT idcustomer,
             email,
             customer_login AS uidx,
             SUM(nvl (product_discount,0) + nvl (coupon_discount,0)) AS savings,
             SUM(item_mrp_value*quantity) AS mrp_total
      FROM fact_core_Item fci
        JOIN dim_customer_idea dci ON fci.idcustomer = dci.id
        JOIN cii.dim_customer_email dce ON dce.uid = dci.id
      WHERE order_created_date BETWEEN 20160702 AND 20160704
      AND   store_id = 1
      AND   is_realised = 1
      GROUP BY 1,
               2,
               3) a
  left JOIN (SELECT DISTINCT uidx
        FROM (SELECT uidx
              FROM clickstream.daily_notif_aggregates
              WHERE load_date >= TO_CHAR(SYSDATE-INTERVAL '30 days','YYYYMMDD')::BIGINT
              UNION ALL
              SELECT uidx
              FROM clickstream.daily_aggregates
              WHERE load_date >= TO_CHAR(SYSDATE-INTERVAL '30 days','YYYYMMDD')::BIGINT) l) b ON a.uidx = b.uidx
              where b.uidx is  null;
              
select * from stv_inflight;
