SELECT SUM(CASE WHEN units > inv THEN inv*price ELSE units*price END) AS revenue
FROM (SELECT a.style_id,
             COUNT(DISTINCT a.uidx) AS units
      FROM (SELECT uidx,
                   style_id
            FROM (SELECT uidx,
                         cl.style_id,
                         action,
                         ROW_NUMBER() OVER (PARTITION BY uidx,cl.style_id ORDER BY added_datetime DESC) AS rnk
                  FROM bidb.collection_log_eors cl
                    JOIN dim_style dp ON cl.style_id = dp.style_id
                  WHERE uidx NOT LIKE '%myntra360.com%'
                  AND   uidx NOT LIKE '%eossbenchmarking%@gmail.com'
                  AND   uidx NOT LIKE '%scmloadtest%'
                  AND   uidx NOT LIKE '%sfloadtest%')
            WHERE rnk = 1
            AND   action = 1) a
        LEFT JOIN (SELECT customer_Login AS uidx,
                          dp.style_id
                   FROM fact_order_live fci
                     JOIN dim_product dp ON dp.sku_id = fci.sku_id
                   WHERE (is_shipped = 1 OR is_realised = 1)
                   AND   order_created_date >= 20170102
                   GROUP BY 1,
                            2) b
               ON a.uidx = b.uidx
              AND a.style_id = b.style_id
          JOIN (SELECT style_id,
               SUM(inventory_count + blocked_order_count) AS inv
        FROM o_atp_inventory at
          JOIN dim_product dp ON at.sku_id = dp.sku_id
        GROUP BY 1) atp ON a.style_id = atp.style_id
      WHERE b.uidx IS NULL
      AND   b.style_id IS NULL
      AND   atp.inv > 0
      GROUP BY 1) u
  JOIN (SELECT style_id,
               SUM(inventory_count - blocked_order_count) AS inv
        FROM o_atp_inventory at
          JOIN dim_product dp ON at.sku_id = dp.sku_id
        GROUP BY 1) atp2 ON u.style_id = atp2.style_id
   join (select style_id, min(article_mrp-article_mrp*nvl(discount_rule_percent::float8/100,0)) as price from pricing_snapshot group by 1) ps on u.style_id=ps.style_id
   


