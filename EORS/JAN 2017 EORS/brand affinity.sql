SELECT a.device_id,
       nvl(first_name,'Customer') as name,
       brand,
       CASE
         WHEN b.brand IS NOT NULL THEN 1
         ELSE 0
       END AS brand_affinity_flag,
       CASE
         WHEN b.brand IS NULL THEN 1
         ELSE 0
       END AS others_flag
FROM (SELECT device_id,max(dci.first_name) as first_name
      FROM (SELECT device_id,
                   uidx
            FROM clickstream.daily_aggregates
            WHERE load_date >= TO_CHAR(SYSDATE-INTERVAL '30 days','YYYYMMDD')::BIGINT
            UNION ALL
            SELECT device_id,
                   uidx
            FROM clickstream.daily_notif_aggregates
            WHERE load_date >= TO_CHAR(SYSDATE-INTERVAL '30 days','YYYYMMDD')::BIGINT) u
            left join dim_customer_idea dci on u.uidx=dci.customer_login
      GROUP BY 1) a
  LEFT JOIN (SELECT DISTINCT device_id,
                    brand
             FROM (SELECT DISTINCT device_id,
                          brand,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_personalize
                   WHERE load_date >= TO_CHAR(SYSDATE-INTERVAL '15 days','YYYYMMDD')::BIGINT
                   GROUP BY 1,
                            2)
             WHERE rnk = 1) b ON a.device_id = b.device_id limit 10000;
             

SELECT count(distinct device_id),
count(DISTINCT case when first_name is not null then device_id end)
                   FROM bidb.notif_personalize
                   WHERE load_date >= TO_CHAR(SYSDATE-INTERVAL '15 days','YYYYMMDD')::bigint

