SELECT DISTINCT a.*,
       nvl(first_name,'Customer') AS name,
       CASE
         WHEN c.uidx IS NOT NULL THEN 1
         ELSE 0
       END AS atwl_flag
FROM (SELECT device_id,
             MAX(uidx) AS uidx
      FROM (SELECT device_id,
                   uidx
            FROM clickstream.daily_aggregates
            WHERE load_date >= TO_CHAR(SYSDATE-INTERVAL '30 days','YYYYMMDD')::BIGINT
            UNION ALL
            SELECT device_id,
                   uidx
            FROM clickstream.daily_notif_aggregates
            WHERE load_date >= TO_CHAR(SYSDATE-INTERVAL '30 days','YYYYMMDD')::BIGINT)
      GROUP BY 1) a
  LEFT JOIN (SELECT uidx
             FROM (SELECT uidx,
                          article_type,
                          COUNT(DISTINCT cl.style_id) AS styles,
                          ROW_NUMBER() OVER (PARTITION BY uidx ORDER BY styles DESC) AS rnk
                   FROM bidb.collection_log_eors cl
                     LEFT JOIN dim_style ds ON cl.style_id = ds.style_id
                   GROUP BY 1,
                            2)
             WHERE rnk = 1 and action = 1) c ON a.uidx = c.uidx
  LEFT JOIN dim_customer_idea dci ON a.uidx = dci.customer_login

