
# coding: utf-8

# In[1]:

import pandas as pd
from pandas import DataFrame
import sqlalchemy as sq
import pickle
import numpy as np 
import scipy as scipy
from scipy import stats
from sklearn import linear_model, metrics, preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor
import xgboost as xgb
from xgboost import XGBRegressor
import multiprocessing
from datetime import date, timedelta
import os
import gc
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

engine = sq.create_engine("postgresql+psycopg2://customer_insights_ddl:Insight@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")


# In[2]:

data=pd.read_csv('/data/pratik/tensor/datasets/ad_yday_validation.csv',error_bad_lines=False)


# In[ ]:

data=pre_data[pre_data['date']>=20180206]


# In[3]:

sql_str="""
select ([brand]+[article_type]+[product_gender]) as bag_id, brand, article_type,product_gender as gender,business_unit 
from fact_category_over_view_metrics 
where date= to_char(sysdate- interval '1 day','YYYYMMDD')::bigint and is_live_style=1 
group by brand, article_type, product_gender, business_unit
"""
bu_map=pd.read_sql_query(sql_str,engine)
bu_map.drop_duplicates('bag_id',inplace=True)


# In[4]:

bag_list=pd.read_csv('/data/pratik/tensor/bag_list_2000.csv',error_bad_lines=False)['bag'].tolist()


# In[5]:

lst=data[['brand','article_type','gender']].drop_duplicates().to_records(index=False).tolist()


# In[ ]:

data['lc_share_pltf'].sum()


# In[7]:

compete=pd.read_csv('/data/pratik/tensor/trends/google_trends.csv',error_bad_lines=False)
compete['comp_index']=compete['Myntra']/compete['Amazon']
compete['date']=compete['date'].str.replace('-','').astype(int)
compete.drop_duplicates('date',inplace=True)
data['comp_index']=compete.comp_index.mean()
data['similar_brands_max']=data['price_cannib_mean']


# In[8]:

data.columns


# In[9]:

ind_list=['wgt_input_td','input_td_diff','lc_share_pltf','live_styles','presale_flag','postsale_flag',
          'sessions','brokeness','freshness','output_cd','loyalty_points','vis_cannib_mean','comp_index','sales_lag',
          'index_week_year','price_cannib_mean','brokenness_rm','freshness_rm','atc_rm']


# In[10]:

def gen_model_error(lst):
    b=lst[0]
    a=lst[1]
    g=lst[2]
    path_o=r'/data/pratik/tensor/models/'
    path_n=r'/data/pratik/tensor/retrained_models/'
    filename=b+'-'+a+'-'+g
    bag=b+a+g   
    dat=data[data['bag_id']==bag]
    dat1=dat[ind_list_old]
    dat2=dat[ind_list]
    with open(path_o+filename+'.pkl', 'rb') as f:
        model_o = pickle.load(f)
        dat['ros_o']=model_o.predict(dat1)
    with open(path_n+filename+'.pkl', 'rb') as f:
        model_n = pickle.load(f)
    dat['ros_n']=model_n.predict(dat2)        
    dat['bag_id']=bag
    print "Model error for "+bag+" done." 
    return dat


# In[11]:

p = multiprocessing.Pool(10)
ros_pred=p.map(gen_model_error, lst)
p.close()
predictions = pd.concat(ros_pred)


# In[13]:

# predictions['ros_o']=predictions['ros_o'].round().astype(int)
predictions['ros_n']=predictions['ros_n'].round().astype(int)
check=data[['date','bag_id','qty_sold']].merge(predictions[['date','bag_id','ros_o','ros_n']],how='left',on=['bag_id','date'])
model_error=check.merge(bu_map,how='left',on='bag_id')
model_error['model_error_old']=model_error['ros_o'] - model_error['qty_sold']
model_error['model_error_new']=model_error['ros_n'] - model_error['qty_sold']


# In[ ]:

model_error.sort_values(by='model_error_old',ascending=False).head()


# In[14]:

bu_summary=model_error[model_error['ros_o'].isnull()==False].groupby('business_unit').sum().reset_index()
bu_summary['model_error_old']=(bu_summary['ros_o']-bu_summary['qty_sold'])*100/bu_summary['qty_sold']
bu_summary['model_error_new']=(bu_summary['ros_n']-bu_summary['qty_sold'])*100/bu_summary['qty_sold']
bu_summary['model_error_old']=bu_summary['model_error_old'].round(2)
bu_summary['model_error_new']=bu_summary['model_error_new'].round(2)


# In[15]:

bu_summary


# In[16]:

brand_summary=model_error[model_error['ros_o'].isnull()==False].groupby('brand').sum().reset_index()
brand_summary['model_error_old']=(brand_summary['ros_o']-brand_summary['qty_sold'])*100/brand_summary['qty_sold']
brand_summary['model_error_new']=(brand_summary['ros_n']-brand_summary['qty_sold'])*100/brand_summary['qty_sold']
brand_summary['model_error_old']=brand_summary['model_error_old'].round(2)
brand_summary['model_error_new']=brand_summary['model_error_new'].round(2)
top_25Brands=brand_summary.sort_values(by='qty_sold',ascending=False).head(n=25)


# In[ ]:

top_25Brands


# In[ ]:

a1=bu_summary.to_html(index=False)
a2=top_25Brands.to_html(index=False)

t1 ="Hi,\nPlease find below the performance and the BAG level response models .\n \nBU level summary:\n"
t2 ="Top 25 brands summary:\n"


# In[ ]:

outpath = '/home/pratik/tmp/'
abc = date.today().strftime('%d-%b-%Y')
filename2=outpath+ "bag_validations_"+str(abc)+".csv"
model_error.to_csv(filename2,index=False)

sender = 'pratik.mondkar@myntra.com'
#receivers = ['shrinivas.ron@myntra.com','pratik.mondkar@myntra.com','sahil.mahajan@myntra.com']
receivers = ['pratik.mondkar@myntra.com']

msg = MIMEMultipart()
msg['Subject'] = 'BAG level response model validation report'
msg['From'] =sender
msg['to'] =", ".join(receivers)

part1 = MIMEText(t1,'plain')
part2 = MIMEText(a1,'html')
part3 = MIMEText(t2,'plain')
part4 = MIMEText(a2,'html')

msg.attach(part1)
msg.attach(part2)
msg.attach(part3)
msg.attach(part4)

f2 = file(filename2)
attachment2 = MIMEText(f2.read())
attachment2.add_header('Content-Disposition', 'attachment', filename=filename2)
msg.attach(attachment2)


try:
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login("pratik.mondkar@myntra.com", "kaxuftanzoyzmjfe")
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"

