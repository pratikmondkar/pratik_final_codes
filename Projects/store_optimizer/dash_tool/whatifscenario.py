

import pandas as pd
from pandas import DataFrame
import numpy as np
import pickle
import math
import time
import scipy as scipy
from scipy import stats
from sklearn import linear_model, metrics, preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor
from sklearn import svm
import xgboost as xgb
from xgboost import XGBRegressor
from modelselector import EstimatorSelectionHelper
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table_experiments as dt
import plotly.graph_objs as go
import plotly.plotly as py
import plotly.figure_factory as ff

from app import app

#app = dash.Dash()

bag_list=pd.read_csv('/data/pratik/tensor/b_a_g_list.csv')

bag_list=bag_list[(bag_list['article_type']!='0') & (bag_list['gender']!='0') & (bag_list['brand']!='0') ]

#with open('/data/pratik/tensor/summary/models.pkl', 'rb') as f:
#    model = pickle.load(f)

init_df=pd.DataFrame({'output_td':np.nan,'output_td_diff':np.nan,'lc_share_pltf':np.nan,'live_styles':np.nan,'presale_flag':np.nan,'sessions':np.nan,'brokeness':np.nan,'freshness':np.nan},index=[0])

layout = html.Div([
    html.Div([

        html.Div([
	    html.Label('Article Type'),
            dcc.Dropdown(
                id='filter-article_type-wi',
		options=[{'label': i, 'value': i} for i in sorted(list(bag_list['article_type'].unique()))],            
		value='Tshirts'
            )
        ],
        style={'width': '33%', 'display': 'inline-block'}),

        html.Div([
	    html.Label('Gender'),
            dcc.Dropdown(
                id='filter-gender-wi',
                options=[{'label': i, 'value': i} for i in sorted(list(bag_list['gender'].unique()))],
		value='Men'
            )
        ], style={'width': '33%', 'float': 'center', 'display': 'inline-block'}),
        html.Div([
        html.Label('Brand'),
            dcc.Dropdown(
                id='filter-brand-wi',
                options=[{'label': i, 'value': i} for i in sorted(list(bag_list['brand'].unique()))],
        value='Roadster'
            )
    ], style={'width': '33%', 'float': 'right', 'display': 'inline-block'})
    ]
        , style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(250, 250, 250)',
        'padding': '10px 10px 10px'
    }),

#        html.Hr(),

        html.Div([

        html.Div([
        html.Label('Trade discount'),
        html.Br(),
            dcc.Input(
                id='td-input',  
                placeholder='Enter a value...',
		inputmode='numeric', 
                type='number',       
                value=''
                )
            ], style={'width': '15%','float': 'center', 'display': 'inline-block','padding': '0px 0px 0px 5px'}),

        html.Div([
        html.Label('TD differential'),
        html.Br(),
            dcc.Input(
                id='td_diff-input',
                placeholder='ratio with 15days', 
                inputmode='numeric',
		type='number',       
                value=''
                )
            ], style={'width': '15%', 'float': 'center', 'display': 'inline-block','padding': '0px 0px 0px 5px'}),
        html.Div([
        html.Label('Visibility'),
        html.Br(),
            dcc.Input(
                id='visibility-input',
                placeholder='Share of LCs', 
                inputmode='numeric',
		type='number',       
                value=''
                )
            ], style={'width': '15%', 'float': 'center', 'display': 'inline-block','padding': '0px 0px 0px 5px'}),
        html.Div([
        html.Label('# of live styles'),
        html.Br(),
            dcc.Input(
                id='live_styles-input',
                placeholder='Share of LCs',
                inputmode='numeric',
		type='number',
                value=''
                )
            ], style={'width': '15%', 'float': 'center', 'display': 'inline-block','padding': '0px 0px 0px 5px'}), 

        html.Div([
        html.Label('Traffic'),
        html.Br(),
            dcc.Input(
                id='traffic-input',
                placeholder='# of sessions', 
                inputmode='numeric',
		type='number',       
                value=''
                )
            ], style={'width': '15%', 'float': 'center', 'display': 'inline-block','padding': '0px 0px 0px 5px'}),
        html.Div([
            dcc.Checklist(
                id='checkbox-presale',
                options=[{'label': 'Presale', 'value': 1}],
                values=[],
                labelStyle={'display': 'inline-block'}
                )
            ], style={'width': '15%', 'float': 'center', 'display': 'inline-block','padding': '0px 0px 0px 5px'})
        ]
        , style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(250, 250, 250)',
        'padding': '10px 10px'
    }),

    html.Hr(),

        html.Div([

        html.Div([
        html.Label('Freshness'),
            dcc.Slider(
                id='freshness-slider',
                min=0,
                max=1,
                step=None,   
                marks={str(i):str(int(i*100)) for i in np.arange(0, 1.05, .05)},         
                value=0.6
                )
            ], style={'width': '45%', 'display': 'inline-block'}),

        html.Div([
        html.Label('Brokenness'),
            dcc.Slider(
                id='brokeness-slider',
                min=0,
                max=1,
                step=None,
                marks={str(i): str(int(i*100)) for i in np.arange(0, 1.05, .05)},
                value=0.3
                )
            ], style={'width': '45%', 'float': 'right', 'display': 'inline-block','padding': '0px 0px 0px 20px'})
        ]
        , style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(250, 250, 250)',
        'padding': '10px 10px'
    }),

    html.Hr(),


    html.Div([
	dt.DataTable(
        	rows=init_df.to_dict('records'),
        	row_selectable=True,
        	filterable=True,
        	sortable=True,
        	selected_row_indices=[],
        	id='input-table-wi'
    	)	

# dcc.Link('Go to Pricing elasticity', href='/pricing')
     ], style={'width': '98%', 'display': 'inline-block', 'padding': '0 20'})
])



@app.callback(
    dash.dependencies.Output('filter-gender-wi', 'options'),
    [dash.dependencies.Input('filter-article_type-wi', 'value')])
def set_gender_options(selected_at):
    return [{'label': i, 'value': i} for i in sorted(list(bag_list[bag_list['article_type']==selected_at]['gender'].unique())) ]

@app.callback(
    dash.dependencies.Output('filter-gender-wi', 'value'),
    [dash.dependencies.Input('filter-gender-wi', 'options')])
def set_gender_value(available_options):
    return available_options[0]['value']

@app.callback(
    dash.dependencies.Output('filter-brand-wi', 'options'),
    [dash.dependencies.Input('filter-article_type-wi', 'value'),
     dash.dependencies.Input('filter-gender-wi', 'value')])
def set_brand_options(selected_at,selected_gender):
    return [{'label': i, 'value': i} for i in sorted(list(bag_list[(bag_list['article_type']==selected_at) & (bag_list['gender']==selected_gender)]['brand'].unique())) ]

@app.callback(
    dash.dependencies.Output('filter-brand-wi', 'value'),
    [dash.dependencies.Input('filter-brand-wi', 'options')])
def set_brand_value(available_options):
    return available_options[0]['value']


@app.callback(
     dash.dependencies.Output('input-table-wi', 'rows'),
     [dash.dependencies.Input('filter-brand-wi', 'value'),
      dash.dependencies.Input('filter-article_type-wi', 'value'),
      dash.dependencies.Input('filter-gender-wi', 'value'),
      dash.dependencies.Input('td-input', 'value'),
      dash.dependencies.Input('td_diff-input', 'value'),
      dash.dependencies.Input('visibility-input', 'value'),
      dash.dependencies.Input('live_styles-input', 'value'),
      dash.dependencies.Input('checkbox-presale', 'values'),
      dash.dependencies.Input('traffic-input', 'value'),
      dash.dependencies.Input('freshness-slider', 'value'),
      dash.dependencies.Input('brokeness-slider', 'value')])
def generate_table(brand,article_type,gender,td,td_diff,vis,ls,ps,tra,fr,br):
	p=len(ps)
	path=r'/data/pratik/tensor/models/'
	filename=brand+'-'+article_type+'-'+gender
    	with open(path+filename+'.pkl', 'rb') as f:
        	model = pickle.load(f)
    	dataframe= pd.DataFrame({'output_td':td,'output_td_diff':td_diff,'lc_share_pltf':vis,'live_styles':ls,'presale_flag':p,'sessions':tra,'brokeness':br,'freshness':fr},index=[0])
	bag=brand+article_type+gender
	dataframe.replace('',np.nan,inplace=True)
	dataframe=dataframe.convert_objects(convert_numeric=True)
	if dataframe.isnull().any().any()==False:
		dataframe['ROS']=model.predict(dataframe[['output_td','output_td_diff','lc_share_pltf','live_styles','presale_flag','sessions','brokeness','freshness']])
    	return dataframe.to_dict('records')


# @app.callback(
#     dash.dependencies.Output('crossfilter-indicator-scatter', 'figure'),
#     [dash.dependencies.Input('filter-date', 'value'),
#      dash.dependencies.Input('filter-article_type', 'value'),
#      dash.dependencies.Input('filter-gender', 'value'),
#      dash.dependencies.Input('filter-brand', 'value'),
#      dash.dependencies.Input('filter-disc-slider', 'value')])

# def update_graph(date, article_type,
#                  gender, brand,
#                  disc):
#     bag=brand+article_type+gender
#     dat = df[(df['date'] == date) & (df['article_type'] == article_type) & (df['gender'] == gender) & (df['brand'] == brand)]
#     dat['output_td']=disc
#     dat['output_td_diff']=disc/dat['output_td_rm']
#     v=np.random.normal(df[df['bag_id']==bag]['lc_share_pltf'].mean(),df[df['bag_id']==bag]['lc_share_pltf'].std(),100)
#     dff=pd.DataFrame()
#     dff=dff.append([dat]*len(v),ignore_index=True)
#     dff['lc_share_pltf']=v
#     dff=dff[dff['lc_share_pltf']>0]
#     dff['ros']=model[bag].predict(dff[['output_td','output_td_diff','lc_share_pltf','live_styles','presale_flag','sessions','brokeness','freshness']])

#     return {
#         'data': [go.Scatter(
#             x=dff['lc_share_pltf'],
#             y=dff['ros'],
#             mode='markers',
#             marker={
#                 'size': 15,
#                 'opacity': 0.5,
#                 'line': {'width': 0.5, 'color': 'white'}
#             }
#         )],
#         'layout': go.Layout(
#             xaxis={
#                 'title': 'Visibity (share of list counts)'
#             },
#             yaxis={
#                 'title': 'Units sold (predicted)'
#             },
#             margin={'l': 40, 'b': 30, 't': 10, 'r': 0},
#             height=450,
#             hovermode='closest'
#         )
#     }

#ap=app.server

#if __name__ == '__main__':
#    ap.run()
