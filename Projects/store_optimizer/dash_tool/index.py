import pandas as pd
from pandas import DataFrame
import pickle
import numpy as np
import math
import time
import scipy as scipy
from scipy import stats
from sklearn import linear_model, metrics, preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor
from sklearn import svm
import xgboost as xgb
from xgboost import XGBRegressor
from modelselector import EstimatorSelectionHelper
from dash.dependencies import Input, Output
import dash_table_experiments as dt
import dash_core_components as dcc
import dash_html_components as html

from app import app,server
import toc, visibility, pricing, whatifscenario, optimization

#df=pd.read_csv('/data/pratik/tensor/tool_data.csv',error_bad_lines=False)

#df=df[(df['brand']!='0') | (df['brand']!='0') | (df['brand']!='0') ]

#with open('/data/pratik/tensor/summary/models.pkl', 'rb') as f:
#    model = pickle.load(f)

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(dt.DataTable(rows=[{}]), style={'display': 'none'}),
	html.Div(
        dcc.Tabs(
            tabs=[
                {'label': 'Table of Contents', 'value': 1},
                {'label': 'Visibility Elasticity', 'value': 2},
                {'label': 'Pricing Elasticity', 'value': 3},
                {'label': 'What if Scenario', 'value': 4},
		{'label': 'Optimization', 'value': 5}
            ],
            value=1,
            id='tabs',
            vertical=True,
                style={
                    'height': '100vh',
                    'borderRight': 'thin lightgrey solid',
                    'textAlign': 'left'
                }
            ),
            style={'width': '20%', 'float': 'left'}
        ),
        html.Div(html.Div(id='tab-output'),style={'width': '80%', 'float': 'right'})
#     html.Div(dt.DataTable, style={'display': 'none'})

#    html.Div(id='page-content')
],style={
        'fontFamily': 'Sans-Serif',
        'margin-left': 'auto',
        'margin-right': 'auto',
    }	
)

@app.callback(Output('url', 'pathname'),
              [Input('tabs', 'value')])
def display_page(value):
    if   value == 2:
         return '/visibility'
    elif value == 3:
         return '/pricing'
    elif value == 4:
         return '/whatif'
    elif value == 5:
	 return '/opt'
    else:
        return '/'


@app.callback(Output('tab-output', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/visibility':
         return visibility.layout
    elif pathname == '/pricing':
         return pricing.layout
    elif pathname=='/whatif':
         return whatifscenario.layout
    elif pathname=='/opt':
	 return optimization.layout
    else:
        return toc.layout



if __name__ == '__main__':
    server.run()
