

import dash
import dash_core_components as dcc
import dash_html_components as html

from app import app

colors = {
    'background': '#FFF8DB',
    'text': '#314036'
}

layout= html.Div(style={'backgroundColor': colors['background'],'height':'100%'}, children=[
	html.H2(children='Pricing Visibility Framework', style={
            'textAlign': 'center',
            'color': colors['text']
        }),
	html.H3(children='List of apps',style={
            'textAlign': 'left',
            'color': colors['text']
        }),
	dcc.Link('Visibility Elasticity', href='/visibility'),
    	html.Br(),
    	dcc.Link('Pricing Elasticity', href='/pricing'),
    	html.Br(),
    	dcc.Link('What if Scenario', href='/whatif'),
])
