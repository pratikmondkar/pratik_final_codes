import pandas as pd
from pandas import DataFrame
import pickle
import numpy as np 
import math
import time
import scipy as scipy
from scipy import stats
from sklearn import linear_model, metrics, preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor
from sklearn import svm
import xgboost as xgb
from xgboost import XGBRegressor
from modelselector import EstimatorSelectionHelper
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import plotly.plotly as py
import plotly.figure_factory as ff

from app import app

df=pd.read_csv('/data/pratik/tensor/datasets/ad_clean_2k_inp.csv',error_bad_lines=False)

#df=df[(df['brand']!='0') | (df['brand']!='0') | (df['brand']!='0') ]

with open('/data/pratik/tensor/datasets/ovr_dist_inp.pkl', 'rb') as f:
    ovr_dist = pickle.load(f)

ind_list=['wgt_input_td','input_td_diff','lc_share_pltf','live_styles','presale_flag','postsale_flag',
          'sessions','brokeness','freshness','output_cd','loyalty_points','vis_cannib_mean','comp_index','sales_lag',
          'index_week_year','price_cannib_mean','brokenness_rm','freshness_rm','atc_rm']


layout = html.Div([
    html.Div([

        html.Div([
	    html.Label('Date'),
            dcc.Dropdown(
                id='filter-date-pri',
		options=[{'label': i, 'value': i} for i in sorted(list(df['date'].unique()))],
                value=20170602
            ),
	    html.Label('Article Type'),
            dcc.Dropdown(
                id='filter-article_type-pri',
		options=[{'label': i, 'value': i} for i in sorted(list(df['article_type'].unique()))],            
		value='Tshirts'
            )
        ],
        style={'width': '49%', 'display': 'inline-block'}),

        html.Div([
	    html.Label('Gender'),
            dcc.Dropdown(
                id='filter-gender-pri',
                options=[{'label': i, 'value': i} for i in sorted(list(df['gender'].unique()))],
		value='Men'
            ),
	    html.Label('Brand'),
            dcc.Dropdown(
                id='filter-brand-pri',
                options=[{'label': i, 'value': i} for i in sorted(list(df['brand'].unique()))],
		value='Roadster'
            )
        ], style={'width': '49%', 'float': 'right', 'display': 'inline-block'})
    ], style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(250, 250, 250)',
        'padding': '10px 5px'
    }),

    html.Hr(),

    html.Div([
        dcc.Graph(
            id='crossfilter-indicator-scatter-pri',
            hoverData={'points': [{'customdata': 'Japan'}]}
        )
    ], style={'width': '98%', 'display': 'inline-block', 'padding': '0 20'}),

    html.Hr(),

    html.Div(
	dcc.Slider(
        id='filter-vis-slider',
        min=0,
        max=1,
        value=0.4,
        step=None,
        marks={str(year): str(year) for year in np.arange(0, .85, .05)}
    ), style={'width': '98%', 'padding': '0px 20px 20px 20px'}),

    html.Hr(),
    html.Div([
        dcc.Graph(
            id='input-table-pri',
        )
    ], style={'width': '98%', 'display': 'inline-block', 'padding': '0 20'})
])


@app.callback(
    dash.dependencies.Output('filter-article_type-pri', 'options'),
    [dash.dependencies.Input('filter-date-pri', 'value')])
def set_AT_options_pr(selected_date):
    return [{'label': i, 'value': i} for i in sorted(list(df[df['date']==selected_date]['article_type'].unique())) ]

@app.callback(
    dash.dependencies.Output('filter-article_type-pri', 'value'),
    [dash.dependencies.Input('filter-article_type-pri', 'options')])
def set_AT_value_pr(available_options):
    return available_options[0]['value']

@app.callback(
    dash.dependencies.Output('filter-gender-pri', 'options'),
    [dash.dependencies.Input('filter-date-pri', 'value'),
     dash.dependencies.Input('filter-article_type-pri', 'value')])
def set_gender_options_pr(selected_date,selected_at):
    return [{'label': i, 'value': i} for i in sorted(list(df[(df['date']==selected_date) & (df['article_type']==selected_at)]['gender'].unique())) ]

@app.callback(
    dash.dependencies.Output('filter-gender-pri', 'value'),
    [dash.dependencies.Input('filter-gender-pri', 'options')])
def set_gender_value_pr(available_options):
    return available_options[0]['value']

@app.callback(
    dash.dependencies.Output('filter-brand-pri', 'options'),
    [dash.dependencies.Input('filter-date-pri', 'value'),
     dash.dependencies.Input('filter-article_type-pri', 'value'),
     dash.dependencies.Input('filter-gender-pri', 'value')])
def set_brand_options_pr(selected_date,selected_at,selected_gender):
    return [{'label': i, 'value': i} for i in sorted(list(df[(df['date']==selected_date) & (df['article_type']==selected_at) & (df['gender']==selected_gender)]['brand'].unique())) ]

@app.callback(
    dash.dependencies.Output('filter-brand-pri', 'value'),
    [dash.dependencies.Input('filter-brand-pri', 'options')])
def set_brand_value_pr(available_options):
    return available_options[0]['value']


@app.callback(
     dash.dependencies.Output('filter-vis-slider', 'marks'),
     [dash.dependencies.Input('filter-article_type-pri', 'value'),
      dash.dependencies.Input('filter-gender-pri', 'value'),
      dash.dependencies.Input('filter-brand-pri', 'value')])

def set_slider_marks(article_type,gender, brand):
    bag=brand+article_type+gender
    mean=ovr_dist.ix[(ovr_dist['bag']==bag) & (ovr_dist['variable']=='lc_share_pltf'),'mean'].values[0]
    std=ovr_dist.ix[(ovr_dist['bag']==bag) & (ovr_dist['variable']=='lc_share_pltf'),'std'].values[0]
    return {str(round(i,3)): str(round(i,3)) for i in np.arange(mean-2*std, mean+2*std, std/5)}

@app.callback(
     dash.dependencies.Output('filter-vis-slider', 'max'),
     [dash.dependencies.Input('filter-article_type-pri', 'value'),
      dash.dependencies.Input('filter-gender-pri', 'value'),
      dash.dependencies.Input('filter-brand-pri', 'value')])

def set_slider_max(article_type,gender, brand):
    bag=brand+article_type+gender
    mean=ovr_dist.ix[(ovr_dist['bag']==bag) & (ovr_dist['variable']=='lc_share_pltf'),'mean'].values[0]
    std=ovr_dist.ix[(ovr_dist['bag']==bag) & (ovr_dist['variable']=='lc_share_pltf'),'std'].values[0]
    return mean+2*std

@app.callback(
     dash.dependencies.Output('filter-vis-slider', 'min'),
     [dash.dependencies.Input('filter-article_type-pri', 'value'),
      dash.dependencies.Input('filter-gender-pri', 'value'),
      dash.dependencies.Input('filter-brand-pri', 'value')])

def set_slider_min(article_type,gender, brand):
    bag=brand+article_type+gender
    mean=ovr_dist.ix[(ovr_dist['bag']==bag) & (ovr_dist['variable']=='lc_share_pltf'),'mean'].values[0]
    std=ovr_dist.ix[(ovr_dist['bag']==bag) & (ovr_dist['variable']=='lc_share_pltf'),'std'].values[0]
    return mean-2*std


@app.callback(
     dash.dependencies.Output('filter-vis-slider', 'value'),
     [dash.dependencies.Input('filter-article_type-pri', 'value'),
      dash.dependencies.Input('filter-gender-pri', 'value'),
      dash.dependencies.Input('filter-brand-pri', 'value')])

def set_slider_value(article_type,gender, brand):
    bag=brand+article_type+gender
    mean=ovr_dist.ix[(ovr_dist['bag']==bag) & (ovr_dist['variable']=='lc_share_pltf'),'mean'].values[0]
    return mean


@app.callback(
     dash.dependencies.Output('input-table-pri', 'figure'),
     [dash.dependencies.Input('filter-date-pri', 'value'),
      dash.dependencies.Input('filter-article_type-pri', 'value'),
      dash.dependencies.Input('filter-gender-pri', 'value'),
      dash.dependencies.Input('filter-brand-pri', 'value'),
      dash.dependencies.Input('filter-vis-slider', 'value')])

def generate_table_pr(date, article_type,gender, brand,vis, max_rows=1):
    dataframe=df[(df['date'] == date) & (df['article_type'] == article_type) & (df['gender'] == gender) & (df['brand'] == brand)]
    dataframe['lc_share_pltf']=vis
    return ff.create_table(dataframe[ind_list])


@app.callback(
    dash.dependencies.Output('crossfilter-indicator-scatter-pri', 'figure'),
    [dash.dependencies.Input('filter-date-pri', 'value'),
     dash.dependencies.Input('filter-article_type-pri', 'value'),
     dash.dependencies.Input('filter-gender-pri', 'value'),
     dash.dependencies.Input('filter-brand-pri', 'value'),
     dash.dependencies.Input('filter-vis-slider', 'value')])

def update_graph_pr(date, article_type,
                 gender, brand,
                 vis):
    bag=brand+article_type+gender
    path=r'/data/pratik/tensor/models/'
    filename=brand+'-'+article_type+'-'+gender
    with open(path+filename+'.pkl', 'rb') as f:
        model = pickle.load(f)    
    dat = df[(df['date'] == date) & (df['article_type'] == article_type) & (df['gender'] == gender) & (df['brand'] == brand)]
    dat['lc_share_pltf']=vis
    v=np.random.normal(ovr_dist.ix[(ovr_dist['bag']==bag) & (ovr_dist['variable']=='wgt_input_td'),'mean'].values[0],ovr_dist.ix[(ovr_dist['bag']==bag) & (ovr_dist['variable']=='wgt_input_td'),'std'].values[0],100)
    dff=pd.DataFrame()
    dff=dff.append([dat]*len(v),ignore_index=True)
    dff['wgt_input_td']=v
    dff['input_td_diff']=dff['wgt_input_td']/dff['input_td_rm']
    dff=dff[dff['wgt_input_td']>0]
    dff['ros']=model.predict(dff[ind_list])

    return {
        'data': [go.Scatter(
            x=dff['wgt_input_td'],
            y=dff['ros'],
            mode='markers',
            marker={
                'size': 15,
                'opacity': 0.5,
                'line': {'width': 0.5, 'color': 'white'}
            }
        )],
        'layout': go.Layout(
            xaxis={
                'title': 'Pricing (Output Trade Discount)'
            },
            yaxis={
                'title': 'Units sold (predicted)'
            },
            margin={'l': 40, 'b': 30, 't': 10, 'r': 0},
            height=450,
            hovermode='closest'
        )
    }
