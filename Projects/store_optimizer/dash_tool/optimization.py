from __future__ import division
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import dash_table_experiments as dt
import flask
import sqlalchemy as sq
import pandas as pd
import json
import datetime
from pyomo.opt import SolverFactory
from pyomo.environ import *
import pdb
from flask.ext.session import Session
from flask import session
import random
import time
from threading import Thread
import urllib

from app import app

JOIN_FOR_INVENTORY = 0
JOIN_FOR_ACTUAL_VALUES = 0
JOIN_FOR_ASP_RGM_REVENUE = 0
PERC = 0.15

final_df = None
final_df_grp = None
total_visibility = -2
total_discount_diff = -2
total_rgm = -2

bu_dis_dict={}


def rename_dataframe(df):
	name_dict={}
	# Level
	# Revenue Gain

	# if df == None:
	# 	return
	print df
	print 'in rename_dataframe'
	if 'ros' in df:
		name_dict['discount']='Suggested Discount'
		name_dict['real_discount']='Actual discount'
		name_dict['visibility']='Suggested Visibility'
		name_dict['real_visibility']='Actual Visibility'

		name_dict['real_rgm_ratio']='Actual GM'
		name_dict['rgm_ratio']='Predicted GM'
		name_dict['qty_sold']='Actual units sold'
		name_dict['ros']='Predicted units sold'
		name_dict['shipped_revenue']='Actual Revenue'
		name_dict['pred_rev']='Predicted Revenue'

		df.rename(columns=name_dict, inplace=True)
	return df.copy(deep=True)

def del_column_dataframe(df):
	# if df == None:
	# 	return
	print df
	list_col = ['rgm','real_rgm', 'real_asp', 'asp']
	for col in list_col:
		if col in df:
			del df[col]
	return df.copy(deep=True)


def set_parameters(visibility, discount, rgm, rev, perc, opt_type):
	df2 = pd.read_csv("bag_model_info20170915", encoding="utf-8-sig")
	final_df = df2
	final_df=final_df.dropna()

	PERC = 0.15
	if perc >= 0:
		PERC = perc


	if JOIN_FOR_INVENTORY == 1 :
			final_df = pd.merge(df2, df1, how='left')
			final_df = final_df.dropna()

	if JOIN_FOR_ACTUAL_VALUES == 1:
			df = pd.read_csv('/Users/16546/myntra/revenue_linear_prog/actual_values.csv', encoding="utf-8-sig")
			df = df.rename(columns = {'output_td':'real_output_td','output_td_diff':'real_output_td_diff','lc_share_pltf':'real_lc_share_pltf'})
			final_df = pd.merge(final_df, df, how='left', on='name')
			final_df = final_df.dropna()

	if JOIN_FOR_ASP_RGM_REVENUE == 1:
			df = pd.read_csv('/Users/16546/myntra/revenue_linear_prog/rgm_and_asp.csv', encoding="utf-8-sig")
			df['bag'] = df['brand'] + df['article_type'] + df['gender']
			final_df = pd.merge(final_df, df[['shipped_revenue','RGM','ASP','bag']], how='left', on='bag')

	print final_df.columns
	print type(final_df)
	final_df = final_df[(final_df['output_td_x']<=final_df['output_td_y']* (1 + PERC) ) & (final_df['output_td_x']>=final_df['output_td_y']* ( 1 - PERC) )]
	final_df = final_df[(final_df['lc_share_pltf_x']<=final_df['lc_share_pltf_y']* (1 + PERC) ) & (final_df['lc_share_pltf_x']>=final_df['lc_share_pltf_y']* ( 1 - PERC) )]
	final_df = final_df[final_df['ros']<=final_df['inventory']]
	final_df = final_df.dropna()

	name_dict={}
	name_dict['output_td_x']='discount'
	name_dict['lc_share_pltf_x']='visibility'
	name_dict['lc_share_pltf_y']='real_visibility'
	name_dict['output_td_y']='real_discount'
	name_dict['rgm']='real_rgm'
	name_dict['aisp']='real_asp'

	final_df.rename(columns=name_dict, inplace=True)

	final_df = final_df.sort_values('bag')
	final_df =final_df.reset_index()

	final_df = final_df.drop('index',axis=1)

	final_df.index.name = "index"
	final_df.index = final_df.index + 1

	# r = random.Random()
	# final_df['real_rgm'] = final_df['real_rgm'] + [r.randint(-50,50) for x in range(0, final_df.index.size)]
	final_df['rgm'] = final_df['asp'] - final_df['real_asp'] + final_df['real_rgm']
	final_df.head()

	final_df.drop('bag',axis=1).to_csv('datafinal.tab',encoding="UTF_8", sep=' ')
	final_df.to_csv('final_with_bag.csv',encoding="UTF_8", index=False)

	final_df_grp = final_df.groupby('bag')['ros'].agg('count').reset_index()
	final_df_grp['sum']=final_df_grp['ros'].cumsum()
	final_df_grp = final_df_grp[['ros','sum']]
	final_df_grp.index.name="index"
	final_df_grp.index = final_df_grp.index + 1
	final_df_grp.rename(columns={'ros':'count'}, inplace=True)

	final_df_grp.to_csv('final_csv_agg.tab', sep=' ')

	query="select ([brand]+[article_type]+[product_gender]) as bag, brand, article_type, business_unit from fact_category_over_view_metrics where date=20170915 and is_live_style=1 group by brand, article_type, product_gender, business_unit"
	engine = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")
	bag_map=pd.read_sql_query(query,engine)
	bag_map=bag_map.drop_duplicates('bag')
	bag_map=bag_map.dropna()


	final_df_map=pd.merge(final_df, bag_map, how='left', on='bag')

	total_visibility = final_df.groupby('bag').nth(0).visibility.sum()

	total_discount_diff = (final_df.groupby('bag').nth(0).qty_sold * final_df.groupby('bag').nth(0).real_discount).sum()/ final_df.groupby('bag').nth(0).qty_sold.sum()

	total_revenue = (final_df.groupby('bag').nth(0).asp * final_df.groupby('bag').nth(0).qty_sold).sum()

	

	#Need session based stuff for below stuff

	if visibility >= 0:
		session['total_visibility'] = visibility
	else:
		session['total_visibility'] = total_visibility

	if discount >= 0:
		session['total_discount_diff'] = discount
	else:
		session['total_discount_diff'] = total_discount_diff

	if rgm >= 0:
		session['total_rgm'] =rgm
	else:
		session['total_rgm'] = -2

	if rev >= 0:
		session['total_revenue'] = rev
	else:
		session['total_revenue'] = total_revenue

	# del session['final_df_grp']
	# del session['final_df']
	# del session['final_df_map']

# 	final_df_map = final_df_map.copy(deep=True)
# 	session['final_df_grp'] = final_df_grp.copy(deep=True)
# 	session['final_df'] = final_df.copy(deep=True)
# 	print final_df_map
# 	session['final_df_map'] = final_df_map.copy(deep=True)



# def get_model():
	model = ConcreteModel()
	var_perc = .15

	print session.keys()

	# final_df = session['final_df']
	# final_df_grp = session['final_df_grp']
	# final_df_map = session['final_df_map']
	bu_dis_dict = session['bu_dis_dict']
	at_dis_dict = {}
	br_dis_dict = {}

	at_dis_dict['Blankets Quilts and Dohars'] = 0.41
	at_dis_dict['Blazers'] = 0.52
	at_dis_dict['Camisoles'] = 0.17
	at_dis_dict['Handbags'] = 0.57
	at_dis_dict['Jeggings'] = 0.45
	at_dis_dict['Wallets'] = 0.50
	at_dis_dict['Earrings'] = 0.57
	at_dis_dict['Flats'] = 0.38
	at_dis_dict['Foundation and Primer'] = 0.18
	at_dis_dict['Innerwear Vests'] = 0.18

	br_dis_dict['Arrow Sport'] = 0.52
	br_dis_dict['Aujjessa'] = 0.58
	br_dis_dict['Aum'] = 0.45
	br_dis_dict['Avaana'] = 0.65
	br_dis_dict['Belle Fille'] = 0.6
	br_dis_dict['Bhama Couture'] = 0.62
	br_dis_dict['Bitterlime'] = 0.51
	br_dis_dict['Black coffee'] = 0.55
	br_dis_dict['Breakbounce'] = 0.48
	br_dis_dict['Puma'] = 0.38



	model.ros = final_df['ros']
	model.visibility = final_df['visibility']
	model.asp = final_df['asp']
	model.rgm = final_df['rgm']
	model.real_asp = final_df['real_asp']
	model.real_rgm = final_df['real_rgm']
	model.qty_sold = final_df['qty_sold']



	model.count = final_df_grp['count']
	model.sum = final_df_grp['sum']

	model.discount = final_df['discount']
	model.ssize = final_df.index.size
	model.bagSize = final_df_grp.index.size


	model.x = Var(final_df.index, domain=Binary)

	def obj_revenue(model):
		return sum(model.ros[i]*model.x[i]*model.asp[i] for i in range(1, 1+model.ssize))

	model.obj_revenue = Objective(rule=obj_revenue, sense=maximize)

	def obj_profit(model):
		return sum(model.rgm[i] * model.ros[i] * model.x[i] for i in range(1, 1+model.ssize))

	model.obj_profit = Objective(rule=obj_profit, sense=maximize)


	def visibility_rule(model):
		return (0, sum(model.x[i] * model.visibility[i] for i in range(1, 1+model.ssize)), session['total_visibility'] )

	model.visibilityConst = Constraint(rule = visibility_rule)


	def one_per_bag_rule(model, i):
		# return sum(model.x[j] for j in range( ((i-1)*model.bag_table_size) + 1, (i*model.bag_table_size) + 1 ) ) <= 1
		return sum(model.x[j] for j in range( model.sum[i] - model.count[i]+1, model.sum[i] + 1 )) <= 1

	def discount_diff(model):
		return sum(model.x[i]*model.discount[i]*model.ros[i] for i in range(1, 1+model.ssize)) <= sum(model.x[i]*model.ros[i] for i in range(1, 1+model.ssize)) * session['total_discount_diff']
	#(sum(model.real_discount[i]*model.qty_sold[i] for i in model.ssize) / sum(model.ros[i])

		

	def bu_constraint_discount(model, i):
		bu_eq = final_df_map[final_df_map['business_unit']==i]
		print i
		# print bu_eq
		print final_df_map.index
		bu_eq.index+=1 #to make 0 index 1. check if it is one in the beginning
		return sum(model.x[j]*model.discount[j]*model.ros[j] for j in bu_eq.index.values) <= sum(model.x[j]*model.ros[j] for j in bu_eq.index.values) * bu_dis_dict[i]


	def br_constraint_discount(model, i):
		br_eq = final_df_map[final_df_map['brand']==i]
		print i
		# print br_eq
		print final_df_map.index
		br_eq.index+=1 #to make 0 index 1. check if it is one in the beginning
		return sum(model.x[j]*model.discount[j]*model.ros[j] for j in br_eq.index.values) <= sum(model.x[j]*model.ros[j] for j in br_eq.index.values) * br_dis_dict[i]


	def at_constraint_discount(model, i):
		at_eq = final_df_map[final_df_map['article_type']==i]
		print i
		# print at_eq
		print final_df_map.index
		at_eq.index+=1 #to make 0 index 1. check if it is one in the beginning
		return sum(model.x[j]*model.discount[j]*model.ros[j] for j in at_eq.index.values) <= sum(model.x[j]*model.ros[j] for j in at_eq.index.values) * at_dis_dict[i]

	rgm_final = 0
	if session['total_rgm'] == -2:
		rgm_final = (sum(model.qty_sold[model.sum[i]]*model.real_rgm[model.sum[i]] for i in range(1,1+model.bagSize))/sum(model.real_asp[model.sum[i]]*model.qty_sold[model.sum[i]] for i in range(1,1+model.bagSize)))
	else:
		rgm_final = session['total_rgm']

	def rgm_constraint(model):
		return sum(model.rgm[i] * model.ros[i] * model.x[i] for i in range(1, 1+model.ssize)) >= sum(model.ros[i] * model.asp[i] * model.x[i] for i in range(1, 1+model.ssize)) * rgm_final

	def revenue_constraint(model):
		return sum(model.ros[i] * model.x[i] * model.asp[i] for i in range(1, 1+model.ssize)) >= session['total_revenue']



	# def agg_discount_ratio(model, agg_level):
	#     df = 

	model.buConstraintDiscountRule = Constraint(bu_dis_dict.keys(), rule=bu_constraint_discount)
	# model.brConstraintDiscountRule = Constraint(br_dis_dict.keys(), rule=br_constraint_discount)
	# model.atConstraintDiscountRule = Constraint(at_dis_dict.keys(), rule=at_constraint_discount)


	model.onePerBagRule = Constraint(final_df_grp.index.values, rule=one_per_bag_rule)
	model.discount_diff_rule = Constraint(rule=discount_diff)
	model.rgm_constraint_rule = Constraint(rule=rgm_constraint)
	model.revenue_constraint_rule = Constraint(rule=revenue_constraint)



	if opt_type == 0:
		model.obj_profit.deactivate()
		model.revenue_constraint_rule.deactivate()
	else:
		model.obj_revenue.deactivate()
		model.rgm_constraint_rule.deactivate()
	opt = SolverFactory("cbc")
	opt.options.ratioGap=0.01
	instance = model.create()
	instance.write('optimize.lp')
	results = opt.solve(instance)
	instance.solutions.store_to(results)

	print results

	data = results.json_repn()
	sol_dict = data['Solution'][1]['Variable']

	data_file = open('final_with_bag.csv')
	data_file_arr=[]
	for item in data_file:
		data_file_arr.append(item)

	extracted_info=[]
	write_file = open('final_results.csv','w')
	write_file.write(data_file_arr[0].strip() + '\n')
	for item in sol_dict:
	#   extracted_info.append((data_file_arr[(int)(item.split('[')[1].split(']')[0]) - 0].strip()))
		if sol_dict[item]['Value'] == 1.0:
			#print item
			#print item.split('[')[1].split(']')[0]
			write_file.write((data_file_arr[(int)(item.split('[')[1].split(']')[0]) - 0].strip()) +'\n')

	# write_file.write(extracted_info)
	write_file.close()
	# df_to_write = pd.DataFrame(x.split(',') for x in extracted_info)
	# df_to_write.to_csv('/Users/16546/myntra/revenue_linear_prog/final_results.csv',index=False)



	input = pd.read_csv('final_with_bag.csv')
	res = pd.read_csv('final_results.csv')

	res.columns

	input = input.groupby('bag').nth(0)
	input = input.drop('output_td_diff',axis=1)
	input = input.drop('ros',axis=1)
	input = input.drop('discount',axis=1)
	input = input.drop('visibility',axis=1)
	input = input.drop('brokeness',axis=1)
	input = input.drop('freshness',axis=1)
	input = input.drop('live_styles',axis=1)
	input = input.drop('presale_flag',axis=1)
	input = input.drop('sessions',axis=1)
	input = input.drop('asp',axis=1)
	input = input.drop('mrp',axis=1)
	input = input.drop('inventory',axis=1)
	input = input.drop('rgm',axis=1)
	# input = input.drop('index',axis=1)


	res = res.drop('real_visibility',axis=1)
	res = res.drop('real_discount',axis=1)
	res = res.drop('inventory',axis=1)
	res = res.drop('qty_sold',axis=1)
	res = res.drop('output_td_diff',axis=1)
	res = res.drop('brokeness',axis=1)
	res = res.drop('freshness',axis=1)
	res = res.drop('live_styles',axis=1)
	res = res.drop('presale_flag',axis=1)
	res = res.drop('sessions',axis=1)
	res = res.drop('real_rgm',axis=1)
	res = res.drop('mrp',axis=1)
	# res = res.drop('index',axis=1)

	res = res.drop('shipped_revenue',axis=1)
	res['pred_rev'] = res['ros'] * res['asp']
	res = res.drop('real_asp',axis=1)

	input = input.reset_index()

	fin = pd.merge(input,res,how='left',on='bag')
	fin = fin.fillna(0)



	query="select ([brand]+[article_type]+[product_gender]) as bag, brand, article_type, business_unit, product_gender from fact_category_over_view_metrics where date=20170915 and is_live_style=1 group by brand, article_type, product_gender, business_unit"
	engine = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")
	bag_map=pd.read_sql_query(query,engine)
	bag_map=bag_map.drop_duplicates('bag')




	o=pd.merge(fin, bag_map, how='left', on='bag')


	date_str = (datetime.datetime.now()-datetime.timedelta(days=1)).strftime ("%Y%m%d")
	o.to_csv('final_result_at_bag_level' + date_str + '.csv')

	o['discount']=o['discount']*o['ros']
	o['real_discount']=o['real_discount']*o['qty_sold']

	o['rgm_ratio_num'] = o['rgm'] * o['ros']
	o['rgm_ratio_denom'] = o['asp'] * o['ros']

	o['real_rgm_ratio_num'] = o['real_rgm'] * o['qty_sold']
	o['real_rgm_ratio_denom'] = o['real_asp'] * o['qty_sold']

	o=o.round(2)
	o.drop('level_0_x', axis=1,inplace=True)
	o.drop('level_0_y', axis=1, inplace=True)

	br = o.groupby('brand').sum()
	at = o.groupby('article_type').sum()
	bu = o.groupby('business_unit').sum()


	br['real_discount'] = br['real_discount']/br['qty_sold']
	br['discount'] = br['discount']/br['ros']
	br['rgm_ratio'] = br['rgm_ratio_num'] / br['rgm_ratio_denom']
	br['real_rgm_ratio'] = br['real_rgm_ratio_num'] / br['real_rgm_ratio_denom']
	br=br.drop('rgm_ratio_num', axis=1)
	br=br.drop('rgm_ratio_denom', axis=1)
	br=br.drop('real_rgm_ratio_num', axis=1)
	br=br.drop('real_rgm_ratio_denom', axis=1)

	bu['discount'] = bu['discount']/bu['ros']
	bu['real_discount'] = bu['real_discount']/bu['qty_sold']
	bu['rgm_ratio'] = bu['rgm_ratio_num'] / bu['rgm_ratio_denom']
	bu['real_rgm_ratio'] = bu['real_rgm_ratio_num'] / bu['real_rgm_ratio_denom']
	bu=bu.drop('rgm_ratio_num', axis=1)
	bu=bu.drop('rgm_ratio_denom', axis=1)
	bu=bu.drop('real_rgm_ratio_num', axis=1)
	bu=bu.drop('real_rgm_ratio_denom', axis=1)

	at['real_discount'] = at['real_discount']/at['qty_sold']
	at['discount'] = at['discount']/at['ros']
	at['rgm_ratio'] = at['rgm_ratio_num'] / at['rgm_ratio_denom']
	at['real_rgm_ratio'] = at['real_rgm_ratio_num'] / at['real_rgm_ratio_denom']
	at=at.drop('rgm_ratio_num', axis=1)
	at=at.drop('rgm_ratio_denom', axis=1)
	at=at.drop('real_rgm_ratio_num', axis=1)
	at=at.drop('real_rgm_ratio_denom', axis=1)

	o=o.drop('rgm_ratio_num', axis=1)
	o=o.drop('rgm_ratio_denom', axis=1)
	o=o.drop('real_rgm_ratio_num', axis=1)
	o=o.drop('real_rgm_ratio_denom', axis=1)

	bu.reset_index(inplace=True)
	br.reset_index(inplace=True)
	at.reset_index(inplace=True)
	bu=bu.round(2)
	at=at.round(2)
	br=br.round(2)
	bu['max_discount'] = session['bu_temp']['max_discount']
	at['max_discount'] = session['at_temp']['max_discount']
	br['max_discount'] = session['br_temp']['max_discount']

	print bu
	# del session['br_temp']
	# del session['bu_temp']
	# del session['at_temp']
	# del session['data_temp']
	session['br_temp'] = br.copy(deep=True)
	session['bu_temp'] = bu.copy(deep=True)
	session['at_temp'] = at.copy(deep=True)
	session['data_temp'] = o.copy(deep=True)

	# bu = session['bu_temp'].copy(deep=True)
	# bu['rgm'] = 100
	# session['bu_temp'] = bu.copy(deep=True)
	# print session['bu_temp']
	# time.sleep(5)
	# session['bu_temp'] = bu.copy(deep=True)




data = pd.read_csv('/data/janus/daily_optimizer_results/final_result_at_bag_level_revenue_20171029.csv')
data = data.round(3)
o = data
o['discount']=o['discount']*o['ros']
o['real_discount']=o['real_discount']*o['qty_sold']

o['rgm_ratio_num'] = o['rgm'] * o['ros']
o['rgm_ratio_denom'] = o['asp'] * o['ros']

o['real_rgm_ratio_num'] = o['real_rgm'] * o['qty_sold']
o['real_rgm_ratio_denom'] = o['real_asp'] * o['qty_sold']


o=o.round(2)
o.drop('level_0_x', axis=1,inplace=True)
o.drop('level_0_y', axis=1, inplace=True)
o=o.drop('Unnamed: 0', axis=1)

br = o.groupby('brand').sum()
at = o.groupby('article_type').sum()
bu = o.groupby('business_unit').sum()


br['real_discount'] = br['real_discount']/br['qty_sold']
br['discount'] = br['discount']/br['ros']
br['rgm_ratio'] = br['rgm_ratio_num'] / br['rgm_ratio_denom']
br['real_rgm_ratio'] = br['real_rgm_ratio_num'] / br['real_rgm_ratio_denom']
br=br.drop('rgm_ratio_num', axis=1)
br=br.drop('rgm_ratio_denom', axis=1)
br=br.drop('real_rgm_ratio_num', axis=1)
br=br.drop('real_rgm_ratio_denom', axis=1)

bu['discount'] = bu['discount']/bu['ros']
bu['real_discount'] = bu['real_discount']/bu['qty_sold']
bu['rgm_ratio'] = bu['rgm_ratio_num'] / bu['rgm_ratio_denom']
bu['real_rgm_ratio'] = bu['real_rgm_ratio_num'] / bu['real_rgm_ratio_denom']
bu=bu.drop('rgm_ratio_num', axis=1)
bu=bu.drop('rgm_ratio_denom', axis=1)
bu=bu.drop('real_rgm_ratio_num', axis=1)
bu=bu.drop('real_rgm_ratio_denom', axis=1)

at['real_discount'] = at['real_discount']/at['qty_sold']
at['discount'] = at['discount']/at['ros']
at['rgm_ratio'] = at['rgm_ratio_num'] / at['rgm_ratio_denom']
at['real_rgm_ratio'] = at['real_rgm_ratio_num'] / at['real_rgm_ratio_denom']
at=at.drop('rgm_ratio_num', axis=1)
at=at.drop('rgm_ratio_denom', axis=1)
at=at.drop('real_rgm_ratio_num', axis=1)
at=at.drop('real_rgm_ratio_denom', axis=1)

bu.reset_index(inplace=True)
br.reset_index(inplace=True)
at.reset_index(inplace=True)
bu=bu.round(2)
at=at.round(2)
br=br.round(2)
print bu


o=o.drop('rgm_ratio_num', axis=1)
o=o.drop('rgm_ratio_denom', axis=1)
o=o.drop('real_rgm_ratio_num', axis=1)
o=o.drop('real_rgm_ratio_denom', axis=1)

data = o


bu['max_discount'] = ''
at['max_discount'] = ''
br['max_discount'] = ''


# app = dash.Dash()



bu_temp = bu.copy(deep=True)
br_temp = br.copy(deep=True)
at_temp = at.copy(deep=True)
data_temp = data.copy(deep=True)


def generate_table(dataframe, max_rows=100):
	return html.Table(
		
		# Header
		[html.Tr([html.Th(col, style={'border-collapse': 'collapse', 'border': '1px solid black','border-bottom': '1px solid #ddd'}) for col in dataframe.columns])] +

		# Body
		[html.Tr([
			html.Td(dataframe.iloc[i][col], style={'border-collapse': 'collapse', 'border': '1px solid black','border-bottom': '1px solid #ddd','text-align':'center'}) for col in dataframe.columns
		],
		style={'border': '1px solid black','border-bottom': '1px solid #ddd'}
		) for i in range(min(len(dataframe), max_rows))]
		,
		style={'border-collapse': 'collapse', 'border': '1px solid black'}
	)


def generate_exp_table(dataframe):
	return dt.DataTable(
		rows=dataframe.to_dict('records'),

		# optional - sets the order of columns
		columns=sorted(dataframe.columns),

		row_selectable=True,
		filterable=True,
		sortable=True,
		selected_row_indices=[],
		id='datatable-gapminder'
	)

layout = html.Div(children=[
	 html.Div([

			html.Div([
			html.Label('Visibility',style={'text-align': 'center','display':'block', 'margin':'auto'}),
				dcc.Input(
					placeholder="Default value - yesterday's actual",
					type='number',
					min=-1,
					max=100,
					step=.0001,
					id='visibility-input',
					style={'width':'65%', 'display':'block', 'margin':'auto'}
					)],
			style={'width': '30%', 'display': 'inline-block'}),

			html.Div([
			html.Label('Discount',style={'text-align': 'center','display':'block', 'margin':'auto'}),
				dcc.Input(
					placeholder="Default value - yesterday's actual",
					min=-1,
					max=1,
					type='number',
					step=.0001,
					id='discount-input',
					style={'width':'65%', 'display':'block', 'margin':'auto'}
				)	        ],
			style={'width': '30%', 'display': 'inline-block'}),

			html.Div([
			html.Label('Delta(in %) from current configuration',style={'text-align': 'center','display':'block', 'margin':'auto'}),
				dcc.Input(
					placeholder="Default value 15%",
					min=-1,
					type='number',
					step=.0001,
					id='dev-input',
					style={'width':'65%', 'display':'block', 'margin':'auto'}
					)
			], style={'width': '30%', 'float': 'right', 'display': 'inline-block'})
		], style={
			'display': 'flex', 'justify-content':'center',
			'borderBottom': 'thin lightgrey solid',
			'backgroundColor': 'rgb(250, 250, 250)',
			'padding': '10px 5px'
		}),
	 html.Div([
	 	html.Div([
	 	html.Label('Optimization Type',style={'text-align': 'left','display':'block', 'margin':'auto'}),
		 dcc.RadioItems(
			id='opt-type',
			options=[
				{'label': 'Maximise revenue', 'value': 'rev'},
				{'label': 'Maximise margin', 'value': 'rgm'}
			],
			value='rev',
			labelStyle={'display': 'inline-block'}
		 	)
		 ], style={'width': '30%', 'display': 'inline-block'}),
	 	html.Div([
	 		html.Label('Gross Margin',style={'text-align': 'center','display':'block', 'margin':'auto'}),
 			dcc.Input(
 				placeholder="Default value - yesterday's actual",
 				type='number',
 				min=-1,
 				max=1,
 				step=.0001,
 				id='rgm-input',
 				style={'width':'65%', 'display':'block', 'margin':'auto'}
 				),
	 		], style={'width': '30%', 'float': 'right', 'display': 'inline-block'})
	 ], style={
			'display': 'flex', 'justify-content':'center',
			'borderBottom': 'thin lightgrey solid',
			'backgroundColor': 'rgb(250, 250, 250)',
			'padding': '10px 5px'
		}),
	# html.Label(children=["Visibility",
	# dcc.Input(
	# placeholder="Share of list counts",
	# type='number',
	# min=-1,
	# max=100,
	# step=.0001,
	# id='visibility-input',
	# value='-1'
	# )]),
	# html.Br(),
	# html.Label(children=["Discount",
	# dcc.Input(
	# placeholder="Enter discount. -1 to retain input's",
	# min=-1,
	# max=1,
	# type='number',
	# step=.0001,
	# id='discount-input',
	# value='-1'
	# )]),
	# html.Br(),
	# html.Label(children=["Gross Margin",
	# dcc.Input(
	# placeholder="Enter GM. -1 to retain input's",
	# type='number',
	# min=-1,
	# max=1,
	# step=.0001,
	# id='rgm-input',
	# value='-1'
	# )]),
	# html.Br(),
	# html.Label(children=["Delta(in %) from current configuration",
	# dcc.Input(
	# placeholder="Enter deviation(in %). -1 for default",
	# min=-1,
	# type='number',
	# step=.0001,
	# id='dev-input',
	# value='-1',
	# )]), 
	html.Br(), 
	html.Button('Optimize', id='opt_button', style={'width': '25%', 'display':'block', 'margin':'auto'}),
	html.Br(),
	html.H5("Summary Level",style={ 'text-align': 'center','display':'block', 'margin':'auto'}),
	html.Div([dcc.Dropdown(
		id='my-dropdown',
		options=[
			{'label': 'Business unit level', 'value': 'bu_temp'},
			{'label': 'Article type level', 'value': 'at_temp'},
			{'label': 'Brand level', 'value': 'br_temp'},
			{'label': 'Bag Level', 'value': 'data_temp'}
		],
		value='data_temp',
		)], style={'width': '25%', 'display':'block', 'margin':'auto'}),
	html.Br(),
	html.H4(children='Summary',id='container'),
	html.Div(children=generate_exp_table(br), id='table1'),
	# html.Br(),
	# html.Button('Re-optimize', id='re-opt_button'),
	html.Br(),
	html.A([html.Button('Download')], href="/dash/file_download", download='download.csv')
])

@app.callback(
	dash.dependencies.Output('table1', 'children'),
	[dash.dependencies.Input('my-dropdown', 'value'),
	dash.dependencies.Input('datatable-gapminder', 'row_update'),
	],
	[dash.dependencies.State('visibility-input', 'value'),
	dash.dependencies.State('discount-input', 'value'),
	dash.dependencies.State('rgm-input', 'value'),
	dash.dependencies.State('dev-input', 'value')
	])
def update_output(value, row_update,visibility, discount, rgm, perc):
	# if 'bu_temp' not in locals():
	#     bu_temp = bu.copy(deep=True)
	#     br_temp = br.copy(deep=True)
	#     at_temp = at.copy(deep=True)
	#     data_temp = data.copy(deep=True)
	if 'bu_temp' not in session:

		session['bu_temp'] = bu_temp.copy(deep=True)
		session['br_temp'] = br_temp.copy(deep=True)
		session['at_temp'] = at_temp.copy(deep=True)
		session['data_temp'] = data_temp.copy(deep=True)
		session['final_df'] = None
		session['final_df_grp'] = None
		session['total_visibility'] = -2
		session['total_discount_diff'] = -2
		session['total_rgm'] = -2
		session['bu_dis_dict']={}
		session['final_df_map'] = None

	

   

	# print click, visibility, discount, rgm, perc

	if row_update != None:
		print "Testing callback data ",value, row_update, type(row_update), row_update[0], type(row_update[0]), row_update[0]['updated']
		if row_update[0]['from_row'] ==  row_update[0]['to_row'] and row_update[0]['updated'].keys()[0] == 'max_discount':
			df = session[value]
			df.loc[row_update[0]['from_row'], 'max_discount'] = row_update[0]['updated']['max_discount']
			session[value] = df.copy(deep=True)
			if value == 'bu_temp' and row_update[0]['updated']['max_discount'] != '':
				try:
					session['bu_dis_dict'][df.loc[row_update[0]['from_row'],'business_unit']] = float(row_update[0]['updated']['max_discount'])
				except ValueError:
					print "Enter a number only"#replace with some alert window
			else:
				session['bu_dis_dict'].pop(df.loc[row_update[0]['from_row'],'business_unit'], None)
			print session['bu_dis_dict']

	print session[value].columns
	print value
	print session[value]
	if (session[value]) is not None:
		session[value]=rename_dataframe(session[value])
		session[value]=del_column_dataframe(session[value])
	session[value].to_csv('download.csv', index=False)
	return generate_exp_table(session[value])



def button_click(visibility, discount, rgm, perc):
	# set_parameters(float(visibility), float(discount), float(rgm), float(perc))
	print session['total_visibility']
	# get_model()
	print "Done"


@app.callback(
	dash.dependencies.Output('my-dropdown', 'value'),
	[dash.dependencies.Input('opt_button', 'n_clicks')
	],
	[dash.dependencies.State('visibility-input', 'value'),
	dash.dependencies.State('discount-input', 'value'),
	dash.dependencies.State('rgm-input', 'value'),
	dash.dependencies.State('dev-input', 'value'),
	dash.dependencies.State('my-dropdown', 'value'),
	dash.dependencies.State('opt-type', 'value')
	])
def update_output1(click, visibility, discount, rgm, perc, value, opt_type_val):

	# if 'click_reopt' not in session:
	# 	session['click_reopt'] = 0
	# 	print 'not in session - click_reopt'
	# elif (click_reopt == 0) or session['click_reopt'] > click_reopt:
	# 	session['click_reopt'] = 0
	# 	print 'in session - click_reopt. value is', click_reopt, session['click_reopt']
	# else:
	# 	print 'in session - click_reopt. value is', click_reopt, session['click_reopt']
	# if click_reopt > session['click_reopt']:
	# 	set_parameters(float(visibility), float(discount), float(rgm), float(perc))
	# 	session['click_reopt'] = click_reopt
	# 	get_model()
	# 	print "Done"
	# 	# return generate_exp_table(session['data_temp'])
	# 	return 'data_temp'

	if click > 0 :
		# with app.app_context():
		# 	thread = Thread(target=button_click, args=(visibility, discount, rgm, perc))
		# 	thread.start()
		

		if visibility == None:
			visibility = -1
		if discount == None:
			discount = -1
		if rgm == None:
			rgm = -1
		if perc == None:
			perc = -1

		# print click, visibility, discount, rgm, perc, type(perc), float(perc)

		try:
			visibility = float(visibility)
		except ValueError:
			visibility = -1

		try:
			discount = float(discount)
		except ValueError:
			discount = -1

		try:
			rgm = float(rgm)
		except:
			rgm = -1

		try:
			perc = float(perc)
		except:
			perc = -1

		# print click, visibility, discount, rgm, perc, type(perc), float(perc)


		opt_type=0
		if opt_type_val is 'rgm':
			opt_type = 1
		if opt_type == 0:
			set_parameters(float(visibility), float(discount), float(rgm), -1 , float(perc), opt_type)
		else:
			set_parameters(float(visibility), float(discount), -1, float(rgm), float(perc), opt_type)
		print session['total_visibility']
		# get_model()
		print "Done"
	return 'data_temp'


@app.server.route('/dash/file_download', methods=['GET']) 
def download_csv():
	# response = flask.make_response('/Users/16546/repo/revenue_linear_prog/dash/download.csv')
	# response.headers['Cache-Control'] = 'no-store'
	# response.headers['Content-Type'] = 'text/csv'
	# response.headers['Content-Disposition'] = 'attachment; filename=download.csv'
	# response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
	# # response.headers['Pragma'] = 'no-cache'
	# return response
	return flask.send_file('/Users/16546/repo/revenue_linear_prog/dash/download.csv',
					 mimetype='text/csv',
					 attachment_filename='download.csv',
					 as_attachment=True,
					 cache_timeout = -1)


 

# @app.callback(
#     dash.dependencies.Output('table1', 'children'),
#     [dash.dependencies.Input('datatable-gapminder', 'editable')])
# def table_edit_capture(editable):
#     print editable

# app.css.append_css({
	# "external_url": "https://codepen.io/chriddyp/pen/bWLwgP.css"
# })
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
SESSION_TYPE = 'redis'
SESSION_PERMANENT = False
app.server.config.from_object(__name__)
Session(app.server)


# ap=app.server

# if __name__ == '__main__':
	 # ap.run()
	 # app.run_server(debug=True)
