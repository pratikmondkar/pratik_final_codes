
# coding: utf-8

# In[1]:
import pandas as pd
from datetime import date, timedelta
from pandas import DataFrame
import sqlalchemy as sq
import pickle
import numpy as np 
import math
import time
import scipy as scipy
from scipy import stats
from sklearn import linear_model, metrics, preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import KFold,train_test_split,cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.metrics import make_scorer
from sklearn.feature_selection import SelectFromModel
from sklearn import svm
import statsmodels.api as sm
import xgboost as xgb
from xgboost import XGBRegressor
from modelselector import EstimatorSelectionHelper
import multiprocessing
from datetime import date, timedelta
import warnings
warnings.filterwarnings('ignore')

import gc


# In[2]:

data=pd.read_csv('/data/pratik/tensor/datasets/ad_clean_2k_inp.csv',error_bad_lines=False)

# In[25]:

lst=data[['brand','article_type','gender']].drop_duplicates().to_records(index=False).tolist()

ind_list=['date','qty_sold','wgt_input_td','input_td_diff','lc_share_pltf','live_styles','presale_flag','postsale_flag',
          'sessions','brokeness','freshness','output_cd','loyalty_points','vis_cannib_mean','comp_index','sales_lag',
          'index_week_year','price_cannib_mean','brokenness_rm','freshness_rm','atc_rm']

# In[4]:

def mape(y_true, y_pred): 
    return np.mean(np.abs((y_true - y_pred) / np.maximum(y_true,y_pred))) * 100


# In[23]:

def get_best_model(lst):
    b=lst[0]
    a=lst[1]
    g=lst[2]
    ad=data[(data['brand']==b) & (data['article_type']==a) & (data['gender']==g) ]
    d=ad[ind_list]    

    train, test= train_test_split(d, test_size=0.2, random_state=0)
    y_train = train.qty_sold
    X_train = train.drop(['qty_sold','date'],axis=1)

    y_test = test.qty_sold
    X_test = test.drop(['qty_sold','date'],axis=1)

    models1 = { 
        'LinearRegression': LinearRegression(),
        'RandomForestRegressor': RandomForestRegressor(),
        'GradientBoostingRegressor': GradientBoostingRegressor(),
        'XGBRegressor': XGBRegressor()
    }

    params1 = { 
        'LinearRegression': { },
        'RandomForestRegressor': { 'n_estimators':[50,100,200],'max_depth':[5,7,9,11], 'random_state':[0]},
        'GradientBoostingRegressor':  { 'n_estimators':[50,100,200],'max_depth':[5,7,9,11] ,'random_state':[0]},
        'XGBRegressor': { 'n_estimators':[50,100,200],'max_depth':[5,7,9,11] ,'random_state':[0], 'booster':['gbtree', 'gblinear'] }
    }

    mape_scorer=make_scorer(mape,greater_is_better=False)
    selection= EstimatorSelectionHelper(models1, params1)
    selection.fit(X_train,y_train, cv=10, n_jobs=-1,scoring=mape_scorer)
    gs_eval=selection.score_summary()
    gs_eval.reset_index(inplace=True)
    gs_eval.drop('index',axis=1,inplace=True)
    best_est=gs_eval.loc[0,'estimator']
    gs_eval

    filename=b+'-'+a+'-'+g
    test_est=selection.grid_searches[best_est].best_estimator_
    mp=mape(test_est.predict(X_test), y_test)
    print( 'Validation MAPE: %0.2f' %mp) 
    pred=pd.concat([test.date.reset_index(),y_test.reset_index(),pd.DataFrame(test_est.predict(X_test),columns=['pred'])], axis=1)
    pred['ape']=np.abs((pred['pred'] - pred['qty_sold']) / np.maximum(pred['qty_sold'],pred['pred']))
    pred['ae']=(pred['pred'] - pred['qty_sold']) /  np.maximum(pred['qty_sold'],pred['pred'])
    pred['bag']=filename
    pred.to_csv('/data/pratik/tensor/validations/nonlinear_validations/'+filename+'.csv',index=False)
    with open('/data/pratik/tensor/retrained_models/'+filename+'.pkl', 'wb') as f:
                    pickle.dump(test_est, f)
    ret=pd.DataFrame({ 'Brand': b,'Article_type': a,'Gender':g,'mape':mp}, index=[filename])
    print b+a+g+': completed'
    return ret


# In[ ]:

p = multiprocessing.Pool(10);


# In[ ]:

fit_data=p.map(get_best_model, lst)
final = pd.concat(fit_data)


# In[ ]:

mp=final.reset_index().reset_index()

dt = date.today().strftime('%d-%b-%Y')
mp.to_csv('/data/pratik/tensor/validations/retrained_models/validations_'+dt+'.csv',index=False)

