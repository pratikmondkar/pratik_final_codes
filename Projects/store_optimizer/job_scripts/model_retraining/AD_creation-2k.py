

# coding: utf-8

# In[1]:

import pandas as pd
import sqlalchemy as sq
import numpy as np
from datetime import datetime, timedelta

engine_prd = sq.create_engine("postgresql+psycopg2://analysis_user:DA@myntra4pr@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")
engine_cs = sq.create_engine("postgresql+psycopg2://analysis_user:DA@myntra4pr@dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")


# In[2]:

sql_str1="""
select a.*,b.ovr_lcount,c.sessions,d.loyalty_points,day_of_the_week,iso_week_of_the_year,week_number,month from
(select a.date,a.brand,a.article_type,product_gender as gender,
sum(sold_quantity) as qty_sold,
sum(atp_net_inv_qty) as inventory,
sum(live_styles) as live_styles,
sum(case when is_live_style=1 and days_since_cataloguing<=60 then live_styles else 0 end) as fresh_styles,
sum(broken_styles) as broken_styles,
sum(list_count) as list_count,
sum(atc_count) as atc_count,
sum(product_discount)/nullif(sum(total_mrp),0) as output_td,
sum(coupon_discount)/nullif(sum(total_mrp),0) as output_cd,
sum(total_mrp)-sum(nvl(product_discount,0)+nvl(revenue,0))/nullif(sum(total_mrp),0) as aux_discount
from customer_insights.fact_category_over_view_metrics a
join dev.bag_list b on a.brand=b.brand and a.article_type=b.article_type and a.product_gender=b.gender
where date >= 20160601
group by 1,2,3,4) a 
left join 
(select date,sum(list_count) as ovr_lcount from customer_insights.fact_category_over_view_metrics where date >= 20160601 group by 1) b on a.date=b.date
left join 
(select load_date as date,sum(sessions) as sessions from customer_insights.daily_traffic 
where load_date>=20160601 group by 1) c on a.date=c.date
left join dim_date dd on a.date=dd.full_date
left join 
(select order_created_date as date,brand,article_type,gender,
sum(loyalty_pts_used)/(2*sum(item_mrp_value*quantity)) as loyalty_points
FROM fact_core_item
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date >=20160601 
group by 1,2,3,4) d on a.date=d.date and a.brand=d.brand and a.article_type=d.article_type and a.gender=d.gender

"""
sql_str2="""
select order_created_date as date from
(select order_created_date,sum(item_revenue_inc_cashback+nvl(shipping_charges)+nvl(gift_charges)) as revenue
FROM fact_core_item
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date >20160601
group by 1)
where revenue>200000000
"""

sql_str3="""
select date,brand,article_type,gender,
sum(ros_15days) as last_15_ros,
sum(ros_15days*nvl(discount_rule_percentage,0)::float)/nullif(sum(ros_15days),0) as wgt_input_td,
avg(case when is_live_style=1 then nvl(discount_rule_percentage,0) end) as input_td 
from
(select date,brand,article_type,product_gender as gender,style_id,sold_quantity,is_live_style,
discount_rule_percentage,
avg(nvl(sold_quantity,0)::float) over (partition by style_id order by date  rows between 15 preceding and 1 preceding) as ros_15days
from fact_category_over_view_metrics
where date>=20160601)
group by 1,2,3,4
"""


# In[3]:

ad=pd.read_sql_query(sql_str1,engine_prd)
inp_td=pd.read_sql_query(sql_str3,engine_prd)


# In[4]:

month_index=pd.read_csv('/data/pratik/tensor/datasets/month_index.csv',error_bad_lines=False)
week_year_index=pd.read_csv('/data/pratik/tensor/datasets/week_year_index.csv',error_bad_lines=False)
week_month_index=pd.read_csv('/data/pratik/tensor/datasets/week_month_index.csv',error_bad_lines=False)
day_index=pd.read_csv('/data/pratik/tensor/datasets/day_index.csv',error_bad_lines=False)


# In[5]:

similar_brands=pd.read_csv('/data/pratik/tensor/datasets/similar_brands_collated.csv',error_bad_lines=False)
similar_brands.rename(columns={'rhs':'brand'},inplace=True)


# In[6]:

ad_final=ad.merge(inp_td,how='left',on=['date','brand','article_type','gender']).merge(month_index,how='left',on=['month','article_type','gender']).merge(week_year_index,how='left',on=['iso_week_of_the_year','article_type','gender']).merge(week_month_index,how='left',on=['week_number','article_type','gender']).merge(day_index,how='left',on=['day_of_the_week','article_type','gender'])
ad_final.head()


# In[7]:

hrd=pd.read_sql_query(sql_str2,engine_prd)
hrd_list=hrd['date'].tolist()


# In[8]:

presale_list=[]
for i in hrd_list:
    a=str(i)
    presale_list.append(int((datetime(year=int(a[0:4]), month=int(a[4:6]), day=int(a[6:8])) - timedelta(days=1)).date().strftime('%Y%m%d')))
postsale_list=[]
for i in hrd_list:
    a=str(i)
    postsale_list.append(int((datetime(year=int(a[0:4]), month=int(a[4:6]), day=int(a[6:8])) + timedelta(days=1)).date().strftime('%Y%m%d')))


# In[9]:

ad_final.fillna(0,inplace=True)
ad_final.head()


# In[10]:

ad_bau=ad_final[(~ad_final['date'].isin(hrd_list)) & (ad_final['live_styles']>0)]
print len(ad_final)
print len(ad_bau)


# In[15]:

ad_bau['bag_id']=ad_bau['brand']+ad_bau['article_type']+ad_bau['gender']
ad_bau.sort_values(by=['date','bag_id'],inplace=True)
ad_bau['brokeness']=ad_bau['broken_styles']/ad_bau['live_styles']
ad_bau['freshness']=ad_bau['fresh_styles']/ad_bau['live_styles']
ad_bau['lc_share_pltf']=ad_bau['list_count']*100/ad_bau['ovr_lcount']
ad_bau['ros_lag']=ad_bau.groupby(['bag_id'])['qty_sold'].shift(1)
ad_bau['ros_rm']=ad_bau.groupby(['bag_id'])['ros_lag'].rolling(15, min_periods=1).mean().reset_index(0,drop=True)
ad_bau['op_td_lag']=ad_bau.groupby(['bag_id'])['output_td'].shift(1)
ad_bau['output_td_rm']=ad_bau.groupby(['bag_id'])['op_td_lag'].rolling(15, min_periods=1).mean().reset_index(0,drop=True)
ad_bau['ip_td_lag']=ad_bau.groupby(['bag_id'])['wgt_input_td'].shift(1)
ad_bau['input_td_rm']=ad_bau.groupby(['bag_id'])['ip_td_lag'].rolling(15, min_periods=1).mean().reset_index(0,drop=True)
ad_bau['input_td_diff']=ad_bau['wgt_input_td']/ad_bau['input_td_rm']
ad_bau['output_td_diff']=ad_bau['output_td']/ad_bau['output_td_rm']
ad_bau['presale_flag']=0
ad_bau.loc[ad_bau['date'].isin(presale_list),'presale_flag']=1
ad_bau['postsale_flag']=0
ad_bau.loc[ad_bau['date'].isin(postsale_list),'postsale_flag']=1


# In[16]:

similar_final=similar_brands[similar_brands['lift']>1]
cannib=ad_bau[['date','brand','article_type','gender','wgt_input_td','lc_share_pltf']].merge(similar_final,how='left',on=['brand','article_type','gender'])
ratio=cannib.groupby(['date','lhs','article_type','gender']).agg({'wgt_input_td':{'price_mean':'mean','price_max':'max'},'lc_share_pltf':{'vis_mean':'mean','vis_max':'max'}}).reset_index(col_level=1)
ratio.rename(columns={'lhs':'brand'},inplace=True)
ratio.columns = ratio.columns.droplevel(0)


# In[17]:

ad_bau_trans=ad_bau.merge(ratio,how='left',on=['date','brand','article_type','gender'])
ad_bau_trans['price_cannib_max']=ad_bau_trans['wgt_input_td']/ad_bau_trans['price_max']
ad_bau_trans['price_cannib_mean']=ad_bau_trans['wgt_input_td']/ad_bau_trans['price_mean']
ad_bau_trans['vis_cannib_max']=ad_bau_trans['lc_share_pltf']/ad_bau_trans['vis_max']
ad_bau_trans['vis_cannib_mean']=ad_bau_trans['lc_share_pltf']/ad_bau_trans['vis_mean']
ad_bau_trans.drop(['price_max','price_mean','vis_mean','vis_max'],axis=1,inplace=True)


# In[18]:

ad_bau_trans['vis_lag']=ad_bau_trans['lc_share_pltf'].shift(1)
ad_bau_trans['vis_rm']=ad_bau_trans['vis_lag'].rolling(7, min_periods=1).mean()
ad_bau_trans['sales_lag']=ad_bau_trans['qty_sold'].shift(1)
ad_bau_trans['sales_rm']=ad_bau_trans['sales_lag'].rolling(7, min_periods=1).mean()
ad_bau_trans['atc_lag']=ad_bau_trans['atc_count'].shift(1)
ad_bau_trans['atc_rm']=ad_bau_trans['atc_lag'].rolling(7, min_periods=1).mean()
ad_bau_trans['price_cannib_lag']=ad_bau_trans['price_cannib_mean'].shift(1)
ad_bau_trans['price_cannib_rm']=ad_bau_trans['price_cannib_lag'].rolling(7, min_periods=1).mean()
ad_bau_trans['vis_cannib_lag']=ad_bau_trans['vis_cannib_mean'].shift(1)
ad_bau_trans['vis_cannib_rm']=ad_bau_trans['vis_cannib_lag'].rolling(7, min_periods=1).mean()
ad_bau_trans['brokenness_lag']=ad_bau_trans['brokeness'].shift(1)
ad_bau_trans['brokenness_rm']=ad_bau_trans['brokenness_lag'].rolling(7, min_periods=1).mean()
ad_bau_trans['freshness_lag']=ad_bau_trans['freshness'].shift(1)
ad_bau_trans['freshness_rm']=ad_bau_trans['freshness_lag'].rolling(7, min_periods=1).mean()
ad_bau_trans.fillna(0,inplace=True)


# In[19]:

ad_bau_trans.columns


# In[20]:

ad_bau_final=ad_bau_trans[ad_bau_trans['date']>=20160616][['date',
 'brand',
 'article_type',
 'gender',
 'bag_id',
 'qty_sold',
 'live_styles',
 'inventory',
 'wgt_input_td',
 'input_td_rm',
 'input_td_diff',
 'output_td',
 'output_td_rm',
 'output_td_diff',
 'brokeness',
 'freshness',
 'lc_share_pltf',
 'presale_flag',
 'postsale_flag',
 'sessions',
 'atc_count',
 'loyalty_points',
 'output_cd',
 'index_month',
 'index_week_year',
 'index_week_month',
 'index_day',
'vis_lag',
'vis_rm',
'sales_lag',
'sales_rm',
'atc_lag',
'atc_rm',
'price_cannib_lag',
'price_cannib_rm',
'vis_cannib_lag',
'vis_cannib_rm',
'brokenness_lag',
'brokenness_rm',
'freshness_lag',
'freshness_rm',
 'vis_cannib_max',
 'vis_cannib_mean',
 'price_cannib_max',
 'price_cannib_mean']]
ad_bau_final


compete=pd.read_csv('/data/pratik/tensor/trends/google_trends.csv',error_bad_lines=False)

compete['comp_index']=compete['Myntra']/compete['Amazon']
compete['date']=compete['date'].str.replace('-','').astype(int)
compete.drop_duplicates('date',inplace=True)

ad_bau_final=ad_bau_final.merge(compete[['date','comp_index']],on='date',how='left')

ad_bau_final.describe().T


# In[22]:

ad_bau_final[['input_td_diff','output_td_diff','price_cannib_max','price_cannib_mean']]=ad_bau_final[['input_td_diff','output_td_diff','price_cannib_max','price_cannib_mean']].fillna(1)
ad_bau_final.replace([np.inf, -np.inf], np.nan,inplace=True)
ad_bau_final[['input_td_diff','output_td_diff','price_cannib_max','price_cannib_mean']]=ad_bau_final[['input_td_diff','output_td_diff','price_cannib_mean','price_cannib_mean']].fillna(10)

ad_bau_final.replace([np.inf, -np.inf], np.nan,inplace=True)
ad_bau_final.fillna(0,inplace=True)


# In[24]:

ad_bau_final.loc[ad_bau_final['input_td_diff']>10,'input_td_diff']=10
ad_bau_final.loc[ad_bau_final['output_td_diff']>10,'output_td_diff']=10
ad_bau_final.loc[ad_bau_final['price_cannib_max']>10,'price_cannib_max']=10
ad_bau_final.loc[ad_bau_final['price_cannib_mean']>10,'price_cannib_mean']=10


# In[25]:

ad_bau_final.describe().T


# In[26]:

ad_bau_final.groupby('date')['lc_share_pltf'].sum()


# In[ ]:

ad_bau_final.to_csv('/data/pratik/tensor/datasets/ad_clean_2k_inp.csv',index=False)
