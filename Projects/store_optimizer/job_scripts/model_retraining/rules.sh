

spark-submit --master yarn --deploy-mode client --executor-memory 6g --driver-memory 6g --conf spark.yarn.executor.memoryOverhead=10G  --packages com.databricks:spark-redshift_2.11:3.0.0-preview1,com.databricks:spark-avro_2.11:4.0.0,com.databricks:spark-csv_2.11:1.3.0,com.amazonaws:aws-java-sdk-s3:1.11.21,org.apache.hadoop:hadoop-aws:2.7.1 --jars ~/affinity_spark/RedshiftJDBC42-1.2.1.1001.jar /home/hadoop/b2b_rules/similar_brands_pyspark.py
