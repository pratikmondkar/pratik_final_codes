from pyspark import SparkContext,SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.functions import *
from pyspark.ml.fpm import FPGrowth

conf = SparkConf().setAppName("App")
sc = SparkContext(conf=conf)

sc._jsc.hadoopConfiguration().set("fs.s3.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem")
sc._jsc.hadoopConfiguration().set("fs.s3.awsAccessKeyId","AKIAJ5HEDBVJTXTBSJNQ")
sc._jsc.hadoopConfiguration().set("fs.s3.awsSecretAccessKey", "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")

sql_context = SQLContext(sc)

#ag_list = sql_context.read.format('com.databricks.spark.csv').options(header='true').load('/Users/8201/PROJECTS/Project X/analytical_dataset.csv')

sql_str="""
select distinct device_id||load_date as id,brand 
from clickstream.discount_aggregates da
join dim_style ds on da.style_id=ds.style_id
where article_type='at_plc' and gender='gn_plc'
"""

ag_list_raw=sc.textFile("s3://testashutosh/backups/ag_list.csv")
ag_list=[x.split(",") for x in ag_list_raw.toLocalIterator()]
for ag in ag_list[81:]:
    sql_context.clearCache()
    at=ag[0]
    gn=ag[1]
    final_sql=sql_str.replace('at_plc',at)
    final_sql=final_sql.replace('gn_plc',gn)
    df = sql_context.read \
        .format("com.databricks.spark.redshift") \
        .option("url", "jdbc:redshift://dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw?user=analysis_user&password=Insight@123!") \
        .option("query", final_sql) \
        .option("tempdir", "s3://testashutosh/backups/temp/data") \
        .option("forward_spark_s3_credentials","true") \
        .load()
    df.cache()
    if df.count() <1000 : 
        continue
    else :
        trans = df.groupBy("id").agg(concat_ws(",",collect_list("brand")))
        trans = trans.withColumnRenamed("concat_ws(,, collect_list(brand))","brands")
        trans = trans.select(col("id"), split(col("brands"), ",\s*").alias("brands"))
        fpGrowth = FPGrowth(itemsCol="brands", minSupport=0.0001, minConfidence=0.05)
        model = fpGrowth.fit(trans)
        freqs=model.freqItemsets
        a=trans.agg(countDistinct(col("id")).alias("count")).collect()[0][0] 
        single_items=model.freqItemsets.filter(size('items')==1)
        rules=model.associationRules.filter(size('antecedent')==1)
        final=rules.join(single_items,rules.consequent==single_items.items,how='left').select('antecedent','consequent','confidence','freq')
        if final.count() <2 : 
            continue
        else :
            final=final.withColumn('support_y', final.freq / a )
            final=final.withColumn('lift', final.confidence / final.support_y ).drop('freq')
            final=final.withColumn('lhs', concat_ws(",", "antecedent")).drop('antecedent')
            final=final.withColumn('rhs', concat_ws(",", "consequent")).drop('consequent')
            final=final.withColumn('article_type',lit(at))
            final=final.withColumn('gender', lit(gn))
            at=at.replace(' ','_')
            final.write.csv('s3://testashutosh/backups/arules/'+at+'_'+gn+'/',header="true", mode="overwrite")
