
library(arules)
library(reshape2)
library(plyr)
library("RPostgreSQL")

con <- dbConnect(PostgreSQL(), host="dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com", port="5439", dbname="myntra_dw", user="analysis_user", password="Insight@123!")


ag_list<-read.csv(file="/data/pratik/tensor/ag_list.csv" ,header=TRUE, sep=",")
ag_list[1,]$article_type
nrow(ag_list)

sql_str="select distinct session_id,brand
from clickstream.sessionstylehour_view a
where a.article_type='%s' and a.gender='%s' and pdpviews>0 and load_date between to_char(sysdate- interval '7 days','YYYYMMDD')::bigint and to_char(sysdate- interval '1 day','YYYYMMDD')::bigint"

for (i in 1:nrow(ag_list)) {
    at=ag_list[i,]$article_type
    gn=ag_list[i,]$gender
    data <- dbGetQuery(con, sprintf(sql_str,at,gn))
    if (nrow(data)<1000) next 
    AggPosData<-split(data$brand,data$session_id)
    txns<-as(AggPosData,"transactions")
    Rules<-apriori(txns,parameter=list(supp=0.0006,conf=0.05,minlen=2,maxlen=2))
    rl = data.frame(lhs = labels(lhs(Rules)),rhs = labels(rhs(Rules)), Rules@quality)
    rl1<-rl[rl$lift>2,]
    if (nrow(rl1)<2) next 
    rl1<-rl1[with(rl1, order(-lift)), ]
    rl1$lhs<-gsub( "\\{", "", rl1$lhs)
    rl1$rhs<-gsub( "\\{", "", rl1$rhs)
    rl1$lhs<-gsub( "\\}", "", rl1$lhs)
    rl1$rhs<-gsub( "\\}", "", rl1$rhs)
    rl1$article_type<-at
    rl1$gender<-gn
    if (i==1) {final<-rl1}
    else {final<-rbind(rl1, final)}
    write.csv(rl1, file = paste0("/data/pratik/tensor/brand_groups/brand_arules_",at,gn,".csv"),row.names=FALSE, na="")
}
