
# coding: utf-8

# In[1]:

from pytrends.request import TrendReq
import pandas as pd
from datetime import date,timedelta

pytrends = TrendReq(hl='en-IN', tz=360)


# In[2]:

base_data=pd.read_csv('/data/pratik/tensor/trends/google_trends.csv',error_bad_lines=False)


# In[3]:

st_dt=base_data.date.max()
en_dt =date.today().strftime('%Y-%m-%d')
time_frame=st_dt+" "+en_dt
print time_frame


# In[4]:

kw_list=['Amazon','Myntra']
pytrends.build_payload(kw_list, cat=0, timeframe=time_frame, geo='IN', gprop='')
d=pytrends.interest_over_time()


# In[40]:

ratio=float(base_data[-1:]['Amazon'].values[0])/d[:1]['Amazon'].values[0]
print "ratio: " + str(ratio) +" "+str(float(base_data[-1:]['Amazon'].values[0])) + " / " + str(d[:1]['Amazon'].values[0])
scaled=d*ratio
scaled_new=scaled.drop('isPartial',axis=1).reset_index()
scaled_new['date']=scaled_new['date'].dt.date
final=base_data.append(scaled_new[1:]).reset_index()
final.drop('index',axis=1,inplace=True)


# In[43]:

final.to_csv('/data/pratik/tensor/trends/google_trends.csv',index=False)
final.to_csv('/data/pratik/tensor/trends/google_trends_'+en_dt+'.csv',index=False)

