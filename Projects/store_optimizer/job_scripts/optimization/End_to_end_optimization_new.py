
# coding: utf-8

# In[1]:

from __future__ import division
import pandas as pd
from pandas import DataFrame
import sqlalchemy as sq
import pickle
import numpy as np 
import scipy as scipy
from scipy import stats
from sklearn import linear_model, metrics, preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor
import xgboost as xgb
from xgboost import XGBRegressor
import multiprocessing
import gc
import sqlalchemy as sq
import pandas as pd
import json
import datetime
from pyomo.opt import SolverFactory
from pyomo.environ import *
import pdb

engine = sq.create_engine("postgresql+psycopg2://analysis_user:DA@myntra4pr@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")


sql_str3="""
select ([brand]+[article_type]+[product_gender]) as bag, brand, article_type,product_gender as gender,business_unit 
from fact_category_over_view_metrics 
where date= to_char(sysdate- interval '1 day','YYYYMMDD')::bigint and is_live_style=1 
group by brand, article_type, product_gender, business_unit
"""

sql_str4="""
select brand,article_type,gender,
sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as shipped_revenue,
sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0))/sum(quantity) as aisp,
sum(item_revenue_inc_cashback + nvl(shipping_charges,0) + nvl(gift_charges,0) - nvl(tax,0) - nvl(cogs,0) - nvl(royalty_commission,0) - nvl(entry_tax,0) - nvl(stn_input_vat_reversal,0))/sum(quantity) as rgm
FROM fact_core_item
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date =to_char(sysdate- interval '1 day','YYYYMMDD')::bigint
group by 1,2,3
"""


# In[3]:

bu_map=pd.read_sql_query(sql_str3,engine)
bu_map.drop_duplicates('bag',inplace=True)

bag_list=pd.read_csv('/data/pratik/tensor/bag_list_2000.csv',error_bad_lines=False)['bag'].tolist()

data=pd.read_csv('/data/pratik/tensor/datasets/ad_yday_validation.csv',error_bad_lines=False)

compete=pd.read_csv('/data/pratik/tensor/trends/google_trends.csv',error_bad_lines=False)
compete['comp_index']=compete['Myntra']/compete['Amazon']
compete['date']=compete['date'].str.replace('-','').astype(int)
compete.drop_duplicates('date',inplace=True)
data['comp_index']=compete.comp_index.mean()

ad_final=data
lst=ad_final[['brand','article_type','gender']].to_records(index=False).tolist()[200:250]


# In[9]:

add_info=pd.read_sql_query(sql_str4,engine)
add_info['bag_id']=add_info['brand']+add_info['article_type']+add_info['gender']


# In[10]:

with open('/data/pratik/tensor/datasets/ovr_dist_inp.pkl', 'rb') as f:
    ovr_dist = pickle.load(f)

def generate_lookup(lst):
    b=lst[0]
    a=lst[1]
    g=lst[2]

    bag=b+a+g
    dist=ovr_dist[ovr_dist['bag']==bag]
    x1=x2=0
    rnge=pd.DataFrame()
    v=np.random.normal(dist['mean'][0],dist['std'][0],100)
    dc=np.random.normal(dist['mean'][1],dist['std'][1],100)
    
    for a in v:    
        for b in dc:
            d=pd.DataFrame({'bag':bag,'lc_share_pltf': a, 'wgt_input_td': b}, index=[x1+x2])
            rnge=rnge.append(d)
            x2=x2+1
        x1=x1+1
    print "values generation for "+bag+" done."
    
    return rnge


# In[ ]:

p = multiprocessing.Pool(12)
lookups=p.map(generate_lookup, lst)
p.close()
final_lookups = pd.concat(lookups)
final_lookups.rename(columns={'bag':'bag_id'},inplace=True)


# In[25]:

#data.drop(['lc_share_pltf','output_td','brand','article_type','gender','fresh_styles','broken_styles','list_count','qty_sold','inventory','input_td','output_td_rm','output_td_diff','input_td_diff','inp_td_rm','wgt_input_td'],axis=1).head()
final_lookups.head()


# In[44]:
data['wgt_inp_td_rm']=data['wgt_input_td']/data['input_td_diff']

data.rename(columns={'bag':'bag_id'},inplace=True)
pred=final_lookups.merge(data.drop(['lc_share_pltf','output_td','brand','article_type','gender','qty_sold','inventory','output_td_rm','output_td_diff','input_td_diff','wgt_input_td'],axis=1),how='left',on='bag_id')
pred['input_td_diff']=pred['wgt_input_td']/pred['wgt_inp_td_rm']
pred['input_td_diff'].fillna(1,inplace=True)
pred.replace([np.inf, -np.inf], np.nan,inplace=True)
pred['input_td_diff'].fillna(10,inplace=True)


# In[45]:

ind_list=['wgt_input_td','input_td_diff','lc_share_pltf','live_styles','presale_flag','postsale_flag',
          'sessions','brokeness','freshness','output_cd','loyalty_points','vis_cannib_mean','comp_index','sales_lag',
          'index_week_year','price_cannib_mean','brokenness_rm','freshness_rm','atc_rm']
rearange_list=ind_list+list(set(pred.columns) - set(ind_list))
pred = pred.reindex_axis(rearange_list, axis=1)


# In[46]:

def gen_final_lookups(lst):
    b=lst[0]
    a=lst[1]
    g=lst[2]
    path_m=r'/data/pratik/tensor/models/'
    filename=b+'-'+a+'-'+g
    bag=b+a+g   
    with open(path_m+filename+'.pkl', 'rb') as f:
        model = pickle.load(f)
    dat=pred[pred['bag_id']==bag][ind_list]  
    dat['ros']=model.predict(dat)
    dat['bag']=bag
    print "lookup creation for "+bag+" done." 
    return dat


# In[47]:

p = multiprocessing.Pool(8)
ros_lookups=p.map(gen_final_lookups, lst)
p.close()
final_final_lookups = pd.concat(ros_lookups)


# In[56]:

add=data[data['bag_id'].isin(bag_list)].reset_index()
orig_values=add[['bag_id','lc_share_pltf','wgt_input_td','inventory','qty_sold']].merge(add_info[['bag_id','aisp','rgm','shipped_revenue']],how='left',on='bag_id')
orig_values['mrp']=orig_values['aisp']/(1-orig_values['wgt_input_td']/100)
orig_values.rename(columns={'bag_id':'bag'},inplace=True)
orig_values[['bag','lc_share_pltf','wgt_input_td','inventory','qty_sold','mrp','rgm']].head()


# In[59]:

lookup_file=final_final_lookups.merge(orig_values[['bag','lc_share_pltf','wgt_input_td','inventory','qty_sold','aisp','shipped_revenue','rgm','mrp']],how='left',on='bag')
lookup_file['asp']=(1-lookup_file['wgt_input_td_x']/100)*lookup_file['mrp']


# In[62]:

f1=lookup_file[((lookup_file['wgt_input_td_x']>=lookup_file['wgt_input_td_y']*0.85) & (lookup_file['wgt_input_td_x']<=lookup_file['wgt_input_td_y']*1.15)) | ((lookup_file['wgt_input_td_y']==0) & (lookup_file['wgt_input_td_x']<=0.15))]
dataset=f1[((f1['lc_share_pltf_x']>=f1['lc_share_pltf_y']*0.85) & (f1['lc_share_pltf_x']<=f1['lc_share_pltf_y']*1.15)) | ((f1['lc_share_pltf_y']<=0.01) & (f1['lc_share_pltf_x']<=0.02))]
final=dataset[dataset['ros']<dataset['inventory']].dropna()

final=final[(final['lc_share_pltf_x']>=0) & (final['wgt_input_td_x']>=0)]

# In[65]:

final['wgt_input_td_x']=final['wgt_input_td_x']/100
final['wgt_input_td_y']=final['wgt_input_td_y']/100


# In[71]:

final_df = final.reset_index()
date_str = (datetime.datetime.now()-datetime.timedelta(days=1)).strftime ("%Y%m%d")
final_df.to_csv(('bag_model_info' + date_str), index=False)


# In[72]:

JOIN_FOR_INVENTORY = 0
JOIN_FOR_ACTUAL_VALUES = 0
JOIN_FOR_ASP_RGM_REVENUE = 0
PERC = 0.15


final_df=final_df.dropna()




# In[73]:

name_dict={}
name_dict['wgt_input_td_x']='discount'
name_dict['lc_share_pltf_x']='visibility'
name_dict['lc_share_pltf_y']='real_visibility'
name_dict['wgt_input_td_y']='real_discount'
name_dict['rgm']='real_rgm'
name_dict['aisp']='real_asp'

final_df.rename(columns=name_dict, inplace=True)

final_df = final_df.sort_values('bag')
final_df =final_df.reset_index()

final_df = final_df.drop('index',axis=1)

final_df.index.name = "index"
final_df.index = final_df.index + 1

final_df['rgm'] = final_df['asp'] - final_df['real_asp'] + final_df['real_rgm']
final_df.head()

final_df.drop('bag',axis=1).to_csv('datafinal.tab',encoding="UTF_8", sep=' ')
final_df.to_csv('final_with_bag.csv',encoding="UTF_8", index=False)

final_df_grp = final_df.groupby('bag')['ros'].agg('count').reset_index()
final_df_grp['sum']=final_df_grp['ros'].cumsum()
final_df_grp = final_df_grp[['ros','sum']]
final_df_grp.index.name="index"
final_df_grp.index = final_df_grp.index + 1
final_df_grp.rename(columns={'ros':'count'}, inplace=True)

final_df_grp.to_csv('final_csv_agg.tab', sep=' ')


final_df_map=pd.merge(final_df, bu_map, how='left', on='bag')

total_visibility = final_df.groupby('bag').nth(0).visibility.sum()

total_discount_diff = (final_df.groupby('bag').nth(0).qty_sold * final_df.groupby('bag').nth(0).real_discount).sum()/ final_df.groupby('bag').nth(0).qty_sold.sum()


bu_dis_dict={}
bu_dis_dict["Women's Western Wear"] = 0.3279


# In[ ]:

# In[15]:



model = ConcreteModel()
var_perc = .15


model.ros = final_df['ros']
model.visibility = final_df['visibility']
model.asp = final_df['asp']
model.rgm = final_df['rgm']
model.real_asp = final_df['real_asp']
model.real_rgm = final_df['real_rgm']
model.qty_sold = final_df['qty_sold']



model.count = final_df_grp['count']
model.sum = final_df_grp['sum']

model.discount = final_df['discount']
model.ssize = final_df.index.size
model.bagSize = final_df_grp.index.size


model.x = Var(final_df.index, domain=Binary)

def obj(model):
    return sum(model.ros[i]*model.x[i]*model.asp[i] for i in range(1, 1+model.ssize))

model.obj = Objective(rule=obj, sense=maximize)

def visibility_rule(model):
    return (0, sum(model.x[i] * model.visibility[i] for i in range(1, 1+model.ssize)), total_visibility )

model.visibilityConst = Constraint(rule = visibility_rule)


def one_per_bag_rule(model, i):
    # return sum(model.x[j] for j in range( ((i-1)*model.bag_table_size) + 1, (i*model.bag_table_size) + 1 ) ) <= 1
    return sum(model.x[j] for j in range( model.sum[i] - model.count[i]+1, model.sum[i] + 1 )) <= 1

def discount_diff(model):
    return sum(model.x[i]*model.discount[i]*model.ros[i] for i in range(1, 1+model.ssize)) <= sum(model.x[i]*model.ros[i] for i in range(1, 1+model.ssize)) * total_discount_diff
#(sum(model.real_discount[i]*model.qty_sold[i] for i in model.ssize) / sum(model.ros[i])

    

def bu_constraint_discount(model, i):
    bu_eq = final_df_map[final_df_map['business_unit']==i]
    print i
    print bu_eq
    bu_eq.index+=1 #to make 0 index 1. check if it is one in the beginning
    return sum(model.x[j]*model.discount[j]*model.ros[j] for j in bu_eq.index.values) <= sum(model.x[j]*model.ros[j] for j in bu_eq.index.values) * bu_dis_dict[i]

def rgm_constraint(model):
    return sum(model.rgm[i] * model.ros[i] * model.x[i] for i in range(1, 1+model.ssize)) >= sum(model.ros[i] * model.asp[i] * model.x[i] for i in range(1, 1+model.ssize)) * (sum(model.qty_sold[model.sum[i]]*model.real_rgm[model.sum[i]] for i in range(1,1+model.bagSize))/sum(model.real_asp[model.sum[i]]*model.qty_sold[model.sum[i]] for i in range(1,1+model.bagSize)))


# def agg_discount_ratio(model, agg_level):
#     df = 

# model.buConstraintDiscountRule = Constraint(bu_dis_dict.keys(), rule=bu_constraint_discount)


model.onePerBagRule = Constraint(final_df_grp.index.values, rule=one_per_bag_rule)
model.discount_diff_rule = Constraint(rule=discount_diff)
model.rgm_constraint_rule = Constraint(rule=rgm_constraint)


# In[16]:


# In[ ]:

#import coopr.environ
opt = SolverFactory("glpk")
opt.options.mipgap=0.01
# data = DataPortal()
# data.load(filename='/Users/16546/myntra/revenue_linear_prog/test.dat', param=(model.bags,model.bag_table_size,model.size))
# data.load(filename='/Users/16546/myntra/revenue_linear_prog/final_csv_agg.tab', param=(model.count,model.sum), index=temp_size)
# pdb.set_trace()
instance = model.create()
#instance.pprint()
results = opt.solve(instance)
instance.solutions.store_to(results)
# print(results.json_repn())

# #sends results to stdout
# #print results
results.write()


# In[17]:



# result_file = open('/Users/16546/myntra/revenue_linear_prog/results.json')
# data = json.load(result_file)
data = results.json_repn()
sol_dict = data['Solution'][1]['Variable']

data_file = open('final_with_bag.csv')
data_file_arr=[]
for item in data_file:
    data_file_arr.append(item)

extracted_info=[]
write_file = open('final_results.csv','w')
write_file.write(data_file_arr[0].strip() + '\n')
for item in sol_dict:
#   extracted_info.append((data_file_arr[(int)(item.split('[')[1].split(']')[0]) - 0].strip()))
    if sol_dict[item]['Value'] == 1.0:
        #print item
        #print item.split('[')[1].split(']')[0]
        write_file.write((data_file_arr[(int)(item.split('[')[1].split(']')[0]) - 0].strip()) +'\n')

# write_file.write(extracted_info)
write_file.close()
# df_to_write = pd.DataFrame(x.split(',') for x in extracted_info)
# df_to_write.to_csv('/Users/16546/myntra/revenue_linear_prog/final_results.csv',index=False)



input = pd.read_csv('final_with_bag.csv')
res = pd.read_csv('final_results.csv')

res.columns

input = input.groupby('bag').nth(0)
#input = input.drop('output_td_diff',axis=1)
input = input.drop('ros',axis=1)
input = input.drop('discount',axis=1)
input = input.drop('visibility',axis=1)
input = input.drop('brokeness',axis=1)
input = input.drop('freshness',axis=1)
input = input.drop('live_styles',axis=1)
input = input.drop('presale_flag',axis=1)
input = input.drop('sessions',axis=1)
input = input.drop('asp',axis=1)
input = input.drop('mrp',axis=1)
input = input.drop('inventory',axis=1)
input = input.drop('rgm',axis=1)
# input = input.drop('index',axis=1)


res = res.drop('real_visibility',axis=1)
res = res.drop('real_discount',axis=1)
res = res.drop('inventory',axis=1)
res = res.drop('qty_sold',axis=1)
#res = res.drop('output_td_diff',axis=1)
res = res.drop('brokeness',axis=1)
res = res.drop('freshness',axis=1)
res = res.drop('live_styles',axis=1)
res = res.drop('presale_flag',axis=1)
res = res.drop('sessions',axis=1)
res = res.drop('real_rgm',axis=1)
res = res.drop('mrp',axis=1)
# res = res.drop('index',axis=1)

res = res.drop('shipped_revenue',axis=1)
res['pred_rev'] = res['ros'] * res['asp']
res = res.drop('real_asp',axis=1)

input = input.reset_index()

fin = pd.merge(input,res,how='left',on='bag')
fin = fin.fillna(0)


o=pd.merge(fin, bu_map, how='left', on='bag')


date_str = (datetime.datetime.now()-datetime.timedelta(days=1)).strftime ("%Y%m%d")
o.to_csv('final_result_at_bag_level' + date_str + '.csv')


o['discount']=o['discount']*o['ros']
o['real_discount']=o['real_discount']*o['qty_sold']

o['rgm_ratio_num'] = o['rgm'] * o['ros']
o['rgm_ratio_denom'] = o['asp'] * o['ros']

o['real_rgm_ratio_num'] = o['real_rgm'] * o['qty_sold']
o['real_rgm_ratio_denom'] = o['real_asp'] * o['qty_sold']

o=o.drop('asp',axis=1)
o=o.drop('real_asp',axis=1)
o=o.drop('real_rgm',axis=1)
o=o.drop('rgm',axis=1)

br = o.groupby('brand').sum()
at = o.groupby('article_type').sum()
bu = o.groupby('business_unit').sum()


br['real_discount'] = br['real_discount']/br['qty_sold']
br['discount'] = br['discount']/br['ros']
br['rgm_ratio'] = br['rgm_ratio_num'] / br['rgm_ratio_denom']
br['real_rgm_ratio'] = br['real_rgm_ratio_num'] / br['real_rgm_ratio_denom']
br=br.drop('rgm_ratio_num', axis=1)
br=br.drop('rgm_ratio_denom', axis=1)
br=br.drop('real_rgm_ratio_num', axis=1)
br=br.drop('real_rgm_ratio_denom', axis=1)

bu['discount'] = bu['discount']/bu['ros']
bu['real_discount'] = bu['real_discount']/bu['qty_sold']
bu['rgm_ratio'] = bu['rgm_ratio_num'] / bu['rgm_ratio_denom']
bu['real_rgm_ratio'] = bu['real_rgm_ratio_num'] / bu['real_rgm_ratio_denom']
bu=bu.drop('rgm_ratio_num', axis=1)
bu=bu.drop('rgm_ratio_denom', axis=1)
bu=bu.drop('real_rgm_ratio_num', axis=1)
bu=bu.drop('real_rgm_ratio_denom', axis=1)

at['real_discount'] = at['real_discount']/at['qty_sold']
at['discount'] = at['discount']/at['ros']
at['rgm_ratio'] = at['rgm_ratio_num'] / at['rgm_ratio_denom']
at['real_rgm_ratio'] = at['real_rgm_ratio_num'] / at['real_rgm_ratio_denom']
at=at.drop('rgm_ratio_num', axis=1)
at=at.drop('rgm_ratio_denom', axis=1)
at=at.drop('real_rgm_ratio_num', axis=1)
at=at.drop('real_rgm_ratio_denom', axis=1)

at.to_csv('at_rollup.csv')
bu.to_csv('bu_rollup.csv')
br.to_csv('br_rollup.csv')

