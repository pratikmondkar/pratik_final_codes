import pandas as pd
from pandas import DataFrame
import pickle
import math
import scipy as scipy
from scipy import stats
import numpy as np
import multiprocessing

data=pd.read_csv('/data/pratik/tensor/datasets/ad_clean_2k_inp.csv',error_bad_lines=False)

lst=data[['brand','article_type','gender']].drop_duplicates().to_records(index=False).tolist()

def get_stats(lst):
    b=lst[0]
    a=lst[1]
    g=lst[2]

    bag=b+a+g
    ad=data[(data['brand']==b) & (data['article_type']==a) & (data['gender']==g) ]    
    
    dist=pd.DataFrame()
    c=0
    for i in ['lc_share_pltf','wgt_input_td']:
        mean=ad[i].mean()
        std=ad[i].std()
        d=pd.DataFrame({'variable': i, 'mean':mean,'std':std}, index=[c])
        dist=dist.append(d)
        c=c+1
    dist['bag']=bag
    print "stats generation for "+bag+" done."
    return dist

p = multiprocessing.Pool(12)
distrb=p.map(get_stats, lst)
p.close()
ovr_dist = pd.concat(distrb)

with open('/data/pratik/tensor/datasets/ovr_dist_inp.pkl', 'wb') as f:
                pickle.dump(ovr_dist, f)