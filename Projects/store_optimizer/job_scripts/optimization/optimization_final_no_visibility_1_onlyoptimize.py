from __future__ import division
import pandas as pd
from pandas import DataFrame
import sqlalchemy as sq
import pickle
import numpy as np
import scipy as scipy
from scipy import stats
from sklearn import linear_model, metrics, preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor
import xgboost as xgb
from xgboost import XGBRegressor
import multiprocessing
import gc
import sqlalchemy as sq
import pandas as pd
import json
import datetime
from pyomo.opt import SolverFactory
from pyomo.environ import *
import pdb

def extract_sol_from_pyomo_result(result):
    data = results.json_repn()
    sol_dict = data['Solution'][1]['Variable']

    data_file = open('final_with_bag.csv')
    data_file_arr=[]
    for item in data_file:
        data_file_arr.append(item)

    extracted_info=[]
    write_file = open('final_results.csv','w')
    write_file.write(data_file_arr[0].strip() + '\n')
    for item in sol_dict:
    #   extracted_info.append((data_file_arr[(int)(item.split('[')[1].split(']')[0]) - 0].strip()))
        if sol_dict[item]['Value'] == 1.0:
            #print item
            #print item.split('[')[1].split(']')[0]
            write_file.write((data_file_arr[(int)(item.split('[')[1].split(']')[0]) - 0].strip()) +'\n')

    # write_file.write(extracted_info)
    write_file.close()
# df_to_write = pd.DataFrame(x.split(',') for x in extracted_info)
# df_to_write.to_csv('/Users/16546/myntra/revenue_linear_prog/final_results.csv',index=False)


def generate_final_output_files(opt_type):
    input = pd.read_csv('final_with_bag.csv')
    res = pd.read_csv('final_results.csv')

    res.columns

    input = input.groupby('bag').nth(0)
    input = input.drop('input_td_diff',axis=1)
    input = input.drop('ros',axis=1)
    input = input.drop('discount',axis=1)
    input = input.drop('visibility',axis=1)
    input = input.drop('brokeness',axis=1)
    input = input.drop('freshness',axis=1)
    input = input.drop('live_styles',axis=1)
    input = input.drop('presale_flag',axis=1)
    input = input.drop('sessions',axis=1)
    input = input.drop('asp',axis=1)
    input = input.drop('mrp',axis=1)
    input = input.drop('inventory',axis=1)
    input = input.drop('rgm',axis=1)
    # input = input.drop('index',axis=1)


    res = res.drop('real_visibility',axis=1)
    res = res.drop('real_discount',axis=1)
    res = res.drop('inventory',axis=1)
    res = res.drop('qty_sold',axis=1)
    res = res.drop('input_td_diff',axis=1)
    res = res.drop('brokeness',axis=1)
    res = res.drop('freshness',axis=1)
    res = res.drop('live_styles',axis=1)
    res = res.drop('presale_flag',axis=1)
    res = res.drop('sessions',axis=1)
    res = res.drop('real_rgm',axis=1)
    res = res.drop('mrp',axis=1)
    # res = res.drop('index',axis=1)

    res = res.drop('shipped_revenue',axis=1)
    res['pred_rev'] = res['ros'] * res['asp']
    res = res.drop('real_asp',axis=1)

    input = input.reset_index()

    fin = pd.merge(input,res,how='left',on='bag')
    fin = fin.fillna(0)



    query="select ([brand]+[article_type]+[product_gender]) as bag, brand, article_type, business_unit, product_gender from fact_category_over_view_metrics where date= to_char(sysdate- interval '1 day','YYYYMMDD')::bigint and is_live_style=1 group by brand, article_type, product_gender, business_unit"
    engine = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")
    bag_map=pd.read_sql_query(query,engine)
    bag_map=bag_map.drop_duplicates('bag')




    o=pd.merge(fin, bag_map, how='left', on='bag')


    date_str = (datetime.datetime.now()-datetime.timedelta(days=1)).strftime ("%Y%m%d")
    o.to_csv('/data/janus/daily_optimizer_results/final_result_at_bag_level_' +  opt_type + '_' + date_str + '.csv')


    o['discount']=o['discount']*o['ros']
    o['real_discount']=o['real_discount']*o['qty_sold']

    o['rgm_ratio_num'] = o['rgm'] * o['ros']
    o['rgm_ratio_denom'] = o['asp'] * o['ros']

    o['real_rgm_ratio_num'] = o['real_rgm'] * o['qty_sold']
    o['real_rgm_ratio_denom'] = o['real_asp'] * o['qty_sold']

    o=o.drop('asp',axis=1)
    o=o.drop('real_asp',axis=1)
    o=o.drop('real_rgm',axis=1)
    o=o.drop('rgm',axis=1)

    br = o.groupby('brand').sum()
    at = o.groupby('article_type').sum()
    bu = o.groupby('business_unit').sum()


    br['real_discount'] = br['real_discount']/br['qty_sold']
    br['discount'] = br['discount']/br['ros']
    br['rgm_ratio'] = br['rgm_ratio_num'] / br['rgm_ratio_denom']
    br['real_rgm_ratio'] = br['real_rgm_ratio_num'] / br['real_rgm_ratio_denom']
    br=br.drop('rgm_ratio_num', axis=1)
    br=br.drop('rgm_ratio_denom', axis=1)
    br=br.drop('real_rgm_ratio_num', axis=1)
    br=br.drop('real_rgm_ratio_denom', axis=1)

    bu['discount'] = bu['discount']/bu['ros']
    bu['real_discount'] = bu['real_discount']/bu['qty_sold']
    bu['rgm_ratio'] = bu['rgm_ratio_num'] / bu['rgm_ratio_denom']
    bu['real_rgm_ratio'] = bu['real_rgm_ratio_num'] / bu['real_rgm_ratio_denom']
    bu=bu.drop('rgm_ratio_num', axis=1)
    bu=bu.drop('rgm_ratio_denom', axis=1)
    bu=bu.drop('real_rgm_ratio_num', axis=1)
    bu=bu.drop('real_rgm_ratio_denom', axis=1)

    at['real_discount'] = at['real_discount']/at['qty_sold']
    at['discount'] = at['discount']/at['ros']
    at['rgm_ratio'] = at['rgm_ratio_num'] / at['rgm_ratio_denom']
    at['real_rgm_ratio'] = at['real_rgm_ratio_num'] / at['real_rgm_ratio_denom']
    at=at.drop('rgm_ratio_num', axis=1)
    at=at.drop('rgm_ratio_denom', axis=1)
    at=at.drop('real_rgm_ratio_num', axis=1)
    at=at.drop('real_rgm_ratio_denom', axis=1)

    at.to_csv('at_rollup.csv')
    bu.to_csv('bu_rollup.csv')
    br.to_csv('br_rollup.csv')

engine = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")
# coding: utf-8

# In[1]:


# In[71]:

final_df=pd.read_csv(('/data/janus/daily_optimizer_results/bag_model_info_no_vis_20171117'))


# In[72]:

JOIN_FOR_INVENTORY = 0
JOIN_FOR_ACTUAL_VALUES = 0
JOIN_FOR_ASP_RGM_REVENUE = 0
PERC = 0.15

query="select ([brand] + [article_type] + [gender]) as bag, sum(net_inventory_count) as s1 from bidb.fact_product_snapshot join bidb.dim_product on fact_product_snapshot.sku_id = dim_product.sku_id where date='20170525' group by brand, article_type, gender order by s1 desc;"
#engine = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

#df1 = pd.read_sql_query(query,engine)
# df2 = pd.read_csv("top_bag_lookup_final_filtered2.csv", encoding="utf-8-sig")
# final_df = df2
final_df=final_df.dropna()

if JOIN_FOR_INVENTORY == 1 :
        final_df = pd.merge(df2, df1, how='left')
        final_df = final_df.dropna()

if JOIN_FOR_ACTUAL_VALUES == 1:
        df = pd.read_csv('/Users/16546/myntra/revenue_linear_prog/actual_values.csv', encoding="utf-8-sig")
        df = df.rename(columns = {'output_td':'real_output_td','output_td_diff':'real_output_td_diff','lc_share_pltf':'real_lc_share_pltf'})
        final_df = pd.merge(final_df, df, how='left', on='name')
        final_df = final_df.dropna()

if JOIN_FOR_ASP_RGM_REVENUE == 1:
        df = pd.read_csv('/Users/16546/myntra/revenue_linear_prog/rgm_and_asp.csv', encoding="utf-8-sig")
        df['bag'] = df['brand'] + df['article_type'] + df['gender']
        final_df = pd.merge(final_df, df[['shipped_revenue','RGM','ASP','bag']], how='left', on='bag')

print final_df.columns
print type(final_df)
# final_df = final_df[(final_df['output_td_x']<=final_df['output_td_y']* (1 + PERC) ) & (final_df['output_td_x']>=final_df['output_td_y']* ( 1 - PERC) )]
# final_df = final_df[(final_df['lc_share_pltf_x']<=final_df['lc_share_pltf_y']* (1 + PERC) ) & (final_df['lc_share_pltf_x']>=final_df['lc_share_pltf_y']* ( 1 - PERC) )]
# final_df = final_df[final_df['ros']<=final_df['inventory']]
# final_df = final_df.dropna()


# In[73]:

name_dict={}
name_dict['wgt_input_td_x']='discount'
name_dict['lc_share_pltf_x']='visibility'
name_dict['lc_share_pltf_y']='real_visibility'
name_dict['wgt_input_td_y']='real_discount'
name_dict['rgm']='real_rgm'
name_dict['aisp']='real_asp'

final_df.rename(columns=name_dict, inplace=True)

final_df = final_df.sort_values('bag')
final_df =final_df.reset_index()

final_df = final_df.drop('index',axis=1)

final_df.index.name = "index"
final_df.index = final_df.index + 1

final_df['rgm'] = final_df['asp'] - final_df['real_asp'] + final_df['real_rgm']
final_df.head()

final_df.drop('bag',axis=1).to_csv('datafinal.tab',encoding="UTF_8", sep=' ')
final_df.to_csv('final_with_bag.csv',encoding="UTF_8", index=False)

final_df_grp = final_df.groupby('bag')['ros'].agg('count').reset_index()
final_df_grp['sum']=final_df_grp['ros'].cumsum()
final_df_grp = final_df_grp[['ros','sum']]
final_df_grp.index.name="index"
final_df_grp.index = final_df_grp.index + 1
final_df_grp.rename(columns={'ros':'count'}, inplace=True)

final_df_grp.to_csv('final_csv_agg.tab', sep=' ')


# In[74]:

query="select ([brand]+[article_type]+[product_gender]) as bag, brand, article_type, business_unit from fact_category_over_view_metrics where date=to_char(sysdate- interval '1 day','YYYYMMDD')::bigint and is_live_style=1 group by brand, article_type, product_gender, business_unit"
engine = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")
bag_map=pd.read_sql_query(query,engine)
bag_map=bag_map.drop_duplicates('bag')


final_df_map=pd.merge(final_df, bag_map, how='left', on='bag')

total_visibility = final_df.groupby('bag').nth(0).visibility.sum()

total_discount_diff = (final_df.groupby('bag').nth(0).qty_sold * final_df.groupby('bag').nth(0).real_discount).sum()/ final_df.groupby('bag').nth(0).qty_sold.sum()

total_revenue = (final_df.groupby('bag').nth(0).real_asp * final_df.groupby('bag').nth(0).qty_sold).sum()

print "total revenue is - ",total_revenue

bu_dis_dict={}


# In[ ]:

# In[15]:



model = ConcreteModel()
var_perc = .15


model.ros = final_df['ros']
model.visibility = final_df['visibility']
model.asp = final_df['asp']
model.rgm = final_df['rgm']
model.real_asp = final_df['real_asp']
model.real_rgm = final_df['real_rgm']
model.qty_sold = final_df['qty_sold']



model.count = final_df_grp['count']
model.sum = final_df_grp['sum']

model.discount = final_df['discount']
model.ssize = final_df.index.size
model.bagSize = final_df_grp.index.size


model.x = Var(final_df.index, domain=Binary)

def obj(model):
	return sum(model.ros[i]*model.x[i]*model.asp[i] for i in range(1, 1+model.ssize))

model.obj_revenue = Objective(rule=obj, sense=maximize)

def obj_profit(model):
    return sum(model.rgm[i] * model.ros[i] * model.x[i] for i in range(1, 1+model.ssize))

model.obj_profit = Objective(rule=obj_profit, sense=maximize)

def visibility_rule(model):
	return (0, sum(model.x[i] * model.visibility[i] for i in range(1, 1+model.ssize)), total_visibility )

#model.visibilityConst = Constraint(rule = visibility_rule)


def one_per_bag_rule(model, i):
	# return sum(model.x[j] for j in range( ((i-1)*model.bag_table_size) + 1, (i*model.bag_table_size) + 1 ) ) <= 1
	return sum(model.x[j] for j in range( model.sum[i] - model.count[i]+1, model.sum[i] + 1 )) <= 1

def discount_diff(model):
	return sum(model.x[i]*model.discount[i]*model.ros[i] for i in range(1, 1+model.ssize)) <= sum(model.x[i]*model.ros[i] for i in range(1, 1+model.ssize)) * total_discount_diff
#(sum(model.real_discount[i]*model.qty_sold[i] for i in model.ssize) / sum(model.ros[i])

	

def bu_constraint_discount(model, i):
    bu_eq = final_df_map[final_df_map['business_unit']==i]
    print i
    print bu_eq
    bu_eq.index+=1 #to make 0 index 1. check if it is one in the beginning
    return sum(model.x[j]*model.discount[j]*model.ros[j] for j in bu_eq.index.values) <= sum(model.x[j]*model.ros[j] for j in bu_eq.index.values) * bu_dis_dict[i]

def rgm_constraint(model):
	return sum(model.rgm[i] * model.ros[i] * model.x[i] for i in range(1, 1+model.ssize)) >= sum(model.ros[i] * model.asp[i] * model.x[i] for i in range(1, 1+model.ssize)) * (sum(model.qty_sold[model.sum[i]]*model.real_rgm[model.sum[i]] for i in range(1,1+model.bagSize))/sum(model.real_asp[model.sum[i]]*model.qty_sold[model.sum[i]] for i in range(1,1+model.bagSize)))

def revenue_constraint(model):
    return sum(model.ros[i] * model.x[i] * model.asp[i] for i in range(1, 1+model.ssize)) >= total_revenue

# def agg_discount_ratio(model, agg_level):
#     df = 

# model.buConstraintDiscountRule = Constraint(bu_dis_dict.keys(), rule=bu_constraint_discount)


model.onePerBagRule = Constraint(final_df_grp.index.values, rule=one_per_bag_rule)
model.discount_diff_rule = Constraint(rule=discount_diff)
model.rgm_constraint_rule = Constraint(rule=rgm_constraint)
model.revenue_constraint_rule = Constraint(rule=revenue_constraint)


model.obj_profit.deactivate()
model.revenue_constraint_rule.deactivate()

# In[16]:


# In[ ]:

#import coopr.environ
opt = SolverFactory("gurobi")
opt.options.MIPGap=0.01
# data = DataPortal()
# data.load(filename='/Users/16546/myntra/revenue_linear_prog/test.dat', param=(model.bags,model.bag_table_size,model.size))
# data.load(filename='/Users/16546/myntra/revenue_linear_prog/final_csv_agg.tab', param=(model.count,model.sum), index=temp_size)
# pdb.set_trace()
#instance = model.create()
#instance.pprint()
results = opt.solve(model)
model.solutions.store_to(results)
# print(results.json_repn())

# #sends results to stdout
# #print results
results.write()
extract_sol_from_pyomo_result(results)
generate_final_output_files('revenue_no_vis')

#model.obj_profit.activate()
#model.revenue_constraint_rule.activate()
#model.obj_revenue.deactivate()
#model.rgm_constraint_rule.deactivate()

#instance = model.create()
#results = opt.solve(model)
#model.solutions.store_to(results)
#extract_sol_from_pyomo_result(results)
#generate_final_output_files('margin')



# In[17]:



# result_file = open('/Users/16546/myntra/revenue_linear_prog/results.json')
# data = json.load(result_file)

