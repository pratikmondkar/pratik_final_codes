create table dev.lp_tag_by_source sortkey(load_date) as
(select a.*,b.final_segment,b.brand,b.article_type,b.gender
from
(SELECT session_end_date as load_date,
			 CASE
         WHEN ref_event_type IN ('RightNavChildClick','Level1LeftNavClick','Level2LeftNavClick','Level3LeftNavClick','topNavClick') THEN 'Burger menu'
         WHEN ref_event_type IN ('streamCardClick','streamCardChildClick','feedCardClick','feedCardChildClick') THEN 'Banner or Card'
         WHEN ref_event_type IN ('SearchFired','searchFired') THEN 'Search'
         ELSE 'others'
       END AS source,
       landing_screen AS screen_name,
       count(*) as lp
FROM customer_insights.funnel_view
WHERE (landing_screen LIKE '%Shopping Page-List%' OR landing_screen LIKE '%Shopping Page-Search%')
AND   session_end_date >= to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '2 days','YYYYMMDD')::bigint
GROUP BY 1,
         2,
         3) a
         left join 
         (select distinct load_date,screen_name,final_segment,brand,article_type,gender from lp_bag_attribution where load_date >= to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '2 days','YYYYMMDD')::bigint) b 
         on a.load_date=b.load_date and a.screen_name=b.screen_name);


CREATE TABLE dev.pi_score distkey 
(
  style_id   
)
AS
(SELECT a.source,
       a.session_end_date as load_date,
       a.style_id,
       b.article_type,
       b.cluster_id,
       nvl(SUM(pdp),0) AS pdp,
       nvl(SUM(atc),0) AS atc,
       nvl(SUM(atwl),0) AS atwl,
       nvl(SUM(atcl),0) AS atcl,
       nvl(SUM(pdplike),0) AS pdplike
FROM (SELECT CASE
               WHEN ref_event_type IN ('RightNavChildClick','Level1LeftNavClick','Level2LeftNavClick','Level3LeftNavClick','topNavClick') THEN 'Burger menu'
               WHEN ref_event_type IN ('streamCardClick','streamCardChildClick','feedCardClick','feedCardChildClick') THEN 'Banner or Card'
               WHEN ref_event_type IN ('SearchFired','searchFired') THEN 'Search'
               ELSE 'others'
             END AS source,
             session_end_date,
             style_id,
             nvl(SUM(a.pdp),0) AS pdp,
             nvl(SUM(a.atc),0) AS atc,
             nvl(SUM(a.added_to_collection),0) AS atcl,
             nvl(SUM(a.addToList),0) AS atwl,
             nvl(SUM(a.pdplike),0) AS pdplike
      FROM (SELECT ref_event_type,
                   data_set_value AS style_id,
                   session_end_date,
                   COUNT(DISTINCT CASE WHEN event_type IN ('ScreenLoad') AND landing_screen LIKE 'Shopping Page-PDP%' THEN event_id END) AS pdp,
                   COUNT(DISTINCT CASE WHEN event_type IN ('addToCart','moveToCart') THEN event_id END) AS atc,
                   COUNT(DISTINCT CASE WHEN event_type IN ('AddToCollection') THEN event_id END) AS added_to_collection,
                   COUNT(DISTINCT CASE WHEN event_type IN ('addToList') THEN event_id END) AS addToList,
                   COUNT(DISTINCT CASE WHEN event_type IN ('pdpLike') THEN event_id END) AS pdplike
            FROM customer_insights.funnel_view
            WHERE event_type IN ('ScreenLoad','addToCart','AddToCollection','addToList','pdpLike','moveToCart')
            AND   session_end_date >= to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '2 days','YYYYMMDD')::bigint
            and   data_set_value SIMILAR TO '[0-9]{6,8}' and len(data_set_value)<10
            GROUP BY 1,
                     2,
                     3) a 
      GROUP BY 1,
               2,
               3) a
  LEFT JOIN customer_insights.style_cluster_map b ON a.style_id = b.style_id
GROUP BY 1,
         2,
         3,
         4,
         5);