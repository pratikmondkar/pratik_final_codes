drop table if exists dev.mfg_base;
create table dev.mfg_base distkey(uidx) as 
(select distinct uidx,style_id from bidb.collection_log_eors where action =1 and added_date>=to_char(sysdate- interval '21 days','YYYYMMDD')::bigint
and uidx in ('032236d7.e4a6.4809.bcbc.fc277135ae16JMYG1fnD6X',	'1397cf42.2ac2.4dc6.90bb.119846922aa6M1K7KPNix1',	'1ade1667.2901.4b84.9acd.52c2c77aa636bZVh5QKBy8',	'36a3ec03.ef64.415d.916e.f5f2d793b606Ony5YC8QxQ',	'48d8112e.62d3.476e.9c0f.e8d4a48b443eUnTacZeQZK',	'5665d440.353e.4768.a9cd.9bf8803359f2NaVLGMX6OK',	'c3e9ec26.2f47.4be2.a637.768ceb27430e2oTkSLSw82',	'b33fc3f1.e389.4f42.9d46.514886ec07b7E6nb1dDW3v',	'be4f1044.095f.46d9.a03c.f1e17285d635jO82bh4F6w',	'cf3cc993.8e35.4226.89b9.c36d4706770coTMVFO0bS5',	'eff394ce.a5d0.488a.b6b2.51d343a00e2e2q4gd5WLhN',	'f8f6be97.254b.42f6.90f6.942d5f98bbbfm2JO6VWOOs'));


alter table dev.mfg_base add column group_id bigint;

update dev.mfg_base 
set group_id=src.group_id
from
(select uidx,1 as group_id
from
(select distinct uidx from dev.mfg_base)) src
where src.uidx=dev.mfg_base.uidx;


drop table if exists dev.mfg_user_group_map;
create table dev.mfg_user_group_map distkey(group_id) as 
(select distinct uidx,group_id from dev.mfg_base);

drop table if exists dev.mfg_user_style_filter ;
create table dev.mfg_user_style_filter distkey(uidx) as 
(select distinct uidx,style_id from dev.mfg_base
union 
select distinct customer_login as uidx ,style_id::bigint
from fact_core_item fci 
join dim_customer_idea dci on fci.idcustomer=dci.id
join (select distinct uidx from dev.mfg_base) c on dci.customer_login=c.uidx
where is_shipped=1 and store_id=1);


drop table if exists dev.mfg_user_gender_excl ;
create table dev.mfg_user_gender_excl distkey(uidx) as 
(select uidx,gender from
(select uidx,
count(distinct case when gender in ('Women','Girls') then 'Women' when gender in ('Men','Boys') then 'Men' end) as cnt,
max(case when gender in ('Women','Girls') then 'Women' when gender in ('Men','Boys') then 'Men' end) as gender
from dev.mfg_base a 
left join dim_style ds on a.style_id=ds.style_id
group by 1)
where cnt=1);




drop table if exists  dev.mfg_bag_lifetime;
create table dev.mfg_bag_lifetime distkey(uidx) as
(SELECT uidx,brand,article_type,gender,rnk,score
             FROM (SELECT user_id as uidx,
                          brand,article_type,gender,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) + 25*sum(units_bought) AS score,
                          ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime a
                   join (select distinct uidx from dev.mfg_base) b on a.user_id=b.uidx
                   where identifier='uidx' and article_type in ('Kurtas',	'Dresses',	'Handbags',	'Shorts',	'Tshirts',	'Jeans',	'Tops',	'Flats',	'Sweatshirts',	'Track Pants',	'Formal Shoes',	'Blazers',	'Shirts',	'Trousers',	'Heels',	'Sunglasses',	'Sandals',	'Backpacks',	'Flip Flops',	'Skirts')
                   GROUP BY 1,2,3,4)
             WHERE rnk <= 5 and score>=5);

alter table dev.mfg_bag_lifetime add column total_score float8;
alter table dev.mfg_bag_lifetime add column style_slots bigint;

update dev.mfg_bag_lifetime 
set total_score=src.total_score,
 		style_slots=(score::float8/nullif(src.total_score,0))*24
from
(select uidx,sum(score) as total_score
from dev.mfg_bag_lifetime
group by 1) src
where src.uidx=dev.mfg_bag_lifetime.uidx;


drop table if exists  dev.mfg_bag_st;
create table dev.mfg_bag_st distkey(uidx) as
(SELECT uidx,brand,article_type,gender,rnk,score
             FROM (SELECT a.uidx,
                          brand,article_type,gender,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) + 25*sum(units_bought) AS score,
                          ROW_NUMBER() OVER (PARTITION BY a.uidx ORDER BY score DESC) AS rnk
                   FROM bidb.notif_personalize a
                   join (select distinct uidx from dev.mfg_base) b on a.uidx=b.uidx
                   where article_type in ('Kurtas',	'Dresses',	'Handbags',	'Shorts',	'Tshirts',	'Jeans',	'Tops',	'Flats',	'Sweatshirts',	'Track Pants',	'Formal Shoes',	'Blazers',	'Shirts',	'Trousers',	'Heels',	'Sunglasses',	'Sandals',	'Backpacks',	'Flip Flops',	'Skirts')
                   GROUP BY 1,2,3,4)
             WHERE rnk <= 5 and score>=5);

alter table dev.mfg_bag_st add column total_score float8;
alter table dev.mfg_bag_st add column style_slots bigint;

update dev.mfg_bag_st 
set total_score=src.total_score,
 		style_slots=(score::float8/nullif(src.total_score,0))*24
from
(select uidx,sum(score) as total_score
from dev.mfg_bag_st
group by 1) src
where src.uidx=dev.mfg_bag_st.uidx;


drop table if exists dev.mfg_bag_to_styles;

create table dev.mfg_bag_to_styles as
(select brand,article_type,gender,style_id,pi_score from
(select brand,article_type,gender,a.style_id,pi_score,PERCENT_RANK() OVER (PARTITION BY brand,article_type,gender ORDER BY pi_score DESC) AS perc from
(select style_id,sum(norm_pi_score)/count(distinct case when is_live_style=1 then date end) as pi_score
from customer_insights.pi_metrics
where date>=to_char(sysdate -  interval '15 days','YYYYMMDD')::bigint
group by 1) a
left join dim_style ds on a.style_id=ds.style_id
join (select style_id,count(distinct case when o.style_status='P' and net_inventory_count>0 then o.sku_id end) as live_skus,count(distinct o.sku_id) as total_skus 
			from o_atp_inventory  o
			join dim_product dp on o.sku_id=dp.sku_id 
			group by 1) b 
on a.style_id=b.style_id and total_skus=live_skus
where pi_score is not null)
WHERE perc <= 0.1);

drop table if exists dev.mfg_bag_to_styles_all;
create table dev.mfg_bag_to_styles_all as
(select brand,article_type,gender,style_id,pi_score from
(select brand,article_type,gender,a.style_id,pi_score,PERCENT_RANK() OVER (PARTITION BY brand,article_type,gender ORDER BY pi_score DESC) AS perc from
(select style_id,sum(norm_pi_score)/count(distinct case when is_live_style=1 then date end) as pi_score
from customer_insights.pi_metrics
where date>=to_char(sysdate -  interval '15 days','YYYYMMDD')::bigint
group by 1) a
left join dim_style ds on a.style_id=ds.style_id
join (select style_id,count(distinct case when o.style_status='P' and net_inventory_count>0 then o.sku_id end) as live_skus,count(distinct o.sku_id) as total_skus 
				from o_atp_inventory o
				join dim_product dp on o.sku_id=dp.sku_id
				group by 1) b 
on a.style_id=b.style_id and total_skus=live_skus
where pi_score is not null));



drop table if exists dev.mfg_user_hist_final;
create table dev.mfg_user_hist_final distkey(uidx) as
(select uidx,brand,article_type,gender,flag,style_id,style_slots,rnk from
(select a.uidx,a.brand,a.article_type,a.gender,b.style_id,style_slots,flag,row_number() OVER (PARTITION BY a.uidx,a.brand,a.article_type,a.gender,a.flag ORDER BY random() DESC) AS rnk from 
(SELECT s1.uidx,brand,article_type,gender,rnk,0 as flag,style_slots from dev.mfg_bag_st s1 join (select distinct uidx from dev.mfg_base) u on s1.uidx=u.uidx
union all 
SELECT s2.uidx,brand,article_type,gender,rnk, 1 as flag,style_slots from dev.mfg_bag_lifetime s2 join (select distinct uidx from dev.mfg_base) u on s2.uidx=u.uidx) a
join dev.mfg_bag_to_styles b on a.brand=b.brand and a.article_type=b.article_type and a.gender=b.gender
left join dev.mfg_user_style_filter c on a.uidx=c.uidx and b.style_id=c.style_id
where c.style_id is null)
where rnk<=style_slots);


drop table if exists dev.mfg_clus_group_int;

create table dev.mfg_clus_group_int distkey(group_id) as 
(select * from
(select a.*,c.total_cus,cl_cus::float8/total_cus as gi,row_number() over (partition by a.group_id order by gi desc) as rnk 
	from
			(select group_id,cluster_id,article_type,count(distinct uidx) as cl_cus
			from dev.mfg_base b join style_cluster_map cm on b.style_id=cm.style_id
			where article_type in ('Kurtas',	'Dresses',	'Handbags',	'Shorts',	'Tshirts',	'Jeans',	'Tops',	'Flats',	'Sweatshirts',	'Track Pants',	'Formal Shoes',	'Blazers',	'Shirts',	'Trousers',	'Heels',	'Sunglasses',	'Sandals',	'Backpacks',	'Flip Flops',	'Skirts')
			group by 1,2,3) a
	left join 
			(select group_id,count(distinct uidx) as total_cus from dev.mfg_base group by 1) c 
	on a.group_id=c.group_id)
	where rnk<=10);

alter table dev.mfg_clus_group_int add column total_gi float8;
alter table dev.mfg_clus_group_int add column style_slots bigint;

update dev.mfg_clus_group_int 
set total_gi=src.total_gi,
 		style_slots=(gi::float8/nullif(src.total_gi,0))*48
from
(select group_id,sum(gi) as total_gi
from dev.mfg_clus_group_int
group by 1) src
where src.group_id=dev.mfg_clus_group_int.group_id;

drop table if exists dev.mfg_cluster_to_styles;

create table dev.mfg_cluster_to_styles as
(select cluster_id,article_type,gender,style_id from
(select cm.article_type,cluster_id,ds.gender,a.style_id,pi_score,PERCENT_RANK() OVER (PARTITION BY cm.article_type,cluster_id ORDER BY pi_score DESC) AS perc,
row_number() OVER (PARTITION BY cm.article_type,cluster_id ORDER BY pi_score DESC) AS rn from
(select style_id,sum(norm_pi_score)/count(distinct case when is_live_style=1 then date end) as pi_score
from customer_insights.pi_metrics
where date>=to_char(sysdate -  interval '15 days','YYYYMMDD')::bigint
group by 1) a
left join customer_insights.style_cluster_map cm on a.style_id=cm.style_id
join (select style_id,count(distinct case when o.style_status='P' and net_inventory_count>0 then o.sku_id end) as live_skus,count(distinct o.sku_id) as total_skus 
				from o_atp_inventory o
				join dim_product dp on o.sku_id=dp.sku_id
				group by 1) b 
on a.style_id=b.style_id and total_skus=live_skus
left join dim_style ds on a.style_id=ds.style_id
where pi_score is not null)
where perc<0.1 or rn<=30);


drop table if exists dev.mfg_temp2;
create table dev.mfg_temp2 distkey(uidx) as
(select d.uidx,a.group_id,a.cluster_id,a.article_type,b.gender,b.style_id,style_slots from 
dev.mfg_clus_group_int a
join dev.mfg_user_group_map d on a.group_id=d.group_id
join dev.mfg_cluster_to_styles b on a.cluster_id=b.cluster_id and a.article_type=b.article_type);

drop table if exists dev.mfg_group_final;
create table dev.mfg_group_final distkey(uidx) as
(select uidx,group_id,cluster_id,article_type,style_id,style_slots,rnk,d_uidx,d_gender,a_gender from
(select a.uidx,a.group_id,cluster_id,article_type,a.style_id,style_slots,d.uidx as d_uidx,d.gender as d_gender ,a.gender as a_gender ,
row_number() OVER (PARTITION BY a.uidx,a.cluster_id,a.article_type ORDER BY random() DESC) AS rnk from
dev.mfg_temp2 a
left join dev.mfg_user_style_filter c on a.uidx=c.uidx and a.style_id=c.style_id
left join dev.mfg_user_gender_excl d on a.uidx=d.uidx 
where c.style_id is null and (d.uidx is null or d.gender=a.gender))
where rnk<=style_slots);



drop table if exists dev.mfg_group_defaults;
create table dev.mfg_group_defaults distkey(group_id) as 
(select c.*,d.total_users,users::float8/total_users as gi from
(select group_id,brand,article_type,gender,count(distinct a.uidx) as users
from dev.mfg_bag_lifetime a
join 
(select distinct uidx,group_id from dev.mfg_base) b on a.uidx=b.uidx
group by 1,2,3,4) c
left join (select group_id,count(distinct uidx) as total_users from dev.mfg_base group by 1) d 
on c.group_id=d.group_id
where users>1);

alter table dev.mfg_group_defaults add column total_gi float8;
alter table dev.mfg_group_defaults add column slots bigint;

update dev.mfg_group_defaults 
set total_gi=src.total_gi,
 		slots=(gi::float8/nullif(src.total_gi,0))*48
from
(select group_id,sum(gi) as total_gi
from dev.mfg_group_defaults
group by 1) src
where src.group_id=dev.mfg_group_defaults.group_id;


drop table if exists dev.mfg_temp1;
create table dev.mfg_temp1 distkey(uidx) as
(select d.uidx,a.group_id,a.brand,a.article_type,a.gender,b.style_id,slots,pi_score from 
dev.mfg_group_defaults a
join dev.mfg_user_group_map d on a.group_id=d.group_id
join dev.mfg_bag_to_styles_all b on a.brand=b.brand and a.article_type=b.article_type and a.gender=b.gender);

drop table if exists dev.mfg_grp_defualt_style;
create table dev.mfg_grp_defualt_style distkey(uidx) as
(select uidx,group_id,brand,article_type,gender,style_id,slots,rnk from
(select a.uidx,a.group_id,brand,article_type,a.gender,a.style_id,slots,row_number() OVER (PARTITION BY a.uidx,a.brand,a.article_type,a.gender ORDER BY pi_score DESC) AS rnk from
dev.mfg_temp1 a
left join dev.mfg_user_style_filter c on a.uidx=c.uidx and a.style_id=c.style_id
left join dev.mfg_user_gender_excl d on a.uidx=d.uidx 
where c.style_id is null and (d.uidx is null or d.gender=a.gender))
where rnk<=slots);




drop table if exists dev.mfg_user_hist_fill1;
create table dev.mfg_user_hist_fill1 distkey(uidx) as
select distinct uidx,style_id,1 as flag,style_slots from dev.mfg_user_hist_final
union all 
(select uidx,style_Id,2 as flag, slots as style_slots from
(select a.uidx,a.style_Id,c.cnt,row_number() over (partition by a.uidx order by slots desc,rnk) as rn from
dev.mfg_grp_defualt_style a
left join (select distinct uidx,style_id from dev.mfg_user_hist_final) b on a.uidx=b.uidx and a.style_id=b.style_id
left join (select uidx,count(*) as cnt  from dev.mfg_user_hist_final group by 1) c on a.uidx=c.uidx
where b.style_id is null) 
where rn<=48-cnt);


drop table if exists dev.mfg_grp_fill1;
create table dev.mfg_grp_fill1 distkey(uidx) as
select distinct uidx,style_id,1 as flag from dev.mfg_group_final
union all 
(select uidx,style_Id,2 as flag from
(select a.uidx,a.style_Id,c.cnt,row_number() over (partition by a.uidx order by slots desc,rnk) as rn from
dev.mfg_grp_defualt_style a
left join (select distinct uidx,style_id from dev.mfg_group_final) b on a.uidx=b.uidx and a.style_id=b.style_id
left join (select uidx,count(*) as cnt  from dev.mfg_group_final group by 1) c on a.uidx=c.uidx
where b.style_id is null) 
where rn<=48-cnt);


drop table if exists dev.mfg_global_defaults;
create table dev.mfg_global_defaults distkey(uidx) as 
select * from
((SELECT uidx,style_id,rnk
FROM dev.mfg_user_gender_excl a,
     (SELECT style_id,gender,users,rnk
      FROM (SELECT a.style_id,ds.gender,users,ROW_NUMBER() OVER (PARTITION BY gender ORDER BY users DESC) rnk
            FROM (SELECT style_id,COUNT(DISTINCT uidx) AS users
                  FROM bidb.collection_log_eors
                  WHERE action = 1 AND   added_date = TO_CHAR(sysdate -INTERVAL '1 day','YYYYMMDD')::BIGINT
                  GROUP BY 1) a
              LEFT JOIN dim_style ds ON a.style_id = ds.style_id
              JOIN (SELECT style_id,
                           COUNT(DISTINCT CASE WHEN o.style_status = 'P' AND net_inventory_count > 0 THEN o.sku_id END) AS live_skus,
                           COUNT(DISTINCT o.sku_id) AS total_skus
                    FROM o_atp_inventory o
                      JOIN dim_product dp ON o.sku_id = dp.sku_id
                    GROUP BY 1) b
                ON a.style_id = b.style_id AND total_skus = live_skus)
      WHERE rnk <= 48) c
WHERE a.gender = c.gender)
UNION ALL
(SELECT uidx,style_id,rnk
FROM (SELECT DISTINCT w.uidx FROM dev.mfg_base w left join dev.mfg_user_gender_excl x on w.uidx=x.uidx where x.uidx is null) a,
     (SELECT style_id,rnk
      FROM (SELECT a.style_id,ROW_NUMBER() OVER (ORDER BY users DESC) rnk
            FROM (SELECT style_id, COUNT(DISTINCT uidx) AS users
                  FROM bidb.collection_log_eors
                  WHERE action = 1 AND   added_date = TO_CHAR(sysdate -INTERVAL '1 day','YYYYMMDD')::BIGINT
                  GROUP BY 1) a
              JOIN (SELECT style_id,
                           COUNT(DISTINCT CASE WHEN o.style_status = 'P' AND net_inventory_count > 0 THEN o.sku_id END) AS live_skus,
                           COUNT(DISTINCT o.sku_id) AS total_skus
                    FROM o_atp_inventory o
                      JOIN dim_product dp ON o.sku_id = dp.sku_id
                    GROUP BY 1) b
                ON a.style_id = b.style_id AND total_skus = live_skus)
      WHERE rnk <= 48) c)) ;


drop table if exists dev.mfg_user_hist_fill2;
create table dev.mfg_user_hist_fill2 distkey(uidx) as
select distinct uidx,style_id::bigint,flag from dev.mfg_user_hist_fill1
union all 
(select uidx,style_Id,3 as flag from
(select a.uidx,a.style_Id,c.cnt,row_number() over (partition by a.uidx order by rnk) as rn from
dev.mfg_global_defaults a
left join (select distinct uidx,style_id from dev.mfg_user_hist_fill1) b on a.uidx=b.uidx and a.style_id=b.style_id
left join (select uidx,count(*) as cnt  from dev.mfg_user_hist_fill1 group by 1) c on a.uidx=c.uidx
where b.style_id is null) 
where rn<=48-cnt);


drop table if exists dev.mfg_grp_fill2;
create table dev.mfg_grp_fill2 distkey(uidx) as
select distinct uidx,style_id::bigint,flag from dev.mfg_grp_fill1
union all 
(select uidx,style_Id,3 as flag from
(select a.uidx,a.style_Id,c.cnt,row_number() over (partition by a.uidx order by rnk) as rn from
dev.mfg_global_defaults a
left join (select distinct uidx,style_id from dev.mfg_grp_fill1) b on a.uidx=b.uidx and a.style_id=b.style_id
left join (select uidx,count(*) as cnt  from dev.mfg_grp_fill1 group by 1) c on a.uidx=c.uidx
where b.style_id is null) 
where rn<=48-cnt);

drop table if exists customer_insights.mfg_usr_reco;
create table customer_insights.mfg_usr_reco distkey(uidx) as
(select uidx,first_name,listagg(style_id,',') within group (order by flag) as style_list from dev.mfg_user_hist_fill2 a join dim_customer_idea dci on a.uidx=dci.customer_login group by 1,2);

drop table if exists customer_insights.mfg_group_reco;
create table customer_insights.mfg_group_reco distkey(uidx) as
(select uidx,first_name,listagg(style_id,',') within group (order by flag) as style_list from dev.mfg_grp_fill2 a join dim_customer_idea dci on a.uidx=dci.customer_login group by 1,2);


select cnt,count(*) from
(select uidx,count(*) as cnt from dev.mfg_user_hist_fill2 group by 1) 
group by 1 
limit 100;


select cnt,count(*) from
(select uidx,count(*) as cnt from dev.mfg_grp_fill2 group by 1) 
group by 1 
limit 100;

grant select on customer_insights.mfg_usr_reco to analysis_user;
grant select on customer_insights.mfg_group_reco to analysis_user;


