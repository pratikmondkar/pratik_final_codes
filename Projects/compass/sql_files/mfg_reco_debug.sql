select * from customer_insights.mfg_group_reco where uidx='c3e9ec26.2f47.4be2.a637.768ceb27430e2oTkSLSw82';

select * from dev.pm_mfg_grp_fill1 where uidx='c3e9ec26.2f47.4be2.a637.768ceb27430e2oTkSLSw82';

select * from dev.pm_mfg_group_final where uidx='c3e9ec26.2f47.4be2.a637.768ceb27430e2oTkSLSw82';

select * from dev.pm_mfg_user_gender_excl where uidx='c3e9ec26.2f47.4be2.a637.768ceb27430e2oTkSLSw82';

select * from dev.pm_mfg_base where uidx='c3e9ec26.2f47.4be2.a637.768ceb27430e2oTkSLSw82';


select uidx,gender from
(select uidx,
count(distinct case when gender in ('Women','Girls') then 'Women' when gender in ('Men','Boys') then 'Men' end) as cnt,
max(case when gender in ('Women','Girls') then 'Women' when gender in ('Men','Boys') then 'Men' end) as gender
from dev.pm_mfg_base a 
left join dim_style ds on a.style_id=ds.style_id
where uidx='c3e9ec26.2f47.4be2.a637.768ceb27430e2oTkSLSw82'
group by 1);


select * from dev.pm_mfg_clus_group_int where group_id='333f72c8-5c69-4d18-908c-70fa675f3d66';


select uidx,
count(distinct case when ds.gender in ('Women','Girls') then 'Women' when ds.gender in ('Men','Boys') then 'Men' end) as cnt,
max(case when ds.gender in ('Women','Girls') then 'Women' when ds.gender in ('Men','Boys') then 'Men' end) as infered_gender,
max(case when dci.gender in ('f') then 'Women' when dci.gender in ('m') then 'Men' end) as registered_gender
from dev.pm_mfg_base a 
left join dim_style ds on a.style_id=ds.style_id
left join dim_customer_idea dci on a.uidx=dci.customer_login
where uidx='025307a9.89f9.4522.99b3.6b7288487e9dPx4q3R7kcq'
group by 1;

select * from dev.pm_mfg_base where group_id='333f72c8-5c69-4d18-908c-70fa675f3d66';


select max(added_datetime) from collection_log_eors where added_date=20170620;


select customer_login as uidx,customer_login as uidx1,case when len(a.first_name) >15 then 'there' else a.first_name end as name ,
case when len(d.group_name) >15 then 'Group' else group_name end as group_name
from dim_customer_idea a
join customer_insights.mfg_group_reco b on a.customer_login=b.uidx
left join dev.pm_mfg_user_group_map c on b.uidx=c.uidx
--left join clickstream.mfg_group d on c.group_id=d.group_id
left join (select distinct uidx from clickstream.events_view 
						where event_type='push-notification-received' and load_date between 20170620 and 20170621
						and utm_campaign='8pm_20jun2017_Y_all_30_21_android_offers_coupons_msg') e on b.uidx=e.uidx
where e.uidx is null;


select * from customer_insights.mfg_usr_reco where uidx='48d8112e.62d3.476e.9c0f.e8d4a48b443eUnTacZeQZK';

select uidx from
(select uidx,count(*) as cnt from dev.pm_mfg_user_hist_fill2 group by 1) 
where cnt<48
limit 10;


select * from customer_insights.mfg_group_reco where uidx='c3e9ec26.2f47.4be2.a637.768ceb27430e2oTkSLSw82';

select * from dev.pm_mfg_user_hist_fill1 where uidx='025307a9.89f9.4522.99b3.6b7288487e9dPx4q3R7kcq';

select * from dev.pm_mfg_global_defaults where uidx='025307a9.89f9.4522.99b3.6b7288487e9dPx4q3R7kcq';

select * from dev.pm_mfg_user_gender_excl where uidx='025307a9.89f9.4522.99b3.6b7288487e9dPx4q3R7kcq';

select * from dev.pm_mfg_base where uidx='c3e9ec26.2f47.4be2.a637.768ceb27430e2oTkSLSw82';


select id,gender from dim_customer_idea where customer_login='025307a9.89f9.4522.99b3.6b7288487e9dPx4q3R7kcq';


select * from dev.pm_mfg_base where uidx='61ebe942.3b6a.4da6.b422.c4ce725afb0aO5OSQyjimJ';

drop table if exists dev.pm_mfg_reco_20170620;
create table dev.pm_mfg_reco_20170620 distkey(uidx) sortkey(reco_type) as
(select uidx,style_id,flag,rnk,'user' as reco_type from dev.pm_mfg_user_hist_fill2
union all 
select uidx,style_id,flag,rnk,'group' as reco_type from dev.pm_mfg_grp_fill2);

