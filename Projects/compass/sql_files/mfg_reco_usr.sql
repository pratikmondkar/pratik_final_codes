create table dev.mfg_base distkey(uidx) as 
(select distinct uidx,nvl(widget_item_whom,data_set_value) as style_id
from clickstream.events_2017_05_28 where event_type in ('AddToCollection') );

alter table dev.mfg_base add column group_id bigint;

update dev.mfg_base 
set group_id=src.group_id
from
(select uidx,cast (random() * 28273 as int) as group_id
from
(select distinct uidx from dev.mfg_base)) src
where src.uidx=dev.mfg_base.uidx;


drop table if exists  dev.mfg_bag_lifetime;
create table dev.mfg_bag_lifetime distkey(uidx) as
(SELECT uidx,brand,article_type,gender,rnk,score
             FROM (SELECT user_id as uidx,
                          brand,article_type,gender,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) + 25*sum(units_bought) AS score,
                          ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime
                   where identifier='uidx' and article_type in ('Kurtas',	'Dresses',	'Handbags',	'Shorts',	'Tshirts',	'Jeans',	'Tops',	'Flats',	'Sweatshirts',	'Track Pants',	'Formal Shoes',	'Blazers',	'Shirts',	'Trousers',	'Heels',	'Sunglasses',	'Sandals',	'Backpacks',	'Flip Flops',	'Skirts')
                   GROUP BY 1,2,3,4)
             WHERE rnk <= 5 and score>=5);

alter table dev.mfg_bag_lifetime add column total_score float8;
alter table dev.mfg_bag_lifetime add column style_slots bigint;

update dev.mfg_bag_lifetime 
set total_score=src.total_score,
 		style_slots=(score::float8/nullif(src.total_score,0))*24
from
(select uidx,sum(score) as total_score
from dev.mfg_bag_lifetime
group by 1) src
where src.uidx=dev.mfg_bag_lifetime.uidx;


select * from dev.mfg_bag_lifetime order by uidx limit 100;

drop table if exists  dev.mfg_bag_st;
create table dev.mfg_bag_st distkey(uidx) as
(SELECT uidx,brand,article_type,gender,rnk,score
             FROM (SELECT uidx,
                          brand,article_type,gender,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) + 25*sum(units_bought) AS score,
                          ROW_NUMBER() OVER (PARTITION BY uidx ORDER BY score DESC) AS rnk
                   FROM bidb.notif_personalize
                   where article_type in ('Kurtas',	'Dresses',	'Handbags',	'Shorts',	'Tshirts',	'Jeans',	'Tops',	'Flats',	'Sweatshirts',	'Track Pants',	'Formal Shoes',	'Blazers',	'Shirts',	'Trousers',	'Heels',	'Sunglasses',	'Sandals',	'Backpacks',	'Flip Flops',	'Skirts')
                   GROUP BY 1,2,3,4)
             WHERE rnk <= 5 and score>=5);

alter table dev.mfg_bag_st add column total_score float8;
alter table dev.mfg_bag_st add column style_slots bigint;

update dev.mfg_bag_st 
set total_score=src.total_score,
 		style_slots=(score::float8/nullif(src.total_score,0))*24
from
(select uidx,sum(score) as total_score
from dev.mfg_bag_st
group by 1) src
where src.uidx=dev.mfg_bag_st.uidx;


drop table if exists dev.mfg_bag_to_styles;

create table dev.mfg_bag_to_styles as
(select brand,article_type,gender,style_id from
(select brand,article_type,gender,a.style_id,pi_score,PERCENT_RANK() OVER (PARTITION BY brand,article_type,gender ORDER BY pi_score DESC) AS perc from
(select style_id,sum(norm_pi_score)/count(distinct case when is_live_style=1 then date end) as pi_score
from customer_insights.pi_metrics
where date>=to_char(sysdate -  interval '15 days','YYYYMMDD')::bigint
group by 1) a
left join dim_style ds on a.style_id=ds.style_id
where pi_score is not null)
WHERE perc <= 0.1);



drop table if exists dev.mfg_user_hist_final;
create table dev.mfg_user_hist_final distkey(uidx) as
(select uidx,brand,article_type,gender,style_id,style_slots,rnk from
(select uidx,a.brand,a.article_type,a.gender,style_id,style_slots,row_number() OVER (PARTITION BY uidx,a.brand,a.article_type,a.gender ORDER BY random() DESC) AS rnk from 
(SELECT s1.uidx,brand,article_type,gender,rnk,0 as flag,style_slots from dev.mfg_bag_st s1 join (select distinct uidx from dev.mfg_base) u on s1.uidx=u.uidx
union all 
SELECT s2.uidx,brand,article_type,gender,rnk, 1 as flag,style_slots from dev.mfg_bag_lifetime s2 join (select distinct uidx from dev.mfg_base) u on s2.uidx=u.uidx) a
left join dev.mfg_bag_to_styles b on a.brand=b.brand and a.article_type=b.article_type and a.gender=b.gender)
where rnk<=style_slots);



