select * from dev.pi_score limit 100;


drop table dev.pi_scored_source;
CREATE TABLE dev.pi_scored_source distkey(style_id) sortkey(load_date) AS
(SELECT load_date, 
			 article_type,
       cluster_id,
       style_id,
       source,
       SUM(source_weight*tag_cnt) as tag_cnt,
       SUM(source_weight*pdp) as pdp,
       SUM(source_weight*atc) as atc,
       SUM(source_weight*atwl) as atwl,
       SUM(source_weight*atcl) AS atcl
FROM (SELECT nvl(a.load_date,b.load_date) as load_date,
             nvl(a.style_id::bigint,b.style_id) as style_id,
             nvl(a.source,b.source) as source,
						 article_type,
             cluster_id,
             CASE
               WHEN nvl(a.source,b.source) = 'Burger menu' THEN 0.2
               WHEN nvl(a.source,b.source) = 'Banner or Card' THEN 0.1
               WHEN nvl(a.source,b.source) = 'Search' THEN 0.3
               WHEN nvl(a.source,b.source) = 'others' THEN 0.1
               ELSE 0
             END AS source_weight,
             pdp,
             atc,
             atcl,
             atwl,
             tag_cnt
      FROM dev.pi_score a
      full outer join 
      (SELECT load_date,
       style_id,
       source,
       nvl(bag_lp_cnt::float / NULLIF(bag_style_cnt,0),0) + nvl(ag_lp_cnt::float / NULLIF(ag_style_cnt,0),0) + nvl(bg_lp_cnt::float / NULLIF(bg_style_cnt,0),0) + nvl(ba_lp_cnt::float / NULLIF(ba_style_cnt,0),0) + nvl(b_lp_cnt::float / NULLIF(b_style_cnt,0),0) + nvl(a_lp_cnt::float / NULLIF(a_style_cnt,0),0) + nvl(g_lp_cnt::float / NULLIF(g_style_cnt,0),0) AS tag_cnt
FROM dev.searc_banner_tag_date) b on a.load_date=b.load_date and a.style_id=b.style_id and a.source=b.source)
GROUP BY 1,
         2,
         3,
         4,
         5);






drop table if exists dev.pm_vis_prefinal;
create table dev.pm_vis_prefinal distkey(style_id) sortkey(load_date) as
(select a.load_date,a.style_id,case when a.source in ('Search','Burger menu') then 'Organic' else 'Inorganic' end as source,sum(a.pdp) as pdp,sum(lp) as lp,
sum(c.base_pi_score) as base_pi_score
from dev.pi_score a
--left join customer_insights.pi_metrics b on a.style_id=b.style_id and a.load_date=b.date
left join dev.final_pi_source c on a.style_id=c.style_id and a.load_date=c.load_date and a.source=c.source
left join dev.pm_hg_vis d on a.style_id=d.style_id and a.load_date=d.load_date and a.source=d.source
group by 1,2,3);

drop table if exists customer_insights.source_pi_details;
create table customer_insights.source_pi_details distkey(style_id) sortkey(load_date) as
(select a.*,(a.base_pi_score/nullif(total_score,0))*b.norm_pi_score as norm_pi_score 
from dev.pm_vis_prefinal a
left join customer_insights.pi_metrics b on a.style_id=b.style_id and a.load_date=b.date
left join (select style_id,load_date,sum(base_pi_score) as total_score from dev.pm_vis_prefinal group by 1,2) c on a.style_id=c.style_id and a.load_date=c.load_date);

select load_date,count(*) from customer_insights.source_pi_details group by 1 limit 100;


select * from  customer_insights.source_pi_details limit 100;

grant select on customer_insights.source_pi_details to adhoc_clickstream_fin;
grant select on customer_insights.source_pi_details to explain_clickstream_fin;
grant select on customer_insights.source_pi_details to adhoc_clickstream_no_fin;
grant select on customer_insights.source_pi_details to explain_clickstream_no_fin;
