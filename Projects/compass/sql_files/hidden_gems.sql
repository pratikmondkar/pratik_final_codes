DROP TABLE IF EXISTS dev.pm_hg_ros;

CREATE TABLE dev.pm_hg_ros
(
			style_id		bigint,
			ros	float8
)
DISTKEY (style_id);


unload(
'
SELECT 
style_id,sum(sold_quantity) ::float8 / nullif(count(distinct case when is_live_style = 1 then date else null end),0) as ros
      FROM customer_insights.fact_category_over_view_metrics
      where date >= 20170701 and commercial_type in (\'SOR\',\'OUTRIGHT\')
      group by 1'

)to 's3://testashutosh/backups/fact_ros' 
credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
parallel on 
ALLOWOVERWRITE 
DELIMITER AS '\001'; 


copy dev.pm_hg_ros
(style_id,ros)  
from 's3://testashutosh/backups/fact_ros' 
credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
delimiter AS '\001' escape 
FILLRECORD 
emptyasnull 
blanksasnull 
TRIMBLANKS 
TRUNCATECOLUMNS 
DATEFORMAT AS 'YYYY-MM-DD HH24:MI:SS' 
TIMEFORMAT AS 'YYYY-MM-DD HH24:MI:SS' 
ACCEPTINVCHARS AS ' ' 
maxerror 100000 explicit_ids 
compupdate off statupdate off; 


create table dev.pm_hg_vis distkey(style_id) as 
(select CASE
         WHEN ref_event_type IN ('RightNavChildClick','Level1LeftNavClick','Level2LeftNavClick','Level3LeftNavClick','topNavClick') THEN 'Burger menu'
         WHEN ref_event_type IN ('streamCardClick','streamCardChildClick','feedCardClick','feedCardChildClick') THEN 'Banner or Card'
         WHEN ref_event_type IN ('SearchFired','searchFired') THEN 'Search'
         ELSE 'others'
       END AS source,
       entity_id as style_id, count(1) as lp,
       20170716 as load_date 
FROM customer_insights.funnel_20170716 a
join clickstream.widget_entity_2017_07_16 b on a.event_id=b.event_id and a.event_type in ('Product list loaded')
group by 1,2);


insert into dev.pm_hg_vis
(select CASE
         WHEN ref_event_type IN ('RightNavChildClick','Level1LeftNavClick','Level2LeftNavClick','Level3LeftNavClick','topNavClick') THEN 'Burger menu'
         WHEN ref_event_type IN ('streamCardClick','streamCardChildClick','feedCardClick','feedCardChildClick') THEN 'Banner or Card'
         WHEN ref_event_type IN ('SearchFired','searchFired') THEN 'Search'
         ELSE 'others'
       END AS source,
       entity_id as style_id, count(1) as lp,
       20170714 as load_date 
FROM customer_insights.funnel_20170714 a
join clickstream.widget_entity_2017_07_14 b on a.event_id=b.event_id and a.event_type in ('Product list loaded')
group by 1,2);


drop table if exists dev.pm_hg_base;
create table dev.pm_hg_base distkey(style_id) as 
(select a.style_id,ds.article_type,ds.gender,nvl(pi_score,0) as pi_score,b.inv,inv/nullif(ros,0) as doh,nvl(ros,0) as ros,
PERCENT_RANK() over (partition by ds.article_type,ds.gender order by nvl(pi_score,0)) as pi_decile,
PERCENT_RANK() over (partition by ds.article_type,ds.gender order by inv/nullif(ros,0)) as doh_decile
from
(select style_Id,avg(norm_pi_score) as pi_score 
from customer_insights.pi_metrics 
where date >=to_char(sysdate-interval '15 days','YYYYMMDD')::bigint
group by 1) a
join (select style_id,sum(net_inventory_count) as inv from o_atp_inventory o join dim_product dp on o.sku_id=dp.sku_id
			 where o.supply_type='ON_HAND' and o.style_status='P' and net_inventory_count>0 group by 1) b on a.style_id=b.style_id
join dev.pm_hg_ros c on a.style_id=c.style_id
left join dim_style ds on a.style_id=ds.style_id);


select * from  dev.pm_hg_base limit 100;

drop table if exists dev.pm_hg_final;
create table dev.pm_hg_final distkey(style_id) as 
(select a.*,case when pi_decile <0.33 then 'L' when pi_decile between 0.33 and 0.66 then 'M' when pi_decile >0.66 then 'H' end as pi_bucket,
case when doh_decile <0.33 then 'L' when doh_decile between 0.33 and 0.66 then 'M' when doh_decile >0.66 then 'H' end as doh_bucket,
b.organic_vis,b.inorganic_vis,nvl(inorganic_vis,0)::float8/nullif(nvl(organic_vis,0)+nvl(inorganic_vis,0),0) as perc_inor_vis_style,
nvl(inorganic_vis,0)::float8/nullif(nvl(total_inorganic_vis,0),0) as perc_inor_vis_total,
PERCENT_RANK() over (partition by article_type,gender order by perc_inor_vis_style) as vis_decile
from dev.pm_hg_base a
left join (select style_id,sum(case when source in ('Burger menu','Search') then lp end) as organic_vis,
sum(case when source not in ('Burger menu','Search') then lp end) as inorganic_vis,1 as flag
from dev.pm_hg_vis group by 1 ) b
left join (select sum(case when source in ('Burger menu','Search') then lp end) as total_organic_vis,
sum(case when source not in ('Burger menu','Search') then lp end) as total_inorganic_vis,1 as flag from dev.pm_hg_vis) c on b.flag=c.flag 
on a.style_id=b.style_id);


select * from dev.pm_hg_final where pi_bucket='H' 
and doh>=90
and inv>20 
and vis_decile<0.22 
and article_type in ('Tshirts','Shirts','Jeans','Casual Shoes','Sports Shoes','Tops','Kurtas','Jackets','Sweatshirts') and gender in ('Men','Women') limit 1000;
