drop table if exists customer_insights.style_cluster_map;
CREATE TABLE customer_insights.style_cluster_map
(
   article_type  varchar(255),
   gender				 varchar(100),
   style_id      bigint,
   cluster_id    bigint,
   cluster_name	 varchar(4000)
) distkey(style_id) sortkey(article_type);

drop table if exists dev.style_cluster_map;
CREATE TABLE dev.style_cluster_map
(
   article_type  varchar(255),
   gender				 varchar(100),
   style_id      bigint,
   cluster_id    bigint
) distkey(style_id) sortkey(article_type);

create table dev.pm_temp_compass 
(
style_id		bigint,
cluster_id	bigint,
cluster_name varchar(4000)
) distkey(style_id);

truncate table customer_insights.style_cluster_map;
copy customer_insights.style_cluster_map
(article_type,gender,style_id,cluster_id,cluster_name)  
from 's3://testashutosh/backups/cluster_map' 
credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
delimiter AS '\001' escape 
FILLRECORD 
emptyasnull 
blanksasnull 
TRIMBLANKS 
TRUNCATECOLUMNS 
DATEFORMAT AS 'YYYY-MM-DD HH24:MI:SS' 
TIMEFORMAT AS 'YYYY-MM-DD HH24:MI:SS' 
ACCEPTINVCHARS AS ' ' 
maxerror 100000 explicit_ids 
compupdate off statupdate off; 


insert into customer_insights.style_cluster_map
select article_type,gender,a.style_id,cluster_id,cluster_name from
dev.pm_temp_compass a
left join dim_style b on a.style_id=b.style_id;


select * from customer_insights.style_cluster_map limit 10;

create table customer_insights.dim_cluster distkey(cluster_id) sortkey(article_type) as
(select distinct article_type,gender,cluster_id,cluster_name from customer_insights.style_cluster_map);

select article_type,gender,count(*) from customer_insights.dim_cluster group by 1,2 order by 3 desc limit 10; 

drop table customer_insights.dim_cluster ;
create table customer_insights.dim_cluster 
(
article_type	varchar(400),
gender				varchar(100),
cluster_id		bigint,
cluster_name varchar(4000)
) distkey(cluster_id);


copy customer_insights.dim_cluster
(article_type,gender,cluster_id,cluster_name)  
from 's3://testashutosh/backups/dim_cluster' 
credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
delimiter AS '\001' escape 
FILLRECORD 
emptyasnull 
blanksasnull 
TRIMBLANKS 
TRUNCATECOLUMNS 
DATEFORMAT AS 'YYYY-MM-DD HH24:MI:SS' 
TIMEFORMAT AS 'YYYY-MM-DD HH24:MI:SS' 
ACCEPTINVCHARS AS ' ' 
maxerror 100000 explicit_ids 
compupdate off statupdate off; 
