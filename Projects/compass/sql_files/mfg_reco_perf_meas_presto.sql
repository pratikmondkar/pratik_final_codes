create table temp_madlytics.pm_mfg_sessions as
select distinct session_id from
prod_madlytics.events
where y='2017' and m='06' and d in ('20','21','22','23','26','27') and event_type in ('push-notification') and utm_campaign in ('8pm_20jun2017_Y_all_30_21_android_offers_coupons_99styles','9pm_22jun2017_Y_seg_30_21_android_group_reco_msg');

drop table if exists  temp_madlytics.pm_mfg_event_details;
create table temp_madlytics.pm_mfg_event_details as
select e.session_id,session_start_time,client_ts,uidx,device_id,event_type,screen_name,data_set_value from
prod_madlytics.events e
join temp_madlytics.pm_mfg_sessions s on e.session_id=s.session_id
where y='2017' and m='06' and d in ('20','21','22','23','26','27') and event_type in ('addToCart','AddToCollection','product_click_list_page','ScreenLoad');



select * from  temp_madlytics.pm_mfg_event_details limit 100;

select data_set_type,count(*) from  prod_madlytics.events
where y='2017' and m='06' and d in ('20') and event_type='ScreenLoad'
group by 1
order by 2 desc
limit 100;


select * from  prod_madlytics.events
where y='2017' and m='06' and d in ('20') limit 100
