select barcode,po_status,count(distinct sku_id) as skus,sum(quantity) as qty,max(approval_date) as ap_date from fact_purchase_order where approval_date between 20170201 and 20170328 group by 1,2;

create table dev.compass_test_po_styles distkey(style_id) as 
(select dp.style_id,min(fii.inward_date) as last_inward_date,max(cluster_id) as cluster_id 
from fact_purchase_order  fpo
left join fact_inventory_item fii on fpo.sku_id=fii.sku_id and fii.inward_date>fpo.approval_date
left join dim_product dp on fpo.sku_id=dp.sku_id
left join dev.style_cluster sc on dp.style_id=sc.style_id
where barcode = 'TOMM230217-09'
group by 1) ;

select * from dev.compass_test_po_styles;


select a.*,b.avg_pi_score
from
(select c.style_id,ds.article_type,c.cluster_id,sum(sold_quantity) as qty,count(1) as live_days
from dev.compass_test_po_styles c 
left join fact_category_over_view_metrics fc on fc.style_id=c.style_id and is_live_style=1 and date between 20170326 and 20170426 
left join dim_style ds on c.style_id=ds.style_id
group by 1,2,3) a
left join  
(select article_type,cluster_id,sum(norm_pi_score)/count(1) as avg_pi_score
from dev.pi_metrics pm
left join dev.style_cluster sc on pm.style_id=sc.style_id
where date between to_char(to_date(20170322,'YYYYMMDD') - interval '30 days','YYYYMMDD')::bigint and 20170321 and is_live_style=1
group  by 1,2) b on a.cluster_id=b.cluster_id and a.article_type=b.article_type;



select d.style_id,sum(norm_pi_score)/count(1) as avg_pi_score from 
dev.style_cluster d
left join dev.pi_metrics sc on d.style_id=sc.style_id
where date between to_char(to_date(20170322,'YYYYMMDD') - interval '30 days','YYYYMMDD')::bigint and 20170321 and is_live_style=1 and cluster_id=419 and article_type='Tshirts' 
group by 1 limit 500;



select count(*) from dev.style_cluster where cluster_id=195 and article_type='Tshirts'
