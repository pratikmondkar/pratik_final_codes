select 
count(distinct case when event_type in ('addToCart','AddToCollection') then uidx end) as atc,
count(distinct case when event_type in ('AddToCollection') then uidx end) as wishlist,
count(distinct case when event_type in ('product_click_list_page') then uidx end) as list_click
from customer_insights.funnel_20170620 
where ref_utm_campaign='8pm_20jun2017_Y_all_30_21_android_offers_coupons_99styles' ;
--and ref_event_type is null;


select 
count(distinct case when event_type in ('addToCart','AddToCollection') then uidx end) as atc,
count(distinct case when event_type in ('AddToCollection') then uidx end) as wishlist,
count(distinct case when event_type in ('product_click_list_page') then uidx end) as list_click
from customer_insights.funnel_20170622 
where ref_utm_campaign='9pm_22jun2017_Y_seg_30_21_android_group_reco_msg' ;
--and ref_event_type is null ; 

select
count(distinct case when event_type in ('push-notification-received') then uidx end) as received_notif,
count(distinct case when event_type in ('push-notification') then uidx end) as open_notif
from clickstream.events_2017_06_22 
where utm_campaign='9pm_22jun2017_Y_seg_30_21_android_group_reco_msg' ;

select * from stv_inflight

select utm_campaign,
count(distinct case when event_type in ('push-notification-received') then uidx end) as received_notif,
count(distinct case when event_type in ('push-notification') then uidx end) as open_notif
from clickstream.events_2017_06_22 
group by 1
order by 2 desc
limit 1000;


--campaigns 
--8pm_20jun2017_Y_all_30_21_android_offers_coupons_99styles
--9pm_22jun2017_Y_seg_30_21_android_group_reco_msg

drop table if exists dev.pm_mfg_sessions_opens;
create table dev.pm_mfg_sessions_opens distkey(session_id) as 
select distinct session_id,max(utm_campaign) as utm_campaign,min(load_date) as load_date 
from clickstream.events_view 
where load_date in (20170620,20170621,20170622,20170623,20170626,20170627) and event_type in ('push-notification') 
and utm_campaign in ('8pm_20jun2017_Y_all_30_21_android_offers_coupons_msg','9pm_22jun2017_Y_seg_30_21_android_group_reco_msg') 
group by 1  ; 

drop table if exists dev.pm_mfg_event_details;
create table dev.pm_mfg_event_details distkey(uidx) as 
select e.session_id,s.utm_campaign,session_start_time,s.load_date,client_ts,uidx,device_id,event_type,screen_name,data_set_value,widget_item_whom 
from clickstream.events_view e 
join dev.pm_mfg_sessions_opens s on e.session_id=s.session_id 
where e.load_date in (20170620,20170621,20170622,20170623,20170626,20170627) 
and event_type in ('addToCart','AddToCollection','product_click_list_page','ScreenLoad') ; 

 

select utm_campaign, load_date,
count(distinct idcustomer) as customers,
sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges ,0)) as revenue, 
sum(quantity) as qty 
from (select utm_campaign,uidx,load_date, case when event_type='AddToCollection' then nvl(widget_item_whom,data_set_value) else data_set_value end as style_id 
			from dev.pm_mfg_event_details where event_type in ('addToCart','AddToCollection') ) ed 
left join dim_customer_idea dci on ed.uidx=dci.customer_login 
left join fact_core_item fci on ed.style_id=fci.style_id and dci.id=fci.idcustomer and ed.load_date<=order_Created_date
where fci.is_booked=1 and fci.store_id=1 and fci.order_created_date between 20170620 and 20170630
group by 1,2   ;


--


drop table if exists dev.pm_mfg_sessions_opens_bench;
create table dev.pm_mfg_sessions_opens_bench distkey(session_id) as 
select distinct session_id,max(utm_campaign) as utm_campaign,min(load_date) as load_date 
from clickstream.events_2017_06_20
where event_type in ('push-notification') and utm_campaign in ('8pm_20jun2017_Y_all_30_21_android_offers_coupons_99styles') 
group by 1  ; 

drop table if exists dev.pm_mfg_event_details_bench;
create table dev.pm_mfg_event_details_bench distkey(uidx) as 
select e.session_id,s.utm_campaign,session_start_time,s.load_date,client_ts,uidx,device_id,event_type,screen_name,data_set_value,widget_item_whom 
from clickstream.events_2017_06_20 e 
join dev.pm_mfg_sessions_opens_bench s on e.session_id=s.session_id 
where event_type in ('addToCart','AddToCollection','product_click_list_page','ScreenLoad') ; 

 

select utm_campaign, load_date,
count(distinct idcustomer) as customers,
sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges ,0)) as revenue, 
sum(quantity) as qty 
from (select utm_campaign,uidx,load_date, case when event_type='AddToCollection' then nvl(widget_item_whom,data_set_value) else data_set_value end as style_id 
			from dev.pm_mfg_event_details_bench where event_type in ('addToCart','AddToCollection') ) ed 
left join dim_customer_idea dci on ed.uidx=dci.customer_login 
left join fact_core_item fci on ed.style_id=fci.style_id and dci.id=fci.idcustomer and ed.load_date<=order_Created_date
where fci.is_booked=1 and fci.store_id=1 and fci.order_created_date between 20170620 and 20170630
group by 1,2 
