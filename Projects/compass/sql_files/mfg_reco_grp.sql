drop table if exists dev.mfg_clus_group_int;

create table dev.mfg_clus_group_int distkey(group_id) as 
(select * from
(select a.*,c.total_cus,cl_cus::float8/total_cus as gi,row_number() over (partition by a.group_id order by gi desc) as rnk 
	from
			(select group_id,cluster_id,article_type,count(distinct uidx) as cl_cus
			from dev.mfg_base b join style_cluster_map cm on b.style_id=cm.style_id
			where article_type in ('Kurtas',	'Dresses',	'Handbags',	'Shorts',	'Tshirts',	'Jeans',	'Tops',	'Flats',	'Sweatshirts',	'Track Pants',	'Formal Shoes',	'Blazers',	'Shirts',	'Trousers',	'Heels',	'Sunglasses',	'Sandals',	'Backpacks',	'Flip Flops',	'Skirts')
			group by 1,2,3) a
	left join 
			(select group_id,count(distinct uidx) as total_cus from dev.mfg_base group by 1) c 
	on a.group_id=c.group_id)
	where rnk<=10);

alter table dev.mfg_clus_group_int add column total_gi float8;
alter table dev.mfg_clus_group_int add column style_slots bigint;

update dev.mfg_clus_group_int 
set total_gi=src.total_gi,
 		style_slots=(gi::float8/nullif(src.total_gi,0))*48
from
(select group_id,sum(gi) as total_gi
from dev.mfg_clus_group_int
group by 1) src
where src.group_id=dev.mfg_clus_group_int.group_id;


select * from dev.mfg_clus_group_int order by group_id limit 1000;

drop table if exists dev.mfg_cluster_to_styles;

create table dev.mfg_cluster_to_styles as
--(select brand,article_type,gender,style_id from
(select article_type,cluster_id,a.style_id,pi_score,PERCENT_RANK() OVER (PARTITION BY article_type,cluster_id ORDER BY pi_score DESC) AS perc from
(select style_id,sum(norm_pi_score)/count(distinct case when is_live_style=1 then date end) as pi_score
from customer_insights.pi_metrics
where date>=to_char(sysdate -  interval '15 days','YYYYMMDD')::bigint
group by 1) a
left join style_cluster_map cm on a.style_id=cm.style_id
where pi_score is not null);



create table dev.mfg_group_final distkey(uidx) as
select d.uidx,c.* from
(select group_id,cluster_id,article_type,style_id,style_slots,rnk from
(select group_id,a.cluster_id,a.article_type,style_id,style_slots,row_number() OVER (PARTITION BY group_id,a.cluster_id,a.article_type ORDER BY random() DESC) AS rnk from 
dev.mfg_clus_group_int a
left join dev.mfg_cluster_to_styles b on a.cluster_id=b.cluster_id and a.article_type=b.article_type)
where rnk<=style_slots) c
join (select distinct uidx,group_id from dev.mfg_base) d on c.group_id=d.group_id;

create table dev.mfg.group_defaults distkey(group_id) as 
(select c.*,d.total_users,users::float8/total_users as common,round((users::float8/total_users)*48,0) as slots from
(select group_id,brand,article_type,gender,count(distinct a.uidx) as users
from dev.mfg_bag_lifetime a
join 
(select distinct uidx,group_id from dev.mfg_base) b on a.uidx=b.uidx
group by 1,2,3,4) c
left join (select group_id,count(distinct uidx) as total_users from dev.mfg_base group by 1) d 
on c.group_id=d.group_id
where users>1);

