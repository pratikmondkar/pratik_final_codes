
# coding: utf-8

import numpy as np
from kmodes import kmodes,kprototypes 
import kmodes.util as util
import pandas as pd
import sqlalchemy as sq
from pandas.io.json import json_normalize
import multiprocessing
import ast
import pickle
from datetime import date

pd.set_option('max_colwidth',60000)
engine = sq.create_engine("postgresql+psycopg2://analysis_user:AdhoCus@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")


#Fixed list of article type for which we have cluster
al=pd.read_csv('/data/pratik/compass/config/at_config.csv',header=None,names=['article_type'])
at_list=al['article_type'].tolist()

#Defining function to parse json string needed to flatten
def only_dict(d):
    '''
    Convert json string representation of dictionary to a python dict
    '''
    return ast.literal_eval(d)


def assign_cluster(i):

    path_m=r'/data/pratik/compass/cluster_models/'
    path_b=r'/data/pratik/compass/mrp_buckets/'

    data=raw_filt[raw_filt['article_type']==i]
    fn=i.replace(" ", "_")
    with open(path_m+i+'.pkl', 'rb') as f:
        model = pickle.load(f)
    with open(path_b+fn+'.txt', 'rb') as f:
        bins = pickle.load(f)    
    weights=pd.read_csv('/data/pratik/compass/weights/'+fn+'.csv',error_bad_lines=False)
    column_list=weights['attribute'].tolist()
    
    attributes=json_normalize(data['style_attributes'].apply(only_dict).tolist())
    attributes.replace(['NA','None',' ',''], np.nan, inplace = True)
    final_attr=pd.concat([data.reset_index().drop(['style_attributes','index'],axis=1),attributes.drop('style_id',axis=1)],axis=1)
    final_attr['mrp_bucket']=pd.cut(final_attr['article_mrp'],bins=bins)
    treat=set(column_list).difference(final_attr.columns)
    for j in treat:
        final_attr[j]=np.NaN
    new=final_attr[column_list]
    clusters=model.predict(new,weights=weights['weight'].tolist()) 
    scored=pd.concat([final_attr.reset_index(),pd.DataFrame(clusters,columns=['cluster'])], axis=1).drop('index',axis=1)
    scored['article_type']=i
    return scored[['article_type','style_id','cluster']]


sql_str="""
        SELECT       ds.style_id,
                     article_type,
                     gender,
                     season,
                     article_mrp,
                     base_colour,
                     style_attributes
        FROM dim_style ds
        join (select distinct style_id from dim_product 
        where style_catalogued_date=to_char(sysdate - interval '1 days','YYYYMMDD')::bigint) b on ds.style_id=b.style_id
    """


raw=pd.read_sql_query(sql_str,engine)
raw_filt=raw[raw['style_attributes'].isnull()==False]

at_list=set(raw_filt['article_type'].unique().tolist()).intersection(at_list)
p = multiprocessing.Pool(10)

clustered_data=p.map(assign_cluster, at_list)

if len(clustered_data)>0 :
    final = pd.concat(clustered_data)
    final['style_id']=final['style_id'].astype(int)
else :
    final=pd.DataFrame(columns=['article_type','style_id','cluster'])    
final.to_csv('/data/pratik/compass/redshift_uploads/daily_assign_'+dt+'.csv',index=False)

