SELECT distinct session_id,
       data_set_value
FROM clickstream.events_view ev
join dim_style ds on ev.data_set_value=ds.style_id
WHERE event_type IN ('ScreenLoad') and screen_name like '%Shopping Page-PDP%'
and ds.article_type in ('Sandals') and ds.gender in ('Men')
AND load_date BETWEEN TO_CHAR(convert_timezone('Asia/Calcutta',sysdate)- interval '20 days' ,'YYYYMMDD')::bigint and TO_CHAR(convert_timezone('Asia/Calcutta',sysdate)- interval '14 day' ,'YYYYMMDD')::bigint
