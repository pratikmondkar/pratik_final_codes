source /home/apollo/.config
export PGPASSWORD=$REDSHIFT_PASSWORD


/usr/bin/pyton2.7 /home/apollo/customer-insights/clickstream_jobs/python_files/cluster_assignment.py

if [ $? -gt 0 ]
then
        echo "Error in python script"
        exit 1;
else
        echo "Clustering ran Successfully"

fi

now=$(date +"%Y_%m_%d")

s3cmd put /data/pratik/compass/redshift_uploads/daily_assign_$now.csv s3://testashutosh/backups/ 

sed -e "s/replace_date/$now/g" /home/apollo/customer-insights/clickstream_jobs/sql_files/new_style_cluster_upload.sql > /home/apollo/compass/style_upload.sql

/usr/bin/psql --echo-all -h $CLICKSTREAM_CLUSTER -U $USER -d $DATABASE -p 5439 -w -v ON_ERROR_STOP=1 -f /home/apollo/compass/style_upload.sql

if [ $? -gt 0 ]
then
        echo "Either there is a Redshift DB connectivity issue or there might be an error in the script "
        exit 1;
else
        echo "uploaded cluster mapping Successfully"

fi

/usr/bin/psql --echo-all -h $REDSHIFT_CLUSTER -U $USER -d $DATABASE -p 5439 -w -v ON_ERROR_STOP=1 -f /home/apollo/customer-insights/clickstream_jobs/sql_files/pi_unload.sql

if [ $? -gt 0 ]
then
        echo "Either there is a Redshift DB connectivity issue or there might be an error in the script "
        exit 1;
else
        echo "Ran Successfully"

fi

/usr/bin/psql --echo-all -h $CLICKSTREAM_CLUSTER -U $USER -d $DATABASE -p 5439 -w -v ON_ERROR_STOP=1 -f /home/apollo/customer-insights/clickstream_jobs/sql_files/pi_calculation.sql

if [ $? -gt 0 ]
then
        echo "Either there is a Redshift DB connectivity issue or there might be an error in the script "
        exit 1;
else
        echo "Ran Successfully"

fi