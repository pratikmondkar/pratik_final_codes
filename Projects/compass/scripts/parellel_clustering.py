
# coding: utf-8

# In[1]:

import numpy as np
from kmodes import kmodes,kprototypes 
import kmodes.util as util
import pandas as pd
import sqlalchemy as sq
from sklearn import preprocessing
from pandas.io.json import json_normalize
import itertools
import json
import ast
import subprocess
import shlex
import pickle
import multiprocessing
import os

pd.set_option('max_colwidth',60000)
engine = sq.create_engine("postgresql+psycopg2://analysis_user:Insight@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")


# In[2]:

#Defining function to parse json string needed to flatten
def only_dict(d):
    '''
    Convert json string representation of dictionary to a python dict
    '''
    return ast.literal_eval(d)


# In[21]:

#Defining the final clustering function that returns the style to cluster mapping
def cluster_kmodes(lst):
    at=lst[0]
    gn=lst[1]
    sql_str="""
    SELECT ds.style_id,
                 gender,
                 season,
                 article_mrp,
                 base_colour,
                 style_attributes
          FROM dim_style ds
          WHERE article_type = %(at)s and gender = %(gn)s
    """

    raw_data=pd.read_sql_query(sql_str,engine,params={"at":at,"gn":gn})


    raw_filt=raw_data[raw_data['style_attributes'].isnull()==False]
    attributes=json_normalize(raw_filt['style_attributes'].apply(only_dict).tolist())
    attributes.replace(['NA','None',' ',''], np.nan, inplace = True)
    attributes_filt=attributes.drop(attributes._get_numeric_data().columns,axis=1)
    u=attributes_filt.describe().transpose()
    u['coverage']=u['count']/len(attributes)
    print "Coverage for %s %s is :" %(at,gn)
    print u[u['coverage']>=0.75]

    column_list=u[u['coverage']>=0.75].reset_index()['index']
    att_shrtlst=attributes[column_list]


    final_attr=pd.concat([raw_filt.reset_index().drop(['style_attributes','index'],axis=1),att_shrtlst],axis=1)
    final_attr['mrp_bucket']=pd.qcut(final_attr['article_mrp'],5)
    final_attr.drop('article_mrp',axis=1,inplace=True)
    final_attr.style_id=final_attr.style_id.astype(float)


    sh_cmd='sh /data/pratik/compass/scripts/cb.sh "'+ at +'" ' + gn
    subprocess.call(shlex.split(sh_cmd))

    fn=at.replace(" ", "_")

    
    cobrowse=pd.read_csv('/data/pratik/compass/cobrowsing/cb_'+fn+gn+'.txt',error_bad_lines=False)
    cobrowse.rename(columns={'data_set_value': 'style_id'},inplace=True)
    cobrowse_filt=cobrowse[cobrowse['style_id'].isnull()==False]
    cobrowse_att=cobrowse_filt.merge(final_attr,how='left',on='style_id')


    weights=pd.DataFrame(columns=['attribute','cb_sessions','total_sessions'])
    c=0
    for i in cobrowse_att.drop(['session_id','style_id'],axis=1).columns:
        cb=cobrowse_att.groupby(['session_id',i],as_index=False)['style_id'].count()
        weights.loc[c]=[i,cb[cb['style_id']>=2]['session_id'].nunique(),cobrowse_filt['session_id'].nunique()]
        c=c+1
    weights['weight']=weights['cb_sessions']/weights['total_sessions']
    weights['article_type']=at
    weights['gender']=gn
    weights['new_weight']=weights['weight'].rank(ascending=1)*2
    print weights

    weights.to_csv('/data/pratik/compass/weights/'+fn+gn+'.csv',index=False)
    
    #finding Number of clusters
    k=len(final_attr)/200
    print "# of clusters (k): %d" %k


    ad=final_attr.drop(['style_id'], axis=1)
    kmodes_huang = kmodes.KModes(n_clusters=k, init='Huang', verbose=1)
    clusters_h=kmodes_huang.fit(ad,weights=weights['new_weight'].tolist()) 


    labels=pd.DataFrame(clusters_h.labels_,columns=['cluster'])
    clustered=pd.concat([final_attr.reset_index(),labels], axis=1).drop('index',axis=1)
    a=clustered['cluster'].value_counts()
    print "biggest 10 cluster sizes: "
    print a[:10]


    cobrowse_cls=cobrowse_filt.merge(clustered[['style_id','cluster']],how='left',on='style_id')


    cb=cobrowse_cls.groupby(['session_id','cluster'],as_index=False).agg({"style_id": pd.Series.nunique})
    cls_score=cb[cb['style_id']>=2].groupby('cluster',as_index=False).agg({"session_id": pd.Series.nunique})
    cls_cnt=cobrowse_cls.groupby('cluster',as_index=False).agg({"session_id": pd.Series.nunique})
    cls_valid=cls_cnt.merge(cls_score,how='left',on='cluster')



    cls_valid['quality']=cls_valid['session_id_y']/cls_valid['session_id_x']
    cls_valid.sort_values(by='quality',ascending=False).to_csv('/data/pratik/compass/validations/'+fn+gn+'.csv')


    clustered[['style_id','cluster']].to_csv('/data/pratik/compass/cluster_op/'+fn+gn+'.csv',index=False)


    # now you can save it to a file
    with open('/data/pratik/compass/cluster_models/'+fn+gn+'.pkl', 'wb') as f:
        pickle.dump(kmodes_huang, f)

    print "completed "+ at + gn
    return clustered[['style_id','cluster']]


# In[22]:

#Selecting the Article_type; this is the only user input
at=['Casual Shoes', 'Shirts', 'Tshirts', 'Jeans', 'Kurtas', 'Jackets', 'Sports Shoes', 'Tops', 'Dresses', 
    'Trousers', 'Sweatshirts', 'Sweaters', 'Watches', 'Handbags', 'Formal Shoes', 'Heels', 'Backpacks', 'Bra', 
    'Track Pants', 'Flats', 'Shorts', 'Flip Flops', 'Kurta Sets', 'Blazers', 'Sunglasses', 'Sandals', 'Skirts']
at=['Sports Shoes','Casual Shoes']
gn=['Men','Women','Unisex','Girls','Boys']
lst=list(itertools.product(at,gn))
p = multiprocessing.Pool(10)


# In[23]:

#Execute parellel jobs
clustered_data=p.map(cluster_kmodes, lst)


# In[ ]:

#Collating all final results in case of single 
'''
cluster_results = pd.concat(clustered_data)
cluster_results
'''


# In[ ]:

#Collating all final results in case of multiple runs
path =r'/data/pratik/compass/cluster_op/' # use your path
frame = pd.DataFrame()
list_ = []
for i in os.listdir(path):
    df = pd.read_csv(path+i,error_bad_lines=False)
    list_.append(df)
final = pd.concat(list_)


# In[ ]:

final['style_id']=final['style_id'].astype(int)


# In[ ]:

final.to_csv('/data/pratik/compass/redshift_uploads/init_cls_top_at.csv',index=False)

