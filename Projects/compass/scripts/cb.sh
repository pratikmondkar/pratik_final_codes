source ~/.db_config
export PGPASSWORD=$REDSHIFT_PASSWORD

at=$1
gn=$2
echo $at $gn

sed -e "s/replace_at/$at/g" /data/pratik/compass/scripts/cb.sql > /data/pratik/compass/scripts/cb_mod.sql

sed -e "s/replace_gn/$gn/g" /data/pratik/compass/scripts/cb_mod.sql > /data/pratik/compass/scripts/cb_mod2.sql

a=${at/ /_}

/usr/bin/psql -h $CLICKSTREAM_CLUSTER -U $USER -d $DATABASE -p 5439 -A -F , -X -w -v ON_ERROR_STOP=1 -f /data/pratik/compass/scripts/cb_mod2.sql -o /data/pratik/compass/cobrowsing/cb_$a$gn.txt

if [ $? -gt 0 ]
then
        echo "Either there is a Redshift DB connectivity issue or there might be an error in the script "
        exit 1;
else
        echo "Ran Successfully"

fi

