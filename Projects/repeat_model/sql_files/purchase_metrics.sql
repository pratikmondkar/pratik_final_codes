SELECT c.idcustomer,
 CASE
         WHEN d.idcustomer IS NOT NULL THEN 1
         ELSE 0
       END AS purchase_flag,
       snapshot_date,
       no_of_days_since_purchase,
       revenue,
       no_of_at,
       (discount*100) as discount,
       days_to_deliver,city_tier,nps_score
from
(select a.idcustomer,snapshot_date,
       city_tier,revenue,no_of_at,
DATEDIFF('d',TO_DATE(a.last_order_date,'YYYYMMDD'), TO_DATE(b.snapshot_date,'YYYYMMDD')) as no_of_days_since_purchase,       
discount,days_to_deliver,nps_score
       from (SELECT foci.idcustomer,
       city_tier,
       SUM(item_revenue_inc_cashback) AS revenue,
       COUNT(DISTINCT article_type) AS no_of_at,
       MAX(order_created_date) AS last_order_date,
       SUM(nvl (coupon_discount,0) + nvl (product_discount,0)) / NULLIF(SUM(nvl (item_mrp_value,0)*nvl (quantity,0)),0) AS discount,
       MAX(DATEDIFF ('d',TO_DATE(order_created_date,'YYYYMMDD'),TO_DATE(order_delivered_date,'YYYYMMDD'))) AS days_to_deliver,
       sum(nps) AS nps_score
FROM (SELECT fci.idcustomer,
             idlocation,
             order_id,
             item_revenue_inc_cashback,
             article_type,
             fci.order_created_date,
             coupon_discount,
             product_discount,
             cart_discount,
             payment_gateway_discount,
             loyalty_pts_used,
             item_mrp_value,
             quantity,
             order_delivered_date
      FROM bidb.fact_core_item fci
      join dim_customer_idea dci on fci.idcustomer=dci.id
      join customer_purchase_sequence as ps on fci.idcustomer=ps.idcustomer and fci.order_group_id=ps.order_group_id
      WHERE 
      (ps.purchase_sequence=2 or ps.purchase_sequence=3)
      AND   fci.order_created_date < 20180227
      and order_delivered_date>19700101
      AND   (is_shipped = 1 OR is_realised = 1)
      AND   store_id = 1 ) AS foci
  LEFT JOIN (SELECT city_tier,
                    id
             FROM (SELECT id,
                          city_tier
                   FROM (SELECT id, city_name FROM bidb.dim_location) AS l
                     JOIN (SELECT city_name, city_tier FROM bidb.city_tier_mapping) AS m ON l.city_name = m.city_name)) AS loc ON loc.id = foci.idlocation
  LEFT JOIN (SELECT shipment_id,
                    CASE
                      WHEN delivery_nps = 'Promoters' THEN 1
                      WHEN delivery_nps = 'Detractors' THEN 0
                    END AS nps
             FROM customer_insights.delivery_nps_summary) AS nps ON foci.order_id = nps.shipment_id
GROUP BY 1,
         2) as a
          JOIN (SELECT idcustomer,
                     20180227::BIGINT AS snapshot_date
              FROM bidb.fact_core_item
              WHERE order_created_date < 20180227
              AND   (is_shipped = 1 OR is_realised = 1)
              AND   store_id = 1
              GROUP BY 1
              HAVING COUNT(DISTINCT order_group_id) = 1) AS b ON a.idcustomer = b.idcustomer) AS c              
 LEFT JOIN (SELECT DISTINCT idcustomer
                    FROM bidb.fact_core_item
             WHERE order_created_date BETWEEN %(date_from_to_placeholder) s 
             AND (is_shipped = 1 OR is_realised = 1)
             AND   store_id = 1) AS d ON c.idcustomer = d.idcustomer

