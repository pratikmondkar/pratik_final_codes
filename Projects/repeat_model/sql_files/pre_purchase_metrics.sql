select base.idcustomer,base.snapshot_date,pre_no_of_sessions,pre_no_of_at,source_of_install from
dev.idcustomer_uidx_map_attriters as base 
left join 
(SELECT idcustomer,
       snapshot_date,
                   SUM(sessions) AS pre_no_of_sessions
                   FROM dev.idcustomer_uidx_map_attriters as dci
  LEFT JOIN clickstream.daily_aggregates AS dail_agg
         ON dci.uidx = dail_agg.uidx
        AND dail_agg.load_date < order_created_date
group by 1,2 ) a on base.idcustomer=a.idcustomer and base.snapshot_date=a.snapshot_date
left join
(SELECT idcustomer,
       snapshot_date,
       COUNT(DISTINCT article_type) AS pre_no_of_at
       FROM dev.idcustomer_uidx_map_attriters as dci
  LEFT JOIN clickstream.discount_aggregates AS disc_agg
         ON dci.uidx = disc_agg.uidx
        AND disc_agg.load_date< order_created_date
  left join bidb.dim_style ds ON disc_agg.style_id = ds.style_id
group by 1,2 ) b on base.idcustomer=b.idcustomer and base.snapshot_date=b.snapshot_date
left join
(select 
idcustomer,
       snapshot_date
       ,source_of_install from dev.customer_attriters_ids as dci
       left join bidb.user_first_install as fi
       ON dci.idcustomer = fi.id
)as c
on base.idcustomer=c.idcustomer and base.snapshot_date=c.snapshot_date