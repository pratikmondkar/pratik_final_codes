select base.idcustomer,base.snapshot_date,no_of_sessions_total,cards_clicked_total,add_to_list_total,no_of_searches_total,add_to_carts_total,pdp_views_total,
no_of_sessions_recent,cards_clicked_recent,add_to_list_recent,no_of_searches_recent,add_to_carts_recent,pdp_views_recent
,last_visit_date
,notif_open_rate_total
,no_of_at_total,discount_perc_total,
notif_open_rate_recent
,no_of_at_recent,discount_perc_recent
,case when add_to_carts_total>0 then 4
       when add_to_list_total>0 then 3
       when pdp_views_total>0 then 2
       when no_of_sessions_total>0 then 1
       else 0 end as funnel_stage
from
dev.idcustomer_uidx_map_attriters as base 
left join 
(SELECT idcustomer,
       snapshot_date,
                   SUM(sessions) AS no_of_sessions_total,
                    SUM(cards_clicked) AS cards_clicked_total,
                    SUM(add_to_list) AS add_to_list_total,
                    SUM(searches_fired) AS no_of_searches_total,
                    sum(added_to_cart) as add_to_carts_total,
                    sum(all_pdp_views) as pdp_views_total,
                    max(load_date) as last_visit_date
FROM dev.idcustomer_uidx_map_attriters as dci
  LEFT JOIN clickstream.daily_aggregates AS dail_agg
         ON dci.uidx = dail_agg.uidx
        AND dail_agg.load_date BETWEEN order_created_date
        AND snapshot_date
        and snapshot_date>order_created_date
group by 1,2 ) a on base.idcustomer=a.idcustomer and base.snapshot_date=a.snapshot_date
left join
(SELECT idcustomer,
       snapshot_date,
                   SUM(sessions) AS no_of_sessions_recent,
                    SUM(cards_clicked) AS cards_clicked_recent,
                    SUM(add_to_list) AS add_to_list_recent,
                    SUM(searches_fired) AS no_of_searches_recent,
                    sum(added_to_cart) as add_to_carts_recent,
                    sum(all_pdp_views) as pdp_views_recent
FROM dev.idcustomer_uidx_map_attriters as dci
  LEFT JOIN clickstream.daily_aggregates AS dail_agg
         ON dci.uidx = dail_agg.uidx
        AND dail_agg.load_date BETWEEN TO_CHAR(TO_DATE(snapshot_date,'YYYYMMDD') -INTERVAL '30 days','YYYYMMDD')::BIGINT
        AND snapshot_date
        and snapshot_date>order_created_date
group by 1,2 ) d on base.idcustomer=d.idcustomer and base.snapshot_date=d.snapshot_date
left join 
(SELECT idcustomer,
       snapshot_date,
      (SUM(notif_open_count) / nullif(SUM(notif_receive_count)*1.0,0))*100 AS notif_open_rate_total
FROM dev.idcustomer_uidx_map_attriters as dci
  LEFT JOIN clickstream.daily_notif_aggregates AS notif_agg
         ON dci.uidx = notif_agg.uidx
        AND notif_agg.load_date BETWEEN order_created_date
        AND snapshot_date
        and snapshot_date>order_created_date
group by 1,2) b on base.idcustomer=b.idcustomer and base.snapshot_date=b.snapshot_date
left join
(SELECT idcustomer,
       snapshot_date,
      (SUM(notif_open_count) / nullif(SUM(notif_receive_count)*1.0,0))*100 AS notif_open_rate_recent
FROM dev.idcustomer_uidx_map_attriters as dci
  LEFT JOIN clickstream.daily_notif_aggregates AS notif_agg
         ON dci.uidx = notif_agg.uidx
        AND notif_agg.load_date BETWEEN TO_CHAR(TO_DATE(snapshot_date,'YYYYMMDD') -INTERVAL '30 days','YYYYMMDD')::BIGINT
        AND snapshot_date
                and snapshot_date>order_created_date
group by 1,2) e on base.idcustomer=e.idcustomer and base.snapshot_date=e.snapshot_date
left join
(SELECT idcustomer,
       snapshot_date,
       COUNT(DISTINCT article_type) AS no_of_at_total,
               AVG((disc_agg.mrp::float - disc_agg.discounted_price::float) / nullif(disc_agg.mrp::float,0)*100) AS discount_perc_total
FROM dev.idcustomer_uidx_map_attriters as dci
  LEFT JOIN clickstream.discount_aggregates AS disc_agg
         ON dci.uidx = disc_agg.uidx
        AND disc_agg.load_date BETWEEN order_created_date
        AND snapshot_date
                and snapshot_date>order_created_date
  left join bidb.dim_style ds ON disc_agg.style_id = ds.style_id
    where trim(disc_agg.mrp) !='' and trim(disc_agg.discounted_price) !=''
group by 1,2 ) c on base.idcustomer=c.idcustomer and base.snapshot_date=c.snapshot_date
left join
(SELECT idcustomer,
       snapshot_date,
       COUNT(DISTINCT article_type) AS no_of_at_recent,
               AVG((disc_agg.mrp::float - disc_agg.discounted_price::float) / nullif(disc_agg.mrp::float,0)*100) AS discount_perc_recent
FROM dev.idcustomer_uidx_map_attriters as dci
  LEFT JOIN clickstream.discount_aggregates AS disc_agg
         ON dci.uidx = disc_agg.uidx
        AND disc_agg.load_date BETWEEN TO_CHAR(TO_DATE(snapshot_date,'YYYYMMDD') -INTERVAL '30 days','YYYYMMDD')::BIGINT
        AND snapshot_date
                and snapshot_date>order_created_date
  left join bidb.dim_style ds ON disc_agg.style_id = ds.style_id
  where trim(disc_agg.mrp) !='' and trim(disc_agg.discounted_price) !=''
group by 1,2 )
f on base.idcustomer=f.idcustomer and base.snapshot_date=f.snapshot_date