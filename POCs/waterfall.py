import pandas as pd
from pandas import DataFrame
import numpy as np
import pickle
import math
import time
from datetime import datetime as dt
import scipy as scipy
from scipy import stats
from sklearn import linear_model, metrics, preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor
from sklearn import svm
import xgboost as xgb
from xgboost import XGBRegressor
from modelselector import EstimatorSelectionHelper
import dash
import dash_core_components as dcc
import dash_html_components as html
#import dash_table_experiments as dt
import plotly.graph_objs as go
import plotly.plotly as py
import plotly.figure_factory as ff

#from app import app

app = dash.Dash()

#app.css.append_css({"external_url": "https://codepen.io/chriddyp/pen/bWLwgP.css"})

#app.css.append_css({"external_url": "https://codepen.io/chriddyp/pen/brPBPO.css"})

bag_list=pd.read_csv('/data/pratik/tensor/b_a_g_list.csv')

inp_data=pd.read_csv('/data/pratik/tensor/datasets/ad_clean_2k_inp.csv',error_bad_lines=False)

compete=pd.read_csv('/data/pratik/tensor/trends/google_trends.csv',error_bad_lines=False)
compete['comp_index']=compete['Myntra']/compete['Amazon']
compete['date']=compete['date'].str.replace('-','').astype(int)
compete.drop_duplicates('date',inplace=True)
inp_data['comp_index']=compete.comp_index.mean()

ind_list=['wgt_input_td','input_td_diff','lc_share_pltf','live_styles','presale_flag','postsale_flag',
 'sessions','brokeness','freshness','output_cd','loyalty_points','vis_cannib_mean','comp_index','sales_lag','index_week_year','price_cannib_mean','brokenness_rm','freshness_rm','atc_rm']


app.layout = html.Div([
    html.Div([

        html.Div([
	    html.Label('Article Type'),
            dcc.Dropdown(
                id='filter-article_type-wi',
		options=[{'label': i, 'value': i} for i in sorted(list(bag_list['article_type'].unique()))],            
		value='Tshirts'
            )
        ],
        style={'width': '33%', 'display': 'inline-block'}),

        html.Div([
	    html.Label('Gender'),
            dcc.Dropdown(
                id='filter-gender-wi',
                options=[{'label': i, 'value': i} for i in sorted(list(bag_list['gender'].unique()))],
		value='Men'
            )
        ], style={'width': '33%', 'float': 'center', 'display': 'inline-block'}),
        html.Div([
        html.Label('Brand'),
            dcc.Dropdown(
                id='filter-brand-wi',
                options=[{'label': i, 'value': i} for i in sorted(list(bag_list['brand'].unique()))],
        value='Roadster'
            )
    ], style={'width': '33%', 'float': 'center', 'display': 'inline-block'})
    ]
        , style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(250, 250, 250)',
        'padding': '10px 10px 10px'
    }),

        html.Hr(),

        html.Div([

        html.Div([
        html.Label('Baseline range'),
            dcc.DatePickerRange(
                id='baseline-daternge',
                min_date_allowed=pd.to_datetime(inp_data.date.min(),format='%Y%m%d'),
                max_date_allowed=pd.to_datetime(inp_data.date.max(),format='%Y%m%d'),
                minimum_nights=0,
                end_date=dt.now(),
                clearable=True,
                start_date_placeholder_text='Select a start date',
                end_date_placeholder_text='Select a end date'
                )
            ], style={'width': '49%','float': 'left', 'display': 'inline-block'}),

        html.Div([
        html.Label('Comparison range'),
            dcc.DatePickerRange(
                id='comp-daternge',  
                min_date_allowed=pd.to_datetime(inp_data.date.min(),format='%Y%m%d'),
                max_date_allowed=pd.to_datetime(inp_data.date.max(),format='%Y%m%d'),
                minimum_nights=0,
                end_date=dt.now(),
                clearable=True,
                start_date_placeholder_text='Select a start date',
                end_date_placeholder_text='Select a end date'
                )
            ], style={'width': '49%','float': 'right', 'display': 'inline-block'}),
        ]
        , style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(250, 250, 250)',
        'padding': '10px 10px 10px'
    }),
    
    html.Hr(),

    html.Div([
        dcc.Graph(
            id='waterfall-chart'
        )
    ], style={'width': '98%', 'display': 'inline-block', 'padding': '0 20'})
            
            
])



@app.callback(
    dash.dependencies.Output('filter-gender-wi', 'options'),
    [dash.dependencies.Input('filter-article_type-wi', 'value')])
def set_gender_options(selected_at):
    return [{'label': i, 'value': i} for i in sorted(list(bag_list[bag_list['article_type']==selected_at]['gender'].unique())) ]

@app.callback(
    dash.dependencies.Output('filter-gender-wi', 'value'),
    [dash.dependencies.Input('filter-gender-wi', 'options')])
def set_gender_value(available_options):
    return available_options[0]['value']

@app.callback(
    dash.dependencies.Output('filter-brand-wi', 'options'),
    [dash.dependencies.Input('filter-article_type-wi', 'value'),
     dash.dependencies.Input('filter-gender-wi', 'value')])
def set_brand_options(selected_at,selected_gender):
    return [{'label': i, 'value': i} for i in sorted(list(bag_list[(bag_list['article_type']==selected_at) & (bag_list['gender']==selected_gender)]['brand'].unique())) ]

@app.callback(
    dash.dependencies.Output('filter-brand-wi', 'value'),
    [dash.dependencies.Input('filter-brand-wi', 'options')])
def set_brand_value(available_options):
    return available_options[0]['value']


@app.callback(
     dash.dependencies.Output('waterfall-chart', 'figure'),
     [dash.dependencies.Input('filter-brand-wi', 'value'),
      dash.dependencies.Input('filter-article_type-wi', 'value'),
      dash.dependencies.Input('filter-gender-wi', 'value'),
      dash.dependencies.Input('baseline-daternge', 'start_date'),
      dash.dependencies.Input('baseline-daternge', 'end_date'),
      dash.dependencies.Input('comp-daternge', 'start_date'),
      dash.dependencies.Input('comp-daternge', 'end_date')])
def generate_waterfall(b,a,g,bsd,bed,csd,ced):
    path_m=r'/data/pratik/tensor/models/'
    bsd=int(dt.strptime(bsd, '%Y-%m-%d').strftime('%Y%m%d'))
    bed=int(dt.strptime(bed, '%Y-%m-%d').strftime('%Y%m%d'))
    csd=int(dt.strptime(csd, '%Y-%m-%d').strftime('%Y%m%d'))
    ced=int(dt.strptime(ced, '%Y-%m-%d').strftime('%Y%m%d'))
    filename=b+'-'+a+'-'+g  
    with open(path_m+filename+'.pkl', 'rb') as f:
        model = pickle.load(f)
    
    ad_b=inp_data[(inp_data['brand']==b) & (inp_data['article_type']==a) & (inp_data['gender']==g) & (inp_data['date']>=bsd) & (inp_data['date']<=bed) ].reset_index()
    ad_s=inp_data[(inp_data['brand']==b) & (inp_data['article_type']==a) & (inp_data['gender']==g) & (inp_data['date']>=csd) & (inp_data['date']<=ced) ].reset_index()

    ad_b['pred']=model.predict(ad_b[ind_list])
    ad_s['pred']=model.predict(ad_s[ind_list])
    
    plot_data=pd.DataFrame()
    plot_data['Baseline']=ad_b['pred']
    for i in ind_list:
        inp=ad_b[ind_list]
        inp[i]=ad_s[i]
        plot_data[i]=model.predict(inp)-ad_b['pred']
    plot_data['mixed_effects']=plot_data[ind_list].sum(axis=1)
    plot_data['Current']=ad_s['pred']
    plot_data['mixed_effects']=(plot_data['Current']-plot_data['Baseline']) - plot_data[ind_list].sum(axis=1)
    
    delta=list(plot_data.values[0])
    plot_values_bot=[]
    for i in range(0,len(plot_data.columns)):
        if i==0 or i==len(plot_data.columns)-1:
            plot_values_bot.append(0)
        else:
            plot_values_bot.append(sum(delta[:i]))

    x_data = list(plot_data.columns)
    y_data = plot_values_bot
    text = list(str(round(i,2)) for i in delta)
    # Base
    trace0 = go.Bar(
        x=x_data,
        y=plot_values_bot,
        #[0, 430, 0, 570, 370, 370, 0],
        marker=dict(
            color='rgba(1,1,1, 0.0)',
        )
    )

    # Revenue
    trace1 = go.Bar(
        x=x_data,
        y=delta,
        marker=dict(
            color='rgba(55, 128, 191, 0.7)',
            line=dict(
                color='rgba(55, 128, 191, 1.0)',
                width=2,
            )
        )
    )
    data = [trace0,trace1]
    layout = go.Layout(
        title='Change attribution',
        barmode='stack',
        paper_bgcolor='rgba(245, 246, 249, 1)',
        plot_bgcolor='rgba(245, 246, 249, 1)',
        showlegend=False
    )

    annotations = []

    for i in range(0, 16):
        annotations.append(dict(x=x_data[i], y=y_data[i], text=text[i],
                                      font=dict(family='Arial', size=10,
                                      color='rgba(245, 246, 249, 1)'),
                                      showarrow=False,))
        layout['annotations'] = annotations

    return go.Figure(data=data, layout=layout)



ap=app.server

if __name__ == '__main__':
    ap.run()
