import pandas as pd
from pandas import DataFrame
import numpy as np
import pickle
import math
import time
from datetime import datetime as dt
import scipy as scipy
from scipy import stats
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table_experiments as dte
import plotly.graph_objs as go
import plotly.plotly as py
import plotly.figure_factory as ff
from rpy2.robjects.packages import importr
import rpy2.robjects as robjects
from rpy2.robjects import pandas2ri
import numpy as np
pandas2ri.activate()
r = robjects.r
#from app import app

app = dash.Dash()

#app.css.append_css({"external_url": "https://codepen.io/chriddyp/pen/bWLwgP.css"})

#app.css.append_css({"external_url": "https://codepen.io/chriddyp/pen/brPBPO.css"})


inp_data=pd.read_csv('/home/pratik/python_notebooks/learning/RB_DelNonT&B_Data_20180320.csv',error_bad_lines=False)
inp_data.rename(columns={'order_created_date':'date'},inplace=True)

ind_list=['shipment_size_1','canc_perc','s2d_lessthan4','at_watches','cod_share','opp_gender_buy','ps_1','south','at_heels','desktop']

tmp=inp_data[inp_data.index==0][ind_list]
tmp['timeperiod']=None
init_df=tmp[tmp.index==-1]

r['load']('/home/pratik/python_notebooks/learning/ReturnsBridge_20180320.rda')
model=r['mod']

app.layout = html.Div([

        html.Div([

        html.Div([
        html.Label('Baseline range'),
            dcc.DatePickerRange(
                id='baseline-daternge',
                min_date_allowed=pd.to_datetime(inp_data.date.min(),format='%Y%m%d'),
                max_date_allowed=pd.to_datetime(inp_data.date.max(),format='%Y%m%d'),
                minimum_nights=0,
		end_date=pd.to_datetime(inp_data.date.max(),format='%Y%m%d'),
		start_date=pd.to_datetime(inp_data.date.max(),format='%Y%m%d'),
#                end_date=dt.now(),
                clearable=True,
                start_date_placeholder_text='Select a start date',
                end_date_placeholder_text='Select a end date'
                )
            ], style={'width': '49%','float': 'left', 'display': 'inline-block'}),

        html.Div([
        html.Label('Comparison range'),
            dcc.DatePickerRange(
                id='comp-daternge',  
                min_date_allowed=pd.to_datetime(inp_data.date.min(),format='%Y%m%d'),
                max_date_allowed=pd.to_datetime(inp_data.date.max(),format='%Y%m%d'),
                minimum_nights=0,
		end_date=pd.to_datetime(inp_data.date.max(),format='%Y%m%d'),
		start_date=pd.to_datetime(inp_data.date.max(),format='%Y%m%d'),
#                end_date=dt.now(),
                clearable=True,
                start_date_placeholder_text='Select a start date',
                end_date_placeholder_text='Select a end date'
                )
            ], style={'width': '49%','float': 'right', 'display': 'inline-block'}),
        ]
        , style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(250, 250, 250)',
        'padding': '10px 10px 10px'
    }),
    
    html.Hr(),

    html.Div([
        dcc.Graph(
            id='waterfall-chart'
        )
    ], style={'width': '98%', 'display': 'inline-block', 'padding': '0 20'}),

    html.Hr(),
 #   html.Div([
 #   dte.DataTable(
 #               rows=init_df.to_dict('records'),
 #               row_selectable=True,
 #               filterable=True,
 #               sortable=True,
 #               selected_row_indices=[],
 #               id='data-table'
 #       )
 #   ], style={'width': '98%', 'display': 'inline-block', 'padding': '0 20'})
 
    html.Div([
        dcc.Graph(
            id='data-table',
        )
    ], style={'width': '98%', 'display': 'inline-block', 'padding': '0 20'})
           
])


@app.callback(
     dash.dependencies.Output('data-table', 'figure'),
     [dash.dependencies.Input('baseline-daternge', 'start_date'),
      dash.dependencies.Input('baseline-daternge', 'end_date'),
      dash.dependencies.Input('comp-daternge', 'start_date'),
      dash.dependencies.Input('comp-daternge', 'end_date')])
def generate_table_pr(bsd,bed,csd,ced):
        bsd=int(dt.strptime(bsd, '%Y-%m-%d').strftime('%Y%m%d'))
    	bed=int(dt.strptime(bed, '%Y-%m-%d').strftime('%Y%m%d'))
    	csd=int(dt.strptime(csd, '%Y-%m-%d').strftime('%Y%m%d'))
	ced=int(dt.strptime(ced, '%Y-%m-%d').strftime('%Y%m%d'))
	ad_b=inp_data[(inp_data['date']>=bsd) & (inp_data['date']<=bed) ].reset_index()[ind_list]
        ad_s=inp_data[(inp_data['date']>=csd) & (inp_data['date']<=ced) ].reset_index()[ind_list]

        ad_b['timeperiod']='Baseline'
        ad_s['timeperiod']='Comparison'

        dataframe=ad_b.append(ad_s)
	rolled_up=dataframe.groupby('timeperiod').mean().reset_index()
        return ff.create_table(rolled_up.round(2))


@app.callback(
     dash.dependencies.Output('waterfall-chart', 'figure'),
     [dash.dependencies.Input('baseline-daternge', 'start_date'),
      dash.dependencies.Input('baseline-daternge', 'end_date'),
      dash.dependencies.Input('comp-daternge', 'start_date'),
      dash.dependencies.Input('comp-daternge', 'end_date')])
def generate_waterfall(bsd,bed,csd,ced):
    bsd=int(dt.strptime(bsd, '%Y-%m-%d').strftime('%Y%m%d'))
    bed=int(dt.strptime(bed, '%Y-%m-%d').strftime('%Y%m%d'))
    csd=int(dt.strptime(csd, '%Y-%m-%d').strftime('%Y%m%d'))
    ced=int(dt.strptime(ced, '%Y-%m-%d').strftime('%Y%m%d'))
    
    ad_b=inp_data[(inp_data['date']>=bsd) & (inp_data['date']<=bed) ].reset_index()
    ad_s=inp_data[(inp_data['date']>=csd) & (inp_data['date']<=ced) ].reset_index()

    ad_b['pred']=np.asarray(r.predict(model, ad_b[ind_list]))
    ad_s['pred']=np.asarray(r.predict(model, ad_s[ind_list]))
    
    plot_data=pd.DataFrame()
    plot_data['Baseline']=ad_b['pred']
    for i in ind_list:
        inp=ad_b[ind_list]
        inp[i]=ad_s[i]
        plot_data[i]=np.asarray(r.predict(model, inp[ind_list]))-ad_b['pred']
    plot_data['mixed_effects']=plot_data[ind_list].sum(axis=1)
    plot_data['Current']=ad_s['pred']
    plot_data['mixed_effects']=(plot_data['Current']-plot_data['Baseline']) - plot_data[ind_list].sum(axis=1)
    
    delta=list(plot_data.values[0])
    plot_values_bot=[]
    for i in range(0,len(plot_data.columns)):
        if i==0 or i==len(plot_data.columns)-1:
            plot_values_bot.append(0)
        else:
            plot_values_bot.append(sum(delta[:i]))

    x_data = list(plot_data.columns)
    y_data = plot_values_bot
    text = list(str(round(i,2)) for i in delta)
    # Base
    trace0 = go.Bar(
        x=x_data,
        y=plot_values_bot,
        #[0, 430, 0, 570, 370, 370, 0],
        marker=dict(
            color='rgba(1,1,1, 0.0)',
        )
    )

    # Revenue
    trace1 = go.Bar(
        x=x_data,
        y=delta,
        marker=dict(
            color='rgba(55, 128, 191, 0.7)',
            line=dict(
                color='rgba(55, 128, 191, 1.0)',
                width=2,
            )
        )
    )
    data = [trace0,trace1]
    layout = go.Layout(
        title='Change attribution',
        barmode='stack',
        paper_bgcolor='rgba(245, 246, 249, 1)',
        plot_bgcolor='rgba(245, 246, 249, 1)',
        showlegend=False
    )

    annotations = []

    for i in range(0, len(plot_data.columns)):
        annotations.append(dict(x=x_data[i], y=y_data[i], text=text[i],
                                      font=dict(family='Arial', size=10,
                                      color='rgba(245, 246, 249, 1)'),
                                      showarrow=False,))
        layout['annotations'] = annotations

    return go.Figure(data=data, layout=layout)



ap=app.server

if __name__ == '__main__':
    ap.run()
