import pandas as pd
from pandas import DataFrame
import pickle
import numpy as np 
import math
import time
import scipy as scipy
from scipy import stats
from sklearn import linear_model, metrics, preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor
from sklearn import svm
import xgboost as xgb
from xgboost import XGBRegressor
from modelselector import EstimatorSelectionHelper
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go


app = dash.Dash()

df=pd.read_csv('/data/pratik/tensor/tool_data.csv',error_bad_lines=False)

with open('/data/pratik/tensor/summary/models.pkl', 'rb') as f:
    model = pickle.load(f)

app.layout = html.Div([
    html.Div([

        html.Div([
            dcc.Dropdown(
                id='filter-date',
                options=[{'label': i, 'value': i} for i in sorted(list(df['date'].unique()))],
                value=20170602
            ),
            dcc.Dropdown(
                id='filter-article_type',
                options=[{'label': i, 'value': i} for i in sorted(list(df['article_type'].unique()))],
                value='Tshirts'
            )
        ],
        style={'width': '49%', 'display': 'inline-block'}),

        html.Div([
            dcc.Dropdown(
                id='filter-gender',
                options=[{'label': i, 'value': i} for i in sorted(list(df['gender'].unique()))],
                value='Men'
            ),
            dcc.Dropdown(
                id='filter-brand',
                options=[{'label': i, 'value': i} for i in sorted(list(df['brand'].unique()))],
                value='Roadster'
            )
        ], style={'width': '49%', 'float': 'right', 'display': 'inline-block'})
    ], style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(250, 250, 250)',
        'padding': '10px 5px'
    }),

    html.Div([
        dcc.Graph(
            id='crossfilter-indicator-scatter',
            hoverData={'points': [{'customdata': 'Japan'}]}
        )
    ], style={'width': '49%', 'display': 'inline-block', 'padding': '0 20'}),
    html.Div(dcc.Slider(
        id='filter-disc-slider',
        min=0,
        max=1,
        value=0.4,
        step=None,
        marks={str(year): str(year) for year in np.arange(0, .85, .05)}
    ), style={'width': '49%', 'padding': '0px 20px 20px 20px'})
])

@app.callback(
    dash.dependencies.Output('filter-article_type', 'options'),
    [dash.dependencies.Input('filter-date', 'value')])
def set_AT_options(selected_date):
    return [{'label': i, 'value': i} for i in sorted(list(data[data['date']==selected_date]['article_type'].unique())) ]

@app.callback(
    dash.dependencies.Output('filter-article_type', 'value'),
    [dash.dependencies.Input('filter-article_type', 'options')])
def set_AT_value(available_options):
    return available_options[0]['value']

@app.callback(
    dash.dependencies.Output('filter-gender', 'options'),
    [dash.dependencies.Input('filter-date', 'value'),
     dash.dependencies.Input('filter-article_type', 'value')])
def set_gender_options(selected_date,selected_at):
    return [{'label': i, 'value': i} for i in sorted(list(data[(data['date']==date.value) & (data['article_type']==selected_at)]['gender'].unique())) ]

@app.callback(
    dash.dependencies.Output('filter-gender', 'value'),
    [dash.dependencies.Input('filter-gender', 'options')])
def set_gender_value(available_options):
    return available_options[0]['value']

@app.callback(
    dash.dependencies.Output('filter-brand', 'options'),
    [dash.dependencies.Input('filter-date', 'value'),
     dash.dependencies.Input('filter-article_type', 'value'),
     dash.dependencies.Input('filter-gender', 'value')])
def set_brand_options(selected_date,selected_at,selected_gender):
    return [{'label': i, 'value': i} for i in sorted(list(data[(data['date']==date.value) & (data['article_type']==selected_at) & (data['gender']==selected_gender)]['gender'].unique())) ]

@app.callback(
    dash.dependencies.Output('filter-brand', 'value'),
    [dash.dependencies.Input('filter-brand', 'options')])
def set_brand_value(available_options):
    return available_options[0]['value']


@app.callback(
    dash.dependencies.Output('crossfilter-indicator-scatter', 'figure'),
    [dash.dependencies.Input('filter-date', 'value'),
     dash.dependencies.Input('filter-article_type', 'value'),
     dash.dependencies.Input('filter-gender', 'value'),
     dash.dependencies.Input('filter-brand', 'value'),
     dash.dependencies.Input('filter-disc-slider', 'value')])

def update_graph(date, article_Type,
                 gender, brand,
                 disc):
    bag=brand+article_Type+gender
    dat = df[(df['date'] == date) & (df['article_type'] == article_type) & (df['gender'] == gender) & (df['brand'] == brand)]
    dat['output_td']=disc
    dat['output_td_diff']=disc/dat['output_td_rm']
    v=np.random.normal(df[df['bag_id']==bag]['lc_share_pltf'].mean(),df[df['bag_id']==bag]['lc_share_pltf'].std(),100)
    dff=pd.DataFrame()
    dff=dff.append([dat]*len(v),ignore_index=True)
    dff['lc_share_pltf']=v
    dff=dff[dff['lc_share_pltf']>0]
    dff['ros']=model[bag].predict(dff[['output_td','output_td_diff','lc_share_pltf','live_styles','presale_flag','sessions','brokeness','freshness']])

    return {
        'data': [go.Scatter(
            x=dff['lc_share_pltf'],
            y=dff['ros'],
            mode='markers',
            marker={
                'size': 15,
                'opacity': 0.5,
                'line': {'width': 0.5, 'color': 'white'}
            }
        )],
        'layout': go.Layout(
            xaxis={
                'title': 'Visibity (share of list counts)'
            },
            yaxis={
                'title': 'Units sold predicted'
            },
            margin={'l': 40, 'b': 30, 't': 10, 'r': 0},
            height=450,
            hovermode='closest'
        )
    }
