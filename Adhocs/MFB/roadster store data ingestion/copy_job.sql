drop table if exists  dev.roadster_store_sales;
CREATE TABLE dev.roadster_store_sales
(
			load_date			varchar(50),
			mrp_revenue		float8,
			discount			float8,
			net_revenue		float8,
			avg_item_selling_price float8,
			avg_order_value				 float8,
			basket_size		float8,
			conversions		float8,
			inventory     float8
)
SORTKEY (load_date);

truncate table dev.roadster_store_sales;

copy dev.roadster_store_sales   (load_date,mrp_revenue,discount,net_revenue,avg_item_selling_price,avg_order_value,basket_size,conversions,inventory)
FROM 's3://testashutosh/backups/roadster_store_data' credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
delimiter ',' 
emptyasnull 
removequotes
blanksasnull 
TRIMBLANKS 
TRUNCATECOLUMNS 
ACCEPTANYDATE 
ACCEPTINVCHARS AS ' ' 
maxerror 10 
explicit_ids 
compupdate off 
statupdate off;

create table bidb.roadster_store_sales sortkey(load_date) as
(select to_char(to_date(load_date,'DD-MON-YY'),'YYYYMMDD')::bigint as load_date,mrp_revenue,discount,net_revenue,avg_item_selling_price,avg_order_value,basket_size,conversions,inventory
from  dev.roadster_store_sales);


delete from bidb.roadster_store_sales where load_date in (select distinct to_char(to_date(load_date,'DD-MON-YY'),'YYYYMMDD')::bigint from dev.roadster_store_sales);

insert into bidb.roadster_store_sales
(select to_char(to_date(load_date,'DD-MON-YY'),'YYYYMMDD')::bigint as load_date,mrp_revenue,discount,net_revenue,avg_item_selling_price,avg_order_value,basket_size,conversions,inventory
from  dev.roadster_store_sales);
 
