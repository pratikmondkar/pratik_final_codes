CREATE TABLE dev.mango_store_sales
(
   store_code							 varchar(50),	
	 load_date               varchar(50),
   mrp_revenue             float8,
   discount                float8,
   tax             				 float8,
   quantity_sold  				 float8,
   orders         				 float8,
   footfalls             	 float8,
   inventory             	 float8,
   cogs               		 float8
) distkey(load_date)
sortkey(store_code);

truncate table dev.mango_store_sales;

copy dev.mango_store_sales   (store_code,load_date,mrp_revenue,discount,tax,quantity_sold,orders,footfalls,inventory,cogs)
FROM 's3://testashutosh/backups/mango_store_data' credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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'
delimiter ','
emptyasnull
removequotes
blanksasnull
TRIMBLANKS
TRUNCATECOLUMNS
ACCEPTANYDATE
ACCEPTINVCHARS AS ' '
maxerror 10
explicit_ids
compupdate off
statupdate off;


select * from dev.mango_store_sales;


CREATE TABLE bidb.mango_store_sales distkey(load_date) sortkey(store_code) as 
select store_code,to_char(to_date(load_date,'YYYY-MM-DD'),'YYYYMMDD')::bigint as load_date,mrp_revenue,discount,tax,quantity_sold,orders,footfalls,inventory,cogs
from  dev.mango_store_sales
