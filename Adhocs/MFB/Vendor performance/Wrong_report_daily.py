import pandas as pd
import numpy as np
import datetime
from datetime import date, timedelta
from pandas import DataFrame
import sqlalchemy as sq
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
engine = sq.create_engine("postgresql+psycopg2://analysis_user:AdhoCus@123!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

sql_str="""
select
p.vendor_article_number,
p.style_id,
p.article_type,
p.size,
brand,
total_inward_value,
total_inwards_qty,
till_date_sales ,
till_date_revenue,
yesterday_sales,
yesterday_revenue,
mrp
from
(select
a.style_id,
a.size,
a.vendor_article_number,
a.article_type,
sum(total_inward_value) as total_inward_value,
sum(total_inwards_qty) as total_inwards_qty
from
(select a.style_id,
a.sku_id,
report_date,
dp.size,
a.vendor_article_number,
a.article_type,
sum(total_inward_value) as total_inward_value,
sum(total_inwards_qty) as total_inwards_qty
from fact_product_profile a
join
dim_product dp
on a.sku_id = dp.sku_id and dp.brand in ('WROGN', 'IMARA') and dp.vendor_id = 882 group by 1,2,3,4,5,6) a
join
(select sku_id,
max(report_date) date1
from fact_product_profile
group by sku_id) b
on a.sku_id=b.sku_id and a.report_date=b.date1 group by 1,2,3,4) p
left join
(select dp.style_id,
brand,
dp.size,
sum(quantity) as till_date_sales ,
sum(item_revenue_inc_cashback) as till_date_revenue,
sum(case when order_created_date = CAST(to_char(dateadd(day, -1, 'today'), 'YYYYMMDD') AS integer) then quantity end) as yesterday_sales,
sum(case when order_created_date = CAST(to_char(dateadd(day, -1, 'today'), 'YYYYMMDD') AS integer) then item_revenue_inc_cashback end) as yesterday_revenue,
avg(item_mrp_value) as mrp
from fact_orderitem a join dim_product dp on a.idproduct = dp.id where dp.brand in ('WROGN', 'IMARA')  and (a.is_shipped=1
or a.is_realised=1) and dp.vendor_id = 882 group by 1,2,3) oi
on p.style_id = oi.style_id and p.size = oi.size
"""

outpath = r'/home/pratik/data_files/'
abc = date.today().strftime('%d-%b-%Y')
filename=outpath+ "MFB_Wrogn_Vendor_data_"+str(abc)+".csv"

report=pd.read_sql_query(sql_str,engine)
report.to_csv(filename,index=False)

sender = 'sudarson.tm@myntra.com'
receivers = ['amit.pandey@myntra.com','sudarson.tm@myntra.com']

msg = MIMEMultipart()
msg['Subject'] = 'MFB Partner Portal - WROGN for '+str(abc)
msg['From'] =sender
msg['to'] =", ".join(receivers)

part = MIMEText("Hi, Please find attached worksheet")
msg.attach(part)

f = file(filename)
attachment = MIMEText(f.read())
attachment.add_header('Content-Disposition', 'attachment', filename=filename)
msg.attach(attachment)

try:
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login("sudarson.tm@myntra.com", "tdnhlfqsqyxgtjwh")
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"
