SELECT master_category,
       brand,
       ROUND(SUM(nvl (pending_value_open_po,0))) AS pending_value_open_po,
       ROUND(SUM(nvl (inward_value_closed_po,0))) AS inward_value_closed_po,
       ROUND(SUM(nvl (consumption,0))) AS consumption
FROM (SELECT ia.barcode AS po_code,
             MAX(ia.master_category) AS master_category,
             MAX(ia.brand) AS brand,
             CASE
               WHEN MAX(po_status) = 'APPROVED' THEN SUM(landed_value*(po_quantity - grn_quantity))
             END AS pending_value_open_po,
             CASE
               WHEN MAX(po_status) != 'APPROVED' THEN SUM(landed_value*grn_quantity)
             END AS inward_value_closed_po,
             CASE
               WHEN MAX(po_status) = 'APPROVED' THEN SUM(landed_value*(po_quantity - grn_quantity))
               ELSE SUM(landed_value*grn_quantity)
             END AS consumption
      FROM inward_analyzer ia
        LEFT JOIN (SELECT barcode,
                          MAX(season_id) AS season_id,
                          MAX(season_year) AS season_year,
                          MAX(closure_date) AS closure_date
                   FROM fact_purchase_order fpo
                   WHERE (approval_date >= 20160101 OR closure_date >= 20160101)
                   AND   po_type = 'OUTRIGHT'
                   AND   LOWER (po_status) IN ('approved','completed','closed')
                   GROUP BY 1) ss ON ia.barcode = ss.barcode
        LEFT JOIN dim_style ds ON ia.style_id = ds.style_id
      WHERE (approval_date >= 20160101 OR ss.closure_date >= 20160101)
      AND   ia.brand_type = 'Private'
      AND   po_type = 'OUTRIGHT'
      AND   LOWER (po_status) IN ('approved','completed','closed')
      GROUP BY 1)
GROUP BY 1,
         2
HAVING ROUND(SUM(nvl (consumption,0))) > 0

