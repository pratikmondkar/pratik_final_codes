--Sessions and Users
select a.*,list_views,list_devices,pdp_views,pdp_devices,add_to_carts,atc_devices,revenue,orders,customers from 
(select campaign_utm_medium,campaign_utm_campaign,count(distinct session_session_id) as sessions ,count(distinct device_device_id) as devices  
from clickstream.applaunch_view 
where load_date=20160110  group by 1,2) a
left join
--List
(select al.campaign_utm_medium,al.campaign_utm_campaign,count(*) as list_views,count(distinct sl.device_device_id) as list_devices  from
clickstream.screenload_view sl
join 
(select distinct session_session_id,device_device_id,campaign_utm_campaign,campaign_utm_medium 
from clickstream.applaunch_view 
where load_date=20160110) al
on sl.session_session_id=al.session_session_id and sl.device_device_id=al.device_device_id
where sl.event_screen_name like 'Shopping Page-List Page%' and load_date=20160110 group by 1,2) b
on a.campaign_utm_campaign=b.campaign_utm_campaign and a.campaign_utm_medium=b.campaign_utm_medium
left join
--PDP
(select al.campaign_utm_medium,al.campaign_utm_campaign,count(*) as pdp_views,count(distinct sl.device_device_id) as pdp_devices  from
clickstream.screenload_view sl
join 
(select distinct session_session_id,device_device_id,campaign_utm_campaign,campaign_utm_medium  
from clickstream.applaunch_view 
where load_date=20160110) al
on sl.session_session_id=al.session_session_id and sl.device_device_id=al.device_device_id
where sl.event_screen_name like 'Shopping Page-PDP%' and load_date=20160110 group by 1,2) c
on a.campaign_utm_campaign=c.campaign_utm_campaign and a.campaign_utm_medium=c.campaign_utm_medium
left join
--Add to cart
(select al.campaign_utm_medium,al.campaign_utm_campaign,count(*) as add_to_carts,count(distinct sl.device_device_id) as atc_devices  from
clickstream.addtocart_view sl
join 
(select distinct session_session_id,device_device_id,campaign_utm_campaign,campaign_utm_medium 
from clickstream.applaunch_view
where load_date=20160110) al
on sl.session_session_id=al.session_session_id and sl.device_device_id=al.device_device_id
where load_date=20160110 group by 1,2) d
on a.campaign_utm_campaign=d.campaign_utm_campaign and a.campaign_utm_medium=d.campaign_utm_medium
left join 
--Transactions and Revenue
(select al.campaign_utm_medium,al.campaign_utm_campaign,sum(event_transaction_revenue) as revenue,count(distinct event_transaction_id) as orders,count(distinct user_customer_id) as customers  from
clickstream.screenload_view sl
join 
(select distinct session_session_id,device_device_id,campaign_utm_campaign,campaign_utm_medium  
from clickstream.applaunch_view 
where load_date=20160110) al
on sl.session_session_id=al.session_session_id and sl.device_device_id=al.device_device_id
where sl.event_screen_name='Checkout-confirmation' and load_date=20160110 group by 1,2) e
on a.campaign_utm_campaign=e.campaign_utm_campaign and a.campaign_utm_medium=e.campaign_utm_medium
