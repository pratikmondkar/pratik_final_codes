import pandas as pd
import datetime
from datetime import date, timedelta
from pandas import DataFrame
import sqlalchemy as sq
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
engine = sq.create_engine("postgresql+psycopg2://analysis_user:AdhoCus@123!@dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

sql_str="""
select al.campaign_utm_medium,al.campaign_utm_campaign,foi.brand,sum(foi.item_revenue_inc_cashback) as revenue,sum(foi.quantity) as qty  from
clickstream.screenload_view sl
join
(select distinct session_session_id,device_device_id,campaign_utm_campaign,campaign_utm_medium
from clickstream.applaunch_view
where load_date=to_char(sysdate-interval '1 day','YYYYMMDD')::bigint and
campaign_utm_campaign in ('hrx_onedigital_women','','hrx_facebook_women','hrx_google_women')) al
on sl.session_session_id=al.session_session_id and sl.device_device_id=al.device_device_id
join bidb.fact_core_item foi on sl.event_transaction_id=foi.order_group_id and foi.order_created_date=to_char(sysdate-interval '1 day','YYYYMMDD')::bigint
where sl.event_screen_name='Checkout-confirmation' and load_date=to_char(sysdate-interval '1 day','YYYYMMDD')::bigint
group by 1,2,3
"""

report=pd.read_sql_query(sql_str,engine)
report.to_csv('/home/pratik/data_files/campaign.csv',index=False)

sender = 'pratik.mondkar@myntra.com'
receivers = ['megha.ahuja@myntra.com','sindhu.nadig@myntra.com','mohua.dasgupta@myntra.com']

msg = MIMEMultipart('alternative')
msg['Subject'] = 'Campaign performance report'
msg['From'] =sender
msg['to'] =", ".join(receivers)

part = MIMEText("Hi, please find the attached today's detailed report")
msg.attach(part)

filename = "/home/pratik/data_files/campaign.csv"
f = file(filename)
attachment = MIMEText(f.read())
attachment.add_header('Content-Disposition', 'attachment', filename=filename)
msg.attach(attachment)

try:
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login("pratik.mondkar@myntra.com", "kaxuftanzoyzmjfe")
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"
