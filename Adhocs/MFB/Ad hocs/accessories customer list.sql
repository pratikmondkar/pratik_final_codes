select dce.email,dci.customer_login as uidx
from
(select distinct idcustomer 
from fact_core_item fci
where store_id=1 and (is_shipped=1 or is_realised=1) and master_category in ('Accessories') and
brand in ('Carlton London','Accessorize','Caprese','Hidesign','Lavie','Parfois')) a
join 
(select distinct idcustomer 
from fact_core_item fci
left join dim_product dp on fci.sku_id=dp.sku_id
where store_id=1 and (is_shipped=1 or is_realised=1) and 
business_unit in ('Women''s Ethnic','Women''s Western Wear') and fci.master_category in ('Apparel')) b on a.idcustomer=b.idcustomer
join 
(select distinct idcustomer 
from fact_core_item fci
where store_id=1 and (is_shipped=1 or is_realised=1) and item_revenue_inc_cashback>=1000 and master_category in ('Apparel')) c on a.idcustomer=c.idcustomer
left join dim_customer_Idea dci on a.idcustomer=dci.id
left join cii.dim_customer_email dce on a.idcustomer=dce.uid;


select distinct master_category from fact_core_item

