SELECT to_char(to_date(foi.order_created_date,'YYYYMMDD'),'YYYY') as year,
			 business_unit,
			 master_category,
			 sub_category,
			 article_type,
       (CASE WHEN (foi.supply_type = 'JUST_IN_TIME' OR commercial_type = 'JIT_Marketplace') THEN 'MP' WHEN (UPPER(commercial_type) = 'OUTRIGHT' AND UPPER(dp.brand_type) = 'PRIVATE') THEN 'MFB' ELSE 'MMB' END) AS brnd_type,
       brand,
			 city_tier,
			 city_group,
	sum(item_revenue_inc_cashback) as revenue
	FROM fact_orderitem foi
  JOIN dim_customer dc ON foi.idcustomer = dc.id
  JOIN dim_location dl ON foi.idlocation = dl.id
  JOIN dim_product dp ON foi.sku_id = dp.sku_id
  JOIN fact_order fo on foi.order_id=fo.order_id
where (foi.is_shipped=1 or foi.is_realised=1) and foi.store_id=1 and foi.order_created_date>=20140101
group by 1,2,3,4,5,6,7,8,9;

select distinct article_type,sub_category,master_category from dim_product

