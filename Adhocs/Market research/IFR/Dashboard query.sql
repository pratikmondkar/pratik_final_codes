SELECT 
				d.cal_year,
				d.month,
				d.month_string,
				dp.style_id,
				dl.city_group,
				SUM(item_revenue_inc_cashback) as revenue,
				COUNT(DISTINCT order_group_id) as orders,
				COUNT(DISTINCT idcustomer) as customers,
				ROUND(SUM(shipped_order_revenue_inc_cashback)/COUNT(DISTINCT order_group_id)::decimal(12,2),2) as avg_ord_value
				FROM fact_orderitem fo 
				JOIN dim_location dl ON fo.idlocation = dl.id
        JOIN dim_customer dc ON fo.idcustomer = dc.id
        JOIN dim_product dp ON fo.sku_id=dp.sku_id
        JOIN dim_date dd ON fo.order_created_date=dd.full_date
				WHERE dd.month_diff<=6 and dd.month_diff>0
				AND  store_id=1
				AND (is_realised=1 OR is_shipped=1)
				AND d.city_group in ()
				group by 1,2,3,4,5
