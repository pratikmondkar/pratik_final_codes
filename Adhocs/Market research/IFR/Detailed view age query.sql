--Age
select g.*,c.city_rev from 
(select  article_type,dpa.attribute_type,dpa.attribute_value,city_group,age_cus,sum(item_revenue_inc_cashback) 
FROM 
			(	select 	dp.article_type,dl.city_group,
								case when datediff(y,TO_DATE(CASE WHEN dc.dob IS NULL OR dc.dob < 19700101 THEN dc.fb_birthday ELSE dc.dob END,'YYYYMMDD'),SYSDATE)::INTEGER <=20 then '<=20'
								when datediff(y,TO_DATE(CASE WHEN dc.dob IS NULL OR dc.dob < 19700101 THEN dc.fb_birthday ELSE dc.dob END,'YYYYMMDD'),SYSDATE)::INTEGER between 20 and 25 then '21-25'
								when datediff(y,TO_DATE(CASE WHEN dc.dob IS NULL OR dc.dob < 19700101 THEN dc.fb_birthday ELSE dc.dob END,'YYYYMMDD'),SYSDATE)::INTEGER between 26 and 30 then '26-30'
								when datediff(y,TO_DATE(CASE WHEN dc.dob IS NULL OR dc.dob < 19700101 THEN dc.fb_birthday ELSE dc.dob END,'YYYYMMDD'),SYSDATE)::INTEGER between 31 and 35 then '31-35'
								when datediff(y,TO_DATE(CASE WHEN dc.dob IS NULL OR dc.dob < 19700101 THEN dc.fb_birthday ELSE dc.dob END,'YYYYMMDD'),SYSDATE)::INTEGER between 36 and 40 then '36-40'
								ELSE '>40' END AS age_cus,
								style_id,sum(item_revenue_inc_cashback) as item_revenue_inc_cashback 
			from fact_orderitem fo
			JOIN dim_location dl ON fo.idlocation = dl.id
			JOIN dim_customer dc ON fo.idcustomer = dc.id
			JOIN dim_product dp ON fo.sku_id=dp.sku_id
			WHERE order_created_date >= (TO_CHAR(SYSDATE-INTERVAL '1 year','YYYYMMDD'))
			AND (is_realised=1 or is_shipped=1) and store_id=1
			AND (dc.dob > 19700101 OR dc.fb_birthday > 19700101)
			AND dl.city_group in ('BANGALORE',	'DELHI-NCR',	'MUMBAI',	'PUNE',	'HYDERABAD',	'CHENNAI',	'JAIPUR',	'KOLKATA',	'AHMEDABAD',	'SURAT',	'GOA',	'LUCKNOW',	'ERNAKULAM',	'PATNA',	'GUWAHATI')
			group by 1,2,3,4) f
JOIN (select distinct style_id,lower(attribute_type) as attribute_type,lower(attribute_value) as attribute_value 
			from dim_product_attributes where attribute_value is not NULL and attribute_value!='NA') dpa ON f.style_id =dpa.style_id::integer 
group by 1,2,3,4,5) g
left join
			(select article_type,dpa.attribute_type,dpa.attribute_value,city_group,sum(item_revenue_inc_cashback) as city_rev
			FROM 
					(	select dp.article_type,dl.city_group,style_id,sum(item_revenue_inc_cashback) as item_revenue_inc_cashback 
						from fact_orderitem fo
					  JOIN dim_product dp ON fo.sku_id=dp.sku_id
					  JOIN dim_location dl ON fo.idlocation = dl.id
						WHERE order_created_date >= (TO_CHAR(SYSDATE-INTERVAL '1 year','YYYYMMDD'))
						AND (is_realised=1 or is_shipped=1) and store_id=1
						AND dl.city_group in ('BANGALORE',	'DELHI-NCR',	'MUMBAI',	'PUNE',	'HYDERABAD',	'CHENNAI',	'JAIPUR',	'KOLKATA',	'AHMEDABAD',	'SURAT',	'GOA',	'LUCKNOW',	'ERNAKULAM',	'PATNA',	'GUWAHATI')
						group by 1,2,3) f
			JOIN (select distinct style_id,lower(attribute_type) as attribute_type,lower(attribute_value) as attribute_value 
						from dim_product_attributes where attribute_value is not NULL and attribute_value!='NA') dpa
			ON f.style_id =dpa.style_id::integer
			group by 1,2,3,4) c
on g.article_type=c.article_type and g.attribute_type=c.attribute_type and g.attribute_value=c.attribute_value and g.city_group=c.city_group;
