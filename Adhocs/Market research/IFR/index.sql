select c.*,n.nation_rev
from 
			(	select dl.city_group,sum(item_revenue_inc_cashback) as city_rev 
				from fact_orderitem fo
			  JOIN dim_location dl ON fo.idlocation = dl.id
				WHERE order_created_date >= (TO_CHAR(SYSDATE-INTERVAL '1 year','YYYYMMDD'))
				AND (is_realised=1 or is_shipped=1) and store_id=1
				AND dl.city_group in ('BANGALORE',	'DELHI-NCR',	'MUMBAI',	'PUNE',	'HYDERABAD',	'CHENNAI',	'JAIPUR',	'KOLKATA',	'AHMEDABAD',	'SURAT',	'GOA',	'LUCKNOW',	'ERNAKULAM',	'PATNA',	'GUWAHATI')
				group by 1) c
cross JOIN 
					(	select sum(item_revenue_inc_cashback) as nation_rev 
						from fact_orderitem fo
					  JOIN dim_product dp ON fo.sku_id=dp.sku_id
						WHERE order_created_date >= (TO_CHAR(SYSDATE-INTERVAL '1 year','YYYYMMDD'))
						AND (is_realised=1 or is_shipped=1) and store_id=1 ) n
order by 1;
