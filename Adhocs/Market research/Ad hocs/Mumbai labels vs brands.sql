select 
case when o.supply_type='ON_HAND' and dp.brand_type='External' then 'Brands'
		 when o.supply_type='JUST_IN_TIME' then 'Labels'
		 else 'MFB' end as types,  
sum(item_revenue_inc_cashback) as rev, count(distinct order_group_id) as orders from
fact_orderitem o
join dim_product dp
on o.sku_id=dp.sku_id
join dim_location dl
on o.idlocation=dl.id
where store_id=1 and (is_realised=1 or is_shipped=1) and order_created_date >=to_char(sysdate- interval '6 months','YYYYMMDD') 
--and city_group='MUMBAI'
group by 1;

select distinct city_group from dim_location
