SELECT fps.date,
			 dp.article_type,
			 unified_size,
       COUNT(distinct fps.style_id) as styles
FROM fact_product_snapshot fps
join dim_product dp
on fps.sku_id=dp.sku_id
WHERE article_type IN ('Kurtas','Tops','Dresses')
AND   gender IN ('Men','Women')
AND   unified_size IN ('M','L','S','XL','XS','XXL')
and date between 20151001 and 20151231
and is_live_on_portal=1 and fps.style_status='P'
GROUP BY 1,2,3
ORDER BY 4 DESC;


SELECT fps.date,
       COUNT(distinct fps.style_id) as styles
FROM fact_product_snapshot fps
join dim_product dp
on fps.sku_id=dp.sku_id
WHERE article_type IN ('Kurtas','Tops','Dresses')
AND   gender IN ('Men','Women')
AND   unified_size IN ('M','L','S','XL','XS','XXL')
and date between 20151001 and 20151231
and is_live_on_portal=1 and fps.style_status='P'
GROUP BY 1
