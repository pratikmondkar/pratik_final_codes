SELECT a.*,
       b.total_styles
FROM (SELECT fps.date,
             dp.article_type,
             unified_size,
             COUNT(DISTINCT fps.style_id) AS styles
      FROM fact_product_snapshot fps
        JOIN dim_product dp ON fps.sku_id = dp.sku_id
      WHERE article_type IN ('Shirts')
      AND   gender IN ('Men','Women')
      AND   unified_size IN ('38','39','40','42','44','46')
      AND   DATE BETWEEN 20151001 AND 20151231
      AND   is_live_on_portal = 1
      AND   fps.style_status = 'P'
      GROUP BY 1,
               2,
               3
      ORDER BY 4 DESC) a
  LEFT JOIN (SELECT fps.date,
                    COUNT(DISTINCT fps.style_id) AS total_styles
             FROM fact_product_snapshot fps
               JOIN dim_product dp ON fps.sku_id = dp.sku_id
             WHERE article_type IN ('Shirts')
             AND   gender IN ('Men','Women')
             AND   unified_size IN ('38','39','40','42','44','46')
             AND   DATE BETWEEN 20151001 AND 20151231
             AND   is_live_on_portal = 1
             AND   fps.style_status = 'P'
             GROUP BY 1) b ON a.date = b.date

