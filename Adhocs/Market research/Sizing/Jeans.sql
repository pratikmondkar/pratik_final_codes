select unified_size,count(1) from dim_product WHERE article_type IN ('Jeans') AND   gender IN ('Men','Women') group by 1;

SELECT a.*,
       b.styles
FROM (SELECT fps.date,
             dp.article_type,
             unified_size,
             COUNT(DISTINCT fps.style_id) AS styles
      FROM fact_product_snapshot fps
        JOIN dim_product dp ON fps.sku_id = dp.sku_id
      WHERE article_type IN ('Jeans')
      AND   gender IN ('Men','Women')
      AND   unified_size IN ('28','30','32','34','36','38')
      AND   DATE BETWEEN 20151001 AND 20151231
      AND   is_live_on_portal = 1
      AND   fps.style_status = 'P'
      GROUP BY 1,
               2,
               3
      ORDER BY 4 DESC) a
  LEFT JOIN (SELECT fps.date,
                    COUNT(DISTINCT fps.style_id) AS styles
             FROM fact_product_snapshot fps
               JOIN dim_product dp ON fps.sku_id = dp.sku_id
             WHERE article_type IN ('Jeans')
             AND   gender IN ('Men','Women')
             AND   unified_size IN ('28','30','32','34','36','38')
             AND   DATE BETWEEN 20151001 AND 20151231
             AND   is_live_on_portal = 1
             AND   fps.style_status = 'P'
             GROUP BY 1) b ON a.date = b.date
