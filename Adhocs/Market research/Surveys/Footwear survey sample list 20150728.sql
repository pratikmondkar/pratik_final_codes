select 
distinct
dc.customer_login
--random()::decimal(10,2) as rand
from fact_orderitem foi,
dim_customer dc,
fact_customer_profile fcp,
dim_product dp
where foi.idcustomer=dc.id and foi.sku_id=dp.sku_id and dc.id=fcp.idcustomer and foi.is_realised=1 and foi.order_created_date>to_char(sysdate - interval '6 months','YYYYMMDD')
and realised_order_count>1 and master_category='Footwear' and avg_selling_price>1000;
--order by rand

select 
distinct
dp.brand,
dp.article_type,
dc.gender,
case 	when datediff(y,to_date(dc.dob,'YYYYMMDD'),sysdate)<21 then '<21'
			when datediff(y,to_date(dc.dob,'YYYYMMDD'),sysdate) between 21 and 30 then '21-30' 
			when datediff(y,to_date(dc.dob,'YYYYMMDD'),sysdate)<21 between 31 and 40 then '31-40' 
			else '>40' end	as age_bucket,
count(distinct dc.id) as cust
--dc.customer_login,

from fact_orderitem foi,
dim_customer dc,
dim_product dp
where foi.idcustomer=dc.id and foi.sku_id=dp.sku_id and foi.is_realised=1 and dc.dob>19700101 and dc.gender in ('M','F') and dp.article_type in ('Formal Shoes','Casual Shoes')
and dp.brand in 
('United Colors of Benetton',
'Knotty Derby',
'Lee Cooper',
'Sparx',
'Woodland',
'GAS',
'Alberto Torresi',
'DIESEL',
'Numero Uno',
'Allen Solly',
'Duke',
'Levis',
'Buckaroo',
'Superdry',
'Louis Philippe',
'Ruosh',
'Clarks',
'Franco Leone',
'Carlton London',
'San Frissco',
'Hush Puppies',
'Red Tape',
'Bata')
group by 1,2,3,4
limit 1000

