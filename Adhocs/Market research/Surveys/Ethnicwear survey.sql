select distinct
dc.customer_login,
dc.default_mobile
--datediff(y,to_date(case when dc.dob is NULL or dc.dob<19700101 then dc.fb_birthday else dc.dob end,'YYYYMMDD'),sysdate) as age_cus
from fact_orderitem fo
join dim_location dl
on fo.idlocation=dl.id
join dim_customer dc
on fo.idcustomer=dc.id
join dim_product dp
on fo.sku_id=dp.sku_id
where order_created_date>=(to_char(sysdate - interval '6 months','YYYYMMDD')) 
and dp.business_unit='Men\'s Ethnic'
--and (dc.dob>19700101 or dc.fb_birthday>19700101)
and (dc.gender='M' or dc.fb_gender='M')
