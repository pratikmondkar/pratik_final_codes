SELECT a.*
	,nvl(total_q1_inv_value, 0) AS total_inventory_value
	,brand_level_45days_item_purc_sold
	,CASE 
		WHEN nvl(total_q1_inv_value, 0) > 100000
			THEN round(total_q1_inv_value::DECIMAL(12, 2) / brand_level_45days_item_purc_sold::DECIMAL(12, 2) * 45)
		ELSE NULL
		END AS brand_doh
	,brand_first_catalogued_date
	,GM AS GM_last_60_days

FROM (
	SELECT fpo.barcode
		,max(fpp.brand) AS brand
		,max(po_type) as category
		,max(fpp.category_manager) AS category_manager
		,max(created_date) AS created_date
		,count(fpo.sku_id) AS total_styles
		,sum(fpo.quantity * fpo.item_purchase_price_inc_tax) AS po_value
		,max(warehouse_id) as warehouse_id

		
	FROM fact_purchase_order fpo
		,fact_product_profile fpp
		,dim_product dp
	WHERE fpp.report_date = to_char(sysdate - interval '1 days', 'YYYYMMDD') 
		AND fpo.sku_id = fpp.sku_id
		AND dp.sku_id=fpo.sku_id
	AND UPPER(fpo.po_status) in ('CREATED', 'PENDING', 'READY')
		AND fpo.is_po_jit = 0
		and UPPER(po_type) in ('OUTRIGHT','SOR') and upper(dp.brand_type) in ('EXTERNAL') and created_date >= to_char(sysdate - interval '45 days', 'YYYYMMDD')
	GROUP BY 1
	) a
LEFT JOIN (
		SELECT brand
			,(sum(item_revenue_inc_cashback) - sum(tax) - sum(cogs) - sum(royalty_commission)) / (sum(item_revenue_inc_cashback) - sum(tax)) AS GM
			,min(dp.style_catalogued_date) as brand_first_catalogued_date
			,sum(item_purchase_price_inc_tax) as brand_level_45days_item_purc_sold
		FROM fact_orderitem fo
			,dim_product dp
		WHERE fo.sku_id = dp.sku_id
			AND is_realised = 1
			AND order_created_date >= to_char(sysdate - interval '45 days', 'YYYYMMDD')
			and item_revenue_inc_cashback > 0
		GROUP BY 1
	) c ON a.brand = c.brand
	
LEFT JOIN (

		SELECT fpp.brand
			,round(sum(total_q1_inv_value)) AS total_q1_inv_value
		FROM fact_product_profile fpp
			,dim_product dp
		WHERE fpp.sku_id = dp.sku_id
			AND report_date = to_char(sysdate - interval '1 days', 'YYYYMMDD')
			AND lower(commercial_type) IN (
				'outright','sor'
				) and upper(dp.brand_type) in ('EXTERNAL')
		GROUP BY 1



) d on a.brand=d.brand
ORDER BY brand_doh desc