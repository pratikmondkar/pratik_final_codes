SELECT *
FROM (
	SELECT fpo.barcode
		,fpp.sku_id
		,fpp.style_id
		,max(fpp.brand) AS brand
		,max(fpp.on_trade_days_since_go_live) as on_trade_days_since_go_live
		,max(cg.last_inward_date) AS last_inward_date
		,max(on_trade_days) AS on_trade_days
		,sum(price_last_act_30days) as item_purchase_price_inc_tax_last_act_30days

	FROM fact_purchase_order fpo
	JOIN fact_product_profile fpp ON fpo.sku_id = fpp.sku_id
	LEFT JOIN (
		SELECT dp.style_id
			,dp.sku_id
			,max(last_inward_date) AS last_inward_date
			,sum(nvl(item_purchase_price_inc_tax, 0)) AS price_last_act_30days
			,max(nvl(on_trade_days, 0)) AS on_trade_days
			,sum(foi.item_revenue_inc_cashback) as item_revenue_inc_cashback_last_30_days
			,sum(foi.tax) as tax_last_30_days
			,sum(foi.cogs) as cogs_last_30_days
		FROM 
		dim_product dp 
		LEFT JOIN fact_orderitem foi ON dp.sku_id=foi.sku_id 
			AND foi.order_created_date BETWEEN dp.last_inward_date and to_char(to_date(dp.last_inward_date, 'YYYYMMDD') + interval '45 Days', 'YYYYMMDD')
				 
		LEFT JOIN (
			SELECT sku_id
				,count(DISTINCT DATE) AS on_trade_days
			FROM fact_product_snapshot fp
			WHERE is_live_on_portal = 1
				AND DATE BETWEEN 
					(
							SELECT last_inward_date
							FROM dim_product dp2
							WHERE fp.sku_id = dp2.sku_id
							)
							AND
						to_char(to_date((
									SELECT last_inward_date
									FROM dim_product dp2
									WHERE  fp.sku_id = dp2.sku_id
									), 'YYYYMMDD') + interval '30 Days', 'YYYYMMDD')
			GROUP BY 1
			) otd ON dp.sku_id = otd.sku_id
		GROUP BY 1,2
		) cg ON fpp.sku_id = cg.sku_id
	JOIN dim_product dp ON fpo.sku_id = dp.sku_id
	WHERE fpp.report_date = to_char(sysdate - interval '1 days', 'YYYYMMDD')
	AND UPPER(fpo.po_status) in ('APPROVED')
	and fpo.approval_date >= 20150715
		AND fpo.is_po_jit = 0
		and UPPER(po_type) in ('OUTRIGHT') and upper(dp.brand_type) in ('EXTERNAL') 
	GROUP BY 1
		,2
		,3
	)
WHERE  on_trade_days_since_go_live >= 30
