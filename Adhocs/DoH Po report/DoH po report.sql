Select a.*,
				brand_first_catalogued_date,
				nvl(total_live_inventory_value,0) as total_inventory_value,
				nvl(repeat_styles,0)::decimal(10,2)/a.total_styles*100 as repeat_styles_percent,
				nvl(repeat_styles_value,0)/NULLIF(a.po_value,0)*100 as repeat_styles_value_precent,
				nvl(repeat_styles_value,0) as repeat_styles_value,
				no_sales_last_60_days_value,
				case when nvl(total_live_inventory_value,0)>100000 then round(total_live_inventory_value::decimal(12,2)/last_30_days_price::decimal(12,2)*30) else NULL end as brand_doh,
				case when nvl(total_live_inventory_value,0)>100000 then round((total_live_inventory_value+po_value)::decimal(12,2)/last_30_days_price::decimal(12,2)*30) else NULL end as projected_brand_doh,
				doh_gt60_value,
				GM*100 as GM_last_60_days
from 
(Select 
		fpo.barcode,
		max(fpp.category_manager) as category_manager,
		count(distinct fpp.category_manager) as cm_cnt,
		max(po_type) as po_type,
		max(fpp.brand) as brand,
		max(fpo.vendor_name) as vendor,
		max(fpo.season_id) as season,
		max(fpo.season_year) as season_year,
		max(created_date) as created_date,
		sum(fpo.quantity) as quantity,
		sum(fpo.quantity*fpo.item_purchase_price_inc_tax) as po_value,
		count(distinct fpp.style_id) as total_styles
from 
		fact_purchase_order fpo,
		fact_product_profile fpp	
where 
		fpp.report_date=to_char(sysdate,'YYYYMMDD') 
		and fpo.sku_id=fpp.sku_id 
		and fpo.po_status in ('CREATED','PENDING') 
		and fpo.is_po_jit =0
		and fpo.created_date>to_char(sysdate-interval '3 months','YYYYMMDD')
group by 1) a
left join
(Select barcode,
				sum(case when on_trade_days_since_go_live>30 then style_value else 0 end) as repeat_styles_value,
				sum(case when style_doh>60 and on_trade_days_since_go_live>30 then style_value else 0 end) as doh_gt60_value,
				sum(case when last_60_days_sales=0 and on_trade_days_since_go_live>30 then style_value else 0 end) as no_sales_last_60_days_value,
				sum(case when on_trade_days_since_go_live>30 then 1 else 0 end) as repeat_styles
from
			(Select 
					fpo.barcode,
					fpp.style_id,
					sum(fpo.quantity*fpo.item_purchase_price_inc_tax) as style_value,
					sum(nvl(last_60_days_sales,0)) as last_60_days_sales,
					sum(nvl(till_date_sales,0)) as till_date_sales,
					sum(total_live_inventory) as total_live_inventory,
					round((sum(total_live_inventory_value)::decimal(10,2)/NULLIF(sum(price_last_act_30days),0)::decimal(10,2))*max(on_trade_days)::decimal(10,2)) as style_doh,
					max(on_trade_days_since_go_live) as on_trade_days_since_go_live
			from 
					fact_purchase_order fpo
					join fact_product_profile fpp	
					on fpo.sku_id=fpp.sku_id 
					left join 
							(select dp.style_id,max(st_date) as st_date,sum(nvl(item_purchase_price_inc_tax,0)) as price_last_act_30days,max(nvl(on_trade_days,0)) as on_trade_days
							from
												(select style_id,max(date) as st_date from fact_product_snapshot where is_live_on_portal=1 group by 1) fps
												left join dim_product dp 
												on dp.style_id=fps.style_id
												left join fact_orderitem foi
												on foi.sku_id=dp.sku_id and foi.order_created_date between to_char(to_date(st_date,'YYYYMMDD')-interval '30 Days','YYYYMMDD') and st_date		
												left join 
																			(select style_id,count(distinct date) as on_trade_days from fact_product_snapshot fp where is_live_on_portal=1 and
												date between to_char(to_date((select max(date) from fact_product_snapshot fp2 
																											where is_live_on_portal=1 and fp.style_id=fp2.style_id ),
																											'YYYYMMDD')-interval '29 Days','YYYYMMDD') 
												and (select max(date) from fact_product_snapshot fp2 
																											where is_live_on_portal=1 and fp.style_id=fp2.style_id) group by 1)  otd
												on fps.style_id=otd.style_id				
					group by 1) cg 
					on fpp.style_id=cg.style_id
			where 
					fpp.report_date=to_char(sysdate,'YYYYMMDD') 
					and fpo.po_status in ('CREATED','PENDING') 
					and fpo.is_po_jit = 0
					and fpo.created_date>to_char(sysdate-interval '3 months','YYYYMMDD')
			group by 1,2)
group by 1) d
on a.barcode=d.barcode
left join 
(Select a.brand,a.total_live_inventory_value,b.last_60_days_price,b.gm,b.last_30_days_price,brand_first_catalogued_date
from
(select 
		fpp.brand,
		round(sum(total_live_inventory_value)) as total_live_inventory_value,
		min(dp.style_catalogued_date) as brand_first_catalogued_date
from 
		fact_product_profile fpp,
		dim_product dp 
where fpp.sku_id=dp.sku_id and report_date=to_char(sysdate,'YYYYMMDD') and lower(commercial_type) in ('outright','sor')  
group by 1
having sum(total_live_inventory_value)>0  
order by 1) a
left join
		(select brand,
						sum(item_purchase_price_inc_tax) as last_60_days_price,
						sum(case when order_created_date>=to_char(sysdate-interval '30 days','YYYYMMDD') then item_purchase_price_inc_tax else 0 end) as last_30_days_price,
						round(sum(item_revenue_inc_cashback)) as last_60_days_rev,
						(sum(item_revenue_inc_cashback)-sum(tax)-sum(cogs)-sum(royalty_commission))/(sum(item_revenue_inc_cashback)-sum(tax)) as GM
			from fact_orderitem fo,dim_product dp
			where fo.sku_id=dp.sku_id and is_realised=1 and order_created_date>=to_char(sysdate-interval '60 days','YYYYMMDD')
			group by 1) b
on a.brand=b.brand
where last_60_days_rev>0) c
on a.brand=c.brand
order by 1;







Select * from 
(Select 
		fpo.barcode,
		fpp.style_id,
		sum(total_live_inventory_value) as total_live_inventory_value,
		sum(total_live_inventory) as total_live_inventory,
		sum(total_q1_inv_qty) as total_q1_inv_qty,
		max(st_date) as st_date,
		min(dp.style_created_date) as style_created_date,
		max(fpp.category_manager) as category_manager,
		max(fpp.brand) as brand,
		max(fpo.season_id) as season,
		max(fpo.season_year) as season_year,
		max(fpo.vendor_name) as vendor,
		sum(fpo.quantity) as quantity,
		max(fpp.on_trade_days_since_go_live) as on_trade_days_since_go_live,
		sum(fpo.quantity*fpo.item_purchase_price_inc_tax) as repeat_styles_value,
		sum(nvl(last_60_days_sales,0)) as last_60_days_sales,
		max(on_trade_days) as on_trade_days_last_active_30days,
		round((sum(total_live_inventory_value)::decimal(10,2)/NULLIF(sum(price_last_act_30days),0)::decimal(10,2))*max(on_trade_days)::decimal(10,2)) as rep_style_doH_30_current,
		round((sum(total_live_inventory_value+fpo.quantity*fpo.item_purchase_price_inc_tax)::decimal(10,2)/NULLIF(sum(price_last_act_30days),0)::decimal(10,2))*max(on_trade_days)::decimal(10,2)) as rep_style_doH_30_projected
from 
		fact_purchase_order fpo
		join fact_product_profile fpp on fpo.sku_id=fpp.sku_id
		left join 
		(select dp.style_id,max(st_date) as st_date,sum(nvl(item_purchase_price_inc_tax,0)) as price_last_act_30days,max(nvl(on_trade_days,0)) as on_trade_days
		from 					(select style_id,max(date) as st_date from fact_product_snapshot where is_live_on_portal=1 group by 1) fps
									left join dim_product dp 
									on dp.style_id=fps.style_id
									left join fact_orderitem foi
									on foi.sku_id=dp.sku_id and foi.order_created_date between to_char(to_date(st_date,'YYYYMMDD')-interval '30 Days','YYYYMMDD') and st_date		
									left join 
																(select style_id,count(distinct date) as on_trade_days from fact_product_snapshot fp where is_live_on_portal=1 and
									date between to_char(to_date((select max(date) from fact_product_snapshot fp2 
																								where is_live_on_portal=1 and fp.style_id=fp2.style_id ),
																								'YYYYMMDD')-interval '29 Days','YYYYMMDD') 
									and (select max(date) from fact_product_snapshot fp2 
																								where is_live_on_portal=1 and fp.style_id=fp2.style_id) group by 1)  otd
									on fps.style_id=otd.style_id				
		group by 1) cg on fpp.style_id=cg.style_id
		join dim_product dp	on fpo.sku_id=dp.sku_id 
		left join (select style_id,max(date) as st_date2 from fact_product_snapshot where is_live_on_portal=1 group by 1) fps2
		on dp.style_id=fps2.style_id
where 
		fpp.report_date=to_char(sysdate,'YYYYMMDD') 
		and fpo.po_status in ('CREATED','PENDING') 
		and fpo.is_po_jit = 0
		and barcode='RLBL220515-01'
		and fpo.created_date>to_char(sysdate-interval '3 months','YYYYMMDD')
group by 1,2)
where on_trade_days_since_go_live>30;

