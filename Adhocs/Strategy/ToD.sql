select a.*, b.orders_with_multiple_items_of_same_article_type_all, b.orders_with_multiple_items_of_same_article_type_new, b.orders_with_multiple_items_of_same_article_type_repeat, c.orders_with_multiple_sizes_of_item_all, c.orders_with_multiple_sizes_of_item_new, c.orders_with_multiple_sizes_of_item_repeat
from
-- new+repeat:
(SELECT city_group,       
SUM(case when purchase_sequence >= 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS revenue_all,
COUNT(DISTINCT (case when purchase_sequence >= 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD')  then oi.idcustomer end)) AS customers_all,
COUNT(DISTINCT (case when purchase_sequence >= 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_group_id end)) AS orders_all,
COUNT(DISTINCT (case when purchase_sequence >= 1 and payment_method ='cod' and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_group_id end)) AS cod_orders_all,
COUNT(DISTINCT (case when purchase_sequence >= 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_id end)) AS shipments_all,
SUM(case when purchase_sequence >= 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.quantity end) AS item_count_all,       
COUNT(DISTINCT (case when purchase_sequence >= 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_id end)) AS shipments_delivered_all,
COUNT(DISTINCT (case when purchase_sequence >= 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.item_id end)) AS items_delivered_all,
SUM (CASE when purchase_sequence >= 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS items_delivered_rev_all,
SUM(CASE WHEN purchase_sequence >= 1 and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS returned_revenue_all,    -- is_refunded when money is refunded and not necessarility restocked in warehouse
COUNT(DISTINCT (CASE WHEN purchase_sequence >= 1 and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS retuned_units_all,
SUM(CASE WHEN purchase_sequence >= 1 and oi.order_status='RTO' AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS rto_revenue_all,
COUNT(DISTINCT (CASE WHEN purchase_sequence >= 1 and oi.order_status='RTO'  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS rto_units_all,
SUM(CASE WHEN purchase_sequence >= 1 and oi.is_returned = 1 and qa_fail_date >19700101  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS Q2_Q3_revenue_all,    -- is_returned = 1 for restocked and   qa_fail_date is not null for (no Q1) items
COUNT(DISTINCT (CASE WHEN purchase_sequence >= 1 and oi.is_returned = 1 and qa_fail_date >19700101  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS Q2_Q3_units_all,
COUNT(DISTINCT (CASE WHEN purchase_sequence >= 1 and frr.return_mode = 'TRY_AND_BUY' and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN frr.core_item_id END)) as doorstep_return_units_all,       
SUM(CASE WHEN purchase_sequence >= 1 and frr.return_mode = 'TRY_AND_BUY' and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) as doorstep_return_gmv_all,
count(distinct (case when purchase_sequence >= 1 and status in ('TRIED_AND_NOT_BOUGHT', 'TRIED_AND_BOUGHT') and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then actual_order_id end)) as actual_tried_shipments_all,


SUM(case when purchase_sequence = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS revenue_new,
COUNT(DISTINCT (case when purchase_sequence = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD')  then oi.idcustomer end)) AS customers_new,
COUNT(DISTINCT (case when purchase_sequence = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_group_id end)) AS orders_new,
COUNT(DISTINCT (case when purchase_sequence = 1 and payment_method ='cod' and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_group_id end)) AS cod_orders_new,
COUNT(DISTINCT (case when purchase_sequence = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_id end)) AS shipments_new,
SUM(case when purchase_sequence = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.quantity end) AS item_count_new,       
COUNT(DISTINCT (case when purchase_sequence = 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_id end)) AS shipments_delivered_new,
COUNT(DISTINCT (case when purchase_sequence = 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.item_id end)) AS items_delivered_new,
SUM (CASE when purchase_sequence = 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS items_delivered_rev_new,
SUM(CASE WHEN purchase_sequence = 1 and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS returned_revenue_new,    -- is_refunded when money is refunded and not necessarility restocked in warehouse
COUNT(DISTINCT (CASE WHEN purchase_sequence = 1 and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS retuned_units_new,
SUM(CASE WHEN purchase_sequence = 1 and oi.order_status='RTO' AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS rto_revenue_new,
COUNT(DISTINCT (CASE WHEN purchase_sequence = 1 and oi.order_status='RTO'  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS rto_units_new,
SUM(CASE WHEN purchase_sequence = 1 and oi.is_returned = 1 and qa_fail_date >19700101  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS Q2_Q3_revenue_new,    -- is_returned = 1 for restocked and   qa_fail_date is not null for (no Q1) items
COUNT(DISTINCT (CASE WHEN purchase_sequence = 1 and oi.is_returned = 1 and qa_fail_date >19700101  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS Q2_Q3_units_new,
COUNT(DISTINCT (CASE WHEN purchase_sequence = 1 and frr.return_mode = 'TRY_AND_BUY' and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN frr.core_item_id END)) as doorstep_return_units_new,       
SUM(CASE WHEN purchase_sequence = 1 and frr.return_mode = 'TRY_AND_BUY' and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) as doorstep_return_gmv_new,
count(distinct (case when purchase_sequence = 1 and status in ('TRIED_AND_NOT_BOUGHT', 'TRIED_AND_BOUGHT') and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then actual_order_id end)) as actual_tried_shipments_new ,

SUM(case when purchase_sequence > 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS revenue_repeat,
COUNT(DISTINCT (case when purchase_sequence > 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD')  then oi.idcustomer end)) AS customers_repeat,
COUNT(DISTINCT (case when purchase_sequence > 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_group_id end)) AS orders_repeat,
COUNT(DISTINCT (case when purchase_sequence > 1 and payment_method ='cod' and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_group_id end)) AS cod_orders_repeat,
COUNT(DISTINCT (case when purchase_sequence > 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_id end)) AS shipments_repeat,
SUM(case when purchase_sequence > 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.quantity end) AS item_count_repeat,       
COUNT(DISTINCT (case when purchase_sequence > 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_id end)) AS shipments_delivered_repeat,
COUNT(DISTINCT (case when purchase_sequence > 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.item_id end)) AS items_delivered_repeat,
SUM (CASE when purchase_sequence > 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS items_delivered_rev_repeat,
SUM(CASE WHEN purchase_sequence > 1 and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS returned_revenue_repeat,    -- is_refunded when money is refunded and not necessarility restocked in warehouse
COUNT(DISTINCT (CASE WHEN purchase_sequence > 1 and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS retuned_units_repeat,
SUM(CASE WHEN purchase_sequence > 1 and oi.order_status='RTO' AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS rto_revenue_repeat,
COUNT(DISTINCT (CASE WHEN purchase_sequence > 1 and oi.order_status='RTO'  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS rto_units_repeat,
SUM(CASE WHEN purchase_sequence > 1 and oi.is_returned = 1 and qa_fail_date >19700101  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS Q2_Q3_revenue_repeat,    -- is_returned = 1 for restocked and   qa_fail_date is not null for (no Q1) items
COUNT(DISTINCT (CASE WHEN purchase_sequence > 1 and oi.is_returned = 1 and qa_fail_date >19700101  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS Q2_Q3_units_repeat,
COUNT(DISTINCT (CASE WHEN purchase_sequence > 1 and frr.return_mode = 'TRY_AND_BUY' and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN frr.core_item_id END)) as doorstep_return_units_repeat,       
SUM(CASE WHEN purchase_sequence > 1 and frr.return_mode = 'TRY_AND_BUY' and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) as doorstep_return_gmv_repeat,
count(distinct (case when purchase_sequence > 1 and status in ('TRIED_AND_NOT_BOUGHT', 'TRIED_AND_BOUGHT') and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then actual_order_id end)) as actual_tried_shipments_repeat


FROM bidb.fact_core_item oi
  left JOIN bidb.fact_order o ON o.order_id = oi.order_id
  left JOIN bidb.dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN bidb.dim_location l ON oi.idlocation = l.id
  lEFT JOIN bidb.fact_returns frr 
       ON FRR.ORDER_ID = oi.ORDER_ID
       AND frr.item_id = oi.item_id
       AND frr.core_item_id = oi.core_item_id
       AND frr.return_status NOT IN ('CFDC','RJUP','RNC','RQCF','RQCP','RQF','RQSF','RRD','RRRS','RRS','RSD')      
left join bidb.fact_try_and_buy_shipment tbs  on oi.order_id = tbs.actual_order_id
left join bidb.fact_try_and_buy_shipment_item tbsi on tbs.order_to_ship_id = tbsi.order_to_ship_id and oi.core_item_id = tbsi.item_barcode

WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
--AND   purchase_sequence >= 1
AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
--AND   oi.core_item_id NOT LIKE '%,%'
group by 1
) a 

left join 

(
select city_group, 
count(distinct (case when purchase_sequence >=1 and diff_items >1 then order_group_id end)) as orders_with_multiple_items_of_same_article_type_all,
count(distinct (case when purchase_sequence =1 and diff_items >1 then order_group_id end)) as orders_with_multiple_items_of_same_article_type_new,
count(distinct (case when purchase_sequence >1 and diff_items >1 then order_group_id end)) as orders_with_multiple_items_of_same_article_type_repeat
from (
select city_group, purchase_sequence, oi.order_group_id, oi.article_type_id, count(distinct oi.style_id) as diff_items
from bidb.fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
where oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
--AND   purchase_sequence >= 1
AND   dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD')
AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
--AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3, 4
) o
group by 1 
) b
on a.city_group = b.city_group 



left join 

(select city_group,
count(distinct (case when purchase_sequence >= 1 and count_sku > 1 then order_group_id end)) as orders_with_multiple_sizes_of_item_all,
count(distinct (case when purchase_sequence = 1 and count_sku > 1 then order_group_id end)) as orders_with_multiple_sizes_of_item_new,
count(distinct (case when purchase_sequence > 1 and count_sku > 1 then order_group_id end)) as orders_with_multiple_sizes_of_item_repeat
from (
SELECT city_group, purchase_sequence, 
oi.order_group_id, oi.style_id, 
count(distinct oi.sku_id) as count_sku
FROM fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
--AND   purchase_sequence >= 1
AND   dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD')
AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
--AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3,4
) o
group by 1
) c
on a.city_group = b.city_group 






union all



select a.*, b.orders_with_multiple_items_of_same_article_type_all, b.orders_with_multiple_items_of_same_article_type_new, b.orders_with_multiple_items_of_same_article_type_repeat, c.orders_with_multiple_sizes_of_item_all, c.orders_with_multiple_sizes_of_item_new, c.orders_with_multiple_sizes_of_item_repeat
from
-- new+repeat:
(SELECT 'overall' as city_group,       
SUM(case when purchase_sequence >= 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS revenue_all,
COUNT(DISTINCT (case when purchase_sequence >= 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD')  then oi.idcustomer end)) AS customers_all,
COUNT(DISTINCT (case when purchase_sequence >= 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_group_id end)) AS orders_all,
COUNT(DISTINCT (case when purchase_sequence >= 1 and payment_method = 'cod' and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_group_id end)) AS cod_orders_all,
COUNT(DISTINCT (case when purchase_sequence >= 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_id end)) AS shipments_all,
SUM(case when purchase_sequence >= 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.quantity end) AS item_count_all,       
COUNT(DISTINCT (case when purchase_sequence >= 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_id end)) AS shipments_delivered_all,
COUNT(DISTINCT (case when purchase_sequence >= 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.item_id end)) AS items_delivered_all,
SUM (CASE when purchase_sequence >= 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS items_delivered_rev_all,
SUM(CASE WHEN purchase_sequence >= 1 and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS returned_revenue_all,    -- is_refunded when money is refunded and not necessarility restocked in warehouse
COUNT(DISTINCT (CASE WHEN purchase_sequence >= 1 and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS retuned_units_all,
SUM(CASE WHEN purchase_sequence >= 1 and oi.order_status='RTO' AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS rto_revenue_all,
COUNT(DISTINCT (CASE WHEN purchase_sequence >= 1 and oi.order_status='RTO'  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS rto_units_all,
SUM(CASE WHEN purchase_sequence >= 1 and oi.is_returned = 1 and qa_fail_date >19700101  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS Q2_Q3_revenue_all,    -- is_returned = 1 for restocked and   qa_fail_date is not null for (no Q1) items
COUNT(DISTINCT (CASE WHEN purchase_sequence >= 1 and oi.is_returned = 1 and qa_fail_date >19700101  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS Q2_Q3_units_all,
COUNT(DISTINCT (CASE WHEN purchase_sequence >= 1 and frr.return_mode = 'TRY_AND_BUY' and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN frr.core_item_id END)) as doorstep_return_units_all,       
SUM(CASE WHEN purchase_sequence >= 1 and frr.return_mode = 'TRY_AND_BUY' and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) as doorstep_return_gmv_all,
count(distinct (case when purchase_sequence >= 1 and status in ('TRIED_AND_NOT_BOUGHT', 'TRIED_AND_BOUGHT') and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then actual_order_id end)) as actual_tried_shipments_all ,


SUM(case when purchase_sequence = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS revenue_new,
COUNT(DISTINCT (case when purchase_sequence = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD')  then oi.idcustomer end)) AS customers_new,
COUNT(DISTINCT (case when purchase_sequence = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_group_id end)) AS orders_new,
COUNT(DISTINCT (case when purchase_sequence = 1 and payment_method = 'cod' and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_group_id end)) AS cod_orders_new,
COUNT(DISTINCT (case when purchase_sequence = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_id end)) AS shipments_new,
SUM(case when purchase_sequence = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.quantity end) AS item_count_new,       
COUNT(DISTINCT (case when purchase_sequence = 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_id end)) AS shipments_delivered_new,
COUNT(DISTINCT (case when purchase_sequence = 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.item_id end)) AS items_delivered_new,
SUM (CASE when purchase_sequence = 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS items_delivered_rev_new,
SUM(CASE WHEN purchase_sequence = 1 and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS returned_revenue_new,    -- is_refunded when money is refunded and not necessarility restocked in warehouse
COUNT(DISTINCT (CASE WHEN purchase_sequence = 1 and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS retuned_units_new,
SUM(CASE WHEN purchase_sequence = 1 and oi.order_status='RTO' AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS rto_revenue_new,
COUNT(DISTINCT (CASE WHEN purchase_sequence = 1 and oi.order_status='RTO'  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS rto_units_new,
SUM(CASE WHEN purchase_sequence = 1 and oi.is_returned = 1 and qa_fail_date >19700101  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS Q2_Q3_revenue_new,    -- is_returned = 1 for restocked and   qa_fail_date is not null for (no Q1) items
COUNT(DISTINCT (CASE WHEN purchase_sequence = 1 and oi.is_returned = 1 and qa_fail_date >19700101  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS Q2_Q3_units_new,
COUNT(DISTINCT (CASE WHEN purchase_sequence = 1 and frr.return_mode = 'TRY_AND_BUY' and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN frr.core_item_id END)) as doorstep_return_units_new,       
SUM(CASE WHEN purchase_sequence = 1 and frr.return_mode = 'TRY_AND_BUY' and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) as doorstep_return_gmv_new,
count(distinct (case when purchase_sequence = 1 and status in ('TRIED_AND_NOT_BOUGHT', 'TRIED_AND_BOUGHT') and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then actual_order_id end)) as actual_tried_shipments_new ,

SUM(case when purchase_sequence > 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS revenue_repeat,
COUNT(DISTINCT (case when purchase_sequence > 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD')  then oi.idcustomer end)) AS customers_repeat,
COUNT(DISTINCT (case when purchase_sequence > 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_group_id end)) AS orders_repeat,
COUNT(DISTINCT (case when purchase_sequence > 1 and payment_method = 'cod' and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_group_id end)) AS cod_orders_repeat,
COUNT(DISTINCT (case when purchase_sequence > 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_id end)) AS shipments_repeat,
SUM(case when purchase_sequence > 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.quantity end) AS item_count_repeat,       
COUNT(DISTINCT (case when purchase_sequence > 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.order_id end)) AS shipments_delivered_repeat,
COUNT(DISTINCT (case when purchase_sequence > 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then oi.item_id end)) AS items_delivered_repeat,
SUM (CASE when purchase_sequence > 1 and oi.is_delivered = 1 AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS items_delivered_rev_repeat,
SUM(CASE WHEN purchase_sequence > 1 and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS returned_revenue_repeat,    -- is_refunded when money is refunded and not necessarility restocked in warehouse
COUNT(DISTINCT (CASE WHEN purchase_sequence > 1 and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS retuned_units_repeat,
SUM(CASE WHEN purchase_sequence > 1 and oi.order_status='RTO' AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS rto_revenue_repeat,
COUNT(DISTINCT (CASE WHEN purchase_sequence > 1 and oi.order_status='RTO'  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS rto_units_repeat,
SUM(CASE WHEN purchase_sequence > 1 and oi.is_returned = 1 and qa_fail_date >19700101  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) AS Q2_Q3_revenue_repeat,    -- is_returned = 1 for restocked and   qa_fail_date is not null for (no Q1) items
COUNT(DISTINCT (CASE WHEN purchase_sequence > 1 and oi.is_returned = 1 and qa_fail_date >19700101  AND dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN oi.core_item_id END)) AS Q2_Q3_units_repeat,
COUNT(DISTINCT (CASE WHEN purchase_sequence > 1 and frr.return_mode = 'TRY_AND_BUY' and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN frr.core_item_id END)) as doorstep_return_units_repeat,       
SUM(CASE WHEN purchase_sequence > 1 and frr.return_mode = 'TRY_AND_BUY' and frr.is_refunded = 1 and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') THEN nvl (item_revenue_inc_cashback,0) END) as doorstep_return_gmv_repeat,
count(distinct (case when purchase_sequence > 1 and status in ('TRIED_AND_NOT_BOUGHT', 'TRIED_AND_BOUGHT') and dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD') then actual_order_id end)) as actual_tried_shipments_repeat


FROM bidb.fact_core_item oi
  left JOIN bidb.fact_order o ON o.order_id = oi.order_id
  left JOIN bidb.dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN bidb.dim_location l ON oi.idlocation = l.id
  lEFT JOIN bidb.fact_returns frr 
       ON FRR.ORDER_ID = oi.ORDER_ID
       AND frr.item_id = oi.item_id
       AND frr.core_item_id = oi.core_item_id
       AND frr.return_status NOT IN ('CFDC','RJUP','RNC','RQCF','RQCP','RQF','RQSF','RRD','RRRS','RRS','RSD')      
left join bidb.fact_try_and_buy_shipment tbs  on oi.order_id = tbs.actual_order_id
left join bidb.fact_try_and_buy_shipment_item tbsi on tbs.order_to_ship_id = tbsi.order_to_ship_id and oi.core_item_id = tbsi.item_barcode

WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
--AND   purchase_sequence >= 1
--AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
and city_group is not null
--AND   oi.core_item_id NOT LIKE '%,%'
--group by 1
) a 

left join 

(
select city_group, 
count(distinct (case when purchase_sequence >=1 and diff_items >1 then order_group_id end)) as orders_with_multiple_items_of_same_article_type_all,
count(distinct (case when purchase_sequence =1 and diff_items >1 then order_group_id end)) as orders_with_multiple_items_of_same_article_type_new,
count(distinct (case when purchase_sequence >1 and diff_items >1 then order_group_id end)) as orders_with_multiple_items_of_same_article_type_repeat
from (
select 'overall' as city_group, purchase_sequence, oi.order_group_id, oi.article_type_id, count(distinct oi.style_id) as diff_items
from bidb.fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
where oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
--AND   purchase_sequence >= 1
AND   dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD')
--AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
and city_group is not null
--AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3, 4
) o
group by 1 
) b
on a.city_group = b.city_group 



left join 

(select city_group,
count(distinct (case when purchase_sequence >= 1 and count_sku > 1 then order_group_id end)) as orders_with_multiple_sizes_of_item_all,
count(distinct (case when purchase_sequence = 1 and count_sku > 1 then order_group_id end)) as orders_with_multiple_sizes_of_item_new,
count(distinct (case when purchase_sequence > 1 and count_sku > 1 then order_group_id end)) as orders_with_multiple_sizes_of_item_repeat
from (
SELECT 'overall' as city_group, purchase_sequence, 
oi.order_group_id, oi.style_id, 
count(distinct oi.sku_id) as count_sku
FROM fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
--AND   purchase_sequence >= 1
AND   dt.full_date between 20160604 and TO_CHAR(convert_timezone('Asia/Calcutta',(getdate()-INTERVAL '1 DAYS')),'YYYYMMDD')
--AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
and city_group is not null
--AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3,4
) o
group by 1
) c
on a.city_group = b.city_group 
order by city_group
