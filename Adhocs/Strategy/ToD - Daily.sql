select u.*, v.orders_with_multiple_sizes_of_item, v.new_orders_with_multiple_sizes_of_item, v.repeat_orders_with_multiple_sizes_of_item, x.orders_with_multiple_items_of_same_article_type, x.new_orders_with_multiple_items_of_same_article_type, x.repeat_orders_with_multiple_items_of_same_article_type
from 
(

select * from 
(
select a.*, b.new_revenue, b.new_customers, b.new_orders, b.new_shipments, b.new_item_count, b.new_returned_revenue, b.new_retuned_units, b.new_rto_revenue, b.new_rto_units, b.new_Q2_Q3_revenue, b.new_Q2_Q3_units, b.new_doorstep_units, b.new_doorstep_gmv, b.new_actual_tried_shipments,
 c.repeat_revenue, c.repeat_customers, c.repeat_orders, c.repeat_shipments, c.repeat_item_count, c.repeat_returned_revenue, c.repeat_retuned_units, c.repeat_rto_revenue, c.repeat_rto_units, c.repeat_Q2_Q3_revenue, c.repeat_Q2_Q3_units, c.repeat_doorstep_units, c.repeat_doorstep_gmv, c.repeat_actual_tried_shipments 
from 
-- new+repeat:
(SELECT city_group,       
SUM(case when  dt2.day_diff =1 then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS revenue,
COUNT(DISTINCT (case when dt2.day_diff =1  then oi.idcustomer end)) AS customers,
COUNT(DISTINCT (case when dt2.day_diff =1 then oi.order_group_id end)) AS orders,
COUNT(DISTINCT (case when dt2.day_diff =1 then oi.order_id end)) AS shipments,
SUM(case when dt2.day_diff =1 then oi.quantity end) AS item_count,       
SUM(CASE WHEN frr.is_refunded = 1 and dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS returned_revenue,    -- is_refunded when money is refunded and not necessarility restocked in warehouse
COUNT(DISTINCT (CASE WHEN frr.is_refunded = 1 and dt2.day_diff =1 THEN oi.core_item_id END)) AS retuned_units,
SUM(CASE WHEN oi.order_status='RTO' AND dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS rto_revenue,
COUNT(DISTINCT (CASE WHEN oi.order_status='RTO'  AND dt2.day_diff =1 THEN oi.core_item_id END)) AS rto_units,
SUM(CASE WHEN oi.is_returned = 1 and qa_fail_date >19700101  AND dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS Q2_Q3_revenue,    -- is_returned = 1 for restocked and   qa_fail_date is not null for (no Q1) items
COUNT(DISTINCT (CASE WHEN oi.is_returned = 1 and qa_fail_date >19700101  AND dt2.day_diff =1 THEN oi.core_item_id END)) AS Q2_Q3_units,
COUNT(DISTINCT (CASE WHEN frr.return_mode = 'TRY_AND_BUY' and dt.day_diff =1 THEN frr.core_item_id END)) as doorstep_units,       
SUM(CASE WHEN frr.return_mode = 'TRY_AND_BUY' and dt.day_diff =1 THEN frr.refund_amount END) as doorstep_gmv,
count(distinct (case when status in ('TRIED_AND_NOT_BOUGHT', 'TRIED_AND_BOUGHT') and dt3.day_diff = 1 then actual_order_id end)) as actual_tried_shipments 

FROM fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt2 ON oi.order_created_date = dt2.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
  lEFT JOIN fact_returns frr 
       ON FRR.ORDER_ID = oi.ORDER_ID
       AND frr.item_id = oi.item_id
       AND frr.core_item_id = oi.core_item_id
       AND frr.return_status NOT IN ('CFDC','RJUP','RNC','RQCF','RQCP','RQF','RQSF','RRD','RRRS','RRS','RSD')      
   left JOIN dim_date dt ON frr.refunded_date = dt.full_date
left join bidb.fact_try_and_buy_shipment tbs  on oi.order_id = tbs.actual_order_id
left join bidb.fact_try_and_buy_shipment_item tbsi on tbs.order_to_ship_id = tbsi.order_to_ship_id and oi.core_item_id = tbsi.item_barcode
left JOIN dim_date dt3 ON to_char(DATE(tbsi.created_on),'YYYYMMDD') = dt3.full_date

WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence >= 1
AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1
) a


left join 
-- new
(SELECT city_group,       
SUM(case when  dt2.day_diff =1 then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS new_revenue,
COUNT(DISTINCT (case when dt2.day_diff =1  then oi.idcustomer end)) AS new_customers,
COUNT(DISTINCT (case when dt2.day_diff =1 then oi.order_group_id end)) AS new_orders,
COUNT(DISTINCT (case when dt2.day_diff =1 then oi.order_id end)) AS new_shipments,
SUM(case when dt2.day_diff =1 then oi.quantity end) AS new_item_count,       
SUM(CASE WHEN frr.is_refunded = 1 and dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS new_returned_revenue,    -- is_refunded when money is refunded and not necessarility restocked in warehouse
COUNT(DISTINCT (CASE WHEN frr.is_refunded = 1 and dt2.day_diff =1 THEN oi.core_item_id END)) AS new_retuned_units,
SUM(CASE WHEN oi.order_status='RTO' AND dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS new_rto_revenue,
COUNT(DISTINCT (CASE WHEN oi.order_status='RTO'  AND dt2.day_diff =1 THEN oi.core_item_id END)) AS new_rto_units,
SUM(CASE WHEN oi.is_returned = 1 and qa_fail_date >19700101  AND dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS new_Q2_Q3_revenue,    -- is_returned = 1 for restocked and   qa_fail_date is not null for (no Q1) items
COUNT(DISTINCT (CASE WHEN oi.is_returned = 1 and qa_fail_date >19700101  AND dt2.day_diff =1 THEN oi.core_item_id END)) AS new_Q2_Q3_units,
COUNT(DISTINCT (CASE WHEN frr.return_mode = 'TRY_AND_BUY' and dt.day_diff =1 THEN frr.core_item_id END)) as new_doorstep_units,       
SUM(CASE WHEN frr.return_mode = 'TRY_AND_BUY' and dt.day_diff =1 THEN frr.refund_amount END) as new_doorstep_gmv,
count(distinct (case when status in ('TRIED_AND_NOT_BOUGHT', 'TRIED_AND_BOUGHT') and dt3.day_diff = 1 then actual_order_id end)) as new_actual_tried_shipments 

FROM fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt2 ON oi.order_created_date = dt2.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
  lEFT JOIN fact_returns frr 
       ON FRR.ORDER_ID = oi.ORDER_ID
       AND frr.item_id = oi.item_id
       AND frr.core_item_id = oi.core_item_id
       AND frr.return_status NOT IN ('CFDC','RJUP','RNC','RQCF','RQCP','RQF','RQSF','RRD','RRRS','RRS','RSD')      
   left JOIN dim_date dt ON frr.refunded_date = dt.full_date
left join bidb.fact_try_and_buy_shipment tbs  on oi.order_id = tbs.actual_order_id
left join bidb.fact_try_and_buy_shipment_item tbsi on tbs.order_to_ship_id = tbsi.order_to_ship_id and oi.core_item_id = tbsi.item_barcode
left JOIN dim_date dt3 ON to_char(DATE(tbsi.created_on),'YYYYMMDD') = dt3.full_date

WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence = 1
AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1
) b on a.city_group = b.city_group

left join 

-- repeat
(SELECT city_group,       
SUM(case when  dt2.day_diff =1 then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS repeat_revenue,
COUNT(DISTINCT (case when dt2.day_diff =1  then oi.idcustomer end)) AS repeat_customers,
COUNT(DISTINCT (case when dt2.day_diff =1 then oi.order_group_id end)) AS repeat_orders,
COUNT(DISTINCT (case when dt2.day_diff =1 then oi.order_id end)) AS repeat_shipments,
SUM(case when dt2.day_diff =1 then oi.quantity end) AS repeat_item_count,       
SUM(CASE WHEN frr.is_refunded = 1 and dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS repeat_returned_revenue,    -- is_refunded when money is refunded and not necessarility restocked in warehouse
COUNT(DISTINCT (CASE WHEN frr.is_refunded = 1 and dt2.day_diff =1 THEN oi.core_item_id END)) AS repeat_retuned_units,
SUM(CASE WHEN oi.order_status='RTO' AND dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS repeat_rto_revenue,
COUNT(DISTINCT (CASE WHEN oi.order_status='RTO'  AND dt2.day_diff =1 THEN oi.core_item_id END)) AS repeat_rto_units,
SUM(CASE WHEN oi.is_returned = 1 and qa_fail_date >19700101  AND dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS repeat_Q2_Q3_revenue,    -- is_returned = 1 for restocked and   qa_fail_date is not null for (no Q1) items
COUNT(DISTINCT (CASE WHEN oi.is_returned = 1 and qa_fail_date >19700101  AND dt2.day_diff =1 THEN oi.core_item_id END)) AS repeat_Q2_Q3_units,
COUNT(DISTINCT (CASE WHEN frr.return_mode = 'TRY_AND_BUY' and dt.day_diff =1 THEN frr.core_item_id END)) as repeat_doorstep_units,       
SUM(CASE WHEN frr.return_mode = 'TRY_AND_BUY' and dt.day_diff =1 THEN frr.refund_amount END) as repeat_doorstep_gmv,
count(distinct (case when status in ('TRIED_AND_NOT_BOUGHT', 'TRIED_AND_BOUGHT') and dt3.day_diff = 1 then actual_order_id end)) as repeat_actual_tried_shipments 

FROM fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt2 ON oi.order_created_date = dt2.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
  lEFT JOIN fact_returns frr 
       ON FRR.ORDER_ID = oi.ORDER_ID
       AND frr.item_id = oi.item_id
       AND frr.core_item_id = oi.core_item_id
       AND frr.return_status NOT IN ('CFDC','RJUP','RNC','RQCF','RQCP','RQF','RQSF','RRD','RRRS','RRS','RSD')      
   left JOIN dim_date dt ON frr.refunded_date = dt.full_date
left join bidb.fact_try_and_buy_shipment tbs  on oi.order_id = tbs.actual_order_id
left join bidb.fact_try_and_buy_shipment_item tbsi on tbs.order_to_ship_id = tbsi.order_to_ship_id and oi.core_item_id = tbsi.item_barcode
left JOIN dim_date dt3 ON to_char(DATE(tbsi.created_on),'YYYYMMDD') = dt3.full_date

WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence > 1
AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1
) c on a.city_group = c.city_group


) p 

union 

select * from 
(




select d.*, e.new_revenue, e.new_customers, e.new_orders, e.new_shipments, e.new_item_count, e.new_returned_revenue, e.new_retuned_units, e.new_rto_revenue, e.new_rto_units, e.new_Q2_Q3_revenue, e.new_Q2_Q3_units, e.new_doorstep_units, e.new_doorstep_gmv, e.new_actual_tried_shipments,
 f.repeat_revenue, f.repeat_customers, f.repeat_orders, f.repeat_shipments, f.repeat_item_count, f.repeat_returned_revenue, f.repeat_retuned_units, f.repeat_rto_revenue, f.repeat_rto_units, f.repeat_Q2_Q3_revenue, f.repeat_Q2_Q3_units, f.repeat_doorstep_units, f.repeat_doorstep_gmv, f.repeat_actual_tried_shipments 
from 
-- new+repeat (All Cities): 
(SELECT 'Overall' as city_group,       
SUM(case when  dt2.day_diff =1 then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS revenue,
COUNT(DISTINCT (case when dt2.day_diff =1  then oi.idcustomer end)) AS customers,
COUNT(DISTINCT (case when dt2.day_diff =1 then oi.order_group_id end)) AS orders,
COUNT(DISTINCT (case when dt2.day_diff =1 then oi.order_id end)) AS shipments,
SUM(case when dt2.day_diff =1 then oi.quantity end) AS item_count,       
SUM(CASE WHEN frr.is_refunded = 1 and dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS returned_revenue,    -- is_refunded when money is refunded and not necessarility restocked in warehouse
COUNT(DISTINCT (CASE WHEN frr.is_refunded = 1 and dt2.day_diff =1 THEN oi.core_item_id END)) AS retuned_units,
SUM(CASE WHEN oi.order_status='RTO' AND dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS rto_revenue,
COUNT(DISTINCT (CASE WHEN oi.order_status='RTO'  AND dt2.day_diff =1 THEN oi.core_item_id END)) AS rto_units,
SUM(CASE WHEN oi.is_returned = 1 and qa_fail_date >19700101  AND dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS Q2_Q3_revenue,    -- is_returned = 1 for restocked and   qa_fail_date is not null for (no Q1) items
COUNT(DISTINCT (CASE WHEN oi.is_returned = 1 and qa_fail_date >19700101  AND dt2.day_diff =1 THEN oi.core_item_id END)) AS Q2_Q3_units,
COUNT(DISTINCT (CASE WHEN frr.return_mode = 'TRY_AND_BUY' and dt.day_diff =1 THEN frr.core_item_id END)) as doorstep_units,       
SUM(CASE WHEN frr.return_mode = 'TRY_AND_BUY' and dt.day_diff =1 THEN frr.refund_amount END) as doorstep_gmv,
count(distinct (case when status in ('TRIED_AND_NOT_BOUGHT', 'TRIED_AND_BOUGHT') and dt3.day_diff = 1 then actual_order_id end)) as actual_tried_shipments 

FROM fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt2 ON oi.order_created_date = dt2.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
  lEFT JOIN fact_returns frr 
       ON FRR.ORDER_ID = oi.ORDER_ID
       AND frr.item_id = oi.item_id
       AND frr.core_item_id = oi.core_item_id
       AND frr.return_status NOT IN ('CFDC','RJUP','RNC','RQCF','RQCP','RQF','RQSF','RRD','RRRS','RRS','RSD')      
   left JOIN dim_date dt ON frr.refunded_date = dt.full_date
left join bidb.fact_try_and_buy_shipment tbs  on oi.order_id = tbs.actual_order_id
left join bidb.fact_try_and_buy_shipment_item tbsi on tbs.order_to_ship_id = tbsi.order_to_ship_id and oi.core_item_id = tbsi.item_barcode
left JOIN dim_date dt3 ON to_char(DATE(tbsi.created_on),'YYYYMMDD') = dt3.full_date

WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence >= 1
--AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
--group by 1
) d


left join 
-- new (All Cities):
(SELECT 'Overall' as city_group,       
SUM(case when  dt2.day_diff =1 then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS new_revenue,
COUNT(DISTINCT (case when dt2.day_diff =1  then oi.idcustomer end)) AS new_customers,
COUNT(DISTINCT (case when dt2.day_diff =1 then oi.order_group_id end)) AS new_orders,
COUNT(DISTINCT (case when dt2.day_diff =1 then oi.order_id end)) AS new_shipments,
SUM(case when dt2.day_diff =1 then oi.quantity end) AS new_item_count,       
SUM(CASE WHEN frr.is_refunded = 1 and dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS new_returned_revenue,    -- is_refunded when money is refunded and not necessarility restocked in warehouse
COUNT(DISTINCT (CASE WHEN frr.is_refunded = 1 and dt2.day_diff =1 THEN oi.core_item_id END)) AS new_retuned_units,
SUM(CASE WHEN oi.order_status='RTO' AND dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS new_rto_revenue,
COUNT(DISTINCT (CASE WHEN oi.order_status='RTO'  AND dt2.day_diff =1 THEN oi.core_item_id END)) AS new_rto_units,
SUM(CASE WHEN oi.is_returned = 1 and qa_fail_date >19700101  AND dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS new_Q2_Q3_revenue,    -- is_returned = 1 for restocked and   qa_fail_date is not null for (no Q1) items
COUNT(DISTINCT (CASE WHEN oi.is_returned = 1 and qa_fail_date >19700101  AND dt2.day_diff =1 THEN oi.core_item_id END)) AS new_Q2_Q3_units,
COUNT(DISTINCT (CASE WHEN frr.return_mode = 'TRY_AND_BUY' and dt.day_diff =1 THEN frr.core_item_id END)) as new_doorstep_units,       
SUM(CASE WHEN frr.return_mode = 'TRY_AND_BUY' and dt.day_diff =1 THEN frr.refund_amount END) as new_doorstep_gmv,
count(distinct (case when status in ('TRIED_AND_NOT_BOUGHT', 'TRIED_AND_BOUGHT') and dt3.day_diff = 1 then actual_order_id end)) as new_actual_tried_shipments 

FROM fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt2 ON oi.order_created_date = dt2.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
  lEFT JOIN fact_returns frr 
       ON FRR.ORDER_ID = oi.ORDER_ID
       AND frr.item_id = oi.item_id
       AND frr.core_item_id = oi.core_item_id
       AND frr.return_status NOT IN ('CFDC','RJUP','RNC','RQCF','RQCP','RQF','RQSF','RRD','RRRS','RRS','RSD')      
   left JOIN dim_date dt ON frr.refunded_date = dt.full_date
left join bidb.fact_try_and_buy_shipment tbs  on oi.order_id = tbs.actual_order_id
left join bidb.fact_try_and_buy_shipment_item tbsi on tbs.order_to_ship_id = tbsi.order_to_ship_id and oi.core_item_id = tbsi.item_barcode
left JOIN dim_date dt3 ON to_char(DATE(tbsi.created_on),'YYYYMMDD') = dt3.full_date

WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence = 1
--AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
--group by 1
) e on d.city_group = e.city_group

left join 

-- repeat(All Cities):
(SELECT 'Overall' as city_group,       
SUM(case when  dt2.day_diff =1 then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS repeat_revenue,
COUNT(DISTINCT (case when dt2.day_diff =1  then oi.idcustomer end)) AS repeat_customers,
COUNT(DISTINCT (case when dt2.day_diff =1 then oi.order_group_id end)) AS repeat_orders,
COUNT(DISTINCT (case when dt2.day_diff =1 then oi.order_id end)) AS repeat_shipments,
SUM(case when dt2.day_diff =1 then oi.quantity end) AS repeat_item_count,       
SUM(CASE WHEN frr.is_refunded = 1 and dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS repeat_returned_revenue,    -- is_refunded when money is refunded and not necessarility restocked in warehouse
COUNT(DISTINCT (CASE WHEN frr.is_refunded = 1 and dt2.day_diff =1 THEN oi.core_item_id END)) AS repeat_retuned_units,
SUM(CASE WHEN oi.order_status='RTO' AND dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS repeat_rto_revenue,
COUNT(DISTINCT (CASE WHEN oi.order_status='RTO'  AND dt2.day_diff =1 THEN oi.core_item_id END)) AS repeat_rto_units,
SUM(CASE WHEN oi.is_returned = 1 and qa_fail_date >19700101  AND dt2.day_diff =1 THEN nvl (item_revenue_inc_cashback,0) END) AS repeat_Q2_Q3_revenue,    -- is_returned = 1 for restocked and   qa_fail_date is not null for (no Q1) items
COUNT(DISTINCT (CASE WHEN oi.is_returned = 1 and qa_fail_date >19700101  AND dt2.day_diff =1 THEN oi.core_item_id END)) AS repeat_Q2_Q3_units,
COUNT(DISTINCT (CASE WHEN frr.return_mode = 'TRY_AND_BUY' and dt.day_diff =1 THEN frr.core_item_id END)) as repeat_doorstep_units,       
SUM(CASE WHEN frr.return_mode = 'TRY_AND_BUY' and dt.day_diff =1 THEN frr.refund_amount END) as repeat_doorstep_gmv,
count(distinct (case when status in ('TRIED_AND_NOT_BOUGHT', 'TRIED_AND_BOUGHT') and dt3.day_diff = 1 then actual_order_id end)) as repeat_actual_tried_shipments 

FROM fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt2 ON oi.order_created_date = dt2.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
  lEFT JOIN fact_returns frr 
       ON FRR.ORDER_ID = oi.ORDER_ID
       AND frr.item_id = oi.item_id
       AND frr.core_item_id = oi.core_item_id
       AND frr.return_status NOT IN ('CFDC','RJUP','RNC','RQCF','RQCP','RQF','RQSF','RRD','RRRS','RRS','RSD')      
   left JOIN dim_date dt ON frr.refunded_date = dt.full_date
left join bidb.fact_try_and_buy_shipment tbs  on oi.order_id = tbs.actual_order_id
left join bidb.fact_try_and_buy_shipment_item tbsi on tbs.order_to_ship_id = tbsi.order_to_ship_id and oi.core_item_id = tbsi.item_barcode
left JOIN dim_date dt3 ON to_char(DATE(tbsi.created_on),'YYYYMMDD') = dt3.full_date

WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence > 1
--AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
--group by 1
) f on d.city_group = f.city_group

) q


) u 


left join 





(



-- for orders with multiple_sizes_item

select * from (
select a.*, nvl(b.new_orders_with_multiple_sizes_of_item,0) as new_orders_with_multiple_sizes_of_item, nvl(c.repeat_orders_with_multiple_sizes_of_item,0) as repeat_orders_with_multiple_sizes_of_item
from 
---- new+repeat:

(select city_group,count(distinct (case when count_sku > 1 then order_group_id end)) as orders_with_multiple_sizes_of_item
from (
SELECT city_group,  
oi.order_group_id, oi.item_id, 
count(distinct oi.sku_id) as count_sku
FROM fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence >= 1
AND   dt.day_diff =1
AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3
) o
group by 1
) a

left join 
---- new:
(select city_group,count(distinct (case when count_sku > 1 then order_group_id end)) as new_orders_with_multiple_sizes_of_item
from (
SELECT city_group,  
oi.order_group_id, oi.item_id, 
count(distinct oi.sku_id) as count_sku
FROM fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence = 1
AND   dt.day_diff =1
AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3
) o
group by 1
) b on a.city_group = b.city_group

left join 

---- repeat:
(select city_group,count(distinct (case when count_sku > 1 then order_group_id end)) as repeat_orders_with_multiple_sizes_of_item
from (
SELECT city_group,  
oi.order_group_id, oi.item_id, 
count(distinct oi.sku_id) as count_sku
FROM fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence > 1
AND   dt.day_diff =1
AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3
) o
group by 1
) c on a.city_group = b.city_group

) p 

union 


select * from 
(
select e.*, nvl(f.new_orders_with_multiple_sizes_of_item,0) as new_orders_with_multiple_sizes_of_item_new, nvl(g.repeat_orders_with_multiple_sizes_of_item,0) as repeat_orders_with_multiple_sizes_of_item
from 
---- new+repeat:
(select city_group,count(distinct (case when count_sku > 1 then order_group_id end)) as orders_with_multiple_sizes_of_item
from (
SELECT 'Overall' as city_group,  
oi.order_group_id, oi.item_id, 
count(distinct oi.sku_id) as count_sku
FROM fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence >= 1
AND   dt.day_diff =1
--AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3
) o
group by 1) e

left join 

---- new:
(select city_group,count(distinct (case when count_sku > 1 then order_group_id end)) as new_orders_with_multiple_sizes_of_item
from (
SELECT 'Overall' as city_group,  
oi.order_group_id, oi.item_id, 
count(distinct oi.sku_id) as count_sku
FROM fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence = 1
AND   dt.day_diff =1
--AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3
) o
group by 1) f on e.city_group = f.city_group
left join 

---- repeat:

(select city_group,count(distinct (case when count_sku > 1 then order_group_id end)) as repeat_orders_with_multiple_sizes_of_item
from (
SELECT 'Overall' as city_group,  
oi.order_group_id, oi.item_id, 
count(distinct oi.sku_id) as count_sku
FROM fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
WHERE 
oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence > 1
AND   dt.day_diff =1
--AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3
) o
group by 1) g on e.city_group = g.city_group

) q


) v on u.city_group = v.city_group

left join 

(
select * from (

select a.*, b.new_orders_with_multiple_items_of_same_article_type, c.repeat_orders_with_multiple_items_of_same_article_type
from 
-- new+repeat:
(
select city_group, count(distinct (case when diff_items >1 then order_group_id end)) as orders_with_multiple_items_of_same_article_type
from (
select city_group, oi.order_group_id, oi.article_type_id, count(distinct oi.item_id) as diff_items
from bidb.fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
where oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence >= 1
AND   dt.day_diff =1
AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3
) o
group by 1 
) a

left join 
-- new:
(
select city_group, count(distinct (case when diff_items >1 then order_group_id end)) as new_orders_with_multiple_items_of_same_article_type
from (
select city_group, oi.order_group_id, oi.article_type_id, count(distinct oi.item_id)  as diff_items
from bidb.fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
where oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence = 1
AND   dt.day_diff =1
AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3
) o
group by 1 
) b on a.city_group = b.city_group

left join 
-- repeat:
(
select city_group, count(distinct (case when diff_items >1 then order_group_id end)) as repeat_orders_with_multiple_items_of_same_article_type
from (
select city_group, oi.order_group_id, oi.article_type_id, count(distinct oi.item_id)  as diff_items
from bidb.fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
where oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence > 1
AND   dt.day_diff =1
AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3
) o
group by 1 
) c on a.city_group = c.city_group

) p 


union

select * from (

select d.*, e.new_orders_with_multiple_items_of_same_article_type, f.repeat_orders_with_multiple_items_of_same_article_type
from 
-- new+repeat:
(
select city_group, count(distinct (case when diff_items >1 then order_group_id end)) as orders_with_multiple_items_of_same_article_type
from (
select 'Overall' as city_group, oi.order_group_id, oi.article_type_id, count(distinct oi.item_id) as diff_items
from bidb.fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
where oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence >= 1
AND   dt.day_diff =1
--AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3
having count(distinct item_id) > 1
) o
group by 1 
) d

left join 
-- new:
(
select city_group, count(distinct (case when diff_items >1 then order_group_id end)) as new_orders_with_multiple_items_of_same_article_type
from (
select 'Overall' as city_group, oi.order_group_id, oi.article_type_id, count(distinct oi.item_id) as diff_items
from bidb.fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
where oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence = 1
AND   dt.day_diff =1
--AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3
having count(distinct item_id) > 1
) o
group by 1 
) e on d.city_group = e.city_group

left join 
-- repeat:
(
select city_group, count(distinct (case when diff_items >1 then order_group_id end)) as repeat_orders_with_multiple_items_of_same_article_type
from (
select 'Overall' as city_group, oi.order_group_id, oi.article_type_id, count(distinct oi.item_id) as diff_items
from bidb.fact_core_item oi
  left JOIN fact_order o ON o.order_id = oi.order_id
  left JOIN dim_date dt ON oi.order_created_date = dt.full_date
  left JOIN dim_location l ON oi.idlocation = l.id
where oi.order_created_date >= TO_CHAR(dateadd(month,-2,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
AND   oi.store_id = 1
and		is_try_and_buy=1
AND   purchase_sequence > 1
AND   dt.day_diff =1
--AND   city_group IN ('BANGALORE','DELHI-NCR','MUMBAI','CHENNAI','KOLKATA','HYDERABAD')
AND   oi.core_item_id NOT LIKE '%,%'
group by 1,2,3
having count(distinct item_id) > 1
) o
group by 1 
) f on d.city_group = f.city_group

) q

) x on u.city_group = x.city_group



















order by city_group

;







