--drop table dev.pm_size_chart_temp;
--create table dev.pm_size_chart_temp diststyle even sortkey(bought_flag) as
truncate table dev.pm_size_chart_temp;
insert into dev.pm_size_chart_temp 
(select a.*,nvl(b.size_chart_flag,0) as size_chart_flag,nvl(b.atc_flag,0) as atc_flag, nvl(bought_flag,0) as bought_flag
from
(select load_date,uidx,style_id::bigint 
from clickstream.discount_aggregates
where load_date>=to_char(sysdate- interval '1 day','YYYYMMDD')::bigint and uidx is not null
and style_id SIMILAR TO '[0-9]{6,8}') a
left join
(select load_date,uidx,style_id,
max(case when event_type ='sizingChart' then 1 else 0 end) as size_chart_flag, 
max(case when event_type ='addToCart' then 1 else 0 end) as atc_flag 
from customer_insights.user_style_sizingchart_daily_updated a
where load_date>=to_char(sysdate- interval '1 day','YYYYMMDD')::bigint and event_type in ('sizingChart','addToCart')
group by 1,2,3) b on a.uidx=b.uidx and a.style_id=b.style_id and a.load_date=b.load_date
left join 
(select distinct dci.customer_login as uidx,fci.style_id, 1 as bought_flag
FROM fact_core_item fci
join dim_customer_idea dci on fci.idcustomer=dci.id
WHERE fci.store_id = 1
AND   (fci.is_shipped = 1 OR fci.is_realised = 1)
AND   order_created_date>=to_char(sysdate- interval '1 day','YYYYMMDD')::bigint) c on a.uidx=c.uidx and a.style_id=b.style_id);


delete from dev.pm_size_chart_metrics_cs_nb where load_date=to_char(sysdate- interval '1 day','YYYYMMDD')::bigint;
insert into dev.pm_size_chart_metrics_cs_nb 
(select load_date,style_id,
count(*) as pdp_views,
count(case when size_chart_flag=1 then 1 end) as size_chart_views,
count(case when size_chart_flag=1 and atc_flag=1 then 1 end) as sc_seen_atc
from dev.pm_size_chart_temp 
where bought_flag=0
group by 1,2);

delete from dev.pm_size_chart_metrics_all_b where load_date=to_char(sysdate- interval '1 day','YYYYMMDD')::bigint;
insert into dev.pm_size_chart_metrics_all_b
(select *,null as returned_flag,null as returned_size_issues_flag,null as exchange_size_issues_flag
from dev.pm_size_chart_temp 
where bought_flag=1);


update dev.pm_size_chart_metrics_all_b 
set 
bought_flag=src.bought_flag,
returned_flag=src.returned_flag,
returned_size_issues_flag=src.returned_size_issues_flag,
exchange_size_issues_flag=src.exchange_size_issues_flag
from
(select order_created_date as load_date,dci.customer_login as uidx,fci.style_id, 1 as bought_flag, 
max(case when frr.is_refunded=1 then 1 else 0 end) as returned_flag,
max(case when frr.is_refunded=1 and return_reason in ('Fit Issues','Size too small','Size too large') then 1 else 0 end) as returned_size_issues_flag,
max(case when frr.is_refunded=1 and return_reason in ('Fit Issues','Size too small','Size too large') and exchange_id then 1 else 0 end) as exchange_size_issues_flag
FROM fact_core_item fci
left join fact_returns frr
        ON FRR.ORDER_ID = FCI.ORDER_ID
       AND frr.item_id = fci.item_id
       AND frr.core_item_id = fci.core_item_id
       AND frr.return_status NOT IN ('CFDC','RJUP','RNC','RQCF','RQCP','RQF','RQSF','RRD','RRRS','RRS','RSD')
       and frr.is_refunded=1
join dim_customer_idea dci on fci.idcustomer=dci.id
WHERE fci.store_id = 1
AND   (fci.is_shipped = 1 OR fci.is_realised = 1)
AND   order_created_date>=to_char(sysdate- interval '45 days','YYYYMMDD')::bigint
group by 1,2,3) src
where src.style_id = dev.pm_size_chart_metrics_all_b.style_id and src.uidx = dev.pm_size_chart_metrics_all_b.uidx and src.load_date=dev.pm_size_chart_metrics_all_b.load_date;


delete from customer_insights.size_chart_metrics_agr where load_date>=to_char(sysdate- interval '45 days','YYYYMMDD')::bigint;

insert into customer_insights.size_chart_metrics_agr
(select load_date,style_id,sum(pdp_views)as pdp_views,sum(size_chart_views) as size_chart_views,sum(sc_seen_atc) as sc_seen_atc,
sum(sc_seen_bought) as sc_seen_bought,sum(sc_seen_all_returns) as sc_seen_all_returns,sum(sc_seen_size_returns) as sc_seen_size_returns,
sum(sc_seen_size_exch) as sc_seen_size_exch,sum(sc_not_seen_bought)as sc_not_seen_bought,sum(sc_not_seen_all_returns) as sc_not_seen_all_returns,
sum(sc_not_seen_size_returns) as sc_not_seen_size_returns, sum(sc_not_seen_size_exch) as sc_not_seen_size_exch
from
(select load_date,style_id,
count(*) as pdp_views,count(case when size_chart_flag=1 then 1 end) as size_chart_views,
count(case when size_chart_flag=1 and atc_flag=1 then 1 end) as sc_seen_atc,
count(case when size_chart_flag=1 and bought_flag=1 then 1 end) as sc_seen_bought,
count(case when size_chart_flag=1 and returned_flag=1 then 1 end) as sc_seen_all_returns,
count(case when size_chart_flag=1 and returned_size_issues_flag=1 then 1 end) as sc_seen_size_returns,
count(case when size_chart_flag=1 and exchange_size_issues_flag=1 then 1 end) as sc_seen_size_exch,
count(case when nvl(size_chart_flag,0)=0 and bought_flag=1 then 1 end) as sc_not_seen_bought,
count(case when nvl(size_chart_flag,0)=0 and returned_flag=1 then 1 end) as sc_not_seen_all_returns,
count(case when nvl(size_chart_flag,0)=0 and returned_size_issues_flag=1 then 1 end) as sc_not_seen_size_returns,
count(case when nvl(size_chart_flag,0)=0 and exchange_size_issues_flag=1 then 1 end) as sc_not_seen_size_exch
from dev.pm_size_chart_metrics_all_b 
where load_date>=to_char(sysdate- interval '45 days','YYYYMMDD')::bigint
group by 1,2
union all 
select *,null as sc_seen_bought,null as sc_seen_all_returns,null as sc_seen_size_returns,null as sc_seen_size_exch,
null as sc_not_seen_bought,null as sc_not_seen_all_returns,null as sc_not_seen_size_returns,null as sc_not_seen_size_exch
from dev.pm_size_chart_metrics_cs_nb
where load_date>=to_char(sysdate- interval '45 days','YYYYMMDD')::bigint)
group by 1,2);
