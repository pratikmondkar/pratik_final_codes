SELECT load_date AS DATE,
       dd.week_of_the_year AS week,
       UPPER(dd.month_string) AS month,
       dd.cal_year AS year,
       style_id,
       pdp_views,
       size_chart_views,
       sc_seen_atc,
       sc_seen_bought,
       sc_seen_all_returns,
       sc_seen_size_returns,
       sc_seen_size_exch,
       sc_not_seen_bought,
       sc_not_seen_all_returns,
       sc_not_seen_size_returns,
       sc_not_seen_size_exch
FROM customer_insights.size_chart_metrics_agr c
  LEFT JOIN dim_date dd ON c.load_date = dd.full_date
WHERE c.DATE BETWEEN $(endDate - offset) $ AND $ endDate $
