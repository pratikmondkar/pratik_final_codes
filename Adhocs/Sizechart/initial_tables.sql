drop table if exists dev.pm_size_chart_metrics_all_b;
create table dev.pm_size_chart_metrics_all_b distkey(uidx) sortkey(load_date) as 
(select *
from customer_insights.size_chart_metrics 
where bought_flag=1);

drop table if exists dev.pm_size_chart_metrics_cs_nb;
create table dev.pm_size_chart_metrics_cs_nb distkey(style_id) sortkey(load_date) as 
(select load_date,style_id,
count(*) as pdp_views,
count(case when size_chart_flag=1 then 1 end) as size_chart_views,
count(case when size_chart_flag=1 and atc_flag=1 then 1 end) as sc_seen_atc
from customer_insights.size_chart_metrics 
where nvl(bought_flag,0)=0
group by 1,2);


drop table if exists customer_insights.size_chart_metrics_agr;
create table customer_insights.size_chart_metrics_agr distkey(style_id) sortkey(load_date) as
(select load_date,style_id,
count(*) as pdp_views,count(case when size_chart_flag=1 then 1 end) as size_chart_views,
count(case when size_chart_flag=1 and atc_flag=1 then 1 end) as sc_seen_atc,
count(case when size_chart_flag=1 and bought_flag=1 then 1 end) as sc_seen_bought,
count(case when size_chart_flag=1 and returned_flag=1 then 1 end) as sc_seen_all_returns,
count(case when size_chart_flag=1 and returned_size_issues_flag=1 then 1 end) as sc_seen_size_returns,
count(case when size_chart_flag=1 and exchange_size_issues_flag=1 then 1 end) as sc_seen_size_exch,
count(case when nvl(size_chart_flag,0)=0 and bought_flag=1 then 1 end) as sc_not_seen_bought,
count(case when nvl(size_chart_flag,0)=0 and returned_flag=1 then 1 end) as sc_not_seen_all_returns,
count(case when nvl(size_chart_flag,0)=0 and returned_size_issues_flag=1 then 1 end) as sc_not_seen_size_returns,
count(case when nvl(size_chart_flag,0)=0 and exchange_size_issues_flag=1 then 1 end) as sc_not_seen_size_exch
from customer_insights.size_chart_metrics group by 1,2);


select count(*) from customer_insights.size_chart_metrics 
