drop table if exists customer_insights.size_chart_metrics;
CREATE TABLE customer_insights.size_chart_metrics
(
   load_date                  bigint,
   uidx                       varchar(100),
   style_id                   bigint,
   size_chart_flag            smallint,
   atc_flag                   smallint,
   bought_flag                smallint,
   returned_flag              smallint,
   returned_size_issues_flag  smallint,
   exchange_size_issues_flag  smallint
) diststyle even 
sortkey(load_date);

insert into customer_insights.size_chart_metrics 
(select a.*,nvl(b.size_chart_flag,0) as size_chart_flag,nvl(b.atc_flag,0) as atc_flag,null as bought_flag, 
null as returned_flag, null as returned_size_issues_flag,null as exchange_size_issues_flag  
from
(select load_date,uidx,style_id::bigint 
from clickstream.discount_aggregates
where load_date>=to_char(sysdate- interval '60 days','YYYYMMDD')::bigint and uidx is not null
and style_id SIMILAR TO '[0-9]{6,8}') a
left join
(select load_date,uidx,style_id,
max(case when event_type ='sizingChart' then 1 else 0 end) as size_chart_flag, 
max(case when event_type ='addToCart' then 1 else 0 end) as atc_flag 
from customer_insights.user_style_sizingchart_daily_updated
where load_date>=to_char(sysdate- interval '60 days','YYYYMMDD')::bigint and event_type in ('sizingChart','addToCart')
group by 1,2,3) b on a.uidx=b.uidx and a.style_id=b.style_id and a.load_date=b.load_date);

update customer_insights.size_chart_metrics 
set 
bought_flag=src.bought_flag,
returned_flag=src.returned_flag,
returned_size_issues_flag=src.returned_size_issues_flag,
exchange_size_issues_flag=src.exchange_size_issues_flag
from
(select order_created_date as load_date,dci.customer_login as uidx,fci.style_id, 1 as bought_flag, 
max(case when frr.is_refunded=1 then 1 else 0 end) as returned_flag,
max(case when frr.is_refunded=1 and return_reason in ('Fit Issues','Size too small','Size too large') then 1 else 0 end) as returned_size_issues_flag,
max(case when frr.is_refunded=1 and return_reason in ('Fit Issues','Size too small','Size too large') and exchange_id then 1 else 0 end) as exchange_size_issues_flag
FROM fact_core_item fci
left join fact_returns frr
        ON FRR.ORDER_ID = FCI.ORDER_ID
       AND frr.item_id = fci.item_id
       AND frr.core_item_id = fci.core_item_id
       AND frr.return_status NOT IN ('CFDC','RJUP','RNC','RQCF','RQCP','RQF','RQSF','RRD','RRRS','RRS','RSD')
       and frr.is_refunded=1
join dim_customer_idea dci on fci.idcustomer=dci.id
WHERE fci.store_id = 1
AND   (fci.is_shipped = 1 OR fci.is_realised = 1)
AND   order_created_date>=to_char(sysdate- interval '60 days','YYYYMMDD')::bigint
group by 1,2,3) src
where src.style_id = customer_insights.size_chart_metrics.style_id and src.uidx = customer_insights.size_chart_metrics.uidx and src.load_date=customer_insights.size_chart_metrics.load_date;



