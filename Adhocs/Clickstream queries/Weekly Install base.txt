select to_char(sysdate - interval '%(ds)s days','YYYYMMDD') as date,ap.*,ma.active from
                (select campaign_source,campaign_name,datediff(mons,install_date,sysdate - interval '%(ds)s days') as tenure,count(distinct device_device_id) as active
                from
                            (select distinct device_device_id from  
                                            (select  device_device_id
                                            from clickstream.applaunch_view
                                            where load_date between to_char(sysdate - interval '%(dstart1)s days','YYYYMMDD') and to_char(sysdate - interval '%(dfinish1)s days','YYYYMMDD')
                                            union all
                                            select device_device_id
                                                from clickstream.beacon_ping_view 
                                            where load_date between to_char(sysdate - interval '%(dstart1)s days','YYYYMMDD') and to_char(sysdate - interval '%(dfinish1)s days','YYYYMMDD')
                                            union all
                                            select device_device_id
                                            from clickstream.push_notification_received_view 
                                            where load_date between to_char(sysdate - interval '%(dstart1)s days','YYYYMMDD') and to_char(sysdate - interval '%(dfinish1)s days','YYYYMMDD')
                                            ) f
                            ) b
                left join 
                app_referral r
                on r.device_id=b.device_device_id and r.keyspace='ANDI' and attribution='Install' and install_date<sysdate - interval '%(ds)s days'
                group by 1,2,3) ma
full outer join 
                (select campaign_source,campaign_name,datediff(mons,install_date,sysdate - interval '%(ds)s days') as tenure,count(distinct device_id) as ovr_installs
                from app_referral where keyspace='ANDI' and attribution='Install' and install_date<sysdate - interval '%(ds)s days' group by 1,2,3) ap
on ma.campaign_source=ap.campaign_source and ma.campaign_name=ap.campaign_name and ma.tenure=ap.tenure