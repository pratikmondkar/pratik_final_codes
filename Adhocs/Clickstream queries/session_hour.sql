select 
session_id,
uidx,
device_id,
gaid,
device_manufacturer,
device_model_number,
device_category,
app_version,
app_name,
os,
city,
state,
country,
is_first_session,
session_start_time,

--Overall
count(1) as events,
COUNT( CASE WHEN event_type = 'ScreenLoad' THEN event_id END) AS ScreenLoads,
(MAX(client_ts::float) -MIN(client_ts::Float))  AS session_duration_seconds,
max(is_logged_in) as is_logged_in,
COUNT(CASE WHEN event_type ilike '%appLaunch%' then event_id end) as app_launches,

--page views 
COUNT(CASE WHEN event_type = 'ScreenLoad' AND screen_name in ('/feed','/stream') then event_id end) as home_page_views,
COUNT(CASE WHEN event_type = 'ScreenLoad' AND screen_name ilike ('Landing Page%') and len(screen_name)<70 then event_id end) as landing_page_views,
COUNT( CASE WHEN event_type = 'ScreenLoad' AND (screen_name LIKE '%Shopping Page-Search%' or screen_name LIKE '%Shopping Page-List%' ) THEN event_id END) AS total_list_page_views,
COUNT( CASE WHEN event_type = 'ScreenLoad' AND screen_name LIKE '%Shopping Page-Search%' THEN event_id END) AS search_results_page_views,
COUNT( CASE WHEN event_type = 'ScreenLoad' AND screen_name LIKE '%Shopping Page-PDP%' THEN event_id END) AS PDP_views,
COUNT( CASE WHEN event_type = 'ScreenLoad' AND screen_name iLIKE '%Checkout%cart%' THEN event_id END) AS cart_views,
COUNT( CASE WHEN event_type = 'ScreenLoad' AND screen_name iLIKE '%Checkout%address%' THEN event_id END) AS address_page_views,
COUNT( CASE WHEN event_type = 'ScreenLoad' AND screen_name iLIKE '%Checkout%payment%' THEN event_id END) AS payment_page_views,

--PDP views 
COUNT(CASE WHEN screen_name LIKE 'Shopping Page-PDP%' AND event_type = 'ScreenLoad' AND SPLIT_PART(screen_name,'/',3) = 'Women' THEN event_id END) AS women_pdp_views,
COUNT(CASE WHEN screen_name LIKE 'Shopping Page-PDP%' AND event_type = 'ScreenLoad' AND SPLIT_PART(screen_name,'/',3) = 'Men' THEN event_id END) AS men_pdp_views,
COUNT(CASE WHEN screen_name LIKE 'Shopping Page-PDP%' AND event_type = 'ScreenLoad' AND SPLIT_PART(screen_name,'/',3) IN ('Boys','Girls') THEN event_id END) AS kids_pdp_views,
COUNT(CASE WHEN screen_name LIKE 'Shopping Page-PDP%' AND event_type = 'ScreenLoad' AND SPLIT_PART(screen_name,'/',3) IN ('Unisex') THEN event_id END) AS unisex_pdp_views,
COUNT(CASE WHEN screen_name LIKE 'Shopping Page-PDP%' AND event_type = 'ScreenLoad' AND SPLIT_PART(screen_name,'/',2) IN ('Apparel') THEN event_id END) AS apparel_pdp_views,
COUNT(CASE WHEN screen_name LIKE 'Shopping Page-PDP%' AND event_type = 'ScreenLoad' AND SPLIT_PART(screen_name,'/',2) IN ('Footwear') THEN event_id END) AS footwear_pdp_views,
COUNT(CASE WHEN screen_name LIKE 'Shopping Page-PDP%' AND event_type = 'ScreenLoad' AND SPLIT_PART(screen_name,'/',2) IN ('Accessories') THEN event_id END) AS accessories_pdp_views,
COUNT(CASE WHEN screen_name LIKE 'Shopping Page-PDP%' AND event_type = 'ScreenLoad' AND SPLIT_PART(screen_name,'/',2) IN ('Home') THEN event_id END) AS home_pdp_views,
COUNT(CASE WHEN screen_name LIKE 'Shopping Page-PDP%' AND event_type = 'ScreenLoad' AND SPLIT_PART(screen_name,'/',2) IN ('Personal Care') THEN event_id END) AS personalcare_pdp_views,
COUNT(CASE WHEN screen_name LIKE 'Shopping Page-PDP%' AND event_type = 'ScreenLoad' AND SPLIT_PART(screen_name,'/',2) IN ('Sporting Goods') THEN event_id END) AS sportinggoods_pdp_views,

--Home page events
max(case when event_type in ('feedCardLoad','streamCardLoad','feedCardChildLoad' , 'streamCardChildLoad') and screen_name in ('/feed','/stream') then nvl(widget_v_position,'0')::int end) as homepage_scroll_depth,
max(case when event_type in ('feedCardClick','streamCardClick','feedCardChildClick' , 'streamCardChildClick') and screen_name in ('/feed','/stream') then nvl(widget_v_position,'0')::int end) as homepage_click_depth,

count( case when event_type in ('feedCardLoad','streamCardLoad','feedCardChildLoad' , 'streamCardChildLoad') and screen_name in ('/feed','/stream') then event_id end) as homepage_total_card_loads,
count( case when event_type in ('feedCardClick','streamCardClick','feedCardChildClick' , 'streamCardChildClick') and screen_name in ('/feed','/stream') then event_id end) as homepage_total_card_clicks,

count( case when event_type in ('feedCardChildLoad' , 'streamCardChildLoad') and card_type = 'suggestions' then event_id end) as mwk_loads,
count( case when event_type in ('feedCardChildClick' , 'streamCardChildClick') and card_type = 'suggestions' then event_id end) as mwk_clicks,

count(case when event_type in ('feedCardLoad','streamCardLoad','feedCardChildLoad' , 'streamCardChildLoad') and card_type in ('event-banner','split-banner','banner','BANNER','CAROUSEL-BANNER','carousel-banner','TOPNAV_CAROUSEL-FALLBACK','SLIDESHOW-FALLBACK','SPLIT-BANNER-FALLBACK','carousel','slideshow','SPLIT-BANNER-FALLBACK','budget-store') then event_id end) as banner_card_loads,
count(case when event_type in ('feedCardClick','streamCardClick','feedCardChildClick' , 'streamCardChildClick') and card_type in ('event-banner','split-banner','banner','BANNER','CAROUSEL-BANNER','carousel-banner','TOPNAV_CAROUSEL-FALLBACK','SLIDESHOW-FALLBACK','SPLIT-BANNER-FALLBACK','carousel','slideshow','SPLIT-BANNER-FALLBACK','budget-store') then event_id end) as banner_card_clicks,
      
count(case when event_type in ('feedCardLoad','streamCardLoad') and card_type IN ('bag-links','grid-card','product-rack-large.v2','custom-product-rack-large.v2' ,'custom-product.v2','ANSWER','ARTICLE','article','COLLECTIONS','collections','POLL','POST.SHOT','post.shot','POST.STYLEUPDATE','post.styleupdate','QUESTION','VIDEO','video','custom-product','custom-product-rack-large','PRODUCT','product','product.v2','product-rack-large') then event_id end) as personalised_card_loads ,
count(case when event_type in ('feedCardClick','streamCardClick','feedCardChildClick' , 'streamCardChildClick') and card_type IN ('bag-links','grid-card','product-rack-large.v2','custom-product-rack-large.v2' ,'custom-product.v2','ANSWER','ARTICLE','article','COLLECTIONS','collections','POLL','POST.SHOT','post.shot','POST.STYLEUPDATE','post.styleupdate','QUESTION','VIDEO','video','custom-product','custom-product-rack-large','PRODUCT','product','product.v2','product-rack-large') then event_id end) as personalised_card_clicks ,

count( case when event_type IN ('Level1LeftNavClick','Level2LeftNavClick','Level3LeftNavClick') then event_id end) as left_burger_clicks,

--by card types loads and clicks
count( case when event_type = 'Level2LeftNavClick' and data_set_name = 'Orders' then event_id end) as my_orders_page_views,
count( case when event_type = 'Level2LeftNavClick' and data_set_name = 'Account' then event_id end) as my_account_page_views,

--List and search events
--add exception handling for non interger values
max(case when event_type in ('SearchScreenLoad','list_page_exit_event') then  nvl(scroll_position,'0')::int end) as scroll_depth_all_list,
max(case when event_type in ('SearchScreenLoad','list_page_exit_event') and screen_name ilike '%Shopping Page-Search%' then  nvl(scroll_position,'0')::int end) as scroll_depth_search_page,
Max(CASE WHEN event_type = 'ScreenLoad' AND screen_name ilike '%Shopping Page-PDP%' AND (prev_screen_name ilike '%Shopping Page-Search%' or prev_screen_name ilike '%Shopping Page-List%') THEN (((prev_sub_widget_v_position-1)*2)+prev_sub_widget_h_position)::bigint END) as click_depth_all_list ,
Max(CASE WHEN event_type = 'ScreenLoad' AND screen_name ilike '%Shopping Page-PDP%' AND prev_screen_name ilike '%Shopping Page-Search%' THEN (((prev_sub_widget_v_position-1)*2)+prev_sub_widget_h_position)::bigint END) as click_depth_search_page ,
cOUNT( CASE WHEN event_type IN ('SearchFired','searchFired') THEN event_id END) AS searches_fired,
count( case when event_type = 'Apply Filters' then event_id end) as filters_applied,

--PDP events
COUNT(CASE WHEN event_type IN ('sizingChart') THEN event_id END) AS sizingChart_click,
COUNT(CASE WHEN event_type IN ('ProductDetailsClick') THEN event_id END) AS ProductDetailsClick,
COUNT(CASE WHEN event_type IN ('pdpLike') THEN event_id END) AS pdp_like,
COUNT(CASE WHEN event_type = 'CheckDelivery' THEN event_id END) AS CheckDelivery,
COUNT(CASE WHEN event_type = 'clickForOffer' THEN event_id END) AS clickForOffer,
COUNT(CASE WHEN event_type = 'ImageZoom' THEN event_id END) AS ImageZoom,
COUNT(CASE WHEN event_type = 'InfoClick' THEN event_id END) AS InfoClick,

--SHortlisting events
COUNT( CASE WHEN event_type = 'AddToCollection' and custom_variable_1 is null THEN event_id END) AS added_to_collection,
COUNT( CASE WHEN event_type = 'AddToCollection' and custom_variable_1 = 'wishlist' THEN event_id END) AS added_to_wishlist,
COUNT( CASE WHEN event_type in ('addToCart','moveToCart') THEN event_id END) AS added_to_cart,
COUNT( CASE WHEN event_type = 'removeFromCollection' and custom_variable_1 not like '%Wishlist%' THEN event_id END) AS remove_from_collection,
COUNT( CASE WHEN event_type = 'removeFromCollection' and custom_variable_1 like '%Wishlist%'  THEN event_id END) AS remove_from_wishlist,

--Orders
COUNT(DISTINCT CASE WHEN event_type = 'ScreenLoad' AND screen_name ilike '%Checkout%confirm%' THEN data_set_value END) AS order_confirmation,
--listagg(CASE WHEN event_type = 'ScreenLoad' AND screen_name ilike '%Checkout%confirm%' THEN data_set_value end, ',') as order_ids,

--Misc
COUNT(CASE WHEN LOWER(event_type) LIKE '%like%' THEN event_id END) AS total_likes,
COUNT(CASE WHEN LOWER(event_type) LIKE '%unlike%' THEN event_id END) AS unlikes,
COUNT(CASE WHEN event_type ilike '%share%' THEN event_id END) AS shares,
COUNT(CASE WHEN event_type = 'Follow' THEN event_id END) AS follow,
COUNT(CASE WHEN event_type = 'unFollow' OR event_type = 'Unfollow' THEN event_id END) AS unfollow

from clickstream.events_2017_12_18

where event_type not in ('beacon-ping','CollapsePushNotification','install','notification-deleted','notification-scheduled','notification-updated','push-notification-capped',
													'push-notification-dismissed','push-notification-received','schedule-received','update')
													
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
order by events desc
limit 100;
