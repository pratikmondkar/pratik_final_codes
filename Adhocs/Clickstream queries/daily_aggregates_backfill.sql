--Updating last heard date
update dev.akash_last_heard 
set last_heard=src.last_event_date
from
(select user_id,greatest(case when last_event_date=20181231 then 20171231 else nvl(last_event_date,0) end,nvl(last_visited_date,0),nvl(last_purchase_date,0)) as last_event_date
from dev.pm_ful_bkfill where user_id is not null) src
where src.user_id=dev.akash_last_heard.user_id;

insert into dev.akash_last_heard 
(user_id,last_heard)
select src.user_id,last_event_date 
from
(select user_id,greatest(case when last_event_date=20181231 then 20171231 else nvl(last_event_date,0) end,nvl(last_visited_date,0),nvl(last_purchase_date,0)) as last_event_date
from dev.pm_ful_bkfill where user_id is not null) src,
dev.akash_last_heard dest
where src.user_id = dest.user_id (+)
AND   dest.user_id IS NULL;

--updating purchase dates

update dev.akash_commerce_lifetime 
set 
last_purchase_date=src.last_purchase_date,
first_purchase_date=src.first_purchase_date
from
(select b.id,last_purchase_date,first_purchase_date
from dev.pm_ful_bkfill a
join dim_customer_idea b on a.user_id=b.customer_login 
where user_id is not null) src
where src.id=dev.akash_commerce_lifetime.idcustomer;

--updating last visited date
update dev.akash_clickstream_lifetime 
set last_visited=src.last_visited_date
from
(select user_id,last_visited_date
from dev.pm_ful_bkfill where user_id is not null) src
where src.user_id=dev.akash_clickstream_lifetime.user_id;


