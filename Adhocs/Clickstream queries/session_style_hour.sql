create table dev.session_style_qc distkey(session_id) as
SELECT device_id,
       uidx,
       session_id , 
       load_date,
       app_name,
       os,
       EXTRACT(HOUR FROM convert_timezone ('Asia/Calcutta',(TIMESTAMP 'epoch' + atlas_ts::FLOAT/ 1000*INTERVAL '1 Second '))) as hour , 
	   nvl(widget_item_whom , data_Set_value) as style_id , 
	     COUNT(CASE WHEN (app_name <> 'MyntraRetailWeb' and event_type = 'ScreenLoad' and  screen_name ilike 'Shopping Page-PDP%'  )  or
	     ( app_name = 'MyntraRetailWeb' and data_Set_type = 'product' ) THEN 1 END) AS pdp_views,
	     count(case when event_type in ('addToCart' , 'movedToCart') then 1 end ) as atc , 
       COUNT(CASE WHEN event_type = 'AddToCollection' AND custom_variable_1 IS NULL THEN 1 END) AS add_to_collection,
       COUNT(CASE WHEN event_type = 'AddToCollection' AND custom_variable_1 IS NOT NULL THEN 1 END) AS add_to_wishlist,
       COUNT(CASE WHEN event_type IN ('sizingChart') THEN 1 END) AS sizingChart_click,
       COUNT(CASE WHEN event_type IN ('ProductDetailsClick') THEN 1 END) AS ProductDetailsClick,
       COUNT(CASE WHEN event_type IN ('pdpLike') THEN 1 END) AS pdp_like,
       COUNT(CASE WHEN event_type = 'broken size button clicked' THEN 1 END) AS broken_size_button_clicked,
       COUNT(CASE WHEN event_type = 'broken size clicked' THEN 1 END) AS broken_size_clicked,
       COUNT(CASE WHEN event_type = 'CheckDelivery' THEN 1 END) AS CheckDelivery
       COUNT(CASE WHEN event_type = 'clickForOffer' THEN 1 END) AS clickForOffer,
       COUNT(CASE WHEN event_type = 'CrossLink' THEN 1 END) AS CrossLink,
       COUNT(CASE WHEN event_type = 'crossLinkClick' THEN 1 END) AS crossLinkClick,
       COUNT(CASE WHEN event_type = 'Follow' THEN 1 END) AS Follow,
       MAX(CASE WHEN event_type = 'ImageSwipe' THEN widget_item0_h_position END) AS ImageSwipe,
       COUNT(CASE WHEN event_type = 'ImageZoom' THEN 1 END) AS ImageZoom,
       COUNT(CASE WHEN event_type = 'InfoClick' THEN 1 END) AS InfoClick,
       COUNT(CASE WHEN event_type = 'More-Profiles' THEN 1 END) AS More_Profiles,
       COUNT(CASE WHEN event_type = 'ProfileVisitClick' THEN 1 END) AS ProfileVisitClick,
       COUNT(CASE WHEN event_type = 'ProfileClick' THEN 1 END) AS ProfileClick,
       MAX(CASE WHEN event_type = 'RecommendedSize' THEN custom_variable_1 END) AS RecommendedSize,
       COUNT(CASE WHEN event_type = 'Similar products loaded' AND widget_type = 'BrokenSizes' THEN 1 END) AS similar_products_loaded_broken_sizes,
       COUNT(CASE WHEN event_type = 'Similar product clicked' AND widget_type = 'BrokenSizes' THEN 1 END) AS similar_products_clicked_broken_sizes,
       COUNT(CASE WHEN event_type = 'Similar products button clicked' AND widget_type = 'button' THEN 1 END) AS similar_products_button_broken_sizes,
       COUNT(CASE WHEN event_type = 'Similar products loaded' AND widget_type = 'list' THEN 1 END) AS similar_products_loaded_list,
       COUNT(CASE WHEN event_type = 'Similar products button clicked' AND widget_type = 'list' THEN 1 END) AS similar_products_button_click_list,
       COUNT(CASE WHEN event_type IN ('similarProductsClick','Similar product clicked') AND widget_type = 'list' THEN 1 END) AS similar_products_click_list,
       COUNT(CASE WHEN event_type = 'Similar product clicked' AND widget_type = 'similar_products_dialog' THEN 1 END) AS similar_products_clicked_dialog_box,
       COUNT(CASE WHEN event_type = 'similarProductsClick' AND widget_type = 'similar_products_reco' THEN 1 END) AS similar_products_clicked_reco,
       COUNT(CASE WHEN event_type = 'Similar products button clicked' AND widget_type = 'SimilarIconPDP' THEN 1 END) AS similar_products_button_clikc_pdpicon,
       COUNT(CASE WHEN event_type = 'Similar products loaded' AND widget_type = 'SimilarIconPDP' THEN 1 END) AS similar_products_loaded_pdpicon,
       COUNT(CASE WHEN event_type = 'Similar product clicked' AND widget_type = 'SimilarIconPDP' THEN 1 END) AS similar_products_clicked_pdpicon,
       MAX(CASE WHEN event_type = 'SizeRecoInfo' THEN custom_variable_1 END) AS size_reco_info,
       COUNT(CASE WHEN event_type = 'unFollow' THEN 1 END) unFollow
FROM clickstream.events_2017_12_12
WHERE   nvl(widget_item_whom , data_Set_value) is not null
AND   event_type not in
('push-notification-received','notification-scheduled','schedule-received','push-notification-dismissed','beacon-ping','notification-updated','CollapsePushNotification','push-notification','install','update')
group by 1,2,3,4,5,6,7,8
 ;
 
 
