unload(
'
SELECT 
style_id,date,time_since_catalogued_buckets,discount_buckets,is_live_style,is_broken_style,is_nonselling_style,is_fresh_style,live_styles,broken_styles,
nonselling_styles,fresh_styles,atp_net_inv_qty,sellable_inv_qty,discount_rule_percentage,inwarded_quantity,inwarded_value,
list_count,pdp_count,atc_count,sold_quantity,sold_qty_full_price,revenue_full_price,revenue,total_discount,
tax,total_mrp,buying_margin,total_cogs,royalty_commission,gmv,other_charges,cart_discount,product_discount,coupon_discount,
vendor_funding,category_funding,returned_quantity,returned_revenue,rto_quantity,rto_revenue,days_since_cataloguing
      FROM customer_insights.fact_category_over_view_metrics
      where date between 20170425 and 20170507'

)to 's3://testashutosh/backups/fact_category_metrics' 
credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
parallel on 
ALLOWOVERWRITE 
DELIMITER AS '\001'; 


drop table if exists dev.lp_tag_by_source; 

create table dev.lp_tag_by_source sortkey(load_date) as
(select a.*,b.final_segment,b.brand,b.article_type,b.gender
from
(SELECT session_end_date as load_date,
			 CASE
         WHEN ref_event_type IN ('RightNavChildClick','Level1LeftNavClick','Level2LeftNavClick','Level3LeftNavClick','topNavClick') THEN 'Burger menu'
         WHEN ref_event_type IN ('streamCardClick','streamCardChildClick','feedCardClick','feedCardChildClick') THEN 'Banner or Card'
         WHEN ref_event_type IN ('SearchFired','searchFired') THEN 'Search'
         ELSE 'others'
       END AS source,
       landing_screen AS screen_name,
       count(*) as lp
FROM customer_insights.funnel_view
WHERE (landing_screen LIKE '%Shopping Page-List%' OR landing_screen LIKE '%Shopping Page-Search%')
AND   session_end_date between 20170425 and 20170507
GROUP BY 1,
         2,
         3) a
         left join 
         (select distinct load_date,screen_name,final_segment,brand,article_type,gender from lp_bag_attribution where load_date between 20170425 and 20170507) b 
         on a.load_date=b.load_date and a.screen_name=b.screen_name);


copy dev.fact_category_over_view_metrics
(style_id,date,time_since_catalogued_buckets,discount_buckets,is_live_style,is_broken_style,is_nonselling_style,is_fresh_style,live_styles,broken_styles,
nonselling_styles,fresh_styles,atp_net_inv_qty,sellable_inv_qty,discount_rule_percentage,inwarded_quantity,inwarded_value,
list_count,pdp_count,atc_count,sold_quantity,sold_qty_full_price,revenue_full_price,revenue,total_discount,
tax,total_mrp,buying_margin,total_cogs,royalty_commission,gmv,other_charges,cart_discount,product_discount,coupon_discount,
vendor_funding,category_funding,returned_quantity,returned_revenue,rto_quantity,rto_revenue,days_since_cataloguing)  
from 's3://testashutosh/backups/fact_category_metrics' 
credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
delimiter AS '\001' escape 
FILLRECORD 
emptyasnull 
blanksasnull 
TRIMBLANKS 
TRUNCATECOLUMNS 
DATEFORMAT AS 'YYYY-MM-DD HH24:MI:SS' 
TIMEFORMAT AS 'YYYY-MM-DD HH24:MI:SS' 
ACCEPTINVCHARS AS ' ' 
maxerror 100000 explicit_ids 
compupdate off statupdate off; 

drop table if exists dev.searc_banner_tag_date;

create table dev.searc_banner_tag_date distkey(style_id) sortkey(load_date) as 
(SELECT nvl(a.date,b.date,c.date,d.date,e.date,f.date,g.date) AS load_date,
			 nvl(a.style_id,b.style_id,c.style_id,d.style_id,e.style_id,f.style_id,g.style_id) AS style_id,
       nvl(a.source,b.source,c.source,d.source,e.source,f.source,g.source) AS source,
       nvl(bag_lp_cnt,0) as bag_lp_cnt,
       nvl(bag_style_cnt,0) as bag_style_cnt,
       nvl(ag_lp_cnt,0) as ag_lp_cnt,
       nvl(ag_style_cnt,0) as ag_style_cnt,
       nvl(bg_lp_cnt,0) as bg_lp_cnt,
       nvl(bg_style_cnt,0) as bg_style_cnt,
       nvl(ba_lp_cnt,0) as ba_lp_cnt,
       nvl(ba_style_cnt,0) as ba_style_cnt,
       nvl(b_lp_cnt,0) as b_lp_cnt,
       nvl(b_style_cnt,0) as b_style_cnt,
       nvl(a_lp_cnt,0) as a_lp_cnt,
       nvl(a_style_cnt,0) as a_style_cnt,
       nvl(g_lp_cnt,0) as g_lp_cnt,
       nvl(g_style_cnt,0) as g_style_cnt
FROM (SELECT a.date,
						 a.style_id,
             b1.source,
             b1.bag_lp_cnt,
             b2.bag_style_cnt
      FROM (SELECT DISTINCT fps.date,
      						 fps.style_id,
                   brand,
                   article_type,
                   gender
            FROM dev.fact_category_over_view_metrics fps
              JOIN dim_style dp ON fps.style_id = dp.style_id
            WHERE is_live_style = 1 and date between 20170425 and 20170507) a
        JOIN (SELECT load_date,
        						 brand,
                     article_type,
                     gender,
                     source,
                     COUNT(*) AS bag_lp_cnt
              FROM dev.lp_tag_by_source
              WHERE final_segment = 'BAG'
              GROUP BY 1,
                       2,
                       3,
                       4,
                       5) b1
          ON a.brand = b1.brand
         AND a.article_type = b1.article_type
         AND a.gender = b1.gender
         and a.date=b1.load_date
        LEFT JOIN (SELECT date,
        									brand,
                          article_type,
                          gender,
                          COUNT(DISTINCT dp.style_id) AS bag_style_cnt
                   FROM dev.fact_category_over_view_metrics fps
                     JOIN dim_style dp ON fps.style_id = dp.style_id
                   WHERE is_live_style = 1 and date between 20170425 and 20170507
                   GROUP BY 1,
                            2,
                            3,
                            4) b2
               ON a.brand = b2.brand
              AND a.article_type = b2.article_type
              AND a.gender = b2.gender
              and a.date = b2.date) a
  FULL OUTER JOIN (SELECT a.date,
  												a.style_id,
                          b1.source,
                          b1.ag_lp_cnt,
                          b2.ag_style_cnt
                   FROM (SELECT DISTINCT fps.date, 
                   							fps.style_id,
                                article_type,
                                gender
                         FROM dev.fact_category_over_view_metrics fps
                           JOIN dim_style dp ON fps.style_id = dp.style_id
                         WHERE is_live_style = 1 and date between 20170425 and 20170507) a
                     JOIN (SELECT load_date,
                     							article_type,
                                  gender,
                                  source,
                                  COUNT(*) AS ag_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE final_segment = 'AG'
                           GROUP BY 1,
                                    2,
                                    3,
                                    4) b1
                       ON a.article_type = b1.article_type
                      AND a.gender = b1.gender
                      and a.date = b1.load_date
                     LEFT JOIN (SELECT date,
                     									 article_type,
                                       gender,
                                       COUNT(DISTINCT dp.style_id) AS ag_style_cnt
                                FROM dev.fact_category_over_view_metrics fps
                                  JOIN dim_style dp ON fps.style_id = dp.style_id
                                WHERE is_live_style = 1 and date between 20170425 and 20170507
                                GROUP BY 1,
                                         2,
                                         3) b2
                            ON a.article_type = b2.article_type
                           AND a.gender = b2.gender
                           and a.date = b2.date) b on a.style_id=b.style_id and a.source=b.source and a.date=b.date
  FULL OUTER JOIN (SELECT a.style_id,
  												a.date,
                          b1.source,
                          b1.bg_lp_cnt,
                          b2.bg_style_cnt
                   FROM (SELECT DISTINCT fps.style_id,
                   							date,
                                brand,
                                gender
                         FROM dev.fact_category_over_view_metrics fps
                           JOIN dim_style dp ON fps.style_id = dp.style_id
                         WHERE is_live_style = 1 and date between 20170425 and 20170507) a
                     JOIN (SELECT load_date,
                     							brand,
                                  gender,
                                  source,
                                  COUNT(*) AS bg_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE final_segment = 'BG'
                           GROUP BY 1,
                                    2,
                                    3,
                                    4) b1
                       ON a.brand = b1.brand
                      AND a.gender = b1.gender
                      and a.date = b1.load_date
                     LEFT JOIN (SELECT date,
                     									 brand,
                                       gender,
                                       COUNT(DISTINCT dp.style_id) AS bg_style_cnt
                                FROM dev.fact_category_over_view_metrics fps
                                  JOIN dim_style dp ON fps.style_id = dp.style_id
                                WHERE is_live_style = 1 and date between 20170425 and 20170507
                                GROUP BY 1,
                                         2,
                                         3) b2
                            ON a.brand = b2.brand
                           AND a.gender = b2.gender
                           and a.date = b2.date) c on a.style_id=c.style_id and a.source=c.source and a.date=c.date
  FULL OUTER JOIN (SELECT a.style_id,
  												a.date,
                          b1.source,
                          b1.ba_lp_cnt,
                          b2.ba_style_cnt
                   FROM (SELECT DISTINCT fps.date, 
                   							fps.style_id,
                                brand,
                                article_type
                         FROM dev.fact_category_over_view_metrics fps
                           JOIN dim_style dp ON fps.style_id = dp.style_id
                         WHERE is_live_style = 1 and date between 20170425 and 20170507) a
                     JOIN (SELECT load_date,
                     							brand,
                                  article_type,
                                  source,
                                  COUNT(*) AS ba_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE final_segment = 'BA'
                           GROUP BY 1,
                                    2,
                                    3,
                                    4) b1
                       ON a.brand = b1.brand
                      AND a.article_type = b1.article_type
                      and a.date = b1.load_date
                     LEFT JOIN (SELECT date,
                     									 brand,
                                       article_type,
                                       COUNT(DISTINCT dp.style_id) AS ba_style_cnt
                                FROM dev.fact_category_over_view_metrics fps
                                  JOIN dim_style dp ON fps.style_id = dp.style_id
                                WHERE is_live_style = 1 and date between 20170425 and 20170507
                                GROUP BY 1,
                                         2,
                                         3) b2
                            ON a.brand = b2.brand
                           AND a.article_type = b2.article_type
                           and a.date = b2.date) d on a.style_id=d.style_id and a.source=d.source and a.date=d.date
  FULL OUTER JOIN (SELECT a.style_id,
  												a.date,
                          b1.source,
                          b1.b_lp_cnt,
                          b2.b_style_cnt
                   FROM (SELECT DISTINCT fps.style_id,
                   							fps.date,
                                brand
                         FROM dev.fact_category_over_view_metrics fps
                           JOIN dim_style dp ON fps.style_id = dp.style_id
                         WHERE is_live_style = 1 and date between 20170425 and 20170507) a
                     JOIN (SELECT load_date,
                     							brand,
                                  source,
                                  COUNT(*) AS b_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE final_segment = 'B'
                           GROUP BY 1,
                                    2,
                                    3) b1 ON a.brand = b1.brand and a.date = b1.load_date
                     LEFT JOIN (SELECT date,
                     									 brand,
                                       COUNT(DISTINCT dp.style_id) AS b_style_cnt
                                FROM dev.fact_category_over_view_metrics fps
                                  JOIN dim_style dp ON fps.style_id = dp.style_id
                                WHERE is_live_style = 1 and date between 20170425 and 20170507
                                GROUP BY 1,2) b2 ON a.brand = b2.brand and a.date = b2.date) e on a.style_id=e.style_id and a.source=e.source and a.date=e.date
  FULL OUTER JOIN (SELECT a.style_id,
  												a.date,
                          b1.source,
                          b1.a_lp_cnt,
                          b2.a_style_cnt
                   FROM (SELECT DISTINCT fps.style_id,
                   							fps.date,
                                article_type
                                FROM dev.fact_category_over_view_metrics fps
                           JOIN dim_style dp ON fps.style_id = dp.style_id
                         WHERE is_live_style = 1 and date between 20170425 and 20170507) a
                     JOIN (SELECT load_date,
                     							article_type,
                                  source,
                                  COUNT(*) AS a_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE final_segment = 'A'
                           GROUP BY 1,
                                    2,
                                    3) b1 ON a.article_type = b1.article_type and a.date = b1.load_date
                     LEFT JOIN (SELECT date,
                     									 article_type,
                                       COUNT(DISTINCT dp.style_id) AS a_style_cnt
                                FROM dev.fact_category_over_view_metrics fps
                                  JOIN dim_style dp ON fps.style_id = dp.style_id
                                WHERE is_live_style = 1 and date between 20170425 and 20170507
                                GROUP BY 1,2) b2 ON a.article_type = b2.article_type and a.date = b2.date) f on a.style_id=f.style_id and a.source=f.source and a.date=f.date
  FULL OUTER JOIN (SELECT a.style_id,
  												a.date,
                          b1.source,
                          b1.g_lp_cnt,
                          b2.g_style_cnt
                   FROM (SELECT DISTINCT fps.style_id,
                   							fps.date,
                                gender
                         FROM dev.fact_category_over_view_metrics fps
                           JOIN dim_style dp ON fps.style_id = dp.style_id
                         WHERE is_live_style = 1 and date between 20170425 and 20170507) a
                     JOIN (SELECT load_date,
                     							gender,
                                  source,
                                  COUNT(*) AS g_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE final_segment = 'G'
                           GROUP BY 1,
                                    2,
                                    3) b1 ON a.gender = b1.gender and a.date = b1.load_date
                     LEFT JOIN (SELECT date,
                     									 gender,
                                       COUNT(DISTINCT dp.style_id) AS g_style_cnt
                                FROM dev.fact_category_over_view_metrics fps
                                  JOIN dim_style dp ON fps.style_id = dp.style_id
                                WHERE is_live_style = 1 and date between 20170425 and 20170507
                                GROUP BY 1,2) b2
                           on a.gender = b2.gender
                           and a.date = b2.date) g on a.style_id=g.style_id and a.source=g.source and a.date=g.date);



DROP TABLE dev.pi_score;

CREATE TABLE dev.pi_score distkey 
(
  style_id   
)
AS
(SELECT a.source,
			 a.session_end_date as load_date,
			 a.style_id,
       b.article_type,
       b.cluster_id,
       nvl(SUM(pdp),0) AS pdp,
       nvl(SUM(atc),0) AS atc,
       nvl(SUM(atwl),0) AS atwl,
       nvl(SUM(atcl),0) AS atcl,
       nvl(SUM(pdplike),0) AS pdplike
FROM (SELECT CASE
               WHEN ref_event_type IN ('RightNavChildClick','Level1LeftNavClick','Level2LeftNavClick','Level3LeftNavClick','topNavClick') THEN 'Burger menu'
               WHEN ref_event_type IN ('streamCardClick','streamCardChildClick','feedCardClick','feedCardChildClick') THEN 'Banner or Card'
               WHEN ref_event_type IN ('SearchFired','searchFired') THEN 'Search'
               ELSE 'others'
             END AS source,
             session_end_date,
             style_id,
             nvl(SUM(a.pdp),0) AS pdp,
             nvl(SUM(a.atc),0) AS atc,
             nvl(SUM(a.added_to_collection),0) AS atcl,
             nvl(SUM(a.addToList),0) AS atwl,
             nvl(SUM(a.pdplike),0) AS pdplike
      FROM (SELECT ref_event_type,
                   data_set_value AS style_id,
                   session_end_date,
                   COUNT(DISTINCT CASE WHEN event_type IN ('ScreenLoad') AND landing_screen LIKE 'Shopping Page-PDP%' THEN event_id END) AS pdp,
                   COUNT(DISTINCT CASE WHEN event_type IN ('addToCart') THEN event_id END) AS atc,
                   COUNT(DISTINCT CASE WHEN event_type IN ('AddToCollection') THEN event_id END) AS added_to_collection,
                   COUNT(DISTINCT CASE WHEN event_type IN ('addToList') THEN event_id END) AS addToList,
                   COUNT(DISTINCT CASE WHEN event_type IN ('pdpLike') THEN event_id END) AS pdplike
            FROM customer_insights.funnel_view
            WHERE event_type IN ('ScreenLoad','addToCart','AddToCollection','addToList','pdpLike')
            AND   session_end_date BETWEEN 20170425 and 20170507
            GROUP BY 1,
                     2,
                     3) a /*               FULL OUTER JOIN (SELECT ref_event_type,
                                       entity_id AS style_id,
                                       COUNT(DISTINCT fv.event_id) AS lp
                                FROM customer_insights.funnel_view fv
                                  JOIN clickstream.widget_entity_view we ON fv.event_id = we.event_id
                                WHERE fv.event_type IN ('Product list loaded')
                                AND   we.event_type IN ('Product list loaded')
                                AND   (fv.landing_screen LIKE '%Shopping Page-List%' OR fv.landing_screen LIKE '%Shopping Page-Search%')
                                GROUP BY 1,
                                         2) b
                            ON a.ref_event_type = b.ref_event_type
                           AND a.style_id = b.style_id*/ 
      GROUP BY 1,
               2,
               3) a
  LEFT JOIN dev.cluster_map b ON a.style_id = b.style_id
GROUP BY 1,
         2,
         3,
         4,
         5);

DROP TABLE dev.pi_scored;

CREATE TABLE dev.pi_scored distkey(style_id) sortkey(load_date) AS
(SELECT load_date, 
			 article_type,
       cluster_id,
       style_id,
       SUM(source_weight*tag_cnt) as tag_cnt,
       SUM(source_weight*pdp) as pdp,
       SUM(source_weight*atc) as atc,
       SUM(source_weight*atwl) as atwl,
       SUM(source_weight*atcl) AS atcl
FROM (SELECT nvl(a.load_date,b.load_date) as load_date,
             nvl(a.style_id,b.style_id) as style_id,
						 article_type,
             cluster_id,
             CASE
               WHEN nvl(a.source,b.source) = 'Burger menu' THEN 0.2
               WHEN nvl(a.source,b.source) = 'Banner or Card' THEN 0.1
               WHEN nvl(a.source,b.source) = 'Search' THEN 0.3
               WHEN nvl(a.source,b.source) = 'others' THEN 0.1
               ELSE 0
             END AS source_weight,
             pdp,
             atc,
             atcl,
             atwl,
             tag_cnt
      FROM dev.pi_score a
      full outer join 
      (SELECT load_date,
       style_id,
       source,
       nvl(bag_lp_cnt::float / NULLIF(bag_style_cnt,0),0) + nvl(ag_lp_cnt::float / NULLIF(ag_style_cnt,0),0) + nvl(bg_lp_cnt::float / NULLIF(bg_style_cnt,0),0) + nvl(ba_lp_cnt::float / NULLIF(ba_style_cnt,0),0) + nvl(b_lp_cnt::float / NULLIF(b_style_cnt,0),0) + nvl(a_lp_cnt::float / NULLIF(a_style_cnt,0),0) + nvl(g_lp_cnt::float / NULLIF(g_style_cnt,0),0) AS tag_cnt
FROM dev.searc_banner_tag_date) b on a.load_date=b.load_date and a.style_id=b.style_id and a.source=b.source)
GROUP BY 1,
         2,
         3,
         4);

drop table dev.final_pi_metrics;

CREATE TABLE dev.final_pi_metrics distkey(style_id) sortkey(date) AS
(SELECT a.*,b.article_type,b.cluster_id,b.tag_cnt,b.pdp,b.atc,b.atwl,b.atcl,
case when c.disc_norm_factor>0.8 then 4.0 when c.disc_norm_factor<0.2 then 0.25 else c.disc_norm_factor/(1-disc_norm_factor) end as disc_norm_factor
FROM dev.fact_category_over_view_metrics a
  LEFT JOIN dev.pi_scored b ON a.style_id = b.style_id and a.date=b.load_date
  left join (select distinct date,psh.style_id,article_type,nvl(discount_rule_percentage,0) as disc_percent,percent_rank() over (partition by date,article_type order by nvl(discount_rule_percentage,0) ) disc_norm_factor
from dev.fact_category_over_view_metrics psh
join dim_style ds on psh.style_id=ds.style_id
where date between 20170425 and 20170507 and is_live_style=1) c ON a.style_id = c.style_id and a.date=c.date 
where a.date between 20170425 and 20170507);

drop table if exists dev.final_final_pi_metrics;
  
create table dev.final_final_pi_metrics distkey(style_id) sortkey(date) as
(select *,
nvl(sold_quantity,0) + nvl(tag_cnt,0)*0.56 +nvl(pdp,0)*0.01 + nvl(atc,0)*0.23 + nvl(atcl,0)*0.23 + nvl(atwl,0)*0.23 as base_pi_score,
(nvl(sold_quantity,0) + nvl(tag_cnt,0)*0.56 +nvl(pdp,0)*0.01 + nvl(atc,0)*0.23 + nvl(atcl,0)*0.23 + nvl(atwl,0)*0.23)/disc_norm_factor as norm_pi_score
from dev.final_pi_metrics);


insert into customer_insights.final_pi_metrics
(select style_id,date,time_since_catalogued_buckets,discount_buckets,is_live_style,is_broken_style,is_nonselling_style,is_fresh_style,live_styles,broken_styles,nonselling_styles,fresh_styles,atp_net_inv_qty,sellable_inv_qty,discount_rule_percentage,inwarded_quantity,inwarded_value,list_count,pdp_count,atc_count,sold_quantity,sold_qty_full_price,revenue_full_price,revenue,total_discount,tax,total_mrp,buying_margin,total_cogs,royalty_commission,gmv,other_charges,cart_discount,product_discount,coupon_discount,vendor_funding,category_funding,returned_quantity,returned_revenue,rto_quantity,rto_revenue,days_since_cataloguing,article_type,null as cluster_id,tag_cnt,pdp,atc,atwl,atcl,disc_norm_factor,base_pi_score,norm_pi_score 
from dev.final_final_pi_metrics);


select date,count(*) from dev.final_final_pi_metrics group by 1;
