drop table if exists dev.lp_tag_by_source; 

create table dev.lp_tag_by_source sortkey(load_date) as
(select a.*,b.final_segment,b.brand,b.article_type,b.gender
from
(SELECT session_end_date as load_date,
			 CASE
         WHEN ref_event_type IN ('RightNavChildClick','Level1LeftNavClick','Level2LeftNavClick','Level3LeftNavClick','topNavClick') THEN 'Burger menu'
         WHEN ref_event_type IN ('streamCardClick','streamCardChildClick','feedCardClick','feedCardChildClick') THEN 'Banner or Card'
         WHEN ref_event_type IN ('SearchFired','searchFired') THEN 'Search'
         ELSE 'others'
       END AS source,
       landing_screen AS screen_name,
       count(*) as lp
FROM customer_insights.funnel_view
WHERE (landing_screen LIKE '%Shopping Page-List%' OR landing_screen LIKE '%Shopping Page-Search%')
AND   session_end_date between 20170101 and 20170227
GROUP BY 1,
         2,
         3) a
         left join 
         (select distinct load_date,screen_name,final_segment,brand,article_type,gender from lp_bag_attribution where load_date between 20170101 and 20170227) b 
         on a.load_date=b.load_date and a.screen_name=b.screen_name)


