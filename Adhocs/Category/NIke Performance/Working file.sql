DROP TABLE dev.pi_score;

CREATE TABLE dev.pi_score distkey
(
  style_id   
)
AS
(SELECT a.source,
       b.article_type,
       b.cluster_id,
       b.style_id,
       nvl(SUM(pdp),0) AS pdp,
       nvl(SUM(atc),0) AS atc,
       nvl(SUM(atwl),0) AS atwl,
       nvl(SUM(atcl),0) AS atcl,
--       nvl(SUM(lp),0) AS lp,
       nvl(SUM(pdplike),0) AS pdplike
FROM dev.cluster_map b
  LEFT JOIN (SELECT CASE
                      WHEN nvl (a.ref_event_type) IN ('RightNavChildClick','Level1LeftNavClick','Level2LeftNavClick','Level3LeftNavClick','topNavClick') THEN 'Burger menu'
                      WHEN nvl (a.ref_event_type) IN ('streamCardClick','streamCardChildClick','feedCardClick','feedCardChildClick') THEN 'Banner or Card'
                      WHEN nvl (a.ref_event_type) IN ('SearchFired','searchFired') THEN 'Search'
                      ELSE 'others'
                    END AS source,
--                    nvl(a.style_id,b.style_id) AS 
                    style_id,
                    nvl(SUM(a.pdp),0) AS pdp,
                    nvl(SUM(a.atc),0) AS atc,
--                    nvl(SUM(b.lp),0) AS lp,
                    nvl(SUM(a.added_to_collection),0) AS atcl,
                    nvl(SUM(a.addToList),0) AS atwl,
                    nvl(SUM(a.pdplike),0) AS pdplike
             FROM (SELECT ref_event_type,
                          data_set_value AS style_id,
                          COUNT(DISTINCT CASE WHEN event_type IN ('ScreenLoad') AND landing_screen LIKE 'Shopping Page-PDP%' THEN event_id END) AS pdp,
                          COUNT(DISTINCT CASE WHEN event_type IN ('addToCart') THEN event_id END) AS atc,
                          COUNT(DISTINCT CASE WHEN event_type IN ('AddToCollection') THEN event_id END) AS added_to_collection,
                          COUNT(DISTINCT CASE WHEN event_type IN ('addToList') THEN event_id END) AS addToList,
                          COUNT(DISTINCT CASE WHEN event_type IN ('pdpLike') THEN event_id END) AS pdplike
                   FROM customer_insights.funnel_view
                   WHERE event_type IN ('ScreenLoad','addToCart','AddToCollection','addToList','pdpLike')
                   and session_end_date between 20170110 and 201700208 
                   GROUP BY 1,
                            2) a
/*               FULL OUTER JOIN (SELECT ref_event_type,
                                       entity_id AS style_id,
                                       COUNT(DISTINCT fv.event_id) AS lp
                                FROM customer_insights.funnel_view fv
                                  JOIN clickstream.widget_entity_view we ON fv.event_id = we.event_id
                                WHERE fv.event_type IN ('Product list loaded')
                                AND   we.event_type IN ('Product list loaded')
                                AND   (fv.landing_screen LIKE '%Shopping Page-List%' OR fv.landing_screen LIKE '%Shopping Page-Search%')
                                GROUP BY 1,
                                         2) b
                            ON a.ref_event_type = b.ref_event_type
                           AND a.style_id = b.style_id*/
             GROUP BY 1,
                      2) a ON a.style_id = b.style_id
GROUP BY 1,
         2,
         3,
         4);

DROP TABLE dev.pi_scored;

CREATE TABLE dev.pi_scored distkey
(
  style_id   
)
AS
(SELECT article_type,
       cluster_id,
       style_id,
--       SUM(source_weight*lp)*0.000758511 + 
       SUM(source_weight*pdp)*0.048832736 + SUM(source_weight*atc) + SUM(source_weight*atwl) + SUM(source_weight*atcl) AS pi_score
FROM (SELECT article_type,
             cluster_id,
             style_id,
             CASE
               WHEN source = 'Burger menu' THEN 0.3
               WHEN source = 'Banner or Card' THEN 0.1
               WHEN source = 'Search' THEN 0.7
               WHEN source = 'others' THEN 0.1
               ELSE 0
             END AS source_weight,
             pdp,
--             lp,
             atc,
             atcl,
             atwl
      FROM dev.pi_score)
GROUP BY 1,
         2,
         3);




drop table dev.final_cluster_metrics;

CREATE TABLE dev.final_cluster_metrics distkey
(
  style_id   
)
AS
(SELECT a.*,
       brand,
       brand_type,
       case when nvl(b.live_days,0)>0 then 1 else 0 end as live_flag,
       nvl(b.live_days,0) AS live_days,
       nvl(b.atp_net_inv_qty,0) AS atp_net_inv_qty,
       nvl(b.sellable_inv_qty,0) AS sellable_inv_qty,
       nvl(b.avg_discount_rule_percentage,0) AS avg_discount_rule_percentage,
       nvl(b.list_count,0) AS list_count,
       nvl(b.sold_quantity,0) AS sold_quantity,
       nvl(b.sold_qty_full_price,0) AS sold_qty_full_price,
       nvl(b.revenue_full_price,0) AS revenue_full_price,
        nvl(b.revenue,0) AS revenue
FROM dev.pi_scored a
  LEFT JOIN (select style_id,
       brand,
       brand_type,
       article_type,
       count(distinct case when is_live_style=1 then load_date end) as live_days,
       sum(case when load_date =20170208 then atp_net_inv_qty else 0 end) as atp_net_inv_qty,
       sum(case when load_date =20170208 then sellable_inv_qty else 0 end) as sellable_inv_qty,
       avg(discount_rule_percentage) as avg_discount_rule_percentage,
       sum(list_count) as list_count,
       sum(sold_quantity) as sold_quantity,
       sum(sold_qty_full_price) as sold_qty_full_price,
       sum(revenue_full_price) as revenue_full_price,
       sum(revenue) as revenue    
from dev.clus_style_perf
group by 1,2,3,4) b ON a.style_id = b.style_id);


select cluster_id,
				brand,
       brand_type,
       article_type,
       count(distinct style_id) as total_styles,
       count(distinct case when live_flag=1 then style_id end) as live_styles,
       sum(pi_score) as pi_score,
       sum(live_days) as live_days,
       sum(atp_net_inv_qty) as atp_net_inv_qty,
       sum(sellable_inv_qty) as sellable_inv_qty,
       sum(case when live_flag=1 then sellable_inv_qty else 0 end) as live_inv_qty,
       avg(case when live_flag=1 then avg_discount_rule_percentage end) as avg_discount_rule_percentage,
       sum(list_count) as list_count,
       sum(sold_quantity) as sold_quantity,
       sum(sold_qty_full_price) as sold_qty_full_price,
       sum(revenue_full_price) as revenue_full_price,
       sum(revenue) as revenue
       from dev.final_cluster_metrics 
       group by 1,2,3,4;
       


       
select 
       article_type,
       cluster_id,
       avg(case when live_flag=1 then avg_discount_rule_percentage end) as avg_discount_rule_percentage
       from dev.final_cluster_metrics 
       group by 1;



insert into dev.final_cluster_metrics
(select article_type,cluster_id,style_id,pi_score,case when inv > 6000 then 6000 else inv end as inv,live_styles,qty from final_cluster);




select load_date,count(distinct device_id) as users,count(distinct session_id) as sessions 
,count(distinct case when os='Android' then device_id end) as and_users,count(distinct case when os='Android' then session_id end) as and_sessions
,count(distinct case when os='iOS' then device_id end) as ios_users,count(distinct case when os='iOS' then session_id end) as ios_sessions
from clickstream.events_view
where event_type in ('ScreenLoad','appLaunch','streamCardLoad','FeedCardLoad')
group by 1;



select load_date,count(distinct device_id) as users,count(distinct session_id) as sessions 
,count(distinct case when os='Android' then device_id end) as and_users,count(distinct case when os='Android' then session_id end) as and_sessions
,count(distinct case when os='iOS' then device_id end) as ios_users,count(distinct case when os='iOS' then session_id end) as ios_sessions
from clickstream.events_2017_02_19
where event_type in ('ScreenLoad','appLaunch','streamCardLoad')
group by 1;

select * from ga_metrics limit 10;

select * from ga_metrics where date between 20170301 and 20170302;

select order_created_date,case when device_channel='mobile-app' then os_info else device_channel end as channel,count(distinct order_group_id) 
from fact_core_item where order_created_date between 20170301 and 20170302 and store_id=1 and is_booked=1 group by 1,2;


SELECT load_date,
                   app_name,
                   count(distinct session_id) as sessions
                   FROM clickstream.events_view
            WHERE event_type in ('ScreenLoad','appLaunch','streamCardLoad')  
            and load_Date between 20170301 and 20170302
            group by 1,2;
            
create table dev.d30_base_gender_and distkey(device_id) as
(select cb.device_id,os,gender,inferred_gender 
from (select distinct device_id,max(os) as os
from
(select distinct lower(device_id) as device_id,os from clickstream.daily_aggregates where os in ('Android') and load_date>=to_char(sysdate - interval '31 days','YYYYMMDD')::bigint
union all
select distinct lower(device_id) as device_id,os from clickstream.daily_notif_aggregates where os in ('Android') and load_date>=to_char(sysdate - interval '31 days','YYYYMMDD')::bigint)
group by 1) cb
left join 
(select a.device_id,gender
from
(select lower(device_id) as device_id,user_id as uidx,row_number() over (partition by device_id order by updated_date desc ) as rnk
from bidb.app_users_magasin) a
left join bidb.dim_customer_idea dci on a.uidx=dci.customer_login
where rnk=1) c on cb.device_id=c.device_id
left join 
(SELECT  device_id,
                    gender as inferred_gender
             FROM (SELECT user_id as device_id,
                          gender,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime
                   WHERE identifier='device_id'
                   GROUP BY 1,
                            2)
             WHERE rnk = 1) d on cb.device_id=d.device_id);


select nvl(case when gender in ('m','f') then gender end, case when inferred_gender in ('Men','Boys') then 'm' when  inferred_gender in ('Women','Girls') then 'f' end,'unk') as final_gender,count(distinct device_id)
from dev.d30_base_gender_and where os='Android'
group by 1 ;


drop table if exists dev.d30_base_gender_ios;

create table dev.d30_base_gender_ios distkey(device_id) as
(select cb.device_id,os,gender,inferred_gender 
from (select distinct device_id,max(os) as os
from
(select distinct device_id as device_id,os from clickstream.daily_aggregates where os in ('iOS') and load_date>=to_char(sysdate - interval '31 days','YYYYMMDD')::bigint
union all
select distinct device_id as device_id,os from clickstream.daily_notif_aggregates where os in ('iOS') and load_date>=to_char(sysdate - interval '31 days','YYYYMMDD')::bigint
union all 
select device_id,'iOS' as os from dev.gcm_ios_20170302)
group by 1) cb
left join 
(select a.device_id,gender
from
(select lower(device_id) as device_id,user_id as uidx,row_number() over (partition by device_id order by updated_date desc ) as rnk
from bidb.app_users_magasin) a
left join cii.dim_customer_email dce on a.uidx=dce.email
left join bidb.dim_customer_idea dci on dce.uid=dci.id
where rnk=1) c on cb.device_id=c.device_id
left join 
(SELECT  device_id,
                    gender as inferred_gender
             FROM (SELECT user_id as device_id,
                          gender,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime
                   WHERE identifier='device_id'
                   GROUP BY 1,
                            2)
             WHERE rnk = 1) d on cb.device_id=d.device_id);


select gender,count(distinct device_id)
from dev.d30_base_gender_ios where os='iOS'
group by 1 ;

select  nvl(case when gender in ('m','f') then gender end, case when inferred_gender in ('Men','Boys') then 'm' when  inferred_gender in ('Women','Girls') then 'f' end,'unk') as final_gender,count(distinct device_id)
from dev.d30_base_gender_ios where os='iOS' group by 1;

select * from dev.d30_base_gender_ios limit 100;

select a.device_id,uidx,gender,rnk
from
(select device_id,user_id as uidx,row_number() over (partition by device_id order by updated_date desc ) as rnk
from bidb.app_users_magasin) a
left join bidb.dim_customer_idea dci on a.uidx=dci.customer_login
where device_id ='5295df3a641a2f0b';

select email,gender from dim_customer_Idea dci join dim_customer_email dce on dci.id=dce.uid where customer_login ='ea90d95d.5c77.46f8.94cc.68413d04c32fr5gZfEmdBs';



select distinct device_id,user_id from app_users_magasin where user_id in ('naresh.krishnaswamy@myntra.com','f676fae9.4b91.4873.a7cb.ecf5d8081eff0XEXWCOBGZ');
select * from app_users_magasin where user_id in ('neha.malhan@myntra.com','c00198c2.bb59.4d44.9c38.30171c425821C8MGTguNY8');

select * from dev.d30_base_gender_ios2 where device_id in ('77CEF32A-52ED-44AC-9B0F-0EE1A11D301C','5AFB2026-8C74-43F2-A1C6-7B8E9F23691B');

select email,uid from cii.dim_customer_email where email like '%neha.malhan%' limit 100;

select gender from dim_customer_idea where customer_login = 'f676fae9.4b91.4873.a7cb.ecf5d8081eff0XEXWCOBGZ' limit 100;

--5AFB2026-8C74-43F2-A1C6-7B8E9F23691B
--5AFB2026-8C74-43F2-A1C6-7B8E9F23691B;

select customer_login,gender from dim_customer_idea where id ='16748101'


drop table dev.d30_base_gender_ios2;
create table dev.d30_base_gender_ios2 distkey(device_id) as
(select cb.device_id,os,gender,inferred_gender 
from (select distinct device_id,max(os) as os
from
(select distinct device_id as device_id,os from clickstream.daily_aggregates where os in ('iOS') and load_date>=to_char(sysdate - interval '31 days','YYYYMMDD')::bigint
union all
select distinct device_id as device_id,os from clickstream.daily_notif_aggregates where os in ('iOS') and load_date>=to_char(sysdate - interval '31 days','YYYYMMDD')::bigint
union all 
select device_id,'iOS' as os from dev.gcm_ios_20170302)
group by 1) cb
left join 
(select a.device_id,nvl(dci1.gender,dci2.gender) as gender
from
(select device_id,user_id as uidx,row_number() over (partition by device_id order by updated_date desc ) as rnk
from bidb.app_users_magasin) a
left join cii.dim_customer_email dce on a.uidx=dce.email
left join bidb.dim_customer_idea dci1 on dce.uid=dci1.id
left join bidb.dim_customer_idea dci2 on dci2.customer_login=a.uidx
where rnk=1) c on cb.device_id=c.device_id
left join 
(SELECT  device_id,
                    gender as inferred_gender
             FROM (SELECT user_id as device_id,
                          gender,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime
                   WHERE identifier='device_id'
                   GROUP BY 1,
                            2)
             WHERE rnk = 1) d on cb.device_id=d.device_id);
             


select * from dev.comb_base_gender where device_id in ('44327bdd9f7c5831',	'5295df3a641a2f0b',	'68f29f14ff0c613e',	'9C2D4107-3CA1-4C8D-9ABF-4A4A791B137B',	'a8a81254b7c581ca',	'cd7f4ce514a13d53',	'd0930aca2edb7b3f',	'E36B5E56-05A3-4403-88A0-4819820B1351');

select a.final_gender,b.final_gender,count(distinct a.device_id) as cnt
from
(select distinct device_id, nvl(case when gender in ('m','f') then gender end, case when inferred_gender in ('Men','Boys') then 'm' when  inferred_gender in ('Women','Girls') then 'f' end,'unk') as final_gender
from dev.d30_base_gender_ios where os='iOS') a
left join 
(select distinct device_id, nvl(case when gender in ('m','f') then gender end, case when inferred_gender in ('Men','Boys') then 'm' when  inferred_gender in ('Women','Girls') then 'f' end,'unk') as final_gender
from dev.d30_base_gender_ios2 where os='iOS') b on a.device_id=b.device_id
group by 1,2;


select  nvl(case when gender in ('m','f') then gender end, case when inferred_gender in ('Men','Boys') then 'm' when  inferred_gender in ('Women','Girls') then 'f' end,'unk') as final_gender,count(distinct device_id)
from dev.d30_base_gender_ios2 where os='iOS'
group by 1


DROP TABLE IF EXISTS dev.device_details_merge;

CREATE TABLE dev.device_details_merge 
AS
(select a.*,c.ga_id,c.os,gender,email,phone,first_login_date,first_purchase_date,last_purchase_date,to_char(c.install_date,'YYYYMMDD')::bigint as install_date
from (select device_id,uidx,load_date as last_event_date
from
(select distinct device_id,uidx,load_date,row_number() over (partition by device_id order by load_date desc) as rnk 
from
				(select device_id,uidx,load_date from clickstream.daily_aggregates where load_date >= to_char(sysdate - interval '2 days','YYYYMMDD')::bigint
				union all
				select device_id,uidx,load_date from clickstream.daily_notif_aggregates where load_date >= to_char(sysdate - interval '2 days','YYYYMMDD')::bigint)
) 
where rnk=1) a
left join bidb.fact_user_lifetime b on a.uidx=b.user_id and b.user_id_identifier='uidx'
left join (select device_id,max(advertising_id) as ga_id, max(device_type) as os,min(created_date) as install_date
						from app_users_magasin
						group by 1) c on a.device_id=c.device_id);


update
customer_insights.device_details
set
uidx=src.uidx,
last_event_date=src.last_event_date,
ga_id=src.ga_id,
os=src.os,
gender=src.gender,
email=src.email,
phone=src.phone,
first_login_date=src.first_login_date,
first_purchase_date=src.first_purchase_date,
last_purchase_date=src.last_purchase_date,
install_date=src.install_date
FROM
(
select
device_id,uidx,last_event_date,ga_id,os,gender,email,phone,first_login_date,first_purchase_date,last_purchase_date,install_date
FROM
dev.device_details_merge
) src
WHERE
src.device_id = customer_insights.device_details.device_id;


INSERT INTO customer_insights.device_details(
device_id,uidx,last_event_date,ga_id,os,gender,email,phone,first_login_date,first_purchase_date,last_purchase_date,install_date
)
SELECT
src.device_id,
src.uidx,
src.last_event_date,
src.ga_id,
src.os,
src.gender,
src.email,
src.phone,
src.first_login_date,
src.first_purchase_date,
src.last_purchase_date,
src.install_date
FROM
(select
device_id,uidx,last_event_date,ga_id,os,gender,email,phone,first_login_date,first_purchase_date,last_purchase_date,install_date
FROM dev.device_details_merge) src,
(SELECT device_id FROM customer_insights.device_details) dest
where
src.device_id = dest.device_id (+)
and dest.device_id is null;

end;


unload(
'
SELECT  device_id,uidx,last_event_date,ga_id,os,gender,email,phone,first_login_date,first_purchase_date,last_purchase_date,install_date
FROM customer_insights.device_details'

)to 's3://testashutosh/backups/device_details' 
credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
parallel on 
ALLOWOVERWRITE 
DELIMITER AS '\001'; 


grant select on customer_insights.device_details to analysis_user


alter table customer_insights.device_details add column first_name varchar(255);

update
customer_insights.device_details
set
first_name=src.first_name
FROM
(
select
customer_login,first_name
FROM
bidb.dim_customer_idea
) src
WHERE
src.customer_login = customer_insights.device_details.uidx;

SELECT os,
       COUNT(device_id)
FROM customer_insights.device_details
GROUP BY 1;


SELECT os,
       COUNT(device_id)
FROM customer_insights.device_details
where last_event_date>to_char(sysdate - interval '30 days' ,'YYYYMMDD')::bigint
group by 1
limit 100;


select * from app_users_magasin
where device_id='874d39cbb25766e8'



SELECT *
FROM dev.device_details_merge 
where os is null and last_event_date>to_char(sysdate - interval '30 days' ,'YYYYMMDD')::bigint
limit 100;

create table dev.os_lookup distkey(device_id) as 
(select device_id,max(os) as os from 
(select device_id,os from clickstream.daily_aggregates
				union all
				select device_id,os from clickstream.daily_notif_aggregates)
				group by 1);

select * from dev.os_lookup limit 100;

drop table dev.device_details_merge; 

select last_event_date,count(*) from customer_insights.device_details where last_event_date>20170301 group by 1


update
customer_insights.device_details
set
os=src.os
FROM
(
select
device_id ,os
FROM
dev.os_lookup
) src
WHERE
src.device_id = customer_insights.device_details.device_id;


CREATE TABLE dev.aum_dedup distkey
(
  advertising_id   
)
AS
(SELECT DISTINCT advertising_id,
       device_id,
       nvl(dci1.id,dci2.id) AS idcustomer
FROM app_users_magasin aum
  LEFT JOIN dim_customer_idea dci1 ON aum.user_id = dci1.customer_login
  LEFT JOIN cii.dim_customer_email dce ON aum.user_id = dce.email
  LEFT JOIN dim_customer_idea dci2 ON dce.uid = dci2.id);
  


select to_char(to_date(load_date,'YYYYMMDD'),'YYYY-MM') as month,sum(sessions) as sessions,sum(screenloads) from clickstream.daily_aggregates where load_date >=20160401 and os='Android' group by 1;

select coupon_code,
TO_CHAR(ROUND(SUM(item_revenue + nvl(cashback_redeemed,0)),0),'99,99,99,99,999') AS revenue,
       TO_CHAR(COUNT(DISTINCT order_group_id),'99,99,99,99,999') AS orders,
       round(sum(coupon_discount)*100 /sum(item_mrp*item_quantity),2) as cd,
       round(sum(trade_discount)*100 /sum(item_mrp*item_quantity),2) as td,
       round(sum(item_revenue + nvl(cashback_redeemed,0) - nvl(tax_amount,0) - nvl(item_purchase_price_inc_tax,0) + nvl(vendor_funding,0) - nvl(royalty_commission,0))*100/sum(item_revenue + cashback_redeemed - tax_amount),2) as gm
FROM fact_order_live fci
WHERE (is_shipped = 1 OR is_realised = 1)
--AND  coupon_code ilike ('%%HHSUN5RDG2%%') or  coupon_code ilike ('%%HHSUN6FRT3%%') or  coupon_code ilike ('%%HHSUN7HNK4%%')
--or  coupon_code ilike ('%%HHSUN8WDS5%%') or  coupon_code ilike ('%%BLOCKBUSTER25%%')
AND   order_created_date =20170423
group by 1


select sum(revenue)
from 
(SELECT load_date,(CASE WHEN uidx IS NOT NULL THEN uidx ELSE device_id END) AS user_id,
       (CASE WHEN uidx IS NOT NULL THEN 'Logged-In-App' ELSE 'Non-Logged-In-App' END) AS identifier
FROM clickstream.daily_aggregates WHERE load_Date between 20170301 and 20170331 GROUP BY 1,2,3
union 
SELECT load_date,(CASE WHEN uidx IS NOT NULL THEN uidx ELSE device_id END) AS user_id,
       (CASE WHEN uidx IS NOT NULL THEN 'Logged-In-App' ELSE 'Non-Logged-In-App' END) AS identifier
FROM customer_insights.daily_aggregates_web WHERE load_Date between 20170301 and 20170331 GROUP BY 1,2,3) a
left join 
(select dci.customer_login,order_created_date,sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as revenue
from 
fact_core_item fci 
left join dim_customer_idea dci on fci.idcustomer=dci.id where store_id=1 and (is_shipped=1 or is_realised=1) and order_Created_date between 20170301 and 20170331  
group by 1,2) b on a.user_id=b.customer_login and a.load_date=b.order_created_date;


select sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as revenue
from 
fact_core_item where store_id=1 and (is_shipped=1 or is_realised=1) and order_Created_date between 20170301 and 20170331 ;


limit 200;


create table customer_insights.traffic_funnel as
(SELECT c.date AS DATE,
       dd.week_of_the_year AS week,
       UPPER(dd.month_string) AS MONTH,
       dd.cal_year AS YEAR,
       nvl(utm_medium,'Organic') AS medium,
       CASE
         WHEN utm_campaign IS NULL AND utm_medium IS NULL THEN 'Organic'
         ELSE utm_campaign
       END AS campaign,
       COUNT(DISTINCT c.session_id) AS total_sessions,
       SUM(list) AS list_views,
       SUM(pdp) AS pdp_views,
       SUM(atc) AS add_to_carts,
       SUM(atc_revenue) AS atc_revenue,
       SUM(atc_qty) AS atc_qty_sold,
       SUM(ses_revenue) AS session_revenue,
       SUM(ses_qty) AS session_qty_sold
FROM (SELECT session_id,
             MIN(load_date) AS DATE
      FROM clickstream.events_view
      WHERE event_type IN ('appLaunch','ScreenLoad')
      and load_date BETWEEN to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '5 days','YYYYMMDD')::bigint AND to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '1 days','YYYYMMDD')::bigint
      GROUP BY 1) c
  LEFT JOIN (SELECT nvl(b.utm_medium,a.utm_medium) AS utm_medium,
                    nvl(b.utm_campaign,a.utm_campaign) AS utm_campaign,
                    nvl(a.session_id,b.session_id) AS session_id
             FROM (SELECT DISTINCT session_id,
                          utm_medium,
                          utm_campaign
                   FROM clickstream.events_view
                   WHERE event_type = 'appLaunch'
                   and load_date BETWEEN to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '5 days','YYYYMMDD')::bigint AND to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '1 days','YYYYMMDD')::bigint) a
               FULL OUTER JOIN (SELECT session_id,
                                       utm_medium,
                                       utm_campaign
                                FROM (SELECT session_id,
                                             utm_medium,
                                             utm_campaign,
                                             ROW_NUMBER() OVER (PARTITION BY session_id ORDER BY client_ts) AS rnk
                                      FROM clickstream.events_view
                                      WHERE event_type IN ('push-notification','DeepLink')
                                      and load_date BETWEEN to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '5 days','YYYYMMDD')::bigint AND to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '1 days','YYYYMMDD')::bigint)
                                WHERE rnk = 1) b ON a.session_id = b.session_id) d ON c.session_id = d.session_id
  LEFT JOIN (SELECT session_id,
                    COUNT(CASE WHEN screen_name LIKE 'Shopping Page-List Page%' OR screen_name LIKE 'Shopping Page-Search%' THEN 1 END) AS list,
                    COUNT(CASE WHEN screen_name LIKE 'Shopping Page-PDP%' THEN 1 END) AS pdp,
                    COUNT(CASE WHEN event_type IN ('addToCart') THEN 1 END) AS atc
             FROM clickstream.events_view
             WHERE event_type IN ('ScreenLoad','addToCart') and load_date BETWEEN to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '5 days','YYYYMMDD')::bigint AND to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '1 days','YYYYMMDD')::bigint
             GROUP BY 1) f ON c.session_id = f.session_id
  LEFT JOIN (SELECT session_id,
                    SUM(quantity) AS atc_qty,
                    SUM(item_revenue_inc_cashback + nvl (shipping_charges,0) + nvl (gift_charges,0)) AS atc_revenue
             FROM (SELECT session_id,
                          uidx,
                          data_set_value
                   FROM (SELECT session_id,
                                uidx,
                                data_set_value,
                                client_ts,
                                ROW_NUMBER() OVER (PARTITION BY uidx,data_set_value ORDER BY client_ts) AS rnk
                         FROM clickstream.events_view
                         WHERE event_type IN ('addToCart')
                         and load_date BETWEEN to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '5 days','YYYYMMDD')::bigint AND to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '1 days','YYYYMMDD')::bigint)
                   WHERE rnk = 1) a
               JOIN bidb.dim_customer_idea dci ON dci.customer_login = a.uidx
               JOIN bidb.fact_core_item fci
                 ON fci.idcustomer = dci.id
                AND a.data_set_value = fci.style_id
                AND order_created_date BETWEEN to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '5 days','YYYYMMDD')::bigint AND to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '1 days','YYYYMMDD')::bigint
             WHERE store_id = 1
             AND   (is_shipped = 1 OR is_realised = 1) 
             GROUP BY 1) e ON e.session_id = c.session_id
  LEFT JOIN (SELECT session_id,
                    SUM(quantity) AS ses_qty,
                    SUM(item_revenue_inc_cashback + nvl (shipping_charges,0) + nvl (gift_charges,0)) AS ses_revenue
             FROM (SELECT DISTINCT session_id,
                          REPLACE(data_set_value,'-','') AS store_order_id
                   FROM clickstream.events_view
                   WHERE event_type IN ('ScreenLoad')
                   and load_date BETWEEN to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '5 days','YYYYMMDD')::bigint AND to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '1 days','YYYYMMDD')::bigint
                   AND   screen_name = 'Checkout-confirmation') a
               JOIN bidb.fact_core_item fci ON fci.store_order_id = a.store_order_id
             WHERE store_id = 1
             AND   (is_shipped = 1 OR is_realised = 1)
             AND   order_created_date BETWEEN to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '5 days','YYYYMMDD')::bigint AND to_char(convert_timezone('Asia/Calcutta',sysdate)- interval '1 days','YYYYMMDD')::bigint
             GROUP BY 1) g ON g.session_id = c.session_id
  LEFT JOIN bidb.dim_date dd ON c.date = dd.full_date
GROUP BY 1,2,3,4,5,6)


SELECT count(*),count(distinct a.uidx) as users
FROM (SELECT uidx,
             style_id
      FROM (SELECT uidx,
                   cl.style_id,
                   action,
                   ROW_NUMBER() OVER (PARTITION BY uidx,cl.style_id ORDER BY added_datetime DESC) AS rnk
            FROM bidb.collection_log_eors cl
            WHERE uidx NOT LIKE '%%myntra360.com%%'
            AND   uidx NOT LIKE '%%eossbenchmarking%%@gmail.com'
            AND   uidx NOT LIKE '%%scmloadtest%%'
            AND   uidx NOT LIKE '%%sfloadtest%%')
      WHERE rnk = 1
      AND   action = 1) a
  JOIN (SELECT DISTINCT style_id
        FROM fact_product_snapshot
        WHERE DATE = 20170106
        AND   (style_status != 'P'
        OR    net_inventory_count = 0) ) b ON a.style_id = b.style_id
  LEFT JOIN (SELECT DISTINCT customer_login
             FROM fact_core_item fci
               JOIN dim_customer_idea dci
                 ON fci.idcustomer = dci.id
                AND store_id = 1
                AND is_shipped = 1
                AND order_created_date BETWEEN 20170102
                AND 20170105) c ON a.uidx = c.customer_login
  left join dim_style ds on a.style_id=ds.style_id
WHERE c.customer_login IS NULL LIMIT 100


SELECT nvl(TO_CHAR(created_date,'YYYY-MM'),'unk') AS install_month,
       COUNT(DISTINCT c.device_id) AS devices
FROM (SELECT DISTINCT device_id
      FROM (SELECT device_id
            FROM clickstream.daily_aggregates
            WHERE load_date between 20161201 and 20161231
            AND   os = 'Android'
            UNION ALL
            SELECT device_id
            FROM clickstream.daily_notif_aggregates
            WHERE load_date between 20161201 and 20161231
            AND   os = 'Android') a) AS c
  LEFT JOIN (SELECT device_id,
                    MIN(created_date) AS created_date
             FROM bidb.app_users_magasin
             GROUP BY 1) i ON c.device_id = i.device_id
GROUP BY 1;

SELECT COUNT(DISTINCT device_id)
FROM (SELECT device_id
      FROM clickstream.daily_aggregates
      WHERE load_date between 20161201 and 20161231
      AND   os = 'Android'
      UNION ALL
      SELECT device_id
      FROM clickstream.daily_notif_aggregates
      WHERE load_date between 20161201 and 20161231
      AND   os = 'Android');

SELECT nvl(TO_CHAR(created_date,'YYYY-MM'),'unk') AS install_month,
       COUNT(DISTINCT device_id) AS total_installs
FROM (SELECT device_id,
             MIN(created_date) AS created_date
      FROM bidb.app_users_magasin
      GROUP BY 1)
      group by 1;

SELECT ds.style_id,
             gender,
             season,
             article_mrp,
             base_colour,
             usage_attr,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Ankle Height') AS ankle_type,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Insole') AS Insole,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Micro Trend') AS Micro_Trend,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Occasion') AS Occasion,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Fastening') AS Fastening,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Pattern') AS Pattern,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Shoe Type') AS Shoe_Type,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Sole Material') AS Sole_Material,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Toe Shape') AS Toe_Shape,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Upper Material') AS upper_material
      FROM dim_style ds
      WHERE article_type = 'Casual Shoes'


select * from pg_table_def where schemaname='dev' limit 10;


create table dev.final2 distkey(uidx) as
(select * from dev.final);


select * from stv_inflight;

grant select on dev.b10_email_final to customer_insights_ddl;
select * from stv_inflight;

drop table if exists customer_insights.b10_email_final;

create table 
customer_insights.b10_email_final distkey (email) as 
(select * from dev.b10_email_final);


grant select on customer_insights.b10_email_final to adhoc_clickstream_fin;
grant select on customer_insights.b10_email_final to explain_clickstream_fin;
grant select on customer_insights.b10_email_final to adhoc_clickstream_no_fin;
grant select on customer_insights.b10_email_final to explain_clickstream_no_fin;


SELECT a.*,
       b.avg_qty_sold
FROM (SELECT style_id,
             SUM(CASE WHEN blocked_order_count_from_atp > net_inventory_count THEN 0 ELSE net_inventory_count - blocked_order_count_from_atp END) AS atp_inv
      FROM fact_product_snapshot
      WHERE DATE = TO_CHAR(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')
      AND   style_status = 'FRG'
      GROUP BY 1) a
  LEFT JOIN (SELECT style_id,
                    AVG(sold_quantity) AS avg_qty_sold
             FROM customer_insights.fact_category_over_view_metrics
             WHERE DATE>= TO_CHAR(convert_timezone('Asia/Calcutta',sysdate -INTERVAL '7 days'),'YYYYMMDD') and style_status = 'FRG'
             GROUP BY 1) b ON a.style_id = b.style_id
--where avg_qty_sold>atp_inv

select count(distinct a.session_id) as list_sessions,
count(distinct case when screen_name LIKE '%Shopping Page-PDP%' then a.session_id end) as PDP_sessions
from clickstream.events_2017_05_09 a
join (select distinct session_id from clickstream.events_2017_05_09
where event_type in ('ScreenLoad') and (screen_name LIKE '%Shopping Page-List%' OR screen_name LIKE '%Shopping Page-Search%') ) b on a.session_id=b.session_id;


drop table if exists dev.b10_email_final;

create table dev.b10_email_final distkey(email) as 
(select email,
ful.refined_firstname,
max(case when fn_rnk=1 then image_url end) as image_url1,
max(case when fn_rnk=2 then image_url end) as image_url2,
max(case when fn_rnk=3 then image_url end) as image_url3,
max(case when fn_rnk=4 then image_url end) as image_url4,
max(case when fn_rnk=5 then image_url end) as image_url5,
max(case when fn_rnk=6 then image_url end) as image_url6,
max(case when fn_rnk=1 then landing_url end) as landing_url1,
max(case when fn_rnk=2 then landing_url end) as landing_url2,
max(case when fn_rnk=3 then landing_url end) as landing_url3,
max(case when fn_rnk=4 then landing_url end) as landing_url4,
max(case when fn_rnk=5 then landing_url end) as landing_url5,
max(case when fn_rnk=6 then landing_url end) as landing_url6
from dev.final2 f
left join dev.brand_mapping bm on f.brand=bm.brand
left join fact_user_lifetime ful on f.uidx=ful.user_id and user_id_identifier='uidx'
group by 1,2);


select * from dev.b10_email_final limit 1000;


DROP TABLE IF EXISTS dev.brand_mapping;

CREATE TABLE dev.brand_mapping
(
			brand			 			 varchar(255),
			image_url				 varchar(2000),
			landing_url			 varchar(2000)
)
DISTKEY (brand);


truncate table dev.brand_mapping;

copy dev.brand_mapping   (brand,image_url,landing_url)
FROM 's3://testashutosh/backups/brand_url_mapping.csv' credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
delimiter ',' 
emptyasnull 
blanksasnull 
TRIMBLANKS 
TRUNCATECOLUMNS 
ACCEPTANYDATE 
ACCEPTINVCHARS AS ' ' 
maxerror 10 
explicit_ids 
compupdate off 
statupdate off;


select cnt,count(distinct uidx)
from
(select uidx,count(case when bm.brand is not null then 1 end) as cnt from dev.b10_email_final f left join dev.brand_mapping bm on f.brand=bm.brand group by 1)
--where cnt=8
--limit 100;
group by 1;



select count(distinct email) from dev.b10_email_final where landing_url1 is null or landing_url2 is null or landing_url3 is null or landing_url4 is null or landing_url5 is null or landing_url6 is null;

select * from dev.final2 f 
left join dev.brand_mapping bm on f.brand=bm.brand
where uidx='00159149.abe3.4185.ac12.48c056677268s8b2Egxgrk'


select count(*) from customer_insights.device_details where device_id = '473bd3a851797ad6';

create table dev.tmp1 distkey(device_id) as 
(select distinct * from customer_insights.device_details where device_id in
(select device_id from
(select device_id,count(*) as cnt from customer_insights.device_details group by 1)
where cnt>=2)
);

select user_id from fact_user_lifetime where user_id='01a0f583.ee50.41dd.96a1.f814caeaa94bEjRMHA9Yb0';


select * from dev.tmp1;

delete from customer_insights.device_details where device_id in (select distinct device_id from dev.tmp1);

insert into customer_insights.device_details 
(select device_id,uidx,last_event_date,ga_id,os,gender,email,phone,first_login_date,first_purchase_date,last_purchase_date,install_date,first_name,ms_engagement,ms_performance,ms_discount_affinity,ms_satisfaction,ms_purchase_intent,refined_firstname
from dev.tmp1)


DROP TABLE IF EXISTS dev.brand_mapping;

CREATE TABLE dev.brand_mapping
(
			brand			 			 varchar(255),
			brand_code			 varchar(10),
			seller_id				 bigint
)
DISTKEY (brand);

truncate table dev.brand_mapping;

copy dev.brand_mapping   (brand,brand_code,seller_id)
FROM 's3://testashutosh/backups/brand_seller_mapping.csv' credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
delimiter ',' 
emptyasnull 
blanksasnull 
TRIMBLANKS 
TRUNCATECOLUMNS 
ACCEPTANYDATE 
ACCEPTINVCHARS AS ' ' 
maxerror 10 
explicit_ids 
compupdate off 
statupdate off;

drop table if exists dev.darwin_migration;

create table dev.darwin_migration distkey(style_id) as
(select distinct sim.style_id,sim.seller_id as old_seller_id,bm.seller_id as new_seller_id
from seller_item_master sim
left join dim_product dp on sim.sku_id=dp.sku_id
left join dev.brand_mapping bm on dp.brand=bm.brand
where enabled=1 and sim.seller_id in (19,21,25,29,30) );


select count(*),count(distinct style_id) from dev.darwin_migration limit 100;

create table dev.chk as
(select distinct style_id from dev.darwin_migration
where style_id in (select style_id from (select style_id ,count(*) cnt from dev.darwin_migration group by 1) where cnt >1));


delete from dev.darwin_migration where style_id in (select distinct style_id from dev.chk);



select old_seller_id,new_seller_id,count(distinct style_id) from dev.darwin_migration group by 1,2;


delete from dev.darwin_migration
WHERE new_seller_id is null ;



select dp.style_id,dp.new_seller_id
FROM dev.darwin_migration dp
  left JOIN (SELECT style_id,
                    SUM(inventory_count) AS inventory_count,
                    MAX(style_status) AS style_status
             FROM fact_atp_inventory
             GROUP BY 1) fps ON dp.style_id = fps.style_id
where fps.inventory_count = 0 or fps.style_status is null;


select counT(*) from dev.darwin_migration;






create table dev.tmp1 distkey(idcustomer) as
(select idcustomer,count(distinct order_group_id) as orders,max(order_value) as max_order_value,max(at) as max_at,max(brand) as max_brand,max(qty) as aty
from
(select idcustomer,order_group_id,sum(item_revenue_inc_cashback) as order_value,count(distinct article_type) as at,count(distinct brand) as brand,sum(quantity) as qty
FROM fact_core_item
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
group by 1,2)
group by 1) ;

create table dev.tmp2 distkey(idcustomer) as
(select idcustomer,count(distinct order_group_id) as orders,max(order_value) as max_order_value,max(at) as max_at,max(brand) as max_brand,max(qty) as aty
from
(select idcustomer,order_group_id,sum(item_revenue_inc_cashback) as order_value,count(distinct article_type) as at,count(distinct brand) as brand,sum(quantity) as qty
FROM fact_orderitem fo
left join dim_product dp on fo.sku_id=dp.sku_id 
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1) and order_created_date<20150401
group by 1,2)
group by 1);


select  idcustomer, sum(orders),max(max_order_value) as max_order_value,max(at) as max_at,max(brand) as max_brand,max(qty) as aty;


unload(
'
SELECT 
style_id,date,is_live_style,base_pi_score,norm_pi_score
      FROM customer_insights.final_pi_metrics'

)to 's3://testashutosh/backups/fact_pi_metrics' 
credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
parallel on 
ALLOWOVERWRITE 
DELIMITER AS '\001'; 

drop table if exists dev.pi_metrics;

CREATE TABLE dev.pi_metrics
(
style_id 					bigint,
date							bigint,
is_live_style			smallint,
base_pi_score			float8,
norm_pi_score			float8
)
distkey (style_id)
sortkey (date);

copy dev.pi_metrics
(style_id,date,is_live_style,base_pi_score,norm_pi_score)  
from 's3://testashutosh/backups/fact_pi_metrics' 
credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
delimiter AS '\001' escape 
FILLRECORD 
emptyasnull 
blanksasnull 
TRIMBLANKS 
TRUNCATECOLUMNS 
DATEFORMAT AS 'YYYY-MM-DD HH24:MI:SS' 
TIMEFORMAT AS 'YYYY-MM-DD HH24:MI:SS' 
ACCEPTINVCHARS AS ' ' 
maxerror 100000 explicit_ids 
compupdate off statupdate off; 


unload(
'
SELECT 
style_id,cluster_id,article_type
      FROM customer_insights.style_cluster_map'

)to 's3://testashutosh/backups/fact_cluster' 
credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
parallel on 
ALLOWOVERWRITE 
DELIMITER AS '\001'; 

drop table if exists dev.style_cluster;

CREATE TABLE dev.style_cluster
(
style_id 					bigint,
cluster_id				bigint,
article_type			varchar(255)
)
distkey (style_id)
sortkey (cluster_id);

copy dev.style_cluster
(style_id,cluster_id,article_type)  
from 's3://testashutosh/backups/fact_cluster' 
credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
delimiter AS '\001' escape 
FILLRECORD 
emptyasnull 
blanksasnull 
TRIMBLANKS 
TRUNCATECOLUMNS 
DATEFORMAT AS 'YYYY-MM-DD HH24:MI:SS' 
TIMEFORMAT AS 'YYYY-MM-DD HH24:MI:SS' 
ACCEPTINVCHARS AS ' ' 
maxerror 100000 explicit_ids 
compupdate off statupdate off; 



select dp.style_id,dp.new_seller_id
FROM dev.darwin_migration dp
  left JOIN (SELECT style_id,
                    SUM(inventory_count) AS inventory_count,
                    MAX(style_status) AS style_status
             FROM fact_atp_inventory
             GROUP BY 1) fps ON dp.style_id = fps.style_id
where fps.inventory_count > 0 and fps.style_status ='P';


select dp.style_id,dp.new_seller_id
FROM dev.darwin_migration dp
  left JOIN (SELECT style_id,
                    SUM(inventory_count) AS inventory_count,
                    MAX(style_status) AS style_status
             FROM fact_atp_inventory
             GROUP BY 1) fps ON dp.style_id = fps.style_id
where fps.inventory_count > 0 and fps.style_status !='P';

select dp.style_id,dp.new_seller_id
FROM dev.darwin_migration dp
  left JOIN (SELECT style_id,
                    SUM(inventory_count) AS inventory_count,
                    MAX(style_status) AS style_status
             FROM fact_atp_inventory
             GROUP BY 1) fps ON dp.style_id = fps.style_id
where fps.inventory_count = 0 or fps.style_status is null;


select
-- distinct uidx,data_set_value as style_id,event_type,custom_variable_1
load_date,
count(case when nvl(widget_item_whom,data_set_value)  in ('null','no_id',' ') then 1 end) as issue,count(1) as total
from clickstream.events_view 
where event_type in ('AddToCollection') and load_date between 20170428 and 20170528
--('AddToCollection','addToList','addToCart')
group by 1
limit 100;


select a.*,b.*
from
(select distinct uidx,nvl(widget_item_whom,data_set_value) as style_id
from clickstream.events_2017_05_28 where event_type in ('AddToCollection')) a
left join (select distinct uidx,style_id from collection_log_eors where action=1 and added_date=20170528) b on a.uidx=b.uidx and a.style_id=b.style_id
where a.style_id not in ('null','no_id',' ') and b.style_Id is null
limit 100;


select style_id,count(*) from
(select distinct uidx,nvl(widget_item_whom,data_set_value) as style_id
from clickstream.events_2017_05_28 where event_type in ('AddToCollection'))
group by 1
order by 2 desc
limit 1000

select * from dim_discount_rule_history where discount_id=2738207 and discount_rule_id=1626948 order by last_modified_on desc limit 100;
select * from dim_discount_history where discount_id=2738207 order by last_modified_on desc limit 100;

select * from fact_core_Item where order_group_id =1506549293 and order_Created_date=20170607;


select * from pricing_snapshot_history where style_id=1796955 and date>=20170501

select count(*) from
(select purchase_seq,brand,fci.article_type,cluster_name, 
case when fci.order_created_date BETWEEN 20170201 and 20170530 then 2017 else 2016 end as year,
case when item_revenue_inc_cashback<= exonomy then 'economy' 
when item_revenue_inc_cashback >exonomy and item_revenue_inc_cashback<=mass then 'mass'
when item_revenue_inc_cashback >mass and item_revenue_inc_cashback<=premium then 'premium'
when item_revenue_inc_cashback >premium and item_revenue_inc_cashback<=blt then 'blt'
when item_revenue_inc_cashback> luxury then 'luxury' 
else 'unknown' end as price_point,
sum(quantity) as qty,sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as revenue
FROM fact_core_item fci 
join dim_customer_idea dci on fci.idcustomer=dci.id
left join dev.pm_purchase_seq ps on fci.order_group_id=ps.order_group_id
left join customer_insights.style_cluster_map cm on fci.style_id=cm.style_id
left join dev.demodemo d on fci.article_type=d.article
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   (fci.order_created_date BETWEEN 20170201 and 20170530 or fci.order_created_date BETWEEN 20160201 and 20160530)
and dci.gender='m' and dci.dob between 19920101 and 20170101 and fci.gender='Men' and fci.article_type in ('Tshirts',	'Shirts',	'Casual Shoes',	'Jeans',	'Sports Shoes',	'Trousers',	'Formal Shoes',	'Jackets',	'Watches',	'Shorts')
group by 1,2,3,4,5,6);
a

select article_type,sum(item_revenue_inc_cashback) as rev 
FROM fact_core_item fci  
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   (fci.order_created_date BETWEEN 20170201 and 20170530 or fci.order_created_date BETWEEN 20160201 and 20160530)
and gender='Men'
group by 1



SELECT uidx,
       brand,
       article_type,
       gender,
       rnk
FROM (SELECT customer_Login AS uidx,
             foi.article_type,
             foi.gender,
             SUM(quantity) AS qty,
             ROW_NUMBER() OVER (PARTITION BY uidx ORDER BY qty DESC) AS rnk
      FROM fact_core_item foi
        JOIN dim_customer_idea dci ON foi.idcustomer = dci.id
        JOIN dev.top_50_at t
          ON foi.article_type = t.article_type
         AND foi.gender = t.gender
      WHERE is_realised = 1
      AND   store_id = 1
      AND   order_created_date BETWEEN 20160101 AND 20170201
      GROUP BY 1,
               2,
               3)
WHERE rnk <= 5;


SELECT brand,
       article_type,
       gender,
       count(distinct user_id) as overall_users,
       count(distinct case when rnk=1 then user_id end) as rnk1_users,
       count(distinct case when rnk=2 then user_id end) as rnk2_users,
       count(distinct case when rnk=3 then user_id end) as rnk3_users,
       count(distinct case when rnk=4 then user_id end) as rnk4_users,
       count(distinct case when rnk=5 then user_id end) as rnk5_users
FROM (SELECT user_id,
             brand,
             article_type,
      			 gender,
             SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
             ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY score DESC) AS rnk
      FROM bidb.notif_person_base_lifetime
      where identifier='uidx'
      GROUP BY 1,
               2,
               3,
               4)
WHERE rnk <= 5
group by 1,2,3;



select order_created_date/100 as month,count(distinct idcustomer) from fact_core_item where store_id=1 and is_booked=1 and order_created_date between 20170401 and 20170731 group by 1


select order_created_date,sum(shipped_order_revenue_inc_cashback) as revenue  
FROM fact_order
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date in ('20150103',	'20150718',	'20150719',	'20160102',	'20160103',	'20160104',	'20160701',	'20160702',	'20160703',	'20160704',	'20170102',	'20170103',	'20170104',	'20170105',	'20170623',	'20170624',	'20170625',	'20170626')
group by 1;


select date,sum(sessions) as sessions from ga_metrics where
date in ('20150103',	'20150718',	'20150719',	'20160102',	'20160103',	'20160104',	'20160701',	'20160702',	'20160703',	'20160704',	'20170102',	'20170103',	'20170104',	'20170105',	'20170623',	'20170624',	'20170625',	'20170626')
group by 1;

select * from
(select order_created_date,order_created_time,count(distinct order_group_id) as orders  ,row_number() over (partition by order_created_date order by orders desc) as rnk
FROM fact_order
WHERE store_id = 1
AND   order_created_date in ('20150103',	'20150718',	'20160102',	'20160701',	'20160702',	'20170102',	'20170103',	'20170623',	'20170624')
group by 1,2)
where rnk=1;


select order_created_date,sum(shipped_order_revenue_inc_cashback) as revenue  
FROM fact_order
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date in ('20150103',	'20150718',	'20150719',	'20160102',	'20160103',	'20160104',	'20160701',	'20160702',	'20160703',	'20160704',	'20170102',	'20170103',	'20170104',	'20170105',	'20170623',	'20170624',	'20170625',	'20170626')
group by 1;


select date,sum(sessions) as sessions from ga_metrics where
date in ('20150103',	'20150718',	'20150719',	'20160102',	'20160103',	'20160104',	'20160701',	'20160702',	'20160703',	'20160704',	'20170102',	'20170103',	'20170104',	'20170105',	'20170623',	'20170624',	'20170625',	'20170626')
group by 1;

select * from
(select order_created_date,order_created_time,count(distinct order_group_id) as orders  ,row_number() over (partition by order_created_date order by orders desc) as rnk
FROM fact_order
WHERE store_id = 1
AND   order_created_date in ('20150103',	'20150718',	'20160102',	'20160701',	'20160702',	'20170102',	'20170103',	'20170623',	'20170624')
group by 1,2)
where rnk=1;


select * from
(select style_id,sum(net_inventory_count) as fps_inv from fact_product_snapshot where date=20170810 and net_inventory_count=0 and style_status='P' group by 1) a
join (select style_id,sum(inventory_count ) as inv_atp from fact_atp_inventory group by 1) b on a.style_id=b.style_id
--where inv_atp>0;
limit 100


select distinct session_id from customer_insights.funnel_20170729 where event_type IN ('banner-child-click','banner-click','navi-child-click','navi-click')
limit 100;


select rw,client_ts,event_type,landing_Screen,ref_widget_item_whom,	ref_widget_whom,	ref_widget_v_position,	ref_widget_item0_h_position,	ref_card_type,	ref_event_type,	ref_publisher_tag,	ref_utm_medium,	ref_referrer_url,	ref_notification_class,	ref_utm_source,	ref_utm_campaign,	ref_widget_item_whom_lp,	ref_widget_whom_lp,	ref_widget_v_position_lp,	ref_widget_item0_h_position_lp,	ref_card_type_lp,	ref_landing_screen,	ref_data_set_name_lp,	ref_event_type_lp
from  customer_insights.funnel_20170729
where session_id='27dc8f52-bc3a-4f18-b52b-5bf48204556e-2155e72e01b66eab' order by rw;


select * from stv_inflight;



BEGIN;

drop table if  exists dev.pm_temp1;
CREATE TABLE dev.pm_temp1
(
   style_id      bigint,
   cluster_id    bigint,
   cluster_name  varchar(4000)
) distkey(style_id);

truncate table dev.pm_temp1;

copy dev.pm_temp1
(style_id,cluster_id,cluster_name)  
from 's3://testashutosh/backups/ss_cs_recl.csv' 
credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
delimiter AS '\001' escape 
FILLRECORD 
emptyasnull 
blanksasnull 
TRIMBLANKS 
TRUNCATECOLUMNS 
DATEFORMAT AS 'YYYY-MM-DD HH24:MI:SS' 
TIMEFORMAT AS 'YYYY-MM-DD HH24:MI:SS' 
ACCEPTINVCHARS AS ' ' 
maxerror 100000 explicit_ids 
compupdate off statupdate off; 

delete from customer_insights.style_cluster_map where article_type in ('Sports Shoes','Casual Shoes')  ;

insert into customer_insights.style_cluster_map
select article_type,gender,a.style_id,cluster_id,cluster_name 
from dev.pm_temp1 a
left join dim_style b on a.style_id=b.style_id;


delete from customer_insights.dim_cluster where article_type in ('Sports Shoes','Casual Shoes')  ;

insert into customer_insights.dim_cluster
select distinct  article_type,gender,cluster_id,cluster_name 
from dev.pm_temp1 a
left join dim_style b on a.style_id=b.style_id ;


select article_type,gender,count(*) from customer_insights.style_cluster_map group by 1,2;


select article_type,gender,count(*) from dim_style group by 1,2;

create table dev.cl_temp as 
select distinct article_type,gender,style_id,cluster_id,cluster_name from customer_insights.style_cluster_map;


select count(*) from dev.cl_temp;


truncate table customer_insights.style_cluster_map;

insert into customer_insights.style_cluster_map
select * from dev.cl_temp;


select count(*) ,count(distinct style_id) from customer_insights.style_cluster_map;


select * from customer_insights.style_cluster_map a
join 
(select style_id from
(select style_id,count(*) cnt from customer_insights.style_cluster_map group by 1)
where cnt>1) b on a.style_Id=b.style_id;

select event_type,count(*) from clickstream.events_2017_09_02 group by 1;

select screen_name,count(*) from clickstream.events_2017_09_02 where event_type in ('gameClick') group by 1 order by 2 desc limit 100;

select landing_screen,count(*) from clickstream.events_2017_09_02 where event_type in ('gameClick') group by 1 order by 2 desc limit 100;

select count(distinct uidx) as logged_in_users,count(distinct case when uidx is null then device_id end) as non_logged_in_users from clickstream.events_2017_09_01 where event_type in ('gameClick');

select load_date,count(distinct uidx) as logged_in_users,count(distinct case when uidx is null then device_id end) as non_logged_in_users 
from clickstream.events_view  
where event_type in ('gameClick') and load_date between 20170830 and 20170904
group by 1;

select count(distinct uidx) as logged_in_users,count(distinct case when uidx is null then device_id end) as non_logged_in_users 
from clickstream.events_view  
where event_type in ('gameClick') and load_date between 20170830 and 20170904


CREATE TABLE dev.brand_seller_map
(
	 brand			varchar(255),
   seller_id  varchar(255)
) distkey (brand);

truncate table dev.brand_seller_map;

copy dev.brand_seller_map
(
brand,
seller_id
)
from 's3://testashutosh/backups/brand_seller_mapping.csv'
credentials 'aws_access_key_id=AKIAILAAXNRLGZ3GKS2A;aws_secret_access_key=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'
delimiter ',' 
emptyasnull
blanksasnull
TRIMBLANKS
TRUNCATECOLUMNS
ACCEPTANYDATE
ACCEPTINVCHARS AS ' '
maxerror 10 explicit_ids
compupdate off statupdate off;

drop table if exists dev.style_migration;

create table dev.style_migration distkey(style_id) sortkey(brand) as
(select style_id,c.brand,old_seller,seller_id as new_seller from
(select distinct a.style_id,seller_id as old_seller,brand 
from seller_item_master a
left join dim_style b on a.style_id=b.style_id
where enabled=1 and seller_id in (19,    21,    25,    29,    30,    32)) c
left join dev.brand_seller_map d on c.brand=d.brand);

select old_seller,new_seller,select count(distinct style_id) as styles from dev.style_migration where old_seller!=new_seller group by 1,2;


select distinct dp.style_id,new_seller
FROM dev.style_migration dp
  left JOIN (SELECT style_id,
                    SUM(inventory_count) AS inventory_count,
                    MAX(style_status) AS style_status
             FROM fact_atp_inventory
             GROUP BY 1) fps ON dp.style_id = fps.style_id
where (fps.inventory_count = 0 or fps.style_status is null) and old_seller!=new_seller;

select * from 
(select count(distinct style_id) as cnt from dev.style_migration where old_seller!=new_seller group by 1)
where cnt>1;


select * from dev.style_migration where style_id in (657775,657860,998224);


select event_type,count(1) from clickstream.events_2017_08_28 where event_type ilike '%nav%' and os='Android' group by 1;


select data_set_name,count(1) from clickstream.events_2017_08_28 where event_type = 'navi-child-click' and os='Android' group by 1 order by 2 desc limit 100;

select data_set_name,count(1) from clickstream.events_2017_08_28 where event_type = 'navi-click' and os='Android' group by 1 order by 2 desc limit 100;

select data_set_name,count(1) from clickstream.events_2017_08_28 where event_type = 'Level1LeftNavClick' and os='Android' group by 1 order by 2 desc limit 100;

select data_set_name,count(1) from clickstream.events_2017_08_28 where event_type = 'topNavClick' and os='Android' group by 1 order by 2 desc limit 100;


select data_set_name,count(1) from clickstream.events_2017_08_28 where event_type = 'topNavClick' and os='Android' and app_version='3.17.1' group by 1 order by 2 desc limit 100;

select event_type,count(1) from clickstream.events_2017_08_28 where event_type ilike '%burger%' and os='Android' group by 1;


select event_type,data_set_name as label,count(distinct case when uidx is null then device_id else uidx end ) as users,count(1) as events
from clickstream.events_2017_08_28 
where event_type in ('Level2LeftNavClick',	'RightNavChildClick',	'navi-click',	'Level1LeftNavClick',	'navi-child-click',	'Level3LeftNavClick',	'topNavClick',	'RightBurgerMenuOpen',	'LeftBurgerMenuOpen') and os='Android' 
group by 1,2
order by 3 desc
limit 1000


CREATE TABLE dev.bag_list
(
	 brand				 varchar(255),
   article_type  varchar(255),
   gender        varchar(100)
) diststyle even;

copy dev.bag_list
(
brand,
article_type,
gender
)
from 's3://testashutosh/backups/b_a_g_list.csv'
credentials 'aws_access_key_id=AKIAILAAXNRLGZ3GKS2A;aws_secret_access_key=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'
delimiter ',' 
emptyasnull
blanksasnull
TRIMBLANKS
TRUNCATECOLUMNS
ACCEPTANYDATE
ACCEPTINVCHARS AS ' '
maxerror 10 explicit_ids
compupdate off statupdate off;


select * from stv_inflight;

select * from dev.brand_seller_map where brand='Harley-Davidson';

delete from dev.brand_seller_map where brand='Harley-Davidson' and seller_id not in (29)

select dp.article_type ,dp.gender ,
sum(inventory_count-nvl(blocked_order_count,0)) as saleable_inventory 
from o_atp_inventory o 
join dim_product dp on o.sku_id=dp.sku_id
where dp.brand ilike '%Moda Rapido%'
group by 1,2;


create table dev.pm_cross_sell_dump distkey(idcustomer) sortkey(order_created_date) as 
SELECT idcustomer,
       dci.gender,
       order_created_date,
       datediff(d,TO_DATE(first_login_date,'YYYYMMDD'),TO_DATE(order_created_date,'YYYYMMDD')) AS days_since_reg,
       ROW_NUMBER() OVER (PARTITION BY idcustomer ORDER BY order_created_date) AS purchase_seq,
       count(distinct article_type) as at_count,
       sum(revenue) as rev,
       sum(quantity) as qty,
       sum(nvl(product_discount,0)+nvl(coupon_discount,0)) as disc 
       FROM dev.orders_all fo
       JOIN dim_customer_idea dci ON fo.idcustomer = dci.id and first_login_date<=order_created_date
       WHERE first_login_date >= 20150101 
       group by 1,2,3,4;
       

select counT(distinct idcustomer) from dev.orders_all;

select a.*,b.bag_discount,a.discount/nullif(b.bag_discount,0) as disc_index  from
(select style_id,nvl(discount_rule_percentage,0)::float/100 as discount 
from fact_category_over_view_metrics 
where brand='Roadster' and article_type ='Tshirts' and product_gender='Men' and date=20170601) a,
(select sum(product_discount)/sum(total_mrp) as bag_discount 
from fact_category_over_view_metrics 
where brand='Roadster' and article_type ='Tshirts' and product_gender='Men' and date=20170601) b
limit 100;


SELECT idcustomer,
       dci.gender,
       order_created_date,
       datediff(d,TO_DATE(first_login_date,'YYYYMMDD'),TO_DATE(order_created_date,'YYYYMMDD')) AS days_since_reg,
       ROW_NUMBER() OVER (PARTITION BY idcustomer ORDER BY order_created_date) AS purchase_seq,
       count(distinct article_type) as at_count,
       sum(revenue) as rev,
       sum(quantity) as qty,
       sum(nvl(product_discount,0)+nvl(coupon_discount,0)) as disc 
       FROM dev.orders_all fo
       JOIN dim_customer_idea dci ON fo.idcustomer = dci.id and first_login_date<=order_created_date
       WHERE first_login_date >= 20150101 
       group by 1,2,3,4;
       
select * from stl_query where querytxt like '%pm_attriters%';


create table dev.pm_attriters distkey(uidx) sortkey(order_created_date) as 
select txn.*,case when nvl(rep.orders,0)>0 then 1 else 0 end as rep_cus_flag,ful.source_of_first_install 
from 
(select distinct dci.customer_login as uidx, fci.idcustomer, fci.order_created_date,dl.city_group,is_rto,is_returned, 
case when device_channel='mobile-app' then os_info else device_channel end as channel,brand,article_type, 
sum(item_revenue_inc_cashback) as rev,sum(quantity) as units_sold, sum(nvl(product_discount,0)+nvl(coupon_discount,0)) as disc 
from fact_core_item fci 
join dim_customer_idea dci on fci.idcustomer = dci.id 
join dim_location dl on fci.idlocation=dl.id 
where order_created_date between 20161001 and 20170725 and is_first_order = 'Y' and store_id = 1 
and device_channel = 'mobile-app' and (is_shipped = 1 or is_realised = 1) 
group by 1,2,3,4,5,6,7,8,9) txn 
left join 
(select idcustomer, count(distinct order_group_id) as orders 
from fact_core_item 
where order_created_date between 20161001 and 20170725 and is_first_order = 'N' and store_id = 1 and (is_shipped = 1 or is_realised = 1) 
group by 1) rep on txn.idcustomer = rep.idcustomer 
left join fact_user_lifetime ful on txn.uidx=ful.user_id and ful.user_id_identifier='uidx'    ;



select rep_orders,
count(distinct uidx), 
sum(list_viewed) as list_viewed,
sum(pdp_viewed) as pdp_viewed,
sum(atced) as atced,
sum(wishlisted) as wishlisted,
sum(search_fired) as search_fired,
sum(mwk_clicked) as mwk_clicked,
sum(bm_clicked) as bm_clicked,
sum(hpcards_clicked) as hpcards_clicked

from

(select txn.uidx, txn.order_created_date, 
case when max(rep.orders) > 0 then 1 else 0 end as rep_orders,
case when sum(da.lp) > 0 then 1 else 0 end as list_viewed,
case when sum(da.pdp) > 0 then 1 else 0 end as pdp_viewed,
case when sum(da.atc) > 0 then 1 else 0 end as atced,
case when sum(da.wishlist) > 0 then 1 else 0 end as wishlisted,
case when sum(da.search) > 0 then 1 else 0 end as search_fired,
case when sum(da.mwk) > 0 then 1 else 0 end as mwk_clicked,
case when sum(da.bm) > 0 then 1 else 0 end as bm_clicked,
case when sum(da.cards) > 0 then 1 else 0 end as hpcards_clicked

from

              (select distinct  dci.customer_login as uidx, fci.idcustomer, fci.order_created_date,dl.city_group
              from fact_core_item fci
              join dim_customer_idea dci on fci.idcustomer = dci.id
              join dim_location dl on fci.idlocation=dl.id
              where order_created_date between 20161001 and 20170725
              and is_first_order = 'Y'
              and store_id = 1 
              and device_channel = 'mobile-app'
              and (is_shipped = 1 or is_realised = 1)) txn

left join 

           (select idcustomer, count(distinct order_group_id) as orders
           from fact_core_item
            where order_created_date between 20161001 and 20170725
            and is_first_order = 'N'
            and store_id = 1            
            and (is_shipped = 1 or is_realised = 1)
            group by 1) rep
            
            on txn.idcustomer = rep.idcustomer

left join 
          (select load_date, uidx, sum(sessions) as sessions, 
          sum(lp_views) as lp, 
          sum(all_pdp_views) as pdp, 
          sum(added_to_cart) as atc,
          sum(mwko_clicks) as mwk,
          sum(burger_menu_clicks) as bm,
          sum(searches_fired) as search,
          sum(add_to_list + add_to_collection) as wishlist,
          sum(cards_clicked) as cards
          from daily_aggregates
          where load_date >= 20161001
          group by 1,2) da
          on txn.uidx = da.uidx
          and da.load_date > txn.order_created_date
          


group by 1,2
)
group by 1   
;



select a.date,a.brand,a.article_type,product_gender as gender,
sum(sold_quantity) as qty_sold,
sum(atp_net_inv_qty) as inventory,
sum(live_styles) as live_styles,
sum(case when is_live_style=1 then fresh_styles else 0 end) as fresh_styles,
sum(broken_styles) as broken_styles,
sum(list_count) as list_count,
sum(product_discount)/nullif(sum(total_mrp),0) as output_td
from customer_insights.fact_category_over_view_metrics a
join dev.bag_list b on a.brand=b.brand and a.article_type=b.article_type and a.product_gender=b.gender
where date >= 20160601
group by 1,2,3,4;

select date,brand,article_type,gender,
sum(ros_15days) as last_15_ros,
sum(ros_15days*nvl(discount_rule_percentage,0))/nullif(sum(ros_15days),0) as wgt_input_td,
avg(case when is_live_style=1 then nvl(discount_rule_percentage,0) end) as input_td 
from
(select date,brand,article_type,product_gender as gender,style_id,sold_quantity,is_live_style,
discount_rule_percentage,
avg(nvl(sold_quantity,0)) over (partition by style_id order by date  rows between 15 preceding and 1 preceding) as ros_15days
from fact_category_over_view_metrics
where date>=20160601)
group by 1,2,3,4
limit 1000;


select screen_name,count(1)
from clickstream.events_2017_11_22 
where screen_name ilike '%list%'
group by 1
order by 2 desc 
limit 1000;

select os,event_type,count(1)
from clickstream.events_view 
where event_type  in ('NBRLoad','NBRClick','NBRViewAll') and load_date between 20171116 and 20171122
group by 1,2
order by 3 desc 
limit 1000;


drop table if exists dev.pm_temp1;
create table dev.pm_temp1 distkey(hour_min) as 
SELECT device_id,
                     hour_min,
                     page
              FROM (SELECT device_id,
                           DATE_TRUNC('mins',(TIMESTAMP 'epoch' + atlas_ts / 1000*INTERVAL '1 Second ')) AS hour_min,
                           CASE
                             WHEN event_type = 'ScreenLoad' AND screen_name IN ('/feed','/stream') THEN 'hp_view'
                             WHEN event_type = 'ScreenLoad' AND screen_name ilike 'Landing Page%' THEN 'landing_page_view'
                             WHEN event_type = 'ScreenLoad' AND screen_name ilike 'Shopping Page-List Page%' OR screen_name LIKE 'Shopping Page-Search Result Page%' OR screen_name = 'Filter' THEN 'list_view'
                             WHEN event_type = 'ScreenLoad' AND screen_name ilike 'Shopping Page-PDP%' OR screen_name ilike '%FullScreenPDPImagesActivity%' THEN 'pdp_view'
                             WHEN event_type = 'ScreenLoad' AND screen_name ilike '%Checkout%Cart%' THEN 'cart_views'
                             WHEN event_type = 'ScreenLoad' AND screen_name ilike '%Checkout%address%' THEN 'address_view'
                             WHEN event_type = 'ScreenLoad' AND screen_name ilike '%Checkout%payment%' THEN 'payment_view'
                             ELSE 'others'
                           END AS page,
                           MAX(atlas_ts) AS atlas_ts,
                           ROW_NUMBER() OVER (PARTITION BY device_id,hour_min ORDER BY MAX(atlas_ts) DESC) AS rnk
                    FROM clickstream.events_2017_11_17
                    WHERE event_type = 'ScreenLoad'
                    GROUP BY 1,
                             2,
                             3)
              WHERE rnk = 1;
              

drop table if exists dev.pm_temp2;
create table dev.pm_temp2 distkey(hour_min) as 
(SELECT distinct DATE_TRUNC('mins',(TIMESTAMP 'epoch' + atlas_ts / 1000*INTERVAL '1 Second ')) AS hour_min
            FROM clickstream.events_2017_11_17
            WHERE event_type = 'ScreenLoad');



SELECT hour_min,
       page,
       COUNT(DISTINCT device_id) AS devices
FROM (SELECT a.hour_min,
             device_id,
             b.page,
             ROW_NUMBER() OVER (PARTITION BY a.hour_min,device_id ORDER BY b.hour_min) AS rnk
      FROM dev.pm_temp2 a
        JOIN dev.pm_temp1 b
          ON datediff(mins,a.hour_min,b.hour_min) BETWEEN 1
         AND 5)
         where rnk=1
         group by 1,2;
         

SELECT *
             FROM (SELECT DISTINCT user_id as uidx,
                          article_type,
                          SUM(pdp_view) as pdp,
                          SUM(liked) as liked,
                          SUM(nvl(added_to_collection,0)+nvl(addtolist,0)) as atcol,
                          SUM(addtocart) atc,
                          sum(units_bought) as units_bought,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(nvl(added_to_collection,0)+nvl(addtolist,0)) +10 *SUM(addtocart) +25*sum(units_bought) AS score,
                          ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime
                   WHERE identifier='uidx' and uidx in 
('b7b37e0f.e1fc.413c.90fa.53ffa0591d788LoAFnmCPA',
'c35cf501.9fc9.4daa.b5a8.d230b5d0f8400mT1ipWOw1',
'ae386adc.ff23.4b3d.a585.a438f57e260aRLXoZcM8o4',
'200b104e.4f4a.4ae2.a41b.82258f60d5d9xpuyslV2GF',
'15512088.9fd6.4b2e.ae21.e8d69cb3cb5fFN1DDM0N9J',
'608eb10e.7e78.4230.9947.ead7d4d35755iEcfqYqukW',
'c080e759.9baf.4cc7.92b3.20ee6adccf8cqXbpbTdIdJ',
'd668389e.668d.4bc3.bf59.b96222e1b235zqw1zpMWvU',
'bdccf53b.0a0a.44ea.a024.16692c5aaf93qJhdBCZQne',
'05027592.f51d.4af3.a2e9.b13da990455by2EdgdIqU5',
'd7c37afb.7337.497d.b9ae.b19ded77e6f2qR4tJZAUEx',
'cc8d9fd3.ea35.4186.b326.cff1006f354fEccrj34LBf',
'f676fae9.4b91.4873.a7cb.ecf5d8081eff0XEXWCOBGZ',
'83c34f37.8357.4cf0.92ad.9e6b6ab7545cGpLvzMMEa4',
'd0ba2443.a59d.4fd0.bb02.8f312d3cd1f48RMiTVkeMC',
'44b4613c.d556.4cd8.b765.25ae68087b6ayxSnsbyVjT',
'5526ea62.b45b.488b.af03.2f63e2a57fce9kR9bVqysE',
'fe47a3b8.6b3a.439e.b374.23a4057184cfEGafKxMaSb',
'9ac74a25.2ec9.497b.81c2.c71d9beaf840uVwk5vOiZA',
'926b7e67.fe1a.4f52.9990.f890bd84542fGU37Ss4n3Y',
'11592c09.cbd5.4014.8893.d99905f3ab26uPs59nXtq4',
'c00198c2.bb59.4d44.9c38.30171c425821C8MGTguNY8',
'7313ea8e.376f.481b.b047.babe1402c4c55t5vHiaFh8',
'46c55da8.8124.4826.9332.f1b903d555bfs16m5rIDSF',
'45fc20ca.5bf4.4011.aa41.423a7550a3aeOAY1tH9Ffl',
'18a632ca.8d88.4cae.bc0f.7f11c31d8911G1dzH5L1s7')
                   GROUP BY 1,
                            2)
             WHERE rnk <= 5;
             


select from_unixtime(batch_min/1000) as hour_min, * from madlytics.rt_user_page_counters;


drop table if exists dev.pm_temp1;
create table dev.pm_temp1 distkey(hour_min) as 
SELECT device_id,
                     hour_min,
                     page
              FROM (SELECT device_id,
                           DATE_TRUNC('mins',(TIMESTAMP 'epoch' + atlas_ts / 1000*INTERVAL '1 Second ')) AS hour_min,
                           CASE
                             WHEN event_type = 'ScreenLoad' AND screen_name IN ('/feed','/stream') THEN 'hp_view'
                             WHEN event_type = 'ScreenLoad' AND screen_name ilike 'Landing Page%' THEN 'landing_page_view'
                             WHEN event_type = 'ScreenLoad' AND screen_name ilike 'Shopping Page-List Page%' OR screen_name LIKE 'Shopping Page-Search Result Page%' OR screen_name = 'Filter' THEN 'list_view'
                             WHEN event_type = 'ScreenLoad' AND screen_name ilike 'Shopping Page-PDP%' OR screen_name ilike '%FullScreenPDPImagesActivity%' THEN 'pdp_view'
                             WHEN event_type = 'ScreenLoad' AND screen_name ilike '%Checkout%Cart%' THEN 'cart_views'
                             WHEN event_type = 'ScreenLoad' AND screen_name ilike '%Checkout%address%' THEN 'address_view'
                             WHEN event_type = 'ScreenLoad' AND screen_name ilike '%Checkout%payment%' THEN 'payment_view'
                             WHEN event_type = 'ScreenLoad' AND screen_name ilike '%Checkout%confirm%' THEN 'orders_view'
                             ELSE 'others'
                           END AS page,
                           MAX(atlas_ts) AS atlas_ts,
                           ROW_NUMBER() OVER (PARTITION BY device_id,hour_min ORDER BY MAX(atlas_ts) DESC) AS rnk
                    FROM clickstream.events_2017_12_12
                    WHERE event_type = 'ScreenLoad'
                    GROUP BY 1,
                             2,
                             3)
              WHERE rnk = 1;
              

drop table if exists dev.pm_temp2;
create table dev.pm_temp2 distkey(hour_min) as 
(SELECT distinct DATE_TRUNC('mins',(TIMESTAMP 'epoch' + atlas_ts / 1000*INTERVAL '1 Second ')) AS hour_min
            FROM clickstream.events_2017_12_12
            WHERE event_type = 'ScreenLoad');



SELECT hour_min,
       page,
       COUNT(DISTINCT device_id) AS devices
FROM (SELECT a.hour_min,
             device_id,
             b.page,
             ROW_NUMBER() OVER (PARTITION BY a.hour_min,device_id ORDER BY b.hour_min) AS rnk
      FROM dev.pm_temp2 a
        JOIN dev.pm_temp1 b
          ON datediff(mins,b.hour_min,a.hour_min) BETWEEN 1
         AND 5)
         where rnk=1
         group by 1,2


SELECT email,
       eors_purchased_value
FROM (SELECT uidx,
             eors_purchased_value
      FROM (SELECT DISTINCT uidx
            FROM mfg_eors7
            WHERE group_is_active = 'True') mfg
        JOIN (SELECT customer_login,
                     SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0)) AS eors_purchased_value
              FROM bidb.fact_order_live
              WHERE (is_realised = 1 OR is_shipped = 1)
              AND   ((order_created_date = 20171221 AND order_created_time > 1855) OR (order_created_date BETWEEN 20171222 AND 20171225))
              GROUP BY 1) fci ON mfg.uidx = fci.customer_login) a
  JOIN (SELECT DISTINCT id, customer_login FROM dim_customer_idea) dci ON a.uidx = dci.customer_login
  JOIN (SELECT DISTINCT uid, email FROM cii.dim_customer_email) dce ON dci.id = dce.uid
ORDER BY 2 DESC LIMIT 200;



select brand||article_type||gender as bag from dev.bag_list ;

truncate table dev.bag_list;

insert into dev.bag_list
select brand,article_type,product_gender as gender,rnk from
(select article_type,brand,product_gender,sum(revenue), row_number() over (order by sum(revenue) desc) as rnk
from fact_category_over_view_metrics
where date>=to_char(sysdate - interval '365 days','YYYYMMDD')::bigint and article_type is not null and brand is not null and product_gender is not null
group by 1,2,3)
where rnk<=2000;

SELECT email,
       eors_purchased_value
FROM (SELECT mfg.uidx,
             eors_purchased_value
      FROM (SELECT DISTINCT uidx
            FROM mfg_eors7
            WHERE group_is_active = 'True') mfg
        JOIN (SELECT uidx,
                     SUM(eors_purchased_value) AS eors_purchased_value
              FROM ((SELECT dci.uidx,
                            SUM(item_revenue_inc_cashback) AS eors_purchased_value
                     FROM fact_core_item fci
                       JOIN (SELECT id, customer_login AS uidx FROM dim_customer_idea) dci ON fci.idcustomer = dci.id
                     WHERE (is_shipped = 1 OR is_realised = 1)
                     AND   store_id = 1
                     AND   ((order_created_date = 20171221 AND order_created_time > 185500) OR (order_created_date BETWEEN 20171222 AND TO_CHAR(SYSDATE -INTERVAL '2 day','YYYYMMDD')::BIGINT))
                     GROUP BY 1)
                     UNION ALL
                     (SELECT customer_login AS uidx,
                            SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0)) AS eors_purchased_value
                     FROM bidb.fact_order_live
                     WHERE (is_realised = 1 OR is_shipped = 1)
                     AND   order_created_date BETWEEN TO_CHAR(SYSDATE -INTERVAL '1 day','YYYYMMDD')::BIGINT AND TO_CHAR(SYSDATE,'YYYYMMDD')::BIGINT
                     GROUP BY 1))
              GROUP BY 1) fci ON mfg.uidx = fci.uidx) a
  JOIN (SELECT DISTINCT id, customer_login FROM dim_customer_idea) dci ON a.uidx = dci.customer_login
  JOIN (SELECT DISTINCT uid, email FROM cii.dim_customer_email) dce ON dci.id = dce.uid
ORDER BY 2 DESC LIMIT 200;


select load_date,count(distinct uidx),count(distinct uidx||data_set_value) as items 
from clickstream.events_view
where event_type = 'AddToCollection' and load_date >=20171214 and uidx is not null
group by 1 ;


select load_date,to_char(load_datetime,'HH24') as hour,os,count(distinct device_id) as users,count(distinct session_id) as sessions
from clickstream.traffic_realtime_archive 
where load_date in (20170101,20170102,20170622,20170623) group by 1,2,3
union all
select load_date,to_char(load_datetime,'HH24') as hour,os,count(distinct device_id) as users,count(distinct session_id) as sessions
from clickstream.traffic_realtime
where load_date in (20171220,20171221) group by 1,2,3;

select email,customer_Login
from dim_customer_idea dci
join dim_customer_email dce on dci.id=dce.uid
where email in ('navdeep.p@myntra.com','sudha.prabhu@myntra.com')


select z.*,y.state_code from
(select nvl(t.load_date,r.order_created_date) as date,nvl(t.state_name,r.state) as state_name, nvl(t.channel,r.channel) as channel,nvl(sessions,0) as sessions,nvl(users,0) as users,
nvl(revenue,0) as revenue,nvl(orders,0) as orders,nvl(customers,0) as customers
from
(select load_date,channel,UPPER(nvl(state_name,'Unknown')) as state_name,sum(sessions) as sessions,sum(users) as users from
(select load_date::bigint,CASE WHEN app_name = 'MyntraRetailWeb' AND lower(device_category) = 'mobile' THEN 'Mweb' WHEN app_name = 'MyntraRetailWeb' AND lower(device_category) = 'desktop' THEN 'Desktop'
when app_name in ('Myntra','MyntraRetailAndroid') then os END AS channel,
nvl(city,'Unknown') as city,count(distinct session_id) as sessions, count(distinct device_id) as users
from clickstream.sessionhour_view  
where load_date::bigint>=to_char(sysdate - interval '2 day','YYYYMMDD')::bigint
group by 1,2,3) a
left join customer_insights.city_state_mapping b on lower(a.city)=lower(b.city)
 group by 1,2,3)t
full outer join 
(select order_created_date,
case when device_channel='web' then 'Desktop' when device_channel='mobile-web' then 'Mweb' else os_info end as channel,
upper(nvl(case when statename='ORISSA' then 'ODISHA' 
when statename='ANDAMAN & NICOBAR ISLANDS' then 'ANDAMAN and NICOBAR ISLANDS' 
when statename='PONDICHERRY' then 'Puducherry'
else statename end,'Unknown')) as state,
sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as revenue,
count(distinct order_group_id) as orders,count(distinct idcustomer) as customers
from fact_core_item fci
left join dim_location dl on fci.idlocation=dl.id
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date >=to_char(sysdate - interval '2 day','YYYYMMDD')::bigint
group by 1,2,3) r on upper(t.state_name)=upper(r.state) and t.load_date=r.order_created_date and t.channel=r.channel) z
left join (select distinct state_name,state_code from customer_insights.city_state_mapping) y on UPPER(z.state_name)=UPPER(y.state_name)
limit 100;


SELECT 	   to_char(sysdate,'YYYYMMDD')::bigint AS DATE,
       to_char(sysdate,'WW') AS week,
       trim(upper(to_char(sysdate,'MONTH')))  AS month,
       to_char(sysdate,'YYYY') AS YEAR_MONTH,
       i.style_id,
       SUM(inv) AS inv,
       SUM(nvl (disc,0)) AS disc,
       SUM(nvl (mrp,0)) AS mrp,
       SUM(nvl (qty,0)) AS qty
FROM (SELECT a.style_id,
             inv
      FROM dev.atp_snapshot_20171221 a
	 left JOIN dim_style ds on a.style_id=ds.style_id
	 where ds.article_type!='E-Gift Cards') i
  LEFT JOIN (SELECT style_id,
                    SUM(disc)  as disc,
                    SUM(mrp) AS mrp,
                    SUM(qty) AS qty
             FROM (SELECT dp.style_id,
                          SUM(product_discount + coupon_discount) AS disc,
                          SUM(item_mrp_value*quantity) AS mrp,
                          SUM(quantity) AS qty
                   FROM fact_core_item fci
                     JOIN dim_product dp ON fci.sku_id = dp.sku_id
                   WHERE cast(order_created_date ||' '|| lpad(order_created_time,6,0) as timestamp) > '2017-12-21 18:40:00' 
                   AND order_created_date<=TO_CHAR(SYSDATE -INTERVAL '1 day','YYYYMMDD')::bigint
				   AND order_created_date<=20170924
                   AND   (is_shipped = 1 OR is_realised = 1)
                   and 		store_id=1
                   GROUP BY 1
                   UNION
                   SELECT dp.style_id,
                          SUM(trade_discount + coupon_discount) AS disc,
                          SUM(item_mrp*item_quantity) AS mrp,
                          SUM(item_quantity) AS qty
                   FROM fact_order_live fol
                     JOIN dim_product dp ON fol.sku_id = dp.sku_id
                   WHERE (is_shipped = 1 OR is_realised = 1)
                   and order_created_date=TO_CHAR(SYSDATE,'YYYYMMDD')::bigint
				   and cast(order_created_date ||' '|| lpad(order_created_time,4,0) as timestamp) > '2017-12-21 18:40:00'
				   AND order_created_date<=20170924
                   GROUP BY 1)
                   group by 1) s ON i.style_id = s.style_id
  LEFT JOIN dim_style dp ON i.style_id = dp.style_id
GROUP BY 1,
         2,
         3,
         4,
         5;
         
select load_date,
			 UPPER(nvl(c.city,'Unknown')) as city,
			 UPPER(nvl(state_name,'Unknown')) as state_name,
			 a.channel,
       sum(sessions) AS sessions,
       COUNT(DISTINCT a.user_id) AS users,
       COUNT(DISTINCT b.user_id) AS customers,
       sum(orders) as orders,
       sum(revenue) as revenue
from
(select load_date,channel,user_id,max(case when rnk=1 then city end) as city,count(distinct session_id) as sessions from
(SELECT load_date,
       CASE
         WHEN app_name = 'MyntraRetailWeb' AND lower(device_category) = 'mobile' THEN 'Mweb'
         WHEN app_name = 'MyntraRetailWeb' AND lower(device_category) = 'desktop' THEN 'Desktop'
         WHEN app_name IN ('Myntra','MyntraRetailAndroid') THEN os
       END AS channel,
       upper(nvl(city,'Unknown')) AS city,
       case when app_name = 'MyntraRetailWeb' then nvl(uidx,device_id) else device_id end as user_id,
       session_id,
       sum(screenloads_final) as screens,
       row_number() over (partition by load_date,channel,user_id,session_id order by screens desc) as rnk
FROM clickstream.sessionhour_view
WHERE load_date >= TO_CHAR(sysdate -INTERVAL '2 day','YYYYMMDD')::BIGINT
group by 1,2,3,4,5)
group by 1,2,3) a
left join 
(select order_created_date,case when device_channel in ('web','mobile-web') then customer_login else device_id end as user_id,
case when device_channel='web' then 'Desktop' when device_channel='mobile-web' then 'Mweb' else os_info end as channel,
sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as revenue,
count(distinct order_group_id) as orders
from fact_core_item fci
left join dim_customer_idea dci on fci.idcustomer=dci.id
left join dim_location dl on fci.idlocation=dl.id
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date >=to_char(sysdate - interval '2 day','YYYYMMDD')::bigint
group by 1,2,3) b on a.user_id=b.user_id and a.channel=b.channel and a.load_date=b.order_created_date
left join customer_insights.city_state_mapping c on lower(a.city)=lower(c.city) 
where a.channel not in ('Mweb','Desktop')
group by 1,2,3,4
limit 100;

select user_id,identifier,brand,rnk
from
(SELECT user_id,
             identifier,
       case when a.brand in ('Biba','Forever21','Roadster','Mast & Harbour','UCB','Nike','Daniel Klien','Vero Moda','Mango','HRX','Jack & Jones','Puma') then a.brand else b.affinity_brand end as brand,
       SUM(pdp_view) AS pdp,
       SUM(liked) AS liked,
       SUM(nvl (added_to_collection,0) + nvl (addtolist,0)) AS atcol,
       SUM(addtocart) atc,
       SUM(units_bought) AS units_bought,
       SUM(pdp_view) +5 *SUM(liked) +10 *SUM(nvl (added_to_collection,0) + nvl (addtolist,0)) +10 *SUM(addtocart) +25 *SUM(units_bought) AS score,
       ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY score DESC) AS rnk
FROM bidb.notif_person_base_lifetime a
left join customer_insights.brand_to_brand_affinity b on a.brand=b.brand and b.affinity_type='Purchase'
where a.brand in ('Biba','Forever21','Roadster','Mast & Harbour','UCB','Nike','Daniel Klien','Vero Moda','Mango','HRX','Jack & Jones','Puma')
or b.affinity_brand in ('Biba','Forever21','Roadster','Mast & Harbour','UCB','Nike','Daniel Klien','Vero Moda','Mango','HRX','Jack & Jones','Puma')
                   GROUP BY 1,2,3)
             WHERE rnk <= 5
             limit 100;


SELECT 
session_id,
						 event_type,
						 referrer_url,
						 split_part(referrer_url,'?',2) as string,
             utm_medium,
             utm_campaign,
             utm_source
--             ROW_NUMBER() OVER (PARTITION BY session_id ORDER BY client_ts ASC) AS rnk
      FROM clickstream.events_2017_12_26
      WHERE event_type IN ('appLaunch','push-notification','DeepLink')
      limit 100;
      
SELECT count(distinct session_id||referrer_url)
      FROM clickstream.events_view
      WHERE event_type IN ('appLaunch','push-notification','DeepLink','ScreenLoad') and load_date between 20171221 and 20171226 and split_part(referrer_url,'?',2) is not null;
      

unload(
'
select session_id,event_type,referrer_url FROM clickstream.events_view
      WHERE event_type IN (''appLaunch'',''push-notification'',''DeepLink'',''ScreenLoad'') and load_date between 20171221 and 20171226 and split_part(referrer_url,''?'',2) is not null '

)to 's3://testashutosh/backups/sesion_utm_data' 
credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
parallel on 
ALLOWOVERWRITE 
DELIMITER AS '\001'; 


drop table if exists dev.orders_all;
create table dev.orders_all distkey(idcustomer) sortkey(order_created_date) as 
select * from
(select idcustomer_idea as idcustomer, order_created_date,article_type,sum(item_revenue_inc_cashback) as revenue,
sum(quantity) as quantity,sum(product_discount) as product_discount ,sum(coupon_discount) as coupon_discount
from fact_orderitem foi
join dim_product dp on foi.sku_id=dp.sku_id
WHERE (is_shipped = 1 OR is_realised = 1)
AND   store_id = 1
and order_created_date<=20150331
group by 1,2,3
union all
select idcustomer, order_created_date,article_type,sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as revenue,
sum(quantity) as quantity,sum(product_discount) as product_discount,sum(coupon_discount) as coupon_discount
from fact_core_item 
WHERE (is_shipped = 1 OR is_realised = 1)
AND   store_id = 1
and order_created_date>20150331
group by 1,2,3)
