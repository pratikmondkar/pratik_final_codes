create table dev.searc_banner_tag_date distkey(style_id) sortkey(load_date) as 
(SELECT nvl(a.date,b.date,c.date,d.date,e.date,f.date,g.date) AS load_date,
			 nvl(a.style_id,b.style_id,c.style_id,d.style_id,e.style_id,f.style_id,g.style_id) AS style_id,
       nvl(a.source,b.source,c.source,d.source,e.source,f.source,g.source) AS source,
       nvl(bag_lp_cnt,0) as bag_lp_cnt,
       nvl(bag_style_cnt,0) as bag_style_cnt,
       nvl(ag_lp_cnt,0) as ag_lp_cnt,
       nvl(ag_style_cnt,0) as ag_style_cnt,
       nvl(bg_lp_cnt,0) as bg_lp_cnt,
       nvl(bg_style_cnt,0) as bg_style_cnt,
       nvl(ba_lp_cnt,0) as ba_lp_cnt,
       nvl(ba_style_cnt,0) as ba_style_cnt,
       nvl(b_lp_cnt,0) as b_lp_cnt,
       nvl(b_style_cnt,0) as b_style_cnt,
       nvl(a_lp_cnt,0) as a_lp_cnt,
       nvl(a_style_cnt,0) as a_style_cnt,
       nvl(g_lp_cnt,0) as g_lp_cnt,
       nvl(g_style_cnt,0) as g_style_cnt
FROM (SELECT a.date,
						 a.style_id,
             b1.source,
             b1.bag_lp_cnt,
             b2.bag_style_cnt
      FROM (SELECT DISTINCT fps.date,
      						 fps.style_id,
                   brand,
                   article_type,
                   gender
            FROM dev.fact_category_over_view_metrics fps
              JOIN dim_style dp ON fps.style_id = dp.style_id
            WHERE is_live_style = 1) a
        JOIN (SELECT load_date,
        						 brand,
                     article_type,
                     gender,
                     source,
                     COUNT(*) AS bag_lp_cnt
              FROM dev.lp_tag_by_source
              WHERE final_segment = 'BAG'
              GROUP BY 1,
                       2,
                       3,
                       4,
                       5) b1
          ON a.brand = b1.brand
         AND a.article_type = b1.article_type
         AND a.gender = b1.gender
         and a.date=b1.load_date
        LEFT JOIN (SELECT date,
        									brand,
                          article_type,
                          gender,
                          COUNT(DISTINCT dp.style_id) AS bag_style_cnt
                   FROM dev.fact_category_over_view_metrics fps
                     JOIN dim_style dp ON fps.style_id = dp.style_id
                   WHERE is_live_style = 1
                   GROUP BY 1,
                            2,
                            3,
                            4) b2
               ON a.brand = b2.brand
              AND a.article_type = b2.article_type
              AND a.gender = b2.gender
              and a.date = b2.date) a
  FULL OUTER JOIN (SELECT a.date,
  												a.style_id,
                          b1.source,
                          b1.ag_lp_cnt,
                          b2.ag_style_cnt
                   FROM (SELECT DISTINCT fps.date, 
                   							fps.style_id,
                                article_type,
                                gender
                         FROM dev.fact_category_over_view_metrics fps
                           JOIN dim_style dp ON fps.style_id = dp.style_id
                         WHERE is_live_style = 1) a
                     JOIN (SELECT load_date,
                     							article_type,
                                  gender,
                                  source,
                                  COUNT(*) AS ag_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE final_segment = 'AG'
                           GROUP BY 1,
                                    2,
                                    3,
                                    4) b1
                       ON a.article_type = b1.article_type
                      AND a.gender = b1.gender
                      and a.date = b1.load_date
                     LEFT JOIN (SELECT date,
                     									 article_type,
                                       gender,
                                       COUNT(DISTINCT dp.style_id) AS ag_style_cnt
                                FROM dev.fact_category_over_view_metrics fps
                                  JOIN dim_style dp ON fps.style_id = dp.style_id
                                WHERE is_live_style = 1
                                GROUP BY 1,
                                         2,
                                         3) b2
                            ON a.article_type = b2.article_type
                           AND a.gender = b2.gender
                           and a.date = b2.date) b on a.style_id=b.style_id and a.source=b.source and a.date=b.date
  FULL OUTER JOIN (SELECT a.style_id,
  												a.date,
                          b1.source,
                          b1.bg_lp_cnt,
                          b2.bg_style_cnt
                   FROM (SELECT DISTINCT fps.style_id,
                   							date,
                                brand,
                                gender
                         FROM dev.fact_category_over_view_metrics fps
                           JOIN dim_style dp ON fps.style_id = dp.style_id
                         WHERE is_live_style = 1) a
                     JOIN (SELECT load_date,
                     							brand,
                                  gender,
                                  source,
                                  COUNT(*) AS bg_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE final_segment = 'BG'
                           GROUP BY 1,
                                    2,
                                    3,
                                    4) b1
                       ON a.brand = b1.brand
                      AND a.gender = b1.gender
                      and a.date = b1.load_date
                     LEFT JOIN (SELECT date,
                     									 brand,
                                       gender,
                                       COUNT(DISTINCT dp.style_id) AS bg_style_cnt
                                FROM dev.fact_category_over_view_metrics fps
                                  JOIN dim_style dp ON fps.style_id = dp.style_id
                                WHERE is_live_style = 1
                                GROUP BY 1,
                                         2,
                                         3) b2
                            ON a.brand = b2.brand
                           AND a.gender = b2.gender
                           and a.date = b2.date) c on a.style_id=c.style_id and a.source=c.source and a.date=c.date
  FULL OUTER JOIN (SELECT a.style_id,
  												a.date,
                          b1.source,
                          b1.ba_lp_cnt,
                          b2.ba_style_cnt
                   FROM (SELECT DISTINCT fps.date, 
                   							fps.style_id,
                                brand,
                                article_type
                         FROM dev.fact_category_over_view_metrics fps
                           JOIN dim_style dp ON fps.style_id = dp.style_id
                         WHERE is_live_style = 1) a
                     JOIN (SELECT load_date,
                     							brand,
                                  article_type,
                                  source,
                                  COUNT(*) AS ba_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE load_date = 20170225
                           AND   final_segment = 'BA'
                           GROUP BY 1,
                                    2,
                                    3,
                                    4) b1
                       ON a.brand = b1.brand
                      AND a.article_type = b1.article_type
                      and a.date = b1.load_date
                     LEFT JOIN (SELECT date,
                     									 brand,
                                       article_type,
                                       COUNT(DISTINCT dp.style_id) AS ba_style_cnt
                                FROM dev.fact_category_over_view_metrics fps
                                  JOIN dim_style dp ON fps.style_id = dp.style_id
                                WHERE is_live_style = 1
                                GROUP BY 1,
                                         2,
                                         3) b2
                            ON a.brand = b2.brand
                           AND a.article_type = b2.article_type
                           and a.date = b2.date) d on a.style_id=d.style_id and a.source=d.source and a.date=d.date
  FULL OUTER JOIN (SELECT a.style_id,
  												a.date,
                          b1.source,
                          b1.b_lp_cnt,
                          b2.b_style_cnt
                   FROM (SELECT DISTINCT fps.style_id,
                   							fps.date,
                                brand
                         FROM dev.fact_category_over_view_metrics fps
                           JOIN dim_style dp ON fps.style_id = dp.style_id
                         WHERE is_live_style = 1) a
                     JOIN (SELECT load_date,
                     							brand,
                                  source,
                                  COUNT(*) AS b_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE final_segment = 'B'
                           GROUP BY 1,
                                    2,
                                    3) b1 ON a.brand = b1.brand and a.date = b1.load_date
                     LEFT JOIN (SELECT date,
                     									 brand,
                                       COUNT(DISTINCT dp.style_id) AS b_style_cnt
                                FROM dev.fact_category_over_view_metrics fps
                                  JOIN dim_style dp ON fps.style_id = dp.style_id
                                WHERE is_live_style = 1
                                GROUP BY 1,2) b2 ON a.brand = b2.brand and a.date = b2.date) e on a.style_id=e.style_id and a.source=e.source and a.date=e.date
  FULL OUTER JOIN (SELECT a.style_id,
  												a.date,
                          b1.source,
                          b1.a_lp_cnt,
                          b2.a_style_cnt
                   FROM (SELECT DISTINCT fps.style_id,
                   							fps.date,
                                article_type
                                FROM dev.fact_category_over_view_metrics fps
                           JOIN dim_style dp ON fps.style_id = dp.style_id
                         WHERE is_live_style = 1) a
                     JOIN (SELECT load_date,
                     							article_type,
                                  source,
                                  COUNT(*) AS a_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE final_segment = 'A'
                           GROUP BY 1,
                                    2,
                                    3) b1 ON a.article_type = b1.article_type and a.date = b1.load_date
                     LEFT JOIN (SELECT date,
                     									 article_type,
                                       COUNT(DISTINCT dp.style_id) AS a_style_cnt
                                FROM dev.fact_category_over_view_metrics fps
                                  JOIN dim_style dp ON fps.style_id = dp.style_id
                                WHERE is_live_style = 1
                                GROUP BY 1,2) b2 ON a.article_type = b2.article_type and a.date = b2.date) f on a.style_id=f.style_id and a.source=f.source and a.date=f.date
  FULL OUTER JOIN (SELECT a.style_id,
  												a.date,
                          b1.source,
                          b1.g_lp_cnt,
                          b2.g_style_cnt
                   FROM (SELECT DISTINCT fps.style_id,
                   							fps.date,
                                gender
                         FROM dev.fact_category_over_view_metrics fps
                           JOIN dim_style dp ON fps.style_id = dp.style_id
                         WHERE is_live_style = 1) a
                     JOIN (SELECT load_date,
                     							gender,
                                  source,
                                  COUNT(*) AS g_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE final_segment = 'G'
                           GROUP BY 1,
                                    2,
                                    3) b1 ON a.gender = b1.gender and a.date = b1.load_date
                     LEFT JOIN (SELECT date,
                     									 gender,
                                       COUNT(DISTINCT dp.style_id) AS g_style_cnt
                                FROM dev.fact_category_over_view_metrics fps
                                  JOIN dim_style dp ON fps.style_id = dp.style_id
                                WHERE is_live_style = 1
                                GROUP BY 1,2) b2
                           on a.gender = b2.gender
                           and a.date = b2.date) g on a.style_id=g.style_id and a.source=g.source and a.date=g.date
);

/*
create table dev.search_banner_tags_final distkey(style_id) sortkey(load_date) as
(select load_date,style_id,source,
nvl(bag_lp_cnt/nullif(bag_style_cnt,0),0)+
nvl(ag_lp_cnt/nullif(ag_style_cnt,0),0)+
nvl(bg_lp_cnt/nullif(bg_style_cnt,0),0)+
nvl(ba_lp_cnt/nullif(ba_style_cnt,0),0)+
nvl(b_lp_cnt/nullif(b_style_cnt,0),0)+
nvl(a_lp_cnt/nullif(a_style_cnt,0),0)+
nvl(g_lp_cnt/nullif(g_style_cnt,0),0) as tag_cnt
from dev.searc_banner_tag_date)
*/

select * from dev.searc_banner_tag_date limit 1000
