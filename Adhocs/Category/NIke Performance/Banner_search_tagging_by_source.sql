SELECT nvl(a.style_id,b.style_id,c.style_id,d.style_id,e.style_id,f.style_id,g.style_id) AS style_id,
       nvl(a.source,b.source,c.source,d.source,e.source,f.source,g.source) AS source,
       bag_lp_cnt,
       bag_style_cnt,
       ag_lp_cnt,
       ag_style_cnt,
       bg_lp_cnt,
       bg_style_cnt,
       ba_lp_cnt,
       ba_style_cnt,
       b_lp_cnt,
       b_style_cnt,
       a_lp_cnt,
       a_style_cnt,
       g_lp_cnt,
       g_style_cnt
FROM (SELECT a.style_id,
             b1.source,
             b1.bag_lp_cnt,
             b2.bag_style_cnt
      FROM (SELECT DISTINCT fps.style_id,
                   brand,
                   article_type,
                   gender
            FROM fact_product_snapshot fps
              JOIN dim_product dp ON fps.sku_id = dp.sku_id
            WHERE is_live_on_portal = 1) a
        JOIN (SELECT brand,
                     article_type,
                     gender,
                     source,
                     COUNT(*) AS bag_lp_cnt
              FROM dev.lp_tag_by_source
              WHERE load_date = 20170225
              AND   final_segment = 'BAG'
              GROUP BY 1,
                       2,
                       3,
                       4) b1
          ON a.brand = b1.brand
         AND a.article_type = b1.article_type
         AND a.gender = b1.gender
        LEFT JOIN (SELECT brand,
                          article_type,
                          gender,
                          COUNT(DISTINCT dp.style_id) AS bag_style_cnt
                   FROM fact_product_snapshot fps
                     JOIN dim_product dp ON fps.sku_id = dp.sku_id
                   WHERE is_live_on_portal = 1
                   GROUP BY 1,
                            2,
                            3) b2
               ON a.brand = b2.brand
              AND a.article_type = b2.article_type
              AND a.gender = b2.gender) a
  FULL OUTER JOIN (SELECT a.style_id,
                          b1.source,
                          b1.ag_lp_cnt,
                          b2.ag_style_cnt
                   FROM (SELECT DISTINCT fps.style_id,
                                article_type,
                                gender
                         FROM fact_product_snapshot fps
                           JOIN dim_product dp ON fps.sku_id = dp.sku_id
                         WHERE is_live_on_portal = 1) a
                     JOIN (SELECT article_type,
                                  gender,
                                  source,
                                  COUNT(*) AS ag_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE load_date = 20170225
                           AND   final_segment = 'AG'
                           GROUP BY 1,
                                    2,
                                    3) b1
                       ON a.article_type = b1.article_type
                      AND a.gender = b1.gender
                     LEFT JOIN (SELECT article_type,
                                       gender,
                                       COUNT(DISTINCT dp.style_id) AS ag_style_cnt
                                FROM fact_product_snapshot fps
                                  JOIN dim_product dp ON fps.sku_id = dp.sku_id
                                WHERE is_live_on_portal = 1
                                GROUP BY 1,
                                         2) b2
                            ON a.article_type = b2.article_type
                           AND a.gender = b2.gender) b on a.style_id=b.style_id and a.source=b.source
  FULL OUTER JOIN (SELECT a.style_id,
                          b1.source,
                          b1.bg_lp_cnt,
                          b2.bg_style_cnt
                   FROM (SELECT DISTINCT fps.style_id,
                                brand,
                                gender
                         FROM fact_product_snapshot fps
                           JOIN dim_product dp ON fps.sku_id = dp.sku_id
                         WHERE is_live_on_portal = 1) a
                     JOIN (SELECT brand,
                                  gender,
                                  source,
                                  COUNT(*) AS bg_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE load_date = 20170225
                           AND   final_segment = 'BG'
                           GROUP BY 1,
                                    2,
                                    3) b1
                       ON a.brand = b1.brand
                      AND a.gender = b1.gender
                     LEFT JOIN (SELECT brand,
                                       gender,
                                       COUNT(DISTINCT dp.style_id) AS bg_style_cnt
                                FROM fact_product_snapshot fps
                                  JOIN dim_product dp ON fps.sku_id = dp.sku_id
                                WHERE is_live_on_portal = 1
                                GROUP BY 1,
                                         2) b2
                            ON a.brand = b2.brand
                           AND a.gender = b2.gender) c on a.style_id=c.style_id and a.source=c.source
  FULL OUTER JOIN (SELECT a.style_id,
                          b1.source,
                          b1.ba_lp_cnt,
                          b2.ba_style_cnt
                   FROM (SELECT DISTINCT fps.style_id,
                                brand,
                                article_type
                         FROM fact_product_snapshot fps
                           JOIN dim_product dp ON fps.sku_id = dp.sku_id
                         WHERE is_live_on_portal = 1) a
                     JOIN (SELECT brand,
                                  article_type,
                                  source,
                                  COUNT(*) AS ba_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE load_date = 20170225
                           AND   final_segment = 'BA'
                           GROUP BY 1,
                                    2,
                                    3) b1
                       ON a.brand = b1.brand
                      AND a.article_type = b1.article_type
                     LEFT JOIN (SELECT brand,
                                       article_type,
                                       COUNT(DISTINCT dp.style_id) AS ba_style_cnt
                                FROM fact_product_snapshot fps
                                  JOIN dim_product dp ON fps.sku_id = dp.sku_id
                                WHERE is_live_on_portal = 1
                                GROUP BY 1,
                                         2) b2
                            ON a.brand = b2.brand
                           AND a.article_type = b2.article_type) d on a.style_id=d.style_id and a.source=d.source
  FULL OUTER JOIN (SELECT a.style_id,
                          b1.source,
                          b1.b_lp_cnt,
                          b2.b_style_cnt
                   FROM (SELECT DISTINCT fps.style_id,
                                brand
                         FROM fact_product_snapshot fps
                           JOIN dim_product dp ON fps.sku_id = dp.sku_id
                         WHERE is_live_on_portal = 1) a
                     JOIN (SELECT brand,
                                  source,
                                  COUNT(*) AS b_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE load_date = 20170225
                           AND   final_segment = 'B'
                           GROUP BY 1,
                                    2) b1 ON a.brand = b1.brand
                     LEFT JOIN (SELECT brand,
                                       COUNT(DISTINCT dp.style_id) AS b_style_cnt
                                FROM fact_product_snapshot fps
                                  JOIN dim_product dp ON fps.sku_id = dp.sku_id
                                WHERE is_live_on_portal = 1
                                GROUP BY 1) b2 ON a.brand = b2.brand) e on a.style_id=e.style_id and a.source=e.source
  FULL OUTER JOIN (SELECT a.style_id,
                          b1.source,
                          b1.a_lp_cnt,
                          b2.a_style_cnt
                   FROM (SELECT DISTINCT fps.style_id,
                                article_type
                                FROM fact_product_snapshot fps
                           JOIN dim_product dp ON fps.sku_id = dp.sku_id
                         WHERE is_live_on_portal = 1) a
                     JOIN (SELECT article_type,
                                  source,
                                  COUNT(*) AS a_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE load_date = 20170225
                           AND   final_segment = 'A'
                           GROUP BY 1,
                                    2) b1 ON a.article_type = b1.article_type
                     LEFT JOIN (SELECT article_type,
                                       COUNT(DISTINCT dp.style_id) AS a_style_cnt
                                FROM fact_product_snapshot fps
                                  JOIN dim_product dp ON fps.sku_id = dp.sku_id
                                WHERE is_live_on_portal = 1
                                GROUP BY 1) b2 ON a.article_type = b2.article_type) f on a.style_id=f.style_id and a.source=f.source
  FULL OUTER JOIN (SELECT a.style_id,
                          b1.source,
                          b1.g_lp_cnt,
                          b2.g_style_cnt
                   FROM (SELECT DISTINCT fps.style_id,
                                gender
                         FROM fact_product_snapshot fps
                           JOIN dim_product dp ON fps.sku_id = dp.sku_id
                         WHERE is_live_on_portal = 1) a
                     JOIN (SELECT gender,
                                  source,
                                  COUNT(*) AS g_lp_cnt
                           FROM dev.lp_tag_by_source
                           WHERE load_date = 20170225
                           AND   final_segment = 'G'
                           GROUP BY 1,
                                    2) b1 ON a.gender = b1.gender
                     LEFT JOIN (SELECT gender,
                                       COUNT(DISTINCT dp.style_id) AS g_style_cnt
                                FROM fact_product_snapshot fps
                                  JOIN dim_product dp ON fps.sku_id = dp.sku_id
                                WHERE is_live_on_portal = 1
                                GROUP BY 1) b2
                           on a.gender = b2.gender) g on a.style_id=g.style_id and a.source=g.source
                           LIMIT 1000

