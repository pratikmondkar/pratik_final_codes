SELECT ds.style_id,
             gender,
             season,
             article_mrp,
             base_colour,
             usage_attr,
--             style_attributes,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Ankle Height') AS ankle_type,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Insole') AS Insole,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Micro Trend') AS Micro_Trend,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Occasion') AS Occasion,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Fastening') AS Fastening,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Pattern') AS Pattern,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Shoe Type') AS Shoe_Type,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Sole Material') AS Sole_Material,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Toe Shape') AS Toe_Shape,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Upper Material') AS upper_material
      FROM dim_style ds
--      join (select style_id from fact_product_snapshot where is_live_on_portal =1 and date=to_char(sysdate - interval '25 days','YYYYMMDD')::bigint) a on ds.style_id=a.style_id
      WHERE article_type = 'Casual Shoes'
      limit 70000
