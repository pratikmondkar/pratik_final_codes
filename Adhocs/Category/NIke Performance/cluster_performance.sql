SELECT c.cluster_id,
       SUM(inv) AS inv,
       nvl(SUM(qty),0) AS qty,
       count(distinct a.style_id) as live_styles
FROM (SELECT style_id,
             SUM(net_inventory_count) AS inv
      FROM fact_product_snapshot fps
      WHERE DATE = 20170130
      AND   is_live_on_portal = 1
      GROUP BY 1) a
  LEFT JOIN (SELECT style_id,
                    SUM(quantity) AS qty
             FROM fact_core_item
             WHERE store_id = 1
             AND   (is_shipped = 1 OR is_realised = 1)
             AND   order_created_date = 20170131
             GROUP BY 1) b ON a.style_id = b.style_id
  JOIN dev.cs_cluster_map c ON a.style_id = c.style_id
GROUP BY 1;


SELECT distinct fps.style_id
      FROM fact_product_snapshot fps
      JOIN dev.ss_cluster_map c ON fps.style_id = c.style_id
      WHERE DATE = 20170130
      AND   is_1218978live_on_portal = 1
      and cluster_id=86

