SELECT ds.style_id,
             gender,
             season,
             article_mrp,
             base_colour,
             usage_attr,
             style_attributes,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Ankle Height') AS ankle_height,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Arch Type') AS arch_type,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Cleats') AS Cleats,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Cushioning') AS Cushioning,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Fastening') AS Fastening,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Sport') AS Sport,
             JSON_EXTRACT_PATH_TEXT(style_attributes,'attributes','Upper Material') AS upper_material
      FROM dim_style ds
--      join (select distinct style_id from fact_product_snapshot where style_status='P' and net_inventory_count>0 and date between 20160601 and 20160704) a on ds.style_id=a.style_id
      WHERE article_type = 'Sports Shoes'
      limit 50000
