--Dump query final
				select
				ia.barcode as po_code,
				max(approval_date) as approval_date, 
				max(po_status) as po_status,
				max(po_type) as po_type,
				max(category_manager) as category_manager,
				max(brand) as brand,
				sum(po_quantity) as po_quantity,
				sum(grn_quantity) as grn_quantity,
				case when max(po_status)='CLOSED' then datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD')) else NULL end as po_age,
				case when max(po_status)='CLOSED' and datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD'))<90 and max(Brand)  in ('Jn Joy',	'SF Jeans',	'Steve Madden',	'Annabelle',	'Van Heusen Woman',	'Dr. Scholl',	'Allen Solly Woman',	'U.S. Polo Assn.',	'Ed Hardy',	'Gstar',	'Honey',	'Bata',	'Allen Solly',	'Liberty',	'Flying Machine',	'Marie Claire',	'Crocs',	'Prym',	'Shuffle',	'People',	'Noble Faith',	'Clarks',	'Fabindia',	'Superdry',	'Wrangler',	'Lavie',	'FSP',	'Rangriti',	'Lee',	'G Star',	'Numero Uno',	'Levis',	'Quiksilver',	'Van Heusen Sport',	'Pepe Jeans',	'GAS',	'Desigual',	'GANT',	'Arrow Sport',	'Closet Label',	'Aurelia',	'Pantaloons (Akkriti + Rangmanch + Trishaa)',	'The North Face',	'Tommy Hilfiger',	'Timberland',	'DC',	'Russell Athletic',	'Proline',	'Indian Terrain',	'United Colors of Benetton',	'French Connection',	'Supra',	'W',	'ASICS',	'Biba',	'Vans',	'FILA',	'Reebok',	'Adidas Neo',	'Adidas',	'Puma',	'Nike')
									then sum(po_quantity*landed_value)-sum(grn_quantity*landed_value) else 0 end as ts_allocation,
				case when max(po_status)='CLOSED' and (datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD'))>=90 or max(Brand) not in ('Jn Joy',	'SF Jeans',	'Steve Madden',	'Annabelle',	'Van Heusen Woman',	'Dr. Scholl',	'Allen Solly Woman',	'U.S. Polo Assn.',	'Ed Hardy',	'Gstar',	'Honey',	'Bata',	'Allen Solly',	'Liberty',	'Flying Machine',	'Marie Claire',	'Crocs',	'Prym',	'Shuffle',	'People',	'Noble Faith',	'Clarks',	'Fabindia',	'Superdry',	'Wrangler',	'Lavie',	'FSP',	'Rangriti',	'Lee',	'G Star',	'Numero Uno',	'Levis',	'Quiksilver',	'Van Heusen Sport',	'Pepe Jeans',	'GAS',	'Desigual',	'GANT',	'Arrow Sport',	'Closet Label',	'Aurelia',	'Pantaloons (Akkriti + Rangmanch + Trishaa)',	'The North Face',	'Tommy Hilfiger',	'Timberland',	'DC',	'Russell Athletic',	'Proline',	'Indian Terrain',	'United Colors of Benetton',	'French Connection',	'Supra',	'W',	'ASICS',	'Biba',	'Vans',	'FILA',	'Reebok',	'Adidas Neo',	'Adidas',	'Puma',	'Nike'))
									then sum(po_quantity*landed_value)-sum(grn_quantity*landed_value) else 0 end as common_pool,
				case when max(po_status) in ('APPROVED','COMPLETED') then sum(landed_value*po_quantity) else sum(landed_value*grn_quantity) end as consumption,
				sum(landed_value*po_quantity) as po_value,	
				sum(landed_value*grn_quantity) as grn_value,
				max(estimated_shipment_date) as estimated_shipment_date,
				max(actual_shipment_date) as actual_shipment_date,
				max(comments) as comments,
				max(vendor_name) as vendor_name,
				max(ss.season_id)as season_id,
				max(season_year) as season_year
from inward_analyzer ia
left join 
			(select barcode,max(season_id) as season_id,max(season_year) as season_year,max(closure_date) as closure_date from fact_purchase_order fpo
			where approval_date >= 20150205 and po_type='OUTRIGHT' and lower(po_status) in ('approved','completed','closed') group by 1) ss
on ia.barcode=ss.barcode
where approval_date >= 20150205 and 
			brand_type='External' and 
			po_type='OUTRIGHT' and
			lower(po_status) in ('approved','completed','closed')  and season_id in (18,137)
group by 1
order by 1;

--CM Brand level query
select 	c.cm_otb,
				c.brand,
				nvl(a.otb,0) as otb_allocation,
				nvl(b.consumption,0) as otb_consumption,
				nvl(a.otb,0)-nvl(b.consumption,0) as otb_pending,
				nvl(b.ts_allocation,0) as ts_allocation,
				0 as ts_consumption,
				0 as ts_pending,
				nvl(b.common_pool,0) as common_pool 
from 
(select distinct co.cm_otb,brand from dim_product dp,ctg_cm_otb_mapping co where dp.category_manager=co.category_manager and brand_type='External' and is_active=1 and dp.category_manager is not NULL) c
left join
(select cm_otb,brand,sum(otb) as otb from ctg_otb_allocation a left join ctg_cm_otb_mapping c on c.category_manager=a.category_manager group by 1,2) a
on c.cm_otb=a.cm_otb and c.brand=a.brand
left join
(Select cm_otb,
				brand,
				round(sum(consumption)) as consumption,
				round(sum(ts_allocation)) as ts_allocation,
				round(sum(common_pool)) as common_pool
						 from (	
																			select
																			ia.barcode as po_code,
																			max(approval_date) as approval_date, 
																			max(category_manager) as category_manager,
																			max(brand) as brand,
																			case when max(po_status)='CLOSED' then datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD')) else NULL end as po_age,
																			case when max(po_status)='CLOSED' and datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD'))<90 and max(Brand) in ('Jn Joy',	'SF Jeans',	'Steve Madden',	'Annabelle',	'Van Heusen Woman',	'Dr. Scholl',	'Allen Solly Woman',	'U.S. Polo Assn.',	'Ed Hardy',	'Gstar',	'Honey','Bata',	'Allen Solly',	'Liberty',	'Flying Machine',	'Marie Claire',	'Crocs',	'Prym',	'Shuffle',	'People',	'Noble Faith',	'Clarks',	'Fabindia',	'Superdry',	'Wrangler',	'Lavie',	'FSP',	'Rangriti',	'Lee',	'G Star',	'Numero Uno',	'Levis',	'Quiksilver',	'Van Heusen Sport',	'Pepe Jeans',	'GAS',	'Desigual',	'GANT',	'Arrow Sport',	'Closet Label',	'Aurelia',	'Pantaloons (Akkriti + Rangmanch + Trishaa)',	'The North Face',	'Tommy Hilfiger',	'Timberland',	'DC',	'Russell Athletic',	'Proline',	'Indian Terrain',	'United Colors of Benetton',	'French Connection',	'Supra',	'W',	'ASICS',	'Biba',	'Vans',	'FILA',	'Reebok',	'Adidas Neo',	'Adidas',	'Puma',	'Nike')
																								then sum(po_quantity*landed_value)-sum(grn_quantity*landed_value) else 0 end as ts_allocation,
																			case when max(po_status)='CLOSED' and datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD'))>=90 or max(Brand) not in ('Jn Joy',	'SF Jeans',	'Steve Madden',	'Annabelle',	'Van Heusen Woman',	'Dr. Scholl',	'Allen Solly Woman',	'U.S. Polo Assn.',	'Ed Hardy',	'Gstar',	'Honey',	'Bata',	'Allen Solly',	'Liberty',	'Flying Machine',	'Marie Claire',	'Crocs',	'Prym',	'Shuffle',	'People',	'Noble Faith',	'Clarks',	'Fabindia',	'Superdry',	'Wrangler',	'Lavie',	'FSP',	'Rangriti',	'Lee',	'G Star',	'Numero Uno',	'Levis',	'Quiksilver',	'Van Heusen Sport',	'Pepe Jeans',	'GAS',	'Desigual',	'GANT',	'Arrow Sport',	'Closet Label',	'Aurelia',	'Pantaloons (Akkriti + Rangmanch + Trishaa)',	'The North Face',	'Tommy Hilfiger',	'Timberland',	'DC',	'Russell Athletic',	'Proline',	'Indian Terrain',	'United Colors of Benetton',	'French Connection',	'Supra',	'W',	'ASICS',	'Biba',	'Vans',	'FILA',	'Reebok',	'Adidas Neo',	'Adidas',	'Puma',	'Nike')
																								then sum(po_quantity*landed_value)-sum(grn_quantity*landed_value) else 0 end as common_pool,
																			case when max(po_status) in ('APPROVED','COMPLETED') then sum(landed_value*po_quantity) else sum(landed_value*grn_quantity) end as consumption
															from inward_analyzer ia
															left join 
																		(select barcode,max(season_id) as season_id,max(season_year) as season_year,max(closure_date) as closure_date from fact_purchase_order fpo
																		where approval_date >= 20150205 and po_type='OUTRIGHT' and lower(po_status) in ('approved','completed','closed') group by 1) ss
															on ia.barcode=ss.barcode
															where approval_date >= 20150205 and 
																		brand_type='External' and 
																		po_type='OUTRIGHT' and
																		lower(po_status) in ('approved','completed','closed') and season_id in (18,137)
															group by 1) con
									left join ctg_cm_otb_mapping co
									on con.category_manager=co.category_manager
group by 1,2) b
on c.cm_otb=b.cm_otb and c.brand=b.brand
where otb is not NULL or consumption is not NULL or ts_allocation is not NULL or common_pool is not NULL
order by 1,2;

--CM level query
select 	c.cm_otb,
				nvl(a.otb,0) as otb_allocation,
				nvl(b.consumption,0) as otb_consumption,
				nvl(a.otb,0)-nvl(b.consumption,0) as otb_pending,
				nvl(b.ts_allocation,0) as ts_allocation,
				0 as ts_consumption,
				0 as ts_pending, 
				nvl(b.common_pool,0) as common_pool 
from 
(select distinct cm_otb from ctg_cm_otb_mapping) c
left join
(select cm_otb,sum(otb) as otb from ctg_otb_allocation a left join ctg_cm_otb_mapping c on c.category_manager=a.category_manager group by 1) a
on c.cm_otb=a.cm_otb 
left join
(Select cm_otb,
				round(sum(consumption)) as consumption,
				round(sum(ts_allocation)) as ts_allocation,
				round(sum(common_pool)) as common_pool
						 from (	
																			select
																			ia.barcode as po_code,
																			max(category_manager) as category_manager,
																			case when max(po_status)='CLOSED' then datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD')) else NULL end as po_age,
																			case when max(po_status)='CLOSED' and datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD'))<90 and max(Brand) in ('Jn Joy',	'SF Jeans',	'Steve Madden',	'Annabelle',	'Van Heusen Woman',	'Dr. Scholl',	'Allen Solly Woman',	'U.S. Polo Assn.',	'Ed Hardy',	'Gstar',	'Honey',	'Bata',	'Allen Solly',	'Liberty',	'Flying Machine',	'Marie Claire',	'Crocs',	'Prym',	'Shuffle',	'People',	'Noble Faith',	'Clarks',	'Fabindia',	'Superdry',	'Wrangler',	'Lavie',	'FSP',	'Rangriti',	'Lee',	'G Star',	'Numero Uno',	'Levis',	'Quiksilver',	'Van Heusen Sport',	'Pepe Jeans',	'GAS',	'Desigual',	'GANT',	'Arrow Sport',	'Closet Label',	'Aurelia',	'Pantaloons (Akkriti + Rangmanch + Trishaa)',	'The North Face',	'Tommy Hilfiger',	'Timberland',	'DC',	'Russell Athletic',	'Proline',	'Indian Terrain',	'United Colors of Benetton',	'French Connection',	'Supra',	'W',	'ASICS',	'Biba',	'Vans',	'FILA',	'Reebok',	'Adidas Neo',	'Adidas',	'Puma',	'Nike')
																								then sum(po_quantity*landed_value)-sum(grn_quantity*landed_value) else 0 end as ts_allocation,
																			case when max(po_status)='CLOSED' and datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD'))>=90 or max(Brand) not in ('Jn Joy',	'SF Jeans',	'Steve Madden',	'Annabelle',	'Van Heusen Woman',	'Dr. Scholl',	'Allen Solly Woman',	'U.S. Polo Assn.',	'Ed Hardy',	'Gstar',	'Honey',	'Bata',	'Allen Solly',	'Liberty',	'Flying Machine',	'Marie Claire',	'Crocs',	'Prym',	'Shuffle',	'People',	'Noble Faith',	'Clarks',	'Fabindia',	'Superdry',	'Wrangler',	'Lavie',	'FSP',	'Rangriti',	'Lee',	'G Star',	'Numero Uno',	'Levis',	'Quiksilver',	'Van Heusen Sport',	'Pepe Jeans',	'GAS',	'Desigual',	'GANT',	'Arrow Sport',	'Closet Label',	'Aurelia',	'Pantaloons (Akkriti + Rangmanch + Trishaa)',	'The North Face',	'Tommy Hilfiger',	'Timberland',	'DC',	'Russell Athletic',	'Proline',	'Indian Terrain',	'United Colors of Benetton',	'French Connection',	'Supra',	'W',	'ASICS',	'Biba',	'Vans',	'FILA',	'Reebok',	'Adidas Neo',	'Adidas',	'Puma',	'Nike')
																								then sum(po_quantity*landed_value)-sum(grn_quantity*landed_value) else 0 end as common_pool,
																			case when max(po_status) in ('APPROVED','COMPLETED') then sum(landed_value*po_quantity) else sum(landed_value*grn_quantity) end as consumption
															from inward_analyzer ia
															left join 
																		(select barcode,max(season_id) as season_id,max(season_year) as season_year,max(closure_date) as closure_date from fact_purchase_order fpo
																		where approval_date >= 20150205 and po_type='OUTRIGHT' and lower(po_status) in ('approved','completed','closed') group by 1) ss
															on ia.barcode=ss.barcode
															where approval_date >= 20150205 and 
																		brand_type='External' and 
																		po_type='OUTRIGHT' and
																		lower(po_status) in ('approved','completed','closed') and season_id in (18,137)
															group by 1) con
									left join ctg_cm_otb_mapping co
									on con.category_manager=co.category_manager
group by 1) b
on c.cm_otb=b.cm_otb 
where otb is not NULL or consumption is not NULL or ts_allocation is not NULL or common_pool is not NULL
order by 1;
