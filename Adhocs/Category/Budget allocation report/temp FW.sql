Select business_unit,
				brand,
				round(sum(consumption)) as consumption,
				round(sum(ts_allocation)) as ts_allocation,
				round(sum(common_pool)) as common_pool
						 from (	
																			select
																			ia.barcode as po_code,
																			max(ia.approval_date) as approval_date, 
																			max(ds.business_unit) as business_unit,
																			max(ia.brand) as brand,
																			case when max(po_status)='CLOSED' then datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD')) else NULL end as po_age,
																			case when max(po_status)='CLOSED' and datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD'))<90 and max(ia.Brand)  in ('Jn Joy',	'SF Jeans',	'Steve Madden',	'Annabelle',	'Van Heusen Woman',	'Dr. Scholl',	'Allen Solly Woman',	'U.S. Polo Assn.',	'Ed Hardy',	'Gstar',	'Honey',	'Bata',	'Allen Solly',	'Liberty',	'Flying Machine',	'Marie Claire',	'Crocs',	'Prym',	'Shuffle',	'People',	'Noble Faith',	'Clarks',	'Fabindia',	'Superdry',	'Wrangler',	'Lavie',	'FSP',	'Rangriti',	'Lee',	'G Star',	'Numero Uno',	'Levis',	'Quiksilver',	'Van Heusen Sport',	'Pepe Jeans',	'GAS',	'Desigual',	'GANT',	'Arrow Sport',	'Closet Label',	'Aurelia',	'Pantaloons (Akkriti + Rangmanch + Trishaa)',	'The North Face',	'Tommy Hilfiger',	'Timberland',	'DC',	'Russell Athletic',	'Proline',	'Indian Terrain',	'United Colors of Benetton',	'French Connection',	'Supra',	'W',	'ASICS',	'Biba',	'Vans',	'FILA',	'Reebok',	'Adidas Neo',	'Adidas',	'Puma',	'Nike')
																								then sum(po_quantity*landed_value)-sum(grn_quantity*landed_value) else 0 end as ts_allocation,
																			case when max(po_status)='CLOSED' and (datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD'))>=90 or max(ia.Brand) not in ('Jn Joy',	'SF Jeans',	'Steve Madden',	'Annabelle',	'Van Heusen Woman',	'Dr. Scholl',	'Allen Solly Woman',	'U.S. Polo Assn.',	'Ed Hardy',	'Gstar',	'Honey',	'Bata',	'Allen Solly',	'Liberty',	'Flying Machine',	'Marie Claire',	'Crocs',	'Prym',	'Shuffle',	'People',	'Noble Faith',	'Clarks',	'Fabindia',	'Superdry',	'Wrangler',	'Lavie',	'FSP',	'Rangriti',	'Lee',	'G Star',	'Numero Uno',	'Levis',	'Quiksilver',	'Van Heusen Sport',	'Pepe Jeans',	'GAS',	'Desigual',	'GANT',	'Arrow Sport',	'Closet Label',	'Aurelia',	'Pantaloons (Akkriti + Rangmanch + Trishaa)',	'The North Face',	'Tommy Hilfiger',	'Timberland',	'DC',	'Russell Athletic',	'Proline',	'Indian Terrain',	'United Colors of Benetton',	'French Connection',	'Supra',	'W',	'ASICS',	'Biba',	'Vans',	'FILA',	'Reebok',	'Adidas Neo',	'Adidas',	'Puma',	'Nike'))
																								then sum(po_quantity*landed_value)-sum(grn_quantity*landed_value) else 0 end as common_pool,
																			case when max(po_status) in ('APPROVED','COMPLETED') then sum(landed_value*po_quantity) else sum(landed_value*grn_quantity) end as consumption
															from inward_analyzer ia
															left join 
																		(select barcode,max(season_id) as season_id,max(season_year) as season_year,max(closure_date) as closure_date from fact_purchase_order fpo
																		where approval_date >= 20150527 and po_type='OUTRIGHT' and lower(po_status) in ('approved','completed','closed') group by 1) ss
															on ia.barcode=ss.barcode
															left join dim_style ds
															on ia.style_id=ds.style_id
															where approval_date >= 20150527 and 
																		ia.brand_type='External' and 
																		po_type='OUTRIGHT' and
																		lower(po_status) in ('approved','completed','closed') and season_id in (138,139)
															group by 1) 
group by 1,2
order by 1,2;

Select business_unit,al.brand,sum(otb) as otb from ctg_otb_allocation al 
			left join (select category_manager,max(business_unit) as business_unit from dim_product group by 1) dp
			on al.category_manager=dp.category_manager group by 1,2 order by 1,2;
