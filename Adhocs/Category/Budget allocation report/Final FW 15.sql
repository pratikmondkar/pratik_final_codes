--Dump query final
				select
				ia.barcode as po_code,
				max(approval_date) as approval_date, 
				max(po_status) as po_status,
				max(po_type) as po_type,
				max(ia.category_manager) as category_manager,
				max(brand) as brand,
				sum(po_quantity) as po_quantity,
				sum(grn_quantity) as grn_quantity,
				case when max(po_status)='CLOSED' then datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD')) else NULL end as po_age,
				case when max(po_status)='CLOSED' and datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD'))<90 and max(ia.Brand)  in ('Jn Joy',	'SF Jeans',	'Steve Madden',	'Annabelle',	'Van Heusen Woman',	'Dr. Scholl',	'Allen Solly Woman',	'U.S. Polo Assn.',	'Ed Hardy',	'Gstar',	'Honey',	'Bata',	'Allen Solly',	'Liberty',	'Flying Machine',	'Marie Claire',	'Crocs',	'Prym',	'Shuffle',	'People',	'Noble Faith',	'Clarks',	'Fabindia',	'Superdry',	'Wrangler',	'Lavie',	'FSP',	'Rangriti',	'Lee',	'G Star',	'Numero Uno',	'Levis',	'Quiksilver',	'Van Heusen Sport',	'Pepe Jeans',	'GAS',	'Desigual',	'GANT',	'Arrow Sport',	'Closet Label',	'Aurelia',	'Pantaloons (Akkriti + Rangmanch + Trishaa)',	'The North Face',	'Tommy Hilfiger',	'Timberland',	'DC',	'Russell Athletic',	'Proline',	'Indian Terrain',	'United Colors of Benetton',	'French Connection',	'Supra',	'W',	'ASICS',	'Biba',	'Vans',	'FILA',	'Reebok',	'Adidas Neo',	'Adidas',	'Puma',	'Nike')
									then sum(po_quantity*landed_value)-sum(grn_quantity*landed_value) else 0 end as ts_allocation,
				case when max(po_status)='CLOSED' and (datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD'))>=90 or max(ia.Brand) not in ('Jn Joy',	'SF Jeans',	'Steve Madden',	'Annabelle',	'Van Heusen Woman',	'Dr. Scholl',	'Allen Solly Woman',	'U.S. Polo Assn.',	'Ed Hardy',	'Gstar',	'Honey',	'Bata',	'Allen Solly',	'Liberty',	'Flying Machine',	'Marie Claire',	'Crocs',	'Prym',	'Shuffle',	'People',	'Noble Faith',	'Clarks',	'Fabindia',	'Superdry',	'Wrangler',	'Lavie',	'FSP',	'Rangriti',	'Lee',	'G Star',	'Numero Uno',	'Levis',	'Quiksilver',	'Van Heusen Sport',	'Pepe Jeans',	'GAS',	'Desigual',	'GANT',	'Arrow Sport',	'Closet Label',	'Aurelia',	'Pantaloons (Akkriti + Rangmanch + Trishaa)',	'The North Face',	'Tommy Hilfiger',	'Timberland',	'DC',	'Russell Athletic',	'Proline',	'Indian Terrain',	'United Colors of Benetton',	'French Connection',	'Supra',	'W',	'ASICS',	'Biba',	'Vans',	'FILA',	'Reebok',	'Adidas Neo',	'Adidas',	'Puma',	'Nike'))
									then sum(po_quantity*landed_value)-sum(grn_quantity*landed_value) else 0 end as common_pool,
				case when max(po_status) in ('APPROVED','COMPLETED') then sum(landed_value*po_quantity) else sum(landed_value*grn_quantity) end as consumption,
				sum(landed_value*po_quantity) as po_value,	
				sum(landed_value*grn_quantity) as grn_value,
				max(estimated_shipment_date) as estimated_shipment_date,
				max(actual_shipment_date) as actual_shipment_date,
				max(comments) as comments,
				max(vendor_name) as vendor_name,
				max(ss.season_id)as season_id,
				max(season_year) as season_year,
				max(prioritization) as prioritization
from inward_analyzer ia
left join 
			(select barcode,max(season_id) as season_id,max(season_year) as season_year,max(closure_date) as closure_date,max(prioritization) as prioritization,max(category_manager) as cm
			from fact_purchase_order fpo where approval_date >= 20150527 and po_type='OUTRIGHT' and lower(po_status) in ('approved','completed','closed') group by 1) ss
on ia.barcode=ss.barcode
where approval_date >= 20150527 and 
			brand_type='External' and 
			po_type='OUTRIGHT' and
			lower(po_status) in ('approved','completed','closed')  and season_id in (138,139)
group by 1
order by 1;


--BU + Brand level
select b.*,			
			aloc.business_unit as allocation_bu,
			aloc.category_manager as allocation_cm,
			aloc.brand as allocation_brand,
			con.business_unit as consumption_bu,
			con.category_manager as consumption_cm,
			con.brand as consumption_brand,
			to_char(nvl(otb,0),'99,99,99,99,999') as otb,
			to_char(nvl(consumption,0),'99,99,99,99,999') as consumption,
			to_char(nvl(otb,0)-nvl(consumption,0)-nvl(ts_available,0)-nvl(ts_allocation,0),'99,99,99,99,999') as otb_pending,
			to_char(nvl(ts_available,0)-nvl(ts_allocation,0),'99,99,99,99,999') as ts_available,
			to_char(nvl(ts_allocation,0),'99,99,99,99,999') as ts_allocation,
			to_char(nvl(ts_consumption,0),'99,99,99,99,999') as ts_consumption,
			to_char(nvl(ts_allocation,0)-nvl(ts_consumption,0),'99,99,99,99,999') as ts_pending,
			to_char(nvl(common_pool,0),'99,99,99,99,999')  as common_pool
from 
(select distinct lower(business_unit) business_unit,lower(brand) brand from dim_product where category_manager is not NULL) b
full outer join 
(Select lower(ctg) business_unit,lower(al.brand) as brand,max(al.category_manager) as category_manager,sum(otb) as otb, 
			sum(case when trenche=99 then otb else 0 end) as ts_allocation from ctg_otb_allocation al 
			group by 1,2) aloc
on b.business_unit=aloc.business_unit and b.brand=aloc.brand
full outer join 
(Select lower(business_unit) as business_unit,
				lower(brand) as brand,
				max(category_manager) as category_manager,
				round(sum(consumption)) as consumption,
				round(sum(ts_available)) as ts_available,
				round(sum(case when prioritization='TOP_SELLER' then consumption else 0 end)) as ts_consumption,
				round(sum(common_pool)) as common_pool
						 from (	
																			select
																			ia.barcode as po_code,
																			max(ia.approval_date) as approval_date, 
																			max(ds.business_unit) as business_unit,
																			max(ia.brand) as brand,
																			max(ia.category_manager) as category_manager,
																			max(prioritization) as prioritization,
																			case when max(po_status)='CLOSED' then datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD')) else NULL end as po_age,
																			case when max(po_status)='CLOSED' and datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD'))<90 and max(ia.Brand)  in ('Jn Joy',	'SF Jeans',	'Steve Madden',	'Annabelle',	'Van Heusen Woman',	'Dr. Scholl',	'Allen Solly Woman',	'U.S. Polo Assn.',	'Ed Hardy',	'Gstar',	'Honey',	'Bata',	'Allen Solly',	'Liberty',	'Flying Machine',	'Marie Claire',	'Crocs',	'Prym',	'Shuffle',	'People',	'Noble Faith',	'Clarks',	'Fabindia',	'Superdry',	'Wrangler',	'Lavie',	'FSP',	'Rangriti',	'Lee',	'G Star',	'Numero Uno',	'Levis',	'Quiksilver',	'Van Heusen Sport',	'Pepe Jeans',	'GAS',	'Desigual',	'GANT',	'Arrow Sport',	'Closet Label',	'Aurelia',	'Pantaloons (Akkriti + Rangmanch + Trishaa)',	'The North Face',	'Tommy Hilfiger',	'Timberland',	'DC',	'Russell Athletic',	'Proline',	'Indian Terrain',	'United Colors of Benetton',	'French Connection',	'Supra',	'W',	'ASICS',	'Biba',	'Vans',	'FILA',	'Reebok',	'Adidas Neo',	'Adidas',	'Puma',	'Nike')
																								then sum(po_quantity*landed_value)-sum(grn_quantity*landed_value) else 0 end as ts_available,
																			case when max(po_status)='CLOSED' and (datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD'))>=90 or max(ia.Brand) not in ('Jn Joy',	'SF Jeans',	'Steve Madden',	'Annabelle',	'Van Heusen Woman',	'Dr. Scholl',	'Allen Solly Woman',	'U.S. Polo Assn.',	'Ed Hardy',	'Gstar',	'Honey',	'Bata',	'Allen Solly',	'Liberty',	'Flying Machine',	'Marie Claire',	'Crocs',	'Prym',	'Shuffle',	'People',	'Noble Faith',	'Clarks',	'Fabindia',	'Superdry',	'Wrangler',	'Lavie',	'FSP',	'Rangriti',	'Lee',	'G Star',	'Numero Uno',	'Levis',	'Quiksilver',	'Van Heusen Sport',	'Pepe Jeans',	'GAS',	'Desigual',	'GANT',	'Arrow Sport',	'Closet Label',	'Aurelia',	'Pantaloons (Akkriti + Rangmanch + Trishaa)',	'The North Face',	'Tommy Hilfiger',	'Timberland',	'DC',	'Russell Athletic',	'Proline',	'Indian Terrain',	'United Colors of Benetton',	'French Connection',	'Supra',	'W',	'ASICS',	'Biba',	'Vans',	'FILA',	'Reebok',	'Adidas Neo',	'Adidas',	'Puma',	'Nike'))
																								then sum(po_quantity*landed_value)-sum(grn_quantity*landed_value) else 0 end as common_pool,
																			case when max(po_status) in ('APPROVED','COMPLETED') then sum(landed_value*po_quantity) else sum(landed_value*grn_quantity) end as consumption
															from inward_analyzer ia
															left join 
																		(select barcode,max(season_id) as season_id,max(season_year) as season_year,max(closure_date) as closure_date,max(prioritization) as prioritization
																		 from fact_purchase_order fpo
																		where approval_date >= 20150527 and po_type='OUTRIGHT' and lower(po_status) in ('approved','completed','closed') group by 1) ss
															on ia.barcode=ss.barcode
															left join dim_style ds
															on ia.style_id=ds.style_id
															where approval_date >= 20150527 and 
																		ia.brand_type='External' and 
																		po_type='OUTRIGHT' and
																		lower(po_status) in ('approved','completed','closed') and season_id in (138,139)
															group by 1) 
group by 1,2) con
on b.business_unit=con.business_unit and b.brand=con.brand
where otb is not NULL or consumption is not NULL or ts_allocation is not NULL or common_pool is not NULL
order by 9 desc;


--BU level
select b.*,			
			aloc.business_unit as allocation_bu,
			aloc.category_manager as allocation_cm,
			con.business_unit as consumption_bu,
			con.category_manager as consumption_cm,
			to_char(nvl(otb,0),'99,99,99,99,999') as otb,
			to_char(nvl(consumption,0),'99,99,99,99,999') as consumption,
			to_char(nvl(otb,0)-nvl(consumption,0)-nvl(ts_available,0)-nvl(ts_allocation,0),'99,99,99,99,999') as otb_pending,
			to_char(nvl(ts_available,0)-nvl(ts_allocation,0),'99,99,99,99,999') as ts_available,
			to_char(nvl(ts_allocation,0),'99,99,99,99,999') as ts_allocation,
			to_char(nvl(ts_consumption,0),'99,99,99,99,999') as ts_consumption,
			to_char(nvl(ts_allocation,0)-nvl(ts_consumption,0),'99,99,99,99,999') as ts_pending,
			to_char(nvl(common_pool,0),'99,99,99,99,999')  as common_pool
from 
(select distinct lower(business_unit) business_unit from dim_product where category_manager is not NULL) b
full outer join 
(Select lower(ctg) business_unit,max(al.category_manager) as category_manager,sum(otb) as otb, 
			sum(case when trenche=99 then otb else 0 end) as ts_allocation from ctg_otb_allocation al 
			group by 1) aloc
on b.business_unit=aloc.business_unit
full outer join 
(Select lower(business_unit) as business_unit,
				max(category_manager) as category_manager,
				round(sum(consumption)) as consumption,
				round(sum(ts_available)) as ts_available,
				round(sum(case when prioritization='TOP_SELLER' then consumption else 0 end)) as ts_consumption,
				round(sum(common_pool)) as common_pool
						 from (	
																			select
																			ia.barcode as po_code,
																			max(ia.approval_date) as approval_date, 
																			max(ds.business_unit) as business_unit,
																			max(ia.category_manager) as category_manager,
																			max(prioritization) as prioritization,
																			case when max(po_status)='CLOSED' then datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD')) else NULL end as po_age,
																			case when max(po_status)='CLOSED' and datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD'))<90 and max(ia.Brand)  in ('Jn Joy',	'SF Jeans',	'Steve Madden',	'Annabelle',	'Van Heusen Woman',	'Dr. Scholl',	'Allen Solly Woman',	'U.S. Polo Assn.',	'Ed Hardy',	'Gstar',	'Honey',	'Bata',	'Allen Solly',	'Liberty',	'Flying Machine',	'Marie Claire',	'Crocs',	'Prym',	'Shuffle',	'People',	'Noble Faith',	'Clarks',	'Fabindia',	'Superdry',	'Wrangler',	'Lavie',	'FSP',	'Rangriti',	'Lee',	'G Star',	'Numero Uno',	'Levis',	'Quiksilver',	'Van Heusen Sport',	'Pepe Jeans',	'GAS',	'Desigual',	'GANT',	'Arrow Sport',	'Closet Label',	'Aurelia',	'Pantaloons (Akkriti + Rangmanch + Trishaa)',	'The North Face',	'Tommy Hilfiger',	'Timberland',	'DC',	'Russell Athletic',	'Proline',	'Indian Terrain',	'United Colors of Benetton',	'French Connection',	'Supra',	'W',	'ASICS',	'Biba',	'Vans',	'FILA',	'Reebok',	'Adidas Neo',	'Adidas',	'Puma',	'Nike')
																								then sum(po_quantity*landed_value)-sum(grn_quantity*landed_value) else 0 end as ts_available,
																			case when max(po_status)='CLOSED' and (datediff(d,to_date( max(approval_date),'YYYYMMDD'),to_date(max(closure_date),'YYYYMMDD'))>=90 or max(ia.Brand) not in ('Jn Joy',	'SF Jeans',	'Steve Madden',	'Annabelle',	'Van Heusen Woman',	'Dr. Scholl',	'Allen Solly Woman',	'U.S. Polo Assn.',	'Ed Hardy',	'Gstar',	'Honey',	'Bata',	'Allen Solly',	'Liberty',	'Flying Machine',	'Marie Claire',	'Crocs',	'Prym',	'Shuffle',	'People',	'Noble Faith',	'Clarks',	'Fabindia',	'Superdry',	'Wrangler',	'Lavie',	'FSP',	'Rangriti',	'Lee',	'G Star',	'Numero Uno',	'Levis',	'Quiksilver',	'Van Heusen Sport',	'Pepe Jeans',	'GAS',	'Desigual',	'GANT',	'Arrow Sport',	'Closet Label',	'Aurelia',	'Pantaloons (Akkriti + Rangmanch + Trishaa)',	'The North Face',	'Tommy Hilfiger',	'Timberland',	'DC',	'Russell Athletic',	'Proline',	'Indian Terrain',	'United Colors of Benetton',	'French Connection',	'Supra',	'W',	'ASICS',	'Biba',	'Vans',	'FILA',	'Reebok',	'Adidas Neo',	'Adidas',	'Puma',	'Nike'))
																								then sum(po_quantity*landed_value)-sum(grn_quantity*landed_value) else 0 end as common_pool,
																			case when max(po_status) in ('APPROVED','COMPLETED') then sum(landed_value*po_quantity) else sum(landed_value*grn_quantity) end as consumption
															from inward_analyzer ia
															left join 
																		(select barcode,max(season_id) as season_id,max(season_year) as season_year,max(closure_date) as closure_date,max(prioritization) as prioritization
																		 from fact_purchase_order fpo
																		where approval_date >= 20150527 and po_type='OUTRIGHT' and lower(po_status) in ('approved','completed','closed') group by 1) ss
															on ia.barcode=ss.barcode
															left join dim_style ds
															on ia.style_id=ds.style_id
															where approval_date >= 20150527 and 
																		ia.brand_type='External' and 
																		po_type='OUTRIGHT' and
																		lower(po_status) in ('approved','completed','closed') and season_id in (138,139)
															group by 1) 
group by 1) con
on b.business_unit=con.business_unit 
where otb is not NULL or consumption is not NULL or ts_allocation is not NULL or common_pool is not NULL
order by 6 desc
