--Dump query final
select * from (
				select
				barcode as po_code,
				max(approval_date) as approval_date, 
				max(po_status) as po_status,
				max(po_type) as po_type,
				max(category_manager) as category_manager,
				max(brand) as brand,
				sum(po_quantity) as po_quantity,
				sum(grn_quantity) as grn_quantity,
				sum(case when lower(po_status) ='approved' then landed_value*po_quantity else landed_value*grn_quantity end) as adj_po_value,
				sum(landed_value*po_quantity) as po_value,	
				sum(landed_value*grn_quantity) as grn_value,
				max(estimated_shipment_date) as estimated_shipment_date,
				max(actual_shipment_date) as actual_shipment_date,
				max(comments) as comments,
				max(vendor_name) as vendor_name
from inward_analyzer 
where approval_date >= 20150205 and 
			brand_type='External' and 
			po_type='OUTRIGHT'
group by 1
order by 1)
where (lower(po_status) in ('approved','completed') or (lower(po_status) in ('closed') and grn_quantity!=0));

--CM Brand Month level query
select c.*,a.*,b.total_consump,b.feb_consump,b.mar_consump,b.apr_consump,b.may_consump,b.jun_consump,b.jun_consump
from 
(		select distinct cm_otb,brand from 
			(select distinct category_manager,brand from dim_product where brand_type='External' and is_active=1 and category_manager is not NULL) cm
		left join ctg_otb_cm_mapping cm_otb
		on cm.category_manager=cm_otb.category_manager) c
left join
(select category_manager,
				brand,			
				sum(otb) as total_otb,
				sum(case when month='FEBRUARY' then otb else 0 end) as Feb_otb_total,
				sum(case when month='FEBRUARY' and trenche=1 then otb else 0 end) as Feb_otb_trench_1,
				sum(case when month='FEBRUARY' and trenche=2 then otb else 0 end) as Feb_otb_trench_2,
				sum(case when month='MARCH' then otb else 0 end) as mar_otb_total,
				sum(case when month='MARCH' and trenche=1 then otb else 0 end) as mar_otb_trench_1,
				sum(case when month='MARCH' and trenche=2 then otb else 0 end) as mar_otb_trench_2,
				sum(case when month='APRIL' then otb else 0 end) as apr_otb_total,
				sum(case when month='APRIL' and trenche=1 then otb else 0 end) as apr_otb_trench_1,
				sum(case when month='APRIL' and trenche=2 then otb else 0 end) as apr_otb_trench_2,
				sum(case when month='MAY' then otb else 0 end) as may_otb_total,
				sum(case when month='MAY' and trenche=1 then otb else 0 end) as may_otb_trench_1,
				sum(case when month='MAY' and trenche=2 then otb else 0 end) as may_otb_trench_2,
				sum(case when month='JUNE' then otb else 0 end) as jun_otb_total,
				sum(case when month='JUNE' and trenche=1 then otb else 0 end) as jun_otb_trench_1,
				sum(case when month='JUNE' and trenche=2 then otb else 0 end) as jun_otb_trench_2,
				sum(case when month='JULY' then otb else 0 end) as jul_otb_total,
				sum(case when month='JULY' and trenche=1 then otb else 0 end) as jul_otb_trench_1,
				sum(case when month='JULY' and trenche=2 then otb else 0 end) as jul_otb_trench_2 
				from ctg_otb_allocation 
				group by 1,2) a
on c.category_manager=a.category_manager and c.brand=a.brand
left join
(	select category_manager,brand,
				sum(consumption) as total_consump,
				sum(case when month='FEBRUARY' then consumption else 0 end) as feb_consump,
				sum(case when month='MARCH' then consumption else 0 end) as mar_consump,
				sum(case when month='APRIL' then consumption else 0 end) as apr_consump,
				sum(case when month='MAY' then consumption else 0 end) as may_consump,
				sum(case when month='JUNE' then consumption else 0 end) as jun_consump,
				sum(case when month='JULY' then consumption else 0 end) as jul_consump
	from(
				Select category_manager,
				brand,
				TO_CHAR(TO_DATE(date_part(mon,to_date(approval_date,'YYYYMMDD'))::smallint,'MM'), 'MONTH') as month,
				sum(po_value) as consumption
						 from (	
															select barcode,	
															max(po_status) as po_status,
															max(approval_date) as approval_date,
															max(category_manager) as category_manager,
															max(brand) as brand,
															sum(po_quantity) as po_quantity,
															sum(grn_quantity) as grn_quantity,
															sum(purchase_value*po_quantity) as po_value	
															from inward_analyzer 
															where approval_date >= 20150201 and 
																		brand_type='External' and 
																		po_type='OUTRIGHT'
															group by 1
															order by 1)
				where (lower(po_status) in ('approved','completed') or (lower(po_status) in ('closed') and po_quantity != grn_quantity ))
group by 1,2,3) 
				group by 1,2) b
on c.category_manager=b.category_manager and c.brand=b.brand
order by 1,2;

--CM brand level query
select c.cm_otb,c.brand,nvl(a.otb,0) as otb,nvl(b.consumption,0) as consumption from 
(select distinct co.cm_otb,brand from dim_product dp,ctg_cm_otb_mapping co where dp.category_manager=co.category_manager and brand_type='External' and is_active=1 and dp.category_manager is not NULL) c
left join
(select category_manager,brand,sum(otb) as otb from ctg_otb_allocation group by 1,2) a
on c.cm_otb=a.category_manager and c.brand=a.brand
left join
(Select cm_otb,
				brand,
				round(sum(po_value)) as consumption
						 from (	
															select barcode,	
															max(po_status) as po_status,
															max(approval_date) as approval_date,
															max(category_manager) as category_manager,
															max(brand) as brand,
															sum(po_quantity) as po_quantity,
															sum(grn_quantity) as grn_quantity,
															sum(case when lower(po_status) ='approved' then landed_value*po_quantity else landed_value*grn_quantity end) as po_value
															from inward_analyzer 
															where approval_date >= 20150205 and 
																		brand_type='External' and 
																		po_type='OUTRIGHT'
															group by 1
															order by 1) con
									left join ctg_cm_otb_mapping co
									on con.category_manager=co.category_manager
				where (lower(po_status) in ('approved','completed') or (lower(po_status) in ('closed') and grn_quantity != 0 ))
group by 1,2) b
on c.cm_otb=b.cm_otb and c.brand=b.brand
where otb is not NULL or consumption is not NULL
order by 1,2;

--CM level Query
select a.cm_otb,round(nvl(b.otb,0)) as otb,round(nvl(c.consumption,0)) as consumption,round(nvl(b.otb,0) - nvl(c.consumption,0)) as balance
from 
(select distinct cm_otb from ctg_cm_otb_mapping) a
left join
(select cm.cm_otb,sum(otb) as otb from ctg_otb_allocation ca,ctg_cm_otb_mapping cm where ca.category_manager=cm.category_manager group by 1) b
on a.cm_otb=b.cm_otb
left join
(Select co.cm_otb,
				round(sum(po_value)) as consumption
						 from (	
															select barcode,	
															max(po_status) as po_status,
															max(approval_date) as approval_date,
															max(category_manager) as category_manager,
															max(brand) as brand,
															sum(po_quantity) as po_quantity,
															sum(grn_quantity) as grn_quantity,
															sum(case when lower(po_status) ='approved' then landed_value*po_quantity else landed_value*grn_quantity end) as po_value	
															from inward_analyzer 
															where approval_date >= 20150205 and 
																		brand_type='External' and 
																		po_type='OUTRIGHT'
															group by 1
															order by 1) con
									left join ctg_cm_otb_mapping co
									on con.category_manager=co.category_manager
				where (lower(po_status) in ('approved','completed') or (lower(po_status) in ('closed') and grn_quantity != 0 ))
group by 1) c
on a.cm_otb=c.cm_otb
order by 1;



--CM Month level
select a.*,b.total_consump,b.feb_consump,b.mar_consump,b.apr_consump,b.may_consump,b.jun_consump,b.jul_consump
from 
(select cm_otb,			
				sum(otb) as total_otb,
				sum(case when month='FEBRUARY' then otb else 0 end) as Feb_otb_total,
				sum(case when month='FEBRUARY' and trenche=1 then otb else 0 end) as Feb_otb_trench_1,
				sum(case when month='FEBRUARY' and trenche=2 then otb else 0 end) as Feb_otb_trench_2,
				sum(case when month='MARCH' then otb else 0 end) as mar_otb_total,
				sum(case when month='MARCH' and trenche=1 then otb else 0 end) as mar_otb_trench_1,
				sum(case when month='MARCH' and trenche=2 then otb else 0 end) as mar_otb_trench_2,
				sum(case when month='APRIL' then otb else 0 end) as apr_otb_total,
				sum(case when month='APRIL' and trenche=1 then otb else 0 end) as apr_otb_trench_1,
				sum(case when month='APRIL' and trenche=2 then otb else 0 end) as apr_otb_trench_2,
				sum(case when month='MAY' then otb else 0 end) as may_otb_total,
				sum(case when month='MAY' and trenche=1 then otb else 0 end) as may_otb_trench_1,
				sum(case when month='MAY' and trenche=2 then otb else 0 end) as may_otb_trench_2,
				sum(case when month='JUNE' then otb else 0 end) as jun_otb_total,
				sum(case when month='JUNE' and trenche=1 then otb else 0 end) as jun_otb_trench_1,
				sum(case when month='JUNE' and trenche=2 then otb else 0 end) as jun_otb_trench_2,
				sum(case when month='JULY' then otb else 0 end) as jul_otb_total,
				sum(case when month='JULY' and trenche=1 then otb else 0 end) as jul_otb_trench_1,
				sum(case when month='JULY' and trenche=2 then otb else 0 end) as jul_otb_trench_2 
				from ctg_otb_allocation alloc
				left join ctg_cm_otb_mapping cm_otb
				on alloc.category_manager=cm_otb.category_manager
				group by 1) a
left join
(	select cm_otb,
				sum(consumption) as total_consump,
				sum(case when month='FEBRUARY' then consumption else 0 end) as feb_consump,
				sum(case when month='MARCH' then consumption else 0 end) as mar_consump,
				sum(case when month='APRIL' then consumption else 0 end) as apr_consump,
				sum(case when month='MAY' then consumption else 0 end) as may_consump,
				sum(case when month='JUNE' then consumption else 0 end) as jun_consump,
				sum(case when month='JULY' then consumption else 0 end) as jul_consump
	from(
				Select category_manager,
				TO_CHAR(TO_DATE(date_part(mon,to_date(approval_date,'YYYYMMDD'))::smallint,'MM'), 'MONTH') as month,
				sum(po_value) as consumption
						 from (	
															select barcode,	
															max(po_status) as po_status,
															max(approval_date) as approval_date,
															max(category_manager) as category_manager,
															max(brand) as brand,
															sum(po_quantity) as po_quantity,
															sum(grn_quantity) as grn_quantity,
															sum(purchase_value*po_quantity) as po_value	
															from inward_analyzer 
															where approval_date >= 20150201 and 
																		brand_type='External' and 
																		po_type='OUTRIGHT'
															group by 1
															order by 1)
				where (lower(po_status) in ('approved','completed') or (lower(po_status) in ('closed') and po_quantity != 0 ))
group by 1,2)  con
left join ctg_cm_otb_mapping cm_otb
on con.category_manager=cm_otb.category_manager
				group by 1) b
on a.cm_otb=b.cm_otb 
order by 1
