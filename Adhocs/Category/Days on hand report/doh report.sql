Select a.*,b.last_30_days_rev,b.last_30_days_cogs,round(total_live_inventory_value::decimal(10,2)/last_30_days_cogs::decimal(10,2)*30) as doh_cost
from
(select 
		fpp.category_manager,
		fpp.brand,
		fpp.article_type,
		fpp.gender,
		commercial_type,
		sum(total_live_inventory) as total_live_inventory ,
		round(sum(total_live_inventory_value)) as total_live_inventory_value ,
		sum(last_30_days_sales) as last_30_days_sales ,
		round((sum(total_live_inventory)::decimal(10,2)/sum(last_30_days_sales)::decimal(10,2))*30) as doh_units
from 
		fact_product_profile fpp,
		dim_product dp 
where fpp.sku_id=dp.sku_id and report_date=to_char(sysdate,'YYYYMMDD') and fpp.brand_type='External' and lower(commercial_type) in ('outright','sor')  
group by 1,2,3,4,5
having 
			sum(total_live_inventory)>0 
order by 1,2,3,4,5) a
left join
		(select dp.category_manager,
						brand,
						article_type,
						gender,
						commercial_type,
						sum(cogs) as last_30_days_cogs,
						round(sum(item_revenue_inc_cashback)) as last_30_days_rev
			from fact_orderitem fo,dim_product dp
			where fo.sku_id=dp.sku_id and is_realised=1 and order_created_date>=to_char(sysdate-interval '30 days','YYYYMMDD')
			group by 1,2,3,4,5) b
on a.category_manager=b.category_manager and a.brand=b.brand and a.article_type=b.article_type and a.gender=b.gender and a.commercial_type=b.commercial_type
where last_30_days_rev>0
order by last_30_days_rev desc;

