select  
  report_date,d.brand_type,d.master_category, 
  d.category_head, d.category_manager, d.sub_category,  d.brand,d.article_type,
  d.commercial_type, d.season_code,d.season,d.year,d.gender, d.supply_type,d.business_group,d.usage_attr, d.occasion_attr,
  sum(nvl(saleable_inv,0)) as saleable_inv,
  sum(nvl(total_q1_inv_qty*30))/sum(NULLIF(last_30_days_sales,0)) as doh
from 
	(select report_date,sku_id ,sum(total_q1_inv_qty) as total_q1_inv_qty,sum(last_30_days_sales) as last_30_days_sales from fact_product_profile 
	where report_date=to_char(sysdate - interval '1 days','YYYYMMDD') group by 1,2) fpp
	left join
	(SELECT 	fii.sku_id,date,sum(inv_count)-sum(blocked_order_count) as saleable_inv
		  FROM fact_inventory_count fii  JOIN dim_product p ON fii.sku_id = p.sku_id
		 	WHERE     fii.quality = 'Q1'
       AND fii.date =to_char(sysdate - interval '1 day','YYYYMMDD')
       AND p.master_category NOT IN ('Free Items')
       AND p.style_status NOT IN ('LQD', 'RTV','SCR', 'FRG','PRT')
       AND fii.warehouse_id not in (24)
       group by 1,2) inv	
	on fpp.report_date=inv.date and fpp.sku_id=inv.sku_id
	left join 
						  (select
						 	o.sku_id,
							order_created_date,
							sum(o.quantity) as Sales_qty,
  						sum(case o.product_discount when 0 then nvl(o.quantity,0) else 0 end) as Sales_wo_tradedisc,
						  sum(case (o.product_discount+o.coupon_discount) when 0 then o.quantity else 0 end) as Sales_atFullPrice,
						  sum(o.item_mrp_value) as MRP_Value,
						  sum(o.coupon_discount) as Coupon_Disc,
						  sum(o.product_discount) as Trade_Disc,
						  sum(o.cart_discount) as Cart_Disc,
						  sum(o.vendor_funding) as Vendor_Disc,
						  sum(d.article_mrp) as article_mrp,
						  sum(d.article_mrp_excl_tax) as Article_Value_posttax,
						  sum(o.cogs) as cogs,
						  sum(o.item_revenue) as Item_Revenue,
						  sum(o.item_revenue_inc_cashback) as Booking_Value,
						  sum(o.item_revenue_inc_cashback/(1+o.tax_rate/100)) as Sales_Value_extax,
						  sum((o.item_revenue_inc_cashback+o.coupon_discount)/(1+o.tax_rate/100)) as Sales_Value_extax_coupon_adjusted
						from fact_orderitem o
						  join  
						  dim_product d on o.sku_id=d.sku_id 
						where o.order_created_date >=to_char(sysdate - interval '30 days','YYYYMMDD') and is_shipped=1 
						group by 1,2) o 
	on fpp.sku_id=o.sku_id and fpp.report_date=o.order_created_date
  join  
  dim_product d on fpp.sku_id=d.sku_id      
where d.master_category not in ('Free Items')
 group by
1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17 limit 100;
