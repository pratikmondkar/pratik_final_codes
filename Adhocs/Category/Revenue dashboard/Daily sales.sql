select  
  dt.year,dt.month, dt.month_string, dt.current_financial_year, dt.quarter, d.brand_type,d.master_category, 
  d.category_head, d.category_manager, d.sub_category,  d.brand,d.article_type,
  d.commercial_type, d.season_code,d.season,d.year,d.gender, d.supply_type,d.business_unit,d.usage_attr,
  sum(o.quantity) as Sales_qty,
  sum(case o.product_discount when 0 then o.quantity else 0 end) as Sales_wo_tradedisc,
  sum(case (o.product_discount+o.coupon_discount) when 0 then o.quantity else 0 end) as Sales_atFullPrice,
  sum(o.item_mrp_value) as MRP_Value,
  sum(o.coupon_discount) as Coupon_Disc,
  sum(o.product_discount) as Trade_Disc,
  sum(o.cart_discount) as Cart_Disc,
  sum(o.vendor_funding) as Vendor_Disc,
  sum(d.article_mrp) as Article_MRP,
  sum(d.article_mrp_excl_tax) as Article_Value_posttax,
  sum(o.cogs) as cogs,
  sum(o.item_revenue) as Item_Revenue,
  sum(o.item_revenue_inc_cashback) as Booking_Value,
  sum(o.item_revenue_inc_cashback/(1+o.tax_rate/100)) as Sales_Value_extax,
  sum((o.item_revenue_inc_cashback+o.coupon_discount)/(1+o.tax_rate/100)) as Sales_Value_extax_coupon_adjusted
from 
  fact_orderitem o, dim_product d, dim_date dt
where 
     o.is_realised=1  
     and o.idproduct = d.id
	 and dt.full_date =  o.order_created_date
     and o.order_created_date>= 20130701 and o.order_created_date<20150401
	 and d.master_category not in ('Free Items')
 group by
dt.year,dt.month, dt.month_string, dt.current_financial_year, dt.quarter, d.brand_type,d.master_category, 
d.category_head, d.category_manager, d.sub_category,  d.brand,d.article_type,
d.commercial_type, d.season_code,d.season,d.year,d.gender, d.supply_type,d.business_unit,d.usage_attr
