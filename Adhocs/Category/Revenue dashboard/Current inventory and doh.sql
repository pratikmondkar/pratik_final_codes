select  
  report_date,d.brand_type,d.master_category, 
  d.category_head, d.category_manager, d.sub_category,  d.brand,d.article_type,
  d.commercial_type, d.season_code,d.season,d.year,d.gender, d.supply_type,d.business_group,d.usage_attr, d.occasion_attr,
  sum(nvl(saleable_inv,0)) as saleable_inv,
  sum(nvl(total_q1_inv_qty*30)) as total_q1_inv,
  sum(nvl(last_30_days_sales,0)) as last_30_days_sales,
  sum(nvl(total_inwards_qty,0)) as total_inwards_qty,
  sum(nvl(till_date_sales,0)) as till_date_sales
from 
	(select report_date,sku_id ,sum(total_q1_inv_qty) as total_q1_inv_qty,sum(last_30_days_sales) as last_30_days_sales,sum(total_inwards_qty) as total_inwards_qty,
	sum(till_date_sales) as till_date_sales from fact_product_profile 
	 group by 1,2) fpp
	left join
	(SELECT 	fii.sku_id,date,sum(inv_count)-sum(blocked_order_count) as saleable_inv
		  FROM fact_inventory_count fii  JOIN dim_product p ON fii.sku_id = p.sku_id
		 	WHERE     fii.quality = 'Q1'
       AND fii.date =to_char(sysdate - interval '1 day','YYYYMMDD')
       AND p.master_category NOT IN ('Free Items')
       AND p.style_status NOT IN ('LQD', 'RTV','SCR', 'FRG','PRT')
       AND fii.warehouse_id not in (24)
       group by 1,2) inv	
	on fpp.report_date=inv.date and fpp.sku_id=inv.sku_id
  join  
  dim_product d on fpp.sku_id=d.sku_id      
where d.master_category not in ('Free Items') and report_date=to_char(sysdate - interval '1 days','YYYYMMDD')
 group by
1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17;
