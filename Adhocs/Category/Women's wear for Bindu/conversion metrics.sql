--Registartions
select gender,count(distinct customer_login) as registrations
from dim_customer_idea
where first_login_date between 20160401 and 20170131 
group by 1;

--commerce
select dci.gender,
count(distinct case when is_first_order='Y' then idcustomer end) as acquisitions,
count(distinct idcustomer) as customers,
sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as rev,
sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)-nvl(tax,0) - nvl(cogs,0) - nvl(entry_tax,0) - nvl(royalty_commission,0) - nvl(stn_input_vat_reversal,0)) as rgm,
count(distinct order_group_id) as orders,
sum(quantity) as qty,
sum(nvl(product_discount,0)+nvl(coupon_discount,0)) as total_disc,
sum(item_mrp_value*quantity) as mrp_total
FROM fact_core_item fci
join dim_customer_idea dci on fci.idcustomer=dci.id
WHERE store_id = 1		
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date BETWEEN 20160401 and 20170131
group by 1;

--clickstream
select dci.gender,
count(distinct uidx) as users,
sum(sessions) as sessions,
sum(lp_views) as lp_views,
sum(all_pdp_views) as pdp_views,
sum(added_to_cart) as atc
FROM clickstream.daily_aggregates da
join bidb.dim_customer_idea dci on da.uidx=dci.customer_login
WHERE load_date BETWEEN 20160401 and 20170131
group by 1;

--revenue metrics by category
select (CASE WHEN dp.gender = 'Women' AND (dp.sub_category IN ('Loungewear and Nightwear','Innerwear') OR (dp.business_unit LIKE 'Women%' AND dp.business_unit LIKE '%Innerwear')) THEN 'Women''s Innerwear, Loungewear & Nightwear'
            WHEN dp.gender = 'Women' AND dp.sub_category = 'Bags' THEN 'Women''s Bags'
            WHEN dp.gender = 'Women' AND (dp.sub_category LIKE '%Jewellery%' OR dp.business_unit = 'Jewellery') THEN 'Jewellery'
            WHEN dp.sub_category NOT IN ('Loungewear and Nightwear','Innerwear','Bags','Jewellery','Precious Jewellery') AND dp.business_unit IN ('Accessories','International Brands','Women''s Ethnic','Women''s Footwear','Women''s Western Wear','Sports') THEN dp.gender|| ' - ' ||dp.business_unit
            WHEN dp.business_unit IN ('Home Furnishing','Kids Wear','Men''s Casual','Men''s Casual Footwear','Men''s LTA','Personal Care') THEN dp.business_unit
            END) AS product_division,
count(distinct case when is_first_order='Y' then idcustomer end) as acquisitions,
count(distinct idcustomer) as customers,
sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as rev,
sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)-nvl(tax,0) - nvl(cogs,0) - nvl(entry_tax,0) - nvl(royalty_commission,0) - nvl(stn_input_vat_reversal,0)) as rgm,
count(distinct order_group_id) as orders,
sum(quantity) as qty,
sum(nvl(product_discount,0)+nvl(coupon_discount,0)) as total_disc,
sum(item_mrp_value*quantity) as mrp_total
FROM fact_core_item fci
join dim_product dp on fci.sku_id=dp.sku_id
WHERE store_id = 1		
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date BETWEEN 20160601 and 20170131
and 	dp.supply_type='ON_HAND' and dp.brand_type='Private'
AND   ((dp.sub_category IN ('Loungewear and Nightwear','Innerwear','Bags','Jewellery','Precious Jewellery') AND dp.gender = 'Women')
                OR (dp.business_unit IN ('Accessories','International Brands','Jewellery','Women''s Ethnic','Women''s Footwear','Women''s Innerwear','Women''s Western Wear','Sports') AND dp.gender = 'Women')
                OR dp.business_unit IN ('Home Furnishing','Kids Wear','Men''s Casual','Men''s Casual Footwear','Men''s LTA','Personal Care'))
group by 1; 

--clickstream metrics by category
select (CASE WHEN dp.gender = 'Women' AND (dp.subcategory IN ('Loungewear and Nightwear','Innerwear') OR (dp.business_unit LIKE 'Women%' AND dp.business_unit LIKE '%Innerwear')) THEN 'Women''s Innerwear, Loungewear & Nightwear'
            WHEN dp.gender = 'Women' AND dp.subcategory = 'Bags' THEN 'Women''s Bags'
            WHEN dp.gender = 'Women' AND (dp.subcategory LIKE '%Jewellery%' OR dp.business_unit = 'Jewellery') THEN 'Jewellery'
            WHEN dp.subcategory NOT IN ('Loungewear and Nightwear','Innerwear','Bags','Jewellery','Precious Jewellery') AND dp.business_unit IN ('Accessories','International Brands','Women''s Ethnic','Women''s Footwear','Women''s Western Wear','Sports') THEN dp.gender|| ' - ' ||dp.business_unit
            WHEN dp.business_unit IN ('Home Furnishing','Kids Wear','Men''s Casual','Men''s Casual Footwear','Men''s LTA','Personal Care') THEN dp.business_unit
            END) AS product_division,
sum(list_sessions) as sessions,
sum(list_page_count) as list_page_count,
sum(pdp_count) as pdp_count,
sum(add_to_cart_count) as atc_count
FROM fact_style_funnel_clickstream fci
join dim_style dp on fci.style_id=dp.style_id
WHERE load_date BETWEEN 20160601 and 20170131
and 	dp.supply_type='ON_HAND' and dp.brand_type='External'
AND   ((dp.subcategory IN ('Loungewear and Nightwear','Innerwear','Bags','Jewellery','Precious Jewellery') AND dp.gender = 'Women')
                OR (dp.business_unit IN ('Accessories','International Brands','Jewellery','Women''s Ethnic','Women''s Footwear','Women''s Innerwear','Women''s Western Wear','Sports') AND dp.gender = 'Women')
                OR dp.business_unit IN ('Home Furnishing','Kids Wear','Men''s Casual','Men''s Casual Footwear','Men''s LTA','Personal Care'))
group by 1;
