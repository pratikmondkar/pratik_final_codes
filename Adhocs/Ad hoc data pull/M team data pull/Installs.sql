select  d.cal_year,
				d.month,
				COUNT(case when campaign_source in ('SafariBrowser',	'ChromeBrowser',	'AndroidBrowser',	'Mobile Site',	'OtherBrowsers',	'UCBrowser1') then 1 end) as msite_installs,
				COUNT(1) as Total_installs
FROM app_referral ar, dim_date d
WHERE to_char(install_date,'YYYYMMDD')=d.full_date 	AND  d.month_diff<=16 and d.month_diff>0
group by 1,2
order by 1,2;

select * from app_referral limit 10
