SELECT 			
				D.cal_year,D.month,
				count(distinct foi.idcustomer) as customers,
				sum(shipped_order_revenue_inc_cashback ) as revenue
				FROM fact_order foi
				JOIN dim_date d
				ON order_created_date=d.full_date
				WHERE  store_id=1
				AND order_created_date between 20140401 and 20150430
				AND (is_realised=1 OR is_shipped=1)
				AND foi.idcustomer in (select distinct idcustomer FROM fact_order foi	WHERE order_created_date between 20140101 and 20150430
										AND	order_channel in ('mobile-web')	AND  store_id=1	AND (is_realised=1 OR is_shipped=1) )
				AND foi.idcustomer not in (select distinct idcustomer FROM fact_order foi	WHERE order_created_date between 20140101 and 20150430
										AND	order_channel in ('mobile-app')	AND  store_id=1	AND (is_realised=1 OR is_shipped=1) )
group by 1,2
order by 1,2;

SELECT 			
				c_year,c_month,
				count(distinct foi.idcustomer) as customers,
				sum(shipped_order_revenue_inc_cashback ) as revenue
				FROM fact_order foi
				JOIN
				(SELECT distinct foi.idcustomer,D.cal_year as c_year,D.month as c_month
								FROM fact_order foi
								JOIN dim_date d
								ON order_created_date=d.full_date
								WHERE  store_id=1	AND order_created_date between 20140401 and 20150430 AND (is_realised=1 OR is_shipped=1)
								AND foi.idcustomer in (select distinct idcustomer FROM fact_order foi	WHERE order_created_date between 20140101 and 20150430
														AND	order_channel in ('mobile-web')	AND  store_id=1	AND (is_realised=1 OR is_shipped=1) )
								AND foi.idcustomer not in (select distinct idcustomer FROM fact_order foi	WHERE order_created_date between 20140101 and 20150430
														AND	order_channel in ('mobile-app')	AND  store_id=1	AND (is_realised=1 OR is_shipped=1) )) c
				ON foi.idcustomer=c.idcustomer
				WHERE  store_id=1
				AND order_created_date >20150515
				AND order_channel = 'mobile-app'
				AND (is_realised=1 OR is_shipped=1)
group by 1,2
order by 1,2;


SELECT 			
				count(distinct foi.idcustomer) as customers,
				sum(shipped_order_revenue_inc_cashback ) as revenue
				FROM fact_order foi
				JOIN dim_date d
				ON order_created_date=d.full_date
				WHERE  store_id=1
				AND order_created_date between 20150101 and 20150430
				AND (is_realised=1 OR is_shipped=1)
				AND foi.idcustomer in (select distinct idcustomer FROM fact_order foi	WHERE order_created_date between 20140101 and 20150430
										AND	order_channel in ('mobile-web')	AND  store_id=1	AND (is_realised=1 OR is_shipped=1) )
				AND foi.idcustomer not in (select distinct idcustomer FROM fact_order foi	WHERE order_created_date between 20140101 and 20150430
										AND	order_channel in ('mobile-app')	AND  store_id=1	AND (is_realised=1 OR is_shipped=1) );
										

SELECT 			
				count(distinct foi.idcustomer) as customers,
				sum(shipped_order_revenue_inc_cashback ) as revenue
				FROM fact_order foi
				JOIN
				(SELECT distinct foi.idcustomer,D.cal_year as c_year,D.month as c_month
								FROM fact_order foi
								JOIN dim_date d
								ON order_created_date=d.full_date
								WHERE  store_id=1	AND order_created_date between 20150101 and 20150430 AND (is_realised=1 OR is_shipped=1)
								AND foi.idcustomer in (select distinct idcustomer FROM fact_order foi	WHERE order_created_date between 20140101 and 20150430
														AND	order_channel in ('mobile-web')	AND  store_id=1	AND (is_realised=1 OR is_shipped=1) )
								AND foi.idcustomer not in (select distinct idcustomer FROM fact_order foi	WHERE order_created_date between 20140101 and 20150430
														AND	order_channel in ('mobile-app')	AND  store_id=1	AND (is_realised=1 OR is_shipped=1) )) c
				ON foi.idcustomer=c.idcustomer
				WHERE  store_id=1
				AND order_created_date >20150515
				AND order_channel = 'mobile-app'
				AND (is_realised=1 OR is_shipped=1)
