--Monthwise app
select	st_mon,to_char(to_date(fo.order_created_date,'YYYYMMDD'),'YYYY-MM') as mon,count(distinct fo.idcustomer) as customers,
				count(distinct fo.order_group_id) as orders, sum(fo.shipped_order_revenue_inc_cashback) as revenue
from fact_order fo,
		(select distinct to_char(install_date,'YYYY-MM') as st_mon,id from app_referral ar, app_users au,dim_customer dc
		where ar.device_id=au.android_id and au.email_id=dc.customer_login and to_char(install_date,'YYYYMMDD')>=20150201
		and campaign_source in ('SafariBrowser',	'ChromeBrowser',	'AndroidBrowser',	'Mobile Site',	'OtherBrowsers',	'UCBrowser1')) co
where fo.idcustomer=co.id and to_date(fo.order_created_date,'YYYYMMDD')>=to_date(st_mon,'YYYY-MM') and is_realised=1
			AND to_date(fo.order_created_date,'YYYYMMDD')<=TO_DATE(st_mon,'YYYY-MM') + interval '90 days' and order_channel='mobile-app'
group by 1,2
order by 1,2;

--Overall app
select	st_mon,count(distinct fo.idcustomer) as customers,
				count(distinct fo.order_group_id) as orders, sum(fo.shipped_order_revenue_inc_cashback) as revenue
from fact_order fo,
		(select distinct to_char(install_date,'YYYY-MM') as st_mon,id from app_referral ar, app_users au,dim_customer dc
		where ar.device_id=au.android_id and au.email_id=dc.customer_login and to_char(install_date,'YYYYMMDD')>=20150201
		and campaign_source in ('SafariBrowser',	'ChromeBrowser',	'AndroidBrowser',	'Mobile Site',	'OtherBrowsers',	'UCBrowser1')) co
where fo.idcustomer=co.id and to_date(fo.order_created_date,'YYYYMMDD')>=to_date(st_mon,'YYYY-MM') and is_realised=1
			AND to_date(fo.order_created_date,'YYYYMMDD')<=TO_DATE(st_mon,'YYYY-MM') + interval '90 days' and order_channel='mobile-app'
group by 1
order by 1;

--Monthwise m-site
select	st_mon,to_char(to_date(fo.order_created_date,'YYYYMMDD'),'YYYY-MM') as mon,count(distinct fo.idcustomer) as customers,
				count(distinct fo.order_group_id) as orders, sum(fo.shipped_order_revenue_inc_cashback) as revenue
from fact_order fo,
		(select distinct to_char(install_date,'YYYY-MM') as st_mon,id from app_referral ar, app_users au,dim_customer dc
		where ar.device_id=au.android_id and au.email_id=dc.customer_login and to_char(install_date,'YYYYMMDD')>=20150201
		and campaign_source in ('SafariBrowser',	'ChromeBrowser',	'AndroidBrowser',	'Mobile Site',	'OtherBrowsers',	'UCBrowser1')) co
where fo.idcustomer=co.id and to_date(fo.order_created_date,'YYYYMMDD')>=to_date(st_mon,'YYYY-MM') and is_realised=1
			AND to_date(fo.order_created_date,'YYYYMMDD')<=TO_DATE(st_mon,'YYYY-MM') + interval '90 days' and order_channel='mobile-web'
group by 1,2
order by 1,2;

--Overall m-site
select	st_mon,count(distinct fo.idcustomer) as customers,
				count(distinct fo.order_group_id) as orders, sum(fo.shipped_order_revenue_inc_cashback) as revenue
from fact_order fo,
		(select distinct to_char(install_date,'YYYY-MM') as st_mon,id from app_referral ar, app_users au,dim_customer dc
		where ar.device_id=au.android_id and au.email_id=dc.customer_login and to_char(install_date,'YYYYMMDD')>=20150201
		and campaign_source in ('SafariBrowser',	'ChromeBrowser',	'AndroidBrowser',	'Mobile Site',	'OtherBrowsers',	'UCBrowser1')) co
where fo.idcustomer=co.id and to_date(fo.order_created_date,'YYYYMMDD')>=to_date(st_mon,'YYYY-MM') and is_realised=1
			AND to_date(fo.order_created_date,'YYYYMMDD')<=TO_DATE(st_mon,'YYYY-MM') + interval '90 days' and order_channel='mobile-web'
group by 1
order by 1;

--Installs
select distinct to_char(install_date,'YYYY-MM') as st_mon,count(1) as installs from app_referral ar
		where to_char(install_date,'YYYYMMDD')>=20150201
		and campaign_source in ('SafariBrowser',	'ChromeBrowser',	'AndroidBrowser',	'Mobile Site',	'OtherBrowsers',	'UCBrowser1') group by 1;
