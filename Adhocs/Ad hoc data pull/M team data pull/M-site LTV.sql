select c_year,c_month,cal_year,month,count(distinct idcustomer) as cus,sum(rev_msite) as revenue
FROM
(SELECT 			
				foi.idcustomer,D.cal_year,D.month,max(c.c_year) as c_year,max(c.c_month) as c_month,
				count(distinct case when order_channel='mobile-web' and (order_created_date<e.end_date or end_date is NULL) then order_group_id end) as order_msite,
				sum( case when order_channel='mobile-web' and (order_created_date<e.end_date or end_date is NULL) then shipped_order_revenue_inc_cashback end) as rev_msite
				FROM fact_order foi
				JOIN dim_date d
				ON order_created_date=d.full_date
				JOIN (select distinct idcustomer,d.cal_year as c_year,d.month as c_month,full_date as st_date FROM fact_order foi, dim_date d	
							WHERE order_created_date=d.full_date AND order_created_date>=20140101 AND	order_channel in ('mobile-web')	AND  store_id=1	
							AND (is_realised=1 OR is_shipped=1) and purchase_type='f') c
				ON foi.idcustomer = c.idcustomer and order_created_date>=c.st_date
				left JOIN (select idcustomer,min(order_created_date) as end_date FROM fact_order foi, dim_date d	
							WHERE order_created_date=d.full_date AND order_created_date>=20140101 AND	order_channel in ('mobile-app','web')	AND  store_id=1	
							AND (is_realised=1 OR is_shipped=1) group by 1) e
				ON foi.idcustomer = e.idcustomer
				WHERE  store_id=1
				AND (is_realised=1 OR is_shipped=1)
group by 1,2,3)
where order_msite>0
group by 1,2,3,4
order by 1,2,3,4
limit 1000;


select d.cal_year,d.month,count( distinct idcustomer) as acq 
FROM fact_order foi, dim_date d	
WHERE order_created_date=d.full_date AND order_created_date>=20140101 AND
order_channel in ('mobile-web')	AND store_id=1	AND (is_realised=1 OR is_shipped=1) and purchase_type='f'
group by 1,2
order by 1,2
