select a.*,nvl(b.gm,0) as gm,nvl(b.acq_cnt,0) as acquisitions,nvl(b.acq_rev,0) as acq_rev
from
				(SELECT 
				order_created_date,
				order_channel,
				SUM(shipped_order_revenue_inc_cashback) as revenue,
				COUNT(DISTINCT order_group_id) as orders,
				COUNT(DISTINCT idcustomer) as customers,
				ROUND(SUM(shipped_order_revenue_inc_cashback)/COUNT(DISTINCT order_group_id)::decimal(12,2),2) as avg_ord_value
				FROM fact_order foi
				WHERE order_created_date in (20150103,20150314,20150530,20150718)
				AND  store_id=1
				AND (is_realised=1 OR is_shipped=1)
				group by 1,2
				order by 1,2) a
left join 
				(SELECT 
				order_created_date,
				order_channel,
				ROUND(SUM(rgm)*100/(SUM(Item_revenue_inc_cashback)-sum(tax)),2) as gm,
				COUNT(DISTINCT CASE WHEN purchase_type='f' THEN idcustomer END) AS acq_cnt,
				SUM(CASE WHEN purchase_type='f' THEN Item_revenue_inc_cashback END) AS acq_rev
				FROM fact_orderitem foi
				WHERE order_created_date in (20150103,20150314,20150530,20150718)
				AND  store_id=1
				AND (is_realised=1 OR is_shipped=1)
				group by 1,2
				order by 1,2) b
on a.order_created_date=b.order_created_date and a.order_channel=b.order_channel
where a.order_channel is not NULL
order by 1,2;

SELECT 
				order_created_date,
				COUNT(DISTINCT idcustomer) as customers
				FROM fact_order foi, dim_date d
				WHERE order_created_date=d.full_date 
				AND  store_id=1 and order_created_date in (20150103,20150314,20150530,20150718)
				AND (is_realised=1 OR is_shipped=1)
				group by 1
				order by 1;
				
SELECT 
				order_created_date,
				CASE WHEN (Upper(dl.city_group) IN ('CHENNAI', 'DELHI-NCR', 'HYDERABAD', 'BANGALORE', 'MUMBAI', 'PUNE')) THEN 'METRO' else 'NON-METRO' end as city_group,
				COUNT(DISTINCT idcustomer) as customers
				FROM fact_order foi, dim_date d, dim_location dl
				WHERE order_created_date=d.full_date and idlocation=dl.id
				AND  store_id=1 and order_created_date in (20150103,20150314,20150530,20150718)
				AND (is_realised=1 OR is_shipped=1)
				group by 1,2
				order by 1,2
