select count(distinct a.device_id) as old_version_users,
count(distinct case when b.device_id is not null and curr_version='3.4.2' then a.device_id end) as migrated_users,
count(distinct case when b.device_id is not null and curr_version='2.6.1' then a.device_id end) as still_on_old_version    
from
(select distinct device_id 
from clickstream.events_view 
where event_type in ('push-notification-received','beacon-ping','appLaunch') and os='Android' 
and load_date between 20160610 and 20160613 and app_version ='2.6.1') a
left join 
(select device_id,max(app_version) as curr_version 
from clickstream.events_view 
where event_type in ('push-notification-received','beacon-ping','appLaunch') and os='Android' 
and load_date >20160613
group by 1) b
on a.device_id=b.device_id
