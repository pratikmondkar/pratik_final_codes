select to_char(created_date,'YYYYMMDD-HH24') as hour,
count(distinct device_id) as devices_mag,
count(distinct case when created_datetime is not null then device_id end) as pr_tune,
count(distinct case when diff =0 then device_id end) as same_day_installs,
count(distinct case when diff between 0 and 7 then device_id end) as seven_day_installs  
from
(select a.device_id,a.advertising_id,created_date,
--convert_timezone('Asia/Calcutta',(TIMESTAMP 'epoch' + atlas_ts/1000 * INTERVAL '1 Second ')) as atlas_ts,convert_timezone('Asia/Calcutta',(TIMESTAMP 'epoch' + client_ts/1000 * INTERVAL '1 Second ')) as client_ts,
created_datetime,install_datetime, datediff(d,created_datetime,created_date) as diff from
(select distinct device_id,device_type,installation_id,advertising_id,created_date 
from app_users_magasin 
where created_date between '2017-08-15 00:00:00' and '2017-08-15 23:59:59' and device_type='Android') a
--left join (select device_id,gaid,installation_id,min(atlas_ts) as atlas_ts,min(client_ts) as client_ts from clickstream.events_2017_08_15 where event_type='install' group by 1,2,3) b on a.device_id=b.device_id
left join (select device_id,created_datetime,install_datetime from app_install_tune ) c on a.advertising_id=c.device_id)
group by 1
;

select to_char(created_datetime,'YYYYMMDD-HH24') as hour,
count(distinct device_id) as devices_tune,
count(distinct case when created_date is not null then device_id end) as pr_mag,
count(distinct case when diff =0 then device_id end) as same_day_installs,
count(distinct case when diff between 0 and 7 then device_id end) as seven_day_installs  
from
(select c.device_id,a.advertising_id,created_date,
created_datetime,install_datetime, datediff(d,created_date,created_datetime) as diff from
 app_users_magasin  a
right join (select device_id,created_datetime,install_datetime from app_install_tune where created_datetime between '2017-08-15 00:00:00' and '2017-08-15 23:59:59' and keyspace='GAID') c on a.advertising_id=c.device_id)
group by 1
;
