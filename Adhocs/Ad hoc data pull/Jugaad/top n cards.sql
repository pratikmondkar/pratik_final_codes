select a.*,b.clicks from
(select fdv.event_whom,event_card_type,nvl(cs.segment,'Unknown') as segment,nvl(case when dc.gender not in ('M','F') then dc.fb_gender else dc.gender end,'U') as gender,count(1) as impressions
from clickstream.feedcardload_view fdv
left join dim_customer dc
on fdv.user_customer_id=dc.customer_login
left join customer_segmentation.segmentation_jan16 cs
on fdv.user_customer_id=cs.customer_login
join clickstream.top_feedcards_feb tf
on fdv.event_whom=tf.event_whom
where fdv.load_date between 20160216 and 20160222 
group by 1,2,3,4) a
left join 
(select event_card_type,fdv.event_whom,nvl(cs.segment,'Unknown') as segment,nvl(case when dc.gender not in ('M','F') then dc.fb_gender else dc.gender end,'U') as gender,count(1) as clicks
from clickstream.feedcardclick_view fdv
left join dim_customer dc
on fdv.user_customer_id=dc.customer_login
left join customer_segmentation.segmentation_jan16 cs
on fdv.user_customer_id=cs.customer_login
join clickstream.top_feedcards_feb tf
on fdv.event_whom=tf.event_whom
where fdv.load_date between 20160216 and 20160222
group by 1,2,3,4) b
on a.event_whom=b.event_whom and a.segment=b.segment and a.gender=b.gender
