select a.*,b.max_click_position from
(select device_device_id,json_extract_path_text(abtest, 'lgp.stream') as ab,max(event_position) as max_imp_position
from clickstream.feedcardload_view fdv
where load_date between 20160304 and 20160306 and app_version='3.0.0'
group by 1,2) a
left join 
(select device_device_id,max(event_position) as max_click_position
from clickstream.feedcardclick_view fdv
where load_date between 20160304 and 20160306 and app_version='3.0.0'
group by 1) b
on a.device_device_id=b.device_device_id;


