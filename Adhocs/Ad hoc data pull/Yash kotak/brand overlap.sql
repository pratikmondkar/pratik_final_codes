select a.*,b.total_FM_customers from
(SELECT brand,
       COUNT(DISTINCT fci.idcustomer) AS overlap_customers
FROM fact_core_item fci
  JOIN (SELECT DISTINCT idcustomer
        FROM fact_core_item
        WHERE store_id = 1
        AND   (is_shipped = 1 OR is_realised = 1)
        AND   order_created_date >= TO_CHAR(sysdate -INTERVAL '3 months','YYYYMMDD')::BIGINT
        and article_type='Tshirts' and gender='Men' and brand ='Flying Machine') b on fci.idcustomer = b.idcustomer
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date >= TO_CHAR(sysdate -INTERVAL '3 months','YYYYMMDD')::BIGINT
and article_type='Tshirts' and gender='Men' and brand in ('Jack & Jones','Wrangler','Pepe Jeans','Wrogn','Lee','Moda Rapido','Roadster','Levis','Mast & Harbour','United Colors of Benetton')
group by 1) a,
(SELECT count(DISTINCT idcustomer) as total_FM_customers
        FROM fact_core_item
        WHERE store_id = 1
        AND   (is_shipped = 1 OR is_realised = 1)
        AND   order_created_date >= TO_CHAR(sysdate -INTERVAL '3 months','YYYYMMDD')::BIGINT
        and article_type='Tshirts' and gender='Men' and brand ='Flying Machine') b

 
