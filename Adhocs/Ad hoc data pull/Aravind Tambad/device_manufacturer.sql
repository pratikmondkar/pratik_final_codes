select load_date,
count(distinct device_id) as ovr_users,
count(distinct case when lower(device_manufacturer)='oneplus' then device_id end) as op_users,
count(distinct case when lower(device_manufacturer)='vivo' then device_id end) as op_users,
count(distinct case when lower(device_manufacturer)='google' or (device_manufacturer in ('LGE','Google','Huawei','motorola') and device_model_number ilike '%nexus%') then device_id end) as gl_users
from clickstream.events_view 
where event_type in ('appLaunch','ScreenLoad','streamCardLoad','streamCardChildLoad')
group by 1;


select load_date,
device_manufacturer,
device_model_number,
count(distinct device_id) as ovr_users
from clickstream.events_view 
where event_type in ('appLaunch','ScreenLoad','streamCardLoad','streamCardChildLoad')
group by 1,2,3
having count(distinct device_id)>100;
