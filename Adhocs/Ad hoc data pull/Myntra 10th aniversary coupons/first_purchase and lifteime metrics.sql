create table dev.inter123 distkey(uidx) as 
(SELECT dci.customer_login AS uidx,
       a.orders,
       a.total_disc,
       a.items,
       c.add_to_wishlist,
       d.brand as preferred_brand,
       e.article_type as preferred_article_type
FROM (SELECT idcustomer_idea,
             COUNT(DISTINCT order_group_id) AS orders,
             SUM(nvl (coupon_discount,0) + nvl (product_discount,0)) AS total_disc,
             SUM(item_count) AS items
      FROM fact_order
      WHERE store_id = 1
      AND   is_realised = 1
      group by 1) a
  JOIN (SELECT DISTINCT idcustomer
        FROM fact_core_item
        WHERE store_id = 1
        AND   (is_shipped = 1 OR is_realised = 1)
        AND   order_created_date >= TO_CHAR(SYSDATE-INTERVAL '6 months','YYYYMMDD')::BIGINT) b ON a.idcustomer_idea = b.idcustomer
  JOIN dim_customer_idea dci ON a.idcustomer_idea = dci.id
  LEFT JOIN (SELECT uidx,
                    SUM(add_to_list) AS add_to_wishlist
             FROM clickstream.daily_aggregates
             GROUP BY 1) c ON dci.customer_login = c.uidx
  LEFT JOIN (SELECT user_id,
                    brand
             FROM (SELECT user_id,
                          brand,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY score DESC) AS rnk
                   FROM notif_person_base_lifetime
                   WHERE identifier = 'uidx'
                   GROUP BY 1,
                            2)
             WHERE rnk = 1) d ON dci.customer_login = d.user_id
  LEFT JOIN (SELECT user_id,
                    article_type
             FROM (SELECT user_id,
                          article_type,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY score DESC) AS rnk
                   FROM notif_person_base_lifetime
                   WHERE identifier = 'uidx'
                   GROUP BY 1,
                            2)
             WHERE rnk = 1) e ON dci.customer_login = e.user_id);
             

create table dev.inter234 distkey(uidx) as 
(SELECT dci.customer_login as uidx,
       fo.order_created_date as first_order_date,
       ds.sub_category as first_order_product_subcategory,
       ds.article_type as first_order_product_article_type,
       ds.brand as first_order_product_brand
FROM (SELECT idcustomer_idea,
             order_group_id,
             order_created_date
      FROM (SELECT DISTINCT idcustomer_idea,
                   order_group_id,
                   order_created_date,
                   ROW_NUMBER() OVER (PARTITION BY idcustomer_idea ORDER BY order_created_date,order_created_time) AS rnk
            FROM bidb.fact_order
            WHERE store_id = 1
            AND   is_realised = 1)
      WHERE rnk = 1) fo
  JOIN (SELECT idcustomer_idea,
               sku_id
        FROM (SELECT DISTINCT idcustomer_idea,
                     sku_id,
                     ROW_NUMBER() OVER (PARTITION BY idcustomer_idea ORDER BY order_created_date,order_created_time,item_revenue_inc_cashback DESC) AS rnk 
             FROM bidb.fact_orderitem
        WHERE store_id = 1
        AND   is_realised = 1)
        where rnk=1) foi
       on fo.idcustomer_idea =foi.idcustomer_idea
   join dim_product ds on foi.sku_id=ds.sku_id
	 join dim_customer_idea dci on fo.idcustomer_idea=dci.id);

select a.*,first_order_date,first_order_product_subcategory,first_order_product_article_type,first_order_product_brand
from dev.inter123 a
left join 
dev.inter234 b on a.uidx=b.uidx
limit 100
