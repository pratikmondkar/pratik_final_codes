SELECT COUNT(DISTINCT a.device_id) AS nai,
       COUNT(DISTINCT CASE WHEN b.device_id IS NULL THEN a.device_id END) AS uninstalls
FROM (SELECT DISTINCT device_id
      FROM clickstream.events_view
      WHERE event_type IN ('push-notification-received','beacon-ping','appLaunch') and os='Android'
      AND   load_date BETWEEN 20160513 AND 20160519) a -- 7 days net active install base
  LEFT JOIN (SELECT DISTINCT device_id
             FROM clickstream.events_view
             WHERE event_type IN ('push-notification-received','beacon-ping','appLaunch') and os='Android'
             AND   load_date BETWEEN 20160520 AND 20160522) b -- 2 days uninstalls
  ON a.device_id = b.device_id;




SELECT COUNT(DISTINCT a.device_id) AS nai,
       COUNT(DISTINCT CASE WHEN b.device_id IS NULL THEN a.device_id END) AS uninstalls
FROM (SELECT DISTINCT device_id
      FROM clickstream.events_view
      WHERE event_type IN ('push-notification-received','beacon-ping','appLaunch') and os='Android'
      AND   load_date BETWEEN 20160401 AND 20160407) a -- 7 days net active install base
  LEFT JOIN (SELECT DISTINCT device_id
             FROM clickstream.events_view
             WHERE event_type IN ('push-notification-received','beacon-ping','appLaunch') and os='Android'
             AND   load_date BETWEEN 20160408 AND 20160410) b -- 2 days uninstalls
  ON a.device_id = b.device_id
