select t1.*,d.first_name,d.last_name,d.gender,e.phone_number,revenue from
(select distinct install_date,user_id,keyspace,campaign_source
				FROM bidb.app_referral r
				  left JOIN bidb.app_users_magasin a ON r.device_id = a.device_id
				  where keyspace='ANDI' and install_date between '2016-06-13 00:00:00' and '2016-06-19 23:59:59' and attribution = 'Install'
				  union
				 select install_date,user_id,keyspace,campaign_source
				FROM bidb.app_referral r
				  left JOIN bidb.app_users_magasin a ON r.device_id = a.advertising_id
				  where  keyspace<>'ANDI' and install_date between '2016-06-13 00:00:00' and '2016-06-19 23:59:59' and attribution = 'Install') t1 
  join bidb.dim_customer_idea d on t1.user_id=d.customer_login
	join (select distinct idcustomer_idea,max(shipping_contact_number) as phone_number,
							sum(shipped_order_revenue_inc_cashback) as revenue from bidb.fact_order 
				where order_created_date between 20160613 and 20160619 and store_id=1 and (is_realised=1 or is_shipped=1 and purchase_type='f')
				group by 1) e
	on d.id=e.idcustomer_idea
