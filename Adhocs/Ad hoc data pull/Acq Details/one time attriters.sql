drop table if exists dev.pm_attriters;

create table dev.pm_attriters distkey(uidx) sortkey(order_created_date) as
select txn.*,case when nvl(rep.orders,0)>0 then 1 else 0 end as rep_cus_flag,ful.source_of_first_install
from
              (select distinct  dci.customer_login as uidx, fci.idcustomer, fci.order_created_date,dl.statename,ct.city_tier,is_rto,is_returned,supply_type,
              case when device_channel='mobile-app' then os_info else device_channel end as channel,brand,article_type,
              sum(item_revenue_inc_cashback) as rev,sum(quantity) as units_sold,
              sum(nvl(product_discount,0)+nvl(coupon_discount,0)) as disc
              from fact_core_item fci
              join dim_customer_idea dci on fci.idcustomer = dci.id
              join dim_location dl on fci.idlocation=dl.id
              left join city_tier_mapping ct on dl.city_name=ct.city_name
              where order_created_date between 20161001 and 20170725
              and is_first_order = 'Y'
              and store_id = 1 
              and device_channel = 'mobile-app'
              and (is_shipped = 1 or is_realised = 1)
              group by 1,2,3,4,5,6,7,8,9,10,11) txn

left join 

           (select idcustomer, count(distinct order_group_id) as orders
           from fact_core_item
            where order_created_date between 20161001 and 20170725
            and is_first_order = 'N'
            and store_id = 1            
            and (is_shipped = 1 or is_realised = 1)
            group by 1) rep
                    
            on txn.idcustomer = rep.idcustomer
            
left join fact_user_lifetime ful on txn.uidx=ful.user_id and ful.user_id_identifier='uidx';



select case when disc< 0.1 then '<10%' 
when disc between 0.1 and 0.19999 then '10%-20%'
when disc between 0.2 and 0.29999 then '20%-30%'
when disc between 0.3 and 0.39999 then '30%-40%'
when disc between 0.4 and 0.49999 then '40%-50%'
when disc between 0.5 and 0.59999 then '50%-60%'
when disc between 0.6 and 0.69999 then '60%-70%'
when disc >=0.7 then '70%+'
else 'unknown' end as disc_bucket,rep_cus_flag,count(distinct uidx) as acq 
from
(select uidx,rep_cus_flag,nvl(sum(disc)/nullif(sum(nvl(disc,0)+nvl(rev,0)),0),0) as disc
from dev.pm_attriters a
--left join dev.demodemo d on a.article_type=d.article
 group by 1,2)
 group by 1,2 limit 1000;

select case when rev/nullif(units_sold,0)<= exonomy then 'economy' 
when rev/nullif(units_sold,0) >exonomy and rev/nullif(units_sold,0)<=mass then 'mass'
when rev/nullif(units_sold,0) >mass and rev/nullif(units_sold,0)<=premium then 'premium'
when rev/nullif(units_sold,0) >premium and rev/nullif(units_sold,0)<=blt then 'blt'
when rev/nullif(units_sold,0)> luxury then 'luxury' 
else 'unknown' end as price_point,rep_cus_flag,count(distinct uidx) as acq 
from dev.pm_attriters a
left join dev.demodemo d on a.article_type=d.article
 group by 1,2 limit 1000;

select case when is_rto=1 or is_returned=1 then 1 else 0 end as r_flag,rep_cus_flag,count(distinct uidx) as acq 
from dev.pm_attriters 
group by 1,2 
