select 	foi.order_created_date, 
				sum(foi.quantity) as qty, 
				sum(foi.item_revenue_inc_cashback) as rev,
				count(distinct order_group_id) as orders,
				count(distinct idcustomer) as customers,
				sum(foi.quantity)/NULLIF(count(distinct order_group_id),0)::decimal(10,2) as units_per_order,
				sum(foi.item_revenue_inc_cashback)/NULLIF(count(distinct order_group_id),0) as rev_per_order,
				(sum(item_mrp_value*quantity) - sum(item_revenue_inc_cashback))/NULLIF(sum(item_mrp_value*quantity),0) as avg_disc_percent,
				sum(rgm)/(sum(item_revenue_inc_cashback)-sum(tax)) as gm
								from fact_orderitem foi 
								where foi.order_created_date >=20140101
								and (foi.is_shipped = 1 or foi.is_realised = 1)
group by 1
order by 1
