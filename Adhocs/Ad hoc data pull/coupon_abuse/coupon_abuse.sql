drop table if exists dev.pm_coupon_abuse;
create table dev.pm_coupon_abuse distkey(idcustomer) as 
select a.*  
,nvl(gross_gmv) - nvl (cancellation_gmv,0) - nvl (rto_gmv,0) - nvl (return_gmv,0) as net_gmv,
case when coupon_discount > 0 and gross_gmv>coupon_amount and nvl(gross_gmv,0) - nvl (cancellation_gmv,0) - nvl (rto_gmv,0) - nvl (return_gmv,0) < coupon_amount then 1 else 0 end as coupon_misuse_flag, -- if coupon availed and net gmv is < 0.8*gross_gmv)
case when gross_gmv > 1000 and nvl(gross_gmv,0) - nvl (cancellation_gmv,0) - nvl (rto_gmv,0) - nvl (return_gmv,0) < 1000 then 1 else 0 end as shipping_misuse_flag,  --(if gross_gmv > 1k and net_gmv < 1k, ordered to save shipping charges)
case when (coupon_discount > 0 and nvl(gross_gmv,0) - nvl (cancellation_gmv,0) - nvl (rto_gmv,0) - nvl (return_gmv,0) < coupon_amount) or (gross_gmv > 1000 and nvl(gross_gmv) - nvl (cancellation_gmv,0) - nvl (rto_gmv,0) - nvl (return_gmv,0) < 1000) then 1 else 0 end as problematic_order_flag
from 
(SELECT 
order_group_id, 
idcustomer,
max(oi.coupon_code) as coupon_code,
max(cl.amount) as coupon_amount,
SUM(case when is_booked = 1 then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS gross_gmv, -- gross
sum(case when is_booked = 1 then nvl(oi.coupon_discount,0) end) as coupon_discount, --nvl
max(CASE WHEN (order_canceltype = 'CCR' or item_canceltype = 'CCR')  THEN 1 ELSE 0 END) as cancellation_flag,
SUM(case when (order_canceltype = 'CCR' or item_canceltype = 'CCR') then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS cancellation_gmv, -- cancellation_rev
max(is_rto) as rto_flag,
SUM(case when is_rto =1 and order_status!= 'F' then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS rto_gmv, -- rto_rev
max(is_returned) as return_flag,
SUM(case when (oi.is_shipped = 1 OR oi.is_realised = 1) and is_returned =1 then nvl (item_revenue_inc_cashback,0) + nvl (oi.shipping_charges,0) + nvl (oi.gift_charges,0) end) AS return_gmv -- return_rev
FROM bidb.fact_core_item oi
left join dev.pm_coupon_list cl on lower(oi.coupon_code)=lower(cl.coupon_code)
WHERE oi.order_created_date between 20170401 and 20170430
AND oi.store_id = 1
and is_booked = 1
group by 1,2
) a;

select * from dev.pm_coupon_abuse limit 100;

select 
count(distinct order_group_id) as orders,
count(distinct case when net_gmv >0 and coupon_misuse_flag=1 then order_group_id end) as coupon_misuse_orders,
count(distinct case when net_gmv >0 and shipping_misuse_flag=1 then order_group_id end) as shipping_misuse_orders,
count(distinct case when net_gmv >0 and problematic_order_flag=1 then order_group_id end) as either_misuse_orders,
count(distinct idcustomer) as customers,
count(distinct case when net_gmv >0 and coupon_misuse_flag=1 then idcustomer end) as coupon_misuse_customers,
count(distinct case when net_gmv >0 and shipping_misuse_flag=1 then idcustomer end) as shipping_misuse_customers,
count(distinct case when net_gmv >0 and problematic_order_flag=1 then idcustomer end) as either_misuse_customers,
sum(gross_gmv- nvl(net_gmv,0)) as diff_gmv,
sum(case when net_gmv >0 and coupon_misuse_flag=1 then gross_gmv- nvl(net_gmv,0) end) as coupon_misuse_diff_gmv,
sum(case when net_gmv >0 and shipping_misuse_flag=1 then gross_gmv- nvl(net_gmv,0) end) as shipping_misuse_diff_gmv,
sum(case when net_gmv >0 and problematic_order_flag=1 then gross_gmv- nvl(net_gmv,0) end) as either_misuse_diff_gmv,
sum(gross_gmv) as tot_gmv,
sum(case when net_gmv >0 and coupon_misuse_flag=1 then gross_gmv end) as coupon_misuse_tot_gmv,
sum(case when net_gmv >0 and shipping_misuse_flag=1 then gross_gmv end) as shipping_misuse_tot_gmv,
sum(case when net_gmv >0 and problematic_order_flag=1 then gross_gmv end) as either_misuse_tot_gmv

 from dev.pm_coupon_abuse limit 100;
 

select sum(gross_gmv ) as gross_gmv, sum(net_gmv) as net_gmv from dev.pm_coupon_abuse;

select * from dev.pm_coupon_abuse where order_group_id=230590461;
 

select 
sum(case when net_gmv >0 and coupon_misuse_flag=1 then coupon_discount end) as coupon_misuse_cd
 from dev.pm_coupon_abuse limit 100;

 

