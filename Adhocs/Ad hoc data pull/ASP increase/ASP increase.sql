select 
month_diff,
case when fo.purchase_sequence=1 then 'F' 
			when fo.purchase_sequence>1 then 'R'
			else 'U' end as purchase_type, 
sum(shipped_order_revenue_inc_cashback) as rev,
count(distinct order_group_id) orders,
sum(item_count) as items,
sum()
from fact_order fo
join dim_date dd
on order_created_date=full_date
where month_diff<=12 and store_id=1
group by 1,2
order by 1,2
