SELECT TO_CHAR(install_date,'YYYY-MM-DD') AS DATE,
       COUNT(DISTINCT device_id) AS installs
FROM app_referral
WHERE attribution = 'Install'
--keyspace  in  'ANDI'   
AND   install_date BETWEEN '2015-10-01 00:00:00' AND '2015-10-31 23:59:59'
GROUP BY 1
ORDER BY 1;

SELECT COUNT(DISTINCT CASE WHEN campaign_source IN ('partnerships (intex)',	'Appiris',	'Facebook',	'FreePaisa',	'Claymotion',	'Momagic - Lava',	'baidu',	'partnerships (micromax)',	'Pointific Digital',	'InMobi',	'MoBrain',	'Hotstar',	'1MobileMarket (Download)',	'Instal.com',	'Crazyapp',	'OneDigitalAd Technologies',	'Clicksmob',	'FreeCharge',	'apploop_GmbH (102c644811d4d61956c3758954d525:::1409_)',	'MFB',	'Glispa',	'Adiquity-Flipkart',	'Jampp',	'Altrooz',	'bestappsmarket.com (AppDisco)',	'Flipkart-Lenovo Pre-burn',	'Google Search',	'TOI',	'Airpush',	'airtel',	'EverythingMe (SuperSonic)',	'Glispa (CD28763p3101___48949___93401___APPIA143789488311952592066600960:::CD28763_PLACEMENT)',	'flipkart',	'Clickdealer',	'Ad2campaign (j7r6c94o8igpvlvpgi6kh870q0_1438259536:::15730)',	'mobileCore',	'Direct OEM - Spice',	'partnerships (ismart)',	'Google Display (Apps)',	'partnerships (karbonn)',	'APK - Baidu4',	'Mobvista',	'Glispa (CD31143p1112532___63641___109393___2013209___b2d03617a1d0674a7a8b5ab0eb9e30e0:::CD31143)',	'convertstar',	'Taptica',	'partnerships (reliance)',	'APK - Baidu1',	'apps.facebook.com',	'changyou.com',	'MAT Glispa (MAT)',	'APK - Mobobeat',	'Native-X',	'gaana',	'partnerships (celkon)',	'Moblin',	'Hungama - Intex',	'Applovin',	'Direct OEM - Karbonn Jaina',	'Motive Interactive',	'Cheetah Mobile',	'Vizury',	'Roadster',	'BBM',	'Todacell',	'Glispa (CD28763p3101___48949___93401___APPIA143910308939425755977424896:::CD28763_PLACEMENT)',	'Glispa (rUiB-hViqZnkhpSbPFa_1G692IZEKGewhkVT1BoXStXavU0YGyML3Z3s1yPqsF5ZiogtvJ_evEAJ133M2b2ZGpStKF6r',	'MobileCore (8e8d3259-0570-4584-81b6-1b529ab5b936-9149531:::PB_PB38)',	'partnerships (rage)',	'UCBrowser',	'Hungama - Karbonn Jaina',	'AppStore-Appsdaily',	'Startapp',	'Iron Source (Internal)',	'HummerOffers',	'iCubesWire',	'MobileCore (6780738c-3d3a-4010-b37b-682539bb8556-1500333:::PB_PB38)',	'EverythingMe (IronSourcePST)',	'1mobile',	'AdIQuity',	'APK - Baidu2',	'Hindu',	'Tyroo',	'youAPPi',	'TapIt',	'Aarki',	'APK - Baidu3',	'Micromax',	'Finesse-Karbonn UTL',	'couponduniya',	'OMG Network',	'EverythingMe (Pubnative)',	'Appaba',	'Mobvista (Internal)',	'mainADV',	'test_smarttags',	'null (null)',	'appbrain (app)',	'MobileCore (319d6283-d6e1-454d-9875-0f342eb4ae57-965951:::PB_PB38)',	'Glispa (CD31143p1112532___63641___109393___2013209___ac9bee37455f399cb6e72bee60770e37:::CD31143)',	'Intsal',	'google (recommend)',	'SBI',	'Glispa (CD29724p236___48949___93401___1441780814mb31960326043:::CD29724_PLACEMENT)',	'ad2campaign (Internal)',	'Yanco',	'MobileCore (4d6032d1-a5d8-4544-b46a-b9776f657b73-18325227:::PB_PB38)',	'Startapp (sJiG-qAGSQuAdYgSdoblcQ:::212010554)',	'PayOOM',	'Appnext (Internal)',	'Glispa (CD28763p4344___48949___93401___APPIA144091778037784563702489088:::CD28763_PLACEMENT)',	'123 Greetings',	'Glispa (CD28763p4344___48949___93401___APPIA144066447522232959552987136:::CD28763_PLACEMENT)',	'Glispa (CD28763p7828___48949___93401___APPIA144023795848843495623663616:::CD28763_PLACEMENT)',	'LoopMe',	'Minimob NEW (Internal)',	'Startapp (087VIihRQNGH74r4uYLGnA:::212010554)',	'closeup',	'Motive Interactive (Internal)',	'Glispa (CD28763p7828___48949___93401___APPIA144009581289926598619389952:::CD28763_PLACEMENT)',	'Jampp (0890cadea4e78f1276877d8a7aecd084:::e5fb57f02faa0cc31ce9ed2502a643d4)',	'Glispa (CD28763p4344___48949___93401___APPIA143900983958930347892006912:::CD28763_PLACEMENT)',	'Adxperience',	'Glispa (CD40617pYour_sub_source_Name___48949___93401___7685202925596105___39733:::CD40617_PLACEMENT)',	'AppirisNEW (Internal)',	'apploop_GmbH (102a3efe93832fb9500ba0fb6cecfc:::1409_)',	'Ad2campaign (1gvrr5q2d8rrpicvnf39hnlbl4_1440267122:::15730)',	'Mpire Network (Internal)',	'apploop_GmbH (10278b6761285e8d76da0dae1277de:::1409_)',	'mobvista (earntalktime_in_incent-55bdbe607146746c44fea2ca)',	'Motive (bIl-LLis_Q10uDtIpo9IpEn0fhBSrzwKM4jJoNHXy_OpLn-QOkkGcVbKDWxyteWwxaePWcRHvti_tFs0VgZIKNYD2Qii',	'Ad2campaign (eapj6mi0eg1oga1boa5hsi5975_1439512166:::15730)',	'everything.me (folder)',	'mobvista (earntalktime_in_incent-55ef296d85e11acf51727317)',	'Glispa (CD28763p7828___48949___93401___APPIA144000597957435885306519552:::CD28763_PLACEMENT)',	'Startapp (4HkiBMZJTQGoD3QAF3bE7w:::212010554)',	'Glispa (CD28763p7828___48949___93401___APPIA144000230610427766184648704:::CD28763_PLACEMENT)',	'Headway Digital',	'Glispa (CD31143p1112532___63641___109393___2013209___865b29b9e36997751917ec4ff6a48e2f:::CD31143)',	'MobileCore (62fbe514-8051-470d-99b3-bac96fbb23de-27502827:::PB_PB25300)',	'Glispa (Jc431YG4V_ZeofJE3M1S0XOPfVbekciasVufCwcnN5n25Vq57e2sipqblSP41VMxiogtvJ_evEAJ133M2b2ZGpStKF6r',	'bestappsmarket.com (android-app)',	'Startapp (_bdfsq-FSKyBHe3dfwOB4w:::212010554)',	'Appiris (1020fb67f393620007a6e184c5e21d:::1517)',	'Glispa (CD28763p7828___48949___93401___APPIA143991632674047989124145152:::CD28763_PLACEMENT)',	'MAT OMG (MAT)',	'Appnext (9322163014916425:::107393)',	'mauleadcpa (1021ee553eba77f214ed49529bc751)',	'Glispa (CD28763p4344___48949___93401___APPIA144074980475238451983589376:::CD28763_PLACEMENT)',	'Glispa (CD28763p7828___48949___93401___APPIA143997235523920300691578880:::CD28763_PLACEMENT)',	'EverythingMe (Motive_PST)',	'Glispa (CD28763p3101___48949___93401___APPIA143965234466666967762542592:::CD28763_PLACEMENT)',	'appnext',	'MAT PayOOM (MAT)',	'Glispa (CD28763p3101___48949___93401___APPIA143944635830823728189775872:::CD28763_PLACEMENT)',	'Glispa (CD28763p3101___48949___93401___APPIA143868387193659752041324544:::CD28763_PLACEMENT)',	'mobvista (earntalktime_in_incent-55be09c9aecbbb5b5f8761ec)',	'Glispa (CD31143p1112532___63641___109393___2013209___897076853eb858409538bb37017f678b:::CD31143)',	'EverythingMe (cross_promotions)',	'Adiquity (Internal)',	'mobvista (earntalktime_in_incent-55bef6adc471750e04f4978c)',	'MobileCore (62fbe514-8051-470d-99b3-bac96fbb23de-33000647:::PB_PB38)',	'mobvista (earntalktime_in_incent-55e70cb2dd01ef410dbb6bf1)',	'79 (79)',	'mobile9.com (mobile)',	'google (share)',	'MobileCore (9daf2a49-2e20-48fb-baed-0ed58b172bc9-7283925:::PB_PB38)',	'Glispa (CD28763p3101___48949___93401___APPIA143844169945389840992559104:::CD28763_PLACEMENT)',	'PropellerAds Media',	'MAT Mobvista (MAT)',	'MobileCore (ca0e8eaa-b489-40c5-baef-52c6d20a2e48-14747907:::PB_PB38)',	'Motive (bIl-LLis_Q0UlKQjOGIWp41H7zDwNVe1M4jJoNHXy_OpLn-QOkkGcVbKDWxyteWwxaePWcRHvti_tFs0VgZIKE24VEO0') THEN device_id end) AS paid_installs,
       COUNT(DISTINCT device_id) AS ovr_installs
FROM app_referral
WHERE keyspace = 'ANDI'
AND   attribution = 'Install'
AND   install_date BETWEEN '2015-10-01 00:00:00' AND '2015-10-31 23:59:59';

SELECT count(DISTINCT androidid) as active_base
      FROM (SELECT androidid,load_date
            FROM app_launch
            WHERE load_date BETWEEN 20150916 AND 20150930
            UNION ALL
            SELECT androidid,load_date
            FROM misc_madlytics_events
            WHERE event IN ('push-notification-received','beacon-ping','schedule-received')
            AND   load_date BETWEEN 20150916 AND 20150930) ;
            
select to_char(to_date(a.ins_date,'YYYYMMDD'),'YYYY-MM-DD') as ins_date,to_char(to_date(a.load_date,'YYYYMMDD'),'YYYY-MM-DD') as date,launches,installs from
(select ins_date,load_date,count(distinct androidid) as active,count(distinct case when event='appLaunch' then androidid end) as launches
from 
( select distinct load_date,androidid,event
from app_launch where load_date>=20150801 ) al
join 
(select distinct to_char(install_date,'YYYYMMDD') as ins_date, device_id 
from app_referral where install_date >= '2015-08-01 00:00:00' and keyspace='ANDI' and attribution='Install') i
on al.androidid=i.device_id and load_date>=i.ins_date
group by 1,2) a
left join 
(select to_char(install_date,'YYYYMMDD') as ins_date, count(distinct device_id) as installs
from app_referral where install_date >= '2015-08-01 00:00:00' and keyspace='ANDI' and attribution='Install' group by 1) b
on a.ins_date=b.ins_date
order by 1,2;

select * from google_analytics.ga_metric_monthly where startdate in (20151001,20150901) and app_platform <>'web' order by 1
