/*BM42 revenue with Discounts buckets*/
select 
	case when a.disc_perc = 0 then '0%'
			 when a.disc_perc between 0.001 and 0.249  then '0-25%'
			 when a.disc_perc between 0.25 and 0.499  then '25-50%'
			 when a.disc_perc >= 0.5 then '50+%' end as disc_bucket,
	sum(revenue) as rev,
	sum(net_cost_price) as net_cost_price,		 
	(sum(revenue)-sum(net_cost_price))/NULLIF(sum(revenue),0)*100 as gm_perc
from 
	(select 
		full_date,day_diff, month_to_date, month_diff,		
		(nvl(oi.coupon_discount,0) + nvl(oi.product_discount,0) + nvl(oi.cart_discount,0) + nvl(oi.loyalty_pts_used/2,0))/NULLIF((oi.item_mrp_value*oi.quantity)::decimal(12,2),0) as disc_perc,
		(oi.item_revenue_inc_cashback-nvl(tax)) as revenue,
		(nvl(cogs,0)+nvl(royalty_commission,0)+nvl(stn_input_vat_reversal,0)+ nvl(entry_tax,0)) as net_cost_price
				
	from fact_core_item oi, dim_date d
	where (oi.is_shipped=1 or oi.is_realised = 1) and oi.store_id=1
		and d.full_date = oi.order_created_date and d.day_diff=0) a
group by 1
