SELECT order_created_date,
       case when order_status='F' then case when item_cancellation_type !='CCR' then 'F-CCC' else 'F-CCR' end else order_status end as order_status,
       COUNT(DISTINCT order_group_id) AS orders,
       COUNT(DISTINCT order_group_id) AS shipments,
       COUNT(DISTINCT item_id) AS items,
       SUM(quantity) AS qty
FROM fact_orderitem
WHERE store_id = 1
AND   order_created_date IN (20160102,20151013,20151210,20150103,20150718)
AND		order_status in ('C','DL','F','PK','Q','SH','WP','RTO')
group by 1,2
order by 1,2;

SELECT order_created_date,
       case when item_cancellation_type !='CCR' then 'CCC' else 'CCR' end as typ,
       COUNT(DISTINCT order_group_id) AS orders,
       COUNT(DISTINCT order_group_id) AS shipments,
       COUNT(DISTINCT item_id) AS items,
       SUM(quantity) AS qty
FROM fact_orderitem
WHERE store_id = 1
AND   order_created_date IN (20160102,20151013,20151210,20150103,20150718)
AND		order_status in ('F')
group by 1,2
order by 1,2;
