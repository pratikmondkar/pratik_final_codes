SELECT discount_rule_percent,
       disc_bucket,
       brand,
       article_type,
       season_code,
       gender,
       supply_type,
       current_commercial_type,
       brand_type,
       business_unit,
       COUNT(DISTINCT style_id) AS styles,
       SUM(start_inv) AS start_inv,
       SUM(rev) AS rev,
       SUM(rgm) AS rgm,
       SUM(tax) AS tax,
       SUM(qty) AS qty,
             CASE
               WHEN sum(qty*age_inv)/NULLIF(sum(qty),0) <= 30 THEN '30'
               WHEN sum(qty*age_inv)/NULLIF(sum(qty),0) BETWEEN 31 AND 60 THEN '30-60'
               WHEN sum(qty*age_inv)/NULLIF(sum(qty),0) BETWEEN 61 AND 90 THEN '60-90'
               WHEN sum(qty*age_inv)/NULLIF(sum(qty),0) BETWEEN 91 AND 120 THEN '90-120'
               WHEN sum(qty*age_inv)/NULLIF(sum(qty),0) BETWEEN 121 AND 150 THEN '120-150'
               WHEN sum(qty*age_inv)/NULLIF(sum(qty),0) BETWEEN 151 AND 180 THEN '150-180'
               else '180'
             END AS age_bucket      
FROM (SELECT p.style_id,
             ds.supply_type,
             ds.brand,
             ds.article_type,
             ds.season_code,
             ds.gender,
             ds.current_commercial_type,
             ds.brand_type,
             ds.business_unit,
             discount_rule_percent,
             CASE
               WHEN discount_rule_percent <= 30 THEN '30'
               WHEN discount_rule_percent BETWEEN 31 AND 40 THEN '40'
               WHEN discount_rule_percent BETWEEN 41 AND 50 THEN '50'
               WHEN discount_rule_percent BETWEEN 51 AND 60 THEN '60'
               WHEN discount_rule_percent BETWEEN 61 AND 70 THEN '70'
               WHEN discount_rule_percent BETWEEN 71 AND 80 THEN '80'
             END AS disc_bucket,
             nvl(start_inv,0) AS start_inv,
             nvl(rev,0) AS rev,
             nvl(rgm,0) AS rgm,
             nvl(tax,0) AS tax,
             nvl(qty,0) AS qty,
             nvl(age_inv,180) as age_inv  
      FROM pricing_snapshot_history p
        LEFT JOIN (SELECT dp.style_id,
                          SUM(nvl (net_inventory_count,0)) AS start_inv
                   FROM fact_inventory_count fic
                     JOIN dim_product dp ON fic.sku_id = dp.sku_id
                   WHERE fic.date = 20160101
                   AND   warehouse_id NOT IN (24,56)
                   GROUP BY 1) i ON p.style_id = i.style_id
        LEFT JOIN (SELECT style_id,
                          SUM(nvl (item_revenue_inc_cashback,0)) AS rev,
                          SUM(nvl (rgm_final,0)) AS rgm,
                          SUM(nvl (tax,0)) AS tax,
                          SUM(nvl (quantity,0)) AS qty,
                          avg(datediff(d,to_date(inward_date,'YYYYMMDD'),sysdate-interval '5 days')) as age_inv
                   FROM fact_core_item fci
                   left join fact_inventory_item fii
                   on fci.core_item_id=fii.core_item_id
                   WHERE fci.store_id = 1
                   AND   (is_shipped = 1 OR is_realised = 1)
                   AND   order_created_date in (20160102,20160103)
                   GROUP BY 1) r ON p.style_id = r.style_id
        LEFT JOIN dim_style ds ON p.style_id = ds.style_id
      WHERE DATE = 20160102
      AND   discount_rule_percent IS NOT NULL)
GROUP BY 1,2,3,4,5,6,7,8,9,10
ORDER BY 1 DESC LIMIT 50000;

select effective_discount_percent,count(1) from fact_core_item where store_id=1 and  limit 100;

select min(date) from fact_inventory_snapshot	


