select 
case when order_created_date in (20160102,20160103) then 'Jan-EORS-16'
when order_created_date in (20150103) then 'Jan-EORS-15'
when order_created_date in (20150718,20150719) then 'Jul-EORS-15'
else 'BBD' end as event,
--TO_CHAR(cast(order_created_date ||' '|| lpad(order_created_time,4,0) as timestamp),'HH24') as EORS_hour,
case when purchase_sequence=1 then 'New' else 'Repeat' end as cus_type,
sum(shipped_order_revenue_inc_cashback) as revenue,
count(distinct order_group_id) as orders,
count(distinct idcustomer) as customers
from fact_order
where store_id=1 and order_created_date in (20160102,20160103,20150103,20150718,20151013) and is_booked=1
group by 1,2
order by 1,2;

select 
order_created_date,
order_created_time,
TO_CHAR(cast(order_created_date ||' '|| lpad(order_created_time,4,0) as timestamp),'YYYYMMDD-HH24') AS State_Change_Month
from fact_order
where store_id=1 and order_created_date in (20160102,20160103) and is_booked=1
limit 100;
