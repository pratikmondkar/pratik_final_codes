SELECT fci.supply_type,
       fci.brand,
       fci.article_type,
--       fci.season_code,
       fci.gender,
       fci.po_type,
       fci.brand_type,
       dp.business_unit,
       dl.city_tier,
       dl.region_name,
--			 dl.city_group,
       case when order_created_date in (20160102,20160103) then 'EORS' else 'Base' end as flag,
       SUM(nvl(item_revenue_inc_cashback,0)) AS rev,
       count(distinct order_group_id) as orders,
       count(distinct idcustomer) as customers,
       SUM(nvl(rgm_final,0)) AS rgm,
       SUM(nvl(tax,0)) AS tax,
       SUM(nvl(product_discount,0) + nvl (coupon_discount,0)) AS disc,
       SUM(nvl (quantity,0)) AS qty       
FROM fact_core_item fci
join dim_product dp on fci.sku_id=dp.sku_id 
join dim_location dl on fci.idlocation=dl.id
WHERE fci.store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   (order_created_date IN (20160102,20160103) OR order_created_date BETWEEN 20151101 AND 20151130)
GROUP BY 1,2,3,4,5,6,7,8,9,10

