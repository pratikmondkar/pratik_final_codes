/*D18 GM Complete Details*/
select 
	b.*, 
	round((b.sales_value_ex_tax_yesterday * 100)/nullif(b.article_mrp_ex_tax_yesterday,0), 2) as sales_value_percent_yesterday,
	round((b.item_purchase_price_yesterday * 100)/nullif(b.article_mrp_ex_tax_yesterday,0), 2) as cost_percent_yesterday,
	round(((b.sales_value_ex_tax_yesterday - b.item_purchase_price_yesterday) * 100)/nullif(b.sales_value_ex_tax_yesterday,0), 2) as gross_margin_percent_yesterday,
	round((b.sales_value_ex_tax_yesterday - b.item_purchase_price_yesterday),0) as rupee_gross_margin,
	round((b.sales_value_ex_tax_yesterday - b.item_purchase_price_yesterday)/quantity,0) as rgm_per_item
from
	(select 
		a.foi_category,
		sum(case when day_diff=0 then article_mrp_ex_tax else 0 end) as article_mrp_ex_tax_yesterday,
		sum(case when day_diff=0 then item_purchase_price else 0 end) as item_purchase_price_yesterday,
		sum(case when day_diff=0 then sales_value_ex_tax+other_charges else 0 end) as sales_value_ex_tax_yesterday,
		sum(case when day_diff=0 then quantity end) as quantity
	from 
		(select 
			(CASE WHEN (o.supply_type = 'JUST_IN_TIME' OR commercial_type = 'JIT_Marketplace') THEN 'MP' WHEN (UPPER(commercial_type) = 'OUTRIGHT' AND UPPER(p.brand_type) = 'PRIVATE') THEN 'MFB' WHEN (UPPER(commercial_type) = 'SOR') THEN 'SOR' WHEN (UPPER(commercial_type) = 'OUTRIGHT') THEN 'OUTRIGHT' ELSE NULL END) AS foi_category,
			day_diff,dt.full_date, dt.month_to_date, dt.last7_days, dt.month_diff, (o.item_mrp_value /(1 +(tax / NULLIF((item_revenue_inc_cashback - tax),0))))*quantity as article_mrp_ex_tax, 
			(cogs+royalty_commission) as item_purchase_price,
			(o.item_revenue_inc_cashback - o.tax) as sales_value_ex_tax,
			0 as other_charges,
			o.quantity
			from 
			fact_orderitem o, dim_product p, dim_date dt
		where 
			(o.is_realised = 1 or o.is_shipped = 1) and o.idproduct = p.id and dt.full_date = o.order_created_date and o.store_id=1
			and dt.day_diff=0 
		) a
	group by
		a.foi_category
	order by
		4 desc) b                                                                                                                                                                                                                                                      
