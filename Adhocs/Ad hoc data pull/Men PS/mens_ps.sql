drop table if exists dev.pm_purchase_seq;
create table dev.pm_purchase_seq distkey(order_group_id) sortkey(order_created_date) as
(select idcustomer,order_group_id,order_created_date,order_created_time,row_number() over (partition by order_created_date,order_created_time) as purchase_seq from
(select distinct idcustomer_idea as idcustomer,order_group_id::varchar(255),order_created_date,order_created_time from fact_order where store_id=1 and is_shipped=1 and order_created_date<20150501
union all
select distinct idcustomer,order_group_id,order_created_date,order_created_time/100 as order_created_time from fact_core_item where store_id=1 and is_shipped=1 and order_created_date>=20150501));

select purchase_seq,brand,fci.article_type,cluster_name, 
case when fci.order_created_date BETWEEN 20170201 and 20170530 then 2017 else 2016 end as year,
case when item_revenue_inc_cashback<= exonomy then 'economy' 
when item_revenue_inc_cashback >exonomy and item_revenue_inc_cashback<=mass then 'mass'
when item_revenue_inc_cashback >mass and item_revenue_inc_cashback<=premium then 'premium'
when item_revenue_inc_cashback >premium and item_revenue_inc_cashback<=blt then 'blt'
when item_revenue_inc_cashback> luxury then 'luxury' 
else 'unknown' end as price_point,
sum(quantity) as qty,sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as revenue
FROM fact_core_item fci 
join dim_customer_idea dci on fci.idcustomer=dci.id
left join dev.pm_purchase_seq ps on fci.order_group_id=ps.order_group_id
left join customer_insights.style_cluster_map cm on fci.style_id=cm.style_id
left join dev.demodemo d on fci.article_type=d.article
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   (fci.order_created_date BETWEEN 20170201 and 20170530 or fci.order_created_date BETWEEN 20160201 and 20160530)
and dci.gender='m' and dci.dob between 19920101 and 20170101 and fci.gender='Men' and fci.article_type in ('Tshirts',	'Shirts',	'Casual Shoes',	'Jeans',	'Sports Shoes',	'Trousers',	'Formal Shoes',	'Jackets',	'Watches',	'Shorts')
group by 1,2,3,4,5,6;


select
case when fci.order_created_date BETWEEN 20170201 and 20170530 then 2017 else 2016 end as year,
sum(quantity) as qty,sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as revenue
FROM fact_core_item fci 
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   (fci.order_created_date BETWEEN 20170201 and 20170530 or fci.order_created_date BETWEEN 20160201 and 20160530) and fci.gender='Men' 
--and fci.article_type in ('Tshirts',	'Shirts',	'Casual Shoes',	'Jeans',	'Sports Shoes',	'Trousers',	'Formal Shoes',	'Jackets',	'Watches',	'Shorts')
group by 1;

select
case when fci.order_created_date BETWEEN 20170201 and 20170530 then 2017 else 2016 end as year,
sum(quantity) as qty,sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as revenue
FROM fact_core_item fci 
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   (fci.order_created_date BETWEEN 20170201 and 20170530 or fci.order_created_date BETWEEN 20160201 and 20160530)
--and fci.article_type in ('Tshirts',	'Shirts',	'Casual Shoes',	'Jeans',	'Sports Shoes',	'Trousers',	'Formal Shoes',	'Jackets',	'Watches',	'Shorts')
group by 1
