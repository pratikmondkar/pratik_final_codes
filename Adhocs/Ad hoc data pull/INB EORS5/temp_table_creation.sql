drop table dev.inb_uidx;
drop table dev.inb_email;


create table dev.inb_uidx distkey(GAID) as (
SELECT distinct aum.GAID
FROM (SELECT a.advertising_id as GAID
      FROM app_users_magasin a
      WHERE created_date BETWEEN '2015-01-01 00:00:00' AND '2016-09-15 23:59:59'
      AND   a.device_type = 'Android'
      AND   a.advertising_id IS NOT NULL
      GROUP BY 1) aum
left join  (SELECT DISTINCT au.advertising_id
                       FROM dim_customer_idea dci,
                            fact_core_item f,
                            app_users_magasin au
                       WHERE dci.id = f.idcustomer
                       and dci.customer_login=au.user_id
                       AND   (f.is_shipped = 1 OR f.is_realised = 1)
                       and store_id=1) p on aum.gaid=p.advertising_id
where p.advertising_id is null
);

create table dev.inb_email distkey(GAID) as (
SELECT distinct aum.GAID
FROM (SELECT a.advertising_id as GAID
      FROM app_users_magasin a
      WHERE created_date BETWEEN '2015-01-01 00:00:00' AND '2016-09-15 23:59:59'
      AND   a.device_type = 'Android'
      AND   a.advertising_id IS NOT NULL
      GROUP BY 1) aum
left join  (SELECT DISTINCT au.advertising_id
                       FROM dim_customer_idea dci,
                       			cii.dim_customer_email dce,
                            fact_core_item f,
                            app_users_magasin au
                       WHERE dci.id = f.idcustomer
                       and dci.id=dce.uid
                       and dce.email=au.user_id
                       AND   (f.is_shipped = 1 OR f.is_realised = 1)
                       and store_id=1) p on aum.gaid=p.advertising_id
where p.advertising_id is null
);

create table customer_insights.inb_final as 
(
select a.GAID
from dev.inb_uidx a
join dev.inb_email b on a.GAID=b.GAID 
);

grant select on customer_insights.inb_final to adhocpanel;
grant select on customer_insights.inb_final to adhoc_fin_user;
grant select on customer_insights.inb_final to analysis_user;

select distinct gaid from customer_insights.inb_final;

select * from stv_recents where status='Running';

cancel 17411;


grant select on dev.inb_uidx to customer_insights_ddl;

drop table customer_insights.inb_final;
