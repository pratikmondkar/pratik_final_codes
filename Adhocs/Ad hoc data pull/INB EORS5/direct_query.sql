
select a.GAID from
(SELECT distinct aum.GAID
FROM (SELECT a.advertising_id as GAID
      FROM app_users_magasin a
      WHERE created_date BETWEEN '2015-01-01 00:00:00' AND '2016-09-15 23:59:59'
      AND   a.device_type = 'Android'
      AND   a.advertising_id IS NOT NULL
      GROUP BY 1) aum
left join  (SELECT DISTINCT au.advertising_id
                       FROM dim_customer_idea dci,
                            fact_core_item f,
                            app_users_magasin au
                       WHERE dci.id = f.idcustomer
                       and dci.customer_login=au.user_id
                       AND   (f.is_shipped = 1 OR f.is_realised = 1)
                       and store_id=1) p on aum.gaid=p.advertising_id
where p.advertising_id is null) a
join (SELECT distinct aum.GAID
FROM (SELECT a.advertising_id as GAID
      FROM app_users_magasin a
      WHERE created_date BETWEEN '2015-01-01 00:00:00' AND '2016-09-15 23:59:59'
      AND   a.device_type = 'Android'
      AND   a.advertising_id IS NOT NULL
      GROUP BY 1) aum
left join  (SELECT DISTINCT au.advertising_id
                       FROM dim_customer_idea dci,
                       			cii.dim_customer_email dce,
                            fact_core_item f,
                            app_users_magasin au
                       WHERE dci.id = f.idcustomer
                       and dci.id=dce.uid
                       and dce.email=au.user_id
                       AND   (f.is_shipped = 1 OR f.is_realised = 1)
                       and store_id=1) p on aum.gaid=p.advertising_id
where p.advertising_id is null
) b on a.GAID=b.GAID
