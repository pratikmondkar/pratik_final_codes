select distinct dc.customer_login,dc.first_name,dc.last_name,dc.default_mobile
from fact_order fo
join dim_customer dc
on fo.idcustomer=dc.id
where (is_shipped=1 or is_realised=1)
and store_id=1 and order_created_date > to_char(sysdate-interval '1 year','YYYYMMDD');

select  distinct dc.customer_login,dc.first_name,dc.last_name,dc.default_mobile
from fact_orderitem fo
join dim_product dp
on fo.sku_id=dp.sku_id
join dim_customer dc
on fo.idcustomer=dc.id
where (is_shipped=1 or is_realised=1)
and dp.gender='Women'
and store_id=1 and order_created_date > to_char(sysdate-interval '1 year','YYYYMMDD')
