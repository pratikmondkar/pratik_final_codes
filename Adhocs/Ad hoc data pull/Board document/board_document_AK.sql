create table dev.pm_chk1 distkey(brand) sortkey(session_end_date) as
select session_end_date,sp.brand,sp.article_type,sp.gender,sum(list_views) as list_counts 
from customer_insights.style_pro sp
-- 
group by 1,2,3,4;


select *  from dev.pm_chk1 sp 
join dev.pm_bag_list b on sp.brand=b.brand and sp.article_type=b.article_type and sp.gender=b.gender;



select a.os,b.os,c.os,d.os,count(distinct nvl(a.uidx,b.uidx,c.uidx,d.uidx)) as users from
(select  distinct os,uidx from clickstream.daily_aggregates where load_date between 20170901 and 20170930 and os='Android') a
full outer join
(select  distinct os,uidx from clickstream.daily_aggregates where load_date between 20170901 and 20170930 and os='iOS') b on a.uidx=b.uidx
full outer join
(select  distinct os,uidx from customer_insights.daily_aggregates_web where load_date between 20170901 and 20170930 and os='Mweb') c on c.uidx=nvl(a.uidx,b.uidx)
full outer join
(select  distinct os,uidx from customer_insights.daily_aggregates_web where load_date between 20170901 and 20170930 and os='Desktop') d on d.uidx=nvl(a.uidx,b.uidx,c.uidx)
group by 1,2,3,4
;


select a.os,b.os,c.os,d.os,count(distinct nvl(a.uidx,b.uidx,c.uidx,d.uidx)) as users from
(select  distinct os,uidx from clickstream.daily_aggregates where load_date = 20170915 and os='Android') a
full outer join
(select  distinct os,uidx from clickstream.daily_aggregates where load_date = 20170915 and os='iOS') b on a.uidx=b.uidx
full outer join
(select  distinct os,uidx from customer_insights.daily_aggregates_web where load_date = 20170915 and os='Mweb') c on c.uidx=nvl(a.uidx,b.uidx)
full outer join
(select  distinct os,uidx from customer_insights.daily_aggregates_web where load_date = 20170915 and os='Desktop') d on d.uidx=nvl(a.uidx,b.uidx,c.uidx)
group by 1,2,3,4;


select  case when nullif(uidx,' ') is not null then 'logged-in' else 'non-loggedin' end as login,sum(sessions) 
from customer_insights.daily_aggregates_web 
where load_date = 20170915 and os='Mweb'
group by 1
;
