select case when order_created_date < 20170316 then 'pre' else 'post' end as bucket,city,
--zipcode,
sum(item_revenue_inc_cashback) as revenue,count(distinct order_group_id) as orders,count(distinct order_created_date) as days 
from fact_core_item 
where (is_shipped=1 or is_realised=1) and brand='Roadster' and
store_id=1 and order_created_date between 20170116 and 20170516 and city  in ('Bangalore','Mumbai')
group by 1,2;



select case when quantity > 10 then 11 else quantity end as quantity,case when articles_bought > 10 then 11 else articles_bought end as articles_bought ,count(distinct uidx) 
from fact_user_lifetime where uidx is not null 
group by 1,2;


select case when orders >=4 then 'Loyal' else 'Non-Loyal' end as loyalty, sum(articles_bought) as  at,sum(orders) as orders,count(distinct uidx) as cust
from fact_user_lifetime where uidx is not null and orders>=1
group by 1;
