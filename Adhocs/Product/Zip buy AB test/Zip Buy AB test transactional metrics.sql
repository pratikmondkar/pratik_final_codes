select 
'Test' as grp,
foi.order_created_date,
sum(foi.item_mrp_value*foi.quantity) as gmv,
sum(foi.item_revenue_inc_cashback) as rev,
COUNT(DISTINCT foi.order_group_id) as orders,
count(distinct idcustomer) as customers, 
sum(rgm) as rgm,
sum(tax) as tax,
sum(foi.product_discount) as product_disc,
sum(foi.coupon_discount) as coupon_disc
from fact_orderitem foi 
JOIN dim_customer dc 
on foi.idcustomer=dc.id
JOIN table_332 tst
on tst.user_data=dc.customer_login
where (foi.is_realised=1 or foi.is_shipped=1) and foi.store_id=1 and 
order_created_date>= 20151120
group by 1,2
