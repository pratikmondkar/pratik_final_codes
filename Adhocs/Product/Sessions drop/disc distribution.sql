SELECT deviceid,
       CASE
         WHEN SUM(pdpdata_tradediscount) / NULLIF(SUM(pdpdata_mrp),0) <= 0.24 THEN 'd1'
         WHEN SUM(pdpdata_tradediscount) / NULLIF(SUM(pdpdata_mrp),0) BETWEEN 0.2401 AND 0.338 THEN 'd2'
         WHEN SUM(pdpdata_tradediscount) / NULLIF(SUM(pdpdata_mrp),0) BETWEEN 0.339 AND 0.4273 THEN 'd3'
         WHEN SUM(pdpdata_tradediscount) / NULLIF(SUM(pdpdata_mrp),0) > 0.4273 THEN 'd4'
         ELSE 'others'
       END AS avg_disc_view
FROM clickstream.pdp_view sl
WHERE load_date BETWEEN 20160301 AND 20160417
GROUP BY 1;

SELECT week_diff,
			 disc_seg,
       COUNT(DISTINCT session_id) AS sessions,
       COUNT(DISTINCT device_id) AS devices,
      FROM clickstream.events_view ev
      join bidb.dim_date dd
      on ev.load_date=dd.full_date
      join (SELECT deviceid,
						       CASE
						         WHEN SUM(pdpdata_tradediscount) / NULLIF(SUM(pdpdata_mrp),0) <= 0.24 THEN 'd1'
						         WHEN SUM(pdpdata_tradediscount) / NULLIF(SUM(pdpdata_mrp),0) BETWEEN 0.2401 AND 0.338 THEN 'd2'
						         WHEN SUM(pdpdata_tradediscount) / NULLIF(SUM(pdpdata_mrp),0) BETWEEN 0.339 AND 0.4273 THEN 'd3'
						         WHEN SUM(pdpdata_tradediscount) / NULLIF(SUM(pdpdata_mrp),0) > 0.4273 THEN 'd4'
						         ELSE 'others'
						       END AS disc_seg
						FROM clickstream.pdp_view sl
						WHERE load_date BETWEEN 20160301 AND 20160417
						GROUP BY 1) c
			on 
      WHERE load_date BETWEEN 20160301 and 20160417 and event_type in ('ScreenLoad','appLaunch')
      GROUP BY 1,2;

select order_created_date,sum(item_mrp_value*quantity) as mrp,sum(product_discount) as td 
from fact_core_item fci 
where order_created_date >= 20160516 and store_id=1 and (is_shipped=1 or is_realised=1)
group by 1
order by 1;


SELECT week_diff,
			 clickstream.get_json_field(ab_tests,'lgp.stream') AS lgp,
       COUNT(DISTINCT session_id) AS sessions,
       COUNT(DISTINCT device_id) AS devices
      FROM clickstream.events_view ev
      join bidb.dim_date dd
      on ev.load_date=dd.full_date
      WHERE load_date BETWEEN 20160301 and 20160417 and event_type in ('ScreenLoad','appLaunch')
      GROUP BY 1,2
