SELECT case when regis_year_month <201101 then '<2011101' else regis_year_month::varchar(10) end as cohort_month,
			 year,
       SUM(orders) AS orders,
       sum(revenue) as revenue,
       COUNT(DISTINCT idcustomer_idea) AS customers
FROM (SELECT order_created_date / 10000 AS YEAR,
             idcustomer_idea,
             first_login_date/100 as regis_year_month,
             COUNT(DISTINCT order_group_id) AS orders,
             sum(shipped_order_revenue_inc_cashback) as revenue
      FROM fact_order fo
      left join dim_customer_idea dci on idcustomer_idea=dci.id 
      WHERE (is_shipped = 1 OR is_realised = 1)
      AND   store_id = 1
      AND   order_created_date BETWEEN 20140101 AND 20161231
      group by 1,2,3)
group by 1,2
