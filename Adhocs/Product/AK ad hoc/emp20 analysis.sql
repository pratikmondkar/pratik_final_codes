	SELECT CASE
	         WHEN dce.email LIKE '%myntra.com' THEN 'Myntra'
	         ELSE 'FK'
	       END AS company,
	       idcustomer,
	       dce.email,
	       count(distinct fci.order_group_id) as orders,
	       SUM(coupon_discount),
	       SUM(item_revenue_inc_cashback) AS revenue,
	       SUM(item_mrp_value*quantity) AS mrp
	FROM fact_core_item fci
	  JOIN (SELECT DISTINCT order_group_id
	        FROM fact_core_item
	        WHERE lower(coupon_code) in ('myntraemp20','flipkartemp20')) a on fci.order_group_id=a.order_group_id
	  JOIN dim_customer_email dce ON fci.idcustomer = dce.uid
	WHERE (dce.email LIKE '%myntra.com'
	OR    dce.email LIKE '%flipkart.com')
	AND   fci.store_id = 1
	AND   (is_shipped = 1 OR is_realised = 1)
	AND   order_created_date >= 20151201
	group by 1,2,3
order by 5 desc;


	SELECT 
	       dce.email,
	       fci.coupon_code,
	       count(distinct fci.order_group_id) as orders,
	       SUM(coupon_discount) as cd,
	       SUM(item_revenue_inc_cashback) AS revenue,
	       SUM(item_mrp_value*quantity) AS mrp
	FROM fact_core_item fci
	  JOIN dim_customer_email dce ON fci.idcustomer = dce.uid
	WHERE dce.email LIKE '%flipkart.com'
	AND   fci.store_id = 1
	AND   (is_shipped = 1 OR is_realised = 1)
	AND   order_created_date >= 20151201
	group by 1,2
order by 4 desc
limit 100




