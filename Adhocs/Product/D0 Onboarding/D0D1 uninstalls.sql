select cnt,count(distinct device_device_id) as devices from
(select device_device_id,count(distinct load_date)as cnt from clickstream.uninstalls group by 1)
group by 1;





SELECT TO_CHAR(install_date,'YYYYMMDD') as install_cohort,
       COUNT(1) AS installs,
       COUNT(CASE WHEN u.device_device_id IS NOT NULL and datediff(d,install_date,TO_DATE(u.load_date,'YYYYMMDD'))=15 THEN 1 END) AS d0_uninstalls,
       COUNT(CASE WHEN u.device_device_id IS NOT NULL and datediff(d,install_date,TO_DATE(u.load_date,'YYYYMMDD'))=16 THEN 1 END) AS d1_uninstalls
FROM bidb.app_referral ap
  LEFT JOIN (select device_device_id,min(load_date) as load_date from clickstream.uninstalls group by 1) u 
  ON ap.device_id = u.device_device_id
WHERE ap.attribution = 'Install'
AND   keyspace = 'ANDI'
AND   install_date between '2016-02-02 00:00:00' and sysdate - interval '16 days' 
group by 1
order by 1;


select to_char(install_date,'YYYYMMDD') as install_date,count(*) 
from clickstream.uninstalls u 
join app_referral ap 
on u.device_device_id=ap.device_id and ap.attribution = 'Install' and keyspace = 'ANDI' 
where u.load_date=20160318
group by 1
order by 2 desc;


select load_date,count(*) from clickstream.uninstalls group by 1;

Feb - 15,27
Mar - 15,18,20
