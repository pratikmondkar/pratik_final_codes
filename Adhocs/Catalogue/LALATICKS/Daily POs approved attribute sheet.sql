select distinct fpo.barcode,dp.brand,dp.article_type,dp.style_id,dp.style_status,fpd.file_name,'http://lala.mynt.myntra.com/index.php/attachments/getFile/?id='||fpd.id as url 
from fact_poattachment_details fpd 
join fact_purchase_order fpo 
on fpd.entity_id=fpo.pi_id 
join dim_product dp 
on fpo.sku_id=dp.sku_id
where fpo.approval_date=to_char(sysdate - interval '1 day','YYYYMMDD') and po_type in ('OUTRIGHT','SOR','DROP_SHIP','CONSIGNMENT')
and article_type not in ('Sarees')
