select
        a.*,dw.warehouse_name,
        nvl(b.revenue,0) as revenue,
        nvl(c.mrp_potential,0) as mrp_potential,
        nvl(d.list_count,0) as list_count 
    from
        ( select
            a.style_id,
            a.style_name,
            a.sku_code,
            a.size,
            a.article_type,
            a.brand,
            a.gender,
            (SELECT
                MIN(bin_barcode) AS bin_barcode                                   
            FROM
                fact_inventory_item i                                   
            WHERE
                i.sku_id = a.sku_id                                           
                AND   quality = 'Q1'    
								AND   i.item_status = 'STORED'
                AND warehouse_id!=24 LIMIT 1) AS bin_barcode,
            (SELECT
                MIN(warehouse_id)                                   
            FROM
                fact_inventory_item                                   
            WHERE
                sku_id = a.sku_id                                           
                AND   quality = 'Q1'                                           
                AND   item_status = 'STORED'
                AND warehouse_id!=24) AS warehouse_id                   
        from
            dim_product a                   
        join
            (
                select
                    style_id,
                    min(sku_id) as sku_id                                   
                from
                    dim_product dp                                   
                where
                    photoshoot_priority != 99                      
                    and commercial_type = 'OUTRIGHT'                      
                    and style_catalogued_date < to_char(sysdate-interval '30 days','YYYYMMDD') 
                    and exists (SELECT 1 FROM fact_inventory_item i  WHERE  i.sku_id = dp.sku_id  AND i.item_status = 'STORED' and warehouse_id!=24)                                    
                group by
                    style_id
            ) b    
                on b.sku_id = a.sku_id 
            ) a 
    left join
        (
            select
                style_id,
                sum(item_revenue_inc_cashback) as revenue 
            from
                fact_orderitem foi,
                dim_product dp      
            where
                foi.sku_id=dp.sku_id 
                and order_created_date > to_char(sysdate-interval '15 days','YYYYMMDD') 
                and is_realised=1     
            group by
                1
        ) b 
            on a.style_id=b.style_id 
    left join
        (
            select
                style_id,
                sum(article_mrp*net_inventory_count) as mrp_potential      
            from
                fact_product_snapshot fps      
            where
                date=to_char(sysdate,'YYYYMMDD')     
            group by
                1
        ) c 
            on      a.style_id=c.style_id 
    left join
        (
            select
                style_id,
                sum(list_count) as list_count 
            from
                fact_basic_funnel_details_snapshot      
            where
                load_date>to_char(sysdate-interval '15days' ,'YYYYMMDD')       
            group by
                1
        ) d 
            on      a.style_id=d.style_id 
    left join
        dim_warehouse dw 
            on a.warehouse_id=dw.warehouse_id 
    where
        --Main criteria to filter styles     
        (revenue<5000 
        or revenue is NULL) 
--        and mrp_potential>80000 
--        and list_count>7500 
        and  a.style_id not in (
 select distinct style_id from catalog_makeover
        )  
    order by
        mrp_potential desc
limit 10000
