drop table if exists dev.part1; 
create table dev.part1 as 
SELECT CASE
               WHEN ait.campaign_name IS NULL AND ait.campaign_source IS NULL THEN 'Direct Organic'
               ELSE class
             END AS class,
             keyspace,
             TO_CHAR(created_datetime,'YYYY-MM') AS install_month,
             count(DISTINCT device_id)as installs
      FROM app_install_tune ait
        LEFT JOIN dev.install_source_mapping ism
               ON nvl (ait.campaign_source,'0') = ism.campaign_source
      WHERE created_datetime > '2016-09-01 00:00:00' and device_id is not null 
      group by 1,2,3 ;
      

select * from dev.part1;

select count(distinct case when apr16_flag = 1 then uidx end) from dev.traffic_plan_recency_cohort_browsing_metrics_v1


drop table if exists dev.part2; 
create table dev.part2 distkey(idcustomer) as 
SELECT DISTINCT CASE
               WHEN ait.campaign_name IS NULL AND ait.campaign_source IS NULL THEN 'Direct Organic'
               ELSE class
             END AS class,
             ait.device_id,
             aum.idcustomer,
             ait.keyspace,
             TO_CHAR(ait.created_datetime,'YYYY-MM') AS install_month
      FROM app_install_tune ait
  left join dev.aum_dedup aum on upper(ait.device_Id)=upper(aum.advertising_id) 
        LEFT JOIN dev.install_source_mapping ism
               ON nvl (ait.campaign_source,'0') = ism.campaign_source
      WHERE ait.created_datetime > '2016-09-01 00:00:00' and ait.device_id is not null;
      


drop table if exists dev.part3_orders; 
create table dev.part3_orders distkey(idcustomer) as
select distinct idcustomer,order_created_date,device_id,advertising_id,item_revenue_inc_cashback from fact_core_item where order_created_date >= 20160901 and store_id=1 and (is_shipped+1 or is_realised=1)
and idcustomer is not null;


select a.*,b.obs_month, NVL(b.customers,0) as customers,NVL(b.revenue,0) as revenue
from
dev.part1 a
left join 
(
SELECT a.install_month,
             to_char(to_date(order_created_date,'YYYYMMDD'),'YYYY-MM') as obs_month,
       keyspace,
       class,
       COUNT(DISTINCT a.device_id) AS installs,
       COUNT(CASE WHEN fci.advertising_id IS NOT NULL THEN a.device_id END) AS customers,
       SUM(CASE WHEN fci.advertising_id IS NOT NULL THEN item_revenue_inc_cashback END) AS revenue
FROM dev.part2 a
  JOIN dev.part3_orders fci ON a.idcustomer = fci.idcustomer and to_char(to_date(order_created_date,'YYYYMMDD'),'YYYY-MM')>=install_month
  group by 1,2,3,4
 ) b on a.keyspace=b.keyspace and a.install_month=b.install_month and a.class=b.class;
 


