select a.*,b.obs_month, b.customers,b.revenue
from
(SELECT CASE
               WHEN ait.campaign_name IS NULL AND ait.campaign_source IS NULL THEN 'Direct Organic'
               ELSE class
             END AS class,
             keyspace,
             TO_CHAR(created_datetime,'YYYY-MM') AS install_month,
             count(DISTINCT device_id)as installs
      FROM app_install_tune ait
        LEFT JOIN (SELECT DISTINCT campaign_source,
                          campaign_name,
                          class
                   FROM (SELECT campaign_source,
                                campaign_name,
                                class,
                                ROW_NUMBER() OVER (PARTITION BY campaign_source,campaign_name ORDER BY len (nvl (class,'')) DESC) AS row_num
                         FROM clickstream.install_source_mapping)
                   WHERE row_num = 1) ism
               ON nvl (ait.campaign_name,'0') = nvl (ism.campaign_name,'0')
              AND nvl (ait.campaign_source,'0') = nvl (ism.campaign_source,'0')
      WHERE created_datetime > '2017-01-01 00:00:00'
      group by 1,2,3) a
left join 
(SELECT a.install_month,
			 to_char(to_date(order_created_date,'YYYYMMDD'),'YYYY-MM') as obs_month,
       keyspace,
       class,
       COUNT(DISTINCT a.device_id) AS installs,
       COUNT(CASE WHEN fci.advertising_id IS NOT NULL THEN a.device_id END) AS customers,
       SUM(CASE WHEN fci.advertising_id IS NOT NULL THEN item_revenue_inc_cashback END) AS revenue
FROM (SELECT DISTINCT CASE
               WHEN ait.campaign_name IS NULL AND ait.campaign_source IS NULL THEN 'Direct Organic'
               ELSE class
             END AS class,
             ait.device_id,
             aum.idcustomer,
             ait.keyspace,
             TO_CHAR(ait.created_datetime,'YYYY-MM') AS install_month
      FROM app_install_tune ait
  left join dev.aum_dedup aum on ait.device_Id=aum.advertising_id 
        LEFT JOIN (SELECT DISTINCT campaign_source,
                          campaign_name,
                          class
                   FROM (SELECT campaign_source,
                                campaign_name,
                                class,
                                ROW_NUMBER() OVER (PARTITION BY campaign_source,campaign_name ORDER BY len (nvl (class,'')) DESC) AS row_num
                         FROM clickstream.install_source_mapping)
                   WHERE row_num = 1) ism
               ON nvl (ait.campaign_name,'0') = nvl (ism.campaign_name,'0')
              AND nvl (ait.campaign_source,'0') = nvl (ism.campaign_source,'0')
      WHERE ait.created_datetime > '2017-01-01 00:00:00') a
  JOIN fact_core_item fci ON a.idcustomer = fci.idcustomer and fci.order_created_date >=20170101
  group by 1,2,3,4) b on a.keyspace=b.keyspace and a.install_month=b.install_month and a.class=b.class;

