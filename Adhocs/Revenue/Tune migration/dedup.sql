drop table if exists customer_insights.app_install_dedup;

create table customer_insights.app_install_dedup as
(SELECT DISTINCT a.device_id,
       CASE
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NULL THEN 'tune'
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NOT NULL THEN 'tune'
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NULL THEN 'apsalar'
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NOT NULL THEN 'tune'
       END AS source,
              CASE
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NULL THEN a.keyspace
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NOT NULL THEN a.keyspace
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NULL THEN b.keyspace
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NOT NULL THEN a.keyspace
       END AS keyspace,
       CASE
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NULL THEN a.campaign_source
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NOT NULL THEN a.campaign_source
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NULL THEN b.campaign_source
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NOT NULL THEN a.campaign_source
       END AS campaign_source,
       CASE
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NULL THEN a.campaign_group
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NOT NULL THEN a.campaign_group
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NULL THEN b.campaign_group
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NOT NULL THEN a.campaign_group
       END AS campaign_group,
              CASE
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NULL THEN a.campaign_name
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NOT NULL THEN a.campaign_name
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NULL THEN b.campaign_name
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NOT NULL THEN a.campaign_name
       END AS campaign_name,
              CASE
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NULL THEN a.click_datetime
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NOT NULL THEN a.click_datetime
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NULL THEN b.click_date
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NOT NULL THEN a.click_datetime
       END AS click_date,
              CASE
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NULL THEN a.created_datetime
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NOT NULL THEN a.created_datetime
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NULL THEN b.install_date
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NOT NULL THEN a.created_datetime
       END AS created_date,
              CASE
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NULL THEN a.pricing_model
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NOT NULL THEN a.pricing_model
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NULL THEN b.pricing_model
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NOT NULL THEN a.pricing_model
       END AS pricing_model,
              CASE
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NULL THEN a.price
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NOT NULL THEN a.price
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NULL THEN a.price
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NOT NULL THEN a.price
       END AS price,
              CASE
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NULL THEN a.source_app_site
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NOT NULL THEN a.source_app_site
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NULL THEN b.source_app_site
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NOT NULL THEN a.source_app_site
       END AS source_app_site,
              CASE
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NULL THEN a.creative
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NOT NULL THEN a.creative
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NULL THEN b.creative
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NOT NULL THEN a.creative
       END AS creative,
                     CASE
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NULL THEN a.long_name
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NOT NULL THEN a.long_name
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NULL THEN b.longname
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NOT NULL THEN a.long_name
       END AS long_name,
                     CASE
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NULL THEN a.custom_user_id
         WHEN b.campaign_source = 'Organic' AND a.campaign_source IS NOT NULL THEN a.custom_user_id
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NULL THEN b.custom_user_id
         WHEN b.campaign_source != 'Organic' AND a.campaign_source IS NOT NULL THEN a.custom_user_id
       END AS custom_user_id      
FROM (SELECT DISTINCT device_id,keyspace,campaign_name,campaign_source,campaign_group,click_datetime,created_datetime,pricing_model,price,source_app_site,creative,long_name,custom_user_id
      FROM app_install_tune
      WHERE created_datetime >= '2016-08-17 00:00:00'
      AND   attribution = 'Install') a
  JOIN (SELECT DISTINCT device_id,keyspace,campaign_name,campaign_source,campaign_group,click_date,install_date,pricing_model,price,source_app_site,creative,longname,custom_user_id
        FROM app_referral
        WHERE install_date > '2016-08-17 00:00:00' 
        AND   attribution = 'Install') b ON a.device_id = b.device_id);

grant select on customer_insights.app_install_dedup to adhocpanel;
grant select on customer_insights.app_install_dedup to adhoc_fin_user;

