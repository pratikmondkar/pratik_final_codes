drop table if exists dev.uidx_last_6months;

create table dev.uidx_last_6months distkey(uidx) as 
(select distinct uidx
from 
(select uidx from clickstream.daily_aggregates where uidx is not null and load_date >=to_char(sysdate - interval '6 months','YYYYMMDD')::bigint
union all
select uidx from clickstream.daily_notif_aggregates where uidx is not null and load_date >=to_char(sysdate - interval '6 months','YYYYMMDD')::bigint
union all 
select uidx from customer_insights.daily_aggregates_web where uidx is not null)
);

select count(*) from dev.uidx_last_6months ;

drop table if exists dev.fpe_temp1;

create table dev.fpe_temp1 distkey(uidx) as 
(select a.uidx,brand as brand_aff,rnk
from dev.uidx_last_6months a
  LEFT JOIN (SELECT uidx,
                    brand,
                    rnk
             FROM (SELECT user_id as uidx,
                          brand,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime
                   where identifier='uidx'
                   GROUP BY 1,
                            2)
             WHERE rnk <= 9) b on a.uidx=b.uidx);
             
select * from  dev.fpe_temp1 limit 100;

drop table if exists dev.fpe_temp2;

create table dev.fpe_temp2 distkey(uidx) as 
(SELECT uidx,
                    gender
             FROM (SELECT user_id as uidx,
                          gender,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime
                   where identifier='uidx'
                   GROUP BY 1,
                            2)
             WHERE rnk = 1) ;



drop table if exists dev.fpe_temp3;

create table dev.fpe_temp3 distkey(uidx) as 
(SELECT uidx,article_type,
                    gender,rnk
             FROM (SELECT user_id as uidx,
                          article_type,
                          gender,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime
                   where identifier='uidx'
                   GROUP BY 1,
                            2,3);

drop table if exists dev.syle_lookup;

create Table dev.syle_lookup as 
(select uidx,article_type,gender,style_id
from
(select customer_login as uidx,article_type,fci.gender,style_id,row_number() over (partition by uidx ,article_type,fci.gender order by order_created_date desc,order_created_time desc) as rn
from fact_core_item fci
left join dim_customer_idea dci on fci.idcustomer=dci.id
where is_realised=1 and store_id=1)
where rn=1);

DROP TABLE IF EXISTS dev.default_brand;

CREATE TABLE dev.default_brand
(
            brand                          varchar(255)
)
DISTKEY (brand);

truncate table dev.default_brand;

copy dev.default_brand   (brand)
FROM 's3://testashutosh/backups/default_brand.csv' credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
delimiter ',' 
emptyasnull 
blanksasnull 
TRIMBLANKS 
TRUNCATECOLUMNS 
ACCEPTANYDATE 
ACCEPTINVCHARS AS ' ' 
maxerror 10 
explicit_ids 
compupdate off 
statupdate off;


drop table if exists dev.temp_ag;

create table dev.temp_ag as
(select uidx,nvl(inferred_gender,'Unisex') as inferred_gender,dci.gender as registered_gender,dci.first_name,
max(case when rnk=1 then article_type end) as article_type1,
max(case when rnk=2 then article_type end) as article_type2,
max(case when rnk=3 then article_type end) as article_type3,
max(case when rnk=4 then article_type end) as article_type4,
max(case when rnk=5 then article_type end) as article_type5, 
max(case when rnk=1 then e.gender end) as gender1,
max(case when rnk=2 then e.gender end) as gender2,
max(case when rnk=3 then e.gender end) as gender3,
max(case when rnk=4 then e.gender end) as gender4,
max(case when rnk=5 then e.gender end) as gender5,
max(case when rnk=1 then style_id end) as style_id1,
max(case when rnk=2 then style_id end) as style_id2,
max(case when rnk=3 then style_id end) as style_id3,
max(case when rnk=4 then style_id end) as style_id4,
max(case when rnk=5 then style_id end) as style_id5
from
(select a.uidx,b.gender as inferred_gender,c.article_type,c.gender,d.style_id,row_Number() over (partition by a.uidx order by rnk) as rnk
from dev.uidx_last_6months a
left join dev.fpe_temp2 b on a.uidx=b.uidx
left join dev.fpe_temp3 c on a.uidx=c.uidx
left join dev.syle_lookup d on a.uidx=d.uidx and c.article_type=d.article_type and c.gender=d.gender) e
left join dim_customer_idea dci on e.uidx=dci.customer_login
where rnk<=5
group by 1,2,3,4);

drop table if exists dev.b10_final;
create table dev.b10_final distkey(uidx) as
(select a.*,
brand1,
brand2,
brand3,
brand4,
brand5,
brand6,
brand7,
brand8,
brand9
from 
dev.temp_ag a
left join 
(select uidx, 
max(case when fn_rnk=1 then brand end) as brand1,
max(case when fn_rnk=2 then brand end) as brand2,
max(case when fn_rnk=3 then brand end) as brand3,
max(case when fn_rnk=4 then brand end) as brand4,
max(case when fn_rnk=5 then brand end) as brand5,
max(case when fn_rnk=6 then brand end) as brand6,
max(case when fn_rnk=7 then brand end) as brand7,
max(case when fn_rnk=8 then brand end) as brand8,
max(case when fn_rnk=9 then brand end) as brand9
from dev.final
group by 1) b on a.uidx=b.uidx );

grant select on customer_insights.b10_final to adhoc_clickstream_fin;
grant select on customer_insights.b10_final to explain_clickstream_fin;


grant select on customer_insights.b10_final to adhoc_clickstream_no_fin;
grant select on customer_insights.b10_final to explain_clickstream_no_fin;

select * from 
dev.fpe_temp3 where uidx='c00198c2.bb59.4d44.9c38.30171c425821C8MGTguNY8';


select * from dev.final limit 10;

update dev.final
set brand='all about you'
where brand ='AAY';


select email from cii.dim_customer_email dce
left join dim_customer_idea dci on dce.uid=dci.id
where customer_login ='c00198c2.bb59.4d44.9c38.30171c425821C8MGTguNY8';

select distinct customer_login as uidx,article_type,fci.gender,style_id,order_group_id
from fact_core_item fci
left join dim_customer_idea dci on fci.idcustomer=dci.id
where is_realised=1 and store_id=1 and customer_login ='c00198c2.bb59.4d44.9c38.30171c425821C8MGTguNY8';


select distinct article_type from dim_style;
