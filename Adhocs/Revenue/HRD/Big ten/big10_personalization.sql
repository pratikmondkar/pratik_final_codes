drop table if exists dev.uidx_last_6months;

create table dev.uidx_last_6months distkey(uidx) as 
(select distinct uidx
from 
(select uidx from clickstream.daily_aggregates where uidx is not null and load_date >=to_char(sysdate - interval '6 months','YYYYMMDD')::bigint
union all
select uidx from clickstream.daily_notif_aggregates where uidx is not null and load_date >=to_char(sysdate - interval '6 months','YYYYMMDD')::bigint
union all 
select uidx from customer_insights.daily_aggregates_web where uidx is not null)
);

select count(*) from dev.uidx_last_6months ;

drop table if exists dev.fpe_temp1;

create table dev.fpe_temp1 distkey(uidx) as 
(select a.uidx,brand as brand_aff
from dev.uidx_last_6months a
  LEFT JOIN (SELECT uidx,
                    brand
             FROM (SELECT user_id as uidx,
                          brand,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime
                   where identifier='uidx'
                   GROUP BY 1,
                            2)
             WHERE rnk = 1) b on a.uidx=b.uidx);
             
select * from  dev.fpe_temp2 limit 100;

drop table if exists dev.fpe_temp2;

create table dev.fpe_temp2 distkey(uidx) as 
(select a.uidx,gender as inferred_gender
from dev.uidx_last_6months a
  LEFT JOIN (SELECT uidx,
                    brand
             FROM (SELECT user_id as uidx,
                          gender,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime
                   where identifier='uidx'
                   GROUP BY 1,
                            2)
             WHERE rnk = 1) b on a.uidx=b.uidx);



drop table if exists dev.syle_lookup;

create Table dev.syle_lookup as 
(select uidx,article_type,gender,style_id
(select uidx,article_type,gender,style_id,row_number() over (partition by uidx ,article_type,gender order by order_created_date desc,order_created_time desc) as rn
from fact_core_item fci
where is_realised=1 and store_id=1)
where rn=1);

drop table if exists dev.fpe_temp2;

create table dev.fpe_temp2 distkey(uidx) as 
(select a.*,bu as bu_aff
from dev.fpe_temp1 a
  LEFT JOIN (SELECT uidx,
                    bu
             FROM (SELECT user_id as uidx,
                          b.bu,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime n
                   join dev.bag_bu_mapping b on n.brand=b.brand and n.article_type =b.article_type and n.gender=b.gender
                   where identifier='uidx'
                   GROUP BY 1,
                            2)
             WHERE rnk = 1) b on a.uidx=b.uidx);


drop table if exists dev.fpe_raw;

create table dev.fpe_raw distkey(uidx) as
(select a.*,b.style_1,b.style_2,b.style_3,b.style_4,b.style_5
from dev.fpe_temp2 a
left join
(select uidx,
max(case when rn=1 then style_id end) as style_1,
max(case when rn=2 then style_id end) as style_2,
max(case when rn=3 then style_id end) as style_3,
max(case when rn=4 then style_id end) as style_4,
max(case when rn=5 then style_id end) as style_5
from dev.syle_lookup
where rn<=5
group by 1) b on a.uidx=b.uidx
);


drop table if exists dev.hpe_prefinal;

create table dev.hpe_prefinal distkey(uidx) as
(select a.uidx,style_1,style_2,style_3,style_4,style_5,d.new_brand1,new_brand2,new_brand3,
b.gig_id1,b.gig_id2,c.gender,c.first_name,datediff(d,to_date(c.first_login_date,'YYYYMMDD'),sysdate) as we_know_you_days,round((random()*40)+50) as celeb_match_percentage,
celeb as celeb_twin_name
from dev.fpe_raw a
left join dev.gig_mapping b on lower(a.brand_aff)=lower(b.brand)
left join bidb.dim_customer_idea c on a.uidx=c.customer_login
left join dev.new_brand_mapping d on lower(a.bu_aff)=lower(d.bu)
left join dev.celeb_mapping e on lower(a.brand_aff)=lower(e.brand) and c.gender=e.gender);
       
drop table if exists dev.hpe_final;

create table dev.hpe_final distkey(uidx) as 
(select uidx,style_1,style_2,style_3,style_4,style_5,
nvl(new_brand1,case when gender='f' then 'Dorothy Perkins' else 'Aeropostale' end) as new_brand1,
nvl(new_brand2,case when gender='f' then 'Bobbi Brown' when gender='m' then 'NBA' else 'Next' end) as new_brand2,
nvl(new_brand3,case when gender='f' then 'Keds' when gender='m' then 'Next' else 'Dorothy Perkins' end) as new_brand3,
nvl(gig_id1,case when gender='f' then 19 else 16 end) as gig_id1,
nvl(gig_id2,case when gender='f' then 34 when gender='m' then 7 else 19 end) as gig_id2,
gender,first_name,we_know_you_days,celeb_match_percentage,
nvl(celeb_twin_name,case when gender='m' then 'Salman Khan' when gender='f' then 'Deepika Padukone' end) as celeb_twin_name
from dev.hpe_prefinal) ;

grant select on dev.hpe_final to customer_insights_ddl;

drop table if exists customer_insights.hpe_final;

create table  customer_insights.hpe_final distkey(uidx) as 
(select *
from dev.hpe_final
where 
       style_1 is not null and
       style_2 is not null and
       style_3 is not null and
       style_4 is not null and
       style_5 is not null and
       new_brand1 is not null and
       new_brand2 is not null and
       new_brand3 is not null and
       gig_id1 is not null and
       gig_id2 is not null and
       we_know_you_days is not null and 
       celeb_match_percentage is not null and
       celeb_twin_name is not null) ;
       

grant select on customer_insights.hpe_final to adhoc_clickstream_fin;
grant select on customer_insights.hpe_final to explain_clickstream_fin;

drop table  customer_insights.hpe_final2;

create table customer_insights.hpe_final2 distkey(uidx) as
(
select a.*,b.gender as infered_gender
from
dev.hpe_final a
left join
(SELECT uidx,
                    gender
             FROM (SELECT user_id as uidx,
                          gender,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime n
                   where identifier='uidx'
                   GROUP BY 1,
                            2)
             WHERE rnk = 1 ) b on a.uidx =b.uidx);
            

drop table  dev.hpe_final2;

create table dev.hpe_final2 distkey(uidx) as 
(select uidx,style_1,style_2,style_3,style_4,style_5,new_brand1,new_brand2,new_brand3,gig_id1,gig_id2,gender,first_name,we_know_you_days,celeb_match_percentage,
nvl(celeb_twin_name,case when infered_gender='Men' then 'Salman Khan' when infered_gender='Women' then 'Deepika Padukone' end) as celeb_twin_name
from customer_insights.hpe_final2) ;


drop table  customer_insights.hpe_final3;

create table  customer_insights.hpe_final3 distkey(uidx) as 
(select *
from dev.hpe_final2
where 
       style_1 is not null and
       style_2 is not null and
       style_3 is not null and
       style_4 is not null and
       style_5 is not null and
       new_brand1 is not null and
       new_brand2 is not null and
       new_brand3 is not null and
       gig_id1 is not null and
       gig_id2 is not null and
       we_know_you_days is not null and 
       celeb_match_percentage is not null and
       celeb_twin_name is not null) ;
       

grant select on customer_insights.hpe_final3 to adhoc_clickstream_fin;
grant select on customer_insights.hpe_final3 to explain_clickstream_fin;


select gig_id2,count(*) from customer_insights.hpe_final3 group by 1 limit 100;

select * from customer_insights.hpe_final3;


