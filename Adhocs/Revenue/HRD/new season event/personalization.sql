drop table if exists dev.cus_at;

create table dev.cus_at distkey(uidx) as
(SELECT a.*,
       b.article_type,
       b.gender,
       b.rnk,
       c.style_id
FROM dev.active_base a      
   JOIN (select uidx,article_type,gender,rnk
          from
							(select customer_Login as uidx ,foi.article_type,foi.gender,sum(quantity) as qty,ROW_NUMBER() OVER (PARTITION BY uidx ORDER BY qty DESC) AS rnk
		          from fact_core_item foi
		          join dim_customer_idea dci on foi.idcustomer=dci.id
		          join dev.top_50_at t on foi.article_type=t.article_type and foi.gender=t.gender
		          where is_realised=1 and store_id=1 and order_created_date between 20160101 and 20170201
		          group by 1,2,3)
		          where rnk<=5) b ON a.uidx = b.uidx  	          
	 left join (select uidx,article_type,gender,style_id
          from
							(select customer_Login as uidx,foi.article_type,foi.gender,style_id,ROW_NUMBER() OVER (PARTITION BY uidx,foi.article_type,foi.gender ORDER BY order_created_date,order_created_time) AS rnk
		          from fact_core_item foi
		          join dim_customer_idea dci on foi.idcustomer=dci.id
							join dev.top_50_at t on foi.article_type=t.article_type and foi.gender=t.gender
		          where is_realised=1 and store_id=1 and order_created_date between 20160101 and 20170201)
		          where rnk=1) c on b.uidx=c.uidx and b.article_type=c.article_type and b.gender=c.gender           
);

drop table if exists dev.cus_brand;

create table dev.cus_brand distkey(uidx) as
(SELECT a.*,
       b.brand,
       b.rnk,
       c.style_id
FROM dev.active_base a      
   JOIN (select uidx,brand,rnk
          from
							(select customer_Login as uidx ,foi.brand,sum(quantity) as qty,ROW_NUMBER() OVER (PARTITION BY uidx ORDER BY qty DESC) AS rnk
		          from fact_core_item foi
		          join dim_customer_idea dci on foi.idcustomer=dci.id
							join dev.top_50_brand t on foi.brand=t.brand
		          where is_realised=1 and store_id=1 and order_created_date between 20160101 and 20170201
		          group by 1,2)
		          where rnk<=3) b ON a.uidx = b.uidx   
	 left join (select uidx,brand,style_id
          from
							(select customer_Login as uidx,foi.brand,style_id,ROW_NUMBER() OVER (PARTITION BY uidx,foi.brand ORDER BY order_created_date,order_created_time) AS rnk
		          from fact_core_item foi
		          join dim_customer_idea dci on foi.idcustomer=dci.id
							join dev.top_50_brand t on foi.brand=t.brand
		          where is_realised=1 and store_id=1 and order_created_date between 20160101 and 20170201)
		          where rnk=1) c on b.uidx=c.uidx and b.brand=c.brand         
);


SELECT a.*,
       b.first_name,
       b.gender
FROM (SELECT nvl(a.uidx,b.uidx) AS uidx,at1,at_style1,at2,at_style2,at3,at_style3,at4,at_style4,at5,at_style5,brand1,brand_style1,brand2,brand_style2,brand3,brand_style3
			FROM
						(SELECT 
						uidx,
						MAX(CASE WHEN rnk=1 THEN article_type END) AS at1,
						MAX(CASE WHEN rnk=1 THEN style_id END) AS at_style1,
						MAX(CASE WHEN rnk=2 THEN article_type END) AS at2,
						MAX(CASE WHEN rnk=2 THEN style_id END) AS at_style2,
						MAX(CASE WHEN rnk=3 THEN article_type END) AS at3,
						MAX(CASE WHEN rnk=3 THEN style_id END) AS at_style3,
						MAX(CASE WHEN rnk=4 THEN article_type END) AS at4,
						MAX(CASE WHEN rnk=4 THEN style_id END) AS at_style4,
						MAX(CASE WHEN rnk=5 THEN article_type END) AS at5,
						MAX(CASE WHEN rnk=5 THEN style_id END) AS at_style5
						FROM dev.cus_at
						GROUP BY 1) a
			FULL OUTER JOIN
						(SELECT 
						uidx,
						MAX(CASE WHEN rnk=1 THEN brand END) AS brand1,
						MAX(CASE WHEN rnk=1 THEN style_id END) AS brand_style1,
						MAX(CASE WHEN rnk=2 THEN brand END) AS brand2,
						MAX(CASE WHEN rnk=2 THEN style_id END) AS brand_style2,
						MAX(CASE WHEN rnk=3 THEN brand END) AS brand3,
						MAX(CASE WHEN rnk=3 THEN style_id END) AS brand_style3
						FROM dev.cus_brand
						GROUP BY 1) b ON a.uidx=b.uidx) a
						left join dim_customer_idea b on a.uidx=b.customer_login;


