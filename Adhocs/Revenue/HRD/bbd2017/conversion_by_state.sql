--create initial table
drop table dev.pm_temp1;
create table dev.pm_temp1 distkey(state_name) sortkey(date) as
(select z.*,y.state_code from
(select nvl(t.load_date,r.order_created_date) as date,nvl(t.state_name,r.state) as state_name, nvl(t.channel,r.channel) as channel,
nvl(sessions,0) as sessions,nvl(users,0) as users, nvl(revenue,0) as revenue,nvl(orders,0) as orders,nvl(customers,0) as customers
from
(select load_date,channel,UPPER(nvl(state_name,'Unknown')) as state_name,sum(sessions) as sessions,sum(users) as users from
(select load_date,channel,nvl(city,'Unknown') as city,sum(sessions) as sessions, count(distinct device_id) as users
from (select load_date,os as channel,city,device_id,sessions from clickstream.daily_aggregates where load_date>=20170801
union all 
select load_date,os as channel,city,device_id,sessions from customer_insights.daily_aggregates_web where load_date>=20170801)
group by 1,2,3) a
left join customer_insights.city_state_mapping b on lower(a.city)=lower(b.city)
 group by 1,2,3)t
full outer join 
(select order_created_date,
case when device_channel='web' then 'Desktop' when device_channel='mobile-web' then 'Mweb' else os_info end as channel,
upper(nvl(case when statename='ORISSA' then 'ODISHA' 	
when statename='ANDAMAN & NICOBAR ISLANDS' then 'ANDAMAN and NICOBAR ISLANDS' 
when statename='PONDICHERRY' then 'Puducherry'
else statename end,'Unknown')) as state,
sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as revenue,
count(distinct order_group_id) as orders,count(distinct idcustomer) as customers
from fact_core_item fci
left join dim_location dl on fci.idlocation=dl.id
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date >=20170801
group by 1,2,3) r on upper(t.state_name)=upper(r.state) and t.load_date=r.order_created_date and t.channel=r.channel) z
left join (select distinct state_name,state_code from customer_insights.city_state_mapping) y on UPPER(z.state_name)=UPPER(y.state_name));

--load into original table

drop table customer_insights.converzion_by_state;

create table customer_insights.converzion_by_state distkey(state_name) sortkey(date) as
(select * from dev.pm_temp1)



--update query

BEGIN;

delete from customer_insights.converzion_by_state where date>=to_char(sysdate - interval '2 day','YYYYMMDD')::bigint;

insert into customer_insights.converzion_by_state
(select z.*,y.state_code from
(select nvl(t.load_date,r.order_created_date) as date,nvl(t.state_name,r.state) as state_name, nvl(t.channel,r.channel) as channel,nvl(sessions,0) as sessions,nvl(users,0) as users,
nvl(revenue,0) as revenue,nvl(orders,0) as orders,nvl(customers,0) as customers
from
(select load_date,channel,UPPER(nvl(state_name,'Unknown')) as state_name,sum(sessions) as sessions,sum(users) as users from
(select load_date,CASE WHEN app_name = 'MyntraRetailWeb' AND lower(device_category) = 'mobile' THEN 'Mweb' WHEN app_name = 'MyntraRetailWeb' AND lower(device_category) = 'desktop' THEN 'Desktop'
when app_name in ('Myntra','MyntraRetailAndroid') then os END AS channel,
nvl(city,'Unknown') as city,count(distinct session_id) as sessions, count(distinct device_id) as users
from clickstream.events_view  
where event_type in ('appLaunch','StreamCardLoad','ScreenLoad','StreamCardChildLoad') and load_date>=to_char(sysdate - interval '2 day','YYYYMMDD')::bigint
group by 1,2,3) a
left join customer_insights.city_state_mapping b on lower(a.city)=lower(b.city)
 group by 1,2,3)t
full outer join 
(select order_created_date,
case when device_channel='web' then 'Desktop' when device_channel='mobile-web' then 'Mweb' else os_info end as channel,
upper(nvl(case when statename='ORISSA' then 'ODISHA' 
when statename='ANDAMAN & NICOBAR ISLANDS' then 'ANDAMAN and NICOBAR ISLANDS' 
when statename='PONDICHERRY' then 'Puducherry'
else statename end,'Unknown')) as state,
sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as revenue,
count(distinct order_group_id) as orders,count(distinct idcustomer) as customers
from fact_core_item fci
left join dim_location dl on fci.idlocation=dl.id
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date >=to_char(sysdate - interval '2 day','YYYYMMDD')::bigint
group by 1,2,3) r on upper(t.state_name)=upper(r.state) and t.load_date=r.order_created_date and t.channel=r.channel) z
left join (select distinct state_name,state_code from customer_insights.city_state_mapping) y on UPPER(z.state_name)=UPPER(y.state_name));

END;
