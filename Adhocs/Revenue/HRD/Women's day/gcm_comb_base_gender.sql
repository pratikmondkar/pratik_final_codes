drop table if exists dev.comb_base;

create table dev.comb_base distkey(device_id) as 
(select distinct device_id,max(os) as os
from
(select distinct lower(device_id) as device_id,os from clickstream.daily_aggregates where os in ('Android','iOS') and load_date>=to_char(sysdate - interval '31 days','YYYYMMDD')::bigint
union all
select distinct lower(device_id) as device_id,os from clickstream.daily_notif_aggregates where os in ('Android','iOS') and load_date>=to_char(sysdate - interval '31 days','YYYYMMDD')::bigint
union all
select lower(device_id) as device_id,'Android' as os from dev.gcm_and_20170302
union all
select lower(device_id) as device_id,'iOS' as os from dev.gcm_ios_20170302)
group by 1);


select os,count(*) from dev.comb_base group by 1;

drop table if exists dev.comb_base_gender;

create table dev.comb_base_gender distkey(device_id) as
(select cb.device_id,os,gender,inferred_gender 
from dev.comb_base cb
left join 
(select a.device_id,gender
from
(select lower(device_id) as device_id,user_id as uidx,row_number() over (partition by device_id order by updated_date desc ) as rnk
from bidb.app_users_magasin) a
left join bidb.dim_customer_idea dci on a.uidx=dci.customer_login
where rnk=1) c on cb.device_id=c.device_id
left join 
(SELECT  device_id,
                    gender as inferred_gender
             FROM (SELECT user_id as device_id,
                          gender,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime
                   WHERE identifier='device_id'
                   GROUP BY 1,
                            2)
             WHERE rnk = 1) d on cb.device_id=d.device_id);


select nvl(case when gender in ('m','f') then gender end, case when inferred_gender in ('Men','Boys') then 'm' when  inferred_gender in ('Women','Girls') then 'f' end,'unk') as final_gender,count(distinct device_id)
from dev.comb_base_gender where os='Android'
group by 1 ;

select distinct device_id, nvl(case when gender in ('m','f') then gender end, case when inferred_gender in ('Men','Boys') then 'm' when  inferred_gender in ('Women','Girls') then 'f' end,'unk') as final_gender
from dev.comb_base_gender_ios where os='iOS';


select * from stv_inflight;



