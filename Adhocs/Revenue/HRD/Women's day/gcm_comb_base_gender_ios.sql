drop table if exists dev.comb_base_gender_ios;

create table dev.comb_base_gender_ios distkey(device_id) as
(select cb.device_id,os,gender,inferred_gender 
from dev.comb_base cb
left join 
(select a.device_id,gender
from
(select device_id,user_id as uidx,row_number() over (partition by device_id order by updated_date desc ) as rnk
from bidb.app_users_magasin) a
left join cii.dim_customer_email dce on a.uidx=dce.email
left join bidb.dim_customer_idea dci on dce.uid=dci.id
where rnk=1) c on cb.device_id=c.device_id
left join 
(SELECT  device_id,
                    gender as inferred_gender
             FROM (SELECT user_id as device_id,
                          gender,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_person_base_lifetime
                   WHERE identifier='device_id'
                   GROUP BY 1,
                            2)
             WHERE rnk = 1) d on cb.device_id=d.device_id
             where os='iOS');
             

select * from dev.comb_base_gender_ios limit 100;

select nvl(case when gender in ('m','f') then gender end, case when inferred_gender in ('Men','Boys') then 'm' when  inferred_gender in ('Women','Girls') then 'f' end,'unk') as final_gender,count(distinct device_id)
from dev.comb_base_gender_ios where os='iOS'
group by 1 ;

select device_id,advertising_id,user_id from app_users_magasin where lower(device_id)='ecb85764-9e4b-4528-a72b-dbae1254abf9';

select count(distinct device_id) from app_users_magasin where device_type='iOS' and user_id not like '%@%' and user_id !='NA' limit 100;


select * from dev.d30_base_gender_ios2
where device_id in ('5AFB2026-8C74-43F2-A1C6-7B8E9F23691B','77CEF32A-52ED-44AC-9B0F-0EE1A11D301C');


