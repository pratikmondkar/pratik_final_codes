select * from dev.ab_women_day_final limit 100;

select count(*) from dev.ab_women_day_final;

drop table if exists dev.women_day_final;

create table dev.women_day_final distkey(uidx) as
(select
customer_login as uidx,
name,
base_colour,
week_number,
brand,
case
when brand in ('W','Biba','Anouk','Libas','Shree','IMARA','Jaipur Kurti','RANGMANCH BY PANTALOONS') then 'The Contemporary Look'
when brand in ('Marks & Spencer','MANGO','Moda Rapido','Mast & Harbour','Vero Moda','ONLY') then 'The Classy Look'
when brand in ('FOREVER 21','Global Desi','Roadster','FabAlley','KnK') then 'The Quirky Look'
when brand in ('DressBerry','all about you','Lavie') then 'The Elegant Look'
when brand in ('Puma','Nike','Tommy Hilfiger','Adidas NEO','ether') then 'The Sporty Look' 
else 'The Chic Look' end as look,
case 
when units_bought::float8/NUllif(case when units_bought>pdps then units_bought else pdps end,0) between 0 and 0.002599999999 then 'Browsing Bree'
when units_bought::float8/NUllif(case when units_bought>pdps then units_bought else pdps end,0) between 0.0026 and 0.015299999999 then 'Picky penelope'
when units_bought::float8/NUllif(case when units_bought>pdps then units_bought else pdps end,0) between 0.0153 and 0.08333333333 then 'Sensible Susan'
when units_bought::float8/NUllif(case when units_bought>pdps then units_bought else pdps end,0) >0.08333333333 then ' Spontaneos Sally'
end as buyer_type,
case when b.article_type is not  null then b.bu
when a.article_type is not null and b.bu is null then 'Personal Care' end  as bu
from dev.ab_women_day_final a
left join dev.article_type_map b
on a.article_type = b.article_type);

drop table if exists dev.women_day_personlize;

create table dev.women_day_personlize distkey(uidx) as 
(select *,case when base_colour is null and week_number is null and buyer_type is null and bu is null then 1 else 0 end as no_data_flag from dev.women_day_final );


--surbhi   49d585c5.8e43.42ca.9d0b.2725aed27b7ceiOtohQH8R
--savita   36a3ec03.ef64.415d.916e.f5f2d793b606Ony5YC8QxQ


select buyer_type,count(*) from dev.women_day_final group by 1 limit 100;

select * from dev.women_day_final where uidx in ('49d585c5.8e43.42ca.9d0b.2725aed27b7ceiOtohQH8R','be4f1044.095f.46d9.a03c.f1e17285d635jO82bh4F6w',
'36a3ec03.ef64.415d.916e.f5f2d793b606Ony5YC8QxQ','5665d440.353e.4768.a9cd.9bf8803359f2NaVLGMX6OK');

select count(*) from dev.women_day_personlize where (base_colour is null or week_number is null or buyer_type is null or bu is null) and no_data_flag=0 limit 100;

select * from notif_person_base_lifetime where user_id in ('49d585c5.8e43.42ca.9d0b.2725aed27b7ceiOtohQH8R','be4f1044.095f.46d9.a03c.f1e17285d635jO82bh4F6w','36a3ec03.ef64.415d.916e.f5f2d793b606Ony5YC8QxQ') limit 1000
