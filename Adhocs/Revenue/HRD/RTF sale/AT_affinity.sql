SELECT a.device_id,
       nvl(a.first_name,'Customer') as first_name,
       b.article_type
FROM (SELECT device_id,
             MAX(dci.first_name) AS first_name
      FROM (SELECT device_id,
                   uidx
            FROM clickstream.daily_aggregates
            WHERE load_date >= TO_CHAR(SYSDATE-INTERVAL '30 days','YYYYMMDD')::BIGINT
            UNION ALL
            SELECT device_id,
                   uidx
            FROM clickstream.daily_notif_aggregates
            WHERE load_date >= TO_CHAR(SYSDATE-INTERVAL '30 days','YYYYMMDD')::BIGINT) u
        LEFT JOIN dim_customer_idea dci ON u.uidx = dci.customer_login
      GROUP BY 1) a
  LEFT JOIN (SELECT device_id,
                    article_type
             FROM (SELECT device_id,
                          article_type,
                          SUM(pdp_view) +5 *SUM(liked) +10 *SUM(added_to_collection) +10 *SUM(addtolist) +10 *SUM(addtocart) AS score,
                          ROW_NUMBER() OVER (PARTITION BY device_id ORDER BY score DESC) AS rnk
                   FROM bidb.notif_personalize
                   GROUP BY 1,
                            2)
             WHERE rnk = 1) b on a.device_id=b.device_id
