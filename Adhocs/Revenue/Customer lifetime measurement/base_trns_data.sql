drop table if exists dev.orders_all;
create table dev.orders_all distkey(idcustomer) sortkey(order_created_date) as 
select * from
(select idcustomer_idea as idcustomer, order_created_date,article_type,sum(item_revenue_inc_cashback) as revenue,
sum(quantity) as quantity,sum(product_discount) as product_discount ,sum(coupon_discount) as coupon_discount
from fact_orderitem foi
join dim_product dp on foi.sku_id=dp.sku_id
WHERE (is_shipped = 1 OR is_realised = 1)
AND   store_id = 1
and order_created_date<=20150331
group by 1,2,3
union all
select idcustomer, order_created_date,article_type,sum(item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0)) as revenue,
sum(quantity) as quantity,sum(product_discount) as product_discount,sum(coupon_discount) as coupon_discount
from fact_core_item 
WHERE (is_shipped = 1 OR is_realised = 1)
AND   store_id = 1
and order_created_date>20150331
group by 1,2,3)
