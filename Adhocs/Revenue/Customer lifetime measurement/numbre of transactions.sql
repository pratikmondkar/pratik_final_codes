SELECT purchase_seq,
       COUNT(DISTINCT idcustomer_idea) AS cus,
       AVG(days_since_reg) AS avg_days_since_reg
FROM (SELECT DISTINCT idcustomer_idea,
             first_login_date,
             order_created_date,
             datediff(d,TO_DATE(first_login_date,'YYYYMMDD'),TO_DATE(order_created_date,'YYYYMMDD')) AS days_since_reg,
             sum(shipped_item_revenue_inc_cashback) as rev,
             sum(shipped_item_count) as qty,
             sum(shipped_product_discount+shipped_coupon_discount) as disc, 
             ROW_NUMBER() OVER (PARTITION BY idcustomer_idea ORDER BY order_created_date) AS purchase_seq
      FROM fact_order fo
        JOIN dim_customer_idea dci ON fo.idcustomer_idea = dci.id
      WHERE (is_shipped = 1 OR is_realised = 1)
      AND   store_id = 1
      AND   first_login_date >= 20150101)
group by 1
order by 1
limit 100;



SELECT count(*)
FROM (SELECT idcustomer_idea,
             first_login_date,
             order_created_date,
             datediff(d,TO_DATE(first_login_date,'YYYYMMDD'),TO_DATE(order_created_date,'YYYYMMDD')) AS days_since_reg,
             sum(shipped_order_revenue_inc_cashback) as rev,
             sum(shipped_item_count) as qty,
             sum(shipped_product_discount+shipped_coupon_discount) as disc, 
             ROW_NUMBER() OVER (PARTITION BY idcustomer_idea ORDER BY order_created_date) AS purchase_seq
      FROM fact_order fo
        JOIN dim_customer_idea dci ON fo.idcustomer_idea = dci.id
      WHERE (is_shipped = 1 OR is_realised = 1)
      AND   store_id = 1
      AND   first_login_date >= 20150101
      group by 1,2,3,4)

limit 100
