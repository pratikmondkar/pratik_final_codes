DROP TABLE if exists dev.aum_dedup;

CREATE TABLE dev.aum_dedup distkey 
(
  advertising_id   
)
AS
(SELECT device_id,
       MIN(advertising_id) AS advertising_id,
       MIN(created_date) AS created_date
FROM app_users_magasin
GROUP BY 1);

DROP TABLE if exists dev.adv_temp;

CREATE TABLE dev.adv_temp distkey 
(
  device_id   
)
AS
(SELECT device_id,
       MIN(install_date) AS install_date
FROM (SELECT device_id,
             install_date
      FROM app_referral
      WHERE attribution = 'Install'
      AND   keyspace IN ('AIFA','IDFA')
      AND   install_date < '2016-08-18 00:00:00'
      UNION ALL
      SELECT device_id,
             created_datetime AS install_date
      FROM app_install_tune
      WHERE attribution = 'Install'
      AND   created_datetime >= '2016-08-18 00:00:00')
WHERE device_id IS NOT NULL
GROUP BY 1);

DROP TABLE if exists dev.temp_d123;

CREATE TABLE dev.temp_d123 distkey 
(
  device_id   
)
AS
(SELECT device_id,
       MIN(install_date) AS install_date
FROM app_referral
WHERE attribution = 'Install'
AND   keyspace IN ('ANDI')
AND   install_date < '2016-08-18 00:00:00'
GROUP BY 1);

DROP TABLE if exists dev.device_install;

CREATE TABLE dev.device_install distkey(device_id)
AS
(SELECT aum.device_id,
       LEAST(aum.created_date,a.install_date) AS install_date
FROM dev.aum_dedup aum
  LEFT JOIN dev.adv_temp a
         ON aum.advertising_id = a.device_id
        AND aum.advertising_id IS NOT NULL
  LEFT JOIN dev.temp_d123 d
         ON aum.device_id = d.device_id
        AND aum.advertising_id IS NOT NULL);

SELECT e.*,
			 f.devices_active_sessions,
       c.total_installs
FROM (SELECT TO_CHAR(install_date,'YYYY-MM') AS install_month,
             TO_CHAR(TO_DATE(load_date,'YYYYMMDD'),'YYYY-MM') AS retention_month,
             COUNT(DISTINCT a.device_id) AS devices_app_installed
      FROM dev.device_install a
        JOIN (SELECT DISTINCT device_id,
                     load_date
              FROM (SELECT device_id,
                           load_date
                    FROM clickstream.daily_aggregates
                    UNION ALL
                    SELECT device_id,
                           load_date
                    FROM clickstream.daily_notif_aggregates)) b
          ON a.device_id = b.device_id
         AND TO_CHAR (install_date,'YYYYMMDD')::BIGINT<= b.load_date
         where a.install_date>='2016-04-01 00:00:00'
      GROUP BY 1,
               2) e
  LEFT JOIN (SELECT TO_CHAR(install_date,'YYYY-MM') AS install_month,
                    COUNT(DISTINCT device_id) AS total_installs
             FROM dev.device_install
             GROUP BY 1) c ON e.install_month = c.install_month
  LEFT JOIN (SELECT TO_CHAR(install_date,'YYYY-MM') AS install_month,
                    TO_CHAR(TO_DATE(load_date,'YYYYMMDD'),'YYYY-MM') AS retention_month,
                    COUNT(DISTINCT a.device_id) AS devices_active_sessions
             FROM dev.device_install a
               JOIN (SELECT device_id, load_date FROM clickstream.daily_aggregates) b
                 ON a.device_id = b.device_id
                AND TO_CHAR (install_date,'YYYYMMDD')::BIGINT<= b.load_date
             where a.install_date>='2016-04-01 00:00:00'
             GROUP BY 1,
                      2) f on e.install_month=f.install_month and e.retention_month=f.retention_month;

