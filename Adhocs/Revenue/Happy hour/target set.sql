create table customer_insights.target_list_HH_20170208 distkey(uidx) as 
(SELECT cl.*,
       nvl(ev.visit_flag,0) as visit_flag,
       nvl(ev.pdp_flag,0) as pdp_flag,
       nvl(ev.list_page_flag,0) as list_page_flag,
       nvl(ev.atc_flag,0) as atc_flag,
       nvl(ev.atwl_flag,0) as atwl_flag,
       nvl(ev.went_to_cart_flag,0) as went_to_cart_flag,
       nvl(ev.went_to_payment_flag,0) as went_to_payment_flag,
       nvl(fol.bought_flag,0) as bought_flag
FROM dev.cus_list_slots cl
  LEFT JOIN (SELECT uidx,
  									MAX(CASE WHEN event_type IN ('ScreenLoad','appLaunch') THEN 1 ELSE 0 END) AS visit_flag,
  									max(case when event_type = 'ScreenLoad' AND screen_name ilike '%Shopping Page-PDP%' then 1 else 0 end) as pdp_flag,
  									max(case when event_type = 'ScreenLoad' AND (screen_name ilike '%happy-hour3%' or screen_name ilike '%happy-hour4%' or screen_name ilike '%happy-hour5%' 
  									or screen_name ilike '%happy-hour03%' or screen_name ilike '%happy-hour04%' or screen_name ilike '%happy-hour05%') then 1 else 0 end) as list_page_flag,
                    MAX(CASE WHEN event_type = 'addToCart' THEN 1 ELSE 0 END) AS atc_flag,
                    MAX(CASE WHEN event_type = 'addToList' THEN 1 ELSE 0 END) AS atwl_flag,
                    MAX(CASE WHEN event_type = 'ScreenLoad' AND screen_name = 'Checkout-Cart' THEN 1 ELSE 0 END) AS went_to_cart_flag,
                    MAX(CASE WHEN event_type = 'ScreenLoad' AND screen_name = 'Checkout-payment' THEN 1 ELSE 0 END) AS went_to_payment_flag
             FROM clickstream.events_2017_02_08
             where event_type in ('ScreenLoad','addToCart','addToList','appLaunch') and atlas_ts>1486553400000 
             GROUP BY 1) ev ON cl.uidx = ev.uidx 
    left join 
    (select distinct customer_login, 1 as bought_flag 
    FROM fact_order_live fci
WHERE (is_shipped = 1 OR is_realised = 1)
AND   order_created_date = 20170208 and order_created_time>1700 ) fol on cl.uidx=fol.customer_login);

SELECT screen_name,
       COUNT(*)
FROM clickstream.events_2017_02_08
WHERE 
GROUP BY 1;


select order_created_time/100 as hour, count(distinct customer_login)
    FROM fact_order_live fci
WHERE (is_shipped = 1 OR is_realised = 1)
AND   order_created_date = 20170208
group by 1;

SELECT max(atlas_ts)
FROM clickstream.events_2017_02_08;

select * from customer_insights.target_list_HH_20170208 limit 5000;

SELECT slot,
       visit_flag,
       pdp_flag,
       list_page_flag,
       atc_flag,
       atwl_flag,
       went_to_cart_flag,
       went_to_payment_flag,
       bought_flag,
       COUNT(DISTINCT uidx)
FROM customer_insights.target_list_HH_20170208
GROUP BY 1,
         2,
         3,
         4,
         5,
         6,
         7,
         8,
				 9


