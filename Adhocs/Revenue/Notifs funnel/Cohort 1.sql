select count(distinct d1.total_users_l) as total_users,
count(case when d1.received_l is not null then 1 end) received_c0,
count(case when d1.received_l is not null and d1.received_t is not null then 1 end) received_c1,
count(case when d1.received_l is not null and d1.received_t is not null and d2.received_t is not null then 1 end) received_c2,
count(case when d1.received_l is not null and d1.received_t is not null and d2.received_t is not null and d3.received_t is not null then 1 end) received_c3,
count(case when d1.opened_l is not null then 1 end) opened_c0,
count(case when d1.opened_l is not null and d1.opened_t is not null then 1 end) opened_c1,
count(case when d1.opened_l is not null and d1.opened_t is not null and d2.opened_t is not null then 1 end) opened_c2,
count(case when d1.opened_l is not null and d1.opened_t is not null and d2.opened_t is not null and d3.opened_t is not null then 1 end) opened_c3,
count(case when d1.not_opened_launched_l is not null then 1 end) not_opened_launched_c0,
count(case when d1.not_opened_launched_l is not null and d1.not_opened_launched_t is not null then 1 end) not_opened_launched_c1,
count(case when d1.not_opened_launched_l is not null and d1.not_opened_launched_t is not null and d2.not_opened_launched_t is not null then 1 end) not_opened_launched_c2,
count(case when d1.not_opened_launched_l is not null and d1.not_opened_launched_t is not null and d2.not_opened_launched_t is not null and d3.not_opened_launched_t is not null then 1 end) not_opened_launched_c3,
count(case when d1.not_opened_not_launched_l is not null then 1 end) not_opened_not_launched_c0,
count(case when d1.not_opened_not_launched_l is not null and d1.not_opened_not_launched_t is not null then 1 end) not_opened_not_launched_c1,
count(case when d1.not_opened_not_launched_l is not null and d1.not_opened_not_launched_t is not null and d2.not_opened_not_launched_t is not null then 1 end) not_opened_not_launched_c2,
count(case when d1.not_opened_not_launched_l is not null and d1.not_opened_not_launched_t is not null and d2.not_opened_not_launched_t is not null and d3.not_opened_not_launched_t is not null then 1 end) not_opened_not_launched_c3
from dev.MIGRATION_1 d1
left join dev.MIGRATION_2 d2
on d1.total_users_l=d2.total_users_l
left join dev.MIGRATION_3 d3
on d1.total_users_l=d3.total_users_l
 limit 100;

select count(distinct total_users_l) from dev.MIGRATION_3;
