select count(distinct d1.total_users_l) as total_users,
count(case when d1.received_l is not null then 1 end) received_c0,
count(case when d1.received_l is not null and d1.received_t is not null then 1 end) received_c1,
count(case when d1.opened_l is not null then 1 end) opened_c0,
count(case when d1.opened_l is not null and d1.opened_t is not null then 1 end) opened_c1,
count(case when d1.not_opened_launched_l is not null then 1 end) not_opened_launched_c0,
count(case when d1.not_opened_launched_l is not null and d1.not_opened_launched_t is not null then 1 end) not_opened_launched_c1,
count(case when d1.not_opened_not_launched_l is not null then 1 end) not_opened_not_launched_c0,
count(case when d1.not_opened_not_launched_l is not null and d1.not_opened_not_launched_t is not null then 1 end) not_opened_not_launched_c1
from dev.MIGRATION_6 d1
 limit 100
