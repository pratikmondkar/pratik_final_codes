SELECT a.*,
       b.week_start_date,
       b.week_end_date
FROM (SELECT iso_week_of_the_year,
             business_unit,
             dci.gender,
             SUM(item_revenue_inc_cashback) AS ovr_rev,
             sum(case when order_sequence = 1 then item_revenue_inc_cashback else 0 end) as acq_rev
      FROM fact_core_item fci
        JOIN dim_date dd ON fci.order_created_date = dd.full_date
        LEFT JOIN dim_product dp ON fci.sku_id = dp.sku_id
        LEFT JOIN dim_customer_idea dci ON fci.idcustomer = dci.id
      WHERE store_id = 1
      AND   (is_shipped = 1 OR is_realised = 1)
      AND   order_created_date >= 20160101
      GROUP BY 1,
               2,
               3
      ORDER BY 4 DESC) a
  LEFT JOIN (SELECT iso_week_of_the_year,
                    MIN(full_date) AS week_start_date,
                    MAX(full_date) AS week_end_date
             FROM dim_date dd
             WHERE full_date BETWEEN 20160101 AND 20161118
             GROUP BY 1) b ON a.iso_week_of_the_year = b.iso_week_of_the_year

