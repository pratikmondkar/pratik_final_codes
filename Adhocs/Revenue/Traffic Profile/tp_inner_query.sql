
---------------------------------------------------------------------------------------
-- Lifetime organic affinity:
-- To fetch user level organic affinity towards article type:

DROP TABLE if exists customer_insights.sk_traffic_dashboard_lifetime_user_level_at_affinity_date_level;

CREATE TABLE customer_insights.sk_traffic_dashboard_lifetime_user_level_at_affinity_date_level AS
select b.* 
from
(SELECT a.*,
                   row_number() OVER (PARTITION BY a.user_id ORDER BY score DESC) AS RANK
            FROM (SELECT distinct user_id, 
                         identifier,
                         article_type,
                         SUM(pdp_view + 5*liked + 10*added_to_collection +7*addToList + 5*addToCart) AS score
                  FROM bidb.notif_person_base_lifetime
                  GROUP BY 1,
                           2,
                           3
                            ) a
                            ) b
      WHERE RANK = 1
      ;


grant select on customer_insights.sk_traffic_dashboard_lifetime_user_level_at_affinity_date_level to analysis_user;

---------------------------------------------------------------------------------------
-- Lifetime organic affinity:
-- To fetch user level organic affinity towards brand:

DROP TABLE if exists customer_insights.sk_traffic_dashboard_lifetime_user_level_brand_affinity_date_level;

CREATE TABLE customer_insights.sk_traffic_dashboard_lifetime_user_level_brand_affinity_date_level AS
select b.* 
from
(SELECT a.*,
                   row_number() OVER (PARTITION BY a.user_id ORDER BY score DESC) AS RANK
            FROM (SELECT distinct user_id, 
                         identifier,
                         brand,
                         SUM(pdp_view + 5*liked + 10*added_to_collection +7*addToList + 5*addToCart) AS score
                  FROM bidb.notif_person_base_lifetime
                  GROUP BY 1,
                           2,
                           3
                            ) a
                            ) b
      WHERE RANK = 1
      ;


grant select on customer_insights.sk_traffic_dashboard_lifetime_user_level_brand_affinity_date_level to analysis_user;



---------------------------------------------------------------------------------------
-- Install Cohort
-- query 1:
-- user+device level data:
-- removing data where device_id id 'NA': 

drop table if exists customer_insights.sk_traffic_user_device_date_level;

create table customer_insights.sk_traffic_user_device_date_level as 
select user_id, device_id, min(created_date) as install_date 
from app_users_magasin 
where device_id != 'NA'
group by 1,2
;

-- Install Cohort
-- query 2:

drop table if exists customer_insights.sk_traffic_user_install_date_date_level;

create table customer_insights.sk_traffic_user_install_date_date_level as 
select distinct user_id, identifier, install_date
from 
(select q.*,
rank() over(partition by (user_id || identifier) order by install_date asc) as rank_user_id
from
(
select p.*,
(case when uidx is not null then uidx else device_id end) as user_id, 
(case when uidx is not null then 'uidx' else 'device_id' end) as identifier 
from
(select u.*, v.customer_login as uidx
from
(select distinct ins.user_id as user_id_mix, ins.device_id, ins.install_date, nvl(dce.uid, dci.id) as customer_id
from customer_insights.sk_traffic_user_device_date_level ins
left join cii.dim_customer_email dce on ins.user_id = dce.email
left join bidb.dim_customer_idea dci on ins.user_id = dci.customer_login
) u
left join bidb.dim_customer_idea v 
on u.customer_id = v.id
) p
) q
) t
where  rank_user_id = 1;


grant select on customer_insights.sk_traffic_user_install_date_date_level to analysis_user;


---------------------------------------------------------------------------------------
--Purchase Cohort + Recency Cohort:
-- fetch min order_created_date at customer level:


drop table if exists customer_insights.customer_min_order_created_date_date_level;


create table customer_insights.customer_min_order_created_date_date_level as
select p.*, q.customer_login as uidx
from
(
select distinct idcustomer, 
min(case when order_created_date <= (TO_CHAR(convert_timezone('Asia/Calcutta',(to_date(replace_date,'YYYYMMDD')-INTERVAL '1 DAYS')),'YYYYMMDD')::BIGINT) then order_created_date end) as min_order_created_date,
max(case when order_created_date <= (TO_CHAR(convert_timezone('Asia/Calcutta',(to_date(replace_date,'YYYYMMDD')-INTERVAL '2 DAYS')),'YYYYMMDD')::BIGINT) then order_created_date end) as max_order_created_date
from 
(select distinct idcustomer_idea as idcustomer, order_created_date 
from bidb.fact_order
where order_created_date != '19700101'
and order_created_date is not null
and order_created_date <= (TO_CHAR(convert_timezone('Asia/Calcutta',(to_date(replace_date,'YYYYMMDD')-INTERVAL '1 DAYS')),'YYYYMMDD')::BIGINT)
AND (is_shipped = 1 OR is_realised = 1)
AND store_id = 1
) u
group by 1
) p
left join bidb.dim_customer_idea q
on p.idcustomer = q.id
;


grant select on customer_insights.customer_min_order_created_date_date_level to analysis_user;


---------------------------------------------------------------------------------------

-- customer daily orders for conversion: (orders/sessions): 

drop table if exists customer_insights.customer_daily_order_date_level;

create table customer_insights.customer_daily_order_date_level as
select p.*, q.customer_login as uidx
from
(select distinct order_created_date, idcustomer, 
count(distinct order_group_id) as orders, 
SUM(nvl (item_revenue_inc_cashback,0) + nvl (shipping_charges,0) + nvl (gift_charges,0)) AS revenue
from bidb.fact_core_item
where ((order_created_date = (TO_CHAR(convert_timezone ('Asia/Calcutta',to_date(replace_date,'YYYYMMDD')),'YYYYMMDD')::BIGINT))
               or (order_created_date = (TO_CHAR(convert_timezone('Asia/Calcutta',(to_date(replace_date,'YYYYMMDD')-INTERVAL '1 DAYS')),'YYYYMMDD')::BIGINT)))       
AND (is_shipped = 1 OR is_realised = 1)
AND store_id = 1
group by 1,2
) p 
left join bidb.dim_customer_idea q
on p.idcustomer = q.id
;


---------------------------------------------------------------------------------------

-- Microsegmentation metrics:

drop table if exists customer_insights.sk_traffic_ms_overall_user_level_date_level;

create table customer_insights.sk_traffic_ms_overall_user_level_date_level as 
select u.*
from (
select distinct month, uidx, eng, perf, di, sa, pi, 
rank() over(partition by uidx order by month desc) as rank_cust_month 
from bidb.fact_microsegmentation a
) u
where rank_cust_month = 1
;


-- query 1: Need to run every month:

--drop table if exists customer_insights.sk_traffic_ms_overall;

--create table customer_insights.sk_traffic_ms_overall as 
--(select '201612' as month, id, eng ,perf, di, sa, pi  from dev.dec_ref_base)
--union all 
--(select '201611' as month, id, eng ,perf, di, sa, pi  from dev.ms_master_full_nov)
--union all 
--(select '201610' as month, id, eng ,perf, di, sa, pi from dev.ms_master_full_oct) 
--union all
--(select '201609' as month, id, eng ,perf, di, sa, pi from dev.ms_master_full_sep)
--union all
--(select '201608' as month, id, eng ,perf, di, sa, pi from dev.ms_master_full_aug)
--union all
--(select '201607' as month, id, eng ,perf, di, sa, pi from dev.ms_master_full_july)
;


grant select on customer_insights.sk_traffic_ms_overall_user_level_date_level to analysis_user;

----------------------------------------------------------------



