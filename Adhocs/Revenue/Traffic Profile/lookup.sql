--------------------------------------------------------------------------------------
-- Lifetime organic affinity:
-- To fetch user level organic affinity towards article type:

DROP TABLE if exists customer_insights.sk_traffic_dashboard_lifetime_user_level_at_affinity;

CREATE TABLE customer_insights.sk_traffic_dashboard_lifetime_user_level_at_affinity DISTKEY (user_id) AS
(select b.*
from
(SELECT a.*,       
                   row_number() OVER (PARTITION BY a.user_id ORDER BY score DESC) AS RANK
            FROM (SELECT distinct user_id,
                         identifier,
                         article_type,
                         SUM(pdp_view + 5*liked + 10*added_to_collection +7*addToList + 5*addToCart) AS score
                  FROM bidb.notif_person_base_lifetime
                  GROUP BY 1,
                           2,
                           3
                            ) a
                            ) b
      WHERE RANK = 1)
;


grant select on customer_insights.sk_traffic_dashboard_lifetime_user_level_at_affinity to analysis_user;

---------------------------------------------------------------------------------------
-- Lifetime organic affinity:
-- To fetch user level organic affinity towards brand:

DROP TABLE if exists customer_insights.sk_traffic_dashboard_lifetime_user_level_brand_affinity;

CREATE TABLE customer_insights.sk_traffic_dashboard_lifetime_user_level_brand_affinity  DISTKEY (user_id) AS
(select b.*
from
(SELECT a.*,       
                   row_number() OVER (PARTITION BY a.user_id ORDER BY score DESC) AS RANK
            FROM (SELECT distinct user_id,
                         identifier,
                         brand,
                         SUM(pdp_view + 5*liked + 10*added_to_collection +7*addToList + 5*addToCart) AS score
                  FROM bidb.notif_person_base_lifetime
                  GROUP BY 1,
                           2,
                           3
                            ) a
                            ) b
      WHERE RANK = 1)
;

grant select on customer_insights.sk_traffic_dashboard_lifetime_user_level_brand_affinity to analysis_user;



---------------------------------------------------------------------------------------
-- Install Cohort
-- query 1:
-- user+device level data:
-- removing data where device_id id 'NA': 

drop table if exists customer_insights.sk_traffic_user_device;

create table customer_insights.sk_traffic_user_device DISTKEY (device_id) as
(select  device_id, min(created_date) as install_date
from app_users_magasin
where device_id != 'NA'
group by 1)
;

---------------------------------------------------------------------------------------
--Purchase Cohort:
-- fetch min order_created_date at customer level:

drop table if exists customer_insights.customer_min_order_created_date;

create table customer_insights.customer_min_order_created_date DISTKEY (uidx) as
(select p.*, q.customer_login as uidx
from
(select distinct idcustomer_idea,
min(order_created_date) as min_order_created_date
from bidb.fact_order
where order_created_date != '19700101'
and order_created_date is not null
AND (is_shipped = 1 OR is_realised = 1)
AND store_id = 1
group by 1
) p
left join bidb.dim_customer_idea q
on p.idcustomer_idea = q.id)
;


grant select on customer_insights.customer_min_order_created_date to analysis_user;

---------------------------------------------------------------------------------------
-- Microsegmentation metrics:
-- query 1: Need to run every month:

--drop table if exists customer_insights.sk_traffic_ms_overall;

--create table customer_insights.sk_traffic_ms_overall as 
--(select '201612' as month, id, eng ,perf, di, sa, pi  from dev.dec_ref_base)
--union all 
--(select '201611' as month, id, eng ,perf, di, sa, pi  from dev.ms_master_full_nov)
--union all 
--(select '201610' as month, id, eng ,perf, di, sa, pi from dev.ms_master_full_oct) 
--union all
--(select '201609' as month, id, eng ,perf, di, sa, pi from dev.ms_master_full_sep)
--union all
--(select '201608' as month, id, eng ,perf, di, sa, pi from dev.ms_master_full_aug)
--union all
--(select '201607' as month, id, eng ,perf, di, sa, pi from dev.ms_master_full_july)
;

-- query 2:
--fetching latest flags for customers: 

  drop table if exists customer_insights.sk_traffic_ms_overall_user_level;

create table customer_insights.sk_traffic_ms_overall_user_level DISTKEY (uidx) as
(select p.*, q.customer_login as uidx
from
(select u.*
from
(select a.*,
rank() over(partition by id order by month desc) as rank_cust_month
from dev.sk_traffic_ms_overall a
) u
where rank_cust_month = 1
) p
inner join bidb.dim_customer_idea q on p.id = q.id)
;


grant select on customer_insights.sk_traffic_ms_overall_user_level to analysis_user;

----------------------------------------------------------------
