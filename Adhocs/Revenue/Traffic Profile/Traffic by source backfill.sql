drop table if exists dev.pm_traffic_source_temp1_bkfill;
create table dev.pm_traffic_source_temp1_bkfill distkey(session_id) as
(SELECT session_id,
       CASE
         WHEN app_name IN ('MyntraRetailWeb') AND device_category ilike '%mobile%' THEN 'MWeb'
         WHEN app_name IN ('MyntraRetailWeb') AND device_category ilike '%desktop%' THEN 'Web'
         ELSE os
       END AS os,
       MIN(load_date) AS DATE,
       count(case when event_type in ('ScreenLoad') and (screen_name LIKE 'Shopping Page-List Page%' OR screen_name LIKE 'Shopping Page-Search%') then 1 end) as list,
       COUNT(CASE WHEN event_type in ('ScreenLoad') and screen_name LIKE 'Shopping Page-PDP%' THEN 1 END) AS pdp,
       COUNT(CASE WHEN event_type IN ('addToCart','moveToCart') THEN 1 END) AS atc
FROM clickstream.events_view
WHERE event_type IN ('appLaunch','ScreenLoad','streamCardLoad','addToCart','moveToCart')
AND   load_date BETWEEN 20170920 AND 20170930
GROUP BY 1,2);

drop table if exists dev.pm_traffic_source_temp_bkfill;

create table dev.pm_traffic_source_temp_bkfill distkey(session_id) sortkey(event_type) as
(select ev.session_id,
       event_type,
       utm_medium,
       utm_campaign,
       utm_source,
       client_ts,
       load_date,
       uidx,
       data_set_value,
       screen_name       
from clickstream.events_view ev
join dev.pm_traffic_source_temp1_bkfill t on ev.session_id=t.session_id and ev.event_type in ('appLaunch','push-notification','DeepLink','addToCart','moveToCart','ScreenLoad')
where load_date BETWEEN 20170920 AND 20170925
 );


drop table if exists dev.pm_traffic_source_temp2;
create table  dev.pm_traffic_source_temp2 distkey(session_id) as 
(SELECT session_id,
       utm_medium,
       utm_campaign,
       utm_source
FROM (SELECT session_id,
             utm_medium,
             utm_campaign,
             utm_source,
             ROW_NUMBER() OVER (PARTITION BY session_id ORDER BY client_ts ASC) AS rnk
      FROM dev.pm_traffic_source_temp_bkfill
      WHERE event_type IN ('appLaunch','push-notification','DeepLink') and utm_medium is not null
      )
WHERE rnk = 1);

drop table if exists dev.pm_traffic_source_temp3;
create table dev.pm_traffic_source_temp3 distkey(session_id) as
(SELECT session_id,
       SUM(quantity) AS atc_qty,
       SUM(item_revenue_inc_cashback + nvl (shipping_charges,0) + nvl (gift_charges,0)) AS atc_revenue
FROM (SELECT distinct a.session_id,
										b.date,
                   a.uidx,
                   a.data_set_value
            FROM dev.pm_traffic_source_temp_bkfill a
            left join dev.pm_traffic_source_temp1_bkfill b on a.session_id=b.session_id
            WHERE event_type IN ('addToCart','moveToCart')
            ) a
  JOIN bidb.dim_customer_idea dci ON dci.customer_login = a.uidx
  JOIN bidb.fact_core_item fci
    ON fci.idcustomer = dci.id
   AND a.data_set_value = fci.style_id
   AND order_created_date between 20170920 AND 20170930
   and order_created_date between a.date and a.date+5
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
GROUP BY 1);

drop table if exists dev.pm_traffic_source_temp4;
create table dev.pm_traffic_source_temp4 distkey(session_id) as
(SELECT session_id,
       SUM(quantity) AS ses_qty,
       SUM(item_revenue_inc_cashback + nvl (shipping_charges,0) + nvl (gift_charges,0)) AS ses_revenue
FROM (SELECT DISTINCT session_id,
             REPLACE(data_set_value,'-','') AS store_order_id
      FROM dev.pm_traffic_source_temp_bkfill
      WHERE event_type IN ('ScreenLoad') 
      AND   screen_name = 'Checkout-confirmation') a
  JOIN bidb.fact_core_item fci ON fci.store_order_id = a.store_order_id
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date between 20170920 AND 20170930  
GROUP BY 1);


DELETE FROM customer_insights.traffic_funnel
WHERE DATE between 20170920 and 20170926;

insert into customer_insights.traffic_funnel
(SELECT a.date,week_of_the_year as week,UPPER(dd.month_string) as month,cal_year as year,
medium,campaign,total_sessions,list_views,pdp_views,add_to_carts,atc_revenue,atc_qty_sold,session_revenue,session_qty_sold,os,source
FROM (SELECT c.date AS DATE,
             os,
             nvl(utm_source,'Organic') AS source,
             nvl(utm_medium,'Organic') AS medium,
             CASE
               WHEN utm_campaign IS NULL AND utm_medium IS NULL THEN 'Organic'
               ELSE utm_campaign
             END AS campaign,
             COUNT(DISTINCT c.session_id) AS total_sessions,
             SUM(list) AS list_views,
             SUM(pdp) AS pdp_views,
             SUM(atc) AS add_to_carts,
             SUM(atc_revenue) AS atc_revenue,
             SUM(atc_qty) AS atc_qty_sold,
             SUM(ses_revenue) AS session_revenue,
             SUM(ses_qty) AS session_qty_sold
             FROM dev.pm_traffic_source_temp1_bkfill c
        LEFT JOIN dev.pm_traffic_source_temp2 d ON c.session_id = d.session_id
        LEFT JOIN dev.pm_traffic_source_temp3 e ON e.session_id = c.session_id
        LEFT JOIN dev.pm_traffic_source_temp4 g ON g.session_id = c.session_id
      GROUP BY 1,2,3,4,5) a
  LEFT JOIN bidb.dim_date dd ON a.date = dd.full_date
  where a.DATE between 20170920 and 20170926)
  ;

END;
