

delete from customer_insights.traffic_dashboard_hourly_profile_date_level
where ((load_Date = (TO_CHAR(convert_timezone ('Asia/Calcutta',to_date(replace_date,'YYYYMMDD')),'YYYYMMDD')::BIGINT))
or (load_Date = (TO_CHAR(convert_timezone('Asia/Calcutta',(to_date(replace_date,'YYYYMMDD')-INTERVAL '1 DAYS')),'YYYYMMDD')::BIGINT)))
;


insert into customer_insights.traffic_dashboard_hourly_profile_date_level
select v.*, 
case when identifier in ('Logged-In-App', 'Logged-In-Web') then nvl(uidx_sessions,0)
     when identifier in ('Non-Logged-In-App', 'Non-Logged-In-Web') then nvl(device_sessions,0)
     end as total_sessions
from
(select distinct o.*, 
case when identifier in ('Logged-In-App','Non-Logged-In-App') and install_month_diff = 0 and install_day_diff = 0 then 'M0 - Same Day' 
     when identifier in ('Logged-In-App','Non-Logged-In-App') and install_month_diff = 0 and install_day_diff != 0 then 'M0 - Not Same Day' 
     when identifier in ('Logged-In-App','Non-Logged-In-App') and install_month_diff = 1 then 'M1' 
     when identifier in ('Logged-In-App','Non-Logged-In-App') and install_month_diff = 2 then 'M2' 
     when identifier in ('Logged-In-App','Non-Logged-In-App') and install_month_diff = 3 then 'M3' 
     when identifier in ('Logged-In-App','Non-Logged-In-App') and install_month_diff between 4 and 6 then 'M4-6' 
     when identifier in ('Logged-In-App','Non-Logged-In-App') and install_month_diff between 7 and 12 then 'M7-12' 
     when identifier in ('Logged-In-App','Non-Logged-In-App') and install_month_diff > 12 then 'M-12+' 
     when identifier in ('Logged-In-Web','Non-Logged-In-Web') then 'Web-Users' 
     END AS install_cohort,      
case when first_purchase_month_diff = 0 and first_purchase_day_diff = 0 then 'M0 - Same Day'
     when first_purchase_month_diff = 0 and first_purchase_day_diff != 0 then 'M0 - Not Same Day'
     when first_purchase_month_diff = 1 then 'M1' 
     when first_purchase_month_diff = 2 then 'M2' 
     when first_purchase_month_diff = 3 then 'M3' 
     when first_purchase_month_diff between 4 and 6 then 'M4-6' 
     when first_purchase_month_diff between 7 and 12 then 'M7-12' 
     when first_purchase_month_diff > 12 then 'M-12+' 
     when first_purchase_month_diff is null and identifier in ('Logged-In-App', 'Logged-In-Web') then 'RNB'  
     when first_purchase_month_diff is null and identifier in ('Non-Logged-In-App', 'Non-Logged-In-Web') then 'Non-Logged-In' END AS acquisition_cohort, 
(case when dci.gender is not null then dci.gender else 'unk' end) as gender, 
case when last_purchase_month_diff = 0 and last_purchase_day_diff != 0 then 'M0'
     when last_purchase_month_diff = 1 then 'M1' 
     when last_purchase_month_diff = 2 then 'M2' 
     when last_purchase_month_diff = 3 then 'M3' 
     when last_purchase_month_diff between 4 and 6 then 'M4-6' 
     when last_purchase_month_diff between 7 and 12 then 'M7-12' 
     when last_purchase_month_diff > 12 then 'M-12+' 
     when last_purchase_month_diff is null and identifier in ('Logged-In-App', 'Logged-In-Web') then 'RNB'     
     when last_purchase_month_diff is null and identifier in ('Non-Logged-In-App', 'Non-Logged-In-Web') then 'Non-Logged-In' END AS recency_cohort, 
     orders, 
     revenue     
--     o.sessions
from 
(
select a.*, 
date_diff('Day',date(install_date), date(load_date)) as  install_day_diff,
date_diff('Month',date(install_date), date(load_date)) as  install_month_diff,
date_diff('Day',date(first_purchase_date), date(load_date)) as  first_purchase_day_diff,
date_diff('Month',date(first_purchase_date), date(load_date)) as  first_purchase_month_diff,
date_diff('Day',date(last_purchase_date), date(load_date)) as  last_purchase_day_diff,
date_diff('Month',date(last_purchase_date), date(load_date)) as  last_purchase_month_diff
--a.sessions
from 
(
select p.*, 
(case when p.identifier in ('Logged-In-App','Logged-In-Web') and q.eng is not null then q.eng 
      when p.identifier in ('Logged-In-App','Logged-In-Web') and q.eng is null then 'New-Users'      
      when p.identifier in ('Non-Logged-In-App','Non-Logged-In-Web') and q.eng is null then 'Non-Logged-In'
      end) as engagement,
(case when p.identifier in ('Logged-In-App','Logged-In-Web')  and q.perf is not null then q.perf 
      when p.identifier in ('Logged-In-App','Logged-In-Web')  and q.perf is null then 'New-Users'
      when p.identifier in ('Non-Logged-In-App','Non-Logged-In-Web') and q.perf is null then 'Non-Logged-In'
      end) as performance,
(case when p.identifier in ('Logged-In-App','Logged-In-Web')  and q.di is not null then q.di 
      when p.identifier in ('Logged-In-App','Logged-In-Web')  and q.di is null then 'New-Users'
      when p.identifier in ('Non-Logged-In-App','Non-Logged-In-Web') and q.di is null then 'Non-Logged-In'
      end) as discount,
(case when p.identifier in ('Logged-In-App','Logged-In-Web')  and q.sa is not null then q.sa 
      when p.identifier in ('Logged-In-App','Logged-In-Web')  and q.sa is null then 'New-Users'
      when p.identifier in ('Non-Logged-In-App','Non-Logged-In-Web') and q.sa is null then 'Non-Logged-In'
      end) as satisfaction,
(case when p.identifier in ('Logged-In-App','Logged-In-Web')  and q.pi is not null then q.pi 
      when p.identifier in ('Logged-In-App','Logged-In-Web')  and q.pi is null then 'New-Users'
      when p.identifier in ('Non-Logged-In-App','Non-Logged-In-Web') and q.pi is null then 'Non-Logged-In'
      end) as purchase_intent,
r.install_date, 
s.min_order_created_date as first_purchase_date, 
s.max_order_created_date as  last_purchase_date
--p.sessions 
from  
(
select a.*, 
(case when b.article_type is not null then b.article_type else 'Unknown' end) as article_type_affinity, 
(case when x.brand is not null then x.brand else 'Unknown' end) as brand_affinity
      FROM (SELECT distinct load_date,
--                   EXTRACT(HOUR FROM convert_timezone ('Asia/Calcutta',(TIMESTAMP 'epoch' + atlas_ts::FLOAT/ 1000*INTERVAL '1 Second '))) AS HOUR,
                   (case when os is not null and uidx is not null then uidx 
                         when os is not null and uidx is null then device_id 
                         when os is null and app_name='MyntraRetailWeb' and uidx is not null then uidx
                         when os is null and app_name='MyntraRetailWeb' and uidx is null then device_id end) as user_id,
                   (case when os is not null and uidx is not null then 'Logged-In-App'
                         when os is not null and uidx is null then 'Non-Logged-In-App'
                         when os is null and app_name='MyntraRetailWeb' and uidx is not null then 'Logged-In-Web'
                         when os is null and app_name='MyntraRetailWeb' and uidx is null then 'Non-Logged-In-Web' end) as identifier,
                   (case when os is not null then os else 'Web' end) as os, 
                   count(distinct case when uidx is not null then session_id end ) as uidx_sessions, 
                   count(distinct case when uidx is null then session_id end ) as device_sessions 
                   FROM clickstream.events_view
            WHERE event_type in ('ScreenLoad','appLaunch','streamCardLoad')  
            and 
            --(load_Date = (TO_CHAR(convert_timezone ('Asia/Calcutta',to_date(replace_date,'YYYYMMDD')),'YYYYMMDD')::BIGINT))
                (load_Date = (TO_CHAR(convert_timezone('Asia/Calcutta',(to_date(replace_date,'YYYYMMDD')-INTERVAL '1 DAYS')),'YYYYMMDD')::BIGINT))     
--            AND   app_version IN ('3.3.0', '3.3.1', '3.3.2', '3.3.3', '3.4.0', '3.4.1', '3.4.2', '3.5.0', '3.5.1', '3.5.2', '3.6.0', '3.6.1','3.6.2', '3.7.0','3.7.1','3.7.2','3.8.0','3.8.1','3.8.2','3.8.3','3.8.4', '3.9.0','3.9.5','3.10.0','3.10.1')
            --(SELECT app_version FROM clickstream.dim_app_version WHERE app_version >= '3.3.0')
            group by 1,2,3,4) a
            left join customer_insights.sk_traffic_dashboard_lifetime_user_level_at_affinity_date_level b on a.user_id = b.user_id 
            left join customer_insights.sk_traffic_dashboard_lifetime_user_level_brand_affinity_date_level x on a.user_id = x.user_id 
      ) p         
left join customer_insights.sk_traffic_ms_overall_user_level q on p.user_id = q.uidx 
left join customer_insights.sk_traffic_user_install_date_date_level r on p.user_id = r.user_id    
LEFT JOIN customer_insights.customer_min_order_created_date_date_level s ON p.user_id = s.uidx
) a
) o
left join bidb.dim_customer_idea dci on o.user_id = dci.customer_login
left join customer_insights.customer_daily_order_date_level cdo on o.user_id = uidx  and o.load_date = cdo.order_created_date
) v
where identifier is not null;


grant select on customer_insights.traffic_dashboard_hourly_profile_date_level to myntra_reporting;    

grant select on customer_insights.traffic_dashboard_hourly_profile_date_level to adhocpanel;

grant select on customer_insights.traffic_dashboard_hourly_profile_date_level to adhoc_fin_user;

grant select on customer_insights.traffic_dashboard_hourly_profile_date_level to analysis_user;

--------------------------------------------------------------------------------------------------------------------

--Transactions + Conversion :

delete from customer_insights.traffic_dashboard_hourly_profile_with_conversion_date_level
where ((load_Date = (TO_CHAR(convert_timezone ('Asia/Calcutta',to_date(replace_date,'YYYYMMDD')),'YYYYMMDD')::BIGINT))
or (load_Date = (TO_CHAR(convert_timezone('Asia/Calcutta',(to_date(replace_date,'YYYYMMDD')-INTERVAL '1 DAYS')),'YYYYMMDD')::BIGINT)))
;

--drop table customer_insights.traffic_dashboard_hourly_profile_with_conversion_date_level;

--create table customer_insights.traffic_dashboard_hourly_profile_with_conversion_date_level as
insert into customer_insights.traffic_dashboard_hourly_profile_with_conversion_date_level
select p.*, 
(case when q.uidx is not null then 1 else 0 end) purchase_flag
from 
customer_insights.traffic_dashboard_hourly_profile_date_level p
left join 
(select distinct order_created_date, 
--left(LPAD (order_created_time, 6,0),2) as hour, 
idcustomer, 
customer_login as uidx 
from bidb.fact_core_item oi
inner join bidb.dim_customer_idea dci on oi.idcustomer = dci.id
where (is_shipped = 1 or is_realised = 1) 
and store_id = 1
and (order_created_date = (TO_CHAR(convert_timezone('Asia/Calcutta',(to_date(replace_date,'YYYYMMDD')-INTERVAL '1 DAYS')),'YYYYMMDD')::BIGINT))       
) q 
on p.load_date = q.order_created_date 
--and p.hour = q.hour
and p.user_id = q.uidx
where (load_date = (TO_CHAR(convert_timezone('Asia/Calcutta',(to_date(replace_date,'YYYYMMDD')-INTERVAL '1 DAYS')),'YYYYMMDD')::BIGINT))
;


grant select on customer_insights.traffic_dashboard_hourly_profile_with_conversion_date_level to myntra_reporting;    

grant select on customer_insights.traffic_dashboard_hourly_profile_with_conversion_date_level to adhocpanel;

grant select on customer_insights.traffic_dashboard_hourly_profile_with_conversion_date_level to adhoc_fin_user;

grant select on customer_insights.traffic_dashboard_hourly_profile_with_conversion_date_level to analysis_user;

------------------------------------------------------------------------------------------------------------------------

delete from customer_insights.traffic_dashboard_hourly_profile_aggregated_data_date_level
where ((load_Date = (TO_CHAR(convert_timezone ('Asia/Calcutta',to_date(replace_date,'YYYYMMDD')),'YYYYMMDD')::BIGINT))
or (load_Date = (TO_CHAR(convert_timezone('Asia/Calcutta',(to_date(replace_date,'YYYYMMDD')-INTERVAL '1 DAYS')),'YYYYMMDD')::BIGINT)));

--drop table customer_insights.traffic_dashboard_hourly_profile_aggregated_data_date_level ;


--create table customer_insights.traffic_dashboard_hourly_profile_aggregated_data_date_level as
insert into customer_insights.traffic_dashboard_hourly_profile_aggregated_data_date_level 
select 
load_date,
identifier,
os,
article_type_affinity,
brand_affinity,
engagement,
performance,
discount,
satisfaction,
purchase_intent,
install_cohort,
acquisition_cohort,
recency_cohort,
gender,
count(distinct user_id) as users , 
count(distinct case when purchase_flag = 1 then  user_id end) as customers, 
sum(orders) as orders,
sum(revenue) as revenue, 
sum(total_sessions) as sessions
from customer_insights.traffic_dashboard_hourly_profile_with_conversion_date_level
where (load_date = (TO_CHAR(convert_timezone('Asia/Calcutta',(to_date(replace_date,'YYYYMMDD')-INTERVAL '1 DAYS')),'YYYYMMDD')::BIGINT))  
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;


grant select on customer_insights.traffic_dashboard_hourly_profile_aggregated_data_date_level to myntra_reporting;    

grant select on customer_insights.traffic_dashboard_hourly_profile_aggregated_data_date_level to adhocpanel;

grant select on customer_insights.traffic_dashboard_hourly_profile_aggregated_data_date_level to adhoc_fin_user;

grant select on customer_insights.traffic_dashboard_hourly_profile_aggregated_data_date_level to analysis_user;



-----------------------------------------------------------------------------------










