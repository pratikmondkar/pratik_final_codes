drop table if exists dev.pm_traffic_15min1;
create table dev.pm_traffic_15min1 distkey(user_id) as
(select case when uidx is null then device_id else uidx end as user_id,load_date,
LPAD(EXTRACT(HOUR FROM convert_timezone ('Asia/Calcutta',(TIMESTAMP 'epoch' + atlas_ts::FLOAT/1000*INTERVAL '1 Second '))),2,'0') AS HOUR,
case when floor(LPAD(EXTRACT(MINUTE FROM convert_timezone ('Asia/Calcutta',(TIMESTAMP 'epoch' + atlas_ts::FLOAT/1000*INTERVAL '1 Second '))),2,'0')/15) = 0 then 1
     when floor(LPAD(EXTRACT(MINUTE FROM convert_timezone ('Asia/Calcutta',(TIMESTAMP 'epoch' + atlas_ts::FLOAT/1000*INTERVAL '1 Second '))),2,'0')/15) = 1 then 2
     when floor(LPAD(EXTRACT(MINUTE FROM convert_timezone ('Asia/Calcutta',(TIMESTAMP 'epoch' + atlas_ts::FLOAT/1000*INTERVAL '1 Second '))),2,'0')/15) = 2 then 3
     when floor(LPAD(EXTRACT(MINUTE FROM convert_timezone ('Asia/Calcutta',(TIMESTAMP 'epoch' + atlas_ts::FLOAT/1000*INTERVAL '1 Second '))),2,'0')/15) = 3 then 4 end as min_bucket,
     count(distinct session_id) as sessions
FROM  clickstream.traffic_realtime
 WHERE load_Date >= TO_CHAR(convert_timezone ('Asia/Calcutta',SYSDATE - interval '1 day'),'YYYYMMDD')::BIGINT and os in ('Android', 'iOS')
 group by 1,2,3,4);
 
drop table if exists dev.pm_traffic_15min2;
create table dev.pm_traffic_15min2 distkey(customer_login) as
(select distinct customer_login,
order_created_date, 
left(LPAD(order_created_time, 4, '0'),2) as hour,
case when floor((right(LPAD(order_created_time, 4, '0'),2))/15) = 0 then 1
     when floor((right(LPAD(order_created_time, 4, '0'),2))/15) = 1 then 2
     when floor((right(LPAD(order_created_time, 4, '0'),2))/15) = 2 then 3
     when floor((right(LPAD(order_created_time, 4, '0'),2))/15) = 3 then 4  else 5 end as min_bucket,
count(distinct order_group_id) as orders,
SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0) - nvl (tax_amount,0) + nvl(gift_charge,0) + nvl(shipping_charge,0)) AS revenue,
SUM(nvl (item_purchase_price_inc_tax,0) - nvl (vendor_funding,0)) AS cogs,
SUM(nvl(royalty_commission,0)) AS rc,
SUM(nvl(tax_amount,0)) AS taxes, 
count(distinct customer_login) as customers
from bidb.fact_order_live oi
where order_created_date >= TO_CHAR(convert_timezone ('Asia/Calcutta',SYSDATE - interval '1 day'),'YYYYMMDD')::BIGINT
AND (is_shipped = 1 OR is_realised = 1)
and device_channel='mobile-app'
and (customer_login NOT like '%myntra360%' and customer_login NOT like '%benchmarking%' and customer_login NOT like '%test%') 
group by 1,2,3,4);


drop table if exists dev.pm_traffic_15min4;
create table dev.pm_traffic_15min4 distkey(brand_affinity) as
SELECT b.order_created_date as load_date,b.hour,b.min_bucket,article_type_affinity,brand_affinity,engagement,performance,discount,satisfaction,purchase_intent,install_cohort,acquisition_cohort,gender,recency_cohort, 
--count(distinct a.user_id) as devices,
--sum(sessions) as sessions,
sum(b.orders) as orders,
sum(b.revenue) as revenue,
sum(b.cogs) as cogs,
sum(b.taxes) as taxes,
sum(b.rc) as rc,
count(distinct b.customer_login) as customers
from
dev.pm_traffic_15min2 b
left join dev.pm_traffic_profile_lookup c on b.customer_login=c.user_id 
group by 1,2,3,3,4,5,6,7,8,9,10,11,12,13,14;



drop table if exists dev.pm_traffic_15min5;
create table dev.pm_traffic_15min5 distkey(brand_affinity) as
SELECT a.load_date,a.hour,a.min_bucket,article_type_affinity,brand_affinity,engagement,performance,discount,satisfaction,purchase_intent,install_cohort,acquisition_cohort,gender,recency_cohort, 
count(distinct a.user_id) as devices, 
sum(sessions) as sessions
from
dev.pm_traffic_15min1 a
left join dev.pm_traffic_profile_lookup c on a.user_id=c.user_id 
group by 1,2,3,3,4,5,6,7,8,9,10,11,12,13,14;

drop table if exists dev.pm_traffic_15min6;
create table dev.pm_traffic_15min6 sortkey(load_date) distkey(brand_affinity) as
SELECT nvl(a.load_date,b.load_date) as load_date,
       nvl(a.hour,b.hour) as hour,
       case when nvl(a.min_bucket,b.min_bucket) = 1 then '0-14'
     when nvl(a.min_bucket,b.min_bucket) = 2 then '15-29'
     when nvl(a.min_bucket,b.min_bucket) = 3 then '30-44'
     when nvl(a.min_bucket,b.min_bucket) = 4 then '45-59' end as Minutes, 
     (nvl(a.hour,b.hour)||' H'||':'|| case when nvl(a.min_bucket,b.min_bucket) = 1 then '0-14'
     when nvl(a.min_bucket,b.min_bucket) = 2 then '15-29'
     when nvl(a.min_bucket,b.min_bucket) = 3 then '30-44'
     when nvl(a.min_bucket,b.min_bucket) = 4 then '45-59' end ||' M') as hour_min,
       nvl(a.min_bucket,b.min_bucket) as min_bucket,
       nvl(a.article_type_affinity,b.article_type_affinity) as article_type_affinity,
       nvl(a.brand_affinity,b.brand_affinity) as brand_affinity,
       nvl(a.engagement,b.engagement) as engagement,
       nvl(a.performance,b.performance) as performance,
       nvl(a.discount,b.discount) as discount,
       nvl(a.satisfaction,b.satisfaction) as satisfaction,
       nvl(a.purchase_intent,b.purchase_intent) as purchase_intent,
       nvl(a.install_cohort,b.install_cohort) as install_cohort,
       nvl(a.acquisition_cohort,b.acquisition_cohort) as acquisition_cohort,
       nvl(a.gender,b.gender) as gender,
       nvl(a.recency_cohort,b.recency_cohort) as recency_cohort,
       a.devices,
       a.sessions,
       b.orders,
       b.revenue,
       b.cogs,
       b.taxes,
       b.rc,
       b.customers
FROM dev.pm_traffic_15min5 a
full outer join dev.pm_traffic_15min4 b
on 
a.load_date=b.load_date and
a.hour=b.hour and
a.min_bucket=b.min_bucket and
a.article_type_affinity=b.article_type_affinity and
a.brand_affinity=b.brand_affinity and
a.engagement=b.engagement and
a.performance=b.performance and
a.discount=b.discount and
a.satisfaction=b.satisfaction and
a.purchase_intent=b.purchase_intent and
a.install_cohort=b.install_cohort and
a.acquisition_cohort=b.acquisition_cohort and
a.gender=b.gender and
a.recency_cohort=b.recency_cohort  ;





select sum(revenue+nvl(taxes,0)) from dev.pm_traffic_15min6 where load_date =20170623;

select * from stv_inflight;

select * from stv_recents where status='Running';


cancel 2358;

select order_created_date,order_created_time/100,sum(item_revenue_inc_cashback+nvl(shipping_charges)+nvl(gift_charges)) as revenue 
FROM fact_core_item
WHERE store_id = 1
AND   is_booked=1
AND   ((order_created_date =20170102 and order_created_time >234500) or (order_created_date =20170103 and order_created_time <001600))
group by 1,2
