drop table if exists dev.pm_traffic_source_temp1;
create table dev.pm_traffic_source_temp1 distkey(session_id) as
(SELECT session_id,
       CASE
         WHEN app_name IN ('MyntraRetailWeb') AND device_category ilike '%mobile%' THEN 'MWeb'
         WHEN app_name IN ('MyntraRetailWeb') AND device_category ilike '%desktop%' THEN 'Web'
         ELSE os
       END AS os,
       MIN(load_date) AS DATE,
       count(case when event_type in ('ScreenLoad') and (screen_name LIKE 'Shopping Page-List Page%' OR screen_name LIKE 'Shopping Page-Search%') then 1 end) as list,
       COUNT(CASE WHEN event_type in ('ScreenLoad') and screen_name LIKE 'Shopping Page-PDP%' THEN 1 END) AS pdp,
       COUNT(CASE WHEN event_type IN ('addToCart','moveToCart') THEN 1 END) AS atc
FROM clickstream.events_view
WHERE event_type IN ('appLaunch','ScreenLoad','streamCardLoad','addToCart','moveToCart')
AND   load_date BETWEEN TO_CHAR(convert_timezone('Asia/Calcutta',SYSDATE) -INTERVAL '5 days','YYYYMMDD')::BIGINT AND TO_CHAR(convert_timezone('Asia/Calcutta',SYSDATE) -INTERVAL '1 days','YYYYMMDD')::BIGINT
GROUP BY 1,2);

delete from dev.pm_traffic_source_temp where load_date <= TO_CHAR(convert_timezone('Asia/Calcutta',SYSDATE) -INTERVAL '6 days','YYYYMMDD')::BIGINT;

insert into dev.pm_traffic_source_temp 
(select ev.session_id,
       event_type,
       utm_medium,
       utm_campaign,
       utm_source,
       client_ts,
       load_date,
       uidx,
       data_set_value,
       screen_name       
from clickstream.events_2017_10_09 ev
join dev.pm_traffic_source_temp1 t on ev.session_id=t.session_id and ev.event_type in ('appLaunch','push-notification','DeepLink','addToCart','moveToCart','ScreenLoad')
 );


drop table if exists dev.pm_traffic_source_temp2;
create table  dev.pm_traffic_source_temp2 distkey(session_id) as 
(SELECT session_id,
       utm_medium,
       utm_campaign,
       utm_source
FROM (SELECT session_id,
             utm_medium,
             utm_campaign,
             utm_source,
             ROW_NUMBER() OVER (PARTITION BY session_id ORDER BY client_ts ASC) AS rnk
      FROM dev.pm_traffic_source_temp
      WHERE event_type IN ('appLaunch','push-notification','DeepLink')
      )
WHERE rnk = 1);

drop table if exists dev.pm_traffic_source_temp3;
create table dev.pm_traffic_source_temp3 distkey(session_id) as
(SELECT session_id,
       SUM(quantity) AS atc_qty,
       SUM(item_revenue_inc_cashback + nvl (shipping_charges,0) + nvl (gift_charges,0)) AS atc_revenue
FROM (SELECT distinct session_id,
                   uidx,
                   data_set_value
            FROM dev.pm_traffic_source_temp
            WHERE event_type IN ('addToCart','moveToCart')
            ) a
  JOIN bidb.dim_customer_idea dci ON dci.customer_login = a.uidx
  JOIN bidb.fact_core_item fci
    ON fci.idcustomer = dci.id
   AND a.data_set_value = fci.style_id
   AND order_created_date BETWEEN TO_CHAR (convert_timezone ('Asia/Calcutta',SYSDATE) -INTERVAL '5 days','YYYYMMDD')::BIGINT
   AND TO_CHAR (convert_timezone ('Asia/Calcutta',SYSDATE) -INTERVAL '1 days','YYYYMMDD')::BIGINT
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
GROUP BY 1);

drop table if exists dev.pm_traffic_source_temp4;
create table dev.pm_traffic_source_temp4 distkey(session_id) as
(SELECT session_id,
       SUM(quantity) AS ses_qty,
       SUM(item_revenue_inc_cashback + nvl (shipping_charges,0) + nvl (gift_charges,0)) AS ses_revenue
FROM (SELECT DISTINCT session_id,
             REPLACE(data_set_value,'-','') AS store_order_id
      FROM dev.pm_traffic_source_temp
      WHERE event_type IN ('ScreenLoad') 
      AND   screen_name = 'Checkout-confirmation') a
  JOIN bidb.fact_core_item fci ON fci.store_order_id = a.store_order_id
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date BETWEEN TO_CHAR(convert_timezone('Asia/Calcutta',SYSDATE) -INTERVAL '5 days','YYYYMMDD')::BIGINT AND TO_CHAR(convert_timezone('Asia/Calcutta',SYSDATE) -INTERVAL '1 days','YYYYMMDD')::BIGINT   
GROUP BY 1);


DELETE FROM customer_insights.traffic_funnel
WHERE DATE BETWEEN TO_CHAR(convert_timezone('Asia/Calcutta',sysdate) -INTERVAL '5 days','YYYYMMDD')::BIGINT AND TO_CHAR(convert_timezone('Asia/Calcutta',sysdate) -INTERVAL '1 days','YYYYMMDD')::BIGINT;

insert into customer_insights.traffic_funnel
(SELECT a.date,week_of_the_year as week,UPPER(dd.month_string) as month,cal_year as year,
medium,campaign,total_sessions,list_views,pdp_views,add_to_carts,atc_revenue,atc_qty_sold,session_revenue,session_qty_sold,os,source
FROM (SELECT c.date AS DATE,
             os,
             nvl(utm_source,'Organic') AS source,
             nvl(utm_medium,'Organic') AS medium,
             CASE
               WHEN utm_campaign IS NULL AND utm_medium IS NULL THEN 'Organic'
               ELSE utm_campaign
             END AS campaign,
             COUNT(DISTINCT c.session_id) AS total_sessions,
             SUM(list) AS list_views,
             SUM(pdp) AS pdp_views,
             SUM(atc) AS add_to_carts,
             SUM(atc_revenue) AS atc_revenue,
             SUM(atc_qty) AS atc_qty_sold,
             SUM(ses_revenue) AS session_revenue,
             SUM(ses_qty) AS session_qty_sold
             FROM dev.pm_traffic_source_temp1 c
        LEFT JOIN dev.pm_traffic_source_temp2 d ON c.session_id = d.session_id
        LEFT JOIN dev.pm_traffic_source_temp3 e ON e.session_id = c.session_id
        LEFT JOIN dev.pm_traffic_source_temp4 g ON g.session_id = c.session_id
      GROUP BY 1,2,3,4,5) a
  LEFT JOIN bidb.dim_date dd ON a.date = dd.full_date);
