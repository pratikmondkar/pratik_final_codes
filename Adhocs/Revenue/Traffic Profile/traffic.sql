
insert into customer_insights.traffic_dashboard_hourly_profile
SELECT load_date,hour,user_id,identifier,os,article_type_affinity,brand_affinity,engagement,performance,discount,satisfaction,purchase_intent,
install_date,0 as purchase_date,install_day_diff,install_month_diff,purchase_day_diff,purchase_month_diff,
       CASE
         WHEN install_month_diff = 0 AND install_day_diff = 0 THEN 'M0 - Same Day'
         WHEN install_month_diff = 0 AND install_day_diff != 0 THEN 'M0 - Not Same Day'
         WHEN install_month_diff = 1 THEN 'M1'
         WHEN install_month_diff = 2 THEN 'M2'
         WHEN install_month_diff = 3 THEN 'M3'
         WHEN install_month_diff BETWEEN 4 AND 6 THEN 'M4-6'
         WHEN install_month_diff BETWEEN 7 AND 12 THEN 'M7-12'
         WHEN install_month_diff > 12 THEN 'M-12+'
       END AS install_cohort,
       CASE
         WHEN purchase_month_diff = 0 AND purchase_day_diff = 0 THEN 'M0 - Same Day'
         WHEN purchase_month_diff = 0 AND purchase_day_diff != 0 THEN 'M0 - Not Same Day'
         WHEN purchase_month_diff = 1 THEN 'M1'
         WHEN purchase_month_diff = 2 THEN 'M2'
         WHEN purchase_month_diff = 3 THEN 'M3'
         WHEN purchase_month_diff BETWEEN 4 AND 6 THEN 'M4-6'
         WHEN purchase_month_diff BETWEEN 7 AND 12 THEN 'M7-12'
         WHEN purchase_month_diff > 12 THEN 'M-12+'
       END AS acquisition_cohort,
       gender
       FROM (SELECT a.*,
                    nvl(b.article_type,'Unknown') AS article_type_affinity,
                    nvl(x.brand,'Unknown') AS brand_affinity,
                    (CASE WHEN a.identifier = 'uidx' AND q.eng IS NOT NULL THEN q.eng WHEN a.identifier = 'uidx' AND q.eng IS NULL THEN 'New-Users' WHEN a.identifier = 'device_id' AND q.eng IS NULL THEN 'Non-Logged-In' END) AS engagement,
                    (CASE WHEN a.identifier = 'uidx' AND q.perf IS NOT NULL THEN q.perf WHEN a.identifier = 'uidx' AND q.perf IS NULL THEN 'New-Users' WHEN a.identifier = 'device_id' AND q.perf IS NULL THEN 'Non-Logged-In' END) AS performance,
                    (CASE WHEN a.identifier = 'uidx' AND q.di IS NOT NULL THEN q.di WHEN a.identifier = 'uidx' AND q.di IS NULL THEN 'New-Users' WHEN a.identifier = 'device_id' AND q.di IS NULL THEN 'Non-Logged-In' END) AS discount,
                    (CASE WHEN a.identifier = 'uidx' AND q.sa IS NOT NULL THEN q.sa WHEN a.identifier = 'uidx' AND q.sa IS NULL THEN 'New-Users' WHEN a.identifier = 'device_id' AND q.sa IS NULL THEN 'Non-Logged-In' END) AS satisfaction,
                    (CASE WHEN a.identifier = 'uidx' AND q.pi IS NOT NULL THEN q.pi WHEN a.identifier = 'uidx' AND q.pi IS NULL THEN 'New-Users' WHEN a.identifier = 'device_id' AND q.pi IS NULL THEN 'Non-Logged-In' END) AS purchase_intent,
                    date_diff('Day',DATE (install_date),DATE (load_date)) AS install_day_diff,
                    date_diff('Month',DATE (install_date),DATE (load_date)) AS install_month_diff,
                    date_diff('Day',DATE (min_order_created_date),DATE (load_date)) AS purchase_day_diff,
                    date_diff('Month',DATE (min_order_created_date),DATE (load_date)) AS purchase_month_diff,
                    NVL(dci.gender,'unk') AS gender,
                    purchase_date,
             FROM (SELECT DISTINCT load_date,
                          EXTRACT(HOUR FROM convert_timezone ('Asia/Calcutta',(TIMESTAMP 'epoch' + atlas_ts::FLOAT/ 1000*INTERVAL '1 Second '))) AS HOUR,
                          (CASE WHEN uidx IS NOT NULL THEN uidx ELSE ev.device_id END) AS user_id,
                          (CASE WHEN uidx IS NOT NULL THEN 'uidx' ELSE 'device_id' END) AS identifier,
                          os,
                          install_date
                   FROM clickstream.events_view ev
                   left join customer_insights.sk_traffic_user_device ins on ev.device_id=ins.device_id
                   WHERE os IS NOT NULL
                   AND   event_type = 'ScreenLoad'
                   AND   load_Date BETWEEN TO_CHAR(convert_timezone('Asia/Calcutta',(SYSDATE-INTERVAL '1 DAY')),'YYYYMMDD')::BIGINT AND TO_CHAR(convert_timezone('Asia/Calcutta',SYSDATE),'YYYYMMDD')::BIGINT) a
               LEFT JOIN customer_insights.sk_traffic_dashboard_lifetime_user_level_at_affinity b
                      ON a.user_id = b.user_id
                     AND a.identifier = b.identifier
               LEFT JOIN customer_insights.sk_traffic_dashboard_lifetime_user_level_brand_affinity x
                      ON a.user_id = x.user_id
                     AND a.identifier = x.identifier
               LEFT JOIN customer_insights.sk_traffic_ms_overall_user_level q ON a.user_id = q.uidx
               LEFT JOIN customer_insights.customer_min_order_created_date s ON a.user_id = s.uidx
               LEFT JOIN bidb.dim_customer_idea dci ON a.user_id = dci.customer_login )
;


grant select on customer_insights.traffic_dashboard_hourly_profile to myntra_reporting;

grant select on customer_insights.traffic_dashboard_hourly_profile to adhocpanel;

grant select on customer_insights.traffic_dashboard_hourly_profile to adhoc_fin_user;

grant select on customer_insights.traffic_dashboard_hourly_profile to analysis_user;

