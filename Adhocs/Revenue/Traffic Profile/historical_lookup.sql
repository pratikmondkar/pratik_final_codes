
create table dev.pm_traffic_profile_lookup distkey(user_id) as
(SELECT DISTINCT o.*,
       CASE
         WHEN identifier IN ('Logged-In','Non-Logged-In') AND install_month_diff = 0 AND install_day_diff = 0 THEN 'M0 - Same Day'
         WHEN identifier IN ('Logged-In','Non-Logged-In') AND install_month_diff = 0 AND install_day_diff != 0 THEN 'M0 - Not Same Day'
         WHEN identifier IN ('Logged-In','Non-Logged-In') AND install_month_diff = 1 THEN 'M1'
         WHEN identifier IN ('Logged-In','Non-Logged-In') AND install_month_diff = 2 THEN 'M2'
         WHEN identifier IN ('Logged-In','Non-Logged-In') AND install_month_diff = 3 THEN 'M3'
         WHEN identifier IN ('Logged-In','Non-Logged-In') AND install_month_diff BETWEEN 4 AND 6 THEN 'M4-6'
         WHEN identifier IN ('Logged-In','Non-Logged-In') AND install_month_diff BETWEEN 7 AND 12 THEN 'M7-12'
         WHEN identifier IN ('Logged-In','Non-Logged-In') AND install_month_diff > 12 THEN 'M-12+'
       END AS install_cohort,
       CASE
         WHEN first_purchase_month_diff = 0 AND first_purchase_day_diff = 0 THEN 'M0 - Same Day'
         WHEN first_purchase_month_diff = 0 AND first_purchase_day_diff != 0 THEN 'M0 - Not Same Day'
         WHEN first_purchase_month_diff = 1 THEN 'M1'
         WHEN first_purchase_month_diff = 2 THEN 'M2'
         WHEN first_purchase_month_diff = 3 THEN 'M3'
         WHEN first_purchase_month_diff BETWEEN 4 AND 6 THEN 'M4-6'
         WHEN first_purchase_month_diff BETWEEN 7 AND 12 THEN 'M7-12'
         WHEN first_purchase_month_diff > 12 THEN 'M-12+'
         WHEN first_purchase_month_diff IS NULL AND identifier IN ('Logged-In') THEN 'RNB'
         WHEN first_purchase_month_diff IS NULL AND identifier IN ('Non-Logged-In') THEN 'Non-Logged-In'
       END AS acquisition_cohort,
       (CASE WHEN dci.gender IS NOT NULL THEN dci.gender ELSE 'unk' END) AS gender,
       CASE
         WHEN last_purchase_month_diff = 0 AND last_purchase_day_diff != 0 THEN 'M0'
         WHEN last_purchase_month_diff = 1 THEN 'M1'
         WHEN last_purchase_month_diff = 2 THEN 'M2'
         WHEN last_purchase_month_diff = 3 THEN 'M3'
         WHEN last_purchase_month_diff BETWEEN 4 AND 6 THEN 'M4-6'
         WHEN last_purchase_month_diff BETWEEN 7 AND 12 THEN 'M7-12'
         WHEN last_purchase_month_diff > 12 THEN 'M-12+'
         WHEN last_purchase_month_diff IS NULL AND identifier IN ('Logged-In') THEN 'RNB'
         WHEN last_purchase_month_diff IS NULL AND identifier IN ('Non-Logged-In') THEN 'Non-Logged-In'
       END AS recency_cohort
--     o.sessions
       FROM (SELECT a.*,
                    date_diff('Day',DATE (install_date),sysdate) AS install_day_diff,
                    date_diff('Month',DATE (install_date),sysdate) AS install_month_diff,
                    date_diff('Day',DATE (first_purchase_date),sysdate) AS first_purchase_day_diff,
                    date_diff('Month',DATE (first_purchase_date),sysdate) AS first_purchase_month_diff,
                    date_diff('Day',DATE (last_purchase_date),sysdate) AS last_purchase_day_diff,
                    date_diff('Month',DATE (last_purchase_date),sysdate) AS last_purchase_month_diff
             --a.sessions
                    FROM (SELECT p.*,
                                 (CASE WHEN p.identifier IN ('Logged-In') AND q.eng IS NOT NULL THEN q.eng WHEN p.identifier IN ('Logged-In') AND q.eng IS NULL THEN 'Not Browsed' WHEN p.identifier IN ('Non-Logged-In','Non-Logged-In-Web') AND q.eng IS NULL THEN 'Non-Logged-In' END) AS engagement,
                                 (CASE WHEN p.identifier IN ('Logged-In') AND q.perf IS NOT NULL THEN q.perf WHEN p.identifier IN ('Logged-In') AND q.perf IS NULL THEN 'Not Purchased' WHEN p.identifier IN ('Non-Logged-In','Non-Logged-In-Web') AND q.perf IS NULL THEN 'Non-Logged-In' END) AS performance,
                                 (CASE WHEN p.identifier IN ('Logged-In') AND q.di IS NOT NULL THEN q.di WHEN p.identifier IN ('Logged-In') AND q.di IS NULL THEN 'No Product viewed and purchased' WHEN p.identifier IN ('Non-Logged-In','Non-Logged-In-Web') AND q.di IS NULL THEN 'Non-Logged-In' END) AS discount,
                                 (CASE WHEN p.identifier IN ('Logged-In') AND q.sa IS NOT NULL THEN q.sa WHEN p.identifier IN ('Logged-In') AND q.sa IS NULL THEN 'Not Purchased' WHEN p.identifier IN ('Non-Logged-In','Non-Logged-In-Web') AND q.sa IS NULL THEN 'Non-Logged-In' END) AS satisfaction,
                                 '' AS purchase_intent,
                                 r.install_date,
                                 s.min_order_created_date AS first_purchase_date,
                                 s.max_order_created_date AS last_purchase_date,
                                 CASE
                                   WHEN p.identifier IN ('Logged-In') THEN nvl (uidx_sessions,0)
                                   WHEN p.identifier IN ('Non-Logged-In') THEN nvl (device_sessions,0)
                                 END AS total_sessions
                          --p.sessions
                                 FROM (SELECT a.*,
                                              (CASE WHEN b.article_type IS NOT NULL THEN b.article_type ELSE 'Unknown' END) AS article_type_affinity,
                                              (CASE WHEN x.brand IS NOT NULL THEN x.brand ELSE 'Unknown' END) AS brand_affinity
                                       FROM (			SELECT DISTINCT
																									       CASE
																									         WHEN uidx IS NOT NULL THEN uidx
																									         WHEN uidx IS NULL THEN device_id
																									       END AS user_id,
																									       max(CASE
																									         WHEN uidx IS NOT NULL THEN 'Logged-In'
																									         WHEN uidx IS NULL THEN 'Non-Logged-In'
																									       END) AS identifier,
																									       max(CASE
																									         WHEN os = 'Android' THEN 'Android'
																									         WHEN os = 'iOS' THEN 'iOS'
																									       END) AS os,
																									       sum(DISTINCT CASE WHEN uidx IS NOT NULL THEN sessions END) AS uidx_sessions,
																									       sum(DISTINCT CASE WHEN uidx IS NULL THEN sessions END) AS device_sessions
																									FROM clickstream.daily_aggregates
																									GROUP BY 1        ) a
                                         LEFT JOIN customer_insights.sk_traffic_dashboard_lifetime_user_level_at_affinity_date_level b ON a.user_id = b.user_id
                                         LEFT JOIN customer_insights.sk_traffic_dashboard_lifetime_user_level_brand_affinity_date_level x ON a.user_id = x.user_id) p
                            LEFT JOIN customer_insights.sk_traffic_ms_overall_user_level_date_level q ON p.user_id = q.uidx
                            LEFT JOIN customer_insights.sk_traffic_user_install_date_date_level r ON p.user_id = r.user_id
                            LEFT JOIN customer_insights.customer_min_order_created_date_date_level s ON p.user_id = s.uidx) a) o
  LEFT JOIN bidb.dim_customer_idea dci ON o.user_id = dci.customer_login
WHERE identifier IS NOT NULL);



select count(*) ,count(distinct user_id) from dev.pm_traffic_profile_lookup;

