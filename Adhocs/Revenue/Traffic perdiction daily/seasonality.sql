SELECT load_date,
       day_of_the_week,
       week_number,
       month,
       year,
       SUM(sessions)
FROM clickstream.daily_aggregates da
  LEFT JOIN dim_date dd ON da.load_date = dd.full_date
WHERE load_date >= 2016001
GROUP BY 1,2,3,4,5;


select month,
       year,
count(distinct order_group_id)
from fact_order fo 
left join dim_date dd on fo.order_created_date=dd.full_date
where is_booked=1 and store_id=1 and order_created_date between 20140101 and 20161231
--and order_created_date not in (20170101,20170101,20170101)
group by 1,2;


select to_date(full_date,'YYYYMMDD') as date,
       day_of_the_week,
       iso_week_of_the_year,
       week_number,
       month
       from dim_date 
       where full_date between 20161201 and 20170313;
       

SELECT full_date,
       day_of_the_week,
       week_number,
       iso_week_of_the_year,
       month,
       year,
       SUM(sessions)
FROM ga_metrics da
  LEFT JOIN dim_date dd ON da.date = dd.full_date
WHERE da.date between 20150101 and 20161231
GROUP BY 1,2,3,4,5,6;

