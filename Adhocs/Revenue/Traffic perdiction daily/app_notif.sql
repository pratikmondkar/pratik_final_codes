SELECT load_date,
       SUM(sale_notif_open_count) AS sale_notif_open_count,
       sum(remkt_notif_open_count) as remkt_notif_open_count,
       sum(trans_notif_open_count) as trans_notif_open_count,
       sum(eng_notif_open_count) as eng_notif_open_count,
       sum(unk_notif_open_count) as unk_notif_open_count,
       sum(notif_open_count) as notif_open_count
       from clickstream.daily_notif_aggregates
       where load_date >=20160901
       group by 1;
       

select to_char(created_datetime,'YYYY-MM-DD') as date,
count(distinct device_id) as install
from app_install_tune 
where created_datetime>='2016-12-01 00:00:00'
group by 1
