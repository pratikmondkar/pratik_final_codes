SELECT a.os,
       a.class,
       COUNT(DISTINCT a.device_id) AS installs,
       COUNT(DISTINCT CASE WHEN b.load_date BETWEEN install_date +interval '1 days' AND install_date +interval '7 days' THEN a.device_id END) AS d_1_7_retention,
       COUNT(DISTINCT CASE WHEN b.load_date BETWEEN install_date +interval '7 days' AND install_date +interval '14 days' THEN a.device_id END) AS d_8_14_retention,
       COUNT(DISTINCT CASE WHEN b.load_date BETWEEN install_date +interval '14 days' AND install_date +interval '21 days' THEN a.device_id END) AS d_14_21_retention,
       COUNT(DISTINCT CASE WHEN b.load_date BETWEEN install_date +interval '21 days' AND install_date +interval '28 days' THEN a.device_id END) AS d_21_28_retention
FROM (SELECT device_id,
             install_date,
             CASE
               WHEN keyspace = 'IDFA' THEN 'iOS'
               ELSE 'Android'
             END AS os,
             class
      FROM (SELECT install_date,
                   CASE
                     WHEN keyspace = 'IDFA' THEN r.idfv
                     ELSE r.device_id
                   END AS device_id,
                   keyspace,
                   campaign_source,
                   campaign_name
            FROM bidb.app_referral r
            WHERE keyspace IN ('ANDI','IDFA')
            AND   install_date BETWEEN '2016-06-01 00:00:00' AND '2016-06-30 23:59:59'
            AND   attribution = 'Install'
            UNION ALL
            SELECT install_date,
                   a.device_id AS device_id,
                   keyspace,
                   campaign_source,
                   campaign_name
            FROM bidb.app_referral r
              LEFT JOIN bidb.app_users_magasin a ON r.device_id = a.advertising_id
            WHERE keyspace = 'AIFA'
            AND   install_date BETWEEN '2016-06-01 00:00:00' AND '2016-06-30 23:59:59'
            AND   attribution = 'Install') i
        LEFT JOIN clickstream.install_source_mapping ism
               ON i.campaign_source = ism.campaign_source
              AND i.campaign_name = ism.campaign_name) a
  LEFT JOIN (SELECT DISTINCT device_id,
                    TO_DATE(load_date,'YYYYMMDD') AS load_date
             FROM clickstream.events_view
             WHERE event_type IN ('ScreenLoad','appLaunch')
             AND   load_date BETWEEN 20160601 AND 20160731) b ON a.device_id = b.device_id
GROUP BY 1,
         2 LIMIT 100

