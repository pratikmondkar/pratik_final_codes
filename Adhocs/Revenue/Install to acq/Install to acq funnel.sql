--Android
SELECT to_char(sysdate - interval '%(dstart1)s days','YYYYMMDD') as week_start_date,
       to_char(sysdate - interval '%(dfinish1)s days','YYYYMMDD') as week_end_date,
       os,
       class,
       SUM(case when os='iOS' then installs/2 else installs end) AS installs,
       SUM(found_app_users) AS found_app_users,
       SUM(found_idea) AS found_idea,
       SUM(total_signups) AS total_signups,
       SUM(d0_signups) AS d0_signups,
       SUM(d7_signups) AS d7_signups,
       SUM(total_acquisitions) AS total_acquisitions,
       SUM(d0_acquisitions) AS d0_acquisitions,
       SUM(d7_acquisitions) AS d7_acquisitions,
       SUM(Total_acq_revenue) AS Total_acq_revenue,
       SUM(d0_acq_revenue) AS d0_acq_revenue,
       SUM(d7_acq_revenue) AS d7_acq_revenue
FROM (SELECT CASE
               WHEN keyspace = 'IDFA' THEN 'iOS'
               ELSE 'Android'
             END AS os,
             class,
             COUNT(DISTINCT t1.device_id) AS installs,
             COUNT(DISTINCT CASE WHEN au_device_id IS NOT NULL THEN t1.device_id END) AS found_app_users,
             COUNT(DISTINCT CASE WHEN customer_login IS NOT NULL THEN t1.device_id END) AS found_idea,
             COUNT(DISTINCT CASE WHEN customer_login IS NOT NULL AND first_login_date >= TO_CHAR(install_date,'YYYYMMDD') THEN t1.device_id END) AS total_signups,
             COUNT(DISTINCT CASE WHEN customer_login IS NOT NULL AND first_login_date = TO_CHAR(install_date,'YYYYMMDD') THEN t1.device_id END) AS d0_signups,
             COUNT(DISTINCT CASE WHEN customer_login IS NOT NULL AND first_login_date BETWEEN TO_CHAR(install_date,'YYYYMMDD') AND TO_CHAR(install_date +INTERVAL '7 days','YYYYMMDD') THEN t1.device_id END) AS d7_signups,
             COUNT(DISTINCT CASE WHEN idcustomer_idea IS NOT NULL THEN t1.device_id END) AS total_acquisitions,
             COUNT(DISTINCT CASE WHEN idcustomer_idea IS NOT NULL AND order_created_date = TO_CHAR(install_date,'YYYYMMDD') THEN t1.device_id END) AS d0_acquisitions,
             COUNT(DISTINCT CASE WHEN idcustomer_idea IS NOT NULL AND order_created_date BETWEEN TO_CHAR(install_date,'YYYYMMDD') AND TO_CHAR(install_date +INTERVAL '7 days','YYYYMMDD') THEN t1.device_id END) AS d7_acquisitions,
             SUM(CASE WHEN e.idcustomer_idea IS NOT NULL THEN revenue ELSE 0 END) AS Total_acq_revenue,
             SUM(CASE WHEN idcustomer_idea IS NOT NULL AND order_created_date = TO_CHAR(install_date,'YYYYMMDD') THEN revenue END) AS d0_acq_revenue,
             SUM(CASE WHEN idcustomer_idea IS NOT NULL AND order_created_date BETWEEN TO_CHAR(install_date,'YYYYMMDD') AND TO_CHAR(install_date +INTERVAL '7 days','YYYYMMDD') THEN revenue END) AS d7_acq_revenue
      FROM (SELECT r.device_id,
                   a.device_id AS au_device_id,
                   install_date,
                   user_id,
                   attribution,
                   keyspace,
                   class
            FROM app_referral r
              LEFT JOIN app_users_magasin a ON r.device_id = a.device_id
              LEFT JOIN install_source_mapping ism
                     ON r.campaign_source = ism.campaign_source
                    AND r.campaign_name = ism.campaign_name
            WHERE keyspace = 'ANDI'
            AND   install_date BETWEEN sysdate - interval '%(dstart1)s days' AND to_char(sysdate - interval '%(dfinish1)s days','YYYYMMDD')
            AND   attribution = 'Install'
            UNION ALL
            SELECT r.device_id,
                   a.device_id AS au_device_id,
                   install_date,
                   user_id user_id,
                   attribution,
                   keyspace,
                   class
            FROM app_referral r
              LEFT JOIN app_users_magasin a ON r.device_id = a.advertising_id
              LEFT JOIN install_source_mapping ism
                     ON r.campaign_source = ism.campaign_source
                    AND r.campaign_name = ism.campaign_name
            WHERE keyspace <> 'ANDI'
            AND   to_char(install_date,'YYYYMMDD') BETWEEN to_char(sysdate - interval '%(dstart1)s days','YYYYMMDD') AND to_char(sysdate - interval '%(dfinish1)s days','YYYYMMDD')
            AND   attribution = 'Install') t1
        LEFT JOIN dim_customer_idea d ON t1.user_id = d.customer_login
        LEFT JOIN (SELECT idcustomer_idea,
                          SUM(shipped_order_revenue_inc_cashback) AS revenue,
                          MAX(order_created_date) AS order_created_date
                   FROM fact_order
                   WHERE order_created_date >= to_char(sysdate - interval '%(dstart1)s days','YYYYMMDD')
                   AND   store_id = 1
                   AND   (is_realised = 1 OR is_shipped = 1)
                   AND   purchase_type = 'f'
                   GROUP BY 1) e ON d.id = e.idcustomer_idea
      GROUP BY 1,
               2
      UNION ALL
      SELECT CASE
               WHEN keyspace = 'IDFA' THEN 'iOS'
               ELSE 'Android'
             END AS os,
             class,
             COUNT(DISTINCT t1.device_id) AS installs,
             COUNT(DISTINCT CASE WHEN au_device_id IS NOT NULL THEN t1.device_id END) AS found_app_users,
             COUNT(DISTINCT CASE WHEN customer_login IS NOT NULL THEN t1.device_id END) AS found_idea,
             COUNT(DISTINCT CASE WHEN customer_login IS NOT NULL AND first_login_date >= TO_CHAR(install_date,'YYYYMMDD') THEN t1.device_id END) AS total_signups,
             COUNT(DISTINCT CASE WHEN customer_login IS NOT NULL AND first_login_date = TO_CHAR(install_date,'YYYYMMDD') THEN t1.device_id END) AS d0_signups,
             COUNT(DISTINCT CASE WHEN customer_login IS NOT NULL AND first_login_date BETWEEN TO_CHAR(install_date,'YYYYMMDD') AND TO_CHAR(install_date +INTERVAL '7 days','YYYYMMDD') THEN t1.device_id END) AS d7_signups,
             COUNT(DISTINCT CASE WHEN idcustomer_idea IS NOT NULL THEN t1.device_id END) AS total_acquisitions,
             COUNT(DISTINCT CASE WHEN idcustomer_idea IS NOT NULL AND order_created_date = TO_CHAR(install_date,'YYYYMMDD') THEN t1.device_id END) AS d0_acquisitions,
             COUNT(DISTINCT CASE WHEN idcustomer_idea IS NOT NULL AND order_created_date BETWEEN TO_CHAR(install_date,'YYYYMMDD') AND TO_CHAR(install_date +INTERVAL '7 days','YYYYMMDD') THEN t1.device_id END) AS d7_acquisitions,
             SUM(CASE WHEN e.idcustomer_idea IS NOT NULL THEN revenue ELSE 0 END) AS Total_acq_revenue,
             SUM(CASE WHEN idcustomer_idea IS NOT NULL AND order_created_date = TO_CHAR(install_date,'YYYYMMDD') THEN revenue END) AS d0_acq_revenue,
             SUM(CASE WHEN idcustomer_idea IS NOT NULL AND order_created_date BETWEEN TO_CHAR(install_date,'YYYYMMDD') AND TO_CHAR(install_date +INTERVAL '7 days','YYYYMMDD') THEN revenue END) AS d7_acq_revenue
      FROM (SELECT r.device_id,
                   a.device_id AS au_device_id,
                   install_date,
                   user_id,
                   attribution,
                   keyspace,
                   class
            FROM app_referral r
              LEFT JOIN app_users_magasin a ON r.device_id = a.advertising_id
              LEFT JOIN install_source_mapping ism
                     ON r.campaign_source = ism.campaign_source
                    AND r.campaign_name = ism.campaign_name
            WHERE keyspace = 'IDFA'
            AND   to_char(install_date,'YYYYMMDD') BETWEEN to_char(sysdate - interval '%(dstart1)s days','YYYYMMDD') AND to_char(sysdate - interval '%(dfinish1)s days','YYYYMMDD')
            AND   attribution = 'Install') t1
        LEFT JOIN dim_customer_email dce ON t1.user_id = dce.email
        LEFT JOIN dim_customer_idea d ON dce.uid = d.id
        LEFT JOIN (SELECT idcustomer_idea,
                          SUM(shipped_order_revenue_inc_cashback) AS revenue,
                          MAX(order_created_date) AS order_created_date
                   FROM fact_order
                   WHERE order_created_date >= to_char(sysdate - interval '%(dstart1)s days','YYYYMMDD') 
                   AND   store_id = 1
                   AND   (is_realised = 1 OR is_shipped = 1)
                   AND   purchase_type = 'f'
                   GROUP BY 1) e ON d.id = e.idcustomer_idea
      GROUP BY 1,
               2)
GROUP BY 1,
         2,3,4
