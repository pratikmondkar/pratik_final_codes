drop table if exists dev.map;

create table dev.map distkey(advertising_id) as 
select distinct advertising_id,device_id,user_id FROM app_users_magasin
WHERE lower(ADVERTISING_ID) IN ( SELECT DISTINCT device_id FROM app_install_tune WHERE created_datetime >= '2016-08-18 00:00:00') ;

SELECT 
TO_CHAR(r.created_datetime,'YYYYMMDD') AS install_date, 
r.keyspace, 
COUNT(DISTINCT r.device_id) AS installs, 
COUNT(DISTINCT CASE WHEN a.advertising_id IS NOT NULL THEN r.device_id END) AS found_app_users 
FROM 
app_install_tune r LEFT JOIN dev.map a ON lower(trim(r.device_id)) = lower(trim(a.advertising_id)) 
WHERE 
attribution = 'Install' and keyspace in ('GAID','IFA')
and created_datetime >= '2016-08-18 00:00:00'
GROUP BY 1,2;


select TO_CHAR(r.created_datetime,'YYYYMMDD') AS install_date,keyspace, count(distinct device_id) as installs
from app_install_tune
where created_datetime >= '2016-09-18 00:00:00' and attribution = 'Install'
group by 1,2;

select count(distinct device_id) from


SELECT distinct r.device_id
FROM app_install_tune r 
WHERE attribution = 'Install' and keyspace in ('IFA') and created_datetime between '2016-09-20 00:00:00' and '2016-09-20 23:59:59' ;

SELECT distinct r.advertising_id
FROM app_users_magasin r 
WHERE device_type in ('iOS') and created_date between '2016-09-20 00:00:00' and '2016-09-20 23:59:59' ;
