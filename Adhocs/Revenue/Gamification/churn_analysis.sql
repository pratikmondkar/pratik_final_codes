select count(*) from 
(SELECT a.customer_login,
       nvl(m2_flag,0) AS m2_flag,
       nvl(m3_flag,0) AS m3_flag,
       nvl(m4_flag,0) AS m4_flag
FROM (SELECT customer_login
      FROM bidb.dim_customer_idea
      WHERE first_login_date BETWEEN 20160301 AND 20160331) a
  LEFT JOIN (SELECT DISTINCT uidx,
                    1 AS m2_flag
             FROM clickstream.events_view
             WHERE event_type IN ('ScreenLoad','appLaunch','push-notification-received','beacon-ping') and load_date BETWEEN 20160501 AND 20160507) m2 ON a.customer_login = m2.uidx
  LEFT JOIN (SELECT DISTINCT uidx,
                    1 AS m3_flag
             FROM clickstream.events_view
             WHERE event_type IN ('ScreenLoad','appLaunch','push-notification-received','beacon-ping') and load_date BETWEEN 20160601 AND 20160607) m3 ON a.customer_login = m3.uidx
  LEFT JOIN (SELECT DISTINCT uidx,
                    1 AS m4_flag
             FROM clickstream.events_view
             WHERE event_type IN ('ScreenLoad','appLaunch','push-notification-received','beacon-ping') and load_date BETWEEN 20160701 AND 20160707) m4 ON a.customer_login = m4.uidx
) 
