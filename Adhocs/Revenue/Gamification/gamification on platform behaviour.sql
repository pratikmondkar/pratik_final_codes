SELECT uidx,
       COUNT(CASE WHEN event_type = 'push-notification-received' THEN 1 END) AS notifs_received_pre,
       COUNT(CASE WHEN event_type = 'push-notification' THEN 1 END) AS notifs_opened_pre,
--       COUNT(CASE WHEN LOWER(data_set_type) IN ('split-banner','banner','split_banner') OR event_type = 'banner-click' THEN 1 END) AS banner_clicks,
       COUNT(CASE WHEN event_type = 'NotificationItemsClick' THEN 1 END) AS inapp_notifs,
       COUNT(CASE WHEN event_type = 'ScreenLoad' AND landing_screen LIKE 'Shopping Page-PDP%' THEN 1 END) AS pdp,
       COUNT(CASE WHEN event_type = 'ScreenLoad' AND landing_screen LIKE 'Shopping Page-List Page%' THEN 1 END) AS list,
       COUNT(CASE WHEN event_type = 'addToCart'  THEN 1 END) AS atc,
       COUNT(CASE WHEN event_type = 'banner-click' THEN 1 END) AS banner_clicks_filt,
--       COUNT(DISTINCT CASE WHEN event_type = 'appLaunch' AND utm_medium IS NULL THEN session_id END) / NULLIF(COUNT(DISTINCT CASE WHEN event_type = 'appLaunch' THEN session_id END)::DECIMAL(10,2),0) AS perc_organic,
       COUNT(DISTINCT CASE WHEN event_type = 'ScreenLoad' THEN session_id END) AS sessions,
       SUM(CASE WHEN event_type = 'ScreenLoad' AND landing_screen LIKE 'Shopping Page-PDP%' THEN mrp - discounted_price END) / NULLIF(SUM(CASE WHEN event_type = 'ScreenLoad' AND landing_screen LIKE 'Shopping Page-PDP%' THEN mrp END),0) AS avg_disc_view
FROM clickstream.events_view
WHERE load_date BETWEEN 20160601 AND 20160615 and uidx is not null 
and event_type in ('ScreenLoad','push-notification-received','push-notification','NotificationItemsClick','addToCart','banner-click')
GROUP BY 1

