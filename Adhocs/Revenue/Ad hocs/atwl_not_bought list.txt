--Query to get list of uidx that have added to wishlist but not bought and are in stock
SELECT DISTINCT uidx
FROM (SELECT uidx,
             COUNT(DISTINCT a.style_id) AS styles
      FROM (SELECT uidx,
                   style_id
            FROM (SELECT uidx,
                         cl.style_id,
                         action,
                         ROW_NUMBER() OVER (PARTITION BY uidx,cl.style_id ORDER BY added_datetime DESC) AS rnk
                  FROM bidb.collection_log_eors cl
                    JOIN dim_style dp ON cl.style_id = dp.style_id
                  WHERE uidx NOT LIKE '%%myntra360.com%%'
                  AND   uidx NOT LIKE '%%eossbenchmarking%%@gmail.com'
                  AND   uidx NOT LIKE '%%scmloadtest%%'
                  AND   uidx NOT LIKE '%%sfloadtest%%')
            WHERE rnk = 1
            AND   action = 1) a
        LEFT JOIN (SELECT DISTINCT customer_login,
                          style_id
                   FROM fact_core_item fci
                     JOIN dim_customer_idea dci ON fci.idcustomer = dci.id
                   WHERE (is_shipped = 1 OR is_realised = 1)
                   AND   store_id = 1
                   AND   order_created_date >= 20161226) b
               ON a.uidx = b.customer_login
              AND a.style_id = b.style_id
        LEFT JOIN (select style_id,SUM (inventory_count - nvl (blocked_order_count)) AS inv FROM fact_atp_inventory
      WHERE is_live_on_portal = 1
      GROUP BY 1) c ON a.style_id = c.style_id WHERE b.customer_login IS NULL AND b.style_id IS NULL AND c.inv > 0
      GROUP BY 1)
      where styles>=3