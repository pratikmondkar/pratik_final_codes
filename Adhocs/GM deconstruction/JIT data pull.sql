SELECT 
		o.order_group_id,
		o.order_status,
		o.item_id,
		o.core_item_id,
		o.warehouse_id,
		p.brand,
		p.sku_code,
		o.order_created_date,
		o.order_created_time,
		o.item_revenue_inc_cashback,
		o.tax,
		cogs,
		(cogs*100)/(o.item_mrp_value*o.quantity) as cogs_percent,
		item_mrp_value,
		vendor_funding,
		item_purchase_price_inc_tax,
		product_discount,
		quantity,
		o.discount_rule_id,
		drh.discount_id,
		discount_funding,
		funding_percentage,
		discount_limit,
		o.last_modified_on
	FROM
	fact_orderitem o, dim_date dt, dim_product p,dim_discount_history dh,dim_discount_rule_history drh
		WHERE 
			o.idproduct=p.id AND dt.full_date = o.order_created_date AND o.discount_rule_id=drh.discount_rule_id and dh.discounT_id=drh.discounT_id and
			o.supply_type='JUST_IN_TIME'   AND (o.is_realised = 1 OR o.is_shipped = 1) 
			and o.order_created_date >= 20151215  and item_purchase_price_inc_tax=0
order by order_created_date,order_created_time
limit 20000;

