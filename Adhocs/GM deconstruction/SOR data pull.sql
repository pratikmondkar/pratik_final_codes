SELECT 
		o.order_group_id,
		o.order_id,
		o.sku_id,
		o.item_id,
		p.brand,
		p.article_type,
		o.order_status,
		o.order_created_date,
		o.order_created_time,
		o.item_revenue_inc_cashback,
		o.tax,
		cogs,
		rgm,
--		 (select basis_of_margin from dim_commercial dc where p.brand=dc.brand and p.article_type_id=dc.article_type_id 
--		 and p.gender=dc.gender and cast(o.order_created_date ||' '|| lpad(o.order_created_time,4,0) as timestamp) between effective_from and effective_till limit 1) as basis_of_margin,
		 (select type_of_agmt from dim_commercial dc where p.brand=dc.brand and p.article_type_id=dc.article_type_id 
		 and p.gender=dc.gender and cast(o.order_created_date ||' '|| lpad(o.order_created_time,4,0) as timestamp) between effective_from and effective_till limit 1) as commercial_type_dc,
		item_mrp_value,
		vendor_funding,
		o.discount_rule_id,
		drh.discount_id,
		fpo.item_purchase_price_inc_tax,
		product_discount,
		o.quantity,
		discount_funding,
		funding_percentage,
		discount_limit,
		fpo.agreed_margin,
		fpo.tax_type,
		o.tax_rate
	FROM
	fact_orderitem o
	left join dim_date dt on dt.full_date = o.order_created_date
	left join dim_product p on o.idproduct=p.id
	left join dim_discount_rule_history drh on o.discount_rule_id=drh.discount_rule_id
	left join dim_discount_history dh on dh.discounT_id=drh.discounT_id 
	left join fact_inventory_item fii on o.order_id=fii.order_id and o.sku_id=fii.sku_id
	left join fact_purchase_order fpo on fii.po_sku_id =fpo.po_sku_id and fii.po_barcode=fpo.barcode
		WHERE 
			o.supply_type='ON_HAND' and 
--			commercial_type <> 'OUTRIGHT' and
			(o.is_realised = 1 OR o.is_shipped = 1) 
			and o.order_created_date = 20151001 
			and (cogs/(o.item_mrp_value*o.quantity) > 1 or cogs/(o.item_mrp_value*o.quantity) <= 0)
--			and p.brand='Wildcraft'
--			and rgm<0
--			and o.cogs<0
limit 10000			
