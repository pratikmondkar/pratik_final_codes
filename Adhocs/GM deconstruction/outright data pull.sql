SELECT o.order_group_id,
       o.item_id,
       p.brand,
       o.order_created_date,
       o.item_revenue_inc_cashback,
       o.quantity,
       o.tax,
       o.discount_rule_id,
       drh.discount_id,
       cogs,
       (cogs*100) /(o.item_mrp_value*o.quantity) AS cogs_percent,
       item_mrp_value,
       vendor_funding,
       funding_basis,
       item_purchase_price_inc_tax,
       product_discount,
       coupon_discount,
       quantity,
       discount_funding,
       funding_percentage,
       discount_limit
FROM fact_orderitem o
  LEFT JOIN dim_date dt ON o.order_created_date = dt.full_date
  LEFT JOIN dim_product p ON o.sku_id = p.sku_id
  LEFT JOIN dim_discount_rule_history drh ON o.discount_rule_id = drh.discount_rule_id 
    LEFT JOIN dim_discount_history dh ON dh.discounT_id = drh.discounT_id
WHERE o.supply_type = 'ON_HAND'
AND   commercial_type = 'OUTRIGHT'
AND   (o.is_realised = 1 OR o.is_shipped = 1)
AND   o.order_created_date >= 20151001
AND   (cogs / (o.item_mrp_value*o.quantity) > 1 OR cogs / (o.item_mrp_value*o.quantity) <= 0)
--			and brand='Bata'  
--			and p.style_id in ('532615','532604')   


