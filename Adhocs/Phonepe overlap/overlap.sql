--Users
SELECT COUNT(DISTINCT a.email) AS phonepe_users,
       COUNT(DISTINCT b.email) AS myntra_users,
       COUNT(DISTINCT CASE WHEN a.email IS NOT NULL AND b.email IS NOT NULL THEN a.email END) AS overlap
FROM (select distinct lower(email) as email from dev.phonepe_mau) a
  FULL OUTER JOIN (SELECT DISTINCT lower(email) as email
                   FROM clickstream.daily_aggregates da
                     JOIN dim_customer_idea dci ON da.uidx = dci.customer_login
                     JOIN cii.dim_customer_email dce ON dci.id = dce.uid
                   WHERE load_date BETWEEN 20170801 AND 20180131 and
                      uidx IS NOT NULL
                   union 
                   SELECT DISTINCT lower(email) as email
                   FROM customer_insights.daily_aggregates_web da
                     JOIN dim_customer_idea dci ON da.uidx = dci.customer_login
                     JOIN cii.dim_customer_email dce ON dci.id = dce.uid
                   WHERE load_date BETWEEN 20170801 AND 20180131 and
                      uidx IS NOT NULL) b ON  a.email= b.email;
                   
--customers 

SELECT COUNT(DISTINCT a.email) AS phonepe_customers,
       COUNT(DISTINCT b.email) AS myntra_customers,
       COUNT(DISTINCT CASE WHEN a.email IS NOT NULL AND b.email IS NOT NULL THEN a.email END) AS overlap
FROM (select distinct lower(email) as email from dev.phonepe_mac) a
  FULL OUTER JOIN (SELECT DISTINCT lower(email) as email
                   FROM bidb.fact_core_item fci
                     JOIN cii.dim_customer_email dce ON fci.idcustomer = dce.uid
                   WHERE order_created_date BETWEEN 20170801 AND 20171231 and 
                   is_booked=1 and store_id=1) b ON  a.email= b.email

