SELECT  dp.style_id,
				dp.sku_id,
       case when commercial_type in ('JIT_MARKETPLACE','SMART_JIT') or supply_type = 'JUST_IN_TIME' then 'JIT_seller' else 'OR_SOR_seller' end as seller
FROM dim_product dp
  JOIN (select distinct style_id from fact_product_snapshot where is_live_on_portal=1 and date=20161208) fps
  on dp.style_id=fps.style_id  
 -- to_char(sysdate,'YYYYMMDD')::bigint
  limit 100;
  

select dp.style_id,case when seller = '1' then 21 when seller = '2' then 25 when seller = '3' then 19 else 29 end as seller,
nvl(fps.style_status,'NA') style_status,nvl(fps.net_inventory_count,0) net_inventory_count
from (select distinct style_id,brand from dim_product) dp 
left join (select style_id,sum(net_inventory_count)as net_inventory_count,max(style_status) as style_status 
						from fact_product_snapshot where date=20161214 group by 1) fps on dp.style_id=fps.style_id
left join dev.brand_mapping bm on dp.brand=bm.brand and seller<>'#N/A';

select * from dev.brand_mapping;


select distinct brand from dim_product
  

