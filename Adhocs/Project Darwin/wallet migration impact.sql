select 
case when c.uidx is not null then 'CB' else 'NCB' end as cashback_flag,
order_created_date,
sum(shipped_order_revenue_inc_cashback) as revenue,
count(distinct order_group_id) as orders,
count(distinct idcustomer_idea) as customers
from
fact_order fo
left join dim_customer_idea dci on fo.idcustomer_idea=dci.id
left join dev.cashback_cus c on dci.customer_login =c.uidx
where (is_shipped=1 or is_realised=1) and store_id=1 and order_created_date >=20160801
group by 1,2;

SELECT CASE
         WHEN c.uidx IS NOT NULL THEN 'CB'
         ELSE 'NCB'
       END AS cashback_flag,
       load_date,
       COUNT( case when screen_name= 'Checkout-Cart' then 1 end) AS cart,
       COUNT( case when screen_name= 'Checkout-address' then 1 end) AS address,
       COUNT( case when screen_name= 'Checkout-payment' then 1 end) AS payment,
       COUNT( case when screen_name= 'Checkout-phonepe-page' then 1 end) AS pp,
       COUNT( case when screen_name= 'Checkout-confirmation' then 1 end) AS confirm
FROM clickstream.events_view fo
  LEFT JOIN dev.cashback_cus c ON fo.uidx = c.uidx
WHERE load_date between 20160801 and 20160831
AND   event_type = 'ScreenLoad'
AND   screen_name IN ('Checkout-confirmation','Checkout-Cart','Checkout-address','Checkout-payment','Checkout-phonepe-page')
GROUP BY 1,2
