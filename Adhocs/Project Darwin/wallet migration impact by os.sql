select 
case when c.uidx is not null then 'CB' else 'NCB' end as cashback_flag,
case when order_channel = 'mobile-app' then os_info else order_channel end as channel,
order_created_date,
sum(shipped_order_revenue_inc_cashback) as revenue,
count(distinct order_group_id) as orders,
count(distinct idcustomer_idea) as customers
from
fact_order fo
left join dim_customer_idea dci on fo.idcustomer_idea=dci.id
left join dev.cashback_cus c on dci.customer_login =c.uidx
where (is_shipped=1 or is_realised=1) and store_id=1 and order_created_date >=20160801
group by 1,2,3
