select brand,sum(item_revenue_inc_cashback+nvl(shipping_charges)+nvl(gift_charges)) as revenue
FROM fact_core_item
WHERE store_id = 1
and seller_id=21
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date >=20170201 
 group by 1;
 
select sum(item_revenue_inc_cashback+nvl(shipping_charges)+nvl(gift_charges)) as revenue
FROM fact_core_item
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date >=20170201 ;


select dp.sku_id,dp.style_id,dp.seller_id as orig_seller_id,29 as new_seller_id
FROM (select distinct sim.sku_id,sim.style_id,sim.seller_id 
			from seller_item_master sim 
			left join dim_product dp on sim.sku_id=dp.sku_id 
			where sim.seller_id=21 and enabled=1 and dp.brand != 'Tommy Hilfiger') dp
  LEFT JOIN (SELECT sku_id,
                    SUM(inventory_count) AS inventory_count,
                    MAX(style_status) AS style_status
             FROM fact_atp_inventory
             GROUP BY 1) fps ON dp.sku_id = fps.sku_id
where fps.style_status='P' and fps.inventory_count = 0;


select distinct dp.style_id,29 as new_seller_id
FROM (select distinct sim.sku_id,sim.style_id,sim.seller_id 
			from seller_item_master sim 
			where sim.seller_id=21 and enabled=1 ) dp
  LEFT JOIN (SELECT style_id,
                    SUM(inventory_count) AS inventory_count,
                    MAX(style_status) AS style_status
             FROM fact_atp_inventory
             GROUP BY 1) fps ON dp.style_id = fps.style_id
where fps.style_status!='P' and fps.inventory_count > 0
