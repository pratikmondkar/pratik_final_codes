select * from seller_brand where enabled=1 limit 100;

select brand,sum(item_revenue_inc_cashback) as rev 
FROM fact_core_item
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date > 20170529
and brand in (select distinct brand from dev.brand_mapping where seller_id=30)
group by 1;
--20170529;

select sku_id,style_id,sum(item_revenue_inc_cashback) as rev 
FROM fact_core_item
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1)
AND   order_created_date > 20170529
and brand ='Anouk' and seller_id=21
group by 1,2
limit 100;


select seller_id ,count(distinct brand_name) from seller_brand where enabled=1 group by 1;


select a.* from
(select distinct sim.style_id,40 as seller_id from
seller_item_master sim 
join dim_product dp on sim.sku_id=dp.sku_id
join dev.pm_brand_list bl on dp.brand=bl.brand
where seller_id=30 and  enabled=1 ) a
  LEFT JOIN (SELECT style_id,
                    SUM(inventory_count) AS inventory_count,
                    MAX(style_status) AS style_status
             FROM fact_atp_inventory
             GROUP BY 1) fps ON a.style_id = fps.style_id
where nvl(fps.inventory_count,0) = 0 
--and fps.style_status='P'  ;
;

select distinct dp.style_id,40 as new_seller_id
FROM (select distinct sim.sku_id,sim.style_id,sim.seller_id 
			from seller_item_master sim 
			where sim.seller_id=30 and enabled=1 ) dp
  LEFT JOIN (SELECT style_id,
                    SUM(inventory_count) AS inventory_count,
                    MAX(style_status) AS style_status
             FROM fact_atp_inventory
             GROUP BY 1) fps ON dp.style_id = fps.style_id
where fps.style_status!='P' and fps.inventory_count > 0
