CREATE TABLE dev.brand_seller_map
(
	 brand			varchar(255),
   seller_id  varchar(255)
) distkey (brand);

copy dev.brand_seller_map
(
brand,
seller_id
)
from 's3://testashutosh/backups/brand_seller_mapping.csv'
credentials 'aws_access_key_id=AKIAILAAXNRLGZ3GKS2A;aws_secret_access_key=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'
delimiter ',' 
emptyasnull
blanksasnull
TRIMBLANKS
TRUNCATECOLUMNS
ACCEPTANYDATE
ACCEPTINVCHARS AS ' '
maxerror 10 explicit_ids
compupdate off statupdate off;



create table dev.style_migration distkey(style_id) sortkey(brand) as
(select style_id,c.brand,old_seller,seller_id as new_seller from
(select distinct a.style_id,seller_id as old_seller,brand 
from seller_item_master a
left join dim_style b on a.style_id=b.style_id
where enabled=1 and seller_id in (19,    21,    25,    29,    30,    32)) c
left join dev.brand_seller_map d on c.brand=d.brand);

select old_seller,new_seller,select count(distinct style_id) as styles from dev.style_migration where old_seller!=new_seller group by 1,2;

select count(distinct dp.style_id)
FROM dev.style_migration dp
  left JOIN (SELECT style_id,
                    SUM(inventory_count) AS inventory_count,
                    MAX(style_status) AS style_status
             FROM fact_atp_inventory
             GROUP BY 1) fps ON dp.style_id = fps.style_id
where (fps.inventory_count > 0 and fps.style_status ='P') and old_seller!=new_seller
