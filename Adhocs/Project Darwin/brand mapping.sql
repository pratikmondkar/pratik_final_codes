SELECT brand,
       category,
       cat_revenue,
       brand_revenue
FROM (SELECT a.brand,
             category,
             cat_revenue,
             brand_revenue,
             ROW_NUMBER() OVER (PARTITION BY a.brand ORDER BY cat_revenue / brand_revenue::FLOAT DESC) AS rnk
      FROM (SELECT f.brand,
                   CASE
         WHEN dp.business_unit ='Men\'s Casual' and dp.article_type in ('Shirts',' Footwear','Tshirts','Jeans') then concat(dp.business_unit,dp.article_type) 
         WHEN dp.business_unit ='Men\'s Casual' and dp.article_type not in ('Shirts',' Footwear','Tshirts','Jeans') then 'Mens Casual Others'
         WHEN dp.business_unit ='Sports' and dp.article_type in ('Sweatshirts','Jackets','Tshirts','Track Pants','Shorts') then 'sports apparrel'
         when dp.business_unit ='Sports' and dp.article_type = 'Sports Shoes' then 'Sports Shoes'
         when dp.business_unit ='Sports' and dp.article_type not in ('Sweatshirts','Jackets','Tshirts','Track Pants','Shorts','Sports Shoes') then 'Sports others'
         WHEN dp.business_unit ='Women\'s Western Wear' and dp.article_type in ('Tops','Dresses') then 'Women Western wear tops and dresses'
         when dp.business_unit ='Women\'s Western Wear' and dp.article_type not in ('Tops','Dresses') then 'Women Western wear Others'
         else dp.business_unit end as category,
                   SUM(item_revenue_inc_cashback) AS cat_revenue
            FROM fact_core_item f
              LEFT JOIN dim_product dp ON f.sku_id = dp.sku_id
            WHERE store_id = 1
            AND   (is_shipped = 1 OR is_realised = 1)
            AND   order_created_date BETWEEN 20160101 AND 20161130
            GROUP BY 1,
                     2) a
        LEFT JOIN (SELECT brand,
                          SUM(item_revenue_inc_cashback) AS brand_revenue
                   FROM fact_core_item
                   WHERE store_id = 1
                   AND   (is_shipped = 1 OR is_realised = 1)
                   AND   order_created_date BETWEEN 20160101 AND 20161130
                   GROUP BY 1) b ON a.brand = b.brand)
WHERE rnk = 1

