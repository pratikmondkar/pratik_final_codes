SELECT  
CASE
         WHEN dp.business_unit ='Men\'s Casual' and dp.article_type in ('Shirts',' Footwear','Tshirts','Jeans') then concat(dp.business_unit,dp.article_type) 
         WHEN dp.business_unit ='Men\'s Casual' and dp.article_type not in ('Shirts',' Footwear','Tshirts','Jeans') then 'Mens Casual Others'
         WHEN dp.business_unit ='Sports' and dp.article_type in ('Sweatshirts','Jackets','Tshirts','Track Pants','Shorts') then 'sports apparrel'
         when dp.business_unit ='Sports' and dp.article_type = 'Sports Shoes' then 'Sports Shoes'
         when dp.business_unit ='Sports' and dp.article_type not in ('Sweatshirts','Jackets','Tshirts','Track Pants','Shorts','Sports Shoes') then 'Sports others'
         WHEN dp.business_unit ='Women\'s Western Wear' and dp.article_type in ('Tops','Dresses') then 'Women Western wear tops and dresses'
         when dp.business_unit ='Women\'s Western Wear' and dp.article_type not in ('Tops','Dresses') then 'Women Western wear Others'
         else dp.business_unit end as category,
count(distinct order_id) as shipments,
sum(item_revenue_inc_cashback) as revenue
from fact_core_item fci 
join dim_product dp
on fci.sku_id=dp.sku_id
where store_id=1 and (is_shipped=1 or is_realised=1) and order_created_date between 20151201 and 20160331
group by 1
