select count(distinct order_group_id)as total_orders,
count(distinct case when booked_item_count>1 then order_group_id end ) as multi_shipment_orders,
count(distinct case when booked_item_count>1 and shipping_charges>0 then order_group_id end ) as shipping_charges_orders,
sum(case when booked_item_count>1 and shipping_charges>0 then shipping_charges end ) as shipping_charges_orders,
count(distinct case when booked_item_count>1 and shipping_charges>0 and shipped_item_count<booked_item_count and shipped_item_count>0 then order_group_id end ) as partial_cancellation_order,
sum(case when booked_item_count>1 and shipping_charges>0 and shipped_item_count<booked_item_count and shipped_item_count>0 then shipping_charges end ) as partial_cancellation_o
from
(SELECT order_group_id,
			 sum(booked_item_count) as booked_item_count,
			 sum(shipped_item_count) as shipped_item_count,
       SUM(shipping_charges) AS shipping_charges
FROM fact_order fo
WHERE store_id = 1
AND   (is_shipped = 1 OR is_realised = 1) AND order_created_date BETWEEN 20160701 AND 20160930
GROUP BY 1);


select count(distinct order_id), count(distinct case when qty=1 then order_id end) as single_shipments
from 
(select order_id,sum(quantity) as qty
from fact_core_item 
where (is_shipped = 1 OR is_realised = 1)and store_id=1 AND order_created_date BETWEEN 20160701 AND 20160930
group by 1)

