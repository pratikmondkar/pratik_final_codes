SELECT dp.sku_id,
       CASE
         WHEN seller = '1' THEN 21
         WHEN seller = '2' THEN 25
         WHEN seller = '3' THEN 19
         ELSE 29
       END AS seller,
       nvl(fps.style_status,'NA') style_status,
       nvl(fps.inventory_count,0) inventory_count
FROM (SELECT DISTINCT sku_id, brand FROM dim_product) dp
  LEFT JOIN (SELECT sku_id,
                    SUM(inventory_count) AS inventory_count,
                    MAX(style_status) AS style_status
             FROM fact_atp_inventory
             GROUP BY 1) fps ON dp.sku_id = fps.sku_id
  LEFT JOIN dev.brand_mapping bm
         ON dp.brand = bm.brand
        AND seller <> '#N/A'
where fps.style_status='P' and fps.inventory_count > 0
