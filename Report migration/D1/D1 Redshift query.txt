select fiit.*,dp.style_id,dcb.warehouse_id from
(select *,substring(state_to,10,14) as barcode from fact_inventory_item_transition 
where state_change_date>20150701 and entity_property='bin') fiit
join dim_core_bins dcb on fiit.barcode=dcb.barcode
join fact_inventory_item fii on fiit.core_item_id=fii.core_item_id
join dim_product dp on fii.sku_id=dp.sku_id
where dcb.id in (86958,270943)
limit 100
