/*D81 App Metrics*/
SELECT
-- Overall
to_char(yest_revM,'99,99,99,99,999') AS yday_rev,
to_char(d2_revM,'99,99,99,99,999') AS d2_rev,
to_char(d3_revM,'99,99,99,99,999') AS d3_rev,
to_char(wtd_revM,'99,99,99,99,999') AS wtd_rev,
to_char(mtd_revM,'99,99,99,99,999') AS mtd_rev,
to_char(m1_revM,'99,99,99,99,999') AS m1_rev,
to_char(m2_revM,'99,99,99,99,999') AS m2_rev,

to_char(yest_ordersM,'99,99,99,99,999') AS yday_orders,
to_char(d2_ordersM,'99,99,99,99,999') AS d2_orders,/*D81 App Metrics*/
SELECT
-- Overall
to_char(yest_revM,'99,99,99,99,999') AS yday_rev,
to_char(d2_revM,'99,99,99,99,999') AS d2_rev,
to_char(d3_revM,'99,99,99,99,999') AS d3_rev,
to_char(wtd_revM,'99,99,99,99,999') AS wtd_rev,
to_char(mtd_revM,'99,99,99,99,999') AS mtd_rev,
to_char(m1_revM,'99,99,99,99,999') AS m1_rev,
to_char(m2_revM,'99,99,99,99,999') AS m2_rev,

to_char(yest_ordersM,'99,99,99,99,999') AS yday_orders,
to_char(d2_ordersM,'99,99,99,99,999') AS d2_orders,
to_char(d3_ordersM,'99,99,99,99,999') AS d3_orders,
to_char(wtd_ordersM,'99,99,99,99,999') AS wtd_orders,
to_char(mtd_ordersM,'99,99,99,99,999') AS mtd_orders,
to_char(m1_ordersM,'99,99,99,99,999') AS m1_orders,
to_char(m2_ordersM,'99,99,99,99,999') AS m2_orders,

to_char(yest_revM/yest_ordersM,'99,99,99,99,999') AS yday_asp,
to_char(d2_revM/d2_ordersM,'99,99,99,99,999') AS d2_asp,
to_char(d3_revM/d3_ordersM,'99,99,99,99,999') AS d3_asp,
to_char(wtd_revM/wtd_ordersM,'99,99,99,99,999') AS wtd_asp,
to_char(mtd_revM/mtd_ordersM,'99,99,99,99,999') AS mtd_asp,
to_char(m1_revM/m1_ordersM,'99,99,99,99,999') AS m1_asp,
to_char(m2_revM/m2_ordersM,'99,99,99,99,999') AS m2_asp,

round(yest_ordersM*100/yday_users::decimal(12,2),2) as yday_conv,
round(d2_ordersM*100/d2_users::decimal(12,2),2) as d2_conv,
round(d3_ordersM*100/d3_users::decimal(12,2),2) as d3_conv,
round(wtd_ordersM*100/NULLIF(wtd_users,0)::decimal(12,2),2) as wtd_conv,
round(mtd_ordersM*100/mtd_users::decimal(12,2),2) as mtd_conv,
round(m1_ordersM*100/m1_users::decimal(12,2),2) as m1_conv,
round(m2_ordersM*100/m2_users::decimal(12,2),2) as m2_conv,

to_char(yday_users,'99,99,99,99,999') as yday_users,
to_char(d2_users,'99,99,99,99,999') as d2_users,
to_char(d3_users,'99,99,99,99,999') as d3_users,
to_char(wtd_users,'99,99,99,99,999') as wtd_users,
to_char(mtd_users,'99,99,99,99,999') as mtd_users,
to_char(m1_users,'99,99,99,99,999') as m1_users,
to_char(m2_users,'99,99,99,99,999') as m2_users,

round(yest_custM*100/yday_users::decimal(12,2),2) as yest_cust_users,
round(d2_custM*100/d2_users::decimal(12,2),2) as d2_cust_users,
round(d3_custM*100/d3_users::decimal(12,2),2) as d3_cust_users,
round(wtd_custM*100/NULLIF(wtd_users,0)::decimal(12,2),2) as wtd_cust_users,
round(mtd_custM*100/mtd_users::decimal(12,2),2) as mtd_cust_users,
round(m1_custM*100/m1_users::decimal(12,2),2) as m1_cust_users,
round(m2_custM*100/m2_users::decimal(12,2),2) as m2_cust_users,

to_char(yday_session_time*yday_sessions/(yday_users*60),'99,99,99,99,999') as yest_session_time,
to_char(d2_session_time*d2_sessions/(d2_users*60),'99,99,99,99,999') as d2_session_time,
to_char(d3_session_time*d3_sessions/(d3_users*60),'99,99,99,99,999') as d3_session_time,
to_char(wtd_session_time*wtd_sessions/(NULLIF(wtd_users,0)*60),'99,99,99,99,999') as wtd_session_time,
to_char(mtd_session_time*mtd_sessions/(mtd_users*60),'99,99,99,99,999') as mtd_session_time,
to_char(m1_session_time*m1_sessions/(m1_users*60),'99,99,99,99,999') as m1_session_time,
to_char(m2_session_time*m2_sessions/(m2_users*60),'99,99,99,99,999') as m2_session_time,

/*
to_char(yest_ins,'99,99,99,99,999') AS yday_ins,
to_char(d2_ins,'99,99,99,99,999') AS d2_ins,
to_char(d3_ins,'99,99,99,99,999') AS d3_ins,
to_char(wtd_ins,'99,99,99,99,999') AS wtd_ins,
to_char(mtd_ins,'99,99,99,99,999') AS mtd_ins,
to_char(m1_ins,'99,99,99,99,999') AS m1_ins,
to_char(m2_ins,'99,99,99,99,999') AS m2_ins,*/

to_char(yest_acqM,'99,99,99,99,999') AS yday_acq,
to_char(d2_acqM,'99,99,99,99,999') AS d2_acq,
to_char(d3_acqM,'99,99,99,99,999') AS d3_acq,
to_char(wtd_acqM,'99,99,99,99,999') AS wtd_acq,
to_char(mtd_acqM,'99,99,99,99,999') AS mtd_acq,
to_char(m1_acqM,'99,99,99,99,999') AS m1_acq,
to_char(m2_acqM,'99,99,99,99,999') AS m2_acq,

to_char(yest_acqM_rev,'99,99,99,99,999') AS yday_acq_rev,
to_char(d2_acqM_rev,'99,99,99,99,999') AS d2_acq_rev,
to_char(d3_acqM_rev,'99,99,99,99,999') AS d3_acq_rev,
to_char(wtd_acqM_rev,'99,99,99,99,999') AS wtd_acq_rev,
to_char(mtd_acqM_rev,'99,99,99,99,999') AS mtd_acq_rev,
to_char(m1_acqM_rev,'99,99,99,99,999') AS m1_acq_rev,
to_char(m2_acqM_rev,'99,99,99,99,999') AS m2_acq_rev,

to_char(yest_mig,'99,99,99,99,999') AS yday_mig,
to_char(d2_mig,'99,99,99,99,999') AS d2_mig,
to_char(d3_mig,'99,99,99,99,999') AS d3_mig,
to_char(wtd_mig,'99,99,99,99,999') AS wtd_mig,
to_char(mtd_mig,'99,99,99,99,999') AS mtd_mig,
to_char(m1_mig,'99,99,99,99,999') AS m1_mig,
to_char(m2_mig,'99,99,99,99,999') AS m2_mig,

-- Android
to_char(yest_revMand,'99,99,99,99,999') AS yday_and_rev,
ROUND(yest_revMand*100/yest_revM,2) as a_rev_per,
to_char(d2_revMand,'99,99,99,99,999') AS d2_and_rev,
ROUND(d2_revMand*100/d2_revM,2) as a2,
to_char(d3_revMand,'99,99,99,99,999') AS d3_and_rev,
ROUND(d3_revMand*100/d3_revM,2) as a3,
to_char(wtd_revMand,'99,99,99,99,999') AS wtd_and_rev,
ROUND(wtd_revMand*100/wtd_revM,2) as a4,
to_char(mtd_revMand,'99,99,99,99,999') AS mtd_and_rev,
ROUND(mtd_revMand*100/mtd_revM,2) as a5,
to_char(m1_revMand,'99,99,99,99,999') AS m1_and_rev,
ROUND(m1_revMand*100/m1_revM,2) as a6,
to_char(m2_revMand,'99,99,99,99,999') AS m2_and_rev,
ROUND(m2_revMand*100/m2_revM,2) as a7,

to_char(yest_ordersMand,'99,99,99,99,999') AS yday_and_orders,
ROUND(yest_ordersMand*100/yest_ordersM::decimal(12,2),2) a_ord_per,

to_char(d2_ordersMand,'99,99,99,99,999') AS d2_and_orders,
ROUND(d2_ordersMand*100/d2_ordersM,2) as a9,

to_char(d3_ordersMand,'99,99,99,99,999') AS d3_and_orders,
ROUND(d3_ordersMand*100/d3_ordersM,2) as a10,

to_char(wtd_ordersMand,'99,99,99,99,999') AS wtd_and_orders,
ROUND(wtd_ordersMand*100/wtd_ordersM,2) as a11,

to_char(mtd_ordersMand,'99,99,99,99,999') AS mtd_and_orders,
ROUND(mtd_ordersMand*100/mtd_ordersM,2) as a12,

to_char(m1_ordersMand,'99,99,99,99,999') AS m1_and_orders,
ROUND(m1_ordersMand*100/m1_ordersM,2) as a13,

to_char(m2_ordersMand,'99,99,99,99,999') AS m2_and_orders,
ROUND(m2_ordersMand*100/m2_ordersM,2) as a14,

to_char(yest_revMand/yest_ordersMand,'99,99,99,99,999') AS yday_and_asp,
to_char(d2_revMand/d2_ordersMand,'99,99,99,99,999') AS d2_and_asp,
to_char(d3_revMand/d3_ordersMand,'99,99,99,99,999') AS d3_and_asp,
to_char(wtd_revMand/wtd_ordersMand,'99,99,99,99,999') AS wtd_and_asp,
to_char(mtd_revMand/mtd_ordersMand,'99,99,99,99,999') AS mtd_and_asp,
to_char(m1_revMand/m1_ordersMand,'99,99,99,99,999') AS m1_and_asp,
to_char(m2_revMand/m2_ordersMand,'99,99,99,99,999') AS m2_and_asp,

round(yest_ordersMand*100/yday_users_and::decimal(12,2),2) as yday_conv_and,
round(d2_ordersMand*100/d2_users_and::decimal(12,2),2) as d2_conv_and,
round(d3_ordersMand*100/d3_users_and::decimal(12,2),2) as d3_conv_and,
round(wtd_ordersMand*100/NULLIF(wtd_users_and,0)::decimal(12,2),2) as wtd_conv_and,
round(mtd_ordersMand*100/mtd_users_and::decimal(12,2),2) as mtd_conv_and,
round(m1_ordersMand*100/m1_users_and::decimal(12,2),2) as m1_conv_and,
round(m2_ordersMand*100/m2_users_and::decimal(12,2),2) as m2_conv_and,

to_char(yday_users_and,'99,99,99,99,999') as yday_users_and,
ROUND(yday_users_and*100/yday_users::decimal(12,2),2) as a_user_per,

to_char(d2_users_and,'99,99,99,99,999') as d2_users_and,
to_char(d3_users_and,'99,99,99,99,999') as d3_users_and,
to_char(wtd_users_and,'99,99,99,99,999') as wtd_users_and,
to_char(mtd_users_and,'99,99,99,99,999') as mtd_users_and,
to_char(m1_users_and,'99,99,99,99,999') as m1_users_and,
to_char(m2_users_and,'99,99,99,99,999') as m2_users_and,


round(yest_custMand*100/yday_users_and::decimal(12,2),2) as yest_cu_and,
round(d2_custMand*100/d2_users_and::decimal(12,2),2) as d2_cu_and,
round(d3_custMand*100/d3_users_and::decimal(12,2),2) as d3_cu_and,
round(wtd_custMand*100/NULLIF(wtd_users_and,0)::decimal(12,2),2) as wtd_cu_and,
round(mtd_custMand*100/mtd_users_and::decimal(12,2),2) as mtd_cu_and,
round(m1_custMand*100/m1_users_and::decimal(12,2),2) as m1_cu_and,
round(m2_custMand*100/m2_users_and::decimal(12,2),2) as m2_cu_and,

to_char(yday_session_time_and*yday_sessions_and/(yday_users_and*60),'99,99,99,99,999') as yest_session_time_and,
to_char(d2_session_time_and*d2_sessions_and/(d2_users_and*60),'99,99,99,99,999') as d2_session_time_and,
to_char(d3_session_time_and*d3_sessions_and/(d3_users_and*60),'99,99,99,99,999') as d3_session_time_and,
to_char(wtd_session_time_and*wtd_sessions_and/(NULLIF(wtd_users_and,0)*60),'99,99,99,99,999') as wtd_session_time_and,
to_char(mtd_session_time_and*mtd_sessions_and/(mtd_users_and*60),'99,99,99,99,999') as mtd_session_time_and,
to_char(m1_session_time_and*m1_sessions_and/(m1_users_and*60),'99,99,99,99,999') as m1_session_time_and,
to_char(m2_session_time_and*m2_sessions_and/(m2_users_and*60),'99,99,99,99,999') as m2_session_time_and,
/*
to_char(yest_ins_and,'99,99,99,99,999') AS yday_ins_and,
to_char(d2_ins_and,'99,99,99,99,999') AS d2_ins_and,
to_char(d3_ins_and,'99,99,99,99,999') AS d3_ins_and,
to_char(wtd_ins_and,'99,99,99,99,999') AS wtd_ins_and,
to_char(mtd_ins_and,'99,99,99,99,999') AS mtd_ins_and,
to_char(m1_ins_and,'99,99,99,99,999') AS m1_ins_and,
to_char(m2_ins_and,'99,99,99,99,999') AS m2_ins_and,*/

to_char(yest_acqMand,'99,99,99,99,999') AS yday_acq_and,
ROUND(yest_acqMand*100/yest_acqM::decimal(12,2),2) as a_acq_per,

to_char(d2_acqMand,'99,99,99,99,999') AS d2_acq_and,
to_char(d3_acqMand,'99,99,99,99,999') AS d3_acq_and,
to_char(wtd_acqMand,'99,99,99,99,999') AS wtd_acq_and,
to_char(mtd_acqMand,'99,99,99,99,999') AS mtd_acq_and,
to_char(m1_acqMand,'99,99,99,99,999') AS m1_acq_and,
to_char(m2_acqMand,'99,99,99,99,999') AS m2_acq_and,

to_char(yest_acqM_rev_and,'99,99,99,99,999') AS yday_acq_rev_and,
to_char(d2_acqM_rev_and,'99,99,99,99,999') AS d2_acq_rev_and,
to_char(d3_acqM_rev_and,'99,99,99,99,999') AS d3_acq_rev_and,
to_char(wtd_acqM_rev_and,'99,99,99,99,999') AS wtd_acq_rev_and,
to_char(mtd_acqM_rev_and,'99,99,99,99,999') AS mtd_acq_rev_and,
to_char(m1_acqM_rev_and,'99,99,99,99,999') AS m1_acq_rev_and,
to_char(m2_acqM_rev_and,'99,99,99,99,999') AS m2_acq_rev_and,

to_char(yest_mig_and,'99,99,99,99,999') AS yday_mig_and,
to_char(d2_mig_and,'99,99,99,99,999') AS d2_mig_and,
to_char(d3_mig_and,'99,99,99,99,999') AS d3_mig_and,
to_char(wtd_mig_and,'99,99,99,99,999') AS wtd_mig_and,
to_char(mtd_mig_and,'99,99,99,99,999') AS mtd_mig_and,
to_char(m1_mig_and,'99,99,99,99,999') AS m1_mig_and,
to_char(m2_mig_and,'99,99,99,99,999') AS m2_mig_and,

-- iOS
to_char(yest_revMiOS,'99,99,99,99,999') AS yday_iOS_rev,
ROUND(yest_revMiOS*100/yest_revM,2) as i_rev_per,

to_char(d2_revMiOS,'99,99,99,99,999') AS d2_iOS_rev,
ROUND(d2_revMiOS*100/d2_revM,2) as i2,

to_char(d3_revMiOS,'99,99,99,99,999') AS d3_iOS_rev,
ROUND(d3_revMiOS*100/d3_revM,2) as i3,

to_char(wtd_revMiOS,'99,99,99,99,999') AS wtd_iOS_rev,
ROUND(wtd_revMiOS*100/wtd_revM,2) as i4,

to_char(mtd_revMiOS,'99,99,99,99,999') AS mtd_iOS_rev,
ROUND(mtd_revMiOS*100/mtd_revM,2) as i5,

to_char(m1_revMiOS,'99,99,99,99,999') AS m1_iOS_rev,
ROUND(m1_revMiOS*100/m1_revM,2) as i6,

to_char(m2_revMiOS,'99,99,99,99,999') AS m2_iOS_rev,
ROUND(m2_revMiOS*100/m1_revM,2) as i7,

to_char(yest_ordersMiOS,'99,99,99,99,999') AS yday_iOS_orders,
ROUND(yest_ordersMiOS*100/yest_ordersM::decimal(12,2),2) as i_ord_per,

to_char(d2_ordersMiOS,'99,99,99,99,999') AS d2_iOS_orders,
ROUND(d2_ordersMiOS*100/d2_ordersM,2) as i9,

to_char(d3_ordersMiOS,'99,99,99,99,999') AS d3_iOS_orders,
ROUND(d3_ordersMiOS*100/d3_ordersM,2) as i10,

to_char(wtd_ordersMiOS,'99,99,99,99,999') AS wtd_iOS_orders,
ROUND(wtd_ordersMiOS*100/wtd_ordersM,2) as i11,

to_char(mtd_ordersMiOS,'99,99,99,99,999') AS mtd_iOS_orders,
ROUND(mtd_ordersMiOS*100/mtd_ordersM,2) as i12,

to_char(m1_ordersMiOS,'99,99,99,99,999') AS m1_iOS_orders,
ROUND(m1_ordersMiOS*100/m1_ordersM,2) as i13,

to_char(m2_ordersMiOS,'99,99,99,99,999') AS m2_iOS_orders,
ROUND(m2_ordersMiOS*100/m2_ordersM,2) as i14,

to_char(yest_revMiOS/yest_ordersMiOS,'99,99,99,99,999') AS yday_iOS_asp,
to_char(d2_revMiOS/d2_ordersMiOS,'99,99,99,99,999') AS d2_iOS_asp,
to_char(d3_revMiOS/d3_ordersMiOS,'99,99,99,99,999') AS d3_iOS_asp,
to_char(wtd_revMiOS/wtd_ordersMiOS,'99,99,99,99,999') AS wtd_iOS_asp,
to_char(mtd_revMiOS/mtd_ordersMiOS,'99,99,99,99,999') AS mtd_iOS_asp,
to_char(m1_revMiOS/m1_ordersMiOS,'99,99,99,99,999') AS m1_iOS_asp,
to_char(m2_revMiOS/m2_ordersMiOS,'99,99,99,99,999') AS m2_iOS_asp,

round(yest_ordersMiOS*100/yday_users_iOS::decimal(12,2),2) as yday_conv_iOS,
round(d2_ordersMiOS*100/d2_users_iOS::decimal(12,2),2) as d2_conv_iOS,
round(d3_ordersMiOS*100/d3_users_iOS::decimal(12,2),2) as d3_conv_iOS,
round(wtd_ordersMiOS*100/NULLIF(wtd_users_iOS,0)::decimal(12,2),2) as wtd_conv_iOS,
round(mtd_ordersMiOS*100/mtd_users_iOS::decimal(12,2),2) as mtd_conv_iOS,
round(m1_ordersMiOS*100/m1_users_iOS::decimal(12,2),2) as m1_conv_iOS,
round(m2_ordersMiOS*100/m2_users_iOS::decimal(12,2),2) as m2_conv_iOS,
 
to_char(yday_users_iOS,'99,99,99,99,999') AS yday_users_iOS,
ROUND(yday_users_iOS*100/yday_users::decimal(12,2),2) as i_user_per,

to_char(d2_users_iOS,'99,99,99,99,999') AS d2_users_iOS,
to_char(d3_users_iOS,'99,99,99,99,999') AS d3_users_iOS,
to_char(wtd_users_iOS,'99,99,99,99,999') AS wtd_users_iOS,
to_char(mtd_users_iOS,'99,99,99,99,999') AS mtd_users_iOS,
to_char(m1_users_iOS,'99,99,99,99,999') AS m1_users_iOS,
to_char(m2_users_iOS,'99,99,99,99,999') AS m2_users_iOS,

to_char(yest_custMios,'99,99,99,99,999') as yest_cust_ios,
to_char(d2_custMios,'99,99,99,99,999') as d2_cust_ios,
to_char(d3_custMios,'99,99,99,99,999') as d3_cust_ios,
to_char(wtd_custMios,'99,99,99,99,999') as wtd_cust_ios,
to_char(mtd_custMios,'99,99,99,99,999') as mtd_cust_ios,
to_char(m1_custMios,'99,99,99,99,999') as m1_cust_ios,
to_char(m2_custMios,'99,99,99,99,999') as m2_cust_ios,

round(yest_custMios*100/yday_users_ios::decimal(12,2),2) as yest_cu_ios,
round(d2_custMios*100/d2_users_ios::decimal(12,2),2) as d2_cu_ios,
round(d3_custMios*100/d3_users_ios::decimal(12,2),2) as d3_cu_ios,
round(wtd_custMios*100/NULLIF(wtd_users_iOS,0)::decimal(12,2),2) as wtd_cu_ios,
round(mtd_custMios*100/mtd_users_ios::decimal(12,2),2) as mtd_cu_ios,
round(m1_custMios*100/m1_users_ios::decimal(12,2),2) as m1_cu_ios,
round(m2_custMios*100/m2_users_ios::decimal(12,2),2) as m2_cu_ios,

to_char(yday_session_time_ios*yday_sessions_ios/(yday_users_ios*60),'99,99,99,99,999') as yest_session_time_ios,
to_char(d2_session_time_ios*d2_sessions_ios/(d2_users_ios*60),'99,99,99,99,999') as d2_session_time_ios,
to_char(d3_session_time_ios*d3_sessions_ios/(d3_users_ios*60),'99,99,99,99,999') as d3_session_time_ios,
to_char(wtd_session_time_ios*wtd_sessions_ios/(NULLIF(wtd_users_iOS,0)*60),'99,99,99,99,999') as wtd_session_time_ios,
to_char(mtd_session_time_ios*mtd_sessions_ios/(mtd_users_ios*60),'99,99,99,99,999') as mtd_session_time_ios,
to_char(m1_session_time_ios*m1_sessions_ios/(m1_users_ios*60),'99,99,99,99,999') as m1_session_time_ios,
to_char(m2_session_time_ios*m2_sessions_ios/(m2_users_ios*60),'99,99,99,99,999') as m2_session_time_ios,
/*
to_char(yest_ins_ios,'99,99,99,99,999') AS yday_ins_ios,
to_char(d2_ins_ios,'99,99,99,99,999') AS d2_ins_ios,
to_char(d3_ins_ios,'99,99,99,99,999') AS d3_ins_ios,
to_char(wtd_ins_ios,'99,99,99,99,999') AS wtd_ins_ios,
to_char(mtd_ins_ios,'99,99,99,99,999') AS mtd_ins_ios,
to_char(m1_ins_ios,'99,99,99,99,999') AS m1_ins_ios,
to_char(m2_ins_ios,'99,99,99,99,999') AS m2_ins_ios,*/

to_char(yest_acqMios,'99,99,99,99,999') AS yday_acq_ios,
ROUND(yest_acqMios*100/yest_acqM::decimal(12,2),2) as i_acq_per,

to_char(d2_acqMios,'99,99,99,99,999') AS d2_acq_ios,
to_char(d3_acqMios,'99,99,99,99,999') AS d3_acq_ios,
to_char(wtd_acqMios,'99,99,99,99,999') AS wtd_acq_ios,
to_char(mtd_acqMios,'99,99,99,99,999') AS mtd_acq_ios,
to_char(m1_acqMios,'99,99,99,99,999') AS m1_acq_ios,
to_char(m2_acqMios,'99,99,99,99,999') AS m2_acq_ios,

to_char(yest_acqM_rev_ios,'99,99,99,99,999') AS yday_acq_rev_ios,
to_char(d2_acqM_rev_ios,'99,99,99,99,999') AS d2_acq_rev_ios,
to_char(d3_acqM_rev_ios,'99,99,99,99,999') AS d3_acq_rev_ios,
to_char(wtd_acqM_rev_ios,'99,99,99,99,999') AS wtd_acq_rev_ios,
to_char(mtd_acqM_rev_ios,'99,99,99,99,999') AS mtd_acq_rev_ios,
to_char(m1_acqM_rev_ios,'99,99,99,99,999') AS m1_acq_rev_ios,
to_char(m2_acqM_rev_ios,'99,99,99,99,999') AS m2_acq_rev_ios,

to_char(yest_mig_ios,'99,99,99,99,999') AS yday_mig_ios,
to_char(d2_mig_ios,'99,99,99,99,999') AS d2_mig_ios,
to_char(d3_mig_ios,'99,99,99,99,999') AS d3_mig_ios,
to_char(wtd_mig_ios,'99,99,99,99,999') AS wtd_mig_ios,
to_char(mtd_mig_ios,'99,99,99,99,999') AS mtd_mig_ios,
to_char(m1_mig_ios,'99,99,99,99,999') AS m1_mig_ios,
to_char(m2_mig_ios,'99,99,99,99,999') AS m2_mig_ios,
 
-- Windows
to_char(yest_revMwin,'99,99,99,99,999') AS yday_win_rev,
ROUND(yest_revMwin*100/yest_revM,2) as w_rev_per,

to_char(d2_revMwin,'99,99,99,99,999') AS d2_win_rev,
ROUND(d2_revMwin*100/d2_revM,2) as w2,

to_char(d3_revMwin,'99,99,99,99,999') AS d3_win_rev,
ROUND(d3_revMwin*100/d2_revM,2) as w3,

to_char(wtd_revMwin,'99,99,99,99,999') AS wtd_win_rev,
ROUND(wtd_revMwin*100/wtd_revM,2) as w4,

to_char(mtd_revMwin,'99,99,99,99,999') AS mtd_win_rev,
ROUND(mtd_revMwin*100/mtd_revM,2) as w5,

to_char(m1_revMwin,'99,99,99,99,999') AS m1_win_rev,
ROUND(m1_revMwin*100/m1_revM,2) as w6,

to_char(m2_revMwin,'99,99,99,99,999') AS m2_win_rev,
ROUND(m2_revMwin*100/m2_revM,2) as w7,

to_char(yest_ordersMwin,'99,99,99,99,999') AS yday_win_orders,
ROUND(yest_ordersMwin*100/yest_ordersM::decimal(12,2),2)  as w_ord_per,

to_char(d2_ordersMwin,'99,99,99,99,999') AS d2_win_orders,
ROUND(d2_ordersMwin*100/d2_ordersM,2) as w9,

to_char(d3_ordersMwin,'99,99,99,99,999') AS d3_win_orders,
 ROUND(d3_ordersMwin*100/d3_ordersM,2) as w10,

to_char(wtd_ordersMwin,'99,99,99,99,999') AS wtd_win_orders,
ROUND(wtd_ordersMwin*100/wtd_ordersM,2) as w11,

to_char(mtd_ordersMwin,'99,99,99,99,999') AS mtd_win_orders,
ROUND(mtd_ordersMwin*100/mtd_ordersM,2) as w12,

to_char(m1_ordersMwin,'99,99,99,99,999') AS m1_win_orders,
ROUND(m1_ordersMwin*100/m1_ordersM,2) as w13,

to_char(m2_ordersMwin,'99,99,99,99,999') AS m2_win_orders,
ROUND(m2_ordersMwin*100/m1_ordersM,2) as w14,

to_char(yest_revMwin/yest_ordersMwin,'99,99,99,99,999') AS yday_win_asp,
to_char(d2_revMwin/d2_ordersMwin,'99,99,99,99,999') AS d2_win_asp,
to_char(d3_revMwin/d3_ordersMwin,'99,99,99,99,999') AS d3_win_asp,
to_char(wtd_revMwin/wtd_ordersMwin,'99,99,99,99,999') AS wtd_win_asp,
to_char(mtd_revMwin/mtd_ordersMwin,'99,99,99,99,999') AS mtd_win_asp,
to_char(m1_revMwin/m1_ordersMwin,'99,99,99,99,999') AS m1_win_asp,
to_char(m2_revMwin/m2_ordersMwin,'99,99,99,99,999') AS m2_win_asp,

round(yest_ordersMwin*100/yday_users_Windows::decimal(12,2),2) as yday_conv_win,
round(d2_ordersMwin*100/d2_users_Windows::decimal(12,2),2) as d2_conv_win,
round(d3_ordersMwin*100/d3_users_Windows::decimal(12,2),2) as d3_conv_win,
round(wtd_ordersMwin*100/NULLIF(wtd_users_Windows,0)::decimal(12,2),2) as wtd_conv_win,
round(mtd_ordersMwin*100/mtd_users_Windows::decimal(12,2),2) as mtd_conv_win,
round(m1_ordersMwin*100/m1_users_Windows::decimal(12,2),2) as m1_conv_win,
round(m2_ordersMwin*100/m2_users_Windows::decimal(12,2),2) as m2_conv_win,

to_char(yday_users_Windows,'99,99,99,99,999') AS yday_users_Windows,
ROUND(yday_users_Windows*100/yday_users::decimal(12,2),2) as w_user_per,

to_char(d2_users_Windows,'99,99,99,99,999') AS d2_users_Windows,
to_char(d3_users_Windows,'99,99,99,99,999') AS d3_users_Windows,
to_char(wtd_users_Windows,'99,99,99,99,999') AS wtd_users_Windows,
to_char(mtd_users_Windows,'99,99,99,99,999') AS mtd_users_Windows,
to_char(m1_users_Windows,'99,99,99,99,999') as m1_users_win,
to_char(m2_users_Windows,'99,99,99,99,999') as m2_users_win,

to_char(yest_custMwin,'99,99,99,99,999') as yest_cust_win,
to_char(d2_custMwin,'99,99,99,99,999') as d2_cust_win,
to_char(d3_custMwin,'99,99,99,99,999') as d3_cust_win,
to_char(wtd_custMwin,'99,99,99,99,999') as wtd_cust_win,
to_char(mtd_custMwin,'99,99,99,99,999') as mtd_cust_win,
to_char(m1_custMwin,'99,99,99,99,999') as m1_cust_win,
to_char(m2_custMwin,'99,99,99,99,999') as m2_cust_win,
 
round(yest_custMwin*100/yday_users_Windows::decimal(12,2),2) as yest_cu_win,
round(d2_custMwin*100/d2_users_windows::decimal(12,2),2) as d2_cu_win,
round(d3_custMwin*100/d3_users_windows::decimal(12,2),2) as d3_cu_win,
round(wtd_custMwin*100/NULLIF(wtd_users_Windows,0)::decimal(12,2),2) as wtd_cu_win,
round(mtd_custMwin*100/mtd_users_windows::decimal(12,2),2) as mtd_cu_win,
round(m1_custMwin*100/m1_users_windows::decimal(12,2),2) as m1_cu_win,
round(m2_custMwin*100/m2_users_windows::decimal(12,2),2) as m2_cu_win,

to_char(yday_session_time_windows*yday_sessions_windows/(yday_users_windows*60),'99,99,99,99,999') as yest_session_time_windows,
to_char(d2_session_time_windows*d2_sessions_windows/(d2_users_windows*60),'99,99,99,99,999') as d2_session_time_windows,
to_char(d3_session_time_windows*d3_sessions_windows/(d3_users_windows*60),'99,99,99,99,999') as d3_session_time_windows,
to_char(wtd_session_time_windows*wtd_sessions_windows/(NULLIF(wtd_users_Windows,0)*60),'99,99,99,99,999') as wtd_session_time_windows,
to_char(mtd_session_time_windows*mtd_sessions_windows/(mtd_users_windows*60),'99,99,99,99,999') as mtd_session_time_windows,
to_char(m1_session_time_windows*m1_sessions_windows/(m1_users_windows*60),'99,99,99,99,999') as m1_session_time_windows,
to_char(m2_session_time_windows*m2_sessions_windows/(m2_users_windows*60),'99,99,99,99,999') as m2_session_time_windows,
/*
to_char(yest_ins_win,'99,99,99,99,999') AS yday_ins_win,
to_char(d2_ins_win,'99,99,99,99,999') AS d2_ins_win,
to_char(d3_ins_win,'99,99,99,99,999') AS d3_ins_win,
to_char(wtd_ins_win,'99,99,99,99,999') AS wtd_ins_win,
to_char(mtd_ins_win,'99,99,99,99,999') AS mtd_ins_win,
to_char(m1_ins_win,'99,99,99,99,999') AS m1_ins_win,
to_char(m2_ins_win,'99,99,99,99,999') AS m2_ins_win,*/

to_char(yest_acqMwin,'99,99,99,99,999') AS yday_acq_win,
ROUND(yest_acqMwin*100/yest_acqM::decimal(12,2),2) as w_acq_per,

to_char(d2_acqMwin,'99,99,99,99,999') AS d2_acq_win,
to_char(d3_acqMwin,'99,99,99,99,999') AS d3_acq_win,
to_char(wtd_acqMwin,'99,99,99,99,999') AS wtd_acq_win,
to_char(mtd_acqMwin,'99,99,99,99,999') AS mtd_acq_win,
to_char(m1_acqMwin,'99,99,99,99,999') AS m1_acq_win,
to_char(m2_acqMwin,'99,99,99,99,999') AS m2_acq_win,

to_char(yest_acqM_rev_win,'99,99,99,99,999') AS yday_acq_rev_win,
to_char(d2_acqM_rev_win,'99,99,99,99,999') AS d2_acq_rev_win,
to_char(d3_acqM_rev_win,'99,99,99,99,999') AS d3_acq_rev_win,
to_char(wtd_acqM_rev_win,'99,99,99,99,999') AS wtd_acq_rev_win,
to_char(mtd_acqM_rev_win,'99,99,99,99,999') AS mtd_acq_rev_win,
to_char(m1_acqM_rev_win,'99,99,99,99,999') AS m1_acq_rev_win,
to_char(m2_acqM_rev_win,'99,99,99,99,999') AS m2_acq_rev_win,

to_char(yest_mig_win,'99,99,99,99,999') AS yday_mig_win,
to_char(d2_mig_win,'99,99,99,99,999') AS d2_mig_win,
to_char(d3_mig_win,'99,99,99,99,999') AS d3_mig_win,
to_char(wtd_mig_win,'99,99,99,99,999') AS wtd_mig_win,
to_char(mtd_mig_win,'99,99,99,99,999') AS mtd_mig_win,
to_char(m1_mig_win,'99,99,99,99,999') AS m1_mig_win,
to_char(m2_mig_win,'99,99,99,99,999') AS m2_mig_win
 
FROM
(SELECT 
-- All App
ROUND(SUM(CASE WHEN day_diff=1 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS yest_revM,
ROUND(SUM(CASE WHEN day_diff=2 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS d2_revM,
ROUND(SUM(CASE WHEN day_diff=3 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS d3_revM,
ROUND(max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and store_id=1) 
else (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) 
and order_channel='mobile-app' and store_id=1) END),0) AS wtd_revM,
ROUND(SUM(CASE WHEN month_to_date=1 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS mtd_revM,
ROUND(SUM(CASE WHEN month_diff=1 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS m1_revM,
ROUND(SUM(CASE WHEN month_diff=2 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS m2_revM,


COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' THEN order_group_id END) AS yest_ordersM,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' THEN order_group_id END) AS d2_ordersM,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' THEN order_group_id END) AS d3_ordersM,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct order_group_id) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and store_id=1) 
else (select count(distinct order_group_id) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and store_id=1) END) 
AS wtd_ordersM,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' THEN order_group_id END) AS mtd_ordersM,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' THEN order_group_id END) AS m1_ordersM,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' THEN order_group_id END) AS m2_ordersM,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' THEN idcustomer END) AS yest_custM,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' THEN idcustomer END) AS d2_custM,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' THEN idcustomer END) AS d3_custM,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and store_id=1) 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and store_id=1) END) 
AS wtd_custM,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' THEN idcustomer END) AS mtd_custM,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' THEN idcustomer END) AS m1_custM,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' THEN idcustomer END) AS m2_custM,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and purchase_type='f' THEN idcustomer END) AS yest_acqM,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and purchase_type='f' THEN idcustomer END) AS d2_acqM,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and purchase_type='f' THEN idcustomer END) AS d3_acqM,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and purchase_type='f' and store_id=1) 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and purchase_type='f' and store_id=1) END) 
AS wtd_acqM,
COUNT( CASE WHEN month_to_date=1 AND order_channel='mobile-app' and purchase_type='f' THEN idcustomer END) AS mtd_acqM,
COUNT( CASE WHEN month_diff=1 AND order_channel='mobile-app' and purchase_type='f' THEN idcustomer END) AS m1_acqM,
COUNT( CASE WHEN month_diff=2 AND order_channel='mobile-app' and purchase_type='f' THEN idcustomer END) AS m2_acqM,

SUM( CASE WHEN day_diff=1 AND order_channel='mobile-app' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS yest_acqM_rev,
SUM( CASE WHEN day_diff=2 AND order_channel='mobile-app' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d2_acqM_rev,
SUM( CASE WHEN day_diff=3 AND order_channel='mobile-app' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d3_acqM_rev,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and purchase_type='f' and store_id=1) 
else (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and purchase_type='f' and store_id=1) END) 
AS wtd_acqM_rev,
SUM( CASE WHEN month_to_date=1 AND order_channel='mobile-app' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS mtd_acqM_rev,
SUM( CASE WHEN month_diff=1 AND order_channel='mobile-app' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m1_acqM_rev,
SUM( CASE WHEN month_diff=2 AND order_channel='mobile-app' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m2_acqM_rev,


-- Android
ROUND(SUM(CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS yest_revMand,
ROUND(SUM(CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS d2_revMand,
ROUND(SUM(CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS d3_revMand,
ROUND(max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android' and store_id=1) 
else (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) 
and order_channel='mobile-app' and browser_info='Android' and store_id=1) END),0) AS wtd_revMand,
ROUND(SUM(CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS mtd_revMand,
ROUND(SUM(CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS m1_revMand,
ROUND(SUM(CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS m2_revMand,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS yest_ordersMand,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS d2_ordersMand,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS d3_ordersMand,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct order_group_id) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android' and store_id=1) 
else (select count(distinct order_group_id) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android' and store_id=1) END)
AS wtd_ordersMand,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS mtd_ordersMand,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS m1_ordersMand,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS m2_ordersMand,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN idcustomer END) AS yest_custMand,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN idcustomer END) AS d2_custMand,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Android' THEN idcustomer END) AS d3_custMand,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android' and store_id=1) 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android' and store_id=1) END) 
AS wtd_custMand,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Android' THEN idcustomer END) AS mtd_custMand,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN idcustomer END) AS m1_custMand,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN idcustomer END) AS m2_custMand,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN idcustomer END) AS yest_acqMand,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN idcustomer END) AS d2_acqMand,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN idcustomer END) AS d3_acqMand,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android' and purchase_type='f' and store_id=1) 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android' and purchase_type='f' and store_id=1) END) 
AS wtd_acqMand,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN idcustomer END) AS mtd_acqMand,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN idcustomer END) AS m1_acqMand,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN idcustomer END) AS m2_acqMand,

SUM( CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS yest_acqM_rev_and,
SUM( CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d2_acqM_rev_and,
SUM( CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d3_acqM_rev_and,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android' and purchase_type='f' and store_id=1) 
else (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android' and purchase_type='f' and store_id=1) END) 
AS wtd_acqM_rev_and,
SUM( CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS mtd_acqM_rev_and,
SUM( CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m1_acqM_rev_and,
SUM( CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m2_acqM_rev_and,

-- iOS
ROUND(SUM(CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS yest_revMiOS,
ROUND(SUM(CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS d2_revMiOS,
ROUND(SUM(CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS d3_revMiOS,
ROUND(max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS' and store_id=1) 
else (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) 
and order_channel='mobile-app' and browser_info='iOS' and store_id=1) END),0) AS wtd_revMiOS,
ROUND(SUM(CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS mtd_revMiOS,
ROUND(SUM(CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS m1_revMiOS,
ROUND(SUM(CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS m2_revMiOS,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS yest_ordersMiOS,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS d2_ordersMiOS,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS d3_ordersMiOS,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct order_group_id) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS' and store_id=1) 
else (select count(distinct order_group_id) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS' and store_id=1) END)
AS wtd_ordersMiOS,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS mtd_ordersMiOS,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS m1_ordersMiOS,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS m2_ordersMiOS,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN idcustomer END) AS yest_custmios,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN idcustomer END) AS d2_custmios,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='iOS' THEN idcustomer END) AS d3_custmios,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS' and store_id=1) 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS' and store_id=1) END) 
AS wtd_custMios,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='iOS' THEN idcustomer END) AS mtd_custmios,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN idcustomer END) AS m1_custmios,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN idcustomer END) AS m2_custmios,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN idcustomer END) AS yest_acqmios,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN idcustomer END) AS d2_acqmios,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN idcustomer END) AS d3_acqmios,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' and store_id=1) 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' and store_id=1) END) 
AS wtd_acqmios,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN idcustomer END) AS mtd_acqmios,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN idcustomer END) AS m1_acqmios,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN idcustomer END) AS m2_acqmios,

SUM( CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS yest_acqM_rev_ios,
SUM( CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d2_acqM_rev_ios,
SUM( CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d3_acqM_rev_ios,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' and store_id=1) 
else (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' and store_id=1) END) 
AS wtd_acqM_rev_ios,
SUM( CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS mtd_acqM_rev_ios,
SUM( CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m1_acqM_rev_ios,
SUM( CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m2_acqM_rev_ios,


-- Windows
ROUND(SUM(CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS yest_revMwin,
ROUND(SUM(CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS d2_revMwin,
ROUND(SUM(CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS d3_revMwin,
ROUND(max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows' and store_id=1) 
else (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) 
and order_channel='mobile-app' and browser_info='Windows' and store_id=1) END),0) AS wtd_revMwin,
ROUND(SUM(CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS mtd_revMwin,
ROUND(SUM(CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS m1_revMwin,
ROUND(SUM(CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS m2_revMwin,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS yest_ordersMwin,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS d2_ordersMwin,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS d3_ordersMwin,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct order_group_id) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows' and store_id=1) 
else (select count(distinct order_group_id) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows' and store_id=1) END)
AS wtd_ordersMwin,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS mtd_ordersMwin,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS m1_ordersMwin,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS m2_ordersMwin,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN idcustomer END) AS yest_custmwin,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN idcustomer END) AS d2_custmwin,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Windows' THEN idcustomer END) AS d3_custmwin,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows' and store_id=1) 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows' and store_id=1) END) 
AS wtd_custMwin,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Windows' THEN idcustomer END) AS mtd_custmwin,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN idcustomer END) AS m1_custmwin,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN idcustomer END) AS m2_custmwin,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN idcustomer END) AS yest_acqmwin,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN idcustomer END) AS d2_acqmwin,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN idcustomer END) AS d3_acqmwin,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' and store_id=1) 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' and store_id=1) END) 
AS wtd_acqmwin,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN idcustomer END) AS mtd_acqmwin,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN idcustomer END) AS m1_acqmwin,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN idcustomer END) AS m2_acqmwin,

SUM( CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS yest_acqM_rev_win,
SUM( CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d2_acqM_rev_win,
SUM( CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d3_acqM_rev_win,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' and store_id=1) 
else (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' and store_id=1) END) 
AS wtd_acqM_rev_win,
SUM( CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS mtd_acqM_rev_win,
SUM( CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m1_acqM_rev_win,
SUM( CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m2_acqM_rev_win

FROM fact_order, dim_date d
WHERE order_created_date=d.full_date 
AND  d.month_diff<=2
AND store_id=1
AND (is_realised=1 OR is_shipped=1)
) X,

(SELECT
-- overall
SUM(CASE WHEN day_diff=1 THEN users END) AS yday_users,
SUM(CASE WHEN day_diff=2 THEN users END) AS d2_users,
SUM(CASE WHEN day_diff=3 THEN users END) AS d3_users,

AVG(CASE WHEN day_diff=1 THEN avgsessionduration END) AS yday_session_time,
AVG(CASE WHEN day_diff=2 THEN avgsessionduration END) AS d2_session_time,
AVG(CASE WHEN day_diff=3 THEN avgsessionduration END) AS d3_session_time,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select AVG(avgsessionduration) from ga_metrics g,dim_date
		where last7_days=1 AND g.date=full_date) 
else (select AVG(avgsessionduration) from ga_metrics g,dim_date
where week_to_date=1 and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_session_time,
AVG(CASE WHEN month_to_date=1 THEN avgsessionduration END) AS mtd_session_time,
AVG(CASE WHEN month_diff=1 THEN avgsessionduration END) AS m1_session_time,
AVG(CASE WHEN month_diff=2 THEN avgsessionduration END) AS m2_session_time,

SUM(CASE WHEN day_diff=1 THEN sessions END) AS yday_sessions,
SUM(CASE WHEN day_diff=2 THEN sessions END) AS d2_sessions,
SUM(CASE WHEN day_diff=3 THEN sessions END) AS d3_sessions,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(sessions) from ga_metrics g,dim_date
		where last7_days=1 AND g.date=full_date) 
else (select sum(sessions) from ga_metrics g,dim_date
where week_to_date=1 and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_sessions,
SUM(CASE WHEN month_to_date=1 THEN sessions END) AS mtd_sessions,
SUM(CASE WHEN month_diff=1 THEN sessions END) AS m1_sessions,
SUM(CASE WHEN month_diff=2 THEN sessions END) AS m2_sessions,

-- android
SUM(CASE WHEN day_diff=1 and app_platform='android' THEN users END) AS yday_users_and,
SUM(CASE WHEN day_diff=2 and app_platform='android' THEN users END) AS d2_users_and,
SUM(CASE WHEN day_diff=3 and app_platform='android' THEN users END) AS d3_users_and,

AVG(CASE WHEN day_diff=1 and app_platform='android' THEN avgsessionduration END) AS yday_session_time_and,
AVG(CASE WHEN day_diff=2 and app_platform='android' THEN avgsessionduration END) AS d2_session_time_and,
AVG(CASE WHEN day_diff=3 and app_platform='android' THEN avgsessionduration END) AS d3_session_time_and,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select AVG(avgsessionduration) from ga_metrics g,dim_date
		where last7_days=1 and app_platform='android' AND g.date=full_date) 
else (select AVG(avgsessionduration) from ga_metrics g,dim_date
where week_to_date=1 and app_platform='android' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_session_time_and,
AVG(CASE WHEN month_to_date=1 and app_platform='android' THEN avgsessionduration END) AS mtd_session_time_and,
AVG(CASE WHEN month_diff=1 and app_platform='android' THEN avgsessionduration END) AS m1_session_time_and,
AVG(CASE WHEN month_diff=2 and app_platform='android' THEN avgsessionduration END) AS m2_session_time_and,

SUM(CASE WHEN day_diff=1 and app_platform='android' THEN sessions END) AS yday_sessions_and,
SUM(CASE WHEN day_diff=2 and app_platform='android' THEN sessions END) AS d2_sessions_and,
SUM(CASE WHEN day_diff=3 and app_platform='android' THEN sessions END) AS d3_sessions_and,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(sessions) from ga_metrics g,dim_date
		where last7_days=1 and app_platform='android' AND g.date=full_date) 
else (select sum(sessions) from ga_metrics g,dim_date
where week_to_date=1 and app_platform='android' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_sessions_and,
SUM(CASE WHEN month_to_date=1 and app_platform='android' THEN sessions END) AS mtd_sessions_and,
SUM(CASE WHEN month_diff=1 and app_platform='android' THEN sessions END) AS m1_sessions_and,
SUM(CASE WHEN month_diff=2 and app_platform='android' THEN sessions END) AS m2_sessions_and,

-- iOS
SUM(CASE WHEN day_diff=1 and app_platform='iOS' THEN users END) AS yday_users_iOS,
SUM(CASE WHEN day_diff=2 and app_platform='iOS' THEN users END) AS d2_users_iOS,
SUM(CASE WHEN day_diff=3 and app_platform='iOS' THEN users END) AS d3_users_iOS,

AVG(CASE WHEN day_diff=1 and app_platform='iOS' THEN avgsessionduration END) AS yday_session_time_iOS,
AVG(CASE WHEN day_diff=2 and app_platform='iOS' THEN avgsessionduration END) AS d2_session_time_iOS,
AVG(CASE WHEN day_diff=3 and app_platform='iOS' THEN avgsessionduration END) AS d3_session_time_iOS,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select AVG(avgsessionduration) from ga_metrics g,dim_date
		where last7_days=1 and app_platform='iOS' AND g.date=full_date) 
else (select AVG(avgsessionduration) from ga_metrics g,dim_date
where week_to_date=1 and app_platform='iOS' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_session_time_iOS,
AVG(CASE WHEN month_to_date=1 and app_platform='iOS' THEN avgsessionduration END) AS mtd_session_time_iOS,
AVG(CASE WHEN month_diff=1 and app_platform='iOS' THEN avgsessionduration END) AS m1_session_time_iOS,
AVG(CASE WHEN month_diff=2 and app_platform='iOS' THEN avgsessionduration END) AS m2_session_time_iOS,

SUM(CASE WHEN day_diff=1 and app_platform='iOS' THEN sessions END) AS yday_sessions_iOS,
SUM(CASE WHEN day_diff=2 and app_platform='iOS' THEN sessions END) AS d2_sessions_iOS,
SUM(CASE WHEN day_diff=3 and app_platform='iOS' THEN sessions END) AS d3_sessions_iOS,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(sessions) from ga_metrics g,dim_date
		where last7_days=1 and app_platform='iOS' AND g.date=full_date) 
else (select sum(sessions) from ga_metrics g,dim_date
where week_to_date=1 and app_platform='iOS' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_sessions_iOS,
SUM(CASE WHEN month_to_date=1 and app_platform='iOS' THEN sessions END) AS mtd_sessions_iOS,
SUM(CASE WHEN month_diff=1 and app_platform='iOS' THEN sessions END) AS m1_sessions_iOS,
SUM(CASE WHEN month_diff=2 and app_platform='iOS' THEN sessions END) AS m2_sessions_iOS,

-- windows

SUM(CASE WHEN day_diff=1 and app_platform='windows' THEN users END) AS yday_users_windows,
SUM(CASE WHEN day_diff=2 and app_platform='windows' THEN users END) AS d2_users_windows,
SUM(CASE WHEN day_diff=3 and app_platform='windows' THEN users END) AS d3_users_windows,

AVG(CASE WHEN day_diff=1 and app_platform='windows' THEN avgsessionduration END) AS yday_session_time_windows,
AVG(CASE WHEN day_diff=2 and app_platform='windows' THEN avgsessionduration END) AS d2_session_time_windows,
AVG(CASE WHEN day_diff=3 and app_platform='windows' THEN avgsessionduration END) AS d3_session_time_windows,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select AVG(avgsessionduration) from ga_metrics g,dim_date
		where last7_days=1 and app_platform='windows' AND g.date=full_date) 
else (select AVG(avgsessionduration) from ga_metrics g,dim_date
where week_to_date=1 and app_platform='windows' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_session_time_windows,
AVG(CASE WHEN month_to_date=1 and app_platform='windows' THEN avgsessionduration END) AS mtd_session_time_windows,
AVG(CASE WHEN month_diff=1 and app_platform='windows' THEN avgsessionduration END) AS m1_session_time_windows,
AVG(CASE WHEN month_diff=2 and app_platform='windows' THEN avgsessionduration END) AS m2_session_time_windows,

SUM(CASE WHEN day_diff=1 and app_platform='windows' THEN sessions END) AS yday_sessions_Windows,
SUM(CASE WHEN day_diff=2 and app_platform='windows' THEN sessions END) AS d2_sessions_Windows,
SUM(CASE WHEN day_diff=3 and app_platform='windows' THEN sessions END) AS d3_sessions_Windows,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(sessions) from ga_metrics g,dim_date
		where last7_days=1 and app_platform='windows' AND g.date=full_date) 
else (select sum(sessions) from ga_metrics g,dim_date
where week_to_date=1 and app_platform='windows' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_sessions_Windows,
SUM(CASE WHEN month_to_date=1 and app_platform='windows' THEN sessions END) AS mtd_sessions_Windows,
SUM(CASE WHEN month_diff=1 and app_platform='windows' THEN sessions END) AS m1_sessions_Windows,
SUM(CASE WHEN month_diff=2 and app_platform='windows' THEN sessions END) AS m2_sessions_Windows


FROM ga_metrics ga, dim_date d
WHERE
ga.date=d.full_date and
d.month_diff<=2
and app_platform in ('android','iOS','windows')
) Y ,

(select
 -- All App
SUM(CASE WHEN day_diff=1 THEN migrations END) AS yest_mig,
SUM(CASE WHEN day_diff=2 THEN migrations END) AS d2_mig,
SUM(CASE WHEN day_diff=3 THEN migrations END) AS d3_mig,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and dd.last7_days=1 and store_id=1) 
else (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and dd.week_to_date=1 and store_id=1)
		END) AS wtd_mig,
SUM(CASE WHEN month_to_date=1 THEN migrations END) AS mtd_mig,
SUM(CASE WHEN month_diff=1 THEN migrations END) AS m1_mig,
SUM(CASE WHEN month_diff=2 THEN migrations END) AS m2_mig,

-- Android
SUM(CASE WHEN day_diff=1 and browser_info='Android' THEN migrations END) AS yest_mig_and,
SUM(CASE WHEN day_diff=2 and browser_info='Android' THEN migrations END) AS d2_mig_and,
SUM(CASE WHEN day_diff=3 and browser_info='Android' THEN migrations END) AS d3_mig_and,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='Android' and dd.last7_days=1 and store_id=1) 
else (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='Android' and dd.week_to_date=1 and store_id=1)
		END) AS wtd_mig_and,
SUM(CASE WHEN month_to_date=1 and browser_info='Android' THEN migrations END) AS mtd_mig_and,
SUM(CASE WHEN month_diff=1 and browser_info='Android' THEN migrations END) AS m1_mig_and,
SUM(CASE WHEN month_diff=2 and browser_info='Android' THEN migrations END) AS m2_mig_and,

-- iOS
SUM(CASE WHEN day_diff=1 and browser_info='iOS' THEN migrations END) AS yest_mig_ios,
SUM(CASE WHEN day_diff=2 and browser_info='iOS' THEN migrations END) AS d2_mig_ios,
SUM(CASE WHEN day_diff=3 and browser_info='iOS' THEN migrations END) AS d3_mig_ios,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='iOS' and dd.last7_days=1 and store_id=1) 
else (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='iOS' and dd.week_to_date=1 and store_id=1)
		END) AS wtd_mig_ios,
SUM(CASE WHEN month_to_date=1 and browser_info='iOS' THEN migrations END) AS mtd_mig_ios,
SUM(CASE WHEN month_diff=1 and browser_info='iOS' THEN migrations END) AS m1_mig_ios,
SUM(CASE WHEN month_diff=2 and browser_info='iOS' THEN migrations END) AS m2_mig_ios,

-- Windows
SUM(CASE WHEN day_diff=1 and browser_info='Windows' THEN migrations END) AS yest_mig_win,
SUM(CASE WHEN day_diff=2 and browser_info='Windows' THEN migrations END) AS d2_mig_win,
SUM(CASE WHEN day_diff=3 and browser_info='Windows' THEN migrations END) AS d3_mig_win,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='Windows' and dd.last7_days=1 and store_id=1) 
else (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='Windows' and dd.week_to_date=1 and store_id=1)
		END) AS wtd_mig_win,
SUM(CASE WHEN month_to_date=1 and browser_info='Windows' THEN migrations END) AS mtd_mig_win,
SUM(CASE WHEN month_diff=1 and browser_info='Windows' THEN migrations END) AS m1_mig_win,
SUM(CASE WHEN month_diff=2 and browser_info='Windows' THEN migrations END) AS m2_mig_win

from 
(select browser_info,full_date,count (distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) 
										as migrations 
from fact_order f, dim_date dd
where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and dd.month_diff<=2 and dd.day_diff>0  and store_id=1
group by 1,2) mg 
left join dim_date d
on mg.full_date=d.full_date) Z,

(Select 
max(case when startdate=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD') then
	 (select sum(users) from ga_metric_weekly g,dim_date where cur_week_minus_1_week=1 AND g.startdate=full_date and app_platform in ('android','iOS','windows')) 
		else (select sum(users) from ga_metric_weekly g,dim_date where week_to_date=1 and g.startdate=full_date and app_platform in ('android','iOS','windows')) END) AS wtd_users,
max(case when startdate=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD') then
	 (select sum(users) from ga_metric_weekly g,dim_date where cur_week_minus_1_week=1 AND g.startdate=full_date and app_platform='android') 
		else (select sum(users) from ga_metric_weekly g,dim_date where week_to_date=1 and g.startdate=full_date and app_platform='android') END) AS wtd_users_and,
max(case when startdate=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD') then
	 (select sum(users) from ga_metric_weekly g,dim_date where cur_week_minus_1_week=1 AND g.startdate=full_date and app_platform='iOS') 
		else (select sum(users) from ga_metric_weekly g,dim_date where week_to_date=1 and g.startdate=full_date and app_platform='iOS') END) AS wtd_users_ios,
max(case when startdate=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD') then
	 (select sum(users) from ga_metric_weekly g,dim_date where cur_week_minus_1_week=1 AND g.startdate=full_date and app_platform='windows') 
		else (select sum(users) from ga_metric_weekly g,dim_date where week_to_date=1 and g.startdate=full_date and app_platform='windows') END) AS wtd_users_windows
from ga_metric_weekly) U1,

(Select 
--Overall
SUM(CASE WHEN month_to_date=1 and app_platform in ('android','iOS','windows') THEN users END) AS mtd_users,
SUM(CASE WHEN month_diff=1 and app_platform in ('android','iOS','windows') THEN users END) AS m1_users,
SUM(CASE WHEN month_diff=2 and app_platform in ('android','iOS','windows') THEN users END) AS m2_users,

--Android
SUM(CASE WHEN month_to_date=1 and app_platform='android' THEN users END) AS mtd_users_and,
SUM(CASE WHEN month_diff=1 and app_platform='android' THEN users END) AS m1_users_and,
SUM(CASE WHEN month_diff=2 and app_platform='android' THEN users END) AS m2_users_and,

--iOS
SUM(CASE WHEN month_to_date=1 and app_platform='iOS' THEN users END) AS mtd_users_ios,
SUM(CASE WHEN month_diff=1 and app_platform='iOS' THEN users END) AS m1_users_ios,
SUM(CASE WHEN month_diff=2 and app_platform='iOS' THEN users END) AS m2_users_ios,

--windows
SUM(CASE WHEN month_to_date=1 and app_platform='windows' THEN users END) AS mtd_users_windows,
SUM(CASE WHEN month_diff=1 and app_platform='windows' THEN users END) AS m1_users_windows,
SUM(CASE WHEN month_diff=2 and app_platform='windows' THEN users END) AS m2_users_windows

 from ga_metric_monthly g , dim_date d where g.startdate=full_date) U2

/*
(select
--All App
COUNT(CASE WHEN day_diff=1 THEN 1 END) AS yest_ins,
COUNT(CASE WHEN day_diff=2 THEN 1 END) AS d2_ins,
COUNT(CASE WHEN day_diff=3 THEN 1 END) AS d3_ins,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.last7_days=1) 
else (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.week_to_date=1) end) AS wtd_ins,
COUNT(CASE WHEN month_to_date=1 THEN 1 END) AS mtd_ins,
COUNT(CASE WHEN month_diff=1 THEN 1 END) AS m1_ins,
COUNT(CASE WHEN month_diff=2 THEN 1 END) AS m2_ins,

-- Android
COUNT(CASE WHEN day_diff=1 and platform='Android' THEN 1 END) AS yest_ins_and,
COUNT(CASE WHEN day_diff=2 and platform='Android' THEN 1 END) AS d2_ins_and,
COUNT(CASE WHEN day_diff=3 and platform='Android' THEN 1 END) AS d3_ins_and,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.last7_days=1 and platform='Android') 
else (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.week_to_date=1 and platform='Android') end) AS wtd_ins_and,
COUNT(CASE WHEN month_to_date=1 and platform='Android' THEN 1 END) AS mtd_ins_and,
COUNT(CASE WHEN month_diff=1 and platform='Android' THEN 1 END) AS m1_ins_and,
COUNT(CASE WHEN month_diff=2 and platform='Android' THEN 1 END) AS m2_ins_and,

-- iOS
COUNT(CASE WHEN day_diff=1 and platform='iOS' THEN 1 END) AS yest_ins_ios,
COUNT(CASE WHEN day_diff=2 and platform='iOS' THEN 1 END) AS d2_ins_ios,
COUNT(CASE WHEN day_diff=3 and platform='iOS' THEN 1 END) AS d3_ins_ios,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.last7_days=1 and platform='iOS') 
else (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.week_to_date=1 and platform='iOS') end) AS wtd_ins_ios,
COUNT(CASE WHEN month_to_date=1 and platform='iOS' THEN 1 END) AS mtd_ins_ios,
COUNT(CASE WHEN month_diff=1 and platform='iOS' THEN 1 END) AS m1_ins_ios,
COUNT(CASE WHEN month_diff=2 and platform='iOS' THEN 1 END) AS m2_ins_ios,

-- Windows
COUNT(CASE WHEN day_diff=1 and platform='Windows' THEN 1 END) AS yest_ins_win,
COUNT(CASE WHEN day_diff=2 and platform='Windows' THEN 1 END) AS d2_ins_win,
COUNT(CASE WHEN day_diff=3 and platform='Windows' THEN 1 END) AS d3_ins_win,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.last7_days=1 and platform='Windows') 
else (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.week_to_date=1 and platform='Windows') end) AS wtd_ins_win,
COUNT(CASE WHEN month_to_date=1 and platform='Windows' THEN 1 END) AS mtd_ins_win,
COUNT(CASE WHEN month_diff=1 and platform='Windows' THEN 1 END) AS m1_ins_win,
COUNT(CASE WHEN month_diff=2 and platform='Windows' THEN 1 END) AS m2_ins_win

from fact_app_install_from_apsalar ins, dim_date d
where ins.load_date=d.full_date and month_diff<=2 and platform in ('Android','iOS','Windows') )I
*/

to_char(d3_ordersM,'99,99,99,99,999') AS d3_orders,
to_char(wtd_ordersM,'99,99,99,99,999') AS wtd_orders,
to_char(mtd_ordersM,'99,99,99,99,999') AS mtd_orders,
to_char(m1_ordersM,'99,99,99,99,999') AS m1_orders,
to_char(m2_ordersM,'99,99,99,99,999') AS m2_orders,

to_char(yest_revM/yest_ordersM,'99,99,99,99,999') AS yday_asp,
to_char(d2_revM/d2_ordersM,'99,99,99,99,999') AS d2_asp,
to_char(d3_revM/d3_ordersM,'99,99,99,99,999') AS d3_asp,
to_char(wtd_revM/wtd_ordersM,'99,99,99,99,999') AS wtd_asp,
to_char(mtd_revM/mtd_ordersM,'99,99,99,99,999') AS mtd_asp,
to_char(m1_revM/m1_ordersM,'99,99,99,99,999') AS m1_asp,
to_char(m2_revM/m2_ordersM,'99,99,99,99,999') AS m2_asp,

round(yest_ordersM*100/yday_users::decimal(12,2),2) as yday_conv,
round(d2_ordersM*100/d2_users::decimal(12,2),2) as d2_conv,
round(d3_ordersM*100/d3_users::decimal(12,2),2) as d3_conv,
round(wtd_ordersM*100/NULLIF(wtd_users,0)::decimal(12,2),2) as wtd_conv,
round(mtd_ordersM*100/mtd_users::decimal(12,2),2) as mtd_conv,
round(m1_ordersM*100/m1_users::decimal(12,2),2) as m1_conv,
round(m2_ordersM*100/m2_users::decimal(12,2),2) as m2_conv,

to_char(yday_users,'99,99,99,99,999') as yday_users,
to_char(d2_users,'99,99,99,99,999') as d2_users,
to_char(d3_users,'99,99,99,99,999') as d3_users,
to_char(wtd_users,'99,99,99,99,999') as wtd_users,
to_char(mtd_users,'99,99,99,99,999') as mtd_users,
to_char(m1_users,'99,99,99,99,999') as m1_users,
to_char(m2_users,'99,99,99,99,999') as m2_users,

round(yest_custM*100/yday_users::decimal(12,2),2) as yest_cust_users,
round(d2_custM*100/d2_users::decimal(12,2),2) as d2_cust_users,
round(d3_custM*100/d3_users::decimal(12,2),2) as d3_cust_users,
round(wtd_custM*100/NULLIF(wtd_users,0)::decimal(12,2),2) as wtd_cust_users,
round(mtd_custM*100/mtd_users::decimal(12,2),2) as mtd_cust_users,
round(m1_custM*100/m1_users::decimal(12,2),2) as m1_cust_users,
round(m2_custM*100/m2_users::decimal(12,2),2) as m2_cust_users,

to_char(yday_session_time*yday_sessions/(yday_users*60),'99,99,99,99,999') as yest_session_time,
to_char(d2_session_time*d2_sessions/(d2_users*60),'99,99,99,99,999') as d2_session_time,
to_char(d3_session_time*d3_sessions/(d3_users*60),'99,99,99,99,999') as d3_session_time,
to_char(wtd_session_time*wtd_sessions/(NULLIF(wtd_users,0)*60),'99,99,99,99,999') as wtd_session_time,
to_char(mtd_session_time*mtd_sessions/(mtd_users*60),'99,99,99,99,999') as mtd_session_time,
to_char(m1_session_time*m1_sessions/(m1_users*60),'99,99,99,99,999') as m1_session_time,
to_char(m2_session_time*m2_sessions/(m2_users*60),'99,99,99,99,999') as m2_session_time,

/*
to_char(yest_ins,'99,99,99,99,999') AS yday_ins,
to_char(d2_ins,'99,99,99,99,999') AS d2_ins,
to_char(d3_ins,'99,99,99,99,999') AS d3_ins,
to_char(wtd_ins,'99,99,99,99,999') AS wtd_ins,
to_char(mtd_ins,'99,99,99,99,999') AS mtd_ins,
to_char(m1_ins,'99,99,99,99,999') AS m1_ins,
to_char(m2_ins,'99,99,99,99,999') AS m2_ins,*/

to_char(yest_acqM,'99,99,99,99,999') AS yday_acq,
to_char(d2_acqM,'99,99,99,99,999') AS d2_acq,
to_char(d3_acqM,'99,99,99,99,999') AS d3_acq,
to_char(wtd_acqM,'99,99,99,99,999') AS wtd_acq,
to_char(mtd_acqM,'99,99,99,99,999') AS mtd_acq,
to_char(m1_acqM,'99,99,99,99,999') AS m1_acq,
to_char(m2_acqM,'99,99,99,99,999') AS m2_acq,

to_char(yest_acqM_rev,'99,99,99,99,999') AS yday_acq_rev,
to_char(d2_acqM_rev,'99,99,99,99,999') AS d2_acq_rev,
to_char(d3_acqM_rev,'99,99,99,99,999') AS d3_acq_rev,
to_char(wtd_acqM_rev,'99,99,99,99,999') AS wtd_acq_rev,
to_char(mtd_acqM_rev,'99,99,99,99,999') AS mtd_acq_rev,
to_char(m1_acqM_rev,'99,99,99,99,999') AS m1_acq_rev,
to_char(m2_acqM_rev,'99,99,99,99,999') AS m2_acq_rev,

to_char(yest_mig,'99,99,99,99,999') AS yday_mig,
to_char(d2_mig,'99,99,99,99,999') AS d2_mig,
to_char(d3_mig,'99,99,99,99,999') AS d3_mig,
to_char(wtd_mig,'99,99,99,99,999') AS wtd_mig,
to_char(mtd_mig,'99,99,99,99,999') AS mtd_mig,
to_char(m1_mig,'99,99,99,99,999') AS m1_mig,
to_char(m2_mig,'99,99,99,99,999') AS m2_mig,

-- Android
to_char(yest_revMand,'99,99,99,99,999') AS yday_and_rev,
ROUND(yest_revMand*100/yest_revM,2) as a_rev_per,
to_char(d2_revMand,'99,99,99,99,999') AS d2_and_rev,
ROUND(d2_revMand*100/d2_revM,2) as a2,
to_char(d3_revMand,'99,99,99,99,999') AS d3_and_rev,
ROUND(d3_revMand*100/d3_revM,2) as a3,
to_char(wtd_revMand,'99,99,99,99,999') AS wtd_and_rev,
ROUND(wtd_revMand*100/wtd_revM,2) as a4,
to_char(mtd_revMand,'99,99,99,99,999') AS mtd_and_rev,
ROUND(mtd_revMand*100/mtd_revM,2) as a5,
to_char(m1_revMand,'99,99,99,99,999') AS m1_and_rev,
ROUND(m1_revMand*100/m1_revM,2) as a6,
to_char(m2_revMand,'99,99,99,99,999') AS m2_and_rev,
ROUND(m2_revMand*100/m2_revM,2) as a7,

to_char(yest_ordersMand,'99,99,99,99,999') AS yday_and_orders,
ROUND(yest_ordersMand*100/yest_ordersM::decimal(12,2),2) a_ord_per,

to_char(d2_ordersMand,'99,99,99,99,999') AS d2_and_orders,
ROUND(d2_ordersMand*100/d2_ordersM,2) as a9,

to_char(d3_ordersMand,'99,99,99,99,999') AS d3_and_orders,
ROUND(d3_ordersMand*100/d3_ordersM,2) as a10,

to_char(wtd_ordersMand,'99,99,99,99,999') AS wtd_and_orders,
ROUND(wtd_ordersMand*100/wtd_ordersM,2) as a11,

to_char(mtd_ordersMand,'99,99,99,99,999') AS mtd_and_orders,
ROUND(mtd_ordersMand*100/mtd_ordersM,2) as a12,

to_char(m1_ordersMand,'99,99,99,99,999') AS m1_and_orders,
ROUND(m1_ordersMand*100/m1_ordersM,2) as a13,

to_char(m2_ordersMand,'99,99,99,99,999') AS m2_and_orders,
ROUND(m2_ordersMand*100/m2_ordersM,2) as a14,

to_char(yest_revMand/yest_ordersMand,'99,99,99,99,999') AS yday_and_asp,
to_char(d2_revMand/d2_ordersMand,'99,99,99,99,999') AS d2_and_asp,
to_char(d3_revMand/d3_ordersMand,'99,99,99,99,999') AS d3_and_asp,
to_char(wtd_revMand/wtd_ordersMand,'99,99,99,99,999') AS wtd_and_asp,
to_char(mtd_revMand/mtd_ordersMand,'99,99,99,99,999') AS mtd_and_asp,
to_char(m1_revMand/m1_ordersMand,'99,99,99,99,999') AS m1_and_asp,
to_char(m2_revMand/m2_ordersMand,'99,99,99,99,999') AS m2_and_asp,

round(yest_ordersMand*100/yday_users_and::decimal(12,2),2) as yday_conv_and,
round(d2_ordersMand*100/d2_users_and::decimal(12,2),2) as d2_conv_and,
round(d3_ordersMand*100/d3_users_and::decimal(12,2),2) as d3_conv_and,
round(wtd_ordersMand*100/NULLIF(wtd_users_and,0)::decimal(12,2),2) as wtd_conv_and,
round(mtd_ordersMand*100/mtd_users_and::decimal(12,2),2) as mtd_conv_and,
round(m1_ordersMand*100/m1_users_and::decimal(12,2),2) as m1_conv_and,
round(m2_ordersMand*100/m2_users_and::decimal(12,2),2) as m2_conv_and,

to_char(yday_users_and,'99,99,99,99,999') as yday_users_and,
ROUND(yday_users_and*100/yday_users::decimal(12,2),2) as a_user_per,

to_char(d2_users_and,'99,99,99,99,999') as d2_users_and,
to_char(d3_users_and,'99,99,99,99,999') as d3_users_and,
to_char(wtd_users_and,'99,99,99,99,999') as wtd_users_and,
to_char(mtd_users_and,'99,99,99,99,999') as mtd_users_and,
to_char(m1_users_and,'99,99,99,99,999') as m1_users_and,
to_char(m2_users_and,'99,99,99,99,999') as m2_users_and,


round(yest_custMand*100/yday_users_and::decimal(12,2),2) as yest_cu_and,
round(d2_custMand*100/d2_users_and::decimal(12,2),2) as d2_cu_and,
round(d3_custMand*100/d3_users_and::decimal(12,2),2) as d3_cu_and,
round(wtd_custMand*100/NULLIF(wtd_users_and,0)::decimal(12,2),2) as wtd_cu_and,
round(mtd_custMand*100/mtd_users_and::decimal(12,2),2) as mtd_cu_and,
round(m1_custMand*100/m1_users_and::decimal(12,2),2) as m1_cu_and,
round(m2_custMand*100/m2_users_and::decimal(12,2),2) as m2_cu_and,

to_char(yday_session_time_and*yday_sessions_and/(yday_users_and*60),'99,99,99,99,999') as yest_session_time_and,
to_char(d2_session_time_and*d2_sessions_and/(d2_users_and*60),'99,99,99,99,999') as d2_session_time_and,
to_char(d3_session_time_and*d3_sessions_and/(d3_users_and*60),'99,99,99,99,999') as d3_session_time_and,
to_char(wtd_session_time_and*wtd_sessions_and/(NULLIF(wtd_users_and,0)*60),'99,99,99,99,999') as wtd_session_time_and,
to_char(mtd_session_time_and*mtd_sessions_and/(mtd_users_and*60),'99,99,99,99,999') as mtd_session_time_and,
to_char(m1_session_time_and*m1_sessions_and/(m1_users_and*60),'99,99,99,99,999') as m1_session_time_and,
to_char(m2_session_time_and*m2_sessions_and/(m2_users_and*60),'99,99,99,99,999') as m2_session_time_and,
/*
to_char(yest_ins_and,'99,99,99,99,999') AS yday_ins_and,
to_char(d2_ins_and,'99,99,99,99,999') AS d2_ins_and,
to_char(d3_ins_and,'99,99,99,99,999') AS d3_ins_and,
to_char(wtd_ins_and,'99,99,99,99,999') AS wtd_ins_and,
to_char(mtd_ins_and,'99,99,99,99,999') AS mtd_ins_and,
to_char(m1_ins_and,'99,99,99,99,999') AS m1_ins_and,
to_char(m2_ins_and,'99,99,99,99,999') AS m2_ins_and,*/

to_char(yest_acqMand,'99,99,99,99,999') AS yday_acq_and,
ROUND(yest_acqMand*100/yest_acqM::decimal(12,2),2) as a_acq_per,

to_char(d2_acqMand,'99,99,99,99,999') AS d2_acq_and,
to_char(d3_acqMand,'99,99,99,99,999') AS d3_acq_and,
to_char(wtd_acqMand,'99,99,99,99,999') AS wtd_acq_and,
to_char(mtd_acqMand,'99,99,99,99,999') AS mtd_acq_and,
to_char(m1_acqMand,'99,99,99,99,999') AS m1_acq_and,
to_char(m2_acqMand,'99,99,99,99,999') AS m2_acq_and,

to_char(yest_acqM_rev_and,'99,99,99,99,999') AS yday_acq_rev_and,
to_char(d2_acqM_rev_and,'99,99,99,99,999') AS d2_acq_rev_and,
to_char(d3_acqM_rev_and,'99,99,99,99,999') AS d3_acq_rev_and,
to_char(wtd_acqM_rev_and,'99,99,99,99,999') AS wtd_acq_rev_and,
to_char(mtd_acqM_rev_and,'99,99,99,99,999') AS mtd_acq_rev_and,
to_char(m1_acqM_rev_and,'99,99,99,99,999') AS m1_acq_rev_and,
to_char(m2_acqM_rev_and,'99,99,99,99,999') AS m2_acq_rev_and,

to_char(yest_mig_and,'99,99,99,99,999') AS yday_mig_and,
to_char(d2_mig_and,'99,99,99,99,999') AS d2_mig_and,
to_char(d3_mig_and,'99,99,99,99,999') AS d3_mig_and,
to_char(wtd_mig_and,'99,99,99,99,999') AS wtd_mig_and,
to_char(mtd_mig_and,'99,99,99,99,999') AS mtd_mig_and,
to_char(m1_mig_and,'99,99,99,99,999') AS m1_mig_and,
to_char(m2_mig_and,'99,99,99,99,999') AS m2_mig_and,

-- iOS
to_char(yest_revMiOS,'99,99,99,99,999') AS yday_iOS_rev,
ROUND(yest_revMiOS*100/yest_revM,2) as i_rev_per,

to_char(d2_revMiOS,'99,99,99,99,999') AS d2_iOS_rev,
ROUND(d2_revMiOS*100/d2_revM,2) as i2,

to_char(d3_revMiOS,'99,99,99,99,999') AS d3_iOS_rev,
ROUND(d3_revMiOS*100/d3_revM,2) as i3,

to_char(wtd_revMiOS,'99,99,99,99,999') AS wtd_iOS_rev,
ROUND(wtd_revMiOS*100/wtd_revM,2) as i4,

to_char(mtd_revMiOS,'99,99,99,99,999') AS mtd_iOS_rev,
ROUND(mtd_revMiOS*100/mtd_revM,2) as i5,

to_char(m1_revMiOS,'99,99,99,99,999') AS m1_iOS_rev,
ROUND(m1_revMiOS*100/m1_revM,2) as i6,

to_char(m2_revMiOS,'99,99,99,99,999') AS m2_iOS_rev,
ROUND(m2_revMiOS*100/m1_revM,2) as i7,

to_char(yest_ordersMiOS,'99,99,99,99,999') AS yday_iOS_orders,
ROUND(yest_ordersMiOS*100/yest_ordersM::decimal(12,2),2) as i_ord_per,

to_char(d2_ordersMiOS,'99,99,99,99,999') AS d2_iOS_orders,
ROUND(d2_ordersMiOS*100/d2_ordersM,2) as i9,

to_char(d3_ordersMiOS,'99,99,99,99,999') AS d3_iOS_orders,
ROUND(d3_ordersMiOS*100/d3_ordersM,2) as i10,

to_char(wtd_ordersMiOS,'99,99,99,99,999') AS wtd_iOS_orders,
ROUND(wtd_ordersMiOS*100/wtd_ordersM,2) as i11,

to_char(mtd_ordersMiOS,'99,99,99,99,999') AS mtd_iOS_orders,
ROUND(mtd_ordersMiOS*100/mtd_ordersM,2) as i12,

to_char(m1_ordersMiOS,'99,99,99,99,999') AS m1_iOS_orders,
ROUND(m1_ordersMiOS*100/m1_ordersM,2) as i13,

to_char(m2_ordersMiOS,'99,99,99,99,999') AS m2_iOS_orders,
ROUND(m2_ordersMiOS*100/m2_ordersM,2) as i14,

to_char(yest_revMiOS/yest_ordersMiOS,'99,99,99,99,999') AS yday_iOS_asp,
to_char(d2_revMiOS/d2_ordersMiOS,'99,99,99,99,999') AS d2_iOS_asp,
to_char(d3_revMiOS/d3_ordersMiOS,'99,99,99,99,999') AS d3_iOS_asp,
to_char(wtd_revMiOS/wtd_ordersMiOS,'99,99,99,99,999') AS wtd_iOS_asp,
to_char(mtd_revMiOS/mtd_ordersMiOS,'99,99,99,99,999') AS mtd_iOS_asp,
to_char(m1_revMiOS/m1_ordersMiOS,'99,99,99,99,999') AS m1_iOS_asp,
to_char(m2_revMiOS/m2_ordersMiOS,'99,99,99,99,999') AS m2_iOS_asp,

round(yest_ordersMiOS*100/yday_users_iOS::decimal(12,2),2) as yday_conv_iOS,
round(d2_ordersMiOS*100/d2_users_iOS::decimal(12,2),2) as d2_conv_iOS,
round(d3_ordersMiOS*100/d3_users_iOS::decimal(12,2),2) as d3_conv_iOS,
round(wtd_ordersMiOS*100/NULLIF(wtd_users_iOS,0)::decimal(12,2),2) as wtd_conv_iOS,
round(mtd_ordersMiOS*100/mtd_users_iOS::decimal(12,2),2) as mtd_conv_iOS,
round(m1_ordersMiOS*100/m1_users_iOS::decimal(12,2),2) as m1_conv_iOS,
round(m2_ordersMiOS*100/m2_users_iOS::decimal(12,2),2) as m2_conv_iOS,
 
to_char(yday_users_iOS,'99,99,99,99,999') AS yday_users_iOS,
ROUND(yday_users_iOS*100/yday_users::decimal(12,2),2) as i_user_per,

to_char(d2_users_iOS,'99,99,99,99,999') AS d2_users_iOS,
to_char(d3_users_iOS,'99,99,99,99,999') AS d3_users_iOS,
to_char(wtd_users_iOS,'99,99,99,99,999') AS wtd_users_iOS,
to_char(mtd_users_iOS,'99,99,99,99,999') AS mtd_users_iOS,
to_char(m1_users_iOS,'99,99,99,99,999') AS m1_users_iOS,
to_char(m2_users_iOS,'99,99,99,99,999') AS m2_users_iOS,

to_char(yest_custMios,'99,99,99,99,999') as yest_cust_ios,
to_char(d2_custMios,'99,99,99,99,999') as d2_cust_ios,
to_char(d3_custMios,'99,99,99,99,999') as d3_cust_ios,
to_char(wtd_custMios,'99,99,99,99,999') as wtd_cust_ios,
to_char(mtd_custMios,'99,99,99,99,999') as mtd_cust_ios,
to_char(m1_custMios,'99,99,99,99,999') as m1_cust_ios,
to_char(m2_custMios,'99,99,99,99,999') as m2_cust_ios,

round(yest_custMios*100/yday_users_ios::decimal(12,2),2) as yest_cu_ios,
round(d2_custMios*100/d2_users_ios::decimal(12,2),2) as d2_cu_ios,
round(d3_custMios*100/d3_users_ios::decimal(12,2),2) as d3_cu_ios,
round(wtd_custMios*100/NULLIF(wtd_users_iOS,0)::decimal(12,2),2) as wtd_cu_ios,
round(mtd_custMios*100/mtd_users_ios::decimal(12,2),2) as mtd_cu_ios,
round(m1_custMios*100/m1_users_ios::decimal(12,2),2) as m1_cu_ios,
round(m2_custMios*100/m2_users_ios::decimal(12,2),2) as m2_cu_ios,

to_char(yday_session_time_ios*yday_sessions_ios/(yday_users_ios*60),'99,99,99,99,999') as yest_session_time_ios,
to_char(d2_session_time_ios*d2_sessions_ios/(d2_users_ios*60),'99,99,99,99,999') as d2_session_time_ios,
to_char(d3_session_time_ios*d3_sessions_ios/(d3_users_ios*60),'99,99,99,99,999') as d3_session_time_ios,
to_char(wtd_session_time_ios*wtd_sessions_ios/(NULLIF(wtd_users_iOS,0)*60),'99,99,99,99,999') as wtd_session_time_ios,
to_char(mtd_session_time_ios*mtd_sessions_ios/(mtd_users_ios*60),'99,99,99,99,999') as mtd_session_time_ios,
to_char(m1_session_time_ios*m1_sessions_ios/(m1_users_ios*60),'99,99,99,99,999') as m1_session_time_ios,
to_char(m2_session_time_ios*m2_sessions_ios/(m2_users_ios*60),'99,99,99,99,999') as m2_session_time_ios,
/*
to_char(yest_ins_ios,'99,99,99,99,999') AS yday_ins_ios,
to_char(d2_ins_ios,'99,99,99,99,999') AS d2_ins_ios,
to_char(d3_ins_ios,'99,99,99,99,999') AS d3_ins_ios,
to_char(wtd_ins_ios,'99,99,99,99,999') AS wtd_ins_ios,
to_char(mtd_ins_ios,'99,99,99,99,999') AS mtd_ins_ios,
to_char(m1_ins_ios,'99,99,99,99,999') AS m1_ins_ios,
to_char(m2_ins_ios,'99,99,99,99,999') AS m2_ins_ios,*/

to_char(yest_acqMios,'99,99,99,99,999') AS yday_acq_ios,
ROUND(yest_acqMios*100/yest_acqM::decimal(12,2),2) as i_acq_per,

to_char(d2_acqMios,'99,99,99,99,999') AS d2_acq_ios,
to_char(d3_acqMios,'99,99,99,99,999') AS d3_acq_ios,
to_char(wtd_acqMios,'99,99,99,99,999') AS wtd_acq_ios,
to_char(mtd_acqMios,'99,99,99,99,999') AS mtd_acq_ios,
to_char(m1_acqMios,'99,99,99,99,999') AS m1_acq_ios,
to_char(m2_acqMios,'99,99,99,99,999') AS m2_acq_ios,

to_char(yest_acqM_rev_ios,'99,99,99,99,999') AS yday_acq_rev_ios,
to_char(d2_acqM_rev_ios,'99,99,99,99,999') AS d2_acq_rev_ios,
to_char(d3_acqM_rev_ios,'99,99,99,99,999') AS d3_acq_rev_ios,
to_char(wtd_acqM_rev_ios,'99,99,99,99,999') AS wtd_acq_rev_ios,
to_char(mtd_acqM_rev_ios,'99,99,99,99,999') AS mtd_acq_rev_ios,
to_char(m1_acqM_rev_ios,'99,99,99,99,999') AS m1_acq_rev_ios,
to_char(m2_acqM_rev_ios,'99,99,99,99,999') AS m2_acq_rev_ios,

to_char(yest_mig_ios,'99,99,99,99,999') AS yday_mig_ios,
to_char(d2_mig_ios,'99,99,99,99,999') AS d2_mig_ios,
to_char(d3_mig_ios,'99,99,99,99,999') AS d3_mig_ios,
to_char(wtd_mig_ios,'99,99,99,99,999') AS wtd_mig_ios,
to_char(mtd_mig_ios,'99,99,99,99,999') AS mtd_mig_ios,
to_char(m1_mig_ios,'99,99,99,99,999') AS m1_mig_ios,
to_char(m2_mig_ios,'99,99,99,99,999') AS m2_mig_ios,
 
-- Windows
to_char(yest_revMwin,'99,99,99,99,999') AS yday_win_rev,
ROUND(yest_revMwin*100/yest_revM,2) as w_rev_per,

to_char(d2_revMwin,'99,99,99,99,999') AS d2_win_rev,
ROUND(d2_revMwin*100/d2_revM,2) as w2,

to_char(d3_revMwin,'99,99,99,99,999') AS d3_win_rev,
ROUND(d3_revMwin*100/d2_revM,2) as w3,

to_char(wtd_revMwin,'99,99,99,99,999') AS wtd_win_rev,
ROUND(wtd_revMwin*100/wtd_revM,2) as w4,

to_char(mtd_revMwin,'99,99,99,99,999') AS mtd_win_rev,
ROUND(mtd_revMwin*100/mtd_revM,2) as w5,

to_char(m1_revMwin,'99,99,99,99,999') AS m1_win_rev,
ROUND(m1_revMwin*100/m1_revM,2) as w6,

to_char(m2_revMwin,'99,99,99,99,999') AS m2_win_rev,
ROUND(m2_revMwin*100/m2_revM,2) as w7,

to_char(yest_ordersMwin,'99,99,99,99,999') AS yday_win_orders,
ROUND(yest_ordersMwin*100/yest_ordersM::decimal(12,2),2)  as w_ord_per,

to_char(d2_ordersMwin,'99,99,99,99,999') AS d2_win_orders,
ROUND(d2_ordersMwin*100/d2_ordersM,2) as w9,

to_char(d3_ordersMwin,'99,99,99,99,999') AS d3_win_orders,
 ROUND(d3_ordersMwin*100/d3_ordersM,2) as w10,

to_char(wtd_ordersMwin,'99,99,99,99,999') AS wtd_win_orders,
ROUND(wtd_ordersMwin*100/wtd_ordersM,2) as w11,

to_char(mtd_ordersMwin,'99,99,99,99,999') AS mtd_win_orders,
ROUND(mtd_ordersMwin*100/mtd_ordersM,2) as w12,

to_char(m1_ordersMwin,'99,99,99,99,999') AS m1_win_orders,
ROUND(m1_ordersMwin*100/m1_ordersM,2) as w13,

to_char(m2_ordersMwin,'99,99,99,99,999') AS m2_win_orders,
ROUND(m2_ordersMwin*100/m1_ordersM,2) as w14,

to_char(yest_revMwin/yest_ordersMwin,'99,99,99,99,999') AS yday_win_asp,
to_char(d2_revMwin/d2_ordersMwin,'99,99,99,99,999') AS d2_win_asp,
to_char(d3_revMwin/d3_ordersMwin,'99,99,99,99,999') AS d3_win_asp,
to_char(wtd_revMwin/wtd_ordersMwin,'99,99,99,99,999') AS wtd_win_asp,
to_char(mtd_revMwin/mtd_ordersMwin,'99,99,99,99,999') AS mtd_win_asp,
to_char(m1_revMwin/m1_ordersMwin,'99,99,99,99,999') AS m1_win_asp,
to_char(m2_revMwin/m2_ordersMwin,'99,99,99,99,999') AS m2_win_asp,

round(yest_ordersMwin*100/yday_users_Windows::decimal(12,2),2) as yday_conv_win,
round(d2_ordersMwin*100/d2_users_Windows::decimal(12,2),2) as d2_conv_win,
round(d3_ordersMwin*100/d3_users_Windows::decimal(12,2),2) as d3_conv_win,
round(wtd_ordersMwin*100/NULLIF(wtd_users_Windows,0)::decimal(12,2),2) as wtd_conv_win,
round(mtd_ordersMwin*100/mtd_users_Windows::decimal(12,2),2) as mtd_conv_win,
round(m1_ordersMwin*100/m1_users_Windows::decimal(12,2),2) as m1_conv_win,
round(m2_ordersMwin*100/m2_users_Windows::decimal(12,2),2) as m2_conv_win,

to_char(yday_users_Windows,'99,99,99,99,999') AS yday_users_Windows,
ROUND(yday_users_Windows*100/yday_users::decimal(12,2),2) as w_user_per,

to_char(d2_users_Windows,'99,99,99,99,999') AS d2_users_Windows,
to_char(d3_users_Windows,'99,99,99,99,999') AS d3_users_Windows,
to_char(wtd_users_Windows,'99,99,99,99,999') AS wtd_users_Windows,
to_char(mtd_users_Windows,'99,99,99,99,999') AS mtd_users_Windows,
to_char(m1_users_Windows,'99,99,99,99,999') as m1_users_win,
to_char(m2_users_Windows,'99,99,99,99,999') as m2_users_win,

to_char(yest_custMwin,'99,99,99,99,999') as yest_cust_win,
to_char(d2_custMwin,'99,99,99,99,999') as d2_cust_win,
to_char(d3_custMwin,'99,99,99,99,999') as d3_cust_win,
to_char(wtd_custMwin,'99,99,99,99,999') as wtd_cust_win,
to_char(mtd_custMwin,'99,99,99,99,999') as mtd_cust_win,
to_char(m1_custMwin,'99,99,99,99,999') as m1_cust_win,
to_char(m2_custMwin,'99,99,99,99,999') as m2_cust_win,
 
round(yest_custMwin*100/yday_users_Windows::decimal(12,2),2) as yest_cu_win,
round(d2_custMwin*100/d2_users_windows::decimal(12,2),2) as d2_cu_win,
round(d3_custMwin*100/d3_users_windows::decimal(12,2),2) as d3_cu_win,
round(wtd_custMwin*100/NULLIF(wtd_users_Windows,0)::decimal(12,2),2) as wtd_cu_win,
round(mtd_custMwin*100/mtd_users_windows::decimal(12,2),2) as mtd_cu_win,
round(m1_custMwin*100/m1_users_windows::decimal(12,2),2) as m1_cu_win,
round(m2_custMwin*100/m2_users_windows::decimal(12,2),2) as m2_cu_win,

to_char(yday_session_time_windows*yday_sessions_windows/(yday_users_windows*60),'99,99,99,99,999') as yest_session_time_windows,
to_char(d2_session_time_windows*d2_sessions_windows/(d2_users_windows*60),'99,99,99,99,999') as d2_session_time_windows,
to_char(d3_session_time_windows*d3_sessions_windows/(d3_users_windows*60),'99,99,99,99,999') as d3_session_time_windows,
to_char(wtd_session_time_windows*wtd_sessions_windows/(NULLIF(wtd_users_Windows,0)*60),'99,99,99,99,999') as wtd_session_time_windows,
to_char(mtd_session_time_windows*mtd_sessions_windows/(mtd_users_windows*60),'99,99,99,99,999') as mtd_session_time_windows,
to_char(m1_session_time_windows*m1_sessions_windows/(m1_users_windows*60),'99,99,99,99,999') as m1_session_time_windows,
to_char(m2_session_time_windows*m2_sessions_windows/(m2_users_windows*60),'99,99,99,99,999') as m2_session_time_windows,
/*
to_char(yest_ins_win,'99,99,99,99,999') AS yday_ins_win,
to_char(d2_ins_win,'99,99,99,99,999') AS d2_ins_win,
to_char(d3_ins_win,'99,99,99,99,999') AS d3_ins_win,
to_char(wtd_ins_win,'99,99,99,99,999') AS wtd_ins_win,
to_char(mtd_ins_win,'99,99,99,99,999') AS mtd_ins_win,
to_char(m1_ins_win,'99,99,99,99,999') AS m1_ins_win,
to_char(m2_ins_win,'99,99,99,99,999') AS m2_ins_win,*/

to_char(yest_acqMwin,'99,99,99,99,999') AS yday_acq_win,
ROUND(yest_acqMwin*100/yest_acqM::decimal(12,2),2) as w_acq_per,

to_char(d2_acqMwin,'99,99,99,99,999') AS d2_acq_win,
to_char(d3_acqMwin,'99,99,99,99,999') AS d3_acq_win,
to_char(wtd_acqMwin,'99,99,99,99,999') AS wtd_acq_win,
to_char(mtd_acqMwin,'99,99,99,99,999') AS mtd_acq_win,
to_char(m1_acqMwin,'99,99,99,99,999') AS m1_acq_win,
to_char(m2_acqMwin,'99,99,99,99,999') AS m2_acq_win,

to_char(yest_acqM_rev_win,'99,99,99,99,999') AS yday_acq_rev_win,
to_char(d2_acqM_rev_win,'99,99,99,99,999') AS d2_acq_rev_win,
to_char(d3_acqM_rev_win,'99,99,99,99,999') AS d3_acq_rev_win,
to_char(wtd_acqM_rev_win,'99,99,99,99,999') AS wtd_acq_rev_win,
to_char(mtd_acqM_rev_win,'99,99,99,99,999') AS mtd_acq_rev_win,
to_char(m1_acqM_rev_win,'99,99,99,99,999') AS m1_acq_rev_win,
to_char(m2_acqM_rev_win,'99,99,99,99,999') AS m2_acq_rev_win,

to_char(yest_mig_win,'99,99,99,99,999') AS yday_mig_win,
to_char(d2_mig_win,'99,99,99,99,999') AS d2_mig_win,
to_char(d3_mig_win,'99,99,99,99,999') AS d3_mig_win,
to_char(wtd_mig_win,'99,99,99,99,999') AS wtd_mig_win,
to_char(mtd_mig_win,'99,99,99,99,999') AS mtd_mig_win,
to_char(m1_mig_win,'99,99,99,99,999') AS m1_mig_win,
to_char(m2_mig_win,'99,99,99,99,999') AS m2_mig_win
 
FROM
(SELECT 
-- All App
ROUND(SUM(CASE WHEN day_diff=1 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS yest_revM,
ROUND(SUM(CASE WHEN day_diff=2 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS d2_revM,
ROUND(SUM(CASE WHEN day_diff=3 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS d3_revM,
ROUND(max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app') 
else (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) 
and order_channel='mobile-app') END),0) AS wtd_revM,
ROUND(SUM(CASE WHEN month_to_date=1 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS mtd_revM,
ROUND(SUM(CASE WHEN month_diff=1 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS m1_revM,
ROUND(SUM(CASE WHEN month_diff=2 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS m2_revM,


COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' THEN order_group_id END) AS yest_ordersM,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' THEN order_group_id END) AS d2_ordersM,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' THEN order_group_id END) AS d3_ordersM,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct order_group_id) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app') 
else (select count(distinct order_group_id) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app') END) 
AS wtd_ordersM,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' THEN order_group_id END) AS mtd_ordersM,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' THEN order_group_id END) AS m1_ordersM,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' THEN order_group_id END) AS m2_ordersM,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' THEN idcustomer END) AS yest_custM,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' THEN idcustomer END) AS d2_custM,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' THEN idcustomer END) AS d3_custM,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app') 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app') END) 
AS wtd_custM,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' THEN idcustomer END) AS mtd_custM,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' THEN idcustomer END) AS m1_custM,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' THEN idcustomer END) AS m2_custM,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and purchase_type='f' THEN idcustomer END) AS yest_acqM,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and purchase_type='f' THEN idcustomer END) AS d2_acqM,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and purchase_type='f' THEN idcustomer END) AS d3_acqM,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and purchase_type='f') 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and purchase_type='f') END) 
AS wtd_acqM,
COUNT( CASE WHEN month_to_date=1 AND order_channel='mobile-app' and purchase_type='f' THEN idcustomer END) AS mtd_acqM,
COUNT( CASE WHEN month_diff=1 AND order_channel='mobile-app' and purchase_type='f' THEN idcustomer END) AS m1_acqM,
COUNT( CASE WHEN month_diff=2 AND order_channel='mobile-app' and purchase_type='f' THEN idcustomer END) AS m2_acqM,

SUM( CASE WHEN day_diff=1 AND order_channel='mobile-app' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS yest_acqM_rev,
SUM( CASE WHEN day_diff=2 AND order_channel='mobile-app' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d2_acqM_rev,
SUM( CASE WHEN day_diff=3 AND order_channel='mobile-app' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d3_acqM_rev,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and purchase_type='f') 
else (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and purchase_type='f') END) 
AS wtd_acqM_rev,
SUM( CASE WHEN month_to_date=1 AND order_channel='mobile-app' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS mtd_acqM_rev,
SUM( CASE WHEN month_diff=1 AND order_channel='mobile-app' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m1_acqM_rev,
SUM( CASE WHEN month_diff=2 AND order_channel='mobile-app' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m2_acqM_rev,


-- Android
ROUND(SUM(CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS yest_revMand,
ROUND(SUM(CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS d2_revMand,
ROUND(SUM(CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS d3_revMand,
ROUND(max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android') 
else (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) 
and order_channel='mobile-app' and browser_info='Android') END),0) AS wtd_revMand,
ROUND(SUM(CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS mtd_revMand,
ROUND(SUM(CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS m1_revMand,
ROUND(SUM(CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS m2_revMand,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS yest_ordersMand,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS d2_ordersMand,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS d3_ordersMand,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct order_group_id) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android') 
else (select count(distinct order_group_id) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android') END)
AS wtd_ordersMand,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS mtd_ordersMand,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS m1_ordersMand,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS m2_ordersMand,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN idcustomer END) AS yest_custMand,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN idcustomer END) AS d2_custMand,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Android' THEN idcustomer END) AS d3_custMand,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android') 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android') END) 
AS wtd_custMand,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Android' THEN idcustomer END) AS mtd_custMand,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN idcustomer END) AS m1_custMand,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN idcustomer END) AS m2_custMand,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN idcustomer END) AS yest_acqMand,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN idcustomer END) AS d2_acqMand,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN idcustomer END) AS d3_acqMand,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android' and purchase_type='f') 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android' and purchase_type='f') END) 
AS wtd_acqMand,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN idcustomer END) AS mtd_acqMand,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN idcustomer END) AS m1_acqMand,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN idcustomer END) AS m2_acqMand,

SUM( CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS yest_acqM_rev_and,
SUM( CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d2_acqM_rev_and,
SUM( CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d3_acqM_rev_and,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android' and purchase_type='f') 
else (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android' and purchase_type='f') END) 
AS wtd_acqM_rev_and,
SUM( CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS mtd_acqM_rev_and,
SUM( CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m1_acqM_rev_and,
SUM( CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Android' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m2_acqM_rev_and,

-- iOS
ROUND(SUM(CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS yest_revMiOS,
ROUND(SUM(CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS d2_revMiOS,
ROUND(SUM(CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS d3_revMiOS,
ROUND(max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS') 
else (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) 
and order_channel='mobile-app' and browser_info='iOS') END),0) AS wtd_revMiOS,
ROUND(SUM(CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS mtd_revMiOS,
ROUND(SUM(CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS m1_revMiOS,
ROUND(SUM(CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS m2_revMiOS,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS yest_ordersMiOS,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS d2_ordersMiOS,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS d3_ordersMiOS,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct order_group_id) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS') 
else (select count(distinct order_group_id) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS') END)
AS wtd_ordersMiOS,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS mtd_ordersMiOS,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS m1_ordersMiOS,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS m2_ordersMiOS,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN idcustomer END) AS yest_custmios,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN idcustomer END) AS d2_custmios,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='iOS' THEN idcustomer END) AS d3_custmios,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS') 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS') END) 
AS wtd_custMios,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='iOS' THEN idcustomer END) AS mtd_custmios,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN idcustomer END) AS m1_custmios,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN idcustomer END) AS m2_custmios,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN idcustomer END) AS yest_acqmios,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN idcustomer END) AS d2_acqmios,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN idcustomer END) AS d3_acqmios,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS' and purchase_type='f') 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS' and purchase_type='f') END) 
AS wtd_acqmios,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN idcustomer END) AS mtd_acqmios,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN idcustomer END) AS m1_acqmios,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN idcustomer END) AS m2_acqmios,

SUM( CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS yest_acqM_rev_ios,
SUM( CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d2_acqM_rev_ios,
SUM( CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d3_acqM_rev_ios,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS' and purchase_type='f') 
else (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS' and purchase_type='f') END) 
AS wtd_acqM_rev_ios,
SUM( CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS mtd_acqM_rev_ios,
SUM( CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m1_acqM_rev_ios,
SUM( CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='iOS' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m2_acqM_rev_ios,


-- Windows
ROUND(SUM(CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS yest_revMwin,
ROUND(SUM(CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS d2_revMwin,
ROUND(SUM(CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS d3_revMwin,
ROUND(max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows') 
else (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) 
and order_channel='mobile-app' and browser_info='Windows') END),0) AS wtd_revMwin,
ROUND(SUM(CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS mtd_revMwin,
ROUND(SUM(CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS m1_revMwin,
ROUND(SUM(CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS m2_revMwin,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS yest_ordersMwin,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS d2_ordersMwin,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS d3_ordersMwin,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct order_group_id) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows') 
else (select count(distinct order_group_id) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows') END)
AS wtd_ordersMwin,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS mtd_ordersMwin,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS m1_ordersMwin,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS m2_ordersMwin,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN idcustomer END) AS yest_custmwin,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN idcustomer END) AS d2_custmwin,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Windows' THEN idcustomer END) AS d3_custmwin,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows') 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows') END) 
AS wtd_custMwin,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Windows' THEN idcustomer END) AS mtd_custmwin,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN idcustomer END) AS m1_custmwin,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN idcustomer END) AS m2_custmwin,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN idcustomer END) AS yest_acqmwin,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN idcustomer END) AS d2_acqmwin,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN idcustomer END) AS d3_acqmwin,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct idcustomer) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows' and purchase_type='f') 
else (select count(distinct idcustomer) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows' and purchase_type='f') END) 
AS wtd_acqmwin,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN idcustomer END) AS mtd_acqmwin,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN idcustomer END) AS m1_acqmwin,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN idcustomer END) AS m2_acqmwin,

SUM( CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS yest_acqM_rev_win,
SUM( CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d2_acqM_rev_win,
SUM( CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS d3_acqM_rev_win,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows' and purchase_type='f') 
else (select SUM( shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows' and purchase_type='f') END) 
AS wtd_acqM_rev_win,
SUM( CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS mtd_acqM_rev_win,
SUM( CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m1_acqM_rev_win,
SUM( CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Windows' and purchase_type='f' THEN shipped_order_revenue_inc_cashback END) AS m2_acqM_rev_win

FROM fact_order, dim_date d
WHERE order_created_date=d.full_date 
AND  d.month_diff<=2
AND (is_realised=1 OR is_shipped=1)
) X,

(SELECT
-- overall
SUM(CASE WHEN day_diff=1 THEN users END) AS yday_users,
SUM(CASE WHEN day_diff=2 THEN users END) AS d2_users,
SUM(CASE WHEN day_diff=3 THEN users END) AS d3_users,

AVG(CASE WHEN day_diff=1 THEN avgsessionduration END) AS yday_session_time,
AVG(CASE WHEN day_diff=2 THEN avgsessionduration END) AS d2_session_time,
AVG(CASE WHEN day_diff=3 THEN avgsessionduration END) AS d3_session_time,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select AVG(avgsessionduration) from ga_metrics g,dim_date
		where last7_days=1 AND g.date=full_date) 
else (select AVG(avgsessionduration) from ga_metrics g,dim_date
where week_to_date=1 and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_session_time,
AVG(CASE WHEN month_to_date=1 THEN avgsessionduration END) AS mtd_session_time,
AVG(CASE WHEN month_diff=1 THEN avgsessionduration END) AS m1_session_time,
AVG(CASE WHEN month_diff=2 THEN avgsessionduration END) AS m2_session_time,

SUM(CASE WHEN day_diff=1 THEN sessions END) AS yday_sessions,
SUM(CASE WHEN day_diff=2 THEN sessions END) AS d2_sessions,
SUM(CASE WHEN day_diff=3 THEN sessions END) AS d3_sessions,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(sessions) from ga_metrics g,dim_date
		where last7_days=1 AND g.date=full_date) 
else (select sum(sessions) from ga_metrics g,dim_date
where week_to_date=1 and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_sessions,
SUM(CASE WHEN month_to_date=1 THEN sessions END) AS mtd_sessions,
SUM(CASE WHEN month_diff=1 THEN sessions END) AS m1_sessions,
SUM(CASE WHEN month_diff=2 THEN sessions END) AS m2_sessions,

-- android
SUM(CASE WHEN day_diff=1 and app_platform='android' THEN users END) AS yday_users_and,
SUM(CASE WHEN day_diff=2 and app_platform='android' THEN users END) AS d2_users_and,
SUM(CASE WHEN day_diff=3 and app_platform='android' THEN users END) AS d3_users_and,

AVG(CASE WHEN day_diff=1 and app_platform='android' THEN avgsessionduration END) AS yday_session_time_and,
AVG(CASE WHEN day_diff=2 and app_platform='android' THEN avgsessionduration END) AS d2_session_time_and,
AVG(CASE WHEN day_diff=3 and app_platform='android' THEN avgsessionduration END) AS d3_session_time_and,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select AVG(avgsessionduration) from ga_metrics g,dim_date
		where last7_days=1 and app_platform='android' AND g.date=full_date) 
else (select AVG(avgsessionduration) from ga_metrics g,dim_date
where week_to_date=1 and app_platform='android' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_session_time_and,
AVG(CASE WHEN month_to_date=1 and app_platform='android' THEN avgsessionduration END) AS mtd_session_time_and,
AVG(CASE WHEN month_diff=1 and app_platform='android' THEN avgsessionduration END) AS m1_session_time_and,
AVG(CASE WHEN month_diff=2 and app_platform='android' THEN avgsessionduration END) AS m2_session_time_and,

SUM(CASE WHEN day_diff=1 and app_platform='android' THEN sessions END) AS yday_sessions_and,
SUM(CASE WHEN day_diff=2 and app_platform='android' THEN sessions END) AS d2_sessions_and,
SUM(CASE WHEN day_diff=3 and app_platform='android' THEN sessions END) AS d3_sessions_and,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(sessions) from ga_metrics g,dim_date
		where last7_days=1 and app_platform='android' AND g.date=full_date) 
else (select sum(sessions) from ga_metrics g,dim_date
where week_to_date=1 and app_platform='android' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_sessions_and,
SUM(CASE WHEN month_to_date=1 and app_platform='android' THEN sessions END) AS mtd_sessions_and,
SUM(CASE WHEN month_diff=1 and app_platform='android' THEN sessions END) AS m1_sessions_and,
SUM(CASE WHEN month_diff=2 and app_platform='android' THEN sessions END) AS m2_sessions_and,

-- iOS
SUM(CASE WHEN day_diff=1 and app_platform='iOS' THEN users END) AS yday_users_iOS,
SUM(CASE WHEN day_diff=2 and app_platform='iOS' THEN users END) AS d2_users_iOS,
SUM(CASE WHEN day_diff=3 and app_platform='iOS' THEN users END) AS d3_users_iOS,

AVG(CASE WHEN day_diff=1 and app_platform='iOS' THEN avgsessionduration END) AS yday_session_time_iOS,
AVG(CASE WHEN day_diff=2 and app_platform='iOS' THEN avgsessionduration END) AS d2_session_time_iOS,
AVG(CASE WHEN day_diff=3 and app_platform='iOS' THEN avgsessionduration END) AS d3_session_time_iOS,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select AVG(avgsessionduration) from ga_metrics g,dim_date
		where last7_days=1 and app_platform='iOS' AND g.date=full_date) 
else (select AVG(avgsessionduration) from ga_metrics g,dim_date
where week_to_date=1 and app_platform='iOS' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_session_time_iOS,
AVG(CASE WHEN month_to_date=1 and app_platform='iOS' THEN avgsessionduration END) AS mtd_session_time_iOS,
AVG(CASE WHEN month_diff=1 and app_platform='iOS' THEN avgsessionduration END) AS m1_session_time_iOS,
AVG(CASE WHEN month_diff=2 and app_platform='iOS' THEN avgsessionduration END) AS m2_session_time_iOS,

SUM(CASE WHEN day_diff=1 and app_platform='iOS' THEN sessions END) AS yday_sessions_iOS,
SUM(CASE WHEN day_diff=2 and app_platform='iOS' THEN sessions END) AS d2_sessions_iOS,
SUM(CASE WHEN day_diff=3 and app_platform='iOS' THEN sessions END) AS d3_sessions_iOS,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(sessions) from ga_metrics g,dim_date
		where last7_days=1 and app_platform='iOS' AND g.date=full_date) 
else (select sum(sessions) from ga_metrics g,dim_date
where week_to_date=1 and app_platform='iOS' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_sessions_iOS,
SUM(CASE WHEN month_to_date=1 and app_platform='iOS' THEN sessions END) AS mtd_sessions_iOS,
SUM(CASE WHEN month_diff=1 and app_platform='iOS' THEN sessions END) AS m1_sessions_iOS,
SUM(CASE WHEN month_diff=2 and app_platform='iOS' THEN sessions END) AS m2_sessions_iOS,

-- windows

SUM(CASE WHEN day_diff=1 and app_platform='windows' THEN users END) AS yday_users_windows,
SUM(CASE WHEN day_diff=2 and app_platform='windows' THEN users END) AS d2_users_windows,
SUM(CASE WHEN day_diff=3 and app_platform='windows' THEN users END) AS d3_users_windows,

AVG(CASE WHEN day_diff=1 and app_platform='windows' THEN avgsessionduration END) AS yday_session_time_windows,
AVG(CASE WHEN day_diff=2 and app_platform='windows' THEN avgsessionduration END) AS d2_session_time_windows,
AVG(CASE WHEN day_diff=3 and app_platform='windows' THEN avgsessionduration END) AS d3_session_time_windows,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select AVG(avgsessionduration) from ga_metrics g,dim_date
		where last7_days=1 and app_platform='windows' AND g.date=full_date) 
else (select AVG(avgsessionduration) from ga_metrics g,dim_date
where week_to_date=1 and app_platform='windows' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_session_time_windows,
AVG(CASE WHEN month_to_date=1 and app_platform='windows' THEN avgsessionduration END) AS mtd_session_time_windows,
AVG(CASE WHEN month_diff=1 and app_platform='windows' THEN avgsessionduration END) AS m1_session_time_windows,
AVG(CASE WHEN month_diff=2 and app_platform='windows' THEN avgsessionduration END) AS m2_session_time_windows,

SUM(CASE WHEN day_diff=1 and app_platform='windows' THEN sessions END) AS yday_sessions_Windows,
SUM(CASE WHEN day_diff=2 and app_platform='windows' THEN sessions END) AS d2_sessions_Windows,
SUM(CASE WHEN day_diff=3 and app_platform='windows' THEN sessions END) AS d3_sessions_Windows,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(sessions) from ga_metrics g,dim_date
		where last7_days=1 and app_platform='windows' AND g.date=full_date) 
else (select sum(sessions) from ga_metrics g,dim_date
where week_to_date=1 and app_platform='windows' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_sessions_Windows,
SUM(CASE WHEN month_to_date=1 and app_platform='windows' THEN sessions END) AS mtd_sessions_Windows,
SUM(CASE WHEN month_diff=1 and app_platform='windows' THEN sessions END) AS m1_sessions_Windows,
SUM(CASE WHEN month_diff=2 and app_platform='windows' THEN sessions END) AS m2_sessions_Windows


FROM ga_metrics ga, dim_date d
WHERE
ga.date=d.full_date and
d.month_diff<=2
and app_platform in ('android','iOS','windows')
) Y ,

(select
 -- All App
SUM(CASE WHEN day_diff=1 THEN migrations END) AS yest_mig,
SUM(CASE WHEN day_diff=2 THEN migrations END) AS d2_mig,
SUM(CASE WHEN day_diff=3 THEN migrations END) AS d3_mig,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and dd.last7_days=1) 
else (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and dd.week_to_date=1)
		END) AS wtd_mig,
SUM(CASE WHEN month_to_date=1 THEN migrations END) AS mtd_mig,
SUM(CASE WHEN month_diff=1 THEN migrations END) AS m1_mig,
SUM(CASE WHEN month_diff=2 THEN migrations END) AS m2_mig,

-- Android
SUM(CASE WHEN day_diff=1 and browser_info='Android' THEN migrations END) AS yest_mig_and,
SUM(CASE WHEN day_diff=2 and browser_info='Android' THEN migrations END) AS d2_mig_and,
SUM(CASE WHEN day_diff=3 and browser_info='Android' THEN migrations END) AS d3_mig_and,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='Android' and dd.last7_days=1) 
else (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='Android' and dd.week_to_date=1)
		END) AS wtd_mig_and,
SUM(CASE WHEN month_to_date=1 and browser_info='Android' THEN migrations END) AS mtd_mig_and,
SUM(CASE WHEN month_diff=1 and browser_info='Android' THEN migrations END) AS m1_mig_and,
SUM(CASE WHEN month_diff=2 and browser_info='Android' THEN migrations END) AS m2_mig_and,

-- iOS
SUM(CASE WHEN day_diff=1 and browser_info='iOS' THEN migrations END) AS yest_mig_ios,
SUM(CASE WHEN day_diff=2 and browser_info='iOS' THEN migrations END) AS d2_mig_ios,
SUM(CASE WHEN day_diff=3 and browser_info='iOS' THEN migrations END) AS d3_mig_ios,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='iOS' and dd.last7_days=1) 
else (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='iOS' and dd.week_to_date=1)
		END) AS wtd_mig_ios,
SUM(CASE WHEN month_to_date=1 and browser_info='iOS' THEN migrations END) AS mtd_mig_ios,
SUM(CASE WHEN month_diff=1 and browser_info='iOS' THEN migrations END) AS m1_mig_ios,
SUM(CASE WHEN month_diff=2 and browser_info='iOS' THEN migrations END) AS m2_mig_ios,

-- Windows
SUM(CASE WHEN day_diff=1 and browser_info='Windows' THEN migrations END) AS yest_mig_win,
SUM(CASE WHEN day_diff=2 and browser_info='Windows' THEN migrations END) AS d2_mig_win,
SUM(CASE WHEN day_diff=3 and browser_info='Windows' THEN migrations END) AS d3_mig_win,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='Windows' and dd.last7_days=1) 
else (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='Windows' and dd.week_to_date=1)
		END) AS wtd_mig_win,
SUM(CASE WHEN month_to_date=1 and browser_info='Windows' THEN migrations END) AS mtd_mig_win,
SUM(CASE WHEN month_diff=1 and browser_info='Windows' THEN migrations END) AS m1_mig_win,
SUM(CASE WHEN month_diff=2 and browser_info='Windows' THEN migrations END) AS m2_mig_win

from 
(select browser_info,full_date,count (distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) 
										as migrations 
from fact_order f, dim_date dd
where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and dd.month_diff<=2 and dd.day_diff>0 
group by 1,2) mg 
left join dim_date d
on mg.full_date=d.full_date) Z,

(Select 
max(case when startdate=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD') then
	 (select sum(users) from ga_metric_weekly g,dim_date where cur_week_minus_1_week=1 AND g.startdate=full_date and app_platform in ('android','iOS','windows')) 
		else (select sum(users) from ga_metric_weekly g,dim_date where week_to_date=1 and g.startdate=full_date and app_platform in ('android','iOS','windows')) END) AS wtd_users,
max(case when startdate=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD') then
	 (select sum(users) from ga_metric_weekly g,dim_date where cur_week_minus_1_week=1 AND g.startdate=full_date and app_platform='android') 
		else (select sum(users) from ga_metric_weekly g,dim_date where week_to_date=1 and g.startdate=full_date and app_platform='android') END) AS wtd_users_and,
max(case when startdate=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD') then
	 (select sum(users) from ga_metric_weekly g,dim_date where cur_week_minus_1_week=1 AND g.startdate=full_date and app_platform='iOS') 
		else (select sum(users) from ga_metric_weekly g,dim_date where week_to_date=1 and g.startdate=full_date and app_platform='iOS') END) AS wtd_users_ios,
max(case when startdate=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD') then
	 (select sum(users) from ga_metric_weekly g,dim_date where cur_week_minus_1_week=1 AND g.startdate=full_date and app_platform='windows') 
		else (select sum(users) from ga_metric_weekly g,dim_date where week_to_date=1 and g.startdate=full_date and app_platform='windows') END) AS wtd_users_windows
from ga_metric_weekly) U1,

(Select 
--Overall
SUM(CASE WHEN month_to_date=1 and app_platform in ('android','iOS','windows') THEN users END) AS mtd_users,
SUM(CASE WHEN month_diff=1 and app_platform in ('android','iOS','windows') THEN users END) AS m1_users,
SUM(CASE WHEN month_diff=2 and app_platform in ('android','iOS','windows') THEN users END) AS m2_users,

--Android
SUM(CASE WHEN month_to_date=1 and app_platform='android' THEN users END) AS mtd_users_and,
SUM(CASE WHEN month_diff=1 and app_platform='android' THEN users END) AS m1_users_and,
SUM(CASE WHEN month_diff=2 and app_platform='android' THEN users END) AS m2_users_and,

--iOS
SUM(CASE WHEN month_to_date=1 and app_platform='iOS' THEN users END) AS mtd_users_ios,
SUM(CASE WHEN month_diff=1 and app_platform='iOS' THEN users END) AS m1_users_ios,
SUM(CASE WHEN month_diff=2 and app_platform='iOS' THEN users END) AS m2_users_ios,

--windows
SUM(CASE WHEN month_to_date=1 and app_platform='windows' THEN users END) AS mtd_users_windows,
SUM(CASE WHEN month_diff=1 and app_platform='windows' THEN users END) AS m1_users_windows,
SUM(CASE WHEN month_diff=2 and app_platform='windows' THEN users END) AS m2_users_windows

 from ga_metric_monthly g , dim_date d where g.startdate=full_date) U2

/*
(select
--All App
COUNT(CASE WHEN day_diff=1 THEN 1 END) AS yest_ins,
COUNT(CASE WHEN day_diff=2 THEN 1 END) AS d2_ins,
COUNT(CASE WHEN day_diff=3 THEN 1 END) AS d3_ins,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.last7_days=1) 
else (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.week_to_date=1) end) AS wtd_ins,
COUNT(CASE WHEN month_to_date=1 THEN 1 END) AS mtd_ins,
COUNT(CASE WHEN month_diff=1 THEN 1 END) AS m1_ins,
COUNT(CASE WHEN month_diff=2 THEN 1 END) AS m2_ins,

-- Android
COUNT(CASE WHEN day_diff=1 and platform='Android' THEN 1 END) AS yest_ins_and,
COUNT(CASE WHEN day_diff=2 and platform='Android' THEN 1 END) AS d2_ins_and,
COUNT(CASE WHEN day_diff=3 and platform='Android' THEN 1 END) AS d3_ins_and,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.last7_days=1 and platform='Android') 
else (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.week_to_date=1 and platform='Android') end) AS wtd_ins_and,
COUNT(CASE WHEN month_to_date=1 and platform='Android' THEN 1 END) AS mtd_ins_and,
COUNT(CASE WHEN month_diff=1 and platform='Android' THEN 1 END) AS m1_ins_and,
COUNT(CASE WHEN month_diff=2 and platform='Android' THEN 1 END) AS m2_ins_and,

-- iOS
COUNT(CASE WHEN day_diff=1 and platform='iOS' THEN 1 END) AS yest_ins_ios,
COUNT(CASE WHEN day_diff=2 and platform='iOS' THEN 1 END) AS d2_ins_ios,
COUNT(CASE WHEN day_diff=3 and platform='iOS' THEN 1 END) AS d3_ins_ios,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.last7_days=1 and platform='iOS') 
else (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.week_to_date=1 and platform='iOS') end) AS wtd_ins_ios,
COUNT(CASE WHEN month_to_date=1 and platform='iOS' THEN 1 END) AS mtd_ins_ios,
COUNT(CASE WHEN month_diff=1 and platform='iOS' THEN 1 END) AS m1_ins_ios,
COUNT(CASE WHEN month_diff=2 and platform='iOS' THEN 1 END) AS m2_ins_ios,

-- Windows
COUNT(CASE WHEN day_diff=1 and platform='Windows' THEN 1 END) AS yest_ins_win,
COUNT(CASE WHEN day_diff=2 and platform='Windows' THEN 1 END) AS d2_ins_win,
COUNT(CASE WHEN day_diff=3 and platform='Windows' THEN 1 END) AS d3_ins_win,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.last7_days=1 and platform='Windows') 
else (select count(*) from fact_app_install_from_apsalar ins, dim_date d
			where ins.load_date=d.full_date and d.week_to_date=1 and platform='Windows') end) AS wtd_ins_win,
COUNT(CASE WHEN month_to_date=1 and platform='Windows' THEN 1 END) AS mtd_ins_win,
COUNT(CASE WHEN month_diff=1 and platform='Windows' THEN 1 END) AS m1_ins_win,
COUNT(CASE WHEN month_diff=2 and platform='Windows' THEN 1 END) AS m2_ins_win

from fact_app_install_from_apsalar ins, dim_date d
where ins.load_date=d.full_date and month_diff<=2 and platform in ('Android','iOS','Windows') )I
*/
