(select
 -- All App
SUM(CASE WHEN day_diff=1 THEN migrations END) AS yest_mig,
SUM(CASE WHEN day_diff=2 THEN migrations END) AS d2_mig,
SUM(CASE WHEN day_diff=3 THEN migrations END) AS d3_mig,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and dd.last7_days=1) 
else (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and dd.week_to_date=1)
		END) AS wtd_mig,
SUM(CASE WHEN month_to_date=1 THEN migrations END) AS mtd_mig,
SUM(CASE WHEN month_diff=1 THEN migrations END) AS m1_mig,
SUM(CASE WHEN month_diff=2 THEN migrations END) AS m2_mig,

-- Android
SUM(CASE WHEN day_diff=1 and browser_info='Android' THEN migrations END) AS yest_mig_and,
SUM(CASE WHEN day_diff=2 and browser_info='Android' THEN migrations END) AS d2_mig_and,
SUM(CASE WHEN day_diff=3 and browser_info='Android' THEN migrations END) AS d3_mig_and,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='Android' and dd.last7_days=1) 
else (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='Android' and dd.week_to_date=1)
		END) AS wtd_mig_and,
SUM(CASE WHEN month_to_date=1 and browser_info='Android' THEN migrations END) AS mtd_mig_and,
SUM(CASE WHEN month_diff=1 and browser_info='Android' THEN migrations END) AS m1_mig_and,
SUM(CASE WHEN month_diff=2 and browser_info='Android' THEN migrations END) AS m2_mig_and,

-- iOS
SUM(CASE WHEN day_diff=1 and browser_info='iOS' THEN migrations END) AS yest_mig_ios,
SUM(CASE WHEN day_diff=2 and browser_info='iOS' THEN migrations END) AS d2_mig_ios,
SUM(CASE WHEN day_diff=3 and browser_info='iOS' THEN migrations END) AS d3_mig_ios,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='iOS' and dd.last7_days=1) 
else (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='iOS' and dd.week_to_date=1)
		END) AS wtd_mig_ios,
SUM(CASE WHEN month_to_date=1 and browser_info='iOS' THEN migrations END) AS mtd_mig_ios,
SUM(CASE WHEN month_diff=1 and browser_info='iOS' THEN migrations END) AS m1_mig_ios,
SUM(CASE WHEN month_diff=2 and browser_info='iOS' THEN migrations END) AS m2_mig_ios,

-- Windows
SUM(CASE WHEN day_diff=1 and browser_info='Windows' THEN migrations END) AS yest_mig_win,
SUM(CASE WHEN day_diff=2 and browser_info='Windows' THEN migrations END) AS d2_mig_win,
SUM(CASE WHEN day_diff=3 and browser_info='Windows' THEN migrations END) AS d3_mig_win,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='Windows' and dd.last7_days=1) 
else (select count(distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) as migrations 
from fact_order f, dim_date dd where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and browser_info='Windows' and dd.week_to_date=1)
		END) AS wtd_mig_win,
SUM(CASE WHEN month_to_date=1 and browser_info='Windows' THEN migrations END) AS mtd_mig_win,
SUM(CASE WHEN month_diff=1 and browser_info='Windows' THEN migrations END) AS m1_mig_win,
SUM(CASE WHEN month_diff=2 and browser_info='Windows' THEN migrations END) AS m2_mig_win

from 
(select browser_info,full_date,count (distinct case when (select count(1) from fact_order fo2 where fo2.order_created_date <dd.full_date and fo2.order_channel = 'mobile-app' and fo2.idcustomer=f.idcustomer)=0 
												then idcustomer end) 
										as migrations 
from fact_order f, dim_date dd
where f.purchase_type='r' and f.order_created_date = dd.full_date and (is_shipped=1 or is_realised=1) and order_channel = 'mobile-app' and dd.month_diff<=2 and dd.day_diff>0 
group by 1,2) mg 
left join dim_date d
on mg.full_date=d.full_date) Z;
