/*D81 App Metrics*/
SELECT
-- Overall
to_char(yest_revM,'99,99,99,99,999') AS yday_rev,
ROUND(yest_revM*100/yest_rev,2) as o1,
to_char(d2_revM,'99,99,99,99,999') AS d2_rev,
ROUND(d2_revM*100/d2_rev,2) as o2,
to_char(d3_revM,'99,99,99,99,999') AS d3_rev,
ROUND(d3_revM*100/d3_rev,2) as o3,
to_char(wtd_revM,'99,99,99,99,999') AS wtd_rev,
ROUND(wtd_revM*100/wtd_rev,2) as o4,
to_char(mtd_revM,'99,99,99,99,999') AS mtd_rev,
ROUND(mtd_revM*100/mtd_rev,2) as o5,
to_char(m1_revM,'99,99,99,99,999') AS m1_rev,
ROUND(m1_revM*100/m1_rev,2) as o6,


to_char(yest_ordersM,'99,99,99,99,999') AS yday_orders,
ROUND(yest_ordersM*100/yest_orders,2) as o7,
to_char(d2_ordersM,'99,99,99,99,999') AS d2_orders,
ROUND(d2_ordersM*100/d2_orders,2) as o8,
to_char(d3_ordersM,'99,99,99,99,999') AS d3_orders,
ROUND(d3_ordersM*100/d3_orders,2) as o9,
to_char(wtd_ordersM,'99,99,99,99,999') AS wtd_orders,
ROUND(wtd_ordersM*100/wtd_orders,2) as o10,
to_char(mtd_ordersM,'99,99,99,99,999') AS mtd_orders,
ROUND(mtd_ordersM*100/mtd_orders,2) as o11,
to_char(m1_ordersM,'99,99,99,99,999') AS m1_orders,
ROUND(m1_ordersM*100/m1_orders,2) as o12,


to_char(yest_revM/yest_ordersM,'99,99,99,99,999') AS yday_asp,
to_char(d2_revM/d2_ordersM,'99,99,99,99,999') AS d2_asp,
to_char(d3_revM/d3_ordersM,'99,99,99,99,999') AS d3_asp,
to_char(wtd_revM/wtd_ordersM,'99,99,99,99,999') AS wtd_asp,
to_char(mtd_revM/mtd_ordersM,'99,99,99,99,999') AS mtd_asp,
to_char(m1_revM/m1_ordersM,'99,99,99,99,999') AS m1_asp,
-- to_char(m2_revM/m2_ordersM,'99,99,99,99,999') AS m2_asp,

round(yest_ordersM*100/yday_sessions,2) as yday_conv,
round(d2_ordersM*100/d2_sessions,2) as d2_conv,
round(d3_ordersM*100/d3_sessions,2) as d3_conv,
round(wtd_ordersM*100/wtd_sessions,2) as wtd_conv,
round(mtd_ordersM*100/mtd_sessions,2) as mtd_conv,
round(m1_ordersM*100/m1_sessions,2) as m1_conv,
-- round(m2_ordersM*100/m2_sessions,2) as m2_conv,

to_char(yday_ins,'99,99,99,99,999') as yday_ins,
to_char(d2_ins,'99,99,99,99,999') as d2_ins,
to_char(d3_ins,'99,99,99,99,999') as d3_ins,
to_char(wtd_ins,'99,99,99,99,999') as wtd_ins,
to_char(mtd_ins,'99,99,99,99,999') as mtd_ins,
to_char(m1_ins,'99,99,99,99,999') as m1_ins,
-- to_char(m2_ins,'99,99,99,99,999') as m2_ins,

to_char(yday_users,'99,99,99,99,999') as yday_users,
to_char(d2_users,'99,99,99,99,999') as d2_users,
to_char(d3_users,'99,99,99,99,999') as d3_users,
to_char(wtd_users,'99,99,99,99,999') as wtd_users,
to_char(mtd_users,'99,99,99,99,999') as mtd_users,
to_char(m1_users,'99,99,99,99,999') as m1_users,
-- to_char(m2_users,'99,99,99,99,999') as m2_users,

to_char(yday_sessions,'99,99,99,99,999') as yday_sessions,
to_char(d2_sessions,'99,99,99,99,999') as d2_sessions,
to_char(d3_sessions,'99,99,99,99,999') as d3_sessions,
to_char(wtd_sessions,'99,99,99,99,999') as wtd_sessions,
to_char(mtd_sessions,'99,99,99,99,999') as mtd_sessions,
to_char(m1_sessions,'99,99,99,99,999') as m1_sessions,
-- to_char(m2_sessions,'99,99,99,99,999') as m2_sessions,

-- Android
to_char(yest_revMand,'99,99,99,99,999') AS yday_and_rev,
ROUND(yest_revMand*100/yest_revM,2) as a1,
to_char(d2_revMand,'99,99,99,99,999') AS d2_and_rev,
ROUND(d2_revMand*100/d2_revM,2) as a2,
to_char(d3_revMand,'99,99,99,99,999') AS d3_and_rev,
ROUND(d3_revMand*100/d3_revM,2) as a3,
to_char(wtd_revMand,'99,99,99,99,999') AS wtd_and_rev,
ROUND(wtd_revMand*100/wtd_revM,2) as a4,
to_char(mtd_revMand,'99,99,99,99,999') AS mtd_and_rev,
ROUND(mtd_revMand*100/mtd_revM,2) as a5,
to_char(m1_revMand,'99,99,99,99,999') AS m1_and_rev,
ROUND(m1_revMand*100/m1_revM,2) as a6,


to_char(yest_ordersMand,'99,99,99,99,999') AS yday_and_orders,
ROUND(yest_ordersMand*100/yest_ordersM,2) a7,

to_char(d2_ordersMand,'99,99,99,99,999') AS d2_and_orders,
ROUND(d2_ordersMand*100/d2_ordersM,2) as a8,

to_char(d3_ordersMand,'99,99,99,99,999') AS d3_and_orders,
ROUND(d3_ordersMand*100/d3_ordersM,2) as a9,

to_char(wtd_ordersMand,'99,99,99,99,999') AS wtd_and_orders,
ROUND(wtd_ordersMand*100/wtd_ordersM,2) as a10,

to_char(mtd_ordersMand,'99,99,99,99,999') AS mtd_and_orders,
ROUND(mtd_ordersMand*100/mtd_ordersM,2) as a11,

to_char(m1_ordersMand,'99,99,99,99,999') AS m1_and_orders,
ROUND(m1_ordersMand*100/m1_ordersM,2) as a12,


to_char(yest_revMand/yest_ordersMand,'99,99,99,99,999') AS yday_and_asp,
to_char(d2_revMand/d2_ordersMand,'99,99,99,99,999') AS d2_and_asp,
to_char(d3_revMand/d3_ordersMand,'99,99,99,99,999') AS d3_and_asp,
to_char(wtd_revMand/wtd_ordersMand,'99,99,99,99,999') AS wtd_and_asp,
to_char(mtd_revMand/mtd_ordersMand,'99,99,99,99,999') AS mtd_and_asp,
to_char(m1_revMand/m1_ordersMand,'99,99,99,99,999') AS m1_and_asp,
-- to_char(m2_revMand/m2_ordersMand,'99,99,99,99,999') AS m2_and_asp,

round(yest_ordersMand*100/yday_sessions_and,2) as yday_conv_and,
round(d2_ordersMand*100/d2_sessions_and,2) as d2_conv_and,
round(d3_ordersMand*100/d3_sessions_and,2) as d3_conv_and,
round(wtd_ordersMand*100/wtd_sessions_and,2) as wtd_conv_and,
round(mtd_ordersMand*100/mtd_sessions_and,2) as mtd_conv_and,
round(m1_ordersMand*100/m1_sessions_and,2) as m1_conv_and,
-- round(m2_ordersMand*100/m2_sessions_and,2) as m2_conv_and,

to_char(yday_ins_and,'99,99,99,99,999') as yday_ins_and,
to_char(d2_ins_and,'99,99,99,99,999') as d2_ins_and,
to_char(d3_ins_and,'99,99,99,99,999') as d3_ins_and,
to_char(wtd_ins_and,'99,99,99,99,999') as wtd_ins_and,
to_char(mtd_ins_and,'99,99,99,99,999') as mtd_ins_and,
to_char(m1_ins_and,'99,99,99,99,999') as m1_ins_and,
-- to_char(m2_ins_and,'99,99,99,99,999') as m2_ins_and,

to_char(yday_users_and,'99,99,99,99,999') as yday_users_and,
to_char(d2_users_and,'99,99,99,99,999') as d2_users_and,
to_char(d3_users_and,'99,99,99,99,999') as d3_users_and,
to_char(wtd_users_and,'99,99,99,99,999') as wtd_users_and,
to_char(mtd_users_and,'99,99,99,99,999') as mtd_users_and,
to_char(m1_users_and,'99,99,99,99,999') as m1_users_and,
-- to_char(m2_users_and,'99,99,99,99,999') as m2_users_and,

to_char(yday_sessions_and,'99,99,99,99,999') as yday_sessions_and,
to_char(d2_sessions_and,'99,99,99,99,999') as d2_sessions_and,
to_char(d3_sessions_and,'99,99,99,99,999') as d3_sessions_and,
to_char(wtd_sessions_and,'99,99,99,99,999') as wtd_sessions_and,
to_char(mtd_sessions_and,'99,99,99,99,999') as mtd_sessions_and,
to_char(m1_sessions_and,'99,99,99,99,999') as m1_sessions_and,
-- to_char(m2_sessions_and,'99,99,99,99,999') as m2_sessions_and,

-- iOS
to_char(yest_revMiOS,'99,99,99,99,999') AS yday_iOS_rev,
ROUND(yest_revMiOS*100/yest_revM,2) as i1,

to_char(d2_revMiOS,'99,99,99,99,999') AS d2_iOS_rev,
ROUND(d2_revMiOS*100/d2_revM,2) as i2,

to_char(d3_revMiOS,'99,99,99,99,999') AS d3_iOS_rev,
ROUND(d3_revMiOS*100/d3_revM,2) as i3,

to_char(wtd_revMiOS,'99,99,99,99,999') AS wtd_iOS_rev,
ROUND(wtd_revMiOS*100/wtd_revM,2) as i4,

to_char(mtd_revMiOS,'99,99,99,99,999') AS mtd_iOS_rev,
ROUND(mtd_revMiOS*100/mtd_revM,2) as i5,

to_char(m1_revMiOS,'99,99,99,99,999') AS m1_iOS_rev,
ROUND(m1_revMiOS*100/m1_revM,2) as i6,


to_char(yest_ordersMiOS,'99,99,99,99,999') AS yday_iOS_orders,
ROUND(yest_ordersMiOS*100/yest_ordersM,2) as i7,

to_char(d2_ordersMiOS,'99,99,99,99,999') AS d2_iOS_orders,
ROUND(d2_ordersMiOS*100/d2_ordersM,2) as i8,

to_char(d3_ordersMiOS,'99,99,99,99,999') AS d3_iOS_orders,
ROUND(d3_ordersMiOS*100/d3_ordersM,2) as i9,

to_char(wtd_ordersMiOS,'99,99,99,99,999') AS wtd_iOS_orders,
ROUND(wtd_ordersMiOS*100/wtd_ordersM,2) as i10,

to_char(mtd_ordersMiOS,'99,99,99,99,999') AS mtd_iOS_orders,
ROUND(mtd_ordersMiOS*100/mtd_ordersM,2) as i11,

to_char(m1_ordersMiOS,'99,99,99,99,999') AS m1_iOS_orders,
ROUND(m1_ordersMiOS*100/m1_ordersM,2) as i12,

to_char(yest_revMiOS/yest_ordersMiOS,'99,99,99,99,999') AS yday_iOS_asp,
to_char(d2_revMiOS/d2_ordersMiOS,'99,99,99,99,999') AS d2_iOS_asp,
to_char(d3_revMiOS/d3_ordersMiOS,'99,99,99,99,999') AS d3_iOS_asp,
to_char(wtd_revMiOS/wtd_ordersMiOS,'99,99,99,99,999') AS wtd_iOS_asp,
to_char(mtd_revMiOS/mtd_ordersMiOS,'99,99,99,99,999') AS mtd_iOS_asp,
to_char(m1_revMiOS/m1_ordersMiOS,'99,99,99,99,999') AS m1_iOS_asp,
-- to_char(m2_revMiOS/m2_ordersMiOS,'99,99,99,99,999') AS m2_iOS_asp,

round(yest_ordersMiOS*100/yday_sessions_iOS,2) as yday_conv_iOS,
round(d2_ordersMiOS*100/d2_sessions_iOS,2) as d2_conv_iOS,
round(d3_ordersMiOS*100/d3_sessions_iOS,2) as d3_conv_iOS,
round(wtd_ordersMiOS*100/wtd_sessions_iOS,2) as wtd_conv_iOS,
round(mtd_ordersMiOS*100/mtd_sessions_iOS,2) as mtd_conv_iOS,
round(m1_ordersMiOS*100/m1_sessions_iOS,2) as m1_conv_iOS,
-- round(m2_ordersMiOS*100/m2_sessions_iOS,2) as m2_conv_iOS,

to_char(yday_ins_iOS,'99,99,99,99,999') AS yday_ins_iOS,
to_char(d2_ins_iOS,'99,99,99,99,999') AS d2_ins_iOS,
to_char(d3_ins_iOS,'99,99,99,99,999') AS d3_ins_iOS,
to_char(wtd_ins_iOS,'99,99,99,99,999') AS wtd_ins_iOS,
to_char(mtd_ins_iOS,'99,99,99,99,999') AS mtd_ins_iOS,
to_char(m1_ins_iOS,'99,99,99,99,999') AS m1_ins_iOS,
-- to_char(m2_ins_iOS,'99,99,99,99,999') AS m2_ins_iOS,
 
to_char(yday_users_iOS,'99,99,99,99,999') AS yday_users_iOS,
to_char(d2_users_iOS,'99,99,99,99,999') AS d2_users_iOS,
to_char(d3_users_iOS,'99,99,99,99,999') AS d3_users_iOS,
to_char(wtd_users_iOS,'99,99,99,99,999') AS wtd_users_iOS,
to_char(mtd_users_iOS,'99,99,99,99,999') AS mtd_users_iOS,
to_char(m1_users_iOS,'99,99,99,99,999') AS m1_users_iOS,
-- to_char(m2_users_iOS,'99,99,99,99,999') AS m2_users_iOS,

to_char(yday_sessions_iOS,'99,99,99,99,999') AS yday_sessions_iOS,
to_char(d2_sessions_iOS,'99,99,99,99,999') AS d2_sessions_iOS,
to_char(d3_sessions_iOS,'99,99,99,99,999') AS d3_sessions_iOS,
to_char(wtd_sessions_iOS,'99,99,99,99,999') AS wtd_sessions_iOS,
to_char(mtd_sessions_iOS,'99,99,99,99,999') AS mtd_sessions_iOS,
to_char(m1_sessions_iOS,'99,99,99,99,999') AS m1_sessions_iOS,
-- to_char(m2_sessions_iOS,'99,99,99,99,999') AS m2_sessions_iOS,
 
-- Windows
to_char(yest_revMwin,'99,99,99,99,999') AS yday_win_rev,
ROUND(yest_revMwin*100/yest_revM,2) as w1,

to_char(d2_revMwin,'99,99,99,99,999') AS d2_win_rev,
ROUND(d2_revMwin*100/d2_revM,2) as w2,

to_char(d3_revMwin,'99,99,99,99,999') AS d3_win_rev,
ROUND(d3_revMwin*100/d2_revM,2) as w3,

to_char(wtd_revMwin,'99,99,99,99,999') AS wtd_win_rev,
ROUND(wtd_revMwin*100/wtd_revM,2) as w4,

to_char(mtd_revMwin,'99,99,99,99,999') AS mtd_win_rev,
ROUND(mtd_revMwin*100/mtd_revM,2) as w5,

to_char(m1_revMwin,'99,99,99,99,999') AS m1_win_rev,
ROUND(m1_revMwin*100/m1_revM,2) as w6,

to_char(yest_ordersMwin,'99,99,99,99,999') AS yday_win_orders,
ROUND(yest_ordersMwin*100/yest_ordersM,2)  as w7,

to_char(d2_ordersMwin,'99,99,99,99,999') AS d2_win_orders,
ROUND(d2_ordersMwin*100/d2_ordersM,2) as w8,

to_char(d3_ordersMwin,'99,99,99,99,999') AS d3_win_orders,
 ROUND(d3_ordersMwin*100/d3_ordersM,2) as w9,

to_char(wtd_ordersMwin,'99,99,99,99,999') AS wtd_win_orders,
ROUND(wtd_ordersMwin*100/wtd_ordersM,2) as w10,

to_char(mtd_ordersMwin,'99,99,99,99,999') AS mtd_win_orders,
ROUND(mtd_ordersMwin*100/mtd_ordersM,2) as w11,

to_char(m1_ordersMwin,'99,99,99,99,999') AS m1_win_orders,
ROUND(m1_ordersMwin*100/m1_ordersM,2) as w12,

to_char(yest_revMwin/yest_ordersMwin,'99,99,99,99,999') AS yday_win_asp,
to_char(d2_revMwin/d2_ordersMwin,'99,99,99,99,999') AS d2_win_asp,
to_char(d3_revMwin/d3_ordersMwin,'99,99,99,99,999') AS d3_win_asp,
to_char(wtd_revMwin/wtd_ordersMwin,'99,99,99,99,999') AS wtd_win_asp,
to_char(mtd_revMwin/mtd_ordersMwin,'99,99,99,99,999') AS mtd_win_asp,
to_char(m1_revMwin/m1_ordersMwin,'99,99,99,99,999') AS m1_win_asp,

round(yest_ordersMwin*100/yday_sessions_Windows,2) as yday_conv_win,
round(d2_ordersMwin*100/d2_sessions_Windows,2) as d2_conv_win,
round(d3_ordersMwin*100/d3_sessions_Windows,2) as d3_conv_win,
round(wtd_ordersMwin*100/wtd_sessions_Windows,2) as wtd_conv_win,
round(mtd_ordersMwin*100/mtd_sessions_Windows,2) as mtd_conv_win,
round(m1_ordersMwin*100/m1_sessions_Windows,2) as m1_conv_win,

to_char(yday_ins_Windows,'99,99,99,99,999') AS yday_ins_Windows,
to_char(d2_ins_Windows,'99,99,99,99,999') AS d2_ins_Windows,
to_char(d3_ins_Windows,'99,99,99,99,999') AS d3_ins_Windows,
to_char(wtd_ins_Windows,'99,99,99,99,999') AS wtd_ins_Windows,
to_char(mtd_ins_Windows,'99,99,99,99,999') AS mtd_ins_Windows,
to_char(m1_ins_Windows,'99,99,99,99,999') as m1_ins_win,
-- to_char(m2_ins_Windows,'99,99,99,99,999') as m2_ins_win,

to_char(yday_users_Windows,'99,99,99,99,999') AS yday_users_Windows,
to_char(d2_users_Windows,'99,99,99,99,999') AS d2_users_Windows,
to_char(d3_users_Windows,'99,99,99,99,999') AS d3_users_Windows,
to_char(wtd_users_Windows,'99,99,99,99,999') AS wtd_users_Windows,
to_char(mtd_users_Windows,'99,99,99,99,999') AS mtd_users_Windows,
to_char(m1_users_Windows,'99,99,99,99,999') as m1_users_win,
-- to_char(m2_users_Windows,'99,99,99,99,999') as m2_users_win,
  
to_char(yday_sessions_Windows,'99,99,99,99,999') AS yday_sessions_Windows,
to_char(d2_sessions_Windows,'99,99,99,99,999') AS d2_sessions_Windows,
to_char(d3_sessions_Windows,'99,99,99,99,999') AS d3_sessions_Windows,
to_char(wtd_sessions_Windows,'99,99,99,99,999') AS wtd_sessions_Windows,
to_char(mtd_sessions_Windows,'99,99,99,99,999') AS mtd_sessions_Windows,
to_char(m1_sessions_Windows,'99,99,99,99,999') as m1_sessions_win
-- to_char(m2_sessions_Windows,'99,99,99,99,999') as m2_sessions_win


FROM
(SELECT 
-- All App
ROUND(SUM(CASE WHEN day_diff=1 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS yest_revM,
ROUND(SUM(CASE WHEN day_diff=2 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS d2_revM,
ROUND(SUM(CASE WHEN day_diff=3 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS d3_revM,
ROUND(max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app') 
else (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) 
and order_channel='mobile-app') END),0) AS wtd_revM,
ROUND(SUM(CASE WHEN month_to_date=1 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS mtd_revM,
ROUND(SUM(CASE WHEN month_diff=1 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS m1_revM,
ROUND(SUM(CASE WHEN month_diff=2 AND order_channel='mobile-app' THEN shipped_order_revenue_inc_cashback END),0) AS m2_revM,


COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' THEN order_group_id END) AS yest_ordersM,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' THEN order_group_id END) AS d2_ordersM,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' THEN order_group_id END) AS d3_ordersM,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct order_group_id) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app') 
else (select count(distinct order_group_id) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app') END) 
AS wtd_ordersM,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' THEN order_group_id END) AS mtd_ordersM,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' THEN order_group_id END) AS m1_ordersM,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' THEN order_group_id END) AS m2_ordersM,


-- Android
ROUND(SUM(CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS yest_revMand,
ROUND(SUM(CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS d2_revMand,
ROUND(SUM(CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS d3_revMand,
ROUND(max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android') 
else (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) 
and order_channel='mobile-app' and browser_info='Android') END),0) AS wtd_revMand,
ROUND(SUM(CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS mtd_revMand,
ROUND(SUM(CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS m1_revMand,
ROUND(SUM(CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN shipped_order_revenue_inc_cashback END),0) AS m2_revMand,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS yest_ordersMand,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS d2_ordersMand,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS d3_ordersMand,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct order_group_id) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android') 
else (select count(distinct order_group_id) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Android') END)
AS wtd_ordersMand,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS mtd_ordersMand,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS m1_ordersMand,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Android' THEN order_group_id END) AS m2_ordersMand,

-- iOS
ROUND(SUM(CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS yest_revMiOS,
ROUND(SUM(CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS d2_revMiOS,
ROUND(SUM(CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS d3_revMiOS,
ROUND(max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS') 
else (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) 
and order_channel='mobile-app' and browser_info='iOS') END),0) AS wtd_revMiOS,
ROUND(SUM(CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS mtd_revMiOS,
ROUND(SUM(CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS m1_revMiOS,
ROUND(SUM(CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN shipped_order_revenue_inc_cashback END),0) AS m2_revMiOS,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS yest_ordersMiOS,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS d2_ordersMiOS,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS d3_ordersMiOS,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct order_group_id) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS') 
else (select count(distinct order_group_id) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='iOS') END)
AS wtd_ordersMiOS,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS mtd_ordersMiOS,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS m1_ordersMiOS,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='iOS' THEN order_group_id END) AS m2_ordersMiOS,

-- Windows
ROUND(SUM(CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS yest_revMwin,
ROUND(SUM(CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS d2_revMwin,
ROUND(SUM(CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS d3_revMwin,
ROUND(max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows') 
else (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) 
and order_channel='mobile-app' and browser_info='Windows') END),0) AS wtd_revMwin,
ROUND(SUM(CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS mtd_revMwin,
ROUND(SUM(CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS m1_revMwin,
ROUND(SUM(CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN shipped_order_revenue_inc_cashback END),0) AS m2_revMwin,

COUNT(DISTINCT CASE WHEN day_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS yest_ordersMwin,
COUNT(DISTINCT CASE WHEN day_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS d2_ordersMwin,
COUNT(DISTINCT CASE WHEN day_diff=3 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS d3_ordersMwin,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct order_group_id) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows') 
else (select count(distinct order_group_id) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) and order_channel='mobile-app' and browser_info='Windows') END)
AS wtd_ordersMwin,
COUNT(DISTINCT CASE WHEN month_to_date=1 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS mtd_ordersMwin,
COUNT(DISTINCT CASE WHEN month_diff=1 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS m1_ordersMwin,
COUNT(DISTINCT CASE WHEN month_diff=2 AND order_channel='mobile-app' and browser_info='Windows' THEN order_group_id END) AS m2_ordersMwin,

-- Over all
SUM(CASE WHEN day_diff=1 THEN shipped_order_revenue_inc_cashback END) AS yest_rev,
SUM(CASE WHEN day_diff=2 THEN shipped_order_revenue_inc_cashback END) AS d2_rev,
SUM(CASE WHEN day_diff=3 THEN shipped_order_revenue_inc_cashback END) AS d3_rev,
ROUND(max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1)) 
else (select sum(shipped_order_revenue_inc_cashback) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1) 
) END),0) AS wtd_rev,
SUM(CASE WHEN month_to_date=1 THEN shipped_order_revenue_inc_cashback END) AS mtd_rev,
SUM(CASE WHEN month_diff=1 THEN shipped_order_revenue_inc_cashback END) AS m1_rev,
SUM(CASE WHEN month_diff=2 THEN shipped_order_revenue_inc_cashback END) AS m2_rev,

COUNT(DISTINCT CASE WHEN day_diff=1 THEN order_group_id END) AS yest_orders,
COUNT(DISTINCT CASE WHEN day_diff=2 THEN order_group_id END) AS d2_orders,
COUNT(DISTINCT CASE WHEN day_diff=3 THEN order_group_id END) AS d3_orders,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select count(distinct order_group_id) from fact_order,dim_date
		where last7_days=1 AND order_created_date=full_date and (is_realised=1 OR is_shipped=1)) 
else (select count(distinct order_group_id) from fact_order,dim_date
where week_to_date=1 and order_created_date=full_date and order_created_date!=to_char(sysdate,'YYYYMMDD') AND (is_realised=1 OR is_shipped=1)) END)
AS wtd_orders,
COUNT(DISTINCT CASE WHEN month_to_date=1 THEN order_group_id END) AS mtd_orders,
COUNT(DISTINCT CASE WHEN month_diff=1 THEN order_group_id END) AS m1_orders,
COUNT(DISTINCT CASE WHEN month_diff=2 THEN order_group_id END) AS m2_orders

FROM fact_order, dim_date d
WHERE order_created_date=d.full_date 
-- AND order_created_date>=20140507
AND  d.month_diff<=1
AND (is_realised=1 OR is_shipped=1)
and order_created_date >= to_char(dateadd(month,-1,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
) X,
(SELECT

-- overall
SUM(CASE WHEN day_diff=1 THEN newusers END) AS yday_ins,
SUM(CASE WHEN day_diff=2 THEN newusers END) AS d2_ins,
SUM(CASE WHEN day_diff=3 THEN newusers END) AS d3_ins,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(newusers) from ga_app_tracking g,dim_date
		where last7_days=1 AND g.date=full_date) 
else (select sum(newusers) from ga_app_tracking g,dim_date
where week_to_date=1 and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END) AS wtd_ins,
SUM(CASE WHEN month_to_date=1 THEN newusers END) AS mtd_ins,
SUM(CASE WHEN month_diff=1 THEN newusers END) AS m1_ins,
SUM(CASE WHEN month_diff=2 THEN newusers END) AS m2_ins,

SUM(CASE WHEN day_diff=1 THEN users END) AS yday_users,
SUM(CASE WHEN day_diff=2 THEN users END) AS d2_users,
SUM(CASE WHEN day_diff=3 THEN users END) AS d3_users,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(users) from ga_app_tracking g,dim_date
		where last7_days=1 AND g.date=full_date) 
else (select sum(users) from ga_app_tracking g,dim_date
where week_to_date=1 and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END) AS wtd_users,
SUM(CASE WHEN month_to_date=1 THEN users END) AS mtd_users,
SUM(CASE WHEN month_diff=1 THEN users END) AS m1_users,
SUM(CASE WHEN month_diff=2 THEN users END) AS m2_users,

SUM(CASE WHEN day_diff=1 THEN sessions END) AS yday_sessions,
SUM(CASE WHEN day_diff=2 THEN sessions END) AS d2_sessions,
SUM(CASE WHEN day_diff=3 THEN sessions END) AS d3_sessions,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(sessions) from ga_app_tracking g,dim_date
		where last7_days=1 AND g.date=full_date) 
else (select sum(sessions) from ga_app_tracking g,dim_date
where week_to_date=1 and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_sessions,
SUM(CASE WHEN month_to_date=1 THEN sessions END) AS mtd_sessions,
SUM(CASE WHEN month_diff=1 THEN sessions END) AS m1_sessions,
SUM(CASE WHEN month_diff=2 THEN sessions END) AS m2_sessions,


-- Android
SUM(CASE WHEN day_diff=1 and app_channel='Android' THEN newusers END) AS yday_ins_and,
SUM(CASE WHEN day_diff=2 and app_channel='Android' THEN newusers END) AS d2_ins_and,
SUM(CASE WHEN day_diff=3 and app_channel='Android' THEN newusers END) AS d3_ins_and,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(newusers) from ga_app_tracking g,dim_date
		where last7_days=1 and app_channel='Android' AND g.date=full_date) 
else (select sum(newusers) from ga_app_tracking g,dim_date
where week_to_date=1 and app_channel='Android' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END) AS wtd_ins_and,
SUM(CASE WHEN month_to_date=1 and app_channel='Android' THEN newusers END) AS mtd_ins_and,
SUM(CASE WHEN month_diff=1 and app_channel='Android' THEN newusers END) AS m1_ins_and,
SUM(CASE WHEN month_diff=2 and app_channel='Android' THEN newusers END) AS m2_ins_and,

SUM(CASE WHEN day_diff=1 and app_channel='Android' THEN users END) AS yday_users_and,
SUM(CASE WHEN day_diff=2 and app_channel='Android' THEN users END) AS d2_users_and,
SUM(CASE WHEN day_diff=3 and app_channel='Android' THEN users END) AS d3_users_and,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(users) from ga_app_tracking g,dim_date
		where last7_days=1 and app_channel='Android' AND g.date=full_date) 
else (select sum(users) from ga_app_tracking g,dim_date
where week_to_date=1 and app_channel='Android' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END) AS wtd_users_and,
SUM(CASE WHEN month_to_date=1 and app_channel='Android' THEN users END) AS mtd_users_and,
SUM(CASE WHEN month_diff=1 and app_channel='Android' THEN users END) AS m1_users_and,
SUM(CASE WHEN month_diff=2 and app_channel='Android' THEN users END) AS m2_users_and,

SUM(CASE WHEN day_diff=1 and app_channel='Android' THEN sessions END) AS yday_sessions_and,
SUM(CASE WHEN day_diff=2 and app_channel='Android' THEN sessions END) AS d2_sessions_and,
SUM(CASE WHEN day_diff=3 and app_channel='Android' THEN sessions END) AS d3_sessions_and,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(sessions) from ga_app_tracking g,dim_date
		where last7_days=1 and app_channel='Android' AND g.date=full_date) 
else (select sum(sessions) from ga_app_tracking g,dim_date
where week_to_date=1 and app_channel='Android' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_sessions_and,
SUM(CASE WHEN month_to_date=1 and app_channel='Android' THEN sessions END) AS mtd_sessions_and,
SUM(CASE WHEN month_diff=1 and app_channel='Android' THEN sessions END) AS m1_sessions_and,
SUM(CASE WHEN month_diff=2 and app_channel='Android' THEN sessions END) AS m2_sessions_and,


-- iOS
SUM(CASE WHEN day_diff=1 and app_channel='iOS' THEN newusers END) AS yday_ins_iOS,
SUM(CASE WHEN day_diff=2 and app_channel='iOS' THEN newusers END) AS d2_ins_iOS,
SUM(CASE WHEN day_diff=3 and app_channel='iOS' THEN newusers END) AS d3_ins_iOS,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(newusers) from ga_app_tracking g,dim_date
		where last7_days=1 and app_channel='iOS' AND g.date=full_date) 
else (select sum(newusers) from ga_app_tracking g,dim_date
where week_to_date=1 and app_channel='iOS' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END) AS wtd_ins_iOS,
SUM(CASE WHEN month_to_date=1 and app_channel='iOS' THEN newusers END) AS mtd_ins_iOS,
SUM(CASE WHEN month_diff=1 and app_channel='iOS' THEN newusers END) AS m1_ins_iOS,
SUM(CASE WHEN month_diff=2 and app_channel='iOS' THEN newusers END) AS m2_ins_iOS,

SUM(CASE WHEN day_diff=1 and app_channel='iOS' THEN users END) AS yday_users_iOS,
SUM(CASE WHEN day_diff=2 and app_channel='iOS' THEN users END) AS d2_users_iOS,
SUM(CASE WHEN day_diff=3 and app_channel='iOS' THEN users END) AS d3_users_iOS,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(users) from ga_app_tracking g,dim_date
		where last7_days=1 and app_channel='iOS' AND g.date=full_date) 
else (select sum(users) from ga_app_tracking g,dim_date
where week_to_date=1 and app_channel='iOS' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END) AS wtd_users_iOS,
SUM(CASE WHEN month_to_date=1 and app_channel='iOS' THEN users END) AS mtd_users_iOS,
SUM(CASE WHEN month_diff=1 and app_channel='iOS' THEN users END) AS m1_users_iOS,
SUM(CASE WHEN month_diff=2 and app_channel='iOS' THEN users END) AS m2_users_iOS,

SUM(CASE WHEN day_diff=1 and app_channel='iOS' THEN sessions END) AS yday_sessions_iOS,
SUM(CASE WHEN day_diff=2 and app_channel='iOS' THEN sessions END) AS d2_sessions_iOS,
SUM(CASE WHEN day_diff=3 and app_channel='iOS' THEN sessions END) AS d3_sessions_iOS,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(sessions) from ga_app_tracking g,dim_date
		where last7_days=1 and app_channel='iOS' AND g.date=full_date) 
else (select sum(sessions) from ga_app_tracking g,dim_date
where week_to_date=1 and app_channel='iOS' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_sessions_iOS,
SUM(CASE WHEN month_to_date=1 and app_channel='iOS' THEN sessions END) AS mtd_sessions_iOS,
SUM(CASE WHEN month_diff=1 and app_channel='iOS' THEN sessions END) AS m1_sessions_iOS,
SUM(CASE WHEN month_diff=2 and app_channel='iOS' THEN sessions END) AS m2_sessions_iOS,


-- Windows
SUM(CASE WHEN day_diff=1 and app_channel='Windows' THEN newusers END) AS yday_ins_Windows,
SUM(CASE WHEN day_diff=2 and app_channel='Windows' THEN newusers END) AS d2_ins_Windows,
SUM(CASE WHEN day_diff=3 and app_channel='Windows' THEN newusers END) AS d3_ins_Windows,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(newusers) from ga_app_tracking g,dim_date
		where last7_days=1 and app_channel='Windows' AND g.date=full_date) 
else (select sum(newusers) from ga_app_tracking g,dim_date
where week_to_date=1 and app_channel='Windows' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END) AS wtd_ins_Windows,
SUM(CASE WHEN month_to_date=1 and app_channel='Windows' THEN newusers END) AS mtd_ins_Windows,
SUM(CASE WHEN month_diff=1 and app_channel='Windows' THEN newusers END) AS m1_ins_Windows,
SUM(CASE WHEN month_diff=2 and app_channel='Windows' THEN newusers END) AS m2_ins_Windows,

SUM(CASE WHEN day_diff=1 and app_channel='Windows' THEN users END) AS yday_users_Windows,
SUM(CASE WHEN day_diff=2 and app_channel='Windows' THEN users END) AS d2_users_Windows,
SUM(CASE WHEN day_diff=3 and app_channel='Windows' THEN users END) AS d3_users_Windows,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(users) from ga_app_tracking g,dim_date
		where last7_days=1 and app_channel='Windows' AND g.date=full_date) 
else (select sum(users) from ga_app_tracking g,dim_date
where week_to_date=1 and app_channel='Windows' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END) AS wtd_users_Windows,
SUM(CASE WHEN month_to_date=1 and app_channel='Windows' THEN users END) AS mtd_users_Windows,
SUM(CASE WHEN month_diff=1 and app_channel='Windows' THEN users END) AS m1_users_Windows,
SUM(CASE WHEN month_diff=2 and app_channel='Windows' THEN users END) AS m2_users_Windows,

SUM(CASE WHEN day_diff=1 and app_channel='Windows' THEN sessions END) AS yday_sessions_Windows,
SUM(CASE WHEN day_diff=2 and app_channel='Windows' THEN sessions END) AS d2_sessions_Windows,
SUM(CASE WHEN day_diff=3 and app_channel='Windows' THEN sessions END) AS d3_sessions_Windows,
max(case when date_part(dow,current_date+interval '1 day')=2 then
	 (select sum(sessions) from ga_app_tracking g,dim_date
		where last7_days=1 and app_channel='Windows' AND g.date=full_date) 
else (select sum(sessions) from ga_app_tracking g,dim_date
where week_to_date=1 and app_channel='Windows' and g.date=full_date and g.date!=to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')) END)  AS wtd_sessions_Windows,
SUM(CASE WHEN month_to_date=1 and app_channel='Windows' THEN sessions END) AS mtd_sessions_Windows,
SUM(CASE WHEN month_diff=1 and app_channel='Windows' THEN sessions END) AS m1_sessions_Windows,
SUM(CASE WHEN month_diff=2 and app_channel='Windows' THEN sessions END) AS m2_sessions_Windows


FROM ga_app_tracking ga, dim_date d
WHERE
ga.date=d.full_date and
d.month_diff<=1
) Y            
