-- monthly orders
select 
ROUND(sum(case when m1=extract(month from sysdate) then GMV end)/10000000) as GMV1,
ROUND(sum(case when m1=extract(month from sysdate-interval '1 month') then GMV end)/10000000) as GMV2,
sum(case when m1=extract(month from sysdate) then orders end) as orders1,
sum(case when m1=extract(month from sysdate-interval '1 month') then orders end) as orders2,
--sum(case when m1=extract(month from sysdate) then traffic end) as traffic1,
--sum(case when m1=extract(month from sysdate-interval '1 month') then traffic end) as traffic2,
sum(case when m1=extract(month from sysdate) then units end) as units1,
sum(case when m1=extract(month from sysdate-interval '1 month') then units end) as units2,
--sum(case when m1=extract(month from sysdate) then conversion end) as conv1,
--sum(case when m1=extract(month from sysdate-interval '1 month') then conversion end) as conv2,
sum(case when m1=extract(month from sysdate) then mfb_share end) as mfb_share1,
sum(case when m1=extract(month from sysdate-interval '1 month') then mfb_share end) as mfb_share2,
sum(case when m1=extract(month from sysdate) then mp_share end) as mp_share1,
sum(case when m1=extract(month from sysdate-interval '1 month') then mp_share end) as mp_share2,
sum(case when m1=extract(month from sysdate) then asp end) as asp1,
sum(case when m1=extract(month from sysdate-interval '1 month') then asp end) as asp2,
sum(case when m1=extract(month from sysdate) then avg_order_size end) as aos1,
sum(case when m1=extract(month from sysdate-interval '1 month') then avg_order_size end) as aos2,
sum(case when m1=extract(month from sysdate) then avg_split_size end) as ass1,
sum(case when m1=extract(month from sysdate-interval '1 month') then avg_split_size end) as ass2,
sum(case when m1=extract(month from sysdate) then avg_bucket_size end) as abs1,
sum(case when m1=extract(month from sysdate-interval '1 month') then avg_bucket_size end) as abs2,
-- customers
sum(case when m1=extract(month from sysdate) then new_customers end) as new_customers1, 
sum(case when m1=extract(month from sysdate-interval '1 month') then new_customers end) as new_customers2,
sum(case when m1=extract(month from sysdate) then repeat_customers end) as repeat1,
sum(case when m1=extract(month from sysdate-interval '1 month') then repeat_customers end) as repeat2,
sum(case when m1=extract(month from sysdate) then repeat_rate end) as rate1,
sum(case when m1=extract(month from sysdate-interval '1 month') then repeat_rate end) as rate2
from
(select a.m1, a.GMV, 
a.orders, 
--visits.traffic,
a.units, 
--a.orders*100*1.0/visits.traffic as conversion, 
a.mfb_revenue*100*1.0/a.GMV as mfb_share,
a.mp_revenue*100*1.0/a.GMV as mp_share, 
a.GMV*1.0/a.units as asp, 
a.GMV*1.0/a.orders as avg_order_size, 
a.orderid_cnt*1.0/a.orders as avg_split_size, 
a.units*1.0/a.orders as avg_bucket_size , a.new_customers, a.repeat_customers, a.repeat_customers*100*1.0/a.orders as repeat_rate
from 
(select 
extract(month from to_date(order_created_date,'YYYYMMDD')) as m1, 
sum(item_revenue_inc_cashback) GMV,
count(distinct order_group_id) orders,
sum(quantity) units,
count(distinct case when (foi.source not like 'aff%' or foi.medium not like 'aff%') then order_group_id  end) as non_aff_orders,
sum(case when foi.supply_type <> 'JUST_IN_TIME' and dp.brand_type <> 'External'  then item_revenue_inc_cashback else 0 end) mfb_revenue,
sum(case when foi.supply_type = 'JUST_IN_TIME' then item_revenue_inc_cashback else 0 end) mp_revenue,
count(distinct foi.order_id) orderid_cnt,
count(distinct case when purchase_sequence=1 then order_group_id else null end) new_customers,
count(distinct case when purchase_sequence>1 then order_group_id else null end) repeat_customers
from 
fact_orderitem foi
join dim_product dp on dp.id=foi.idproduct
where foi.order_created_date between to_char(current_date-interval '1 month','YYYYMM01') 
		and (select max(dd.full_date) from bidb.dim_date dd where dd.week_diff=1)
and (is_realised = 1 or is_shipped=1) group by 1) a 
/*join
(select extract(month from to_date(gt.date,'YYYYMMDD')) as m2, sum(number_of_visits) as traffic from ga_traffic gt
where gt.date between to_char(current_date-interval '1 month','YYYYMM01') 
		and (select max(dd.full_date) from bidb.dim_date dd where dd.week_diff=1)
	 group by 1) visits 
on a.m1=visits.m2*/
) a;      
