-- weekly orders
select 
ROUND(sum(case when wd=1 then GMV end)/10000000) as GMV1,
ROUND(sum(case when wd=2 then GMV end)/10000000) as GMV2,
ROUND(sum(case when wd=3 then GMV end)/10000000) as GMV3,
ROUND(sum(case when wd=4 then GMV end)/10000000) as GMV4,
ROUND(sum(case when wd=5 then GMV end)/10000000) as GMV5, 
sum(case when wd=1 then orders end) as orders1,
sum(case when wd=2 then orders end) as orders2,
sum(case when wd=3 then orders end) as orders3,
sum(case when wd=4 then orders end) as orders4,
sum(case when wd=5 then orders end) as orders5,
/*
sum(case when wd=1 then traffic end) as traffic1,
sum(case when wd=2 then traffic end) as traffic2,
sum(case when wd=3 then traffic end) as traffic3,
sum(case when wd=4 then traffic end) as traffic4,
sum(case when wd=5 then traffic end) as traffic5,*/
sum(case when wd=1 then units end) as units1,
sum(case when wd=2 then units end) as units2,
sum(case when wd=3 then units end) as units3,
sum(case when wd=4 then units end) as units4,
sum(case when wd=5 then units end) as units5,
/*
sum(case when wd=1 then conversion end) as conv1,
sum(case when wd=2 then conversion end) as conv2,
sum(case when wd=3 then conversion end) as conv3,
sum(case when wd=4 then conversion end) as conv4,
sum(case when wd=5 then conversion end) as conv5,*/
sum(case when wd=1 then mfb_share end) as mfb_share1,
sum(case when wd=2 then mfb_share end) as mfb_share2,
sum(case when wd=3 then mfb_share end) as mfb_share3,
sum(case when wd=4 then mfb_share end) as mfb_share4,
sum(case when wd=5 then mfb_share end) as mfb_share5,
sum(case when wd=1 then mp_share end) as mp_share1,
sum(case when wd=2 then mp_share end) as mp_share2,
sum(case when wd=3 then mp_share end) as mp_share3,
sum(case when wd=4 then mp_share end) as mp_share4,
sum(case when wd=5 then mp_share end) as mp_share5,
sum(case when wd=1 then asp end) as asp1,
sum(case when wd=2 then asp end) as asp2,
sum(case when wd=3 then asp end) as asp3,
sum(case when wd=4 then asp end) as asp4,
sum(case when wd=5 then asp end) as asp5,
sum(case when wd=1 then avg_order_size end) as aos1,
sum(case when wd=2 then avg_order_size end) as aos2,
sum(case when wd=3 then avg_order_size end) as aos3,
sum(case when wd=4 then avg_order_size end) as aos4,
sum(case when wd=5 then avg_order_size end) as aos5,
sum(case when wd=1 then avg_split_size end) as ass1,
sum(case when wd=2 then avg_split_size end) as ass2,
sum(case when wd=3 then avg_split_size end) as ass3,
sum(case when wd=4 then avg_split_size end) as ass4,
sum(case when wd=5 then avg_split_size end) as ass5,
sum(case when wd=1 then avg_bucket_size end) as abs1,
sum(case when wd=2 then avg_bucket_size end) as abs2,
sum(case when wd=3 then avg_bucket_size end) as abs3,
sum(case when wd=4 then avg_bucket_size end) as abs4,
sum(case when wd=5 then avg_bucket_size end) as abs5,
-- customers
sum(case when wd=1 then new_customers end) as new_customers1, 
sum(case when wd=2 then new_customers end) as new_customers2,
sum(case when wd=3 then new_customers end) as new_customers3,
sum(case when wd=4 then new_customers end) as new_customers4,
sum(case when wd=5 then new_customers end) as new_customers5,
sum(case when wd=1 then repeat_customers end) as repeat1,
sum(case when wd=2 then repeat_customers end) as repeat2,
sum(case when wd=3 then repeat_customers end) as repeat3,
sum(case when wd=4 then repeat_customers end) as repeat4,
sum(case when wd=5 then repeat_customers end) as repeat5,
sum(case when wd=1 then repeat_rate end) as rate1,
sum(case when wd=2 then repeat_rate end) as rate2,
sum(case when wd=3 then repeat_rate end) as rate3,
sum(case when wd=4 then repeat_rate end) as rate4,
sum(case when wd=5 then repeat_rate end) as rate5
from
(select 
a.week_diff as wd,
a.GMV, 
a.orders, 
a.units, 
--visits.traffic,
--a.orders*100*1.0/visits.traffic as conversion, 
a.mfb_revenue*100*1.0/a.GMV as mfb_share,
a.mp_revenue*100*1.0/a.GMV as mp_share, 
a.GMV/a.units as asp, 
a.GMV/a.orders as avg_order_size, 
a.orderid_cnt/a.orders as avg_split_size, 
a.units/a.orders as avg_bucket_size , 
a.new_customers, 
a.repeat_customers,
a.repeat_customers*100*1.0/a.orders as repeat_rate
from 
(select 
		dd.week_diff, 
		sum(item_revenue_inc_cashback) GMV,
		count(distinct order_group_id) orders,
		sum(quantity) units,
		count(distinct case when (foi.source not like 'aff%' or foi.medium not like 'aff%') then order_group_id  end) as non_aff_orders,
		sum(case when foi.supply_type <> 'JUST_IN_TIME' and dp.brand_type <> 'External'  then item_revenue_inc_cashback else 0 end) mfb_revenue,
		sum(case when foi.supply_type = 'JUST_IN_TIME' then item_revenue_inc_cashback else 0 end) mp_revenue,
		count(distinct foi.order_id) orderid_cnt,
		count(distinct case when purchase_sequence=1 then order_group_id else null end) new_customers,
		count(distinct case when purchase_sequence>1 then order_group_id else null end) repeat_customers
from 
fact_orderitem foi
join dim_product dp on dp.id=foi.idproduct
join bidb.dim_date dd on dd.full_date=foi.order_created_date
where dd.week_diff between 1 and 5
and foi.order_created_date between to_char(current_date-interval '42 day','YYYYMMDD') and to_char(current_date-interval '1 day','YYYYMMDD')
and (is_realised = 1 or is_shipped=1) group by 1) a
/*join
(select dd.week_diff, sum(number_of_visits) as traffic from ga_traffic gt
join bidb.dim_date dd on dd.full_date=gt.date
where dd.week_diff between 1 and 5
	 group by 1) visits 
on a.week_diff=visits.week_diff*/
) final;      
