/*BM42 RGM First Customers*/
SELECT
	to_char(f.yday_rgm,'99,99,99,99,999.9') AS yday_rgm
--	TRUNC((f.yday_rgm - f.yday_minus_1_rgm)*100/f.yday_minus_1_rgm,1) as p1,
--	f.mtd_rgm, f.month_minus_1_rgm, f.month_minus_2_rgm, f.month_minus_3_rgm
FROM
	(SELECT 
		SUM(CASE WHEN d.day_diff=1 THEN d.rgm END)*100/SUM(CASE WHEN d.day_diff=1 THEN d.revenue END) AS yday_rgm
/*		SUM(CASE WHEN d.day_diff=2 THEN d.rgm END)*100/SUM(CASE WHEN d.day_diff=2 THEN d.revenue END) AS yday_minus_1_rgm,
		SUM(CASE WHEN d.month_to_date=1 THEN d.rgm END)/SUM(CASE WHEN d.month_to_date=1 THEN d.revenue END) AS mtd_rgm,
		SUM(CASE WHEN d.month_diff=1 THEN d.rgm END)/SUM(CASE WHEN d.month_diff=1 THEN d.revenue END) AS month_minus_1_rgm,
		SUM(CASE WHEN d.month_diff=2 THEN d.rgm END)/SUM(CASE WHEN d.month_diff=2 THEN d.revenue END) AS month_minus_2_rgm,
		SUM(CASE WHEN d.month_diff=3 THEN d.rgm END)/SUM(CASE WHEN d.month_diff=3 THEN d.revenue END) AS month_minus_3_rgm*/
	FROM	
		(SELECT 
			a.order_created_date, aa.day_diff, aa.month_to_date, aa.month_diff, 
			(a.revenue-a.net_cost_price) AS rgm, a.revenue 
		FROM 
			(SELECT 
				o.order_created_date,  
				SUM(item_revenue+cashback_redeemed)-SUM(o.tax_amount) AS revenue,
				SUM(item_purchase_price_inc_tax - vendor_funding + royalty_commission) AS net_cost_price 
			FROM orders_directional o, dim_date dt 
			WHERE dt.month_diff<=3 AND 
			o.order_created_date = dt.full_date AND 
			o.max_purchase_sequence is NULL AND (o.is_shipped=1 OR o.is_realised=1)
and o.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01') 
			GROUP BY o.order_created_date) a
			JOIN
			(select full_date, day_diff, month_to_date, month_diff from dim_date where month_diff<=3) aa on a.order_created_date=aa.full_date ) d) f  
