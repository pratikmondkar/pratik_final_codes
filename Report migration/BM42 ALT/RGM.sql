/*BM42 RGM Overall*/
SELECT 
	to_char(e.yday_rgm,'99,99,99,99,999') AS yday_rgm,
--	trunc((e.yday_rgm - e.yday_minus_1_rgm)*100/e.yday_minus_1_rgm,1) as p1,
--	e.mtd_rgm, e.month_minus_1_rgm, e.month_minus_2_rgm, e.month_minus_3_rgm,

	to_char(e.yday_rgm_percent,'99,99,99,99,999.9') || '%' as yday_rgm_percent,
--    trunc((e.yday_rgm_percent - e.yday_minus_1_rgm_percent)*100/e.yday_minus_1_rgm_percent,1) AS p2,
--	e.mtd_rgm_percent, e.month_minus_1_rgm_percent, e.month_minus_2_rgm_percent, e.month_minus_3_rgm_percent,
	
	to_char((e.yday_rgm_percent+2.1),'99,99,99,99,999.9')  || '%' as yday_rgm_percent_1,
--	trunc(((e.yday_rgm_percent+0.021) - (e.yday_minus_1_rgm_percent+0.021))*100/(e.yday_minus_1_rgm_percent+0.021),1) AS p3,
--	(e.mtd_rgm_percent+0.021) AS mtd_rgm_percent_1, (e.month_minus_1_rgm_percent+0.021) AS month_minus_1_rgm_percent_1, 
--	(e.month_minus_2_rgm_percent+0.021) AS month_minus_2_rgm_percent_1, (e.month_minus_3_rgm_percent+0.021) AS month_minus_3_rgm_percent_1,
	
	to_char(e.yday_rgm_Mpercent,'99,99,99,99,999.9')  || '%' AS yday_rgm_Mpercent,
--	trunc((e.yday_rgm_Mpercent - e.yday_minus_1_rgm_Mpercent)*100/e.yday_minus_1_rgm_Mpercent,1) as p4,
--	e.mtd_rgm_Mpercent, e.month_minus_1_rgm_Mpercent, e.month_minus_2_rgm_Mpercent, e.month_minus_3_rgm_Mpercent,
	
	to_char(e.yday_rgm_Ppercent,'99,99,99,99,999.9')  || '%' as yday_rgm_Ppercent
--	trunc((e.yday_rgm_Ppercent - e.yday_minus_1_rgm_Ppercent)*100/e.yday_minus_1_rgm_Ppercent,1) AS p5,
--	e.mtd_rgm_Ppercent, e.month_minus_1_rgm_Ppercent, e.month_minus_2_rgm_Ppercent, e.month_minus_3_rgm_Ppercent,
			
	FROM
	(SELECT 
		SUM(CASE WHEN d.day_diff=1 THEN d.rgm END) AS yday_rgm,
/*		SUM(CASE WHEN d.day_diff=2 THEN d.rgm END) AS yday_minus_1_rgm,
		to_char(SUM(CASE WHEN d.month_to_date=1 THEN d.rgm END),'99,99,99,99,999') AS mtd_rgm,
		to_char(SUM(CASE WHEN d.month_diff=1 THEN d.rgm END),'99,99,99,99,999') AS month_minus_1_rgm,
		to_char(SUM(CASE WHEN d.month_diff=2 THEN d.rgm END),'99,99,99,99,999') AS month_minus_2_rgm,
		to_char(SUM(CASE WHEN d.month_diff=3 THEN d.rgm END),'99,99,99,99,999') AS month_minus_3_rgm,*/

		SUM(CASE WHEN d.day_diff=1 THEN d.rgm END)*100/SUM(CASE WHEN d.day_diff=1 THEN d.revenue END) AS yday_rgm_percent,
/*		SUM(CASE WHEN d.day_diff=2 THEN d.rgm END)*100/SUM(CASE WHEN d.day_diff=2 THEN d.revenue END) AS yday_minus_1_rgm_percent,
		SUM(CASE WHEN d.month_to_date=1 THEN d.rgm END)/SUM(CASE WHEN d.month_to_date=1 THEN d.revenue END) AS mtd_rgm_percent,
		SUM(CASE WHEN d.month_diff=1 THEN d.rgm END)/SUM(CASE WHEN d.month_diff=1 THEN d.revenue END) AS month_minus_1_rgm_percent,
		SUM(CASE WHEN d.month_diff=2 THEN d.rgm END)/SUM(CASE WHEN d.month_diff=2 THEN d.revenue END) AS month_minus_2_rgm_percent,
		SUM(CASE WHEN d.month_diff=3 THEN d.rgm END)/SUM(CASE WHEN d.month_diff=3 THEN d.revenue END) AS month_minus_3_rgm_percent,*/
		
		SUM(CASE WHEN d.day_diff=1 THEN d.rgm_mrkt END)*100/SUM(CASE WHEN d.day_diff=1 THEN d.revenue_mrkt END) AS yday_rgm_Mpercent,
/*		SUM(CASE WHEN d.day_diff=2 THEN d.rgm_mrkt END)*100/SUM(CASE WHEN d.day_diff=2 THEN d.revenue_mrkt END) AS yday_minus_1_rgm_Mpercent,
		SUM(CASE WHEN d.month_to_date=1 THEN d.rgm_mrkt END)/SUM(CASE WHEN d.month_to_date=1 THEN d.revenue_mrkt END) AS mtd_rgm_Mpercent,
		SUM(CASE WHEN d.month_diff=1 THEN d.rgm_mrkt END)/SUM(CASE WHEN d.month_diff=1 THEN d.revenue_mrkt END) AS month_minus_1_rgm_Mpercent,
		SUM(CASE WHEN d.month_diff=2 THEN d.rgm_mrkt END)/SUM(CASE WHEN d.month_diff=2 THEN d.revenue_mrkt END) AS month_minus_2_rgm_Mpercent,
		SUM(CASE WHEN d.month_diff=3 THEN d.rgm_mrkt END)/SUM(CASE WHEN d.month_diff=3 THEN d.revenue_mrkt END) AS month_minus_3_rgm_Mpercent,*/
		
		SUM(CASE WHEN d.day_diff=1 THEN d.rgm_private END)*100/SUM(CASE WHEN d.day_diff=1 THEN d.revenue_private END) AS yday_rgm_Ppercent
/*		SUM(CASE WHEN d.day_diff=2 THEN d.rgm_private END)*100/SUM(CASE WHEN d.day_diff=2 THEN d.revenue_private END) AS yday_minus_1_rgm_Ppercent,
		SUM(CASE WHEN d.month_to_date=1 THEN d.rgm_private END)/SUM(CASE WHEN d.month_to_date=1 THEN d.revenue_private END) AS mtd_rgm_Ppercent,
		SUM(CASE WHEN d.month_diff=1 THEN d.rgm_private END)/SUM(CASE WHEN d.month_diff=1 THEN d.revenue_private END) AS month_minus_1_rgm_Ppercent,
		SUM(CASE WHEN d.month_diff=2 THEN d.rgm_private END)/SUM(CASE WHEN d.month_diff=2 THEN d.revenue_private END) AS month_minus_2_rgm_Ppercent,
		SUM(CASE WHEN d.month_diff=3 THEN d.rgm_private END)/SUM(CASE WHEN d.month_diff=3 THEN d.revenue_private END) AS month_minus_3_rgm_Ppercent,  */
		
	FROM	
		(SELECT 
			b.order_created_date, aa.day_diff, aa.month_to_date, aa.month_diff, aa.last7_days, 
			
			(b.revenue-b.net_cost_price) AS rgm,
			(b.revenue_mrkt-b.net_cost_price_mrkt) AS rgm_mrkt, 
			(b.revenue_private-b.net_cost_price_private) AS rgm_private, 

			nvl(b.revenue,0) AS revenue,
			b.revenue_mrkt,
			b.revenue_private
			
			FROM 
			
				(SELECT 
				oi.order_created_date, 
				sum(item_revenue+cashback_redeemed-tax_amount) as revenue,
				SUM(CASE when oi.supply_type='JUST_IN_TIME' then (item_revenue+cashback_redeemed-tax_amount) END) as revenue_mrkt,
				SUM(CASE when p.brand_type='Private' then (item_revenue+cashback_redeemed-tax_amount) END) as revenue_private,
				
				SUM(item_purchase_price_inc_tax - vendor_funding + royalty_commission) as net_cost_price,
				SUM(CASE WHEN oi.supply_type='JUST_IN_TIME' then item_purchase_price_inc_tax - vendor_funding END) as net_cost_price_mrkt,
				SUM(CASE WHEN p.brand_type='Private' then item_purchase_price_inc_tax - vendor_funding + royalty_commission END) as net_cost_price_private   		 
   		 
			FROM 
			orders_directional oi, dim_product p  
			WHERE oi.order_created_date>=to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01') 
			AND (oi.is_shipped=1 OR oi.is_realised=1) AND oi.sku_id = p.sku_id 
			GROUP BY oi.order_created_date) b 
			left join
			(select full_date, day_diff, month_to_date, month_diff, last7_days
			from dim_date where month_diff<=3) aa 
			on b.order_created_date=aa.full_date
			) d                                                                                               
) e
