/*BM42 Discounts*/
select
	to_char(a.yday_PDrevenue,'99,99,99,99,999') as yday_PDrev,
/*	trunc((a.yday_PDrevenue - a.yday_minus_1_PDrevenue)*100/nullif(a.yday_minus_1_PDrevenue,0),1) p1,
	to_char(a.mtd_PDrevenue,'99,99,99,99,999') as mtd_PDrevenue,
	to_char(a.month_minus_1_PDrevenue,'99,99,99,99,999') as month_minus_1_PDrevenue, 
	to_char(a.month_minus_2_PDrevenue,'99,99,99,99,999') as month_minus_2_PDrevenue,
	to_char(a.month_minus_3_PDrevenue,'99,99,99,99,999') as month_minus_3_PDrevenue,*/
	
	to_char(a.yday_CDrevenue,'99,99,99,99,999') as yday_CDrev,
/*	trunc((a.yday_CDrevenue - a.yday_minus_1_CDrevenue)*100/nullif(a.yday_minus_1_CDrevenue,0),1) as p2,
	to_char(a.mtd_CDrevenue,'99,99,99,99,999') as mtd_CDrevenue,
	to_char(a.month_minus_1_CDrevenue,'99,99,99,99,999') as month_minus_1_CDrevenue, 
	to_char(a.month_minus_2_CDrevenue,'99,99,99,99,999') as month_minus_2_CDrevenue,
	to_char(a.month_minus_3_CDrevenue,'99,99,99,99,999') as month_minus_3_CDrevenue,*/
	
	to_char(a.yday_NoCPDrevenue,'99,99,99,99,999') as yday_NoCPDrev,
/*	trunc((a.yday_NoCPDrevenue - a.yday_minus_1_NoCPDrevenue)*100/nullif(a.yday_minus_1_NoCPDrevenue,0),1) as p3,
	to_char(a.mtd_NoCPDrevenue,'99,99,99,99,999') as mtd_NoCPDrevenue,
	to_char(a.month_minus_1_NoCPDrevenue,'99,99,99,99,999') as month_minus_1_NoCPDrevenue, 
	to_char(a.month_minus_2_NoCPDrevenue,'99,99,99,99,999') as month_minus_2_NoCPDrevenue,
	to_char(a.month_minus_3_NoCPDrevenue,'99,99,99,99,999') as month_minus_3_NoCPDrevenue,*/
	
	to_char(a.yday_CPDrevenue,'99,99,99,99,999') as yday_CPDrev
/*	trunc((a.yday_CPDrevenue - a.yday_minus_1_CPDrevenue)*100/nullif(a.yday_minus_1_CPDrevenue,0),1) as p4,
	to_char(a.mtd_CPDrevenue,'99,99,99,99,999') as mtd_CPDrevenue,
	to_char(a.month_minus_1_CPDrevenue,'99,99,99,99,999') as month_minus_1_CPDrevenue, 
	to_char(a.month_minus_2_CPDrevenue,'99,99,99,99,999') as month_minus_2_CPDrevenue,
	to_char(a.month_minus_3_CPDrevenue,'99,99,99,99,999') as month_minus_3_CPDrevenue*/
from 	
	(select 
		sum(case when dt.day_diff = 1 and o.coupon_discount = 0 and o.trade_discount > 0 then item_revenue+cashback_redeemed end) as yday_PDrevenue,
/*		sum(case when dt.day_diff = 2 and o.coupon_discount = 0 and o.trade_discount > 0 then item_revenue+cashback_redeemed end) as yday_minus_1_PDrevenue,
		sum(case when dt.month_to_date = 1 and o.coupon_discount = 0 and o.trade_discount > 0 then item_revenue+cashback_redeemed end) as mtd_PDrevenue,
		sum(case when dt.month_diff = 1 and o.coupon_discount = 0 and o.trade_discount > 0 then item_revenue+cashback_redeemed end) as month_minus_1_PDrevenue,
		sum(case when dt.month_diff = 2 and o.coupon_discount = 0 and o.trade_discount > 0 then item_revenue+cashback_redeemed end) as month_minus_2_PDrevenue,
		sum(case when dt.month_diff = 3 and o.coupon_discount = 0 and o.trade_discount > 0 then item_revenue+cashback_redeemed end) as month_minus_3_PDrevenue,*/
		
		sum(case when dt.day_diff = 1 and o.coupon_discount > 0 and o.trade_discount = 0 then item_revenue+cashback_redeemed end) as yday_CDrevenue,
/*		sum(case when dt.day_diff = 2 and o.coupon_discount > 0 and o.trade_discount = 0 then item_revenue+cashback_redeemed end) as yday_minus_1_CDrevenue,
		sum(case when dt.month_to_date = 1 and o.coupon_discount > 0 and o.trade_discount = 0 then item_revenue+cashback_redeemed end) as mtd_CDrevenue,
		sum(case when dt.month_diff = 1 and o.coupon_discount > 0 and o.trade_discount = 0 then item_revenue+cashback_redeemed end) as month_minus_1_CDrevenue,
		sum(case when dt.month_diff = 2 and o.coupon_discount > 0 and o.trade_discount = 0 then item_revenue+cashback_redeemed end) as month_minus_2_CDrevenue,
		sum(case when dt.month_diff = 3 and o.coupon_discount > 0 and o.trade_discount = 0 then item_revenue+cashback_redeemed end) as month_minus_3_CDrevenue,*/
		
		sum(case when dt.day_diff = 1 and o.coupon_discount = 0 and o.trade_discount = 0 then item_revenue+cashback_redeemed end) as yday_NoCPDrevenue,
/*		sum(case when dt.day_diff = 2 and o.coupon_discount = 0 and o.trade_discount = 0 then item_revenue+cashback_redeemed end) as yday_minus_1_NoCPDrevenue,
		sum(case when dt.month_to_date = 1 and o.coupon_discount = 0 and o.trade_discount = 0 then item_revenue+cashback_redeemed end) as mtd_NoCPDrevenue,
		sum(case when dt.month_diff = 1 and o.coupon_discount = 0 and o.trade_discount = 0 then item_revenue+cashback_redeemed end) as month_minus_1_NoCPDrevenue,
		sum(case when dt.month_diff = 2 and o.coupon_discount = 0 and o.trade_discount = 0 then item_revenue+cashback_redeemed end) as month_minus_2_NoCPDrevenue,
		sum(case when dt.month_diff = 3 and o.coupon_discount = 0 and o.trade_discount = 0 then item_revenue+cashback_redeemed end) as month_minus_3_NoCPDrevenue,*/
		
		sum(case when dt.day_diff = 1 and o.coupon_discount > 0 and o.trade_discount > 0 then item_revenue+cashback_redeemed end) as yday_CPDrevenue
/*		sum(case when dt.day_diff = 2 and o.coupon_discount > 0 and o.trade_discount > 0 then item_revenue+cashback_redeemed end) as yday_minus_1_CPDrevenue,
		sum(case when dt.month_to_date = 1 and o.coupon_discount > 0 and o.trade_discount > 0 then item_revenue+cashback_redeemed end) as mtd_CPDrevenue,
		sum(case when dt.month_diff = 1 and o.coupon_discount > 0 and o.trade_discount > 0 then item_revenue+cashback_redeemed end) as month_minus_1_CPDrevenue,
		sum(case when dt.month_diff = 2 and o.coupon_discount > 0 and o.trade_discount > 0 then item_revenue+cashback_redeemed end) as month_minus_2_CPDrevenue,
		sum(case when dt.month_diff = 3 and o.coupon_discount > 0 and o.trade_discount > 0 then item_revenue+cashback_redeemed end) as month_minus_3_CPDrevenue*/
		
	from 
		orders_directional o, 
		dim_date dt
	where 
		dt.month_diff<=3 
		and o.order_created_date = dt.full_date 
		and (o.is_shipped = 1 or o.is_realised = 1)
		and o.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
		) a

 
