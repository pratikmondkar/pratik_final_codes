/*BM42 Total Discount Percent*/
SELECT
	to_char(a.yday_revenue,'99,99,99,99,999.9')  AS yday_rev,
/*	TRUNC((a.yday_revenue - a.yday_minus_1_revenue)*100/a.yday_minus_1_revenue,1) as p1,
	to_char(a.mtd_revenue,'99,99,99,99,999.9') AS mtd_revenue, to_char(a.month_minus_1_revenue,'99,99,99,99,999.9') AS month_minus_1_revenue, 
	to_char(a.month_minus_2_revenue,'99,99,99,99,999.9') AS month_minus_2_revenue, to_char(a.month_minus_3_revenue,'99,99,99,99,999.9') AS month_minus_3_revenue,*/
	
	to_char(a.yday_CDLrevenue,'99,99,99,99,999.9') AS yday_CDrev,
/*	TRUNC((a.yday_CDLrevenue - a.yday_minus_1_CDLrevenue)*100/a.yday_minus_1_CDLrevenue,1) as p2,
	to_char(a.mtd_CDLrevenue,'99,99,99,99,999.9') AS mtd_CDrevenue, to_char(a.month_minus_1_CDLrevenue,'99,99,99,99,999.9') AS month_minus_1_CDrevenue, 
	to_char(a.month_minus_2_CDLrevenue,'99,99,99,99,999.9') AS month_minus_2_CDrevenue, to_char(a.month_minus_3_CDLrevenue,'99,99,99,99,999.9') AS month_minus_3_CDrevenue,*/
	
	to_char(a.yday_PDrevenue,'99,99,99,99,999.9')  AS yday_PDrev
/*	TRUNC((a.yday_PDrevenue - a.yday_minus_1_PDrevenue)*100/a.yday_minus_1_PDrevenue,1) as p3,
	to_char(a.mtd_PDrevenue,'99,99,99,99,999.9') AS mtd_PDrevenue, to_char(a.month_minus_1_PDrevenue,'99,99,99,99,999.9') AS month_minus_1_PDrevenue, 
	to_char(a.month_minus_2_PDrevenue,'99,99,99,99,999.9') AS month_minus_2_PDrevenue, to_char(a.month_minus_3_PDrevenue,'99,99,99,99,999.9') AS month_minus_3_PDrevenue*/
	
FROM 
	(SELECT 
		SUM(CASE WHEN dt.day_diff = 1 THEN coupon_discount  END)*100/SUM(CASE WHEN dt.day_diff = 1 THEN item_mrp END) AS yday_CDLrevenue,
/*		SUM(CASE WHEN dt.day_diff = 2 THEN coupon_discount  END)*100/SUM(CASE WHEN dt.day_diff = 2 THEN item_mrp END) AS yday_minus_1_CDLrevenue,
		SUM(CASE WHEN dt.month_to_date = 1 THEN coupon_discount  END)*100/SUM(CASE WHEN dt.month_to_date = 1 THEN item_mrp END) AS mtd_CDLrevenue,
		SUM(CASE WHEN dt.month_diff = 1 THEN coupon_discount  END)*100/SUM(CASE WHEN dt.month_diff = 1 THEN item_mrp END) AS month_minus_1_CDLrevenue,
		SUM(CASE WHEN dt.month_diff = 2 THEN coupon_discount  END)*100/SUM(CASE WHEN dt.month_diff = 2 THEN item_mrp END) AS month_minus_2_CDLrevenue,
		SUM(CASE WHEN dt.month_diff = 3 THEN coupon_discount  END)*100/SUM(CASE WHEN dt.month_diff = 3 THEN item_mrp END) AS month_minus_3_CDLrevenue,*/
		
		SUM(CASE WHEN dt.day_diff = 1 THEN trade_discount END)*100/SUM(CASE WHEN dt.day_diff = 1 THEN item_mrp END) AS yday_PDrevenue,
/*		SUM(CASE WHEN dt.day_diff = 2 THEN trade_discount END)*100/SUM(CASE WHEN dt.day_diff = 2 THEN item_mrp END) AS yday_minus_1_PDrevenue,
		SUM(CASE WHEN dt.month_to_date = 1 THEN trade_discount END)*100/SUM(CASE WHEN dt.month_to_date = 1 THEN item_mrp END) AS mtd_PDrevenue,
		SUM(CASE WHEN dt.month_diff = 1 THEN trade_discount END)*100/SUM(CASE WHEN dt.month_diff = 1 THEN item_mrp END) AS month_minus_1_PDrevenue,
		SUM(CASE WHEN dt.month_diff = 2 THEN trade_discount END)*100/SUM(CASE WHEN dt.month_diff = 2 THEN item_mrp END) AS month_minus_2_PDrevenue,
		SUM(CASE WHEN dt.month_diff = 3 THEN trade_discount END)*100/SUM(CASE WHEN dt.month_diff = 3 THEN item_mrp END) AS month_minus_3_PDrevenue,*/
		
		SUM(CASE WHEN dt.day_diff = 1 THEN coupon_discount + trade_discount END)*100/SUM(CASE WHEN dt.day_diff = 1 THEN item_mrp END) AS yday_revenue
/*		SUM(CASE WHEN dt.day_diff = 2 THEN coupon_discount + trade_discount END)*100/SUM(CASE WHEN dt.day_diff = 2 THEN item_mrp END) AS yday_minus_1_revenue,
		SUM(CASE WHEN dt.month_to_date = 1 THEN coupon_discount + trade_discount END)*100/SUM(CASE WHEN dt.month_to_date = 1 THEN item_mrp END) AS mtd_revenue,
		SUM(CASE WHEN dt.month_diff = 1 THEN coupon_discount + trade_discount END)*100/SUM(CASE WHEN dt.month_diff = 1 THEN item_mrp END) AS month_minus_1_revenue,
		SUM(CASE WHEN dt.month_diff = 2 THEN coupon_discount + trade_discount END)*100/SUM(CASE WHEN dt.month_diff = 2 THEN item_mrp END) AS month_minus_2_revenue,
		SUM(CASE WHEN dt.month_diff = 3 THEN coupon_discount + trade_discount END)*100/SUM(CASE WHEN dt.month_diff = 3 THEN item_mrp END) AS month_minus_3_revenue			*/
		
	FROM 
		orders_directional o, dim_date dt
	WHERE 
		dt.month_diff<=3 AND o.order_created_date = dt.full_date AND (o.is_shipped = 1 OR o.is_realised=1) and o.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')) a      
