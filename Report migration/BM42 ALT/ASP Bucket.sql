/*BM42 ASP Bucket*/
SELECT
	(trunc(a.rev_lt_1000_yday,1) ||  '% (' ||  trunc(asp_lt_1000_yday,0) || ')') AS rev_lt_1000_yday,
	(trunc(a.rev_lt_1500_yday,1) ||  '% (' ||  trunc(asp_lt_1500_yday,0) || ')') AS rev_lt_1500_yday,
	(trunc(a.rev_lt_2000_yday,1) ||  '% (' ||  trunc(asp_lt_2000_yday,0) || ')') AS rev_lt_2000_yday,
	(trunc(a.rev_gt_2000_yday,1) ||  '% (' ||  trunc(asp_gt_2000_yday,0) || ')') AS rev_gt_2000_yday
	
/*	(trunc(a.rev_lt_1000_mtd,1) ||  '% (' ||  trunc(asp_lt_1000_mtd,0) || ')') AS rev_lt_1000_mtd,
	(trunc(a.rev_lt_1500_mtd,1) ||  '% (' ||  trunc(asp_lt_1500_mtd,0) || ')') AS rev_lt_1500_mtd,
	(trunc(a.rev_lt_2000_mtd,1) ||  '% (' ||  trunc(asp_lt_2000_mtd,0) || ')') AS rev_lt_2000_mtd,
	(trunc(a.rev_gt_2000_mtd,1) ||  '% (' ||  trunc(asp_gt_2000_mtd,0) || ')') AS rev_gt_2000_mtd,

	(trunc(a.rev_lt_1000_m1,1) ||  '% (' ||  trunc(asp_lt_1000_m1,0) || ')') AS rev_lt_1000_m1,
	(trunc(a.rev_lt_1500_m1,1) ||  '% (' ||  trunc(asp_lt_1500_m1,0) || ')') AS rev_lt_1500_m1,
	(trunc(a.rev_lt_2000_m1,1) ||  '% (' ||  trunc(asp_lt_2000_m1,0) || ')') AS rev_lt_2000_m1,
	(trunc(a.rev_gt_2000_m1,1) ||  '% (' ||  trunc(asp_gt_2000_m1,0) || ')') AS rev_gt_2000_m1,

	(trunc(a.rev_lt_1000_m2,1) ||  '% (' ||  trunc(asp_lt_1000_m2,0) || ')') AS rev_lt_1000_m2,
	(trunc(a.rev_lt_1500_m2,1) ||  '% (' ||  trunc(asp_lt_1500_m2,0) || ')') AS rev_lt_1500_m2,
	(trunc(a.rev_lt_2000_m2,1) ||  '% (' ||  trunc(asp_lt_2000_m2,0) || ')') AS rev_lt_2000_m2,
	(trunc(a.rev_gt_2000_m2,1) ||  '% (' ||  trunc(asp_gt_2000_m2,0) || ')') AS rev_gt_2000_m2,

	(trunc(a.rev_lt_1000_m3,1) ||  '% (' ||  trunc(asp_lt_1000_m3,0) || ')') AS rev_lt_1000_m3,
	(trunc(a.rev_lt_1500_m3,1) ||  '% (' ||  trunc(asp_lt_1500_m3,0) || ')') AS rev_lt_1500_m3,
	(trunc(a.rev_lt_2000_m3,1) ||  '% (' ||  trunc(asp_lt_2000_m3,0) || ')') AS rev_lt_2000_m3,
	(trunc(a.rev_gt_2000_m3,1) ||  '% (' ||  trunc(asp_gt_2000_m3,0) || ')') AS rev_gt_2000_m3*/

FROM
	(SELECT
		cast(SUM(CASE WHEN b.revenue < 1000 AND aa.day_diff=1 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.day_diff=1 THEN 1 END) AS rev_lt_1000_yday,
		AVG(CASE WHEN b.revenue < 1000 AND aa.day_diff=1 THEN b.revenue END) AS asp_lt_1000_yday,
		cast(SUM(CASE WHEN b.revenue >= 1000 AND b.revenue < 1500 AND aa.day_diff=1 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.day_diff=1 THEN 1 END) AS rev_lt_1500_yday,
		AVG(CASE WHEN b.revenue >= 1000 AND b.revenue < 1500 AND aa.day_diff=1 THEN b.revenue END) AS asp_lt_1500_yday,
		cast(SUM(CASE WHEN b.revenue >= 1500 AND b.revenue < 2000 AND aa.day_diff=1 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.day_diff=1 THEN 1 END) AS rev_lt_2000_yday,
		AVG(CASE WHEN b.revenue >= 1500 AND b.revenue < 2000 AND aa.day_diff=1 THEN b.revenue END) AS asp_lt_2000_yday,
		cast(SUM(CASE WHEN b.revenue >= 2000 AND aa.day_diff=1 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.day_diff=1 THEN 1 END) AS rev_gt_2000_yday,
		AVG(CASE WHEN b.revenue >= 2000 AND aa.day_diff=1 THEN b.revenue END) AS asp_gt_2000_yday
		
/*		cast(SUM(CASE WHEN b.revenue < 1000 AND aa.month_to_date=1 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_to_date=1 THEN 1 END) AS rev_lt_1000_mtd,
		AVG(CASE WHEN b.revenue < 1000 AND aa.month_to_date=1 THEN b.revenue END) AS asp_lt_1000_mtd,
		cast(SUM(CASE WHEN b.revenue >= 1000 AND b.revenue < 1500 AND aa.month_to_date=1 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_to_date=1 THEN 1 END) AS rev_lt_1500_mtd,
		AVG(CASE WHEN b.revenue >= 1000 AND b.revenue < 1500 AND aa.month_to_date=1 THEN b.revenue END) AS asp_lt_1500_mtd,
		cast(SUM(CASE WHEN b.revenue >= 1500 AND b.revenue < 2000 AND aa.month_to_date=1 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_to_date=1 THEN 1 END) AS rev_lt_2000_mtd,
		AVG(CASE WHEN b.revenue >= 1500 AND b.revenue < 2000 AND aa.month_to_date=1 THEN b.revenue END) AS asp_lt_2000_mtd,
		cast(SUM(CASE WHEN b.revenue >= 2000 AND aa.month_to_date=1 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_to_date=1 THEN 1 END) AS rev_gt_2000_mtd,
		AVG(CASE WHEN b.revenue >= 2000 AND aa.month_to_date=1 THEN b.revenue END) AS asp_gt_2000_mtd,
	
		cast(SUM(CASE WHEN b.revenue < 1000 AND aa.month_diff=1 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_diff=1 THEN 1 END) AS rev_lt_1000_m1,
		AVG(CASE WHEN b.revenue < 1000 AND aa.month_diff=1 THEN b.revenue END) AS asp_lt_1000_m1,
		cast(SUM(CASE WHEN b.revenue >= 1000 AND b.revenue < 1500 AND aa.month_diff=1 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_diff=1 THEN 1 END) AS rev_lt_1500_m1,
		AVG(CASE WHEN b.revenue >= 1000 AND b.revenue < 1500 AND aa.month_diff=1 THEN b.revenue END) AS asp_lt_1500_m1,
		
		cast(SUM(CASE WHEN b.revenue >= 1500 AND b.revenue < 2000 AND aa.month_diff=1 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_diff=1 THEN 1 END) AS rev_lt_2000_m1,
		AVG(CASE WHEN b.revenue >= 1500 AND b.revenue < 2000 AND aa.month_diff=1 THEN b.revenue END) AS asp_lt_2000_m1,
		cast(SUM(CASE WHEN b.revenue >= 2000 AND aa.month_diff=1 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_diff=1 THEN 1 END) AS rev_gt_2000_m1,
		AVG(CASE WHEN b.revenue >= 2000 AND aa.month_diff=1 THEN b.revenue END) AS asp_gt_2000_m1,
		
		cast(SUM(CASE WHEN b.revenue < 1000 AND aa.month_diff=2 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_diff=2 THEN 1 END) AS rev_lt_1000_m2,
		AVG(CASE WHEN b.revenue < 1000 AND aa.month_diff=2 THEN b.revenue END) AS asp_lt_1000_m2,
		cast(SUM(CASE WHEN b.revenue >= 1000 AND b.revenue < 1500 AND aa.month_diff=2 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_diff=2 THEN 1 END) AS rev_lt_1500_m2,
		AVG(CASE WHEN b.revenue >= 1000 AND b.revenue < 1500 AND aa.month_diff=2 THEN b.revenue END) AS asp_lt_1500_m2,
		cast(SUM(CASE WHEN b.revenue >= 1500 AND b.revenue < 2000 AND aa.month_diff=2 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_diff=2 THEN 1 END) AS rev_lt_2000_m2,
		AVG(CASE WHEN b.revenue >= 1500 AND b.revenue < 2000 AND aa.month_diff=2 THEN b.revenue END) AS asp_lt_2000_m2,
		cast(SUM(CASE WHEN b.revenue >= 2000 AND aa.month_diff=2 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_diff=2 THEN 1 END) AS rev_gt_2000_m2,
		AVG(CASE WHEN b.revenue >= 2000 AND aa.month_diff=2 THEN b.revenue END) AS asp_gt_2000_m2,
	
		cast(SUM(CASE WHEN b.revenue < 1000 AND aa.month_diff=3 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_diff=3 THEN 1 END) AS rev_lt_1000_m3,
		AVG(CASE WHEN b.revenue < 1000 AND aa.month_diff=3 THEN b.revenue END) AS asp_lt_1000_m3,
		cast(SUM(CASE WHEN b.revenue >= 1000 AND b.revenue < 1500 AND aa.month_diff=3 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_diff=3 THEN 1 END) AS rev_lt_1500_m3,
		AVG(CASE WHEN b.revenue >= 1000 AND b.revenue < 1500 AND aa.month_diff=3 THEN b.revenue END) AS asp_lt_1500_m3,
		cast(SUM(CASE WHEN b.revenue >= 1500 AND b.revenue < 2000 AND aa.month_diff=3 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_diff=3 THEN 1 END) AS rev_lt_2000_m3,
		AVG(CASE WHEN b.revenue >= 1500 AND b.revenue < 2000 AND aa.month_diff=3 THEN b.revenue END) AS asp_lt_2000_m3,
		cast(SUM(CASE WHEN b.revenue >= 2000 AND aa.month_diff=3 THEN 1 END) as numeric)*100/SUM(CASE WHEN aa.month_diff=3 THEN 1 END) AS rev_gt_2000_m3,
		AVG(CASE WHEN b.revenue >= 2000 AND aa.month_diff=3 THEN b.revenue END) AS asp_gt_2000_m3*/
	
	FROM 
		(SELECT SUM(item_revenue+cashback_redeemed) AS revenue, MIN(o.order_created_date) AS order_created_date
		FROM orders_directional o
		WHERE o.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
		AND (o.is_shipped = 1 OR o.is_realised=1) 
		GROUP BY o.order_group_id) b 
		JOIN
		(select full_date, day_diff, month_diff, month_to_date from dim_date where month_diff<=3) aa on b.order_created_date=aa.full_date
		)a                                                                                                                       
