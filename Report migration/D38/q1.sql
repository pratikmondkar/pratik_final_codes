SELECT 
	titles.*, cc.*
FROM
	(SELECT '0-1' AS title
		UNION SELECT '2-3' AS title
		UNION SELECT '4-5' AS title
		UNION SELECT '6-7' AS title
		UNION SELECT '8-15' AS title
		UNION SELECT '15+' AS title) titles
	LEFT JOIN
	(SELECT 
		COUNT(*) AS style_count,
		sum(b.inwarded_pieces),
		(CASE WHEN(b.pending_days>=0 AND b.pending_days<=1) THEN '0-1'
				WHEN(b.pending_days>1 AND b.pending_days<=3) THEN '2-3'
				WHEN(b.pending_days>3 AND b.pending_days<=5) THEN '4-5'
				WHEN(b.pending_days>5 AND b.pending_days<=7) THEN '6-7'
				WHEN(b.pending_days>7 AND b.pending_days<=15) THEN '8-15'
				WHEN(b.pending_days>15) THEN '15+' 
				ELSE 'NIL' END) AS buckets,
		SUM(b.pending_days) AS sum_pending_days,
		SUM(b.pending_days*b.inwarded_pieces) AS sum_pending_multiply_inwards
	FROM 
		(SELECT 
			 ip.inward AS inwarded_pieces,
			a.style_id, a.style_name, a.vendor_article_number, a.brand,photoshoot_issued_date,style_first_inward_date,style_first_lot_created_date,
		(CASE WHEN (photoshoot_issued_date IS NOT NULL AND photoshoot_issued_date>'1970-01-01' AND fin_status=1) THEN DATEDIFF(d,sysdate, a.photoshoot_issued_date)
				 WHEN (photoshoot_issued_date IS NOT NULL AND photoshoot_issued_date>'1970-01-01' AND fin_status=0) THEN DATEDIFF(d,sysdate, a.photoshoot_issued_date)
				 WHEN (style_first_inward_date IS NOT NULL AND style_first_inward_date>'1970-01-01' AND fin_status=0) THEN DATEDIFF(d,sysdate, a.style_first_inward_date)
				 /*when (style_first_lot_created_date is not null and style_first_lot_created_date!=0) then datediff(now(), a.style_first_lot_created_date)*/ END) AS pending_days,
			(CASE WHEN (photoshoot_issued_date IS NOT NULL AND photoshoot_issued_date>'1970-01-01' AND fin_status=1) THEN 'Stuck-Photoshoot'
				 WHEN (photoshoot_issued_date IS NOT NULL AND photoshoot_issued_date>'1970-01-01' AND fin_status=0) THEN 'Stuck-Transit'
				 WHEN (style_first_inward_date IS NOT NULL AND style_first_inward_date>'1970-01-01' AND fin_status=0) THEN 'Stuck-Inward'
				 /*when (style_first_lot_created_date is not null and style_first_lot_created_date!=0) then 'Stuck-Lot'*/ END) AS pending_reason
		FROM
			(SELECT 
				p.style_id, max(p.style_name) as style_name, max(p.vendor_article_number) as vendor_article_number, max(p.brand) as brand,
				MIN(NOW() /*grn.lot_received_date + interval t3.time_value HOUR_MINUTE*/) AS style_first_lot_created_date,
				MAX(CASE WHEN p.first_inward_date > 19700101 THEN cast(p.first_inward_date ||' '|| lpad(t3.time_value,4,0) as timestamp)  END) AS style_first_inward_date,
				MAX(COALESCE(
					(CASE WHEN p.photoshoot_issue_date>19700101 THEN cast(p.photoshoot_issue_date ||' '|| lpad(t2.time_value,4,0) as timestamp)  END),
					(SELECT MIN(cast(dt.actual_date ||' '|| lpad(t.time_value,4,0) as timestamp)) FROM fact_inventory_location loc, fact_inventory_item item, dim_date dt, dim_time t  WHERE loc.core_item_id = item.core_item_id AND item.sku_id = p.sku_id AND loc.bin_id = 47822 AND loc.state_change_date = dt.full_date AND loc.state_change_time = t.time))) AS photoshoot_issued_date,
				max(p.style_catalogued_date) as style_catalogued_date,
				MAX(CASE WHEN fii.fii_cnt  = 0 THEN 0
				WHEN fii.fii_cnt > 0 THEN 1 END) AS fin_status
			FROM 
				dim_product p, dim_time t1, dim_time t2, dim_time t3,
				(SELECT sku_id,COUNT(1) as fii_cnt FROM fact_inventory_item fii WHERE fii.quality='Q1' AND warehouse_id=3 AND item_status!='TRANSIT' group by 1) fii
			WHERE 
				p.style_status IN ('D','CON') AND p.style_catalogued_time = t1.time 
				AND NOT EXISTS (SELECT 1 FROM dim_product p1 WHERE p1.style_id = p.style_id AND p1.style_status = 'P') AND p.photoshoot_issue_time = t2.time and fii.sku_id-p.sku_id
				AND p.first_inward_time = t3.time AND p.first_inward_date > 19700101
			GROUP BY
				p.style_id) a 
				left join 
				(SELECT style_id,COUNT(*) as inward FROM fact_inventory_item item, dim_product p2 WHERE item.sku_id = p2.sku_id  AND item.item_status ='STORED' AND item.quality='Q1' group by 1) ip
				on a.style_id = ip.style_id
				WHERE style_catalogued_date = 19700101) b
	WHERE
		b.pending_reason='Stuck-Photoshoot' AND b.inwarded_pieces > 0
	GROUP BY 
		buckets) cc ON cc.buckets=titles.title                                                                                                                                          
