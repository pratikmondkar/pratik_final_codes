/*BM42 RGM with buckets*/
select 
	cast(sum(case when a.item_rgm < 0 and aa.day_diff=1 then 1 end) as numeric)/sum(case when aa.day_diff=1 then 1 end) as yday_lt_0_rgm,
	cast(sum(case when a.item_rgm between 0 and 249 and aa.day_diff=1 then 1 end) as numeric)/sum(case when aa.day_diff=1 then 1 end) as yday_lt_250_rgm,
	cast(sum(case when a.item_rgm between 250 and 499 and aa.day_diff=1 then 1 end) as numeric)/sum(case when aa.day_diff=1 then 1 end) as yday_lt_500_rgm,
	cast(sum(case when a.item_rgm >= 500 and aa.day_diff=1 then 1 end) as numeric)/sum(case when aa.day_diff=1 then 1 end) as yday_gt_500_rgm,
		
	cast(sum(case when a.item_rgm < 0 and aa.month_to_date=1 then 1 end) as numeric)/sum(case when aa.month_to_date=1 then 1 end) as mtd_lt_0_rgm,
	cast(sum(case when a.item_rgm between 0 and 249 and aa.month_to_date=1 then 1 end) as numeric)/sum(case when aa.month_to_date=1 then 1 end) as mtd_lt_250_rgm,
	cast(sum(case when a.item_rgm between 250 and 499 and aa.month_to_date=1 then 1 end) as numeric)/sum(case when aa.month_to_date=1 then 1 end) as mtd_lt_500_rgm,
	cast(sum(case when a.item_rgm >= 500 and aa.month_to_date=1 then 1 end) as numeric)/sum(case when aa.month_to_date=1 then 1 end) as mtd_gt_500_rgm,

	cast(sum(case when a.item_rgm < 0 and aa.month_diff=1 then 1 end) as numeric)/sum(case when aa.month_diff=1 then 1 end) as m1_lt_0_rgm,
	cast(sum(case when a.item_rgm between 0 and 249 and aa.month_diff=1 then 1 end) as numeric)/sum(case when aa.month_diff=1 then 1 end) as m1_lt_250_rgm,
	cast(sum(case when a.item_rgm between 250 and 499 and aa.month_diff=1 then 1 end) as numeric)/sum(case when aa.month_diff=1 then 1 end) as m1_lt_500_rgm,
	cast(sum(case when a.item_rgm >= 500 and aa.month_diff=1 then 1 end) as numeric)/sum(case when aa.month_diff=1 then 1 end) as m1_gt_500_rgm,

	cast(sum(case when a.item_rgm < 0 and aa.month_diff=2 then 1 end) as numeric)/sum(case when aa.month_diff=2 then 1 end) as m2_lt_0_rgm,
	cast(sum(case when a.item_rgm between 0 and 249 and aa.month_diff=2 then 1 end) as numeric)/sum(case when aa.month_diff=2 then 1 end) as m2_lt_250_rgm,
	cast(sum(case when a.item_rgm between 250 and 499 and aa.month_diff=2 then 1 end) as numeric)/sum(case when aa.month_diff=2 then 1 end) as m2_lt_500_rgm,
	cast(sum(case when a.item_rgm >= 500 and aa.month_diff=2 then 1 end) as numeric)/sum(case when aa.month_diff=2 then 1 end) as m2_gt_500_rgm,

	cast(sum(case when a.item_rgm < 0 and aa.month_diff=3 then 1 end) as numeric)/sum(case when aa.month_diff=3 then 1 end) as m3_lt_0_rgm,
	cast(sum(case when a.item_rgm between 0 and 249 and aa.month_diff=3 then 1 end) as numeric)/sum(case when aa.month_diff=3 then 1 end) as m3_lt_250_rgm,
	cast(sum(case when a.item_rgm between 250 and 499 and aa.month_diff=3 then 1 end) as numeric)/sum(case when aa.month_diff=3 then 1 end) as m3_lt_500_rgm,
	cast(sum(case when a.item_rgm >= 500 and aa.month_diff=3 then 1 end) as numeric)/sum(case when aa.month_diff=3 then 1 end) as m3_gt_500_rgm

from 
	(select 
		oi.order_group_id, min(oi.order_created_date) as order_created_date, 		
round(sum(oi.item_revenue_inc_cashback)-sum(oi.tax)+max(o.shipping_charges)+max(gift_charges)+max(emi_charges)
			-sum(cogs+royalty_commission)) as item_rgm,
		sum(oi.item_revenue_inc_cashback)+max(o.shipping_charges)+max(gift_charges)+max(emi_charges) as item_revenue_inc_tax		
	from fact_orderitem oi, fact_order o, dim_product p 
	where oi.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
	and (oi.is_shipped=1 or oi.is_realised = 1) and oi.store_id=2
		and o.order_id = oi.order_id and oi.idproduct = p.id
	group by oi.order_group_id) a
	join
	(select full_date,day_diff, month_to_date, month_diff from dim_date where month_diff<=3) aa on a.order_created_date=aa.full_date               
