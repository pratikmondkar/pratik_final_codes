/*BM42 New registrations*/
select 
	day_minus_1_registration as yday_registration,
	trunc((a.day_minus_1_registration - a.day_minus_2_registration)*100/a.day_minus_2_registration, 1) as p1,
	mtd_registration, month_minus_1_registration, month_minus_2_registration, month_minus_3_registration
from
	(select 
		sum(case when dt.day_diff = 1 then 1 end) as day_minus_1_registration,
		sum(case when dt.day_diff = 2 then 1 end) as day_minus_2_registration,
		sum(case when dt.month_to_date = 1 then 1 end) as mtd_registration,
		sum(case when dt.month_diff = 1 then 1 end) as month_minus_1_registration,
		sum(case when dt.month_diff = 2 then 1 end) as month_minus_2_registration,
		sum(case when dt.month_diff = 3 then 1 end) as month_minus_3_registration
	from dim_customer c, dim_date dt
	where c.first_login_date = dt.full_date and dt.month_diff<=3
and c.first_login_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')) a