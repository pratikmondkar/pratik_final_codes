

select  Platform  , 
(yday_sessions_app/yday_sessions_app_tot)::float as yday_sessions_app_perc ,
((yday_sessions_app/yday_sessions_app_tot::float)-(yday_minus_1_sessions_app/yday_minus_1_sessions_app_tot::float))::float*100 as p1,
(mtd_sessions_app/mtd_sessions_app_tot::float) as mtd_sessions_app_perc,
(month_minus_1_sessions_app/month_minus_1_sessions_app_tot)::float as month_minus_1_sessions_app_perc ,
(month_minus_2_sessions_app/month_minus_2_sessions_app_tot)::float as month_minus_2_sessions_app_perc ,
(month_minus_3_sessions_app/month_minus_3_sessions_app_tot)::float as month_minus_3_sessions_app_perc 
from
(
select  case when app_platform   = 'windows' then '-Windows' when app_platform ='android' then '-Android'  when app_platform ='iOS' then '-iOS' end as Platform,
		1 as flag,
		sum(case when dt.day_diff = 1   then sessions end)::float as yday_sessions_app,
		sum(case when dt.day_diff = 2    then sessions end) as yday_minus_1_sessions_app,
		sum(case when dt.month_to_date = 1   then sessions end)::float  as mtd_sessions_app,
		sum(case when dt.month_diff = 1   then sessions end)::float  as month_minus_1_sessions_app,
		sum(case when dt.month_diff = 2   then sessions end)::float  as month_minus_2_sessions_app,
		sum(case when dt.month_diff = 3   then sessions end)::float  as month_minus_3_sessions_app	
	from 
		google_analytics.ga_metrics ga, dim_date dt 
	where 
		dt.month_diff<=3 
		and ga.date = dt.full_date 
		and ga.date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
		and app_platform <>'web'
		group by 1,2
	) a
	join
(
select  1 as flag,
		sum(case when dt.day_diff = 1   then sessions end)::float as yday_sessions_app_tot,
		sum(case when dt.day_diff = 2    then sessions end) as yday_minus_1_sessions_app_tot,
		sum(case when dt.month_to_date = 1   then sessions end)  as mtd_sessions_app_tot,
		sum(case when dt.month_diff = 1   then sessions end)   as month_minus_1_sessions_app_tot,
		sum(case when dt.month_diff = 2   then sessions end)   as month_minus_2_sessions_app_tot,
		sum(case when dt.month_diff = 3   then sessions end)  as month_minus_3_sessions_app_tot	
	from 
		google_analytics.ga_metrics ga, dim_date dt 
	where 
		dt.month_diff<=3 
		and ga.date = dt.full_date 
		and ga.date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
		and app_platform <>'web'
		group by 1
)	b 
on a.flag=b.flag
where platform is not null
order by platform
            