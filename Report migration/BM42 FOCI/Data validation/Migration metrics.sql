--FOCI
SELECT 
			(CASE WHEN (foci.supply_type = 'JUST_IN_TIME' OR po_type = 'JIT_Marketplace') THEN 'MP' WHEN (UPPER(po_type) = 'OUTRIGHT' AND UPPER(foci.brand_type) = 'PRIVATE') THEN 'MFB' WHEN (UPPER(po_type) = 'SOR') THEN 'SOR' WHEN (UPPER(po_type) = 'OUTRIGHT') THEN 'OUTRIGHT' ELSE NULL END) AS foci_category,
      (CASE WHEN (foci.supply_type = 'JUST_IN_TIME' OR commercial_type = 'JIT_Marketplace') THEN 'MP' WHEN (UPPER(commercial_type) = 'OUTRIGHT' AND UPPER(dp.brand_type) = 'PRIVATE') THEN 'MFB' WHEN (UPPER(commercial_type) = 'SOR') THEN 'SOR' WHEN (UPPER(commercial_type) = 'OUTRIGHT') THEN 'OUTRIGHT' ELSE NULL END) AS foi_category,
      SUM(item_revenue_inc_cashback) AS total_gmv,
      sum(cogs) as cogs,
      sum(tax) as tax,
      sum(rgm_final) as rgm
      

FROM fact_core_item foci
join dim_product dp
on foci.sku_id=dp.sku_id
WHERE foci.order_created_date =20160102
AND   (is_shipped = 1 OR is_realised = 1)
AND   store_id = 1
GROUP BY 1,2
ORDER BY 1,2

