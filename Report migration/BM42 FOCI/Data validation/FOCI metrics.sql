SELECT aa.month_diff,
       SUM(b.revenue + b.other_charges - b.net_cost_price) AS rgm,
       SUM(b.revenue_mrkt - b.net_cost_price_mrkt) AS rgm_mrkt,
       SUM(b.revenue_private - b.net_cost_price_private) AS rgm_private,
       SUM(nvl (b.revenue + b.other_charges,0)) AS revenue,
       SUM(b.revenue_mrkt) AS revenue_mrkt,
       SUM(b.revenue_private) AS revenue_private,
       SUM(b.orders) AS orders,
       SUM(b.qty) AS qty,
       sum(rev+b.other_charges) as rev
FROM (SELECT order_created_date,
             SUM(oi.item_revenue_inc_cashback - oi.tax) AS revenue,
             SUM(quantity) AS qty,
             SUM(oi.item_revenue_inc_cashback) AS rev,
             COUNT(DISTINCT order_group_id) AS orders,
             SUM(CASE WHEN oi.supply_type = 'JUST_IN_TIME' THEN (oi.item_revenue_inc_cashback - oi.tax) END) AS revenue_mrkt,
             SUM(CASE WHEN p.brand_type = 'Private' THEN (oi.item_revenue_inc_cashback - oi.tax) END) AS revenue_private,
             SUM(nvl (cogs,0) + nvl (royalty_commission,0) + nvl (stn_input_vat_reversal,0) + nvl (entry_tax,0)) AS net_cost_price,
             SUM(CASE WHEN oi.supply_type = 'JUST_IN_TIME' THEN cogs + nvl (stn_input_vat_reversal,0) + nvl (entry_tax,0) END) AS net_cost_price_mrkt,
             SUM(CASE WHEN p.brand_type = 'Private' THEN nvl (cogs,0) + nvl (royalty_commission,0) + nvl (stn_input_vat_reversal,0) + nvl (entry_tax,0) END) AS net_cost_price_private,
             SUM(nvl (oi.shipping_charges,0) + nvl (oi.cod_charges,0) + nvl (oi.gift_charges,0) + nvl (oi.emi_charges,0)) AS other_charges
      FROM fact_core_item oi,
           dim_product p
      WHERE oi.order_created_date >= TO_CHAR(dateadd (MONTH,-6,convert_timezone ('Asia/Calcutta',getdate ())),'YYYYMM01')
      AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
      AND   store_id = 1
      AND   oi.sku_id = p.sku_id
      GROUP BY 1) b
  JOIN (SELECT full_date,
               month_diff
        FROM dim_date
        WHERE month_diff <= 5) aa ON b.order_created_date = aa.full_date
GROUP BY 1
order by 1
