SELECT (CASE WHEN o.supply_type = 'JUST_IN_TIME' THEN 'MARKET PLACE' WHEN (o.supply_type = 'ON_HAND' AND UPPER(commercial_type) = 'OUTRIGHT' AND UPPER(brand_type) = 'PRIVATE') THEN 'MFB' WHEN (o.supply_type = 'ON_HAND' AND UPPER(commercial_type) = 'SOR') THEN 'MMB SOR' ELSE 'MMB OUTRIGHT' END) AS category,
       TO_CHAR(TO_DATE(order_created_date,'YYYYMMDD'),'MON-YY') as month,
       SUM(item_revenue_inc_cashback) AS total_gmv,
       SUM(tax) AS tax,
       SUM(quantity) AS quantity,
       SUM(vendor_funding) AS vendor_funding,
       SUM(tax_usr_recovered) AS tax_usr_recovered,
       SUM(royalty_commission) AS royalty_commission,
       SUM(cogs) AS cogs,
       COUNT(DISTINCT order_group_id) AS orders,
       COUNT(DISTINCT order_Id) AS shipments,
       sum(case when item_returned_date > 19700101 then item_revenue_inc_cashback end ) as return_revenue,
       sum(case when order_rto_date > 19700101 then item_revenue_inc_cashback end ) as rto_revenue,
       sum(case when item_returned_date > 19700101 then quantity end ) as return_units,
       sum(case when order_rto_date > 19700101 then quantity end ) as return_units
FROM fact_orderitem o
join dim_product dp on o.sku_id=dp.sku_id
WHERE order_created_date >= 20150801
AND   (is_shipped = 1 OR is_realised = 1)
AND   store_id = 1
GROUP BY 1,2
ORDER BY 1,2

