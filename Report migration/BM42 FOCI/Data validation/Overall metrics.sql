SELECT       SUM(item_revenue_inc_cashback) AS rev,
             count(distinct order_group_id) as orders,
             count(distinct order_id) as shipments,
             sum(quantity) as qty,
             sum(cogs) as cogs,
             sum(tax) as tax
      FROM fact_orderitem
      WHERE store_id = 1
      AND   (is_shipped = 1 OR is_realised = 1)
      AND   order_created_date BETWEEN 20151001 AND 20151031;
      
      
SELECT       SUM(item_revenue_inc_cashback) AS rev,
             count(distinct order_group_id) as orders,
             count(distinct order_id) as shipments,
             sum(calculated_quantity) as qty,
             sum(cogs) as cogs,
             sum(tax_on_product) as tax,
             sum(nvl(shipping_charges_for_coreitem,0)) as shipping_charges,
             sum(nvl(gift_charge_for_coreitem,0)) as gift_charges
      FROM fact_core_item
      WHERE store_id = 1
      AND   (is_shipped = 1 OR is_realised = 1)
      AND   order_created_on BETWEEN '2015-10-01 00:00:00' AND '2015-10-31 23:59:59';
      

SELECT SUM(nvl(shipping_charges,0)) as shipping_charges,
			sum(nvl(gift_charges,0)) AS gift_charges
      FROM fact_order
      WHERE store_id = 1
      AND   (is_shipped = 1 OR is_realised = 1)
      AND   order_created_date BETWEEN 20151001 AND 20151031
