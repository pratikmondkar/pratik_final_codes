SELECT *       
FROM (SELECT order_id AS foi_orders,
             SUM(nvl(shipping_charges,0)+nvl(gift_charges,0)) AS other_charges
      FROM fact_order
      WHERE store_id = 1
      AND   (is_shipped = 1 OR is_realised = 1)
      AND   order_created_date BETWEEN 20151001 AND 20151031
      GROUP BY 1) foi
  INNER JOIN (SELECT order_id AS foci_orders,
                     sum(nvl(shipping_charges_for_coreitem,0)+nvl(gift_charge_for_coreitem,0)) as other_charges
              FROM fact_core_item
              WHERE store_id = 1
              AND   (is_shipped = 1 OR is_realised = 1)
              AND   order_created_on BETWEEN '2015-10-01 00:00:00' AND '2015-10-31 23:59:59'
              GROUP BY 1) foci ON foi_orders = foci_orders::numeric
WHERE foi.other_charges-foci.other_charges>0.2
LIMIT 2000;


select order_id,order_group_id,po_id,shipping_charges_for_coreitem,shipping_charges_for_shipment from fact_core_item where 
order_id in (95024888,95036363)

