SELECT 
				to_char(oi.order_created_on,'YYYYMMDD') as order_created_date ,
				sum(oi.item_revenue_inc_cashback) as item_revenue_inc_cashback,
				sum(oi.tax_on_product) as tax,
				SUM(CASE when oi.supply_type='JUST_IN_TIME' then (oi.item_revenue_inc_cashback-oi.tax_on_product) END) as revenue_mrkt,
				SUM(CASE when p.brand_type='Private' then (oi.item_revenue_inc_cashback-oi.tax_on_product) END) as revenue_private,
				SUM(nvl(cogs,0)) as cogs,
				sum(nvl(royalty_commission,0)) as royalty_commission,
				SUM(CASE WHEN oi.supply_type='JUST_IN_TIME' then cogs END) as cogs_mrkt,
				SUM(CASE WHEN p.brand_type='Private' then nvl(cogs,0)+nvl(royalty_commission,0) END) as cogs_private,
				sum(oi.shipping_charges_for_coreitem+oi.cod_charge_for_coreitem+oi.gift_charge_for_coreitem+oi.emi_charge_for_coreitem) as other_charges
					 		 
			FROM 
			fact_core_item_20151110 oi, dim_product p  
			WHERE to_char(oi.order_created_on,'YYYYMMDD')>=to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01') 
			AND (oi.is_shipped=1 OR oi.is_realised=1) and store_id=1 AND oi.sku_id = p.sku_id 
			GROUP BY 1
