SELECT aa.month_diff,
       sum(b.revenue + a.other_charges - b.net_cost_price) AS rgm,
       sum(b.revenue_mrkt - b.net_cost_price_mrkt) AS rgm_mrkt,
       sum(b.revenue_private - b.net_cost_price_private) AS rgm_private,
       sum(nvl(b.revenue + a.other_charges,0)) AS revenue,
       sum(b.revenue_mrkt) as revenue_mrkt,
       sum(b.revenue_private) as revenue_private,
       SUM(b.orders) AS orders,
       SUM(b.qty) AS qty,
       sum(rev+a.other_charges) as rev
FROM (SELECT o.order_created_date,
             SUM(o.shipping_charges + o.cod_charges + o.gift_charges + o.emi_charges) AS other_charges
      FROM fact_order o,
           dim_date dt
      WHERE dt.month_diff <= 5
      AND   store_id = 1
      AND   (o.is_shipped = 1 OR o.is_realised = 1)
      AND   o.order_created_date = dt.full_date
      AND   o.order_created_date >= TO_CHAR(dateadd (MONTH,-6,convert_timezone ('Asia/Calcutta',getdate ())),'YYYYMM01')
      GROUP BY o.order_created_date) a
  JOIN (SELECT full_date,
               day_diff,
               month_to_date,
               month_diff,
               last7_days
        FROM dim_date
        WHERE month_diff <= 5) aa ON a.order_created_date = aa.full_date
  LEFT JOIN (SELECT oi.order_created_date,
  									count(distinct order_group_id) AS orders,
       							SUM(quantity) AS qty,
       							SUM(oi.item_revenue_inc_cashback) AS rev,
                    SUM(oi.item_revenue_inc_cashback - oi.tax) AS revenue,
                    SUM(CASE WHEN oi.supply_type = 'JUST_IN_TIME' THEN (oi.item_revenue_inc_cashback - oi.tax) END) AS revenue_mrkt,
                    SUM(CASE WHEN p.brand_type = 'Private' THEN (oi.item_revenue_inc_cashback - oi.tax) END) AS revenue_private,
                    SUM(CASE WHEN oi.current_season_flag = 1 THEN (oi.item_revenue_inc_cashback - oi.tax) END) AS revenue_season1,
                    SUM(CASE WHEN (oi.current_season_flag = 0 OR oi.current_season_flag IS NULL) THEN (oi.item_revenue_inc_cashback - oi.tax) END) AS revenue_season0,
                    SUM(cogs + royalty_commission) AS net_cost_price,
                    SUM(CASE WHEN oi.supply_type = 'JUST_IN_TIME' THEN cogs END) AS net_cost_price_mrkt,
                    SUM(CASE WHEN p.brand_type = 'Private' THEN cogs + royalty_commission END) AS net_cost_price_private
             FROM fact_orderitem oi,
                  dim_product p
             WHERE oi.order_created_date >= TO_CHAR(dateadd (MONTH,-6,convert_timezone ('Asia/Calcutta',getdate ())),'YYYYMM01')
             AND   (oi.is_shipped = 1 OR oi.is_realised = 1)
             AND   store_id = 1
             AND   oi.idproduct = p.id
             GROUP BY oi.order_created_date) b ON a.order_created_date = b.order_created_date
group by 1
order by 1
