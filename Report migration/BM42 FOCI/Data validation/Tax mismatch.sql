SELECT *
--			 SUM(foi.cogs) AS foi_cogs,
--       SUM(foci.cogs) AS foci_cogs,
--       SUM(foi.tax) AS foi_tax,
--       SUM(foci.tax) AS foci_tax
       
FROM (SELECT order_id AS foi_orders,
             MAX(order_group_id) AS order_group_id,
             SUM(item_revenue_inc_cashback) AS rev,
             sum(quantity) as qty,
             SUM(cogs) AS cogs,
             SUM(tax) AS tax
      FROM fact_orderitem
      WHERE store_id = 1
      AND   (is_shipped = 1 OR is_realised = 1)
      AND   order_created_date BETWEEN 20151001 AND 20151031
      GROUP BY 1) foi
  FULL OUTER JOIN (SELECT order_id AS foci_orders,
                     MAX(order_group_id) AS order_group_id,
                     SUM(item_revenue_inc_cashback) AS rev,
                     sum(calculated_quantity) as qty,
                     SUM(cogs) AS cogs,
                     SUM(tax_on_product) AS tax
              FROM fact_core_item
              WHERE store_id = 1
              AND   (is_shipped = 1 OR is_realised = 1)
              AND   order_created_on BETWEEN '2015-10-01 00:00:00' AND '2015-10-31 23:59:59'
              GROUP BY 1) foci ON foi_orders = foci_orders::numeric
WHERE foi.tax-foci.tax>0.2
LIMIT 2000;

select order_id,order_group_id,po_id,tax_type,tax_percentage,tax_recovered,tax_on_product,entry_tax from fact_core_item where 
order_id in(95935628,95061539)


