SELECT 
			b.*,a.other_charges			
			FROM 
			(SELECT 
				o.order_created_date,
				sum(o.shipping_charges+o.cod_charges+o.gift_charges+o.emi_charges) as other_charges
			FROM fact_order o
			WHERE  store_id=1 and (o.is_shipped=1 OR o.is_realised=1)
			and o.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
			GROUP BY o.order_created_date) a
			LEFT JOIN
			(SELECT 
				order_created_date,
				sum(oi.item_revenue_inc_cashback) as item_revenue_inc_cashback,
				sum(oi.tax) as tax,
				SUM(CASE when oi.supply_type='JUST_IN_TIME' then (oi.item_revenue_inc_cashback-oi.tax) END) as revenue_mrkt,
				SUM(CASE when p.brand_type='Private' then (oi.item_revenue_inc_cashback-oi.tax) END) as revenue_private,
				SUM(nvl(cogs,0)) as cogs,
				sum(nvl(royalty_commission,0)) as royalty_commission,
				SUM(CASE WHEN oi.supply_type='JUST_IN_TIME' then cogs END) as cogs_mrkt,
				SUM(CASE WHEN p.brand_type='Private' then nvl(cogs,0)+nvl(royalty_commission,0) END) as cogs_private
			FROM 
			fact_orderitem oi, dim_product p  
			WHERE oi.order_created_date>=to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01') 
			AND (oi.is_shipped=1 OR oi.is_realised=1) and store_id=1 AND oi.idproduct = p.id 
			GROUP BY oi.order_created_date) b
			ON a.order_created_date = b.order_created_date			
order by 1
