--FOCI
SELECT (CASE WHEN (supply_type = 'JUST_IN_TIME' OR po_type = 'JIT_Marketplace') THEN 'MP' WHEN (UPPER(po_type) = 'OUTRIGHT' AND UPPER(brand_type) = 'PRIVATE') THEN 'MFB' WHEN (UPPER(po_type) = 'SOR') THEN 'SOR' WHEN (UPPER(po_type) = 'OUTRIGHT') THEN 'OUTRIGHT' ELSE NULL END) AS category,
       TO_CHAR(order_release_created_on,'MON-YY') AS month,
       SUM(item_revenue_inc_cashback) AS total_gmv,
       SUM(tax_on_product) AS tax,
       SUM(calculated_quantity) AS quantity,
       SUM(0) AS vendor_funding,
       SUM(tax_recovered) AS tax_usr_recovered,
       SUM(royalty_commission) AS royalty_commission,
       SUM(cogs) AS cogs,
       COUNT(DISTINCT order_group_id) AS orders,
       COUNT(DISTINCT order_Id) AS shipments,
       SUM(CASE WHEN restocked_date > 19700101 THEN item_revenue_inc_cashback END) AS return_revenue,
       SUM(CASE WHEN rto_completed_date > 19700101 THEN item_revenue_inc_cashback END) AS rto_revenue,
       SUM(CASE WHEN restocked_date > 19700101 THEN calculated_quantity END) AS return_units,
       SUM(CASE WHEN rto_completed_date > 19700101 THEN calculated_quantity END) AS return_units
FROM fact_core_item foci
WHERE foci.order_release_created_on >= '2015-08-01 00:00:00'
AND   (is_shipped = 1 OR is_realised = 1)
AND   store_id = 1
GROUP BY 1,2
ORDER BY 1,2

