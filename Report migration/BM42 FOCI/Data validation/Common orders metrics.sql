SELECT COUNT(DISTINCT foi_orders) AS foi_shipments,
			 COUNT(DISTINCT foci_orders) AS foci_shipments,
			 COUNT(DISTINCT foi.order_group_id) AS foi_orders, 
       COUNT(DISTINCT foci.order_group_id) AS foci_orders,       
			 SUM(foi.rev) AS foi_rev,
       SUM(foci.rev) AS foci_rev,
			 SUM(foi.qty) AS foi_qty,
       SUM(foci.qty) AS foci_qty,
       SUM(foi.cogs) AS foi_cogs,
       SUM(foci.cogs) AS foci_cogs,
       SUM(foi.tax) AS foi_tax,
       SUM(foci.tax) AS foci_tax
       
FROM (SELECT order_id AS foi_orders,
             MAX(order_group_id) AS order_group_id,
             SUM(item_revenue_inc_cashback) AS rev,
             sum(quantity) as qty,
             SUM(cogs) AS cogs,
             SUM(tax) AS tax
      FROM fact_orderitem
      WHERE store_id = 1
      AND   (is_shipped = 1 OR is_realised = 1)
      AND   order_created_date BETWEEN 20151001 AND 20151031
      GROUP BY 1) foi
  INNER JOIN (SELECT order_id AS foci_orders,
                     MAX(order_group_id) AS order_group_id,
                     SUM(item_revenue_inc_cashback) AS rev,
                     sum(calculated_quantity) as qty,
                     SUM(cogs) AS cogs,
                     SUM(tax_on_product) AS tax
              FROM fact_core_item_20151201
              WHERE store_id = 1
              AND   (is_shipped = 1 OR is_realised = 1)
              AND   order_created_on BETWEEN '2015-10-01 00:00:00' AND '2015-10-31 23:59:59'
              GROUP BY 1) foci ON foi_orders = foci_orders::numeric
--WHERE (foi_orders IS NULL OR foci_orders IS NULL)
LIMIT 1000

