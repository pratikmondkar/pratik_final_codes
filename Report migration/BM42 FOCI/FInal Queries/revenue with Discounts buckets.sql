	/*BM42 revenue with Discounts buckets*/
select 
	sum(case when a.disc_perc = 0 and  day_diff=1 then item_revenue_inc_tax end)/sum(case when  day_diff=1 then item_revenue_inc_tax end) as yday_0_disc,
	sum(case when a.disc_perc between 0.001 and 0.249 and  day_diff=1 then item_revenue_inc_tax end)/sum(case when  day_diff=1 then item_revenue_inc_tax end) as yday_lt_250_disc,
	sum(case when a.disc_perc between 0.25 and 0.499 and  day_diff=1 then item_revenue_inc_tax end)/sum(case when  day_diff=1 then item_revenue_inc_tax end) as yday_lt_500_disc,
	sum(case when a.disc_perc >= 0.5 and  day_diff=1 then item_revenue_inc_tax end)/sum(case when  day_diff=1 then item_revenue_inc_tax end) as yday_gt_500_disc,
		
	sum(case when a.disc_perc = 0 and  month_to_date=1 then item_revenue_inc_tax end)/sum(case when  month_to_date=1 then item_revenue_inc_tax end) as mtd_0_disc,
	sum(case when a.disc_perc between 0.001 and 0.249 and  month_to_date=1 then item_revenue_inc_tax end)/sum(case when  month_to_date=1 then item_revenue_inc_tax end) as mtd_lt_250_disc,
	sum(case when a.disc_perc between 0.25 and 0.499 and  month_to_date=1 then item_revenue_inc_tax end)/sum(case when  month_to_date=1 then item_revenue_inc_tax end) as mtd_lt_500_disc,
	sum(case when a.disc_perc >= 0.5 and  month_to_date=1 then item_revenue_inc_tax end)/sum(case when  month_to_date=1 then item_revenue_inc_tax end) as mtd_gt_500_disc,

	sum(case when a.disc_perc = 0 and  month_diff=1 then item_revenue_inc_tax end)/sum(case when  month_diff=1 then item_revenue_inc_tax end) as m1_0_disc,
	sum(case when a.disc_perc between 0.001 and 0.249 and  month_diff=1 then item_revenue_inc_tax end)/sum(case when  month_diff=1 then item_revenue_inc_tax end) as m1_lt_250_disc,
	sum(case when a.disc_perc between 0.25 and 0.499 and  month_diff=1 then item_revenue_inc_tax end)/sum(case when  month_diff=1 then item_revenue_inc_tax end) as m1_lt_500_disc,
	sum(case when a.disc_perc >= 0.5 and  month_diff=1 then item_revenue_inc_tax end)/sum(case when  month_diff=1 then item_revenue_inc_tax end) as m1_gt_500_disc,

	sum(case when a.disc_perc = 0 and  month_diff=2 then item_revenue_inc_tax end)/sum(case when  month_diff=2 then item_revenue_inc_tax end) as m2_0_disc,
	sum(case when a.disc_perc between 0.001 and 0.249 and  month_diff=2 then item_revenue_inc_tax end)/sum(case when  month_diff=2 then item_revenue_inc_tax end) as m2_lt_250_disc,
	sum(case when a.disc_perc between 0.25 and 0.499 and  month_diff=2 then item_revenue_inc_tax end)/sum(case when  month_diff=2 then item_revenue_inc_tax end) as m2_lt_500_disc,
	sum(case when a.disc_perc >= 0.5 and  month_diff=2 then item_revenue_inc_tax end)/sum(case when  month_diff=2 then item_revenue_inc_tax end) as m2_gt_500_disc,

	sum(case when a.disc_perc = 0 and  month_diff=3 then item_revenue_inc_tax end)/sum(case when  month_diff=3 then item_revenue_inc_tax end) as m3_0_disc,
	sum(case when a.disc_perc between 0.001 and 0.249 and  month_diff=3 then item_revenue_inc_tax end)/sum(case when  month_diff=3 then item_revenue_inc_tax end) as m3_lt_250_disc,
	sum(case when a.disc_perc between 0.25 and 0.499 and  month_diff=3 then item_revenue_inc_tax end)/sum(case when  month_diff=3 then item_revenue_inc_tax end) as m3_lt_500_disc,
	sum(case when a.disc_perc >= 0.5 and  month_diff=3 then item_revenue_inc_tax end)/sum(case when  month_diff=3 then item_revenue_inc_tax end) as m3_gt_500_disc

from 
	(select 
		full_date,day_diff, month_to_date, month_diff,		
(nvl(oi.coupon_discount,0) + nvl(oi.product_discount,0) + nvl(oi.cart_discount,0) + nvl(oi.loyalty_pts_used/2,0))/NULLIF((oi.item_mrp_value*oi.quantity)::decimal(12,2),0) as disc_perc,
		(oi.item_revenue_inc_cashback+nvl(oi.shipping_charges,0)+nvl(oi.gift_charges,0)+nvl(oi.emi_charges,0)) as item_revenue_inc_tax		
	from fact_core_item oi, dim_date d
	where oi.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
	and (oi.is_shipped=1 or oi.is_realised = 1) and oi.store_id=1
		and oi.order_created_date=d.full_date ) a