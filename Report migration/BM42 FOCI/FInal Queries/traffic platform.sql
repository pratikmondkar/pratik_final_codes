select  Platform  , 
yday_sessions_app,
((yday_sessions_app-yday_minus_1_sessions_app)/yday_minus_1_sessions_app)::float*100 as p1,
mtd_sessions_app,
month_minus_1_sessions_app,
month_minus_2_sessions_app,
month_minus_3_sessions_app
from
(
select  case when app_platform   = 'web' then 'Desktop' when app_platform = 'mobile' then 'Msite' else 'App' end as Platform,
		sum(case when dt.day_diff = 1   then sessions end)::float as yday_sessions_app,
		sum(case when dt.day_diff = 2    then sessions end)::float as yday_minus_1_sessions_app,
		sum(case when dt.month_to_date = 1   then sessions end)/(select count(distinct full_date) from dim_date where month_to_date=1) as mtd_sessions_app,
		sum(case when dt.month_diff = 1   then sessions end)/(select count(distinct full_date) from dim_date where month_diff=1) as month_minus_1_sessions_app,
		sum(case when dt.month_diff = 2   then sessions end)/(select count(distinct full_date) from dim_date where month_diff=2) as month_minus_2_sessions_app,
		sum(case when dt.month_diff = 3   then sessions end)/(select count(distinct full_date) from dim_date where month_diff=3) as month_minus_3_sessions_app	
	from 
				(SELECT g.date,
					 app_platform,
		       SUM(CASE WHEN app_platform = 'web' and g.date<=20160520 THEN sessions - nvl (m_ses,0) else sessions end) AS sessions
		FROM google_analytics.ga_metrics g
		  LEFT JOIN (SELECT DATE,
		                    sessions AS m_ses
		             FROM google_analytics.ga_metrics ga
		             where app_platform = 'mobile' and date>20160209) m ON g.date = m.date
		GROUP BY 1,2) ga, dim_date dt 
	where 
		dt.month_diff<=3 
		and ga.date = dt.full_date 
		and (ga.app_platform not in ('mobile','web') or ga.date!=20160209)
		and ga.date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
		group by 1
	) 
order by platform                                                      