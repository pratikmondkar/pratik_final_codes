SELECT nvl(age_bucket,'0-30') as age_bucket,
       sum(CASE WHEN day_diff = 1 THEN disc end) / NULLIF(sum(CASE WHEN day_diff = 1 THEN mrp end),0)::decimal(12,2) AS yday_disc,
       sum(CASE WHEN month_to_date = 1 THEN disc end) / NULLIF(sum(CASE WHEN month_to_date = 1 THEN mrp end),0)::decimal(12,2) AS mtd_disc,
       sum(CASE WHEN month_diff = 1 THEN disc end) / NULLIF(sum(CASE WHEN month_diff = 1 THEN mrp end),0)::decimal(12,2) AS m1_disc,
       sum(CASE WHEN month_diff = 2 THEN disc end) / NULLIF(sum(CASE WHEN month_diff = 2 THEN mrp end),0)::decimal(12,2) AS m2_disc,
       sum(CASE WHEN month_diff = 3 THEN disc end) / NULLIF(sum(CASE WHEN month_diff = 3 THEN mrp end),0)::decimal(12,2) AS m3_disc
FROM (SELECT order_created_date,
             CASE
                WHEN avg_sku_age BETWEEN 31 AND 60 THEN '030-60'
                WHEN avg_sku_age BETWEEN 61 AND 90 THEN '060-90'
                WHEN avg_sku_age BETWEEN 91 AND 120 THEN '090-120'
                WHEN avg_sku_age BETWEEN 121 AND 150 THEN '120-150'
                WHEN avg_sku_age BETWEEN 151 AND 180 THEN '150-180'
                WHEN avg_sku_age > 180 THEN '180+'
                ELSE '0-30'
              END AS age_bucket,
             day_diff,
             month_to_date,
             month_diff,
             SUM(item_mrp_value*quantity) AS mrp,
             SUM(nvl (effective_discount,0)) AS disc
      FROM fact_core_item fci
        LEFT JOIN dim_date dd ON fci.order_created_date = dd.full_date
      WHERE store_id = 1
      AND   (is_shipped = 1 OR is_realised = 1)
      and order_created_date>=20160215
      AND   month_diff <= 3
      group by 1,2,3,4,5)
group by 1
order by 1