






select  
to_char(yday_installs_app,'99,99,99,99,999') as yday_installs_app,
round((yday_installs_app-yday_minus_1_installs_app)/NULLIF(yday_minus_1_installs_app,0)::float*100,1) as p1,
to_char(mtd_installs_app,'99,99,99,99,999') as mtd_installs_app,
to_char(month_minus_1_installs_app,'99,99,99,99,999') as month_minus_1_installs_app,
to_char(month_minus_2_installs_app,'99,99,99,99,999') as month_minus_2_installs_app,
to_char(month_minus_3_installs_app,'99,99,99,99,999') as month_minus_3_installs_app
from
(
select
		count(case when dt.day_diff = 1 then 1 end)::float as yday_installs_app,
		count(case when dt.day_diff = 2 then 1 end)::float as yday_minus_1_installs_app,
		count(case when dt.month_to_date = 1 then 1 end) as mtd_installs_app,
		count(case when dt.month_diff = 1 then 1 end) as month_minus_1_installs_app,
		count(case when dt.month_diff = 2 then 1 end) as month_minus_2_installs_app,
		count(case when dt.month_diff = 3 then 1 end) as month_minus_3_installs_app	
	from 
		fact_app_install_from_apsalar ins, dim_date dt 
	where 
		dt.month_diff<=3 
		and ins.load_date = dt.full_date 
--		and ins.date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
	)                                          