select * from
(
select  traffic.Platform  , 
(yday_orders_app/yday_sessions_app::float) as yday_conv,
((yday_orders_app/yday_sessions_app::float)-(yday_minus_1_orders_app/yday_minus_1_sessions_app::float))*100 as p1,
(mtd_orders_app/mtd_sessions_app::float)  as mtd_conv,
(month_minus_1_orders_app/month_minus_1_sessions_app::float) as month_minus_1_conv,
(month_minus_2_orders_app/month_minus_2_sessions_app::float) as month_minus_2_conv,
(month_minus_3_orders_app/month_minus_3_sessions_app::float) as month_minus_3_conv
from
(
select  case when app_platform   = 'windows' then 'Windows' 
	when app_platform ='android' then 'Android'  
	when app_platform ='iOS' then 'iOS' 
	when app_platform = 'mobile' then 'M-Site'
	when app_platform = 'web' then 'Desktop'end as Platform,
		sum(case when dt.day_diff = 1   then sessions end)::float as yday_sessions_app,
		sum(case when dt.day_diff = 2    then sessions end) as yday_minus_1_sessions_app,
		sum(case when dt.month_to_date = 1   then sessions end) as mtd_sessions_app,
		sum(case when dt.month_diff = 1   then sessions end) as month_minus_1_sessions_app,
		sum(case when dt.month_diff = 2   then sessions end) as month_minus_2_sessions_app,
		sum(case when dt.month_diff = 3   then sessions end) as month_minus_3_sessions_app	
	from 
		google_analytics.ga_metrics ga, dim_date dt 
	where 
		dt.month_diff<=3 
		and ga.date = dt.full_date 
		and ga.date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
		group by 1
	) traffic 
	join
	(
select  case when device_channel = 'mobile-app' and browser_info = 'Android' then 'Android' 
		when device_channel = 'mobile-app' and browser_info = 'iOS' then 'iOS' 
		when device_channel = 'mobile-app' and browser_info = 'Windows' then 'Windows' 
		when device_channel = 'mobile-web' then 'M-Site'
		when device_channel = 'web' then 'Desktop' end as platform,
		count(distinct  case when dt.day_diff = 1   then  order_group_id end)::float as yday_orders_app,
		count(distinct  case when dt.day_diff = 2    then  order_group_id end) as yday_minus_1_orders_app,
		count(distinct  case when dt.month_to_date = 1   then  order_group_id end) as mtd_orders_app,
		count(distinct  case when dt.month_diff = 1   then  order_group_id end) as month_minus_1_orders_app,
		count(distinct  case when dt.month_diff = 2   then  order_group_id end) as month_minus_2_orders_app,
		count(distinct  case when dt.month_diff = 3   then  order_group_id end) as month_minus_3_orders_app	
	from 
		fact_core_item fo, dim_date dt 
	where 
		dt.month_diff<=3 
		and store_id=1
		and fo.order_created_date = dt.full_date 
		and fo.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
		and device_channel in ('mobile-app', 'mobile-web','web')
		group by 1
		) orders
			on traffic.platform = orders.platform
	)		
			
			order by
			case when platform = 'Android' then 1
			when platform = 'iOS' then 2
			when platform = 'Windows' then 3
			when platform = 'M-Site' then 4 
			when platform = 'Desktop' then 5
			end
			asc
			
			                                                      