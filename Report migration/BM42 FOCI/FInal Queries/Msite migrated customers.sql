/*BM42 Revenue*/
select
	a.yday_revenue_contri_app_cus,
	(a.yday_revenue_contri_app_cus - a.yday_minus_1_revenue_contri_app_cus)*100 as p1, 
	a.mtd_revenue_contri_app_cus  , 
	a.month_minus_1_revenue_contri_app_cus  ,
	a.month_minus_2_revenue_contri_app_cus  , 
	a.month_minus_3_revenue_contri_app_cus  ,
	a.month_minus_1_revenue_contri_app_cus  , 
	a.month_minus_2_revenue_contri_app_cus  , 
	a.month_minus_3_revenue_contri_app_cus ,

	a.yday_revenue_contri_no_app_cus,
	(a.yday_revenue_contri_no_app_cus - a.yday_minus_1_revenue_contri_no_app_cus)*100 as p2, 
	a.mtd_revenue_contri_no_app_cus  , 
	a.month_minus_1_revenue_contri_no_app_cus  ,
	a.month_minus_2_revenue_contri_no_app_cus  , 
	a.month_minus_3_revenue_contri_no_app_cus  ,
	a.month_minus_1_revenue_contri_no_app_cus  , 
	a.month_minus_2_revenue_contri_no_app_cus  , 
	a.month_minus_3_revenue_contri_no_app_cus  	
	from 	
	(
	select 
		sum(case when dt.day_diff = 1 and app_flag = 1 and purchase_sequence >1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.day_diff = 1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as yday_revenue_contri_app_cus,
		sum(case when dt.day_diff = 2 and app_flag = 1 and purchase_sequence >1  then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.day_diff = 2 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as yday_minus_1_revenue_contri_app_cus,
		sum(case when dt.month_to_date = 1 and app_flag = 1 and purchase_sequence >1  then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.month_to_date = 1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as mtd_revenue_contri_app_cus,
		sum(case when dt.month_diff = 1 and app_flag = 1 and purchase_sequence >1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.month_diff = 1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as month_minus_1_revenue_contri_app_cus,	
		sum(case when dt.month_diff = 2 and app_flag = 1 and purchase_sequence >1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.month_diff = 2 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as month_minus_2_revenue_contri_app_cus,
		sum(case when dt.month_diff = 3 and app_flag = 1 and purchase_sequence >1  then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.month_diff = 3 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as month_minus_3_revenue_contri_app_cus,

		sum(case when dt.day_diff = 1 and app_flag = 0 and purchase_sequence >1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.day_diff = 1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as yday_revenue_contri_no_app_cus,
		sum(case when dt.day_diff = 2 and app_flag = 0 and purchase_sequence >1  then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.day_diff = 2 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as yday_minus_1_revenue_contri_no_app_cus,
		sum(case when dt.month_to_date = 1 and app_flag = 0 and purchase_sequence >1   then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.month_to_date = 1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as mtd_revenue_contri_no_app_cus,
		sum(case when dt.month_diff = 1 and app_flag = 0 and purchase_sequence >1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.month_diff = 1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as month_minus_1_revenue_contri_no_app_cus,	
		sum(case when dt.month_diff = 2 and app_flag = 0 and purchase_sequence >1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.month_diff = 2 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as month_minus_2_revenue_contri_no_app_cus,
		sum(case when dt.month_diff = 3 and app_flag = 0 and purchase_sequence >1  then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.month_diff = 3 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as month_minus_3_revenue_contri_no_app_cus	
		
		from 
		fact_order o join dim_date dt on o.order_created_date = dt.full_date 
		join
		(
		select idcustomer , case when app_flag >0 then 1 else 0 end as app_flag
		from
		(
		select idcustomer, sum(app_flag) as app_flag 
		from
		(
		select distinct idcustomer , case when order_channel = 'mobile-app' and is_booked = 1 then 1 else 0 end as app_flag
		from fact_order 	
		) 
		group by 1
		)
		order by app_flag 
		)c
		on o.idcustomer = c.idcustomer
		where 
		dt.month_diff<=3
		and store_id=1
		and (o.is_shipped = 1 or o.is_realised=1) 
		and order_channel = 'mobile-web'
		and order_Created_date>= 20160216
		) a