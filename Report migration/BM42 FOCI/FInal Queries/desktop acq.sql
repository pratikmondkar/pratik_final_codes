/*BM42 Revenue*/
select
	a.yday_acq,
	trunc((a.yday_acq - a.yday_minus_1_acq)*100/nullif(a.yday_minus_1_acq,0),1) as p1, 
	a.mtd_acq , 
	a.month_minus_1_acq  ,
	a.month_minus_2_acq  , 
	a.month_minus_3_acq  ,
	a.month_minus_1_acq  , 
	a.month_minus_2_acq  , 
	a.month_minus_3_acq  
	from 	
	(
	select 
		count(distinct case when dt.day_diff = 1 and purchase_sequence = 1 then fci.idcustomer end) as yday_acq,
		count(distinct case when dt.day_diff = 2 and purchase_sequence = 1  then fci.idcustomer end) as yday_minus_1_acq,
		count(distinct case when dt.month_to_date = 1 and purchase_sequence = 1  then fci.idcustomer end) as mtd_acq,
		count(distinct case when dt.month_diff = 1 and purchase_sequence = 1 then fci.idcustomer end) as month_minus_1_acq,	
		count(distinct case when dt.month_diff = 2 and purchase_sequence = 1 then fci.idcustomer end) as month_minus_2_acq,
		count(distinct case when dt.month_diff = 3 and purchase_sequence = 1  then fci.idcustomer end) as month_minus_3_acq

	from 
		fact_core_item fci 
		join fact_order o on fci.order_id=o.order_id 
		join dim_date dt on fci.order_created_date = dt.full_date 
		where 
		dt.month_diff<=3
		and fci.store_id=1
		and (fci.is_shipped = 1 or fci.is_realised=1) 
		and device_channel = 'web'
		and fci.order_Created_date>= 20160527
		) a