SELECT Platform,
       (yday_sessions_app / yday_sessions_app_tot)::float AS yday_sessions_app_perc,
       ((yday_sessions_app / yday_sessions_app_tot::float) -(yday_minus_1_sessions_app / yday_minus_1_sessions_app_tot::float))::float *100 AS p1,
       (mtd_sessions_app / mtd_sessions_app_tot::float) AS mtd_sessions_app_perc,
       (month_minus_1_sessions_app / month_minus_1_sessions_app_tot)::float AS month_minus_1_sessions_app_perc,
       (month_minus_2_sessions_app / month_minus_2_sessions_app_tot)::float AS month_minus_2_sessions_app_perc,
       (month_minus_3_sessions_app / month_minus_3_sessions_app_tot)::float AS month_minus_3_sessions_app_perc
FROM (SELECT CASE
               WHEN app_platform = 'windows' THEN '-Windows'
               WHEN app_platform = 'android' THEN '-Android'
               WHEN app_platform = 'iOS' THEN '-iOS'
               when app_platform = 'web' THEN '-Desktop'
               when app_platform = 'mobile' THEN '-Msite'
             END AS Platform,
             1 AS flag,
             SUM(CASE WHEN dt.day_diff = 1 THEN sessions END)::FLOAT AS yday_sessions_app,
             SUM(CASE WHEN dt.day_diff = 2 THEN sessions END) AS yday_minus_1_sessions_app,
             SUM(CASE WHEN dt.month_to_date = 1 THEN sessions END)::FLOAT AS mtd_sessions_app,
             SUM(CASE WHEN dt.month_diff = 1 THEN sessions END)::FLOAT AS month_minus_1_sessions_app,
             SUM(CASE WHEN dt.month_diff = 2 THEN sessions END)::FLOAT AS month_minus_2_sessions_app,
             SUM(CASE WHEN dt.month_diff = 3 THEN sessions END)::FLOAT AS month_minus_3_sessions_app
      FROM google_analytics.ga_metrics ga,
           bidb.dim_date dt
      WHERE dt.month_diff <= 3
      AND   ga.date = dt.full_date
      and 	(ga.app_platform <>'web' or ga.date>20160527)
      AND   ga.date >= TO_CHAR(dateadd (MONTH,-3,convert_timezone ('Asia/Calcutta',getdate ())),'YYYYMM01')
      GROUP BY 1,
               2) a
  JOIN (SELECT 1 AS flag,
               SUM(CASE WHEN dt.day_diff = 1 THEN sessions END)::FLOAT AS yday_sessions_app_tot,
               SUM(CASE WHEN dt.day_diff = 2 THEN sessions END) AS yday_minus_1_sessions_app_tot,
               SUM(CASE WHEN dt.month_to_date = 1 THEN sessions END) AS mtd_sessions_app_tot,
               SUM(CASE WHEN dt.month_diff = 1 THEN sessions END) AS month_minus_1_sessions_app_tot,
               SUM(CASE WHEN dt.month_diff = 2 THEN sessions END) AS month_minus_2_sessions_app_tot,
               SUM(CASE WHEN dt.month_diff = 3 THEN sessions END) AS month_minus_3_sessions_app_tot
        FROM google_analytics.ga_metrics ga,
             bidb.dim_date dt
        WHERE dt.month_diff <= 3
        AND   ga.date = dt.full_date
        and 	(ga.app_platform <>'web' or ga.date>20160527)
        AND   ga.date >= TO_CHAR(dateadd (MONTH,-3,convert_timezone ('Asia/Calcutta',getdate ())),'YYYYMM01')
        GROUP BY 1) b ON a.flag = b.flag
WHERE platform IS NOT NULL
order by yday_sessions_app_perc desc