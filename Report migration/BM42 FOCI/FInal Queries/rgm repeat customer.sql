/*BM42 RGM Repeat Customers*/
SELECT
	to_char(f.yday_rgm,'99,99,99,99,999.9')  AS yday_rgm,
	TRUNC((f.yday_rgm - f.yday_minus_1_rgm)*100/f.yday_minus_1_rgm,1) as p1,
	f.mtd_rgm, f.month_minus_1_rgm, f.month_minus_2_rgm, f.month_minus_3_rgm
FROM
	(SELECT 
		SUM(CASE WHEN d.day_diff=1 THEN d.rgm END)*100/SUM(CASE WHEN d.day_diff=1 THEN d.revenue END) AS yday_rgm,
		SUM(CASE WHEN d.day_diff=2 THEN d.rgm END)*100/SUM(CASE WHEN d.day_diff=2 THEN d.revenue END) AS yday_minus_1_rgm,
		SUM(CASE WHEN d.month_to_date=1 THEN d.rgm END)/SUM(CASE WHEN d.month_to_date=1 THEN d.revenue END) AS mtd_rgm,
		SUM(CASE WHEN d.month_diff=1 THEN d.rgm END)/SUM(CASE WHEN d.month_diff=1 THEN d.revenue END) AS month_minus_1_rgm,
		SUM(CASE WHEN d.month_diff=2 THEN d.rgm END)/SUM(CASE WHEN d.month_diff=2 THEN d.revenue END) AS month_minus_2_rgm,
		SUM(CASE WHEN d.month_diff=3 THEN d.rgm END)/SUM(CASE WHEN d.month_diff=3 THEN d.revenue END) AS month_minus_3_rgm
	FROM	
		(SELECT 
			a.order_created_date, aa.day_diff, aa.month_to_date, aa.month_diff, 
			(a.revenue-b.net_cost_price) AS rgm, a.revenue 
		FROM 
			(SELECT 
				o.order_created_date,
				SUM(o.shipped_order_revenue_inc_cashback)-SUM(o.shipped_tax) AS revenue 
			FROM fact_order o, dim_date dt 
			WHERE dt.month_diff<=3 AND 
			o.order_created_date = dt.full_date AND o.purchase_type='r' 
			AND (o.is_shipped=1 OR o.is_realised=1) and store_id=1
and o.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
			GROUP BY o.order_created_date) a
			join
			(select  day_diff, month_to_date, month_diff,full_date  from dim_date where month_diff<=3) aa 
			on a.order_created_date=aa.full_date
			LEFT JOIN
			(SELECT 
				o.order_created_date,  
				SUM(nvl(cogs,0)+nvl(royalty_commission,0)+nvl(stn_input_vat_reversal,0)+ nvl(entry_tax,0)) AS net_cost_price 
			FROM fact_core_item oi, fact_order o, dim_product p
			WHERE oi.order_id=o.order_id and oi.order_created_date>=to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01') AND 
			oi.sku_id = p.sku_id AND o.purchase_type='r' and o.store_id=1
			AND (oi.is_shipped=1 OR oi.is_realised=1)
			GROUP BY 1) b ON a.order_created_date = b.order_created_date) d 
	) f 