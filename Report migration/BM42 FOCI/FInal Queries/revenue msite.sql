/*BM42 Revenue*/
select
	(to_char(a.yday_revenue,'99,99,99,99,999')) as yday_rev,
	trunc((a.yday_revenue - a.yday_minus_1_revenue)*100/nullif(a.yday_minus_1_revenue,0),1) as p1, 

	(to_char(a.yday_order,'99,99,99,99,999')) as yday_order,
	trunc((a.yday_order - a.yday_minus_1_order)*100/nullif(a.yday_minus_1_order,0),1) as p2,
	(to_char(a.yday_revenue/a.yday_order,'99,99,99,99,999')) as yday_asp,
	trunc((a.yday_revenue/nullif(a.yday_order,0) - a.yday_minus_1_revenue/nullif(a.yday_minus_1_order,0))*100/(a.yday_minus_1_revenue/nullif(a.yday_minus_1_order,0)),1) as p3,
	
	to_char(a.mtd_revenue,'99,99,99,99,999') as mtd_revenue, 
	to_char(a.month_minus_1_revenue,'99,99,99,99,999') as month_minus_1_revenue, 
	to_char(a.month_minus_2_revenue,'99,99,99,99,999') as month_minus_2_revenue, 
	to_char(a.month_minus_3_revenue,'99,99,99,99,999') as month_minus_3_revenue,
	
	to_char(a.yday_item_count,'99,99,99,99,999') as yday_item_count, 
	to_char(a.mtd_item_count,'99,99,99,99,999') as mtd_item_count, 
	to_char(a.month_minus_1_item_count,'99,99,99,99,999') as month_minus_1_item_count, 
	to_char(a.month_minus_2_item_count,'99,99,99,99,999') as month_minus_2_item_count,
	to_char(a.month_minus_3_item_count,'99,99,99,99,999') as month_minus_3_item_count,	
	trunc((a.yday_item_count - a.yday_minus_1_item_count)*100/nullif(a.yday_minus_1_item_count,0),1) as p6, 
	
	to_char(a.month_minus_1_revenue,'99,99,99,99,999') as month_minus_1_revenue, 
	to_char(a.month_minus_2_revenue,'99,99,99,99,999') as month_minus_2_revenue, 
	to_char(a.month_minus_3_revenue,'99,99,99,99,999') as month_minus_3_revenue,
	
	a.mtd_order, 
	a.month_minus_1_order,
    month_minus_2_order,
    month_minus_3_order,
	a.mtd_revenue/nullif(a.mtd_order,0) as mtd_asp, 
	a.month_minus_1_revenue/nullif(a.month_minus_1_order,0) as month_minus_1_asp, 
	a.month_minus_2_revenue/nullif(month_minus_2_order,0) as month_minus_2_asp, 
	a.month_minus_3_revenue/nullif(month_minus_3_order,0) as month_minus_3_asp
from 	
	(
	select 
		sum(case when dt.day_diff = 1 then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as yday_revenue,
		count(distinct (case when dt.day_diff = 1 then o.order_group_id end)) as yday_order,
		sum(case when dt.day_diff = 1 then o.quantity end) as yday_item_count,
	
		sum(case when dt.day_diff = 2 then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as yday_minus_1_revenue,
		count(distinct (case when dt.day_diff = 2 then o.order_group_id end)) as yday_minus_1_order,
		sum(case when dt.day_diff = 2 then o.quantity end) as yday_minus_1_item_count,
	
		
		sum(case when dt.month_to_date = 1 then o.quantity end) as mtd_item_count,
		sum(case when dt.month_diff = 1 then o.quantity end) as month_minus_1_item_count,
		sum(case when dt.month_diff = 2 then o.quantity end) as month_minus_2_item_count,
		sum(case when dt.month_diff = 3 then o.quantity end) as month_minus_3_item_count,
			
		sum(case when dt.month_to_date = 1 then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as mtd_revenue,
		sum(case when dt.month_diff = 1 then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_1_revenue,
		sum(case when dt.month_diff = 2 then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_2_revenue,
		sum(case when dt.month_diff = 3 then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_3_revenue,

	
		count(distinct (case when dt.month_to_date = 1 then o.order_group_id end)) as mtd_order,
		count(distinct (case when dt.month_diff = 1 then o.order_group_id end)) as month_minus_1_order,
		count(distinct (case when dt.month_diff = 2 then o.order_group_id end)) as month_minus_2_order,
		count(distinct (case when dt.month_diff = 3 then o.order_group_id end)) as month_minus_3_order
	
	from 
		fact_core_item o join dim_date dt on order_created_date = dt.full_date 
		where 
		dt.month_diff<=3
		and store_id=1 and device_channel = 'mobile-web' and order_created_date >= 20160215
		and (o.is_shipped = 1 or o.is_realised=1) 
		) a