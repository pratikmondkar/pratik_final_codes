/*BM42 RGM Overall*/
SELECT 
	to_char(e.yday_rgm,'99,99,99,99,999') AS yday_rgm,
	trunc((e.yday_rgm - e.yday_minus_1_rgm)/e.yday_minus_1_rgm,1) as p1,
	e.mtd_rgm, e.month_minus_1_rgm, e.month_minus_2_rgm, e.month_minus_3_rgm,

	to_char(e.yday_rgm_percent,'99,99,99,99,999.9') || '%' as yday_rgm_percent,
    trunc((e.yday_rgm_percent - e.yday_minus_1_rgm_percent)*100/e.yday_minus_1_rgm_percent,1) AS p2,
	e.mtd_rgm_percent, e.month_minus_1_rgm_percent, e.month_minus_2_rgm_percent, e.month_minus_3_rgm_percent
	FROM
	(SELECT 
		SUM(CASE WHEN d.day_diff=1 THEN d.rgm END) AS yday_rgm,
		SUM(CASE WHEN d.day_diff=2 THEN d.rgm END) AS yday_minus_1_rgm,
		to_char(SUM(CASE WHEN d.month_to_date=1 THEN d.rgm END),'99,99,99,99,999') AS mtd_rgm,
		to_char(SUM(CASE WHEN d.month_diff=1 THEN d.rgm END),'99,99,99,99,999') AS month_minus_1_rgm,
		to_char(SUM(CASE WHEN d.month_diff=2 THEN d.rgm END),'99,99,99,99,999') AS month_minus_2_rgm,
		to_char(SUM(CASE WHEN d.month_diff=3 THEN d.rgm END),'99,99,99,99,999') AS month_minus_3_rgm,

		SUM(CASE WHEN d.day_diff=1 THEN d.rgm END)*100/SUM(CASE WHEN d.day_diff=1 THEN d.revenue END) AS yday_rgm_percent,
		SUM(CASE WHEN d.day_diff=2 THEN d.rgm END)*100/SUM(CASE WHEN d.day_diff=2 THEN d.revenue END) AS yday_minus_1_rgm_percent,
		SUM(CASE WHEN d.month_to_date=1 THEN d.rgm END)/SUM(CASE WHEN d.month_to_date=1 THEN d.revenue END) AS mtd_rgm_percent,
		SUM(CASE WHEN d.month_diff=1 THEN d.rgm END)/SUM(CASE WHEN d.month_diff=1 THEN d.revenue END) AS month_minus_1_rgm_percent,
		SUM(CASE WHEN d.month_diff=2 THEN d.rgm END)/SUM(CASE WHEN d.month_diff=2 THEN d.revenue END) AS month_minus_2_rgm_percent,
		SUM(CASE WHEN d.month_diff=3 THEN d.rgm END)/SUM(CASE WHEN d.month_diff=3 THEN d.revenue END) AS month_minus_3_rgm_percent
	FROM	
		(SELECT 
			b.order_created_date, aa.day_diff, aa.month_to_date, aa.month_diff, aa.last7_days, 
			
			(b.revenue+b.other_charges-b.net_cost_price) AS rgm,
			nvl(b.revenue+b.other_charges,0) AS revenue			
			FROM 
			
			(SELECT 
				order_created_date,
				sum(nvl(oi.item_revenue_inc_cashback,0)-nvl(oi.tax,0)) as revenue,
				SUM(nvl(cogs,0)+nvl(royalty_commission,0)+nvl(stn_input_vat_reversal,0)+ nvl(entry_tax,0)) as net_cost_price,
				sum(nvl(oi.shipping_charges,0)+nvl(oi.cod_charges,0)+nvl(oi.gift_charges,0)+nvl(oi.emi_charges,0)) as other_charges
					 		 
			FROM 
			fact_core_item oi join dim_product p  on oi.sku_id = p.sku_id 
			WHERE oi.order_created_date>=to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01') 
			AND (oi.is_shipped=1 OR oi.is_realised=1) and store_id=1 and device_channel = 'mobile-web' and order_created_date >= 20160215
			GROUP BY 1) b 
			JOIN
			(select full_date, day_diff, month_to_date, month_diff, last7_days
			from dim_date where month_diff<=3) aa on b.order_created_date=aa.full_date
			) d
) e