/*desktop acq contibution*/
select
	a.yday_revenue_contri,
	trunc((a.yday_revenue_contri - a.yday_minus_1_revenue_contri)*100/nullif(a.yday_minus_1_revenue_contri,0),1) as p1, 
	a.mtd_revenue_contri, 
	a.month_minus_1_revenue_contri  ,
	a.month_minus_2_revenue_contri  , 
	a.month_minus_3_revenue_contri  ,
	a.month_minus_1_revenue_contri  , 
	a.month_minus_2_revenue_contri  , 
	a.month_minus_3_revenue_contri  
	from 	
	(
	select 
		sum( case when dt.day_diff = 1 and purchase_sequence = 1 then nvl(item_revenue_inc_cashback,0)+nvl(fci.shipping_charges,0)+nvl(fci.gift_charges,0) end)/sum(case when dt.day_diff = 1 then nvl(item_revenue_inc_cashback,0)+nvl(fci.shipping_charges,0)+nvl(fci.gift_charges,0) end)::float as yday_revenue_contri,
		sum( case when dt.day_diff = 2 and purchase_sequence = 1  then nvl(item_revenue_inc_cashback,0)+nvl(fci.shipping_charges,0)+nvl(fci.gift_charges,0) end)/sum(case when dt.day_diff = 2 then nvl(item_revenue_inc_cashback,0)+nvl(fci.shipping_charges,0)+nvl(fci.gift_charges,0) end)::float as yday_minus_1_revenue_contri,
		sum( case when dt.month_to_date = 1 and purchase_sequence = 1  then nvl(item_revenue_inc_cashback,0)+nvl(fci.shipping_charges,0)+nvl(fci.gift_charges,0) end)/sum(case when dt.month_to_date = 1 then nvl(item_revenue_inc_cashback,0)+nvl(fci.shipping_charges,0)+nvl(fci.gift_charges,0) end)::float as mtd_revenue_contri,
		sum( case when dt.month_diff = 1 and purchase_sequence = 1 then nvl(item_revenue_inc_cashback,0)+nvl(fci.shipping_charges,0)+nvl(fci.gift_charges,0) end)/sum(case when dt.month_diff = 1 then nvl(item_revenue_inc_cashback,0)+nvl(fci.shipping_charges,0)+nvl(fci.gift_charges,0) end)::float as month_minus_1_revenue_contri,	
		sum( case when dt.month_diff = 2 and purchase_sequence = 1 then nvl(item_revenue_inc_cashback,0)+nvl(fci.shipping_charges,0)+nvl(fci.gift_charges,0) end)/sum(case when dt.month_diff = 2 then nvl(item_revenue_inc_cashback,0)+nvl(fci.shipping_charges,0)+nvl(fci.gift_charges,0) end)::float as month_minus_2_revenue_contri,
		sum( case when dt.month_diff = 3 and purchase_sequence = 1  then nvl(item_revenue_inc_cashback,0)+nvl(fci.shipping_charges,0)+nvl(fci.gift_charges,0) end)/sum(case when dt.month_diff = 3 then nvl(item_revenue_inc_cashback,0)+nvl(fci.shipping_charges,0)+nvl(fci.gift_charges,0) end)::float as month_minus_3_revenue_contri

	from 
		fact_core_item fci 
		join fact_order o on fci.order_id=o.order_id 
		join dim_date dt on fci.order_created_date = dt.full_date 
		where 
		dt.month_diff<=3
		and fci.store_id=1
		and (fci.is_shipped = 1 or fci.is_realised=1) 
		and fci.device_channel = 'web'
		and fci.order_Created_date>= 20160527
		) a