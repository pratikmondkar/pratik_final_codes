/*BM42 Revenue at OI level (MrktPlc+Private)*/
select
	a.yday_revenue/yday_tot_revenue as yday_rev,
	(a.yday_revenue/yday_tot_revenue) - (yday_minus_1_revenue/yday_minus_1_tot_revenue)/(a.yday_revenue/yday_tot_revenue) as p1,
	a.mtd_revenue/mtd_tot_revenue as mtd_revenue, 
	a.month_minus_1_revenue/month_minus_1_tot_revenue as month_minus_1_revenue, 
	a.month_minus_2_revenue/month_minus_2_tot_revenue as month_minus_2_revenue, 
	a.month_minus_3_revenue/month_minus_3_tot_revenue as month_minus_3_revenue,
	
	a.yday_Mrktrevenue/yday_tot_revenue  as yday_mrev,
	(a.yday_Mrktrevenue/yday_tot_revenue) - (yday_minus_1_Mrktrevenue/yday_minus_1_tot_revenue)/(a.yday_Mrktrevenue/yday_tot_revenue) as p2,
	a.mtd_Mrktrevenue/mtd_tot_revenue as mtd_Mrevenue, 
	a.month_minus_1_Mrktrevenue/month_minus_1_tot_revenue as month_minus_1_Mrevenue, 
	a.month_minus_2_Mrktrevenue/month_minus_2_tot_revenue as month_minus_2_Mrevenue, 
	a.month_minus_3_Mrktrevenue/month_minus_3_tot_revenue as month_minus_3_Mrevenue
from 	
	(select 
		sum(case when dt.day_diff = 1 and brand_type = 'Private' then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as yday_revenue,
		sum(case when dt.day_diff = 2 and brand_type = 'Private' then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as yday_minus_1_revenue,
		sum(case when dt.month_to_date = 1 and brand_type = 'Private' then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as mtd_revenue,
		sum(case when dt.month_diff = 1 and brand_type = 'Private' then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_1_revenue,
		sum(case when dt.month_diff = 2 and brand_type = 'Private' then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_2_revenue,
		sum(case when dt.month_diff = 3 and brand_type = 'Private' then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_3_revenue,
		
		sum(case when dt.day_diff = 1 and o.supply_type='JUST_IN_TIME' then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as yday_Mrktrevenue,
		sum(case when dt.day_diff = 2 and o.supply_type='JUST_IN_TIME' then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as yday_minus_1_Mrktrevenue,
		sum(case when dt.month_to_date = 1 and o.supply_type='JUST_IN_TIME' then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as mtd_Mrktrevenue,
		sum(case when dt.month_diff = 1 and o.supply_type='JUST_IN_TIME' then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_1_Mrktrevenue,
		sum(case when dt.month_diff = 2 and o.supply_type='JUST_IN_TIME' then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_2_Mrktrevenue,
		sum(case when dt.month_diff = 3 and o.supply_type='JUST_IN_TIME' then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_3_Mrktrevenue,
		
		sum(case when dt.day_diff = 1 then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as yday_tot_revenue,
		sum(case when dt.day_diff = 2 then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as yday_minus_1_tot_revenue,
		sum(case when dt.month_to_date = 1 then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as mtd_tot_revenue,
		sum(case when dt.month_diff = 1 then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_1_tot_revenue,
		sum(case when dt.month_diff = 2 then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_2_tot_revenue,
		sum(case when dt.month_diff = 3 then o.item_revenue_inc_cashback+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_3_tot_revenue
		
	
	from 
		fact_core_item o, dim_date dt
	where 
		dt.month_diff<=3 and o.order_created_date = dt.full_date
		and (o.is_shipped = 1 or o.is_realised=1) and store_id=1
and o.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')) a