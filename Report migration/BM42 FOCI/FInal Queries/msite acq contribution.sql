/*BM42 Revenue*/
select
	a.yday_revenue_contri,
	trunc((a.yday_revenue_contri - a.yday_minus_1_revenue_contri)*100/nullif(a.yday_minus_1_revenue_contri,0),1) as p1, 
	a.mtd_revenue_contri, 
	a.month_minus_1_revenue_contri  ,
	a.month_minus_2_revenue_contri  , 
	a.month_minus_3_revenue_contri  ,
	a.month_minus_1_revenue_contri  , 
	a.month_minus_2_revenue_contri  , 
	a.month_minus_3_revenue_contri  
	from 	
	(
	select 
		sum(case when dt.day_diff = 1 and purchase_sequence = 1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.day_diff = 1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as yday_revenue_contri,
		sum(case when dt.day_diff = 2 and purchase_sequence = 1  then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.day_diff = 2 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as yday_minus_1_revenue_contri,
		sum(case when dt.month_to_date = 1 and purchase_sequence = 1  then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.month_to_date = 1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as mtd_revenue_contri,
		sum(case when dt.month_diff = 1 and purchase_sequence = 1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.month_diff = 1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as month_minus_1_revenue_contri,	
		sum(case when dt.month_diff = 2 and purchase_sequence = 1 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.month_diff = 2 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as month_minus_2_revenue_contri,
		sum(case when dt.month_diff = 3 and purchase_sequence = 1  then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)/sum(case when dt.month_diff = 3 then nvl(order_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as month_minus_3_revenue_contri

	from 
		fact_order o join dim_date dt on order_created_date = dt.full_date 
		where 
		dt.month_diff<=3
		and store_id=1
		and (o.is_shipped = 1 or o.is_realised=1) 
		and order_channel = 'mobile-web'
		and order_Created_date>= 20160216
		) a