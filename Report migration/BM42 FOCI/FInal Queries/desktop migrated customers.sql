/*BM42 Revenue*/ 
SELECT a.yday_revenue_contri_app_cus,
       (a.yday_revenue_contri_app_cus - a.yday_minus_1_revenue_contri_app_cus)*100 AS p1,
       a.mtd_revenue_contri_app_cus,
       a.month_minus_1_revenue_contri_app_cus,
       a.month_minus_2_revenue_contri_app_cus,
       a.month_minus_3_revenue_contri_app_cus,
       a.month_minus_1_revenue_contri_app_cus,
       a.month_minus_2_revenue_contri_app_cus,
       a.month_minus_3_revenue_contri_app_cus,
       a.yday_revenue_contri_no_app_cus,
       (a.yday_revenue_contri_no_app_cus - a.yday_minus_1_revenue_contri_no_app_cus)*100 AS p2,
       a.mtd_revenue_contri_no_app_cus,
       a.month_minus_1_revenue_contri_no_app_cus,
       a.month_minus_2_revenue_contri_no_app_cus,
       a.month_minus_3_revenue_contri_no_app_cus,
       a.month_minus_1_revenue_contri_no_app_cus,
       a.month_minus_2_revenue_contri_no_app_cus,
       a.month_minus_3_revenue_contri_no_app_cus
FROM (SELECT SUM(CASE WHEN dt.day_diff = 1 AND app_flag = 1 AND f.purchase_sequence > 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END) / SUM(CASE WHEN dt.day_diff = 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END)::FLOAT AS yday_revenue_contri_app_cus,
             SUM(CASE WHEN dt.day_diff = 2 AND app_flag = 1 AND f.purchase_sequence > 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END) / SUM(CASE WHEN dt.day_diff = 2 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END)::FLOAT AS yday_minus_1_revenue_contri_app_cus,
             SUM(CASE WHEN dt.month_to_date = 1 AND app_flag = 1 AND f.purchase_sequence > 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END) / SUM(CASE WHEN dt.month_to_date = 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END)::FLOAT AS mtd_revenue_contri_app_cus,
             SUM(CASE WHEN dt.month_diff = 1 AND app_flag = 1 AND f.purchase_sequence > 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END) / SUM(CASE WHEN dt.month_diff = 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END)::FLOAT AS month_minus_1_revenue_contri_app_cus,
             SUM(CASE WHEN dt.month_diff = 2 AND app_flag = 1 AND f.purchase_sequence > 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END) / SUM(CASE WHEN dt.month_diff = 2 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END)::FLOAT AS month_minus_2_revenue_contri_app_cus,
             SUM(CASE WHEN dt.month_diff = 3 AND app_flag = 1 AND f.purchase_sequence > 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END) / SUM(CASE WHEN dt.month_diff = 3 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END)::FLOAT AS month_minus_3_revenue_contri_app_cus,
             SUM(CASE WHEN dt.day_diff = 1 AND app_flag = 0 AND f.purchase_sequence > 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END) / SUM(CASE WHEN dt.day_diff = 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END)::FLOAT AS yday_revenue_contri_no_app_cus,
             SUM(CASE WHEN dt.day_diff = 2 AND app_flag = 0 AND f.purchase_sequence > 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END) / SUM(CASE WHEN dt.day_diff = 2 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END)::FLOAT AS yday_minus_1_revenue_contri_no_app_cus,
             SUM(CASE WHEN dt.month_to_date = 1 AND app_flag = 0 AND f.purchase_sequence > 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END) / SUM(CASE WHEN dt.month_to_date = 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END)::FLOAT AS mtd_revenue_contri_no_app_cus,
             SUM(CASE WHEN dt.month_diff = 1 AND app_flag = 0 AND f.purchase_sequence > 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END) / SUM(CASE WHEN dt.month_diff = 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END)::FLOAT AS month_minus_1_revenue_contri_no_app_cus,
             SUM(CASE WHEN dt.month_diff = 2 AND app_flag = 0 AND f.purchase_sequence > 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END) / SUM(CASE WHEN dt.month_diff = 2 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END)::FLOAT AS month_minus_2_revenue_contri_no_app_cus,
             SUM(CASE WHEN dt.month_diff = 3 AND app_flag = 0 AND f.purchase_sequence > 1 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END) / SUM(CASE WHEN dt.month_diff = 3 THEN nvl (item_revenue_inc_cashback,0) + nvl (o.shipping_charges,0) + nvl (o.gift_charges,0) END)::FLOAT AS month_minus_3_revenue_contri_no_app_cus
      FROM fact_core_item o
        JOIN dim_date dt ON o.order_created_date = dt.full_date
        join fact_order f on o.order_id=f.order_id
        JOIN (SELECT idcustomer,
                     CASE
                       WHEN app_flag > 0 THEN 1
                       ELSE 0
                     END AS app_flag
              FROM (SELECT idcustomer,
                           SUM(app_flag) AS app_flag
                    FROM (SELECT DISTINCT idcustomer,
                                 CASE
                                   WHEN device_channel = 'mobile-app' AND is_booked = 1 THEN 1
                                   ELSE 0
                                 END AS app_flag
                          FROM fact_core_item)
                    GROUP BY 1)
              ORDER BY app_flag) c ON o.idcustomer = c.idcustomer
      WHERE dt.month_diff <= 3
      AND   o.store_id = 1
      AND   (o.is_shipped = 1 OR o.is_realised = 1)
      AND   device_channel = 'web'
      AND   o.order_Created_date >= 20160527) a