






/*BM42 Repeat Purchases*/
select
	to_char(a.yday_revenue,'99,99,99,99,999') as yday_rev,
	trunc((a.yday_revenue - a.yday_minus_1_revenue)*100/nullif(a.yday_minus_1_revenue,0),1) as p1,
	to_char(a.yday_revenue/a.yday_order,'99,99,99,99,999')  as yday_asp,
	trunc((a.yday_revenue/nullif(a.yday_order,0) - a.yday_minus_1_revenue/nullif(a.yday_minus_1_order,0))*100/nullif((a.yday_minus_1_revenue/a.yday_minus_1_order),0),1) as p2,
	trunc(cast(a.yday_item_count as numeric)/a.yday_order,2)  as yday_basket,
	trunc((a.yday_item_count/a.yday_order - a.yday_minus_1_item_count/nullif(yday_minus_1_order,0))*100/nullif((a.yday_minus_1_item_count/yday_minus_1_order),0),1) as p3,
	to_char(a.yday_customer,'99,99,99,99,999') as yday_customer,
	trunc((a.yday_customer - a.yday_minus_1_customer)*100/nullif(a.yday_minus_1_customer,0),1) as p4,
	
	to_char(a.mtd_revenue,'99,99,99,99,999') as mtd_revenue, 
	to_char(a.month_minus_1_revenue,'99,99,99,99,999') as month_minus_1_revenue, 
	to_char(a.month_minus_2_revenue,'99,99,99,99,999') as month_minus_2_revenue, 
	to_char(a.month_minus_3_revenue,'99,99,99,99,999') as month_minus_3_revenue,
	a.mtd_revenue/nullif(a.mtd_order,0) as mtd_asp, 
	a.month_minus_1_revenue/nullif(a.month_minus_1_order,0) as month_minus_1_asp, 
	a.month_minus_2_revenue/nullif(month_minus_2_order,0) as month_minus_2_asp, 
	a.month_minus_3_revenue/nullif(month_minus_3_order,0) as month_minus_3_asp,
	a.mtd_item_count/nullif(a.mtd_order,0) as mtd_basket, 
	trunc(cast(a.month_minus_1_item_count as numeric),2)/nullif(a.month_minus_1_order,0) as month_minus_1_basket, 
	trunc(cast(a.month_minus_2_item_count as numeric),2)/nullif(a.month_minus_2_order,0) as month_minus_2_basket, 
	trunc(cast(a.month_minus_3_item_count as numeric),2)/nullif(a.month_minus_1_order,0) as month_minus_3_basket,
	mtd_customer, 	
	month_minus_1_customer, 
	month_minus_2_customer, 
	month_minus_3_customer
from 	
	(select 
		sum(case when dt.day_diff = 1 then o.shipped_order_revenue_inc_cashback end) as yday_revenue,
		count(distinct (case when dt.day_diff = 1 then o.order_group_id end)) as yday_order,
		count(distinct (case when dt.day_diff = 1 then o.idcustomer end)) as yday_customer,
		sum(case when dt.day_diff = 1 then o.shipped_item_count end) as yday_item_count,
	
		sum(case when dt.day_diff = 2 then o.shipped_order_revenue_inc_cashback end) as yday_minus_1_revenue,
		count(distinct (case when dt.day_diff = 2 then o.order_group_id end)) as yday_minus_1_order,
		count(distinct (case when dt.day_diff = 2 then o.idcustomer end)) as yday_minus_1_customer,
		sum(case when dt.day_diff = 2 then o.shipped_item_count end) as yday_minus_1_item_count,
		
		sum(case when dt.month_to_date = 1 then o.shipped_order_revenue_inc_cashback end) as mtd_revenue,
		sum(case when dt.month_diff = 1 then o.shipped_order_revenue_inc_cashback end) as month_minus_1_revenue,
		sum(case when dt.month_diff = 2 then o.shipped_order_revenue_inc_cashback end) as month_minus_2_revenue,
		sum(case when dt.month_diff = 3 then o.shipped_order_revenue_inc_cashback end) as month_minus_3_revenue,
	
		count(distinct (case when dt.month_to_date = 1 then o.order_group_id end)) as mtd_order,
		count(distinct (case when dt.month_diff = 1 then o.order_group_id end)) as month_minus_1_order,
		count(distinct (case when dt.month_diff = 2 then o.order_group_id end)) as month_minus_2_order,
		count(distinct (case when dt.month_diff = 3 then o.order_group_id end)) as month_minus_3_order,

		count(distinct (case when dt.month_to_date = 1 then o.idcustomer end)) as mtd_customer,
		count(distinct (case when dt.month_diff = 1 then o.idcustomer end)) as month_minus_1_customer,
		count(distinct (case when dt.month_diff = 2 then o.idcustomer end)) as month_minus_2_customer,
		count(distinct (case when dt.month_diff = 3 then o.idcustomer end)) as month_minus_3_customer,
		
		sum(case when dt.month_to_date = 1 then o.shipped_item_count end) as mtd_item_count,
		sum(case when dt.month_diff = 1 then o.shipped_item_count end) as month_minus_1_item_count,
		sum(case when dt.month_diff = 2 then o.shipped_item_count end) as month_minus_2_item_count,
		sum(case when dt.month_diff = 3 then o.shipped_item_count end) as month_minus_3_item_count
	
	from 
		fact_order o, dim_date dt
	where 
    dt.month_diff<=3 
    and store_id=1
    and o.order_created_date = dt.full_date 
	and o.purchase_sequence > 1 and (o.is_shipped = 1 or o.is_realised=1)
    and o.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
    ) a                                                                                                                                                                                                                         