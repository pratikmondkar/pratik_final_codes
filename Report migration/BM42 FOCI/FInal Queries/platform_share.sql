select channel as browser_info  , 
yday_revenue_app::float/yday_revenue_all::float as yday_revenue_app_perc,
((yday_revenue_app::float/yday_revenue_all::float)-(yday_minus_1_revenue_app::float/yday_minus_1_revenue_all::float))*100 as p5,
mtd_revenue_app::float/mtd_revenue_all::float as mtd_revenue_app_perc,
month_minus_1_revenue_app::float/month_minus_1_revenue_all::float as month_minus_1_revenue_app_perc,
month_minus_2_revenue_app::float/month_minus_2_revenue_all::float as month_minus_2_revenue_app_perc,
month_minus_3_revenue_app::float/month_minus_3_revenue_all::float as month_minus_3_revenue_app_perc
from
(
select case when device_channel ='mobile-app' then ' -'||Browser_info||' app'
			 when device_channel='mobile-web' then ' -M-site'
			 when device_channel='web' then ' -Desktop' end as channel, 1 as flag,
		sum(case when dt.day_diff = 1   then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as yday_revenue_app,
		sum(case when dt.day_diff = 2    then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as yday_minus_1_revenue_app,
		sum(case when dt.month_to_date = 1   then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as mtd_revenue_app,
		sum(case when dt.month_diff = 1   then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_1_revenue_app,
		sum(case when dt.month_diff = 2   then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_2_revenue_app,
		sum(case when dt.month_diff = 3   then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_3_revenue_app	
	from 
		fact_core_item fo_1, dim_date dt 
	where 
		dt.month_diff<=3 
		and store_id=1
		and fo_1.order_created_date = dt.full_date 
		and (fo_1.is_shipped = 1 or fo_1.is_realised=1)
		and device_channel is not null
    and fo_1.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMMDD')
		group by 1,2
	) a
join
(
select 
1 as flag,
		sum(case when dt.day_diff = 1   then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end)::float as yday_revenue_all,
		sum(case when dt.day_diff = 2    then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as yday_minus_1_revenue_all,
		sum(case when dt.month_to_date = 1   then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as mtd_revenue_all,
		sum(case when dt.month_diff = 1   then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_1_revenue_all,
		sum(case when dt.month_diff = 2   then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_2_revenue_all,
		sum(case when dt.month_diff = 3   then nvl(item_revenue_inc_cashback,0)+nvl(shipping_charges,0)+nvl(gift_charges,0) end) as month_minus_3_revenue_all
	from 
		fact_core_item fo_1, dim_date dt
	where 
		dt.month_diff<=3 
		and store_id=1
		and fo_1.order_created_date = dt.full_date 
		and (fo_1.is_shipped = 1 or fo_1.is_realised=1)
		and device_channel is not null
        and fo_1.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMMDD')
		group by 1
) b
on a.flag=b.flag
where yday_revenue_app is not null
order by yday_revenue_app_perc desc