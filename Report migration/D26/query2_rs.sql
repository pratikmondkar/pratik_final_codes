
select 
	a.courier_code,
	count(*) as shipped_order_count,
	(sum((case(true) when (delivery_status = 'undelivered') then 1 else 0 end))/count(*))*100 as undelivered_count,
	(sum((case(true) when (rto_status = 1) then 1 else 0 end))/count(*))*100 as open_rto,
	(sum((case(true) when (delivery_status = 'delivered') then 1 else 0 end))/count(*))*100 as delivered_count,
	(sum((case(true) when (delivery_status = 'delivered' and (a.diff >= 0 and a.diff <= 2)) then 1 else 0 end))/count(*))*100 as "0-2",
	(sum((case(true) when (delivery_status = 'delivered' and (a.diff >= 3 and a.diff <= 5)) then 1 else 0 end))/count(*))*100 as "3-5",
	(sum((case(true) when (delivery_status = 'delivered' and (a.diff >= 6 and a.diff <= 8)) then 1 else 0 end))/count(*))*100 as "6-8",
	(sum((case(true) when (delivery_status = 'delivered' and (a.diff >= 9 and a.diff <= 11)) then 1 else 0 end))/count(*))*100 as "9-11",
	(sum((case(true) when (delivery_status = 'delivered' and (a.diff > 11)) then 1 else 0 end))/count(*))*100 as ">11",
	(sum((case(true) when (delivery_status = 'delivered' and (a.diff < 0)) then 1 else 0 end))/count(*))*100 as Others
from
	(select 
		o.order_shipped_date, c.courier_code, l.city_name,
		datediff(d,cast(o.order_delivered_date ||' '|| lpad(o.order_delivered_time,4,0) as timestamp), cast(o.order_shipped_date ||' '|| lpad(o.order_shipped_time,4,0) as timestamp)) as diff,
		(case(true) when (o.order_status = 'DL' or o.order_status = 'C') then 'delivered' else 'undelivered' end) as delivery_status,
		(select 1 from fact_rto o1 where o1.order_id = o.order_id limit 1) as rto_status
	from 
		fact_order o, dim_time t1, dim_time t2, dim_courier c, dim_location l
	where 
		o.order_shipped_time = t1.time and o.order_delivered_time = t2.time and o.order_shipped_date > to_char(sysdate - interval '1 month', 'YYYYMMDD')
		and c.id = o.idcourier and l.id = o.idlocation and c.id > 0
		and o.order_status in ('DL', 'C', 'SH')) a
group by
	a.courier_code      
