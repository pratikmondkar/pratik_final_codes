
select a.courier_code, (case(true) 
	when (a.diff >= 0 and a.diff <= 2) then '0-2'
	when (a.diff >= 3 and a.diff <= 5) then '3-5'
	when (a.diff >= 6 and a.diff <= 8) then '6-8'
	when (a.diff >= 9 and a.diff <= 11) then '9-11'
	when (a.diff > 11) then '>11' 
	else 'Others' end) as day_diff, 
	round(count(*)/max(b.order_count)*100,2) as perc
from
	(select 
		o.order_id, c.courier_code, dt.week_of_the_year, dt.year,
		datediff(d,sysdate,cast(o.order_shipped_date ||' '|| lpad(o.order_shipped_time,4,0) as timestamp)) as diff, o.order_shipped_date
	from 
		fact_order o, dim_time t1, dim_courier c, dim_date dt
	where 
		c.id = o.idcourier and o.order_status in ('SH') and dt.full_date = o.order_shipped_date and
		not exists (select 1 from fact_rto r where r.order_id = o.order_id) and c.courier_code not in ('SP', 'DH') and
		o.order_shipped_time = t1.time and o.order_shipped_date > to_char(sysdate-interval '3 month', 'YYYYMMDD')) a,
	(select
		c.courier_code, count(*) as order_count
	from
		fact_order o, dim_courier c
	where
		c.id = o.idcourier and o.order_status in ('SH','DL','C') and o.order_shipped_date > to_char(sysdate-interval '3 month', 'YYYYMMDD')
		and c.id > 0
	group by
		c.courier_code) b
where
	a.courier_code = b.courier_code and a.courier_code != ''
group by
	day_diff, a.courier_code
order by
	a.courier_code, day_diff      
