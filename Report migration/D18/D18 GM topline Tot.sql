/*D18 GM Total*/

select 
yday_sales_ex_tax/yday_mrp_ex_tax as yday_sales_percent_mrp,
yday_cost_inc_tax/yday_mrp_ex_tax as yday_cost_percent_mrp,
(yday_sales_ex_tax - yday_cost_inc_tax) as yday_rgm,
(yday_sales_ex_tax - yday_cost_inc_tax)/yday_items_sold as yday_rgm_per_item,

(yday_sales_ex_tax - yday_cost_inc_tax)/yday_sales_ex_tax as yday_gm,

last7_days_sales_ex_tax/last7_days_mrp_ex_tax as last7_days_sales_percent_mrp,
last7_days_cost_inc_tax/last7_days_mrp_ex_tax as last7_days_cost_percent_mrp,
(last7_days_sales_ex_tax-last7_days_cost_inc_tax)/last7_days_sales_ex_tax as last7_days_gm,

mtd_sales_ex_tax/mtd_mrp_ex_tax as mtd_sales_percent_mrp,
mtd_cost_inc_tax/mtd_mrp_ex_tax as mtd_cost_percent_mrp,
(mtd_sales_ex_tax-mtd_cost_inc_tax)/mtd_sales_ex_tax as mtd_gm,

month_minus_1_sales_ex_tax/month_minus_1_mrp_ex_tax as month_minus_1_sales_percent_mrp,
month_minus_1_cost_inc_tax/month_minus_1_mrp_ex_tax as month_minus_1_cost_percent_mrp,
(month_minus_1_sales_ex_tax-month_minus_1_cost_inc_tax)/month_minus_1_sales_ex_tax as month_minus_1_gm,

month_minus_2_sales_ex_tax/month_minus_2_mrp_ex_tax as month_minus_2_sales_percent_mrp,
month_minus_2_cost_inc_tax/month_minus_2_mrp_ex_tax as month_minus_2_cost_percent_mrp,
(month_minus_2_sales_ex_tax-month_minus_2_cost_inc_tax)/month_minus_2_sales_ex_tax as month_minus_2_gm,

month_minus_3_sales_ex_tax/month_minus_3_mrp_ex_tax as month_minus_3_sales_percent_mrp,
month_minus_3_cost_inc_tax/month_minus_3_mrp_ex_tax as month_minus_3_cost_percent_mrp,
(month_minus_3_sales_ex_tax-month_minus_3_cost_inc_tax)/month_minus_3_sales_ex_tax as month_minus_3_gm

from 	
(select 
sum(case when dt.day_diff = 1 then o.sales_ex_tax + nvl(o1.other_charges,0) end) as yday_sales_ex_tax,
sum(case when dt.day_diff = 1 then items_sold end) as yday_items_sold,

sum(case when dt.last7_days = 1 then o.sales_ex_tax + nvl(o1.other_charges,0) end) as last7_days_sales_ex_tax,
sum(case when dt.month_to_date = 1 then o.sales_ex_tax + nvl(o1.other_charges,0) end) as mtd_sales_ex_tax,
sum(case when dt.month_diff = 1 then o.sales_ex_tax + nvl(o1.other_charges,0) end) as month_minus_1_sales_ex_tax,
sum(case when dt.month_diff = 2 then o.sales_ex_tax + nvl(o1.other_charges,0) end) as month_minus_2_sales_ex_tax,
sum(case when dt.month_diff = 3 then o.sales_ex_tax + nvl(o1.other_charges,0) end) as month_minus_3_sales_ex_tax,

sum(case when dt.day_diff = 1 then o.cost_inc_tax end) as yday_cost_inc_tax,
sum(case when dt.last7_days = 1 then o.cost_inc_tax end) as last7_days_cost_inc_tax,
sum(case when dt.month_to_date = 1 then o.cost_inc_tax end) as mtd_cost_inc_tax,
sum(case when dt.month_diff = 1 then o.cost_inc_tax end) as month_minus_1_cost_inc_tax,
sum(case when dt.month_diff = 2 then o.cost_inc_tax end) as month_minus_2_cost_inc_tax,
sum(case when dt.month_diff = 3 then o.cost_inc_tax end) as month_minus_3_cost_inc_tax,

sum(case when dt.day_diff = 1 then o.mrp_ex_tax end) as yday_mrp_ex_tax,
sum(case when dt.last7_days = 1 then o.mrp_ex_tax end) as last7_days_mrp_ex_tax,
sum(case when dt.month_to_date = 1 then o.mrp_ex_tax end) as mtd_mrp_ex_tax,
sum(case when dt.month_diff = 1 then o.mrp_ex_tax end) as month_minus_1_mrp_ex_tax,
sum(case when dt.month_diff = 2 then o.mrp_ex_tax end) as month_minus_2_mrp_ex_tax,
sum(case when dt.month_diff = 3 then o.mrp_ex_tax end) as month_minus_3_mrp_ex_tax

from 
(select 
order_created_date, 
sum(item_revenue_inc_cashback-tax) as sales_ex_tax,
SUM(cogs+royalty_commission) as cost_inc_tax,
sum(p.article_mrp_excl_tax*oi.quantity) as mrp_ex_tax,
sum(item_revenue_inc_cashback-tax) as total_sales_ex_tax,
sum(quantity) as items_sold
from fact_orderitem oi, dim_product p
where order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
and order_created_date < to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')
and (is_realised = 1 or is_shipped = 1) 
and oi.idproduct = p.id
and oi.store_id=1
group by order_created_date) o
join dim_date dt on o.order_created_date = dt.full_date 
left join (select order_created_date, sum(shipping_charges)+sum(cod_charges)+sum(gift_charges)+sum(emi_charges) as other_charges
from fact_order 
where order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
and order_created_date < to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD') 
and (is_realised = 1 or is_shipped = 1) and store_id=1
group by order_created_date) o1 on o1.order_created_date = dt.full_date) a                                                                                                                                                                                                                              
