
SELECT BRAND,date_string_month_minus_1, date_string_month_minus_2, date_string_month_minus_3,
last7_days_cost_percent_mrp,	last7_days_gm,	last7_days_sales_percent_mrp,	month_minus_1_cost_percent_mrp,	month_minus_1_gm,	
month_minus_1_sales_percent_mrp,	month_minus_2_cost_percent_mrp,	month_minus_2_gm,	month_minus_2_sales_percent_mrp,	
month_minus_3_cost_percent_mrp,	month_minus_3_gm,	month_minus_3_sales_percent_mrp,	mtd_cost_percent_mrp,	
mtd_gm,	mtd_sales_percent_mrp,	yday_cost_percent_mrp,	yday_gm,	yday_rgm,	yday_rgm_per_item,	yday_sales_percent_mrp
FROM
(
(
select brand,
to_char(sysdate - interval '1 month','MONTH') || '-' ||  to_char(sysdate - interval '1 month','YY') as date_string_month_minus_1,
to_char(sysdate - interval '2 month','MONTH') || '-' || to_char(sysdate - interval '2 month','YY') as date_string_month_minus_2,
to_char(sysdate - interval '3 month','MONTH') || '-' || to_char(sysdate - interval '3 month','YY') as date_string_month_minus_3,	
	
	yday_sales_ex_tax/yday_mrp_ex_tax as yday_sales_percent_mrp,
	yday_cost_inc_tax/yday_mrp_ex_tax as yday_cost_percent_mrp,
	(yday_sales_ex_tax-yday_cost_inc_tax)/yday_sales_ex_tax as yday_gm,
	yday_sales_ex_tax-yday_cost_inc_tax as yday_rgm,
	(yday_sales_ex_tax-yday_cost_inc_tax)/yday_quantity as yday_rgm_per_item,
	
	last7_days_sales_ex_tax/last7_days_mrp_ex_tax as last7_days_sales_percent_mrp,
	last7_days_cost_inc_tax/last7_days_mrp_ex_tax as last7_days_cost_percent_mrp,
	(last7_days_sales_ex_tax-last7_days_cost_inc_tax)/last7_days_sales_ex_tax as last7_days_gm,

	mtd_sales_ex_tax/mtd_mrp_ex_tax as mtd_sales_percent_mrp,
	mtd_cost_inc_tax/mtd_mrp_ex_tax as mtd_cost_percent_mrp,
	(mtd_sales_ex_tax-mtd_cost_inc_tax)/mtd_sales_ex_tax as mtd_gm,

	month_minus_1_sales_ex_tax/month_minus_1_mrp_ex_tax as month_minus_1_sales_percent_mrp,
	month_minus_1_cost_inc_tax/month_minus_1_mrp_ex_tax as month_minus_1_cost_percent_mrp,
	(month_minus_1_sales_ex_tax-month_minus_1_cost_inc_tax)/month_minus_1_sales_ex_tax as month_minus_1_gm,

	month_minus_2_sales_ex_tax/month_minus_2_mrp_ex_tax as month_minus_2_sales_percent_mrp,
	month_minus_2_cost_inc_tax/month_minus_2_mrp_ex_tax as month_minus_2_cost_percent_mrp,
	(month_minus_2_sales_ex_tax-month_minus_2_cost_inc_tax)/month_minus_2_sales_ex_tax as month_minus_2_gm,

	month_minus_3_sales_ex_tax/month_minus_3_mrp_ex_tax as month_minus_3_sales_percent_mrp,
	month_minus_3_cost_inc_tax/month_minus_3_mrp_ex_tax as month_minus_3_cost_percent_mrp,
	(month_minus_3_sales_ex_tax-month_minus_3_cost_inc_tax)/month_minus_3_sales_ex_tax as month_minus_3_gm	
	from
(select brand,
		sum(case when day_diff = 1 then sales end) as yday_sales_ex_tax,
		sum(case when day_diff = 1 then cost_inc_tax end) as yday_cost_inc_tax,
		sum(case when day_diff = 1 then mrp_ex_tax end) as yday_mrp_ex_tax,
		sum(case when day_diff = 1 then quantity end) as yday_quantity,
		sum(case when last7_days = 1 then sales end) as last7_days_sales_ex_tax,
		sum(case when last7_days = 1 then cost_inc_tax end) as last7_days_cost_inc_tax,
		sum(case when last7_days = 1 then mrp_ex_tax end) as last7_days_mrp_ex_tax,		
		sum(case when month_to_date = 1 then sales end) as mtd_sales_ex_tax,
		sum(case when month_to_date = 1 then cost_inc_tax end) as mtd_cost_inc_tax,
		sum(case when month_to_date = 1 then mrp_ex_tax end) as mtd_mrp_ex_tax,
		sum(case when month_diff = 1 then sales end) as month_minus_1_sales_ex_tax,
		sum(case when month_diff = 1 then cost_inc_tax end) as month_minus_1_cost_inc_tax,
		sum(case when month_diff = 1 then mrp_ex_tax end) as month_minus_1_mrp_ex_tax,
		sum(case when month_diff = 2 then sales end) as month_minus_2_sales_ex_tax,
		sum(case when month_diff = 2 then cost_inc_tax end) as month_minus_2_cost_inc_tax,
		sum(case when month_diff = 2 then mrp_ex_tax end) as month_minus_2_mrp_ex_tax,
		sum(case when month_diff = 3 then sales end) as month_minus_3_sales_ex_tax,
		sum(case when month_diff = 3 then cost_inc_tax end) as month_minus_3_cost_inc_tax,
		sum(case when month_diff = 3 then mrp_ex_tax end) as month_minus_3_mrp_ex_tax
	from 
		 (select 
		 (case when p.brand_type='Private' then 'Fashion Brand' else 'Multi Brand' end) as brand, 
		day_diff, last7_days, month_to_date, month_diff,
			(item_revenue_inc_cashback-tax) as sales,
			(cogs+royalty_commission) as cost_inc_tax,
			oi.quantity,
			(p.article_mrp_excl_tax*oi.quantity) as mrp_ex_tax
		from fact_orderitem oi, dim_product p, dim_date dt
		where oi.supply_type='ON_HAND' and (oi.is_realised = 1 or oi.is_shipped = 1) and oi.idproduct = p.id
		and oi.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01') and order_created_date < to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')
		and oi.order_created_date = dt.full_date and dt.month_diff<=3
		and oi.store_id=1) a
		group by 1) b  
		ORDER BY BRAND
)
UNION ALL
(
select brand,
to_char(sysdate - interval '1 month','MONTH') || '-' ||  to_char(sysdate - interval '1 month','YY') as date_string_month_minus_1,
to_char(sysdate - interval '2 month','MONTH') || '-' || to_char(sysdate - interval '2 month','YY') as date_string_month_minus_2,
to_char(sysdate - interval '3 month','MONTH') || '-' || to_char(sysdate - interval '3 month','YY') as date_string_month_minus_3,	
	
	yday_sales_ex_tax/yday_mrp_ex_tax as yday_sales_percent_mrp,
	yday_cost_inc_tax/yday_mrp_ex_tax as yday_cost_percent_mrp,
	(yday_sales_ex_tax-yday_cost_inc_tax)/yday_sales_ex_tax as yday_gm,
	yday_sales_ex_tax-yday_cost_inc_tax as yday_rgm,
	(yday_sales_ex_tax-yday_cost_inc_tax)/yday_quantity as yday_rgm_per_item,
	
	last7_days_sales_ex_tax/last7_days_mrp_ex_tax as last7_days_sales_percent_mrp,
	last7_days_cost_inc_tax/last7_days_mrp_ex_tax as last7_days_cost_percent_mrp,
	(last7_days_sales_ex_tax-last7_days_cost_inc_tax)/last7_days_sales_ex_tax as last7_days_gm,

	mtd_sales_ex_tax/mtd_mrp_ex_tax as mtd_sales_percent_mrp,
	mtd_cost_inc_tax/mtd_mrp_ex_tax as mtd_cost_percent_mrp,
	(mtd_sales_ex_tax-mtd_cost_inc_tax)/mtd_sales_ex_tax as mtd_gm,

	month_minus_1_sales_ex_tax/month_minus_1_mrp_ex_tax as month_minus_1_sales_percent_mrp,
	month_minus_1_cost_inc_tax/month_minus_1_mrp_ex_tax as month_minus_1_cost_percent_mrp,
	(month_minus_1_sales_ex_tax-month_minus_1_cost_inc_tax)/month_minus_1_sales_ex_tax as month_minus_1_gm,

	month_minus_2_sales_ex_tax/month_minus_2_mrp_ex_tax as month_minus_2_sales_percent_mrp,
	month_minus_2_cost_inc_tax/month_minus_2_mrp_ex_tax as month_minus_2_cost_percent_mrp,
	(month_minus_2_sales_ex_tax-month_minus_2_cost_inc_tax)/month_minus_2_sales_ex_tax as month_minus_2_gm,

	month_minus_3_sales_ex_tax/month_minus_3_mrp_ex_tax as month_minus_3_sales_percent_mrp,
	month_minus_3_cost_inc_tax/month_minus_3_mrp_ex_tax as month_minus_3_cost_percent_mrp,
	(month_minus_3_sales_ex_tax-month_minus_3_cost_inc_tax)/month_minus_3_sales_ex_tax as month_minus_3_gm	
	from
(select brand,
		sum(case when day_diff = 1 then sales end) as yday_sales_ex_tax,
		sum(case when day_diff = 1 then cost_inc_tax end) as yday_cost_inc_tax,
		sum(case when day_diff = 1 then mrp_ex_tax end) as yday_mrp_ex_tax,
		sum(case when day_diff = 1 then quantity end) as yday_quantity,
		sum(case when last7_days = 1 then sales end) as last7_days_sales_ex_tax,
		sum(case when last7_days = 1 then cost_inc_tax end) as last7_days_cost_inc_tax,
		sum(case when last7_days = 1 then mrp_ex_tax end) as last7_days_mrp_ex_tax,		
		sum(case when month_to_date = 1 then sales end) as mtd_sales_ex_tax,
		sum(case when month_to_date = 1 then cost_inc_tax end) as mtd_cost_inc_tax,
		sum(case when month_to_date = 1 then mrp_ex_tax end) as mtd_mrp_ex_tax,
		sum(case when month_diff = 1 then sales end) as month_minus_1_sales_ex_tax,
		sum(case when month_diff = 1 then cost_inc_tax end) as month_minus_1_cost_inc_tax,
		sum(case when month_diff = 1 then mrp_ex_tax end) as month_minus_1_mrp_ex_tax,
		sum(case when month_diff = 2 then sales end) as month_minus_2_sales_ex_tax,
		sum(case when month_diff = 2 then cost_inc_tax end) as month_minus_2_cost_inc_tax,
		sum(case when month_diff = 2 then mrp_ex_tax end) as month_minus_2_mrp_ex_tax,
		sum(case when month_diff = 3 then sales end) as month_minus_3_sales_ex_tax,
		sum(case when month_diff = 3 then cost_inc_tax end) as month_minus_3_cost_inc_tax,
		sum(case when month_diff = 3 then mrp_ex_tax end) as month_minus_3_mrp_ex_tax
	from 
		 (select 
		 (case when p.brand_type='External' and p.COMMERCIAL_TYPE = 'OUTRIGHT' then 'MMB OR' 
		 when p.brand_type='External' and p.COMMERCIAL_TYPE = 'SOR' then  'MMB SOR' end) as brand, 
		day_diff, last7_days, month_to_date, month_diff,
			(item_revenue_inc_cashback-tax) as sales,
			(cogs+royalty_commission) as cost_inc_tax,
			oi.quantity,
			(p.article_mrp_excl_tax*oi.quantity) as mrp_ex_tax
		from fact_orderitem oi, dim_product p, dim_date dt
		where oi.supply_type='ON_HAND' and (oi.is_realised = 1 or oi.is_shipped = 1) and oi.idproduct = p.id
		and oi.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01') and order_created_date < to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')
		and oi.order_created_date = dt.full_date and dt.month_diff<=3
		and oi.store_id=1) a
		WHERE BRAND IS NOT NULL
		group by 1
		) b
		ORDER BY BRAND
)
)		
ORDER BY BRAND
