SELECT 
    a.style_id,
	style_status,
	photoshoot_priority	
FROM
    dim_product a
WHERE
    a.style_status IN ('D' , 'CON')
        AND a.photoshoot_priority != 99
        AND (SELECT 
            SUM(c.net_inventory_count)
        FROM
            fact_inventory_count c,
            dim_product p1
        WHERE
            c.sku_id = p1.sku_id
                AND p1.style_id = a.style_id
                AND c.quality = '1'
                AND c.date = DATE_FORMAT(now(), '%Y%m%d')) > 1
        AND EXISTS( SELECT 
            1
        FROM
            fact_inventory_item i
        WHERE
            i.sku_id = a.sku_id
                AND i.item_status = 'STORED')
        AND NOT EXISTS( SELECT 
            1
        FROM
            dim_product p1
        WHERE
            a.style_id = p1.style_id
                AND (p1.style_status = 'P'
                OR p1.style_catalogued_date > 19700101))
        AND NOT EXISTS( SELECT 
            1
        FROM
            fact_inventory_item i,
            dim_product p2
        WHERE
            i.sku_id = p2.sku_id
                AND p2.style_id = a.style_id
                AND i.item_status IN ('PROCESSING' , 'TRANSIT'))
GROUP BY 1,2,3;


SELECT 
    style_id,
    style_status,
    photoshoot_priority
FROM
    dim_product a
WHERE
   style_id=728389;

select item_status from bidb.fact_inventory_item fii
where  sku_id in (select distinct sku_id from bidb.dim_product dp where dp.style_id=728389);

SELECT 
            SUM(c.net_inventory_count)
        FROM
            fact_inventory_count c,
            dim_product p1
        WHERE
            c.sku_id = p1.sku_id
                AND p1.style_id =587221
                AND c.quality = '1'
                AND c.date = DATE_FORMAT(now() - interval 1 day, '%Y%m%d');

SELECT 
           p1.style_id, SUM(c.net_inventory_count)
        FROM
            fact_inventory_count c,
            dim_product p1
        WHERE
            c.sku_id = p1.sku_id
                AND p1.style_id in('587172','587173','587175','587176','587219','587221','587222','587223','620862','650807','650817')
                AND c.quality = '1'
                AND c.date = 20150416
          group by p1.style_id;
