/*D18 GM Complete Details*/
select 
	b.*, 
	round((b.sales_value_ex_tax_yesterday * 100)/nullif(b.article_mrp_ex_tax_yesterday,0), 2) as sales_value_percent_yesterday,
	round((b.item_purchase_price_yesterday * 100)/nullif(b.article_mrp_ex_tax_yesterday,0), 2) as cost_percent_yesterday,
	round(((b.sales_value_ex_tax_yesterday - b.item_purchase_price_yesterday) * 100)/nullif(b.sales_value_ex_tax_yesterday,0), 2) as gross_margin_percent_yesterday,
	round((b.sales_value_ex_tax_yesterday - b.item_purchase_price_yesterday),0) as rupee_gross_margin,
	round((b.sales_value_ex_tax_yesterday - b.item_purchase_price_yesterday)/quantity,0) as rgm_per_item
/*	round((b.sales_value_ex_tax_7_days * 100)/nullif(b.article_mrp_ex_tax_7_days,0), 2) as sales_value_percent_7_days,
	round((b.item_purchase_price_7_days * 100)/nullif(b.article_mrp_ex_tax_7_days,0), 2) as cost_percent_7_days,
	round(((b.sales_value_ex_tax_7_days - b.item_purchase_price_7_days) * 100)/nullif(b.sales_value_ex_tax_7_days,0), 2) as gross_margin_percent_7_days,
	round((b.sales_value_ex_tax_mtd * 100)/nullif(b.article_mrp_ex_tax_mtd,0), 2) as sales_value_percent_mtd,
	round((b.item_purchase_price_mtd * 100)/nullif(b.article_mrp_ex_tax_mtd,0), 2) as cost_percent_mtd,
	round(((b.sales_value_ex_tax_mtd - b.item_purchase_price_mtd) * 100)/nullif(b.sales_value_ex_tax_mtd,0), 2) as gross_margin_percent_mtd,
	round((b.sales_value_ex_tax_month_minus_1 * 100)/nullif(b.article_mrp_ex_tax_month_minus_1,0), 2) as sales_value_percent_month_minus_1,
	round((b.item_purchase_price_month_minus_1 * 100)/nullif(b.article_mrp_ex_tax_month_minus_1,0), 2) as cost_percent_month_minus_1,
	round(((b.sales_value_ex_tax_month_minus_1 - b.item_purchase_price_month_minus_1) * 100)/nullif(b.sales_value_ex_tax_month_minus_1,0), 2) as gross_margin_percent_month_minus_1,
	round((b.sales_value_ex_tax_month_minus_2 * 100)/nullif(b.article_mrp_ex_tax_month_minus_2,0), 2) as sales_value_percent_month_minus_2,
	round((b.item_purchase_price_month_minus_2 * 100)/nullif(b.article_mrp_ex_tax_month_minus_2,0), 2) as cost_percent_month_minus_2,
	round(((b.sales_value_ex_tax_month_minus_2 - b.item_purchase_price_month_minus_2) * 100)/nullif(b.sales_value_ex_tax_month_minus_2,0), 2) as gross_margin_percent_month_minus_2,
	round((b.sales_value_ex_tax_month_minus_3 * 100)/nullif(b.article_mrp_ex_tax_month_minus_3,0), 2) as sales_value_percent_month_minus_3,
	round((b.item_purchase_price_month_minus_3 * 100)/nullif(b.article_mrp_ex_tax_month_minus_3,0), 2) as cost_percent_month_minus_3,
	round(((b.sales_value_ex_tax_month_minus_3 - b.item_purchase_price_month_minus_3) * 100)/nullif(b.sales_value_ex_tax_month_minus_1,0), 2) as gross_margin_percent_month_minus_3*/
from
	(select 
		a.brand,
		sum(case when day_diff=1 then article_mrp_ex_tax else 0 end) as article_mrp_ex_tax_yesterday,
		sum(case when day_diff=1 then item_purchase_price else 0 end) as item_purchase_price_yesterday,
		sum(case when day_diff=1 then sales_value_ex_tax+other_charges else 0 end) as sales_value_ex_tax_yesterday,
		sum(case when day_diff=1 then quantity end) as quantity
/*		sum(case when (a.last7_days = 1) then article_mrp_ex_tax else 0 end) as article_mrp_ex_tax_7_days,
		sum(case when (a.last7_days = 1) then item_purchase_price else 0 end) as item_purchase_price_7_days,
		sum(case when (a.last7_days = 1) then sales_value_ex_tax+other_charges else 0 end) as sales_value_ex_tax_7_days,
		sum(case when (a.month_to_date = 1) then article_mrp_ex_tax else 0 end) as article_mrp_ex_tax_mtd,
		sum(case when (a.month_to_date = 1) then item_purchase_price else 0 end) as item_purchase_price_mtd,
		sum(case when (a.month_to_date = 1) then sales_value_ex_tax+other_charges else 0 end) as sales_value_ex_tax_mtd,
		sum(case when month_diff=1 then article_mrp_ex_tax else 0 end) as article_mrp_ex_tax_month_minus_1,
		sum(case when month_diff=1 then item_purchase_price else 0 end) as item_purchase_price_month_minus_1,
		sum(case when month_diff=1 then sales_value_ex_tax+other_charges else 0 end) as sales_value_ex_tax_month_minus_1,
		sum(case when month_diff=2 then article_mrp_ex_tax else 0 end) as article_mrp_ex_tax_month_minus_2,
		sum(case when month_diff=2 then item_purchase_price else 0 end) as item_purchase_price_month_minus_2,
		sum(case when month_diff=2 then sales_value_ex_tax+other_charges else 0 end) as sales_value_ex_tax_month_minus_2,
		sum(case when month_diff=3 then article_mrp_ex_tax else 0 end) as article_mrp_ex_tax_month_minus_3,
		sum(case when month_diff=3 then item_purchase_price else 0 end) as item_purchase_price_month_minus_3,
		sum(case when month_diff=3 then sales_value_ex_tax+other_charges else 0 end) as sales_value_ex_tax_month_minus_3*/
	from 
		(select 
			CASE WHEN p.brand LIKE 'Kook N Keech%' THEN 'Kook N Keech' 
		     WHEN p.brand LIKE 'Yellow Kite%' THEN 'Yellow Kite' 
			  WHEN p.brand in ('D Muse by Dressberry', 'Gossip by Dressberry', 'DressBerry') then 'DressBerry'
			 WHEN p.brand LIKE 'Anouk%' THEN 'Anouk' ELSE p.brand END AS brand,
			day_diff,dt.full_date, dt.month_to_date, dt.last7_days, dt.month_diff, (p.article_mrp_excl_tax*o.item_quantity) as article_mrp_ex_tax, 
			(item_purchase_price_inc_tax - vendor_funding + royalty_commission) as item_purchase_price,
			(item_revenue+cashback_redeemed-tax_amount) as sales_value_ex_tax,
			0 as other_charges,
			o.item_quantity as quantity
			from 
			orders_directional o, dim_product p, dim_date dt
		where 
			(o.is_realised = 1 or o.is_shipped = 1) and o.sku_id = p.sku_id and dt.full_date = o.order_created_date
			and dt.month_diff<=3 and o.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01') 
			and order_created_date < to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')
		) a
	group by
		a.brand
	order by
		4 desc) b                                                                                                                                                                                                                                                      
