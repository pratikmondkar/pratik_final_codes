/*D18 GM Market Place*/
select 
	yday_sales_ex_tax/yday_mrp_ex_tax as yday_sales_percent_mrp,
	yday_cost_inc_tax/yday_mrp_ex_tax as yday_cost_percent_mrp,
	(yday_sales_ex_tax-yday_cost_inc_tax)/yday_sales_ex_tax as yday_gm,
	yday_sales_ex_tax-yday_cost_inc_tax as yday_rgm,
	(yday_sales_ex_tax-yday_cost_inc_tax)/yday_quantity as yday_rgm_per_item

/*
	last7_days_sales_ex_tax/last7_days_mrp_ex_tax as last7_days_sales_percent_mrp,
	last7_days_cost_inc_tax/last7_days_mrp_ex_tax as last7_days_cost_percent_mrp,
	(last7_days_sales_ex_tax-last7_days_cost_inc_tax)/last7_days_sales_ex_tax as last7_days_gm,

	mtd_sales_ex_tax/mtd_mrp_ex_tax as mtd_sales_percent_mrp,
	mtd_cost_inc_tax/mtd_mrp_ex_tax as mtd_cost_percent_mrp,
	(mtd_sales_ex_tax-mtd_cost_inc_tax)/mtd_sales_ex_tax as mtd_gm,

	month_minus_1_sales_ex_tax/month_minus_1_mrp_ex_tax as month_minus_1_sales_percent_mrp,
	month_minus_1_cost_inc_tax/month_minus_1_mrp_ex_tax as month_minus_1_cost_percent_mrp,
	(month_minus_1_sales_ex_tax-month_minus_1_cost_inc_tax)/month_minus_1_sales_ex_tax as month_minus_1_gm,

	month_minus_2_sales_ex_tax/month_minus_2_mrp_ex_tax as month_minus_2_sales_percent_mrp,
	month_minus_2_cost_inc_tax/month_minus_2_mrp_ex_tax as month_minus_2_cost_percent_mrp,
	(month_minus_2_sales_ex_tax-month_minus_2_cost_inc_tax)/month_minus_2_sales_ex_tax as month_minus_2_gm,

	month_minus_3_sales_ex_tax/month_minus_3_mrp_ex_tax as month_minus_3_sales_percent_mrp,
	month_minus_3_cost_inc_tax/month_minus_3_mrp_ex_tax as month_minus_3_cost_percent_mrp,
	(month_minus_3_sales_ex_tax-month_minus_3_cost_inc_tax)/month_minus_3_sales_ex_tax as month_minus_3_gm*/	
	from
(select 
		sum(case when day_diff = 1 then sales+other_charges end) as yday_sales_ex_tax,
		sum(case when day_diff = 1 then cost_inc_tax end) as yday_cost_inc_tax,
		sum(case when day_diff = 1 then mrp_ex_tax end) as yday_mrp_ex_tax,
		sum(case when day_diff = 1 then quantity end) as yday_quantity
/*		sum(case when last7_days = 1 then sales+other_charges end) as last7_days_sales_ex_tax,
		sum(case when last7_days = 1 then cost_inc_tax end) as last7_days_cost_inc_tax,
		sum(case when last7_days = 1 then mrp_ex_tax end) as last7_days_mrp_ex_tax,		
		sum(case when month_to_date = 1 then sales+other_charges end) as mtd_sales_ex_tax,
		sum(case when month_to_date = 1 then cost_inc_tax end) as mtd_cost_inc_tax,
		sum(case when month_to_date = 1 then mrp_ex_tax end) as mtd_mrp_ex_tax,
		sum(case when month_diff = 1 then sales+other_charges end) as month_minus_1_sales_ex_tax,
		sum(case when month_diff = 1 then cost_inc_tax end) as month_minus_1_cost_inc_tax,
		sum(case when month_diff = 1 then mrp_ex_tax end) as month_minus_1_mrp_ex_tax,
		sum(case when month_diff = 2 then sales+other_charges end) as month_minus_2_sales_ex_tax,
		sum(case when month_diff = 2 then cost_inc_tax end) as month_minus_2_cost_inc_tax,
		sum(case when month_diff = 2 then mrp_ex_tax end) as month_minus_2_mrp_ex_tax,
		sum(case when month_diff = 3 then sales+other_charges end) as month_minus_3_sales_ex_tax,
		sum(case when month_diff = 3 then cost_inc_tax end) as month_minus_3_cost_inc_tax,
		sum(case when month_diff = 3 then mrp_ex_tax end) as month_minus_3_mrp_ex_tax*/
	from 
		 (select 
			dt.day_diff, dt.last7_days, dt.month_to_date, dt.month_diff, (item_revenue+cashback_redeemed-tax_amount) as sales,
			item_purchase_price_inc_tax - vendor_funding as cost_inc_tax,
			oi.item_quantity as quantity,
			(p.article_mrp_excl_tax*oi.item_quantity) as mrp_ex_tax,
				 0 as other_charges
		from orders_directional oi, dim_product p, dim_date dt
		where dt.month_diff<=3 and oi.order_created_date=dt.full_date and (is_realised = 1 or is_shipped = 1) 
			and order_created_date < to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD') and oi.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01')
			and oi.sku_id = p.sku_id and oi.supply_type='JUST_IN_TIME'
		) o) oo                                                              
