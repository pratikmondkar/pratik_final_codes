/*D18 GM Details Pri+Ex*/

(
select 
	b.*, 
/*	to_char(sysdate - interval '1 month','MONTH') || '-' ||  to_char(sysdate - interval '1 month','YY') as date_string_month_minus_1,
to_char(sysdate - interval '2 month','MONTH') || '-' || to_char(sysdate - interval '2 month','YY') as date_string_month_minus_2,
to_char(sysdate - interval '3 month','MONTH') || '-' || to_char(sysdate - interval '3 month','YY') as date_string_month_minus_3,	*/
	round(((b.sales_value_ex_tax_yesterday - b.item_purchase_price_yesterday) * 100)/b.sales_value_ex_tax_yesterday, 2) as gross_margin_percent_yesterday
/*	round(((b.sales_value_ex_tax_7_days - b.item_purchase_price_7_days) * 100)/b.sales_value_ex_tax_7_days, 2) as gross_margin_percent_7_days,
	round(((b.sales_value_ex_tax_mtd - b.item_purchase_price_mtd) * 100)/b.sales_value_ex_tax_mtd, 2) as gross_margin_percent_mtd,
	round(((b.sales_value_ex_tax_month_minus_1 - b.item_purchase_price_month_minus_1) * 100)/b.sales_value_ex_tax_month_minus_1, 2) as gross_margin_percent_month_minus_1,
	round(((b.sales_value_ex_tax_month_minus_2 - b.item_purchase_price_month_minus_2) * 100)/b.sales_value_ex_tax_month_minus_2, 2) as gross_margin_percent_month_minus_2,
	round(((b.sales_value_ex_tax_month_minus_3 - b.item_purchase_price_month_minus_3) * 100)/b.sales_value_ex_tax_month_minus_1, 2) as gross_margin_percent_month_minus_3*/
from
	(select 
		a.brand,
		sum(case when a.day_diff=1 then item_purchase_price else 0 end) as item_purchase_price_yesterday,
		sum(case when a.day_diff=1 then sales_value_ex_tax else 0 end) as sales_value_ex_tax_yesterday
/*		sum(case when (a.last7_days = 1) then item_purchase_price else 0 end) as item_purchase_price_7_days,
		sum(case when (a.last7_days = 1) then sales_value_ex_tax else 0 end) as sales_value_ex_tax_7_days,
		sum(case when (a.month_to_date = 1) then item_purchase_price else 0 end) as item_purchase_price_mtd,
		sum(case when (a.month_to_date = 1) then sales_value_ex_tax else 0 end) as sales_value_ex_tax_mtd,
		sum(case when a.month_diff=1 then item_purchase_price else 0 end) as item_purchase_price_month_minus_1,
		sum(case when a.month_diff=1 then sales_value_ex_tax else 0 end) as sales_value_ex_tax_month_minus_1,
		sum(case when a.month_diff=2 then item_purchase_price else 0 end) as item_purchase_price_month_minus_2,
		sum(case when a.month_diff=2 then sales_value_ex_tax else 0 end) as sales_value_ex_tax_month_minus_2,
		sum(case when a.month_diff=3 then item_purchase_price else 0 end) as item_purchase_price_month_minus_3,
		sum(case when a.month_diff=3 then sales_value_ex_tax else 0 end) as sales_value_ex_tax_month_minus_3*/
	from 
		 (select 
		 (case when p.brand_type='Private' then 'Fashion Brand' else 'Multi Brand' end) as brand, 
		day_diff, last7_days, month_to_date, month_diff,
			(item_revenue+cashback_redeemed-tax_amount) as sales_value_ex_tax,
			(item_purchase_price_inc_tax - vendor_funding+royalty_commission) as item_purchase_price
		from orders_directional oi, dim_product p, dim_date dt
		where oi.supply_type='ON_HAND' and (oi.is_realised = 1 or oi.is_shipped = 1) and oi.sku_id = p.sku_id
		and oi.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01') and order_created_date < to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')
		and oi.order_created_date = dt.full_date and dt.month_diff<=3) a
		group by 1) b   
		where brand is not null
)
union all
(
select 
	b.*, 
/*	to_char(sysdate - interval '1 month','MONTH') || '-' ||  to_char(sysdate - interval '1 month','YY') as date_string_month_minus_1,
to_char(sysdate - interval '2 month','MONTH') || '-' || to_char(sysdate - interval '2 month','YY') as date_string_month_minus_2,
to_char(sysdate - interval '3 month','MONTH') || '-' || to_char(sysdate - interval '3 month','YY') as date_string_month_minus_3,*/	
round(((b.sales_value_ex_tax_yesterday - b.item_purchase_price_yesterday) * 100)/b.sales_value_ex_tax_yesterday, 2) as gross_margin_percent_yesterday
/*	round(((b.sales_value_ex_tax_7_days - b.item_purchase_price_7_days) * 100)/b.sales_value_ex_tax_7_days, 2) as gross_margin_percent_7_days,
	round(((b.sales_value_ex_tax_mtd - b.item_purchase_price_mtd) * 100)/b.sales_value_ex_tax_mtd, 2) as gross_margin_percent_mtd,
	round(((b.sales_value_ex_tax_month_minus_1 - b.item_purchase_price_month_minus_1) * 100)/b.sales_value_ex_tax_month_minus_1, 2) as gross_margin_percent_month_minus_1,
	round(((b.sales_value_ex_tax_month_minus_2 - b.item_purchase_price_month_minus_2) * 100)/b.sales_value_ex_tax_month_minus_2, 2) as gross_margin_percent_month_minus_2,
	round(((b.sales_value_ex_tax_month_minus_3 - b.item_purchase_price_month_minus_3) * 100)/b.sales_value_ex_tax_month_minus_1, 2) as gross_margin_percent_month_minus_3*/
from
	(select 
		a.brand,
		sum(case when a.day_diff=1 then item_purchase_price else 0 end) as item_purchase_price_yesterday,
		sum(case when a.day_diff=1 then sales_value_ex_tax else 0 end) as sales_value_ex_tax_yesterday
/*		sum(case when (a.last7_days = 1) then item_purchase_price else 0 end) as item_purchase_price_7_days,
		sum(case when (a.last7_days = 1) then sales_value_ex_tax else 0 end) as sales_value_ex_tax_7_days,
		sum(case when (a.month_to_date = 1) then item_purchase_price else 0 end) as item_purchase_price_mtd,
		sum(case when (a.month_to_date = 1) then sales_value_ex_tax else 0 end) as sales_value_ex_tax_mtd,
		sum(case when a.month_diff=1 then item_purchase_price else 0 end) as item_purchase_price_month_minus_1,
		sum(case when a.month_diff=1 then sales_value_ex_tax else 0 end) as sales_value_ex_tax_month_minus_1,
		sum(case when a.month_diff=2 then item_purchase_price else 0 end) as item_purchase_price_month_minus_2,
		sum(case when a.month_diff=2 then sales_value_ex_tax else 0 end) as sales_value_ex_tax_month_minus_2,
		sum(case when a.month_diff=3 then item_purchase_price else 0 end) as item_purchase_price_month_minus_3,
		sum(case when a.month_diff=3 then sales_value_ex_tax else 0 end) as sales_value_ex_tax_month_minus_3*/
	from 
		 (select 
		 (case when p.brand_type='External' and p.COMMERCIAL_TYPE = 'OUTRIGHT' then 'MMB OUTRIGHT' 
		 when p.brand_type='External' and p.COMMERCIAL_TYPE = 'SOR' then  'MMB SOR' end) as brand, 
		day_diff, last7_days, month_to_date, month_diff,
			(item_revenue+cashback_redeemed-tax_amount) as sales_value_ex_tax,
			(item_purchase_price_inc_tax - vendor_funding+royalty_commission) as item_purchase_price
		from orders_directional oi, dim_product p, dim_date dt
		where oi.supply_type='ON_HAND' and (oi.is_realised = 1 or oi.is_shipped = 1) and oi.sku_id = p.sku_id
		and oi.order_created_date >= to_char(dateadd(month,-3,convert_timezone('Asia/Calcutta',getdate())),'YYYYMM01') and order_created_date < to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')
		and oi.order_created_date = dt.full_date and dt.month_diff<=3) a
		group by 1) b
		where brand is not null
)		
