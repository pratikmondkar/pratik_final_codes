WITH uniq_sku_vendor_name AS
(
  SELECT sku_id,
         MAX(vendor_name) AS vendor_name
  FROM fact_purchase_order AS FPO
  WHERE vendor_name != 'OPEN STOCK'
  GROUP BY sku_id
  HAVING COUNT(distinct vendor_name) = 1
),
FPS_sku_style_live AS
(
  SELECT sku_id,
         is_live_on_portal AS sku_live,
         MAX(is_live_on_portal) OVER (PARTITION BY style_id) AS style_live
  FROM fact_product_snapshot AS FPS
  WHERE FPS.date = CAST(TO_CHAR(SYSDATE-INTERVAL '1 day','YYYYMMDD') AS BIGINT)
)
SELECT FII.core_item_id AS Item_Code,
       FII.item_barcode AS Item_Barcode,
       DIM_PRD.sku_code AS SKU_Code,
       DIM_PRD.style_id AS style_id,
       DIM_PRD.brand AS Brand,
       DIM_PRD.article_type AS Article_Type,
       TO_DATE((CAST(FII.rejected_date AS CHAR) || '-' || LPAD(CAST(FII.rejected_time AS CHAR),4,'0')),'YYYYMMDD-HHMI') AS State_Change_date,
       TO_CHAR(TO_DATE((CAST(FII.rejected_date AS CHAR) || '-' || LPAD(CAST(FII.rejected_time AS CHAR),4,'0')),'YYYYMMDD-HHMI'),'MONTH') AS State_Change_Month,
       FPO.vendor_name AS vendor_name_org,
       FPO.sku_id AS sku_id,
       CASE
         WHEN FPO.vendor_name != 'OPEN STOCK' THEN FPO.vendor_name
         ELSE uniq_sku_vendor_name.vendor_name
       END AS Vendor_Name,
       CASE
         WHEN FPO.item_purchase_price IS NULL OR FPO.item_purchase_price = 0 THEN DIM_PRD.article_mrp_excl_tax*.65
         ELSE FPO.item_purchase_price
       END AS Item_Purchase_Price,
       CASE
         WHEN FPO.item_purchase_price_inc_tax IS NULL OR FPO.item_purchase_price_inc_tax = 0 THEN DIM_PRD.article_mrp*.65
         ELSE FPO.item_purchase_price_inc_tax
       END AS Item_Purchase_Price_Inc_Tax,
       FPO.tax_rate AS Tax_Rate,
       FPO.item_landed_price AS Landed_Price,
       FII.bin_barcode AS Bin_Barcode,
       FII.item_status AS Item_Status,
       FII.quality AS Quality_Status,
       FII.warehouse_id AS warehouse_id,
       FII.po_barcode AS PO_Code,
       FII.lot_barcode AS LOT_Code,
       FII.inward_date AS Inward_date,
       DIM_PRD.gtin AS EAN_Code,
       FII.reject_reason_code AS reject_reason_code,
       FII.reject_reason_description AS reject_reason_description,
       TO_CHAR(TO_DATE(FII.inward_date,'YYYYMMDD'),'YYYY-MM-DD') AS Inward_date,
       FII.reject_reason_code AS Reject_code,
       FII.reject_reason_description AS COMMENT,
       DIM_PRD.master_category AS Master_Category,
       DIM_PRD.sub_category AS Sub_Category,
       DIM_PRD.category_manager AS Category_Manager,
       DIM_PRD.brand_type AS Brand_Type,
       DIM_PRD.commercial_type AS Commercial_Type,
       DIM_PRD.vendor_article_number AS Vendor_Article_Number,
       DIM_PRD.category_head AS Category_Head,
       DIM_PRD.season_code AS Season_Code,
       SUBSTRING(FII.rejected_date,1,4) AS rejected_year,
       CASE
         WHEN DATEDIFF (DAY,TO_DATE(FII.inward_date,'%Y%m%d'),convert_timezone ('Asia/Calcutta',getdate ())) <= 30 THEN '0-30 days'
         WHEN DATEDIFF (DAY,TO_DATE(FII.inward_date,'%Y%m%d'),convert_timezone ('Asia/Calcutta',getdate ())) <= 60 THEN '30-60 days'
         WHEN DATEDIFF (DAY,TO_DATE(FII.inward_date,'%Y%m%d'),convert_timezone ('Asia/Calcutta',getdate ())) <= 90 THEN '60-90 days'
         WHEN DATEDIFF (DAY,TO_DATE(FII.inward_date,'%Y%m%d'),convert_timezone ('Asia/Calcutta',getdate ())) <= 120 THEN '90-120 days'
         WHEN DATEDIFF (DAY,TO_DATE(FII.inward_date,'%Y%m%d'),convert_timezone ('Asia/Calcutta',getdate ())) <= 150 THEN '120-150 days'
         WHEN DATEDIFF (DAY,TO_DATE(FII.inward_date,'%Y%m%d'),convert_timezone ('Asia/Calcutta',getdate ())) <= 180 THEN '150-180 days'
         WHEN DATEDIFF (DAY,TO_DATE(FII.inward_date,'%Y%m%d'),convert_timezone ('Asia/Calcutta',getdate ())) > 180 THEN '180+ days'
       END AS Inward_Age_Bucket,
       CASE
         WHEN DATEDIFF (DAY,TO_DATE(rejected_date,'YYYYMMDD'),convert_timezone ('Asia/Calcutta',getdate ())) <= 7 THEN '0-7 days'
         WHEN DATEDIFF (DAY,TO_DATE(rejected_date,'YYYYMMDD'),convert_timezone ('Asia/Calcutta',getdate ())) <= 14 THEN '7-14 days'
         WHEN DATEDIFF (DAY,TO_DATE(rejected_date,'YYYYMMDD'),convert_timezone ('Asia/Calcutta',getdate ())) > 14 THEN '14+ days'
       END AS Rresolution_Pending_Days,
       CASE
         WHEN FII.rejected_at IN ('OUTBOUND_QC','RETURN_FROM_OPS') THEN 'Outbound'
         ELSE CASE
         WHEN FII.rejected_at = 'CUSTOMER_RETURNED' THEN 'Not Outbound'
         ELSE NULL
       END END AS QC_Rejected,
       action_status AS action_status,
       DIM_Action_Status.description description,
       DIM_Action_Status. "OWNER" "OWNER",
       DIM_Action_Status.followup_action AS followup_action,
       DIM_PRD.style_status AS style_status,
       CASE
         WHEN FPS_sku_style_live.sku_live = 1 THEN 'Y'
         ELSE 'N'
       END AS is_sku_live_on_portal,
       CASE
         WHEN FPS_sku_style_live.style_live = 1 THEN 'Y'
         ELSE 'N'
       END AS is_style_live_on_portal,
       DIM_PRD.vendor_article_number AS vendor_article_number,
       DIM_PRD.vendor_article_name AS vendor_article_name,
       DIM_PRD.size AS size
FROM fact_inventory_item AS FII
  JOIN dim_product AS DIM_PRD ON (FII.sku_id = DIM_PRD.sku_id)
  JOIN fact_purchase_order AS FPO ON (FII.po_sku_id = FPO.po_sku_id)
  LEFT JOIN uniq_sku_vendor_name ON (FII.sku_id = uniq_sku_vendor_name.sku_id)
  LEFT JOIN dim_rejection_action_status AS DIM_Action_Status ON (FII.action_status = DIM_Action_Status.action_code)
  LEFT JOIN FPS_sku_style_live AS FPS_sku_style_live ON (FII.sku_id = FPS_sku_style_live.sku_id)
WHERE FII.quality IN ('Q2','Q3','Q4')
AND   FII.item_status IN ('STORED','FOUND','CUSTOMER_RETURNED','RETURN_FROM_OPS')
AND   FII.action_status NOT IN ('CL_ADDED','CL_NOT_FOUND','CL_TRANSFER_INVENTORY')
AND   (FPO.vendor_name = 'OPEN STOCK' and uniq_sku_vendor_name.sku_id IS  NULL)
