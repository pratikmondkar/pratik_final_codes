with vendor_inactive as (select vendors_inactive,count(*) as count_vend_inact from dim_quality_rejection_stocks_parameters group by 1),
--	vendor_name as (SELECT distinct po_sku_id,vendor_name FROM fact_purchase_order WHERE vendor_name != 'OPEN STOCK')
 qc_brand as (SELECT "100_percentage_QC_brand",COUNT(*) as qc_brand  FROM dim_quality_rejection_stocks_parameters group by 1),
 fact_product_snapshot as (SELECT sku_id,is_live_on_portal FROM fact_product_snapshot WHERE DATE = to_char(sysdate-INTERVAL '1 DAY', 'YYYYMMDD')),
 fact_product_snapshot2 as (SELECT style_id,MAX(is_live_on_portal) as is_live_on_portal FROM fact_product_snapshot WHERE DATE = to_char(sysdate - INTERVAL '1 DAY', 'YYYYMMDD') group by 1)
Select main.*,
				case when vi.count_vend_inact > 0 then 'Yes' else 'No' end as is_vendor_inactive,
				case when qc.qc_brand > 0 then 'Yes' else 'No' end as "100_percentage_QC_brand",
				case when fps.is_live_on_portal = 1 then 'Y' else 'N ' end AS is_sku_live_on_portal,
				case when fps2.is_live_on_portal = 1 then 'Y' else 'N ' end AS is_style_live_on_portal
from(
SELECT 
	invn_item.core_item_id AS Item_Code,
	invn_item.item_barcode AS Item_Barcode,
	dim_prd.sku_code AS SKU_Code,
	dim_prd.style_id AS style_id,
	dim_prd.brand AS Brand,
	dim_prd.article_type AS Article_Type,
	purchase_order.vendor_name AS vendor_name,
	cast(invn_item.rejected_date ||' '|| lpad(invn_item.rejected_time,4,0) as timestamp) as State_Change_date,
	TO_CHAR(TO_DATE(date_part(mon,cast(invn_item.rejected_date ||' '|| lpad(invn_item.rejected_time,4,0) as timestamp))::smallint,'MM'), 'MONTH') as State_Change_Month,
	purchase_order.vendor_name AS vendor_name_org,
	purchase_order.sku_id AS sku_id,
	case when purchase_order.vendor_name != 'OPEN STOCK' then purchase_order.vendor_name end,
--		(SELECT distinct vendor_name 
--		 FROM fact_purchase_order AS Q1
--		 WHERE Q1.sku_id = purchase_order.sku_id AND Q1.vendor_name != 'OPEN STOCK') AS Vendor_Name,
	(case when purchase_order.item_purchase_price IS NULL OR purchase_order.item_purchase_price = 0 then dim_prd.article_mrp_excl_tax * .65 else purchase_order.item_purchase_price end) AS Item_Purchase_Price,
	purchase_order.tax_rate AS Tax_Rate,
	purchase_order.item_landed_price AS Landed_Price,
	invn_item.bin_barcode AS Bin_Barcode,
	invn_item.item_status AS Item_Status,
	invn_item.quality AS Quality_Status,
	invn_item.warehouse_id AS warehouse_id,
	invn_item.po_barcode AS PO_Code,
	invn_item.lot_barcode AS LOT_Code,
	invn_item.inward_date AS Inward_date,
	dim_prd.gtin AS EAN_Code,
	invn_item.reject_reason_code AS reject_reason_code,
	invn_item.reject_reason_description AS reject_reason_description,
	to_char(to_date(invn_item.Inward_date,'YYYYMMDD'),'YYYYMMDD') AS Inward_date,
	invn_item.reject_reason_code AS Reject_code,
	invn_item.reject_reason_description AS COMMENT,
	-- Fields added for D58 Extended report
	dim_prd.master_category AS Master_Category,
	dim_prd.sub_category AS Sub_Category,
	dim_prd.category_manager AS Category_Manager,
	dim_prd.brand_type AS Brand_Type,
	dim_prd.commercial_type AS Current_Commercial_Type,
	dim_prd.vendor_article_number AS Vendor_Article_Number,
	dim_prd.category_head AS Category_Head,
	dim_prd.season_code AS Season_Code,
	
	SUBSTRING(invn_item.rejected_date,1,4) AS rejected_year,
	CASE 
		WHEN datediff(d,to_date(invn_item.inward_date,'YYYYMMDD'),SYSDATE) <= 30 THEN '0-30 days'
		WHEN datediff(d,to_date(invn_item.inward_date,'YYYYMMDD'),SYSDATE) <= 60 THEN '30-60 days'
		WHEN datediff(d,to_date(invn_item.inward_date,'YYYYMMDD'),SYSDATE) <= 90 THEN '60-90 days'
		WHEN datediff(d,to_date(invn_item.inward_date,'YYYYMMDD'),SYSDATE) <= 120 THEN '90-120 days'
		WHEN datediff(d,to_date(invn_item.inward_date,'YYYYMMDD'),SYSDATE) <= 150 THEN '120-150 days'
		WHEN datediff(d,to_date(invn_item.inward_date,'YYYYMMDD'),SYSDATE) <= 180 THEN '150-180 days'
		WHEN datediff(d,to_date(invn_item.inward_date,'YYYYMMDD'),SYSDATE) > 180 THEN '180+ days'
	END AS Inward_Age_Bucket,
	
	CASE 
		WHEN datediff(d,TO_DATE(rejected_date,'YYYYMMDD'),SYSDATE) <= 7 THEN '0-7 days'
		WHEN datediff(d,TO_DATE(rejected_date,'YYYYMMDD'),SYSDATE) <= 14 THEN '7-14 days'
		WHEN datediff(d,TO_DATE(rejected_date,'YYYYMMDD'),SYSDATE) > 14 THEN '14+ days'
	END AS Rresolution_Pending_Days,
	
	case when invn_item.rejected_at IN ('OUTBOUND_QC', 'RETURN_FROM_OPS') then 'Outbound' else case when invn_item.rejected_at = 'CUSTOMER_RETURNED' then 'Not Outbound' else NULL end end  AS QC_Rejected,
	
	action_status AS action_status,
	(SELECT description FROM dim_rejection_action_status WHERE action_code = invn_item.action_status) AS description,
	(SELECT OWNER FROM dim_rejection_action_status WHERE action_code = invn_item.action_status) AS OWNER,
	(SELECT followup_action FROM dim_rejection_action_status WHERE action_code = invn_item.action_status) AS followup_action,

	dim_prd.style_status AS style_status,
		
	dim_prd.vendor_article_number AS vendor_article_number,
	dim_prd.vendor_article_name AS vendor_article_name,
	dim_prd.size AS size
 FROM
	fact_inventory_item AS invn_item,
	dim_product AS dim_prd,
	fact_purchase_order AS purchase_order
WHERE
	invn_item.quality IN ('Q2','Q3','Q4') AND
	invn_item.sku_id = dim_prd.sku_id AND
	invn_item.po_sku_id = purchase_order.po_sku_id AND
	(	purchase_order.vendor_name != 'OPEN STOCK' 
		OR 
		exists (SELECT COUNT(DISTINCT vendor_name) 
		     FROM fact_purchase_order AS Q1
		     WHERE Q1.sku_id = purchase_order.sku_id AND Q1.vendor_name != 'OPEN STOCK')
	) AND
	invn_item.item_status IN ('STORED','FOUND','CUSTOMER_RETURNED','RETURN_FROM_OPS') AND
	invn_item.action_status NOT IN ('CL_ADDED','CL_NOT_FOUND','CL_TRANSFER_INVENTORY')) as main
	left join vendor_inactive as vi 
	on vi.vendors_inactive=main.vendor_name 
	left join qc_brand as qc
	on qc."100_percentage_QC_brand"=main.brand
	left join fact_product_snapshot as fps
	on fps.sku_id=main.sku_id
	left join fact_product_snapshot2 as fps2
	on fps2.sku_id=main.sku_id
--	left join  vendor_name
--	on main.po_sku_id
           LIMIT 10                         
