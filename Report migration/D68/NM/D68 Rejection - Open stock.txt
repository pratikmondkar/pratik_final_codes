
/* D68 Rejection - Open Stock*/
SELECT 
	invn_item.core_item_id AS Item_Code,
	purchase_order.po_sku_id,
	invn_item.item_barcode AS Item_Barcode,
	dim_prd.sku_code AS SKU_Code,
	-- dim_prd.style_id AS style_id,
	CAST(dim_prd.style_id AS UNSIGNED) AS style_id,
	dim_prd.brand AS Brand,
	dim_prd.article_type AS Article_Type,
	STR_TO_DATE(CONCAT(CAST(invn_item.rejected_date AS CHAR),'-', LPAD(CAST(invn_item.rejected_time AS CHAR), 4, '0')), '%Y%m%d-%H%i') AS State_Change_date,
	DATE_FORMAT(STR_TO_DATE(CONCAT(CAST(invn_item.rejected_date AS CHAR),'-', LPAD(CAST(invn_item.rejected_time AS CHAR), 4, '0')), '%Y%m%d-%H%i'),'%M') AS State_Change_Month,
	purchase_order.vendor_name AS vendor_name_org,
	purchase_order.sku_id AS sku_id,
	purchase_order.vendor_name AS Vendor_Name,
	IF( purchase_order.item_purchase_price IS NULL OR purchase_order.item_purchase_price = 0 , dim_prd.article_value_ex_tax * .65 , purchase_order.item_purchase_price ) AS Item_Purchase_Price,
	purchase_order.tax_rate AS Tax_Rate,
	purchase_order.item_landed_price AS Landed_Price,
	invn_item.bin_barcode AS Bin_Barcode,
	invn_item.item_status AS Item_Status,
	invn_item.quality AS Quality_Status,
	invn_item.warehouse_id AS warehouse_id,
	invn_item.po_barcode AS PO_Code,
	invn_item.lot_barcode AS LOT_Code,
	invn_item.inward_date AS Inward_date,
	dim_prd.gtin AS EAN_Code,
	invn_item.reject_reason_code,
	invn_item.reject_reason_description,
	
	DATE_FORMAT(STR_TO_DATE(invn_item.Inward_date,'%Y%m%d'),'%Y-%m-%d') AS Inward_date,
	invn_item.reject_reason_code AS Reject_code,
	invn_item.reject_reason_description AS COMMENT,
	
	-- Fields added for D58 Extended report
	dim_prd.master_category AS Master_Category,
	dim_prd.subcategory AS Sub_Category,
	dim_prd.category_manager AS Category_Manager,
	dim_prd.brand_type AS Brand_Type,
	dim_prd.current_commercial_type AS Current_Commercial_Type,
	dim_prd.vendor_article_number AS Vendor_Article_Number,

	dim_prd.category_head AS Category_Head,
	dim_prd.season_code AS Season_Code,
	
	IF((SELECT COUNT(*) FROM dim_quality_rejection_stocks_parameters WHERE 100_percentage_QC_brand = dim_prd.brand) > 0 , 'Yes' , 'No' ) AS 100_percentage_QC_brand,
	
	IF((SELECT COUNT(*) FROM dim_quality_rejection_stocks_parameters WHERE vendors_inactive = purchase_order.vendor_name) > 0 , 'Yes' , 'No' ) AS is_vendor_inactive,
	
	
	SUBSTR(invn_item.rejected_date,1,4) AS rejected_year,
	
	CASE 
		WHEN TIMESTAMPDIFF(DAY,STR_TO_DATE(invn_item.inward_date,'%Y%m%d'),SYSDATE()) <= 30 THEN '0-30 days'
		WHEN TIMESTAMPDIFF(DAY,STR_TO_DATE(invn_item.inward_date,'%Y%m%d'),SYSDATE()) <= 60 THEN '30-60 days'
		WHEN TIMESTAMPDIFF(DAY,STR_TO_DATE(invn_item.inward_date,'%Y%m%d'),SYSDATE()) <= 90 THEN '60-90 days'
		WHEN TIMESTAMPDIFF(DAY,STR_TO_DATE(invn_item.inward_date,'%Y%m%d'),SYSDATE()) <= 120 THEN '90-120 days'
		WHEN TIMESTAMPDIFF(DAY,STR_TO_DATE(invn_item.inward_date,'%Y%m%d'),SYSDATE()) <= 150 THEN '120-150 days'
		WHEN TIMESTAMPDIFF(DAY,STR_TO_DATE(invn_item.inward_date,'%Y%m%d'),SYSDATE()) <= 180 THEN '150-180 days'
		WHEN TIMESTAMPDIFF(DAY,STR_TO_DATE(invn_item.inward_date,'%Y%m%d'),SYSDATE()) > 180 THEN '180+ days'
	END AS Inward_Age_Bucket,
	
	CASE 
		WHEN TIMESTAMPDIFF(DAY,STR_TO_DATE(rejected_date,'%Y%m%d'),SYSDATE()) <= 7 THEN '0-7 days'
		WHEN TIMESTAMPDIFF(DAY,STR_TO_DATE(rejected_date,'%Y%m%d'),SYSDATE()) <= 14 THEN '7-14 days'
		WHEN TIMESTAMPDIFF(DAY,STR_TO_DATE(rejected_date,'%Y%m%d'),SYSDATE()) > 14 THEN '14+ days'
	END AS Rresolution_Pending_Days,
	
	IF( invn_item.rejected_at IN ('OUTBOUND_QC', 'RETURN_FROM_OPS') , 'Outbound- Open Stock', IF( invn_item.rejected_at = 'CUSTOMER_RETURNED' , 'Not Outbound - Open Stock',NULL ) )  AS QC_Rejected,
	
	action_status AS action_status,
	(SELECT description FROM dim_rejection_action_status WHERE action_code = invn_item.action_status) AS description,
	(SELECT OWNER FROM dim_rejection_action_status WHERE action_code = invn_item.action_status) AS OWNER,
	(SELECT followup_action FROM dim_rejection_action_status WHERE action_code = invn_item.action_status) AS followup_action,

	dim_prd.style_status AS style_status,
	
	IF( (SELECT fact_prd_snap.is_live_on_portal FROM fact_product_snapshot AS fact_prd_snap WHERE fact_prd_snap.DATE = CAST(DATE_FORMAT(NOW()-INTERVAL 1 DAY, '%Y%m%d') AS UNSIGNED) AND fact_prd_snap.sku_id = dim_prd.sku_id) = 1 , 'Y', 'N ') AS is_sku_live_on_portal,
	IF( (SELECT MAX(fact_prd_snap.is_live_on_portal) FROM fact_product_snapshot AS fact_prd_snap WHERE fact_prd_snap.DATE = CAST(DATE_FORMAT(NOW()-INTERVAL 1 DAY, '%Y%m%d') AS UNSIGNED) AND fact_prd_snap.style_id = dim_prd.style_id) = 1 , 'Y', 'N ') AS is_style_live_on_portal,
	
	dim_prd.vendor_article_number AS vendor_article_number,
	dim_prd.vendor_article_name AS vendor_article_name,
	dim_prd.size AS size
FROM
	fact_inventory_item AS invn_item,
	dim_product AS dim_prd,
	fact_purchase_order AS purchase_order
WHERE
	invn_item.quality IN ('Q2','Q3','Q4') AND
	invn_item.sku_id = dim_prd.sku_id AND
	invn_item.po_sku_id = purchase_order.po_sku_id AND
	purchase_order.vendor_name = 'OPEN STOCK' AND
	1 != (SELECT COUNT(DISTINCT vendor_name) 
		     FROM fact_purchase_order AS Q1
		     WHERE Q1.sku_id = purchase_order.sku_id AND Q1.vendor_name != 'OPEN STOCK')
	 AND
	invn_item.item_status IN ('STORED','FOUND','CUSTOMER_RETURNED','RETURN_FROM_OPS') AND
	invn_item.action_status NOT IN ('CL_ADDED','CL_NOT_FOUND','CL_TRANSFER_INVENTORY')             