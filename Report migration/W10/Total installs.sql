SELECT COUNT(DISTINCT device_device_id)
FROM (SELECT device_device_id
      FROM clickstream.applaunch_view
      WHERE load_date BETWEEN TO_CHAR(SYSDATE-INTERVAL '15 days','YYYYMMDD') AND TO_CHAR(SYSDATE-INTERVAL '1 days','YYYYMMDD')
      UNION ALL
      SELECT device_device_id
      FROM clickstream.beacon_ping_view
      WHERE load_date BETWEEN TO_CHAR(SYSDATE-INTERVAL '15 days','YYYYMMDD') AND TO_CHAR(SYSDATE-INTERVAL '1 days','YYYYMMDD')
      UNION ALL
      SELECT device_device_id
      FROM clickstream.push_notification_received_view
      WHERE load_date BETWEEN TO_CHAR(SYSDATE-INTERVAL '15 days','YYYYMMDD') AND TO_CHAR(SYSDATE-INTERVAL '1 days','YYYYMMDD')) f

