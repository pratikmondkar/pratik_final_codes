select 
--Sign ups
to_char(yest_signups,'99,99,99,99,999') AS yday_signups,
to_char(d2_signups,'99,99,99,99,999') AS d2_signups,
to_char(d3_signups,'99,99,99,99,999') AS d3_signups,
to_char(wtd_signups,'99,99,99,99,999') AS wtd_signups,
to_char(w1_signups,'99,99,99,99,999') AS w1_signups,
to_char(mtd_signups,'99,99,99,99,999') AS mtd_signups,
to_char(m1_signups,'99,99,99,99,999') AS m1_signups,
to_char(td_signups,'99,99,99,99,999') AS td_signups,

-- Acquisitions
to_char(yest_acq,'99,99,99,99,999') AS yday_acq,
to_char(d2_acq,'99,99,99,99,999') AS d2_acq,
to_char(d3_acq,'99,99,99,99,999') AS d3_acq,
to_char(wtd_acq,'99,99,99,99,999') AS wtd_acq,
to_char(w1_acq,'99,99,99,99,999') AS w1_acq,
to_char(mtd_acq,'99,99,99,99,999') AS mtd_acq,
to_char(m1_acq,'99,99,99,99,999') AS m1_acq,
to_char(td_acq,'99,99,99,99,999') AS td_acq,

--Acquisition Revenue
to_char(yest_acq_rev,'99,99,99,99,999') AS yday_acq_rev,
to_char(d2_acq_rev,'99,99,99,99,999') AS d2_acq_rev,
to_char(d3_acq_rev,'99,99,99,99,999') AS d3_acq_rev,
to_char(wtd_acq_rev,'99,99,99,99,999') AS wtd_acq_rev,
to_char(w1_acq_rev,'99,99,99,99,999') AS w1_acq_rev,
to_char(mtd_acq_rev,'99,99,99,99,999') AS mtd_acq_rev,
to_char(m1_acq_rev,'99,99,99,99,999') AS m1_acq_rev,
to_char(td_acq_rev,'99,99,99,99,999') AS td_acq_rev,

--Rewards Awarded
to_char(yest_rwd_cr,'99,99,99,99,999') AS yday_rwd_cr,
to_char(d2_rwd_cr,'99,99,99,99,999') AS d2_rwd_cr,
to_char(d3_rwd_cr,'99,99,99,99,999') AS d3_rwd_cr,
to_char(wtd_rwd_cr,'99,99,99,99,999') AS wtd_rwd_cr,
to_char(w1_rwd_cr,'99,99,99,99,999') AS w1_rwd_cr,
to_char(mtd_rwd_cr,'99,99,99,99,999') AS mtd_rwd_cr,
to_char(m1_rwd_cr,'99,99,99,99,999') AS m1_rwd_cr,
to_char(td_rwd_cr,'99,99,99,99,999') AS td_rwd_cr,

-- Rewards Redeemed
to_char(yest_rwd_red,'99,99,99,99,999') AS yday_rwd_red,
to_char(d2_rwd_red,'99,99,99,99,999') AS d2_rwd_red,
to_char(d3_rwd_red,'99,99,99,99,999') AS d3_rwd_red,
to_char(wtd_rwd_red,'99,99,99,99,999') AS wtd_rwd_red,
to_char(w1_rwd_red,'99,99,99,99,999') AS w1_rwd_red,
to_char(mtd_rwd_red,'99,99,99,99,999') AS mtd_rwd_red,
to_char(m1_rwd_red,'99,99,99,99,999') AS m1_rwd_red,
to_char(td_rwd_red,'99,99,99,99,999') AS td_rwd_red,

--Cost per Acquisition
to_char(yest_acq_rev/yest_rwd_cr::decimal(10,2),'99,99,99,99,999') AS yday_cost_acq,
to_char(d2_acq_rev/d2_rwd_cr::decimal(10,2),'99,99,99,99,999') AS d2_cost_acq,
to_char(d3_acq_rev/d3_rwd_cr::decimal(10,2),'99,99,99,99,999') AS d3_cost_acq,
to_char(wtd_acq_rev/wtd_rwd_cr::decimal(10,2),'99,99,99,99,999') AS wtd_cost_acq,
to_char(w1_acq_rev/w1_rwd_cr::decimal(10,2),'99,99,99,99,999') AS w1_cost_acq,
to_char(mtd_acq_rev/mtd_rwd_cr::decimal(10,2),'99,99,99,99,999') AS mtd_cost_acq,
to_char(m1_acq_rev/m1_rwd_cr::decimal(10,2),'99,99,99,99,999') AS m1_cost_acq,
to_char(td_acq_rev/td_rwd_cr::decimal(10,2),'99,99,99,99,999') AS td_cost_acq
from
(
SELECT 
--signups
SUM(CASE WHEN day_diff=1 THEN signups END) AS yest_signups,
SUM(CASE WHEN day_diff=2 THEN signups END) AS d2_signups,
SUM(CASE WHEN day_diff=3 THEN signups END) AS d3_signups,
SUM(CASE WHEN week_to_date=1 THEN signups END) AS wtd_signups,
SUM(CASE WHEN week_diff=1 THEN signups END) AS w1_signups,
SUM(CASE WHEN month_to_date=1 THEN signups END) AS mtd_signups,
SUM(CASE WHEN month_diff=1 THEN signups END) AS m1_signups,
SUM(signups) as td_signups,

--acquisitions
SUM(CASE WHEN day_diff=1 THEN acquisitions END) AS yest_acq,
SUM(CASE WHEN day_diff=2 THEN acquisitions END) AS d2_acq,
SUM(CASE WHEN day_diff=3 THEN acquisitions END) AS d3_acq,
SUM(CASE WHEN week_to_date=1 THEN acquisitions END) AS wtd_acq,
SUM(CASE WHEN week_diff=1 THEN acquisitions END) AS w1_acq,
SUM(CASE WHEN month_to_date=1 THEN acquisitions END) AS mtd_acq,
SUM(CASE WHEN month_diff=1 THEN acquisitions END) AS m1_acq,
SUM(acquisitions) as td_acq,

--rewards credited
SUM(CASE WHEN day_diff=1 THEN rewards_credited END) AS yest_rwd_cr,
SUM(CASE WHEN day_diff=2 THEN rewards_credited END) AS d2_rwd_cr,
SUM(CASE WHEN day_diff=3 THEN rewards_credited END) AS d3_rwd_cr,
SUM(CASE WHEN week_to_date=1 THEN rewards_credited END) AS wtd_rwd_cr,
SUM(CASE WHEN week_diff=1 THEN rewards_credited END) AS w1_rwd_cr,
SUM(CASE WHEN month_to_date=1 THEN rewards_credited END) AS mtd_rwd_cr,
SUM(CASE WHEN month_diff=1 THEN rewards_credited END) AS m1_rwd_cr,
SUM(rewards_credited) as td_rwd_cr

FROM 
fact_referral_app_summary
JOIN DIM_DATE DD
on signups_date=dd.full_date) X,

(select 
--ACQ revenue
SUM(CASE WHEN day_diff=1 THEN shipped_order_revenue_inc_cashback END) AS yest_acq_rev,
SUM(CASE WHEN day_diff=2 THEN shipped_order_revenue_inc_cashback END) AS d2_acq_rev,
SUM(CASE WHEN day_diff=3 THEN shipped_order_revenue_inc_cashback END) AS d3_acq_rev,
SUM(CASE WHEN week_to_date=1 THEN shipped_order_revenue_inc_cashback END) AS wtd_acq_rev,
SUM(CASE WHEN week_diff=1 THEN shipped_order_revenue_inc_cashback END) AS w1_acq_rev,
SUM(CASE WHEN month_to_date=1 THEN shipped_order_revenue_inc_cashback END) AS mtd_acq_rev,
SUM(CASE WHEN month_diff=1 THEN shipped_order_revenue_inc_cashback END) AS m1_acq_rev,
SUM(shipped_order_revenue_inc_cashback) as td_acq_rev,

--Rewards redeemed
SUM(CASE WHEN day_diff=1 THEN loyalty_discount_redeemed END) AS yest_rwd_red,
SUM(CASE WHEN day_diff=2 THEN loyalty_discount_redeemed END) AS d2_rwd_red,
SUM(CASE WHEN day_diff=3 THEN loyalty_discount_redeemed END) AS d3_rwd_red,
SUM(CASE WHEN week_to_date=1 THEN loyalty_discount_redeemed END) AS wtd_rwd_red,
SUM(CASE WHEN week_diff=1 THEN loyalty_discount_redeemed END) AS w1_rwd_red,
SUM(CASE WHEN month_to_date=1 THEN loyalty_discount_redeemed END) AS mtd_rwd_red,
SUM(CASE WHEN month_diff=1 THEN loyalty_discount_redeemed END) AS m1_rwd_red,
SUM(loyalty_discount_redeemed) as td_rwd_red

from fact_order fo
join (select distinct order_id from fact_referral_conversion) r
on fo.order_id=r.order_id
join dim_date dd
on fo.order_created_date=dd.full_date ) Y

