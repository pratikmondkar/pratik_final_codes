SELECT table_schema,
--       table_name,
--			 mbytes/1024 AS total_gbs
       sum(mbytes) / 1024 AS total_gbs
FROM admin.v_table_info
--where table_schema='customer_insights' 
--and table_name like 'mfg%'
group by 1
ORDER BY total_gbs DESC;

drop table if exists dev.ss_ev_base_14sep;
drop table if exists dev.sj_magasin_temp_jan18;
drop table if exists dev.sj_magasin_temp;
drop table if exists dev.mfg_bag_lifetime;
drop table if exists dev.mfg_user_hist_final2;
drop table if exists dev.mfg_cus_group_map;
drop table if exists dev.mfg_user_hist_final;
drop table if exists dev.mfg_grp_fill2;
drop table if exists dev.mfg_bag_to_styles;
drop table if exists dev.mfg_group_defaults;
drop table if exists dev.mfg_grp_defualt_style;
drop table if exists dev.mfg_group_final;
drop table if exists dev.mfg_temp1;
drop table if exists dev.mfg_global_defaults;
drop table if exists dev.mfg_user_hist_fill1;
drop table if exists dev.mfg_clus_group_int;
drop table if exists dev.mfg_group_cluster;
drop table if exists dev.mfg_temp2;
drop table if exists dev.mfg_cluster_to_styles;
drop table if exists dev.mfg_bag_to_styles_all;
drop table if exists dev.mfg_bag_st;
drop table if exists dev.mfg_user_gender_excl;
drop table if exists dev.mfg_user_hist_fill2;
drop table if exists dev.mfg_user_group_map;

VACUUM dev.pm_traffic_source_temp;
