 select 'select * from customer_insights.' || table_name || ' where load_date = ' || replace(right(table_name,9),'_','') || ' union all '
from
information_schema.tables
where
table_schema = 'customer_insights'
and (table_name like 'funnel_2017%' or table_name like 'funnel_2016%')
and replace(right(table_name,9),'_','')::bigint between to_char(convert_timezone('Asia/Calcutta',sysdate-60),'YYYYMMDD')::bigint
and to_char(convert_timezone('Asia/Calcutta',sysdate),'YYYYMMDD')::bigint
order by table_name ; 
