DROP TABLE IF EXISTS dev.myntra_jabong_overlap;

CREATE TABLE dev.myntra_jabong_overlap
(
mynt_style_id 	bigint,
mynt_sku_id			bigint,
jabong_style_id	varchar(50),
jabong_sku_id		varchar(50)
)
DISTKEY (mynt_sku_id);

--truncate table dev.email_list;

copy dev.myntra_jabong_overlap   (mynt_style_id,mynt_sku_id,jabong_style_id,jabong_sku_id)
FROM 's3://testashutosh/backups/Feb9_DEDUPE000.csv' credentials 'aws_access_key_id=AKIAJ5HEDBVJTXTBSJNQ;aws_secret_access_key=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' 
delimiter ',' 
emptyasnull 
blanksasnull 
TRIMBLANKS 
TRUNCATECOLUMNS 
ACCEPTANYDATE 
ACCEPTINVCHARS AS ' ' 
maxerror 100000 
explicit_ids 
compupdate off 
statupdate off;



grant select on customer_insights.myntra_jabong_overlap to adhocpanel;
grant select on customer_insights.myntra_jabong_overlap to adhoc_fin_user;
grant select on customer_insights.myntra_jabong_overlap to adhoc_nofin_user;
grant select on customer_insights.myntra_jabong_overlap to explain_nofin_user;

grant select on customer_insights.myntra_jabong_overlap to adhoc_clickstream_fin;
grant select on customer_insights.myntra_jabong_overlap to explain_clickstream_fin;
grant select on customer_insights.myntra_jabong_overlap to adhoc_clickstream_no_fin;
grant select on customer_insights.myntra_jabong_overlap to explain_clickstream_no_fin;

grant select on dev.style_cluster to customer_insights_ddl;


create table customer_insights.myntra_jabong_overlap distkey(mynt_sku_id) as
select dp.business_unit,dp.article_type,dp.brand,dp.gender,dp.master_category,b.*
from dev.myntra_jabong_overlap b
left join dim_product dp on b.mynt_sku_id=dp.sku_id;

select count(distinct jabong_sku_id) jabong_skus, 
count(distinct mynt_sku_id) as mynt_skus,
count(1) as records
from customer_insights.myntra_jabong_overlap_duplicates
where mynt_id_count>1 and jabong_id_count>1 limit 100;

select * from customer_insights.myntra_jabong_overlap_duplicates where jabong_id_count>1 order by jabong_sku_id limit 100;

select * from customer_insights.myntra_jabong_overlap limit 100;

select business_unit,
count(distinct case when mynt_id_count=1 and jabong_id_count>1 then mynt_sku_id end) as myntra_duplicates,
count(distinct case when mynt_id_count>1 and jabong_id_count=1 then mynt_sku_id end) as jabong_duplicates,
count(distinct case when mynt_id_count>1 and jabong_id_count>1 then mynt_sku_id end) as both_duplicates
from customer_insights.myntra_jabong_overlap_duplicates
group by 1;

select a.*,b.overlap_skus,b.overlap_style
from
(select business_unit,
count(distinct sku_id) as all_skus,
count(distinct style_id) as all_style
from dim_product
group by 1) a  
left join
(select business_unit,
count(distinct case when jabong_sku_id is not null then mynt_sku_id end) as overlap_skus,
count(distinct case when jabong_sku_id is not null then mynt_style_id end) as overlap_style
from customer_insights.myntra_jabong_overlap
group by 1) b on a.business_unit=b.business_unit
;
