
SELECT COALESCE(foci.order_created_date) AS DATE,
       dd.week_of_the_year AS week,
       UPPER(dd.month_string) AS MONTH,
       dd.cal_year AS YEAR,
       COALESCE(foci.sku_id::BIGINT,fii.sku_id) AS sku_code,
       dp.brand,--dimension
       dp.article_type,
       dp.gender,
       dp.master_category,
       dp.business_unit,
       COALESCE(foci.po_type,fii.po_type) AS JIT_Classification,
       CASE
         WHEN sj_sku.sku_id IS NULL THEN 0
         ELSE 1
       END AS IS_SJIT_SKU_FLAG,
       case when dw.warehouse_name is null then 'N/A' else dw.warehouse_name end as Warehouse_name,
       nvl(fii.po_quantity,0) AS PO_Quantity,--metric
       nvl(fii.Fullfilment_quantity,0) AS Fullfilment_quantity,
       nvl(fii.Inward_Rejects,0) AS Inward_Rejects,
       nvl(fii.Fullfilment_quantity,0) - nvl(fii.Inward_Rejects,0) AS Inwarded_quantity,
       nvl(fii.Inventory_on_hand,0) AS Inventory_On_Hand,
       nvl(foci.Sold_Quantity,0) AS Sold_Quantity,
       nvl(foci.shipped_revenue,0) AS Shipped_Revenue,
       nvl(foci.Consolidated_items,0) AS Consolidated_items,
       nvl(foci.Cancelled_quantity,0) AS Cancelled_quantity,
       nvl(foci.TAT_O2P_hours,0) AS TAT_O2P_hours,
       nvl(foci.Promoters_count,0) AS Promoters_count,
       nvl(foci.Demoters_count,0) AS Demoters_count,
       nvl(foci.Responses_count,0) AS Responses_count,
       foci.TAT_denom_counter AS TAT_AVG_Denominator,
       dp.sku_id AS SKU_Count
FROM foci
  FULL JOIN fii
         ON foci.sku_id = fii.sku_id
        AND foci.order_created_date = fii.inward_date
        AND foci.po_type = fii.po_type
        AND foci.warehouse_id = fii.warehouse_id
  LEFT JOIN dim_product dp ON COALESCE (foci.sku_id::BIGINT,fii.sku_id) = dp.sku_id
  LEFT JOIN SJIT_SKU sj_sku ON COALESCE (foci.sku_id::BIGINT,fii.sku_id) = sj_sku.sku_id
  LEFT JOIN dim_date dd ON dd.full_date = COALESCE (foci.order_created_date,fii.inward_date)
  LEFT JOIN dim_warehouse dw ON dw.warehouse_id = COALESCE (foci.warehouse_id,fii.warehouse_id)